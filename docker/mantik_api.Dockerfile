ARG DEFAULT_LISTEN_PORT=8000

FROM thomose/poetry:3.11-1.2.2-slim as builder

COPY src/mantik_api /opt/src/mantik_api
COPY pyproject.toml poetry.lock /opt/
COPY docker/scripts /opt/scripts
COPY README.md /opt/README.md

WORKDIR /opt

RUN . scripts/install-src-package.sh

RUN . scripts/delete-cache.sh

FROM python:3.11-slim

ARG DEFAULT_LISTEN_PORT


ENV GIT_PYTHON_REFRESH=quiet

COPY --from=builder /venv /venv
ENV PATH=/venv/bin:$PATH

COPY alembic/ /opt/alembic
COPY alembic.ini /opt/

# Add labels.yaml for label curation on app startup
COPY labels/labels.yaml /opt/
ENV LABELS_YAML_PATH=/opt/labels.yaml

# gunicorn settings
# For the general documentation of gunicorn settings, see
# https://docs.gunicorn.org/en/stable/settings.html
#
# Notes:
#
# * For the number of workers chosen, see
#   https://docs.gunicorn.org/en/stable/design.html#how-many-workers
# * `--access/error-logfile="-"` enables logging to stdout
# * `--timeout` is set to 0 (infinity) to avoid workers being killed when submitting
#   runs to the Compute Backend that take some time to be submitted.
#   This should become redundant with https://gitlab.com/groups/mantik-ai/-/epics/89
#
ENV WORKERS=4
ENV WORKER_TIMEOUT=0
ENV LISTEN_PORT=${DEFAULT_LISTEN_PORT}

EXPOSE ${LISTEN_PORT}

WORKDIR /opt

ENTRYPOINT gunicorn mantik_api.app:app \
            --workers ${WORKERS} \
            --worker-class uvicorn.workers.UvicornWorker \
            --timeout=${WORKER_TIMEOUT}\
            --access-logfile="-" \
            --error-logfile="-" \
            --bind 0.0.0.0:${LISTEN_PORT}

