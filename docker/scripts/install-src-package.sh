# Installs the mantik package in /venv
python -m venv /venv \
&& . /venv/bin/activate \
&& pip install --upgrade pip \
&& poetry build \
&& pip install /opt/dist/*.whl
