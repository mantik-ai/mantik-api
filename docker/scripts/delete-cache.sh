find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf && \
  cd /venv && \
  find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf
