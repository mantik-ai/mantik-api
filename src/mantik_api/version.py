import logging

logger = logging.getLogger(__name__)


def _get_current_minor_version(version: str) -> str:
    minor = version.rsplit(".", maxsplit=1)[0]
    logger.info("Running API version %s (minor %s", version, minor)
    return minor


def _get_next_minor_version(version: str) -> str:
    version = version.removeprefix("v")
    major, minor, _ = map(int, version.split("."))
    next_minor = minor + 1
    return f"v{major}.{next_minor}"


def _create_version_path(version: str) -> str:
    return version.replace(".", "-")


LATEST_PATCH = "v0.2.0"
LATEST_MINOR = _get_current_minor_version(LATEST_PATCH)
LATEST_MINOR_PATH = _create_version_path(LATEST_MINOR)
NEXT_MINOR = _get_next_minor_version(LATEST_PATCH)
NEXT_MINOR_PATH = _create_version_path(NEXT_MINOR)
