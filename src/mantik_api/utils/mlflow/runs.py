import dataclasses
import logging
import os
import typing as t
import uuid

import mantik_api.tokens.jwt as jwt
import mantik_api.utils.mlflow._api as _api
import mantik_api.utils.mlflow.exceptions as mlflow_exceptions
import mantik_api.utils.mlflow.names as names
import mantik_api.utils.mlflow.search as _search
import mantik_api.utils.s3 as s3

logger = logging.getLogger(__name__)

ARTIFACT_BUCKET_NAME_ENV_VAR = "ARTIFACT_BUCKET_NAME"


@dataclasses.dataclass
class Run:
    id: uuid.UUID
    name: str

    @classmethod
    def from_json(cls, json: dict) -> t.Self:
        info = json["info"]
        return cls(
            id=uuid.UUID(info["run_id"]),
            name=info["run_name"],
        )


def delete(run_id: uuid.UUID, token: jwt.JWT) -> None:
    """Deletes an MLflow run on the tracking server.

    Parameters
    ----------
    run_id : uuid.UUID
        ID of the run.
    token : mantik_api.tokens.jwt.JWT
        Token to send in the request header to the MLflow Tracking Server.

    References
    -----
    https://mlflow.org/docs/latest/rest-api.html#delete-run

    """
    _api.send_post_request(
        url_endpoint="runs/delete",
        data={"run_id": _prepare_uuid(run_id)},
        token=token,
    )


def _prepare_uuid(id_: uuid.UUID) -> str:
    """Prepare the UUID for the MLflow API.

    The MLflow API returns UUIDs as strings without dashes (`-`),
    and also expected them without any.

    Since we treat UUIDs as strings with the respective dashes,
    they have to be removed before sending them to the MLflow API.

    """
    return id_.hex


def create_unique_name(
    experiment_id: int,
    name: str,
    token: jwt.JWT,
) -> str:
    name_suffix_stripped = names.strip_suffix(name)
    runs = search(
        experiment_id=experiment_id,
        token=token,
        filter_by=f"run_name LIKE '{name_suffix_stripped}%'",
    )
    if not runs:
        return name
    unique_name = names.append_suffix(
        name=name_suffix_stripped,
        names=[run.name for run in runs],
    )
    return unique_name


def search(
    experiment_id: int,
    token: jwt.JWT,
    filter_by: str,
    order_by: list[str] | None = None,
    max_results: int = 1000,
) -> list:
    return _search.search(
        endpoint="runs/search",
        data={"experiment_ids": [str(experiment_id)]},
        response_field="runs",
        constructor=Run.from_json,
        token=token,
        filter_by=filter_by,
        order_by=order_by or ["run_name"],
        max_results=max_results,
    )


def _list_artifact_directory(
    run_id: uuid.UUID, token: jwt.JWT, path: str | None = None
) -> dict:
    data = {"run_id": run_id.hex}
    if path:
        data = data | {"path": path}

    return _api.send_get_request(
        token=token, data=data, url_endpoint="artifacts/list"
    ).json()


def get_model_uri(
    mlflow_run_id: uuid.UUID,
    token: jwt.JWT,
    list_artifact_directory: t.Callable = _list_artifact_directory,
) -> str:
    """Returns the model uri for an MLflow run.

    In case of multiple models, the first one is chosen."""

    def _recurse_through_artifact_path(path=None, model_paths=None):
        if model_paths is None:
            model_paths = []

        files = list_artifact_directory(
            run_id=mlflow_run_id, token=token, path=path
        ).get("files", [])
        for file in files:
            if "MLmodel" in file["path"]:
                model_paths.append(path)

            if file["is_dir"]:
                _path = f"{path}/{file['path']}" if path is not None else file["path"]
                _recurse_through_artifact_path(path=_path, model_paths=model_paths)

        return model_paths

    model_paths = _recurse_through_artifact_path()

    if len(model_paths) == 0:
        raise mlflow_exceptions.RunArtifactsNotFoundException()
    elif len(model_paths) > 1:
        logger.warning(
            "Warning, multiple models present. Choosing the first one from: %s",
            str(model_paths),
        )

    model_path = model_paths[0]
    artifact_root = list_artifact_directory(run_id=mlflow_run_id, token=token)[
        "root_uri"
    ]
    return f"{artifact_root}/{model_path}"


def edit_run_name(mlflow_run_id: str, name: str, token: jwt.JWT) -> None:
    """Update an MLflow run on the tracking server.

    Parameters
    ----------
    mlflow_run_id : str
        Run ID for which run to update.
    name : str
        New name of the run.
    token : mantik_api.tokens.jwt.JWT
        Token to send in the request header to the MLflow Tracking Server.

    References
    ----------
    https://mlflow.org/docs/latest/rest-api.html#update-run
    """
    mlflow_run_id = str(mlflow_run_id).replace("-", "")

    _api.send_post_request(
        url_endpoint="runs/update",
        data={"run_id": mlflow_run_id, "run_name": name},
        token=token,
    ).json()


def get_run(mantik_run_id: str, token: jwt.JWT) -> dict:
    mlflow_run_id = str(mantik_run_id).replace("-", "")
    result = _api.send_get_request(
        url_endpoint="runs/get",
        data={"run_id": str(mlflow_run_id)},
        token=token,
    ).json()
    return result


def generate_pre_signed_run_artifact_url(
    mlflow_run_id: uuid.UUID,
    mlflow_experiment_id: int,
) -> str:
    """Returns pre-signed download url for run artifacts (as a zipped folder)

    If the run artifacts have not been zipped previously, first zip them
    and upload the zip file to the artifact store.

    Otherwise, generate and return a pre-signed url for the existing zip file.
    """
    bucket_name = os.environ[ARTIFACT_BUCKET_NAME_ENV_VAR]

    # MLflow run IDs are equivalent to the UUIDs without dashes (`uuid.UUID.hex`)
    artifact_key_prefix = (
        f"mlartifacts/{mlflow_experiment_id}/{mlflow_run_id.hex}/artifacts"
    )
    zipped_artifact_key = (
        f"mlartifacts/{mlflow_experiment_id}/{mlflow_run_id.hex}/artifacts.zip"
    )

    if not s3.key_exists(bucket_name=bucket_name, key=zipped_artifact_key):
        s3.zip_s3_directory(
            bucket_name=bucket_name,
            prefix=artifact_key_prefix,
            target_key=zipped_artifact_key,
        )

    return s3.generate_pre_signed_url(bucket_name=bucket_name, key=zipped_artifact_key)


def create(
    run_name: str,
    mlflow_experiment_id: int,
    token: jwt.JWT,
) -> uuid.UUID:
    run_id = _api.send_post_request(
        url_endpoint="runs/create",
        data={"run_name": run_name, "experiment_id": str(mlflow_experiment_id)},
        token=token,
    ).json()["run"]["info"]["run_id"]

    return uuid.UUID(run_id)
