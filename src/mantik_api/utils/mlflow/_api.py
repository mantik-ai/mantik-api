import logging
import re

import fastapi
import requests
import starlette.status as status

import mantik_api.tokens.jwt as jwt
import mantik_api.utils.env as env
import mantik_api.utils.mlflow.exceptions as exceptions

# import mantik_api.routes.mlflow.tracking_server_router as tracking_server_router

logger = logging.getLogger(__name__)


def _send_request(
    token: jwt.JWT, data: dict, url_endpoint: str, method: str
) -> requests.Response:
    url = _clean_double_slashes(
        f'{env.get_required_env_var("MLFLOW_TRACKING_URI_INTERNAL")}'
        f"/api/2.0/mlflow/{url_endpoint}"
    )
    try:
        response = requests.request(
            method=method,
            url=url,
            json=data,
            headers={
                "Authorization": f"Bearer {token.to_string()}",
                "Accept": "application/json",
                "MantikOrigin": "MantikApi",
            },
        )
        response.raise_for_status()
    except requests.HTTPError as e:
        logger.exception(
            "Call to MLflow API %s with data %s failed: %s",
            url,
            data,
            e.response.text,
            exc_info=True,
        )
        if e.response.status_code == status.HTTP_400_BAD_REQUEST:
            _handle_mlflow_exception(response=e.response, exception=e)
        if e.response.status_code == status.HTTP_403_FORBIDDEN:
            raise fastapi.HTTPException(
                status_code=e.response.status_code, detail=e.response.text
            )
        raise
    else:
        return response


def send_post_request(
    token: jwt.JWT, data: dict, url_endpoint: str
) -> requests.Response:
    return _send_request(
        token=token, data=data, url_endpoint=url_endpoint, method="POST"
    )


def send_get_request(
    token: jwt.JWT, data: dict, url_endpoint: str
) -> requests.Response:
    return _send_request(
        token=token, data=data, url_endpoint=url_endpoint, method="GET"
    )


def _clean_double_slashes(url: str) -> str:
    scheme, rest = url.split("://", 1)
    clean_rest = rest.replace("//", "/")
    clean_url = f"{scheme}://{clean_rest}"
    return clean_url


def _handle_mlflow_exception(
    response: requests.Response, exception: requests.HTTPError
) -> None:
    data = response.json()
    error_code = data["error_code"]
    message = data["message"]

    if _duplicate_resource(error_code=error_code):
        if (name := _duplicate_experiment_name(message)) is not None:
            raise exceptions.DuplicateResourceException(
                f"Experiment {name!r} already exists"
            )

    raise requests.HTTPError(
        f"Request to MLflow API failed with code {error_code}: {message}",
        request=exception.request,
        response=exception.response,
    )


def _duplicate_resource(error_code: str) -> bool:
    return error_code == "RESOURCE_ALREADY_EXISTS"


def _duplicate_experiment_name(message: str) -> str | None:
    result = re.findall(r"Experiment\(name=(.*)\) already exists", message)
    try:
        [match] = result
        return match
    except ValueError:
        return None
