class MlflowException(Exception):
    """Base class for MLflow Exceptions."""


class DuplicateResourceException(MlflowException):
    """Name (e.g. of experiment or run) already exists.

    Notes
    -----
    In the MLflow DB, experiment and run names within experiments have
    to be unique.

    """


class RunArtifactsNotFoundException(MlflowException):
    """No artifacts for run exist"""
