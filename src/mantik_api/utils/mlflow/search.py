import typing as t

import mantik_api.tokens.jwt as jwt
import mantik_api.utils.mlflow._api as _api


def search(
    endpoint: str,
    response_field: str,
    constructor: t.Callable,
    token: jwt.JWT,
    filter_by: str,
    order_by: list[str] | None = None,
    max_results: int = 1000,
    data: dict | None = None,
) -> list:
    """Search all entities matching given filter expression.

    Parameters
    ----------
    endpoint: str
         Endpoint to call
         e.g. experiments/search or runs/search
    response_field: str
         Key to the entities list in the response.
         e.g. "experiments" or "runs"
    constructor: t.Callable,
    token : mantik_api.tokens.jwt.JWT
        Token to send in the request header to the MLflow Tracking Server.
    filter_by : str
        SQL filter string.

        Only ``LIKE`` supported. Multiple expressions can be appended using ``AND``.

        Example: ``"name LIKE 'test-experiment-%'"``
    order_by : list[str], default=["name"]
        Fields by which to order.
    max_results : int, default=1000
        Maximum number of results to return.
    data: dict | None, default=[None]
         Extra data to be passed to the endpoint.


    Returns
    -------
    list
        Matching entities.


    References
    ----------
    https://mlflow.org/docs/latest/rest-api.html#search-experiment

    """
    entities = []
    next_page_token = None
    while True:
        data = {
            **(data or {}),
            "max_results": max_results,
            "filter": filter_by,
            "order_by": order_by or ["name"],
            # ViewType 3 lists all experiments (active and flagged as delete), see
            # https://mlflow.org/docs/2.2.2/python_api/mlflow.entities.html#mlflow.entities.ViewType  # noqa: E501
            "view_type": 3,
        }
        if next_page_token is not None:
            data["page_token"] = next_page_token

        result = _api.send_post_request(
            url_endpoint=endpoint,
            data=data,
            token=token,
        ).json()
        # The response is just an empty dictionary if no match was found.
        if not result:
            return entities

        entities.extend(result[response_field])

        # If response contains ``next_page_token``, there is another set of
        # experiments matching the expression.
        if "next_page_token" in result and result["next_page_token"] is not None:
            next_page_token = result["next_page_token"]
            continue
        else:
            break
    return list(map(constructor, entities))
