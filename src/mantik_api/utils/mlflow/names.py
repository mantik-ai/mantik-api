import re


def strip_suffix(name: str) -> str:
    """Strip a `-<number>` suffix from a given string."""
    regex = re.compile(r"(-[0-9]*)?$")
    return regex.sub("", name)


def append_suffix(name: str, names: list[str]) -> str:
    name_suffix_stripped = strip_suffix(name)

    if name_suffix_stripped not in names:
        return name_suffix_stripped

    suffixes = _get_suffixes(name=name_suffix_stripped, names=names)
    new_suffix = _create_new_suffix(suffixes)
    return f"{name_suffix_stripped}-{new_suffix}"


def _get_suffixes(name: str, names: list[str]) -> list[int]:
    """Get all suffixes of existing experiments that have the same name.

    Returns
    -----
    list[int]
        If ``name="test"``, and there is no existing experiments with that name
        and an integer suffix, the returned list will be empty.

        If there is at least one more experiment with name ``test-[0-9]*``,
        the returned list will contain that suffix. E.g. if the name is
        ``test-123``, the returned list will be ``[123]``.

    """
    regex = re.compile(rf"^{name}(-[0-9]*)?$")
    suffixes = []
    for _name in names:
        if not regex.match(_name):
            continue
        [suffix] = regex.findall(_name)
        if suffix:
            suffixes.append(int(suffix.removeprefix("-")))
    return sorted(suffixes)


def _create_new_suffix(suffixes: list[int]) -> int:
    """Find the first missing integer in ``suffixes``."""
    if not suffixes:
        return 1
    for index, suffix in enumerate(suffixes):
        new_suffix = index + 1
        if suffix == new_suffix:
            continue
        return new_suffix
    return len(suffixes) + 1
