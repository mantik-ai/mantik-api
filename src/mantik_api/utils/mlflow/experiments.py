import dataclasses
import typing as t

import mantik_api.tokens.jwt as jwt
import mantik_api.utils.mlflow._api as _api
import mantik_api.utils.mlflow.names as names
import mantik_api.utils.mlflow.search as _search


@dataclasses.dataclass
class Experiment:
    id: int
    name: str

    @classmethod
    def from_json(cls, json: dict) -> t.Self:
        return cls(
            id=int(json["experiment_id"]),
            name=json["name"],
        )


def create_unique_name(
    name: str,
    token: jwt.JWT,
) -> str:
    name_suffix_stripped = names.strip_suffix(name)
    experiments = search(
        token=token,
        filter_by=f"name LIKE '{name_suffix_stripped}%'",
    )
    if not experiments:
        return name
    return names.append_suffix(
        name=name_suffix_stripped,
        names=[experiment.name for experiment in experiments],
    )


def search(
    token: jwt.JWT,
    filter_by: str,
    order_by: list[str] | None = None,
    max_results: int = 1000,
) -> list:
    return _search.search(
        endpoint="experiments/search",
        response_field="experiments",
        constructor=Experiment.from_json,
        token=token,
        filter_by=filter_by,
        order_by=order_by,
        max_results=max_results,
    )


def create(experiment_name: str, token: jwt.JWT) -> int:
    """Creates an MLflow experiment on the tracking server.

    Parameters
    ----------
    experiment_name : str
        Name of the experiment.
    token : mantik_api.tokens.jwt.JWT
        Token to send in the request header to the MLflow Tracking Server.

    Returns
    -------
    int
        ID of the experiment


    References
    ----------
    https://mlflow.org/docs/latest/rest-api.html#create-experiment

    """
    return int(
        _api.send_post_request(
            url_endpoint="experiments/create",
            data={"name": experiment_name},
            token=token,
        ).json()["experiment_id"]
    )


def update(experiment_id: int, experiment_name: str, token: jwt.JWT) -> None:
    """Updates an MLflow experiment on the tracking server.

    Parameters
    ----------
    experiment_id : int
        MLflow ID of the experiment.
    experiment_name : str
        Name of the experiment.
    token : mantik_api.tokens.jwt.JWT
        Token to send in the request header to the MLflow Tracking Server.

    References
    ----------
    https://mlflow.org/docs/latest/rest-api.html#update-experiment

    """
    _api.send_post_request(
        url_endpoint="experiments/update",
        data={"experiment_id": str(experiment_id), "new_name": experiment_name},
        token=token,
    )


def delete(experiment_id: int, token: jwt.JWT) -> None:
    """Deletes an MLflow experiment on the tracking server.

    Parameters
    ----------
    experiment_id : int
        ID of the experiment.
    token : mantik_api.tokens.jwt.JWT
        Token to send in the request header to the MLflow Tracking Server.

    References
    -----
    https://mlflow.org/docs/latest/rest-api.html#delete-experiment

    """
    _api.send_post_request(
        url_endpoint="experiments/delete",
        data={"experiment_id": str(experiment_id)},
        token=token,
    )
