import enum


class CaseInsensitiveStrEnum(enum.StrEnum):
    """StrEnum that allows case-insensitive values."""

    @classmethod
    def _missing_(cls, value: str):
        """Allow case-insensitive values."""
        value = value.lower()
        for member in cls:
            if member.lower() == value:
                return member
        return None
