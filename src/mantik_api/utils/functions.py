import itertools
import typing as t


def flat_map(list2d: list[list[t.Any]]) -> list[t.Any]:
    return list(itertools.chain.from_iterable(list2d))
