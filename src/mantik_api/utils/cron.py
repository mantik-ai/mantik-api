import datetime

import fastapi
import pyawscron
import pytz
import starlette.status as status


def validate_cron_expression(expression: str) -> None:
    aws_expression = _unix_to_aws_cron_expression(expression)
    _validate_aws_cron(aws_expression)
    _prevent_undesired_cron_expressions(aws_expression)


def _unix_to_aws_cron_expression(expression: str) -> str:
    """Convert a Unix Cron expression to an AWS Cron expression.

    Notes
    -----
    We use default Cron expressions (5 places) in the API,
    but process as AWS Cron expression (6 places) internally.
    Hence, for validation, we need to add one `*` for the `year`
    place.

    """
    return f"{expression} *"


def _validate_aws_cron(expression: str) -> None:
    try:
        pyawscron.AWSCron(expression)
    except (ValueError, IndexError):
        raise fastapi.HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail=f"Invalid cron expression: {expression}",
        )


def _prevent_undesired_cron_expressions(expression: str) -> None:
    first_occurrence, second_occurrence = pyawscron.AWSCron.get_next_n_schedule(
        n=2, from_date=datetime.datetime.now(tz=datetime.UTC), cron=expression
    )
    interval = second_occurrence - first_occurrence

    if interval < datetime.timedelta(hours=1):
        raise fastapi.HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail="Schedule interval must be greater than 1 hour",
        )


def validate_time_zone(time_zone: str) -> None:
    try:
        pytz.timezone(time_zone)
    except pytz.UnknownTimeZoneError:
        raise fastapi.HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail=f"Invalid time zone: {time_zone}",
        )
