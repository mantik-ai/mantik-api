import io


class UnseekableBytesStream(io.RawIOBase):
    """Stream bytes in an unseekable stream.

    Allows to append bytes and read written bytes. Can be used to read bytes
    from ``Iterator[bytes]`` such as a streamed ``requests.get`` response.

    """

    def __init__(self):
        self._buffer = b""

    def writable(self):
        return True

    def write(self, b) -> int:
        """Write files to stream."""
        if self.closed:
            raise ValueError("Stream was closed")
        self._buffer += b
        return len(b)

    def get(self) -> bytes:
        """Get current bytes and reset buffer."""
        chunk = self._buffer
        self._buffer = b""
        return chunk
