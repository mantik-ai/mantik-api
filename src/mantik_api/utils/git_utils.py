import concurrent.futures
import typing as t
import uuid
from urllib.parse import urlparse

import fastapi
import requests.utils

import mantik_api.database
import mantik_api.database as database
import mantik_api.models as models
import mantik_api.routes.rbac.bearer
from mantik_api.invoke_compute_backend import exceptions as exceptions


def raw_gitlab_url(url: str) -> str:
    """Return the raw gitlab URL

    Supports self-hosted and public gitlab urls.
    """
    project_path = urlparse(url).path[1:]
    gitlab_project_id_url_encoded = requests.utils.quote(project_path, safe="")
    return f"{urlparse(url).scheme}://{urlparse(url).hostname}/api/v4/projects/{gitlab_project_id_url_encoded}"  # noqa E501


def raw_file_gitlab_url(url: str, filepath: str, ref: str) -> str:
    """Return the raw gitlab URL of a gitlab file

    Supports self-hosted and public gitlab urls.
    """
    filepath_url_encoded = requests.utils.quote(filepath, safe="")

    return f"{raw_gitlab_url(url)}/repository/files/{filepath_url_encoded}/raw?ref={ref}"  # noqa E501


def raw_github_url(url: str) -> str:
    repo = urlparse(url).path[1:]
    return f"https://api.github.com/repos/{repo}"


def raw_bitbucket_url(url: str):
    repo = urlparse(url).path[1:]
    return f"https://api.bitbucket.org/2.0/repositories/{repo}"


def raw_file_github_url(repo: str, filepath: str, ref: str) -> str:
    return f"https://raw.githubusercontent.com/{repo}/{ref}/{filepath}"


def raw_file_bitbucket_url(repo: str, filepath: str, ref: str, domain: str) -> str:
    # Distinguish between bitbucket.org and self-hosted
    if domain == "bitbucket.org":
        return f"{repo}/raw/{ref}/{filepath}"
    else:  # self-hosted
        return f"{repo}/browse/{filepath}?at={ref}"


def git_repository_is_accessible(
    git_url: str,
    platform: mantik_api.database.git_repository.Platform,
    access_token: str | None,
) -> bool:
    match platform:
        case mantik_api.database.git_repository.Platform.GitLab:
            request_url = raw_gitlab_url(url=git_url)
        case mantik_api.database.git_repository.Platform.GitHub:
            request_url = raw_github_url(url=git_url)
        case mantik_api.database.git_repository.Platform.Bitbucket:
            request_url = raw_bitbucket_url(url=git_url)
        case _:
            raise exceptions.InvokeComputeBackendException("Unsupported Git platform")

    request_headers = (
        git_authorization_header(access_token=access_token, platform=platform)
        if access_token
        else None
    )
    response = requests.get(request_url, headers=request_headers)
    return response.status_code == 200


def git_authorization_header(
    access_token: str, platform: mantik_api.database.git_repository.Platform
) -> dict:
    match platform:
        case mantik_api.database.git_repository.Platform.GitLab:
            return {"PRIVATE-TOKEN": access_token}
        case mantik_api.database.git_repository.Platform.GitHub:
            return {"Authorization": f"token {access_token}"}
        case _:
            raise exceptions.InvokeComputeBackendException(
                f"Unsupported Git platform for private repos {platform}"
            )


def check_if_repos_are_unlocked(
    db: mantik_api.database.client.main.Client,
    project_id: uuid.UUID,
    repo_ids: list[uuid.UUID],
    orm: type[database.code_repository.CodeRepository]
    | type[database.data_repository.DataRepository],
    token,
) -> dict[uuid.UUID, bool]:
    """Check whether a set of git repos is accessible to the calling user."""
    git_repositories = [
        db.get_one(
            orm_model_type=orm,
            constraints={
                orm.id: repository_id,
                orm.project_id: project_id,
            },
        )
        for repository_id in repo_ids
    ]

    git_access_tokens = []

    for git_repository in git_repositories:
        git_repository_connection_id = git_repository.my_git_connection_id(
            token.user_id
        )

        if not git_repository_connection_id:
            git_access_tokens.append(None)
            continue

        connection = db.get_one(
            orm_model_type=database.connection.Connection,
            constraints={
                database.connection.Connection.id: git_repository_connection_id,
                database.connection.Connection.user_id: token.user_id,
            },
            select_in_loading={database.connection.Connection.user: {}},
        )

        # get the access token from the Vault
        git_access_token = (
            models.connection.Connection.from_database_model_with_credentials(
                connection, token=token
            ).token
        )
        git_access_tokens.append(git_access_token)

    # the function will make many HTTP requests to git, therefore
    # it is made concurrent to speed up performance. Below we collect the parameters
    # for the concurrent calls
    function_arguments_list: list[tuple[str, database.git_repository.Platform, str]] = [
        (git_repo.uri, git_repo.platform, git_access_token)
        for git_repo, git_access_token in zip(git_repositories, git_access_tokens)
    ]
    results = execute_function_concurrently(
        function=git_repository_is_accessible,
        list_of_parameters_tuples=function_arguments_list,
    )
    return {
        git_repo_id: is_accessible
        for git_repo_id, is_accessible in zip(repo_ids, results)
    }


T = t.TypeVar("T")


def execute_function_concurrently(
    function: t.Callable[[...], T],
    list_of_parameters_tuples: list[tuple],
    max_workers=10,
) -> list[T]:
    """Executes a function concurrently for a list of parameter tuples."""
    with concurrent.futures.ThreadPoolExecutor(max_workers=max_workers) as executor:
        return list(
            executor.map(
                lambda parameters_tuple: function(*parameters_tuple),
                list_of_parameters_tuples,
            )
        )


def verify_platform_matches(
    repository_platform: mantik_api.database.git_repository.Platform,
    connection_platform: mantik_api.database.connection.ConnectionProvider,
):
    if not repository_platform.value == connection_platform.value:
        raise fastapi.HTTPException(
            422,
            detail=f"Connection platform {repository_platform.value!r} and "
            f"repository platform {connection_platform.value!r} do not match.",
        )
