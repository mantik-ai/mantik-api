import dataclasses
import logging
import uuid

import hvac.exceptions

import mantik_api.tokens.jwt as jwt
import mantik_api.utils.env as env

logger = logging.getLogger(__name__)


@dataclasses.dataclass(frozen=True)
class Credentials:
    login_name: str | None = None
    password: str | None = None
    token: str | None = None
    private_key: str | None = None

    @classmethod
    def from_response(cls, response: dict):
        data = response["data"]["data"]
        return cls(
            login_name=data.get("login_name"),
            password=data.get("password"),
            token=data.get("token"),
            private_key=data.get("private_key"),
        )

    def to_dict(self) -> dict[str, str | None]:
        return dataclasses.asdict(self)


def create_or_update_credential(
    connection_id: uuid.UUID,
    credentials: Credentials,
    token: jwt.JWT,
):
    try:  # FIXME: remove once database is fixed
        client = _get_vault_client(token)
        entity_id = _get_entity_id(client)
        client.secrets.kv.v2.create_or_update_secret(
            path=f"{entity_id}/{str(connection_id)}",
            secret=credentials.to_dict(),
        )
    except hvac.exceptions.VaultError as e:
        logger.warning(
            "Failed to create/update connection %s in vault: %s", connection_id, str(e)
        )


def delete_credential(
    connection_id: uuid.UUID,
    token: jwt.JWT,
):
    try:  # FIXME: remove once database is fixed
        client = _get_vault_client(token)
        entity_id = _get_entity_id(client)
        client.secrets.kv.v2.delete_latest_version_of_secret(
            path=f"{entity_id}/{str(connection_id)}",
        )
    except hvac.exceptions.VaultError as e:
        logger.warning(
            "Failed to delete connection %s from vault: %s", connection_id, str(e)
        )


def get_credential(
    connection_id: uuid.UUID,
    token: jwt.JWT,
) -> Credentials:
    try:  # FIXME: remove once database is fixed
        client = _get_vault_client(token)
        entity_id = _get_entity_id(client)
        response = client.secrets.kv.v2.read_secret(
            path=f"{entity_id}/{str(connection_id)}",
        )
        return Credentials.from_response(response)
    except hvac.exceptions.VaultError as e:
        logger.warning(
            "Failed to fetch connection %s from vault, returning fake credentials: %s",
            connection_id,
            str(e),
        )
        return Credentials()


def _get_vault_client(token: jwt.JWT) -> hvac.Client:
    client = hvac.Client(url=env.get_required_env_var("VAULT_URL"))
    client_token = client.auth.jwt.jwt_login(
        role="user",
        jwt=token.to_string(),
    )[
        "auth"
    ]["client_token"]
    client.token = client_token
    return client


def _get_entity_id(client: hvac.Client) -> str:
    return client.auth.token.lookup_self()["data"]["entity_id"]
