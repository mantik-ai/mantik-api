import io
import os
import zipfile

import boto3
import botocore.exceptions

import mantik_api.utils.mlflow.exceptions as mlflow_exceptions

ONE_HOUR_IN_SECONDS = 3600


def generate_pre_signed_url(
    bucket_name: str, key: str, expires_in_seconds: int = ONE_HOUR_IN_SECONDS
) -> str:
    """Return a pre-signed S3 download URL."""
    s3_client = boto3.client("s3")

    return s3_client.generate_presigned_url(
        "get_object",
        Params={"Bucket": bucket_name, "Key": key},
        ExpiresIn=expires_in_seconds,
    )


def key_exists(bucket_name: str, key: str) -> bool:
    return key in list_dir(bucket_name=bucket_name, prefix=key)


def get_file(bucket_name: str, key: str):
    s3 = boto3.client("s3")
    try:
        return s3.get_object(Bucket=bucket_name, Key=key)["Body"].read()
    except botocore.exceptions.ClientError as ex:
        if ex.response["Error"]["Code"] == "NoSuchKey":
            raise mlflow_exceptions.RunArtifactsNotFoundException()
        else:
            raise


def list_dir(bucket_name: str, prefix: str) -> list[str]:
    """NOTE: this could return a lot of values if prefix is not extremely specific"""
    s3 = boto3.client("s3")
    result = []
    paginator = s3.get_paginator("list_objects_v2")
    pages = paginator.paginate(Bucket=bucket_name, Prefix=prefix)

    for page in pages:
        result += [o["Key"] for o in page.get("Contents", [])]
    return result


def zip_s3_directory(bucket_name: str, prefix: str, target_key: str) -> None:
    s3 = boto3.client("s3")
    keys = list_dir(bucket_name, prefix)
    if not keys:
        raise mlflow_exceptions.RunArtifactsNotFoundException()

    zip_buffer = io.BytesIO()
    with zipfile.ZipFile(zip_buffer, "a") as zip_file:
        for file_key in keys:
            relative_file_path = os.path.relpath(file_key, prefix)
            file_data = get_file(bucket_name=bucket_name, key=file_key)
            zip_file.writestr(relative_file_path, file_data)

    zip_buffer.seek(0)
    s3.put_object(Bucket=bucket_name, Key=target_key, Body=zip_buffer.getvalue())


def delete_key(bucket_name: str, key: str) -> None:
    """NOTE: If the key does NOT exist, it will NOT trigger an error."""
    s3 = boto3.client("s3")
    s3.delete_object(Bucket=bucket_name, Key=f"{key}")
