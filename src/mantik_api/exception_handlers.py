import fastapi.exceptions
import starlette.status as status

import mantik_api.aws.cognito.exceptions as cognito_exceptions
import mantik_api.aws.scheduler as scheduler
import mantik_api.compute_backend as compute_backend
import mantik_api.database.exceptions as database_exceptions
import mantik_api.invoke_compute_backend.exceptions as invoke_compute_backend_exceptions
import mantik_api.utils.mlflow as mlflow


async def database_not_found_exception_handler(
    request: fastapi.Request,
    exc: database_exceptions.NotFoundError,  # noqa
) -> fastapi.responses.JSONResponse:
    return fastapi.responses.JSONResponse(
        status_code=status.HTTP_404_NOT_FOUND,
        content={"detail": "Resource not found"},
    )


async def authentication_failed_exception_handler(
    request: fastapi.Request,
    exc: cognito_exceptions.AuthenticationFailedException,  # noqa
) -> fastapi.responses.JSONResponse:
    return fastapi.responses.JSONResponse(
        status_code=status.HTTP_401_UNAUTHORIZED,
        content={"detail": str(exc)},
    )


async def invoke_compute_backend_exception_handler(
    request: fastapi.Request,
    exc: invoke_compute_backend_exceptions.InvokeComputeBackendException,  # noqa
) -> fastapi.responses.JSONResponse:
    return fastapi.responses.JSONResponse(
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        content={"detail": str(exc)},
    )


async def tracking_server_exception(
    request: fastapi.Request,
    exc: mlflow.exceptions.MlflowException,  # noqa
) -> fastapi.responses.JSONResponse:
    return fastapi.responses.JSONResponse(
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        content={"detail": str(exc)},
    )


async def inconsistent_ids_exception_handler(
    request: fastapi.Request,
    exc: database_exceptions.InconsistentIdsError,  # noqa
) -> fastapi.responses.JSONResponse:
    return fastapi.responses.JSONResponse(
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        content={"detail": "IDs are inconsistent"},
    )


async def sqlalchemy_exception_handler(
    request: fastapi.Request,
    exc: database_exceptions.DatabaseExecutionError,  # noqa
) -> fastapi.responses.JSONResponse:
    return fastapi.responses.JSONResponse(
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        content={"detail": exc.message},
    )


async def referenced_resource_exists_handler(
    request: fastapi.Request,
    exc: database_exceptions.ReferencedResourceExistsError,  # noqa
) -> fastapi.responses.JSONResponse:
    return fastapi.responses.JSONResponse(
        status_code=status.HTTP_409_CONFLICT,
        content={"detail": str(exc)},
    )


async def run_schedule_action_failed_handler(
    request: fastapi.Request,
    exc: scheduler.client.SchedulerActionFailedException,
) -> fastapi.responses.JSONResponse:
    return fastapi.responses.JSONResponse(
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        content={"detail": str(exc)},
    )


async def compute_backend_exception_handler(
    request: fastapi.Request,
    exc: compute_backend.exceptions.ComputeBackendException,
) -> fastapi.responses.JSONResponse:
    return fastapi.responses.JSONResponse(
        status_code=status.HTTP_424_FAILED_DEPENDENCY,
        content={"detail": str(exc)},
    )


HANDLERS = {
    database_exceptions.NotFoundError: database_not_found_exception_handler,
    cognito_exceptions.AuthenticationFailedException: authentication_failed_exception_handler,  # noqa: E501
    invoke_compute_backend_exceptions.InvokeComputeBackendException: invoke_compute_backend_exception_handler,  # noqa: E501
    mlflow.exceptions.MlflowException: tracking_server_exception,
    database_exceptions.InconsistentIdsError: inconsistent_ids_exception_handler,
    database_exceptions.DatabaseExecutionError: sqlalchemy_exception_handler,
    database_exceptions.ReferencedResourceExistsError: referenced_resource_exists_handler,  # noqa: E501
    scheduler.client.SchedulerActionFailedException: run_schedule_action_failed_handler,  # noqa: E501
    compute_backend.exceptions.ComputeBackendException: compute_backend_exception_handler,  # noqa: E501
}
