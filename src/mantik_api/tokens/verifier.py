"""JWT validation.

See
https://docs.aws.amazon.com/cognito/latest/developerguide/amazon-cognito-user-pools-using-tokens-verifying-a-jwt.html  # noqa: E501

"""
import logging

import mantik_api.aws.cognito as _cognito
import mantik_api.tokens.exceptions as exceptions
import mantik_api.tokens.jwks as _jwks
import mantik_api.tokens.jwt as _jwt

logger = logging.getLogger(__name__)


class TokenVerifier:
    """Verify JWTs."""

    def __init__(
        self,
        cognito: _cognito.client.Properties | None = None,
        jwks: _jwks.JWKS | None = None,
        secret_required: bool | None = None,
    ):
        """Get JWKS file if not given."""
        if cognito is None:
            self._ensure_secret_required_parameter_is_set(secret_required)
            cognito = _cognito.client.Properties.from_env(
                secret_required=secret_required
            )

        self.cognito = cognito
        self.jwks = jwks or _jwks.JWKS.from_cognito(self.cognito)

    @staticmethod
    def _ensure_secret_required_parameter_is_set(
        secret_required: bool | None = None,
    ) -> None:
        """Ensure that the secret parameter is set and not `None`.

        The class allows an optional injection of the Cognito Properties. If
        these are not injected, they are initialized in this class. This,
        however, requires to pass a flag `secret_required` that - if `True` -
        triggers that the environment variable for the Cognito App Client
        secret is read.

        Hence, it must be ensured that this flag is given if the Cognito
        Properties are not injected.

        """
        if secret_required is None:
            raise RuntimeError(
                "If Cognito Properties are not injected, it has to be "
                "defined whether the Cognito App Client secret is required"
            )

    def verify_token(self, token: str, buffer_time_days: int = 0) -> _jwt.JWT:
        """Return whether a given token is valid.

        Parameters
        ----------
        token : str
            The token to verify.
        buffer_time_days : int
            Extra days of validity that you can add to a token,
            default is 0.

        Raises
        ------
        ValidationFailedException
            The given token is not valid.

        """
        logger.debug("Verifying token %s", token)
        jwt = _jwt.JWT.from_token(token)
        self._verify_token_signature_and_claims(jwt, buffer_time_days=buffer_time_days)
        return jwt

    def _verify_token_signature_and_claims(
        self, jwt: _jwt.JWT, buffer_time_days: int
    ) -> None:
        """buffer_time_days are extra days of validity that you can add to a token"""
        _verify_signature(jwt, jwks=self.jwks)
        _verify_expiration(jwt, buffer_time_days=buffer_time_days)
        _verify_client(jwt, cognito=self.cognito)
        _verify_issuer(jwt, cognito=self.cognito)
        _verify_token_type(jwt)


def _verify_signature(
    jwt: _jwt.JWT,
    jwks: _jwks.JWKS,
) -> None:
    if not jwks.signature_valid(jwt):
        raise exceptions.InvalidSignatureException("Invalid signature")


def _verify_expiration(jwt: _jwt.JWT, buffer_time_days: int) -> None:
    """buffer_time_days are extra days of validity that you can add to a token"""
    if jwt.has_expired(buffer_time_days=buffer_time_days):
        raise exceptions.TokenExpiredException("Token expired")


def _verify_client(jwt: _jwt.JWT, cognito: _cognito.client.Properties) -> None:
    if not jwt.issued_by_client(cognito.app_client_id):
        raise exceptions.InvalidClientException("Token issued by unauthorized client")


def _verify_issuer(jwt: _jwt.JWT, cognito: _cognito.client.Properties) -> None:
    if not jwt.issued_by_user_pool(cognito.idp_url):
        raise exceptions.InvalidIssuerException("Token issued by unauthorized client")


def _verify_token_type(jwt: _jwt.JWT) -> None:
    if not jwt.is_access_token():
        raise exceptions.IncorrectTokenTypeException(
            f"Token type '{jwt.token_type}' not allowed ('access' required)"
        )
