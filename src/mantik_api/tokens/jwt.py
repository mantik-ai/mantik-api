"""JWT validation.

Inspired by https://github.com/jgontrum/fastapi_jwt_auth_with_aws_cognito

"""
import datetime
import logging
import typing as t
import uuid

import jose.jwt
import jose.utils
import pydantic

import mantik_api.tokens.exceptions as exceptions

logger = logging.getLogger(__name__)


class AWSCognitoJWTHeader(pydantic.BaseModel):
    kid: str
    alg: str

    @classmethod
    def from_unverified_token(cls, token: str) -> t.Self:
        as_dict = jose.jwt.get_unverified_headers(token)
        return cls(**as_dict)


class AWSCognitoJWTClaims(pydantic.BaseModel):
    """
    AWS JWT Claims section,
    see https://docs.aws.amazon.com/cognito/latest/developerguide/amazon-cognito-user-pools-using-the-access-token.html # noqa: E501

    Note: We set all claims that are not used by us to be optional to
    improve interoperability.
    """

    sub: str
    device_key: str | None
    cognito_groups: list[str] | None
    iss: str
    version: int | None
    client_id: str
    origin_jti: str | None
    event_id: str | None
    token_use: str
    scope: str | None
    auth_time: int | None
    exp: int
    iat: int | None
    jti: str | None
    username: str

    @classmethod
    def from_unverified_token(cls, token: str) -> t.Self:
        as_dict = jose.jwt.get_unverified_claims(token)
        return cls(**as_dict)


class JWT(pydantic.BaseModel):
    """Holds the content of a JWT."""

    _jwt_token: str = pydantic.PrivateAttr(None)
    header: AWSCognitoJWTHeader
    claims: AWSCognitoJWTClaims
    signature: str
    message: str

    @classmethod
    def from_token(cls, token: str) -> t.Self:
        """Create from a given token.

        Parameters
        ----------
        token : str
            The token to read.

        Raises
        ------
        VerificationFailedException
            Reading of the token has failed.

        """
        message, signature = _separate_message_and_signature(token)
        try:
            jwt = cls(
                header=AWSCognitoJWTHeader.from_unverified_token(token),
                claims=AWSCognitoJWTClaims.from_unverified_token(token),
                signature=signature,
                message=message,
            )
            jwt.set_jwt_token(token)
            return jwt
        except jose.JWTError:
            logger.error("Failed to construct credentials from JWT", exc_info=True)
            raise exceptions.VerificationFailedException("Invalid token")

    @property
    def kid(self) -> str:
        """Return the kid."""
        return self.header.kid

    @property
    def encoded_message(self) -> bytes:
        """Return the encoded message."""
        return self.message.encode("utf-8")

    @property
    def encoded_signature(self) -> bytes:
        """Return the encoded signature."""
        return jose.utils.base64url_decode(self.signature.encode("utf-8"))

    def has_expired(self, buffer_time_days: int) -> bool:
        """Return whether the token has expired.
        buffer_time_days are extra days of validity that you can add to a token"""
        return datetime.datetime.now() > self.expires_at + datetime.timedelta(
            days=buffer_time_days
        )

    @property
    def token_type(self) -> str:
        """Return the token type."""
        return self.claims.token_use

    @property
    def expires_at(self) -> datetime.datetime:
        """Return the expiration date."""
        return datetime.datetime.fromtimestamp(int(self.claims.exp))

    def issued_by_client(self, cognito_app_client_id: str) -> bool:
        """Return whether token was issued by client with given ID."""
        return self.claims.client_id == cognito_app_client_id

    def issued_by_user_pool(self, cognito_idp_url: str) -> bool:
        """Return whether token was issued by given user pool."""
        return self.claims.iss == cognito_idp_url

    def is_access_token(self) -> bool:
        """Return whether the given token as an access token."""
        return self.token_type == "access"

    @property
    def user_id(self) -> uuid.UUID:
        """Return the token's subject claim. For AWS Cognito this is the User ID."""
        return uuid.UUID(self.claims.sub)

    def set_jwt_token(self, token: str) -> None:
        """Return the token as JWT string."""
        self._jwt_token = token

    def to_string(self) -> str:
        """Return the token as JWT string."""
        return self._jwt_token


def _separate_message_and_signature(token: str) -> tuple[str, str]:
    try:
        message, signature = token.rsplit(".", 1)
    except ValueError:
        logger.debug("Splitting of JWT into message and signature failed")
        raise exceptions.VerificationFailedException("Invalid token")
    return message, signature
