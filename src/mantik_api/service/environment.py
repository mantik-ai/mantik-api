import uuid

import fastapi

from mantik_api import database as database
from mantik_api.database import environment as env_database
from mantik_api.database import exceptions as exceptions
from mantik_api.routes import dependencies as dependencies


def get_environment(
    project_id: uuid.UUID,
    environment_id: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> env_database.Environment:
    environment = db.get_one(
        env_database.Environment,
        constraints={
            env_database.Environment.id: environment_id,
        },
        select_in_loading={
            env_database.Environment.data_repository: {},
        },
    )
    exceptions.check_uuids_consistency(
        project_id, environment.data_repository.project_id
    )
    return environment
