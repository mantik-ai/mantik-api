import uuid

import fastapi
import starlette.status as status

import mantik_api.aws.cognito.exceptions as exceptions
import mantik_api.database as database
import mantik_api.database.client.select as select
import mantik_api.database.exceptions as database_exceptions
import mantik_api.models.invitations as invitations


def get_invited_to_name(
    db: database.client.main.Client,
    invited_to_type: database.invitation.InvitedToType,
    invited_to_id: uuid.UUID,
) -> str:
    match invited_to_type:
        case database.invitation.InvitedToType.PROJECT:
            project = db.get_one(
                database.project.Project,
                constraints={database.project.Project.id: invited_to_id},
            )
            return project.name
        case database.invitation.InvitedToType.GROUP:
            group = db.get_one(
                database.user_group.UserGroup,
                constraints={database.user_group.UserGroup.id: invited_to_id},
            )
            return group.name
        case database.invitation.InvitedToType.ORGANIZATION:
            organization = db.get_one(
                database.organization.Organization,
                constraints={database.organization.Organization.id: invited_to_id},
            )
            return organization.name


def get_invited_name(
    db: database.client.main.Client,
    invited_type: database.invitation.InvitedType,
    invited_id: uuid.UUID,
    invited_user_name: str,
) -> str:
    match invited_type:
        case database.invitation.InvitedType.USER:
            return invited_user_name
        case database.invitation.InvitedType.GROUP:
            group = db.get_one(
                database.user_group.UserGroup,
                constraints={database.user_group.UserGroup.id: invited_id},
            )
            return group.name
        case database.invitation.InvitedType.ORGANIZATION:
            organization = db.get_one(
                database.organization.Organization,
                constraints={database.organization.Organization.id: invited_id},
            )
            return organization.name


def get_inviter_name(db: database.client.main.Client, inviter_id: uuid.UUID) -> str:
    inviter = db.get_one(
        database.user.User,
        constraints={database.user.User.id: inviter_id},
    )
    return inviter.preferred_name


def delete_related_invitations(
    db: database.client.main.Client,
    model: type[select.OrmBase] | select.OrmBase,
    model_id: uuid.UUID,
) -> None:
    match model:
        case database.user.User:
            _delete_invited_ids(
                db=db,
                invited_type=database.invitation.InvitedType.USER,
                invited_id=model_id,
            )

        case database.project.Project:
            _delete_invited_to_ids(
                db=db,
                invited_to_type=database.invitation.InvitedToType.PROJECT,
                invited_to_id=model_id,
            )

        case database.user_group.UserGroup:
            _delete_invited_ids(
                db=db,
                invited_type=database.invitation.InvitedType.GROUP,
                invited_id=model_id,
            )

            _delete_invited_to_ids(
                db=db,
                invited_to_type=database.invitation.InvitedToType.GROUP,
                invited_to_id=model_id,
            )

        case database.organization.Organization:
            _delete_invited_ids(
                db=db,
                invited_type=database.invitation.InvitedType.ORGANIZATION,
                invited_id=model_id,
            )

            _delete_invited_to_ids(
                db=db,
                invited_to_type=database.invitation.InvitedToType.ORGANIZATION,
                invited_to_id=model_id,
            )


def _delete_invited_ids(
    db: database.client.main.Client,
    invited_type: database.invitation.InvitedType,
    invited_id: uuid.UUID,
) -> None:
    result = db.get_all(
        database.invitation.Invitation,
        constraints={
            database.invitation.Invitation.invited_id: invited_id,
            database.invitation.Invitation.invited_type: invited_type,
        },
    )
    for invite in result.entities:
        db.delete(invite)


def _delete_invited_to_ids(
    db: database.client.main.Client,
    invited_to_type: database.invitation.InvitedToType,
    invited_to_id: uuid.UUID,
) -> None:
    result = db.get_all(
        database.invitation.Invitation,
        constraints={
            database.invitation.Invitation.invited_to_id: invited_to_id,
            database.invitation.Invitation.invited_to_type: invited_to_type,
        },
    )
    for invite in result.entities:
        db.delete(invite)


def validate_and_add_invitation(
    add_invitation: invitations.AddInvitation,
    db: database.client.main.Client,
    caller_user_id: uuid.UUID,
    new_invitation_id: uuid.UUID,
):
    already_contained = False

    invited_to_type = add_invitation.invited_to_type
    invited_type = add_invitation.invited_type

    with db.session():
        match invited_to_type:
            case database.invitation.InvitedToType.PROJECT:
                project = db.get_one(
                    database.project.Project,
                    constraints={
                        database.project.Project.id: add_invitation.invited_to_id
                    },
                    select_in_loading={
                        database.project.Project.project_members: {},
                        database.project.Project.project_user_groups: {
                            database.user_group.UserGroup.members: {}
                        },
                        database.project.Project.project_organizations: {
                            database.organization.Organization.members: {},
                            database.organization.Organization.groups: {
                                database.user_group.UserGroup.members: {}
                            },
                        },
                    },
                )
                try:
                    user_role = db.get_one(
                        database.project.UserProjectAssociationTable,
                        constraints={
                            database.project.UserProjectAssociationTable.project_id: project.id,  # noqa
                            database.project.UserProjectAssociationTable.user_id: caller_user_id,  # noqa
                        },
                    )
                    if user_role.role != database.project.ProjectRole.OWNER:
                        raise exceptions.AuthenticationFailedException(
                            "Only owner can invite users to project"
                        )
                except database_exceptions.NotFoundError:
                    if caller_user_id != project.owner_id:
                        raise exceptions.AuthenticationFailedException(
                            "Only owner can invite users to project"
                        )
                if (
                    project.contains_user(add_invitation.invited_id)
                    or project.contains_group(add_invitation.invited_id)
                    or project.contains_organization(add_invitation.invited_id)
                ):
                    already_contained = True

            case database.invitation.InvitedToType.GROUP:
                group = db.get_one(
                    database.user_group.UserGroup,
                    constraints={
                        database.user_group.UserGroup.id: add_invitation.invited_to_id
                    },
                    select_in_loading={database.user_group.UserGroup.members: {}},
                )
                if caller_user_id != group.admin_id:
                    raise exceptions.AuthenticationFailedException(
                        "Only admin can invite users to group"
                    )
                if group.contains_user(add_invitation.invited_id):
                    already_contained = True

            case database.invitation.InvitedToType.ORGANIZATION:
                organization = db.get_one(
                    database.organization.Organization,
                    constraints={
                        database.organization.Organization.id: add_invitation.invited_to_id  # noqa: E501
                    },
                    select_in_loading={
                        database.organization.Organization.members: {},
                        database.organization.Organization.groups: {
                            database.user_group.UserGroup.members: {}
                        },
                    },
                )
                if caller_user_id != organization.contact_id:
                    raise exceptions.AuthenticationFailedException(
                        "Only the organization contact "
                        "can invite users to the organization"
                    )
                if organization.contains_user(
                    add_invitation.invited_id
                ) or organization.contains_group(add_invitation.invited_id):
                    already_contained = True

        match invited_type:
            case database.invitation.InvitedType.USER:
                db.get_one(
                    database.user.User,
                    constraints={database.user.User.id: add_invitation.invited_id},
                )
            case database.invitation.InvitedType.GROUP:
                db.get_one(
                    database.user_group.UserGroup,
                    constraints={
                        database.user_group.UserGroup.id: add_invitation.invited_id  # noqa: E501
                    },
                )
            case database.invitation.InvitedType.ORGANIZATION:
                db.get_one(
                    database.organization.Organization,
                    constraints={
                        database.organization.Organization.id: add_invitation.invited_id  # noqa: E501
                    },
                )

        if already_contained:
            raise fastapi.HTTPException(
                status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                detail=f"Entity already part of the {add_invitation.invited_to_type.name.lower()}",  # noqa: E501
            )

        db.add(
            add_invitation.to_database_model(
                invitation_id=new_invitation_id,
                inviter_id=caller_user_id,
            ),
        )
