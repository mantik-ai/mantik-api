import json
import logging
import uuid

import fastapi
import starlette.status as status

import mantik_api.checks as checks
import mantik_api.database as database
import mantik_api.invoke_compute_backend.submit_run as submit_run
import mantik_api.models as models
import mantik_api.models.run as run
import mantik_api.routes.mlflow.tracking_server_router as tracking_server_router
import mantik_api.routes.rbac.bearer as bearer
import mantik_api.tokens as tokens
import mantik_api.tokens.jwt as jwt
import mantik_api.utils.env as env
import mantik_api.utils.mlflow as mlflow
import mantik_api.utils.mlflow.runs as mlflow_runs
import mantik_api.utils.vault as vault

logger = logging.getLogger(__name__)
BACKEND_CONFIG_FILENAME = "compute-backend-config.json"


def environment_variables_for_mantik_client_data_download(
    run: models.run.AddRun,
) -> dict:
    env_vars = {
        "MANTIK_DATA_REPOSITORY_ID": str(run.data_repository_id),
    }

    if run.data_commit:
        env_vars = env_vars | {"MANTIK_DATA_REPOSITORY_COMMIT": run.data_commit}

    if run.data_branch:
        env_vars = env_vars | {"MANTIK_DATA_REPOSITORY_BRANCH": run.data_branch}

    if run.data_target_dir:
        env_vars = env_vars | {"MANTIK_TARGET_DIR": run.data_target_dir}

    return env_vars


def trigger_run(
    db: database.client.main.Client,
    add_run: models.run.AddRun,
    token: bearer.Token,
    project_id: uuid.UUID,
    caller_user_id: uuid.UUID,
) -> database.run.Run:
    checks.user_owns_connection(
        connection_id=add_run.connection_id, user_id=caller_user_id, db=db
    )

    connection = db.get_one(
        orm_model_type=database.connection.Connection,
        constraints={database.connection.Connection.id: add_run.connection_id},
    )

    experiment_repository = db.get_one(
        orm_model_type=database.experiment_repository.ExperimentRepository,
        constraints={
            database.experiment_repository.ExperimentRepository.id: add_run.experiment_repository_id  # noqa: E501
        },
    )

    code_repository = db.get_one(
        orm_model_type=database.code_repository.CodeRepository,
        constraints={
            database.code_repository.CodeRepository.id: add_run.code_repository_id
        },
    )

    code_connection = code_repository.my_git_connection_id(user_id=caller_user_id)
    code_connection_git_access_token = (
        vault.get_credential(connection_id=code_connection, token=token).token
        if code_connection
        else None
    )

    data_repository = None
    if "data_repository_id" in add_run:
        data_repository = db.get_one(
            orm_model_type=database.data_repository.DataRepository,
            constraints={
                database.data_repository.DataRepository.id: add_run.data_repository_id  # noqa: E501
            },
        )

    if add_run.environment_id is not None:
        environment = db.get_one(
            orm_model_type=database.environment.Environment,
            constraints={
                database.environment.Environment.id: add_run.environment_id  # noqa
            },
        )

        add_run.backend_config = database.run.inject_environment_into_backend_config(
            environment=environment, backend_config=add_run.backend_config
        )

    add_run.name = mlflow.runs.create_unique_name(
        experiment_id=experiment_repository.mlflow_experiment_id,
        name=add_run.name,
        token=token,
    )

    logger.info("Creating MLflow run for name %s", add_run.name)

    add_run.mlflow_run_id = mlflow.runs.create(
        run_name=add_run.name,
        mlflow_experiment_id=experiment_repository.mlflow_experiment_id,
        token=token,
    )

    db_run = add_run_details_to_database(
        db=db,
        add_run=add_run,
        project_id=project_id,
        caller_user_id=caller_user_id,
        mlflow_run_id=add_run.mlflow_run_id,
        remote_system_job_id=None,
    )
    db.commit()

    environment_variables = {
        "MANTIK_PROJECT_ID": str(project_id),
    }

    if add_run.data_repository_id:
        environment_variables |= environment_variables_for_mantik_client_data_download(
            run=add_run
        )

    match db_run.backend_type:
        case database.run.SupportedBackends.SSH:
            compute_backend_url = submit_run._create_compute_backend_url_for_ssh(
                mlflow_experiment_id=experiment_repository.mlflow_experiment_id
            )
            request_compute_backend_data = _create_request_data_for_ssh(
                token=token,
                backend_config_filename=BACKEND_CONFIG_FILENAME,
                run=add_run,
                connection=connection,
                environment_variables=environment_variables,
            )
        case (
            database.run.SupportedBackends.UNICORE
            | database.run.SupportedBackends.FIRECREST
        ):
            compute_backend_url = (
                submit_run._create_compute_backend_url_for_unicore_or_firecrest(
                    mlflow_experiment_id=experiment_repository.mlflow_experiment_id
                )
            )

            request_compute_backend_data = (
                _create_request_data_for_unicore_or_firecrest(
                    token=token,
                    backend_config_filename=BACKEND_CONFIG_FILENAME,
                    run=add_run,
                    connection=connection,
                    environment_variables=environment_variables,
                )
            )
            if add_run.connection_id is None or add_run.compute_budget_account is None:
                raise fastapi.HTTPException(
                    status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                    detail="Submitting a run requires a connection and a "
                    "compute budget account",
                )

    logger.info(
        "Submitting run %s for project %s to Compute Backend", add_run, project_id
    )

    details = submit_run.submit_run(
        token=token,
        run=add_run,
        data_repository=data_repository,
        code_repository=code_repository,
        code_connection_git_access_token=code_connection_git_access_token,
        compute_backend_url=compute_backend_url,
        backend_config_filename=BACKEND_CONFIG_FILENAME,
        request_compute_backend_data=request_compute_backend_data,
    )
    logger.info("Run was submitted successfully to Compute Backend: %s", details)

    logger.info(
        "Updating Run object with remote system job id: %s",
        details.remote_system_job_id,
    )
    db.overwrite(db_run, {"remote_system_job_id": details.remote_system_job_id})

    return db_run


def add_run_details_to_database(
    db: database.client.main.Client,
    add_run: models.run.AddRun,
    project_id: uuid.UUID,
    caller_user_id: uuid.UUID,
    mlflow_run_id: uuid.UUID,
    remote_system_job_id: str | None,
) -> database.run.Run:
    logger.debug(
        "Creating Model Repository for project %s and run %s ",
        project_id,
        add_run,
    )
    if add_run.code_repository_id:
        new_model_repository = models.model_repository.AddModelRepository(
            code_repository_id=add_run.code_repository_id,
            branch=add_run.branch,
            commit=add_run.commit,
        )
        new_model_repository_id = uuid.uuid4()
        db.add(
            new_model_repository.to_database_model(
                project_id=project_id, model_repository_id=new_model_repository_id
            )
        )
        logger.debug("Saving run for project %s to database: %s", project_id, add_run)

        add_run_db = add_run.to_database_model(
            mantik_run_id=mlflow_run_id,
            mlflow_run_id=mlflow_run_id,
            project_id=project_id,
            user_id=caller_user_id,
            model_repository_id=new_model_repository_id,
            remote_system_job_id=remote_system_job_id,
        )

    else:
        logger.debug("Saving run for project %s to database: %s", project_id, add_run)
        add_run_db = add_run.to_database_model(
            mantik_run_id=mlflow_run_id,
            mlflow_run_id=mlflow_run_id,
            project_id=project_id,
            model_repository_id=None,
            user_id=caller_user_id,
            remote_system_job_id=remote_system_job_id,
        )

    db.add(add_run_db)

    return add_run_db


def fix_endless_running_jobs(
    run: database.run.Run, token: tokens.jwt.JWT, client: database.client.main.Client
):
    if run.status != database.run.RunStatus.RUNNING:
        return run

    mlflow_run_id = run.mlflow_run_id
    mlflow_response = mlflow_runs.get_run(mlflow_run_id, token=token)
    if mlflow_response["run"]["info"]["status"] in ["FINISHED", "FAILED", "KILLED"]:
        client.overwrite(
            run,
            fields_to_overwrite=run.to_overwrite_kwargs(
                status=database.run.RunStatus.FAILED
            ),
        )
        run.status = database.run.RunStatus.FAILED
    return run


def _create_request_data_for_unicore_or_firecrest(
    token: jwt.JWT,
    backend_config_filename: str,
    run: run.AddRun,
    connection: database.connection.Connection,
    environment_variables: dict,
) -> dict[str, str]:
    credentials = vault.get_credential(connection_id=connection.id, token=token)
    return {
        "active_run_id": str(run.mlflow_run_id.hex),
        "entry_point": run.entry_point,
        "mlflow_parameters": json.dumps(run.mlflow_parameters),
        submit_run.COMPUTE_BACKEND_API_USER_PARAMETER: credentials.login_name,
        submit_run.COMPUTE_BACKEND_API_PASSWORD_PARAMETER: credentials.password,
        "compute_budget_account": run.compute_budget_account,
        "compute_backend_config": backend_config_filename,
        "mlflow_tracking_uri": env.get_required_env_var(
            tracking_server_router.TRACKING_SERVER_URL_PUBLIC_ENV_VAR
        ),
        "mlflow_tracking_token": token.to_string(),
        "environment_variables": json.dumps(environment_variables),
    }


def _create_request_data_for_ssh(
    token: jwt.JWT,
    backend_config_filename: str,
    run: run.AddRun,
    connection: database.connection.Connection,
    environment_variables: dict,
) -> dict[str, str]:
    credentials = vault.get_credential(connection_id=connection.id, token=token)
    return {
        "active_run_id": str(run.mlflow_run_id.hex),
        "entry_point": run.entry_point,
        "mlflow_parameters": json.dumps(run.mlflow_parameters),
        submit_run.COMPUTE_BACKEND_API_PRIVATE_KEY_PARAMETER: credentials.private_key,
        submit_run.COMPUTE_BACKEND_API_SSH_PASSWORD_PARAMETER: credentials.password,
        submit_run.COMPUTE_BACKEND_API_SSH_USERNAME_PARAMETER: credentials.login_name,
        "compute_backend_config": backend_config_filename,
        "mlflow_tracking_uri": env.get_required_env_var(
            tracking_server_router.TRACKING_SERVER_URL_PUBLIC_ENV_VAR
        ),
        "mlflow_tracking_token": token.to_string(),
        "environment_variables": json.dumps(environment_variables),
    }
