import uuid

import boto3


def containerize_trained_model(
    model_id: uuid.UUID,
    project_id: uuid.UUID,
    token: str,
    trained_model_uri: str,
    code_build_project_name: str,
):
    code_build_client = boto3.client("codebuild")
    code_build_client.start_build(
        projectName=code_build_project_name,
        environmentVariablesOverride=[
            {"name": "MODEL_URI", "value": trained_model_uri, "type": "PLAINTEXT"},
            {
                "name": "IMAGE_NAME",
                "value": f"{str(model_id)}-docker",
                "type": "PLAINTEXT",
            },
            {
                "name": "MLFLOW_TRACKING_TOKEN",
                "value": token,
                "type": "PLAINTEXT",
            },
            {"name": "MODEL_ID", "value": str(model_id), "type": "PLAINTEXT"},
            {"name": "PROJECT_ID", "value": str(project_id), "type": "PLAINTEXT"},
        ],
    )
