import typing
import urllib.parse
import uuid

import boto3
import botocore.exceptions
import fastapi

import mantik_api.database as database
import mantik_api.database.exceptions as database_exceptions


def is_s3_url(url: str) -> bool:
    return urllib.parse.urlparse(url).scheme == "s3"


def extract_bucket_name_from_s3_url(s3_url: str) -> str:
    return urllib.parse.urlparse(s3_url).netloc


def extract_file_key_from_s3_url(s3_url: str) -> str:
    return urllib.parse.urlparse(s3_url).path[1::]  # skip first slash


def file_exists_in_s3(file_key: str, bucket_name: str, s3_client) -> bool:
    return file_key in list_keys_in_bucket(s3_client, bucket_name=bucket_name)


def create_s3_key_for_file(user_id: uuid.UUID, filename: str) -> str:
    return f"{str(user_id)}/{filename}"


def handle_s3_exceptions_decorator(f: typing.Callable) -> typing.Callable:
    def wrapper(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except botocore.exceptions.ClientError as e:
            if e.response["Error"]["Code"] == "AccessDenied":
                raise fastapi.HTTPException(403, "Unauthorized to access bucket")
            elif e.response["Error"]["Code"] == "NoSuchBucket":
                raise fastapi.HTTPException(404, "No bucket of given name")
            elif e.response["Error"]["Code"] == "NoSuchKey":
                raise fastapi.HTTPException(404, "Resource not found")

            raise fastapi.HTTPException(400, str(e))

    return wrapper


@handle_s3_exceptions_decorator
def upload_file_to_s3(
    file: fastapi.UploadFile, user_id: uuid.UUID, bucket_name: str
) -> str:
    s3 = boto3.client("s3")
    file_key = create_s3_key_for_file(user_id=user_id, filename=file.filename)

    if file_exists_in_s3(file_key=file_key, bucket_name=bucket_name, s3_client=s3):
        raise fastapi.HTTPException(400, "Filename already exists. Delete it first.")

    s3.upload_fileobj(file.file, bucket_name, file_key)
    return f"s3://{bucket_name}/{file_key}"


def upload_image_to_url(
    file: fastapi.UploadFile,
    base_url: str,
    current_user_id: uuid.UUID,
    db_client: database.client.main.Client,
) -> str:
    if not is_s3_url(base_url):
        raise fastapi.HTTPException(403, f"Not authorized to upload to {base_url}")

    new_file_url = upload_file_to_s3(
        file=file,
        user_id=current_user_id,
        bucket_name=extract_bucket_name_from_s3_url(base_url),
    )
    append_new_vm_url_for_user(
        db_client=db_client, user_id=current_user_id, url=new_file_url
    )

    return new_file_url


def append_new_vm_url_for_user(
    db_client: database.client.main.Client, user_id: uuid.UUID, url: str
) -> None:
    user = db_client.get_one(
        orm_model_type=database.user.User,
        constraints={database.user.User.id: user_id},
    )
    old_urls = user.vm_urls or []
    new_urls = old_urls + [url]
    db_client.overwrite(user, fields_to_overwrite={"vm_urls": new_urls})


def delete_vm_url_for_user(
    db_client: database.client.main.Client, user_id: uuid.UUID, url: str
) -> None:
    current_user = db_client.get_one(
        orm_model_type=database.user.User,
        constraints={database.user.User.id: user_id},
    )

    urls_of_current_user = current_user.vm_urls or []

    try:
        urls_of_current_user.remove(url)
    except ValueError:
        raise database_exceptions.NotFoundError

    db_client.update(
        current_user,
        constraints={database.user.User.id: current_user.id},
        fields_to_update={database.user.User.vm_urls: urls_of_current_user},
    )


def extract_user_id_from_s3_url(s3_url: str) -> str:
    return urllib.parse.urlparse(s3_url).path.split("/")[1]


@handle_s3_exceptions_decorator
def delete_s3_resource(s3_url: str) -> dict:
    bucket_name = extract_bucket_name_from_s3_url(s3_url)
    file_key = extract_file_key_from_s3_url(s3_url)

    s3 = boto3.client("s3")
    if not file_exists_in_s3(file_key=file_key, bucket_name=bucket_name, s3_client=s3):
        raise fastapi.HTTPException(404, "Image not found therefore not deleted")

    return s3.delete_object(Bucket=bucket_name, Key=file_key)


def list_keys_in_bucket(s3_client, bucket_name: str) -> list[str]:
    s3_objects_response = s3_client.list_objects_v2(Bucket=bucket_name)
    return [s3_object["Key"] for s3_object in s3_objects_response.get("Contents", [])]


@handle_s3_exceptions_decorator
def get_s3_object(s3_url: str) -> dict:
    file_key = extract_file_key_from_s3_url(s3_url)
    bucket_name = extract_bucket_name_from_s3_url(s3_url)

    s3 = boto3.client("s3")
    return s3.get_object(Bucket=bucket_name, Key=file_key)
