import datetime
import typing as t
import uuid

import mantik_api.database as database
import mantik_api.models.base as base
import mantik_api.models.data_repository as data_repository


class Environment(base.BaseModelWithUserRole):
    environment_id: uuid.UUID
    name: str
    type: str
    data_repository: data_repository.DataRepository
    variables: dict[str, str] | None
    created_at: datetime.datetime
    updated_at: datetime.datetime | None

    @classmethod
    def from_database_model(
        cls,
        environment: database.environment.Environment,
        user_role: database.role_details.ProjectRole,
        user_id: uuid.UUID | None,
    ) -> t.Self:
        return cls(
            environment_id=environment.id,
            name=environment.name,
            type=environment.type,
            data_repository=data_repository.DataRepository.from_database_model(
                environment.data_repository, user_role=user_role, user_id=user_id
            ),
            variables=environment.variables,
            user_role=user_role,
            created_at=environment.created_at,
            updated_at=environment.updated_at,
        )


class AddEnvironment(base.BaseModel):
    name: str
    type: str
    data_repository_id: uuid.UUID
    variables: dict[str, str] | None

    def to_database_model(
        self, environment_id: uuid.UUID
    ) -> database.environment.Environment:
        return database.environment.Environment(
            id=environment_id,
            **self.dict(
                convert_keys_to_camel_case=False,
            )
        )


class ProjectsProjectIdEnvironmentsGet200Response(base.PaginationBaseModel):
    environments: list[Environment] | None


class ProjectsProjectIdEnvironmentsPost201Response(base.BaseModel):
    environment_id: uuid.UUID


ProjectsProjectIdEnvironmentsPost201Response.update_forward_refs()
ProjectsProjectIdEnvironmentsGet200Response.update_forward_refs()
Environment.update_forward_refs()
AddEnvironment.update_forward_refs()
