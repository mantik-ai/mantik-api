import mantik_api.models.base as base


class InitiateForgotPasswordUserDetails(base.UsernameAndEmailBaseModel):
    pass


class InitiateForgotPasswordUserDetailsPost200Response(base.BaseModel):
    user_name: str


class ConfirmForgotPasswordUserDetails(base.BaseModel):
    user_name: str
    confirmation_code: str
    new_password: str


InitiateForgotPasswordUserDetails.update_forward_refs()
InitiateForgotPasswordUserDetailsPost200Response.update_forward_refs()
ConfirmForgotPasswordUserDetails.update_forward_refs()
