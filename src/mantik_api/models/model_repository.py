import datetime
import typing as t
import uuid

import mantik_api.database as database
import mantik_api.models.base as base
import mantik_api.models.code_repository as api_code_repository
import mantik_api.models.label as api_label


class ModelRepository(base.BaseModelWithUserRole):
    model_repository_id: uuid.UUID
    uri: str | None
    description: str | None
    code_repository: api_code_repository.CodeRepository | None
    branch: str | None
    commit: str | None
    labels: list[api_label.Label] | None
    created_at: datetime.datetime
    updated_at: datetime.datetime | None

    @classmethod
    def from_database_model(
        cls,
        model_repository: database.model_repository.ModelRepository,
        user_role: database.role_details.ProjectRole,
    ) -> t.Self:
        return cls(
            model_repository_id=model_repository.id,
            uri=model_repository.uri,
            description=model_repository.description,
            code_repository=api_code_repository.CodeRepository.from_database_model(
                model_repository.code_repository,
                user_role=user_role,
                user_id=None,
            )
            if api_code_repository.CodeRepository is not None
            else None,
            branch=model_repository.branch,
            commit=model_repository.commit,
            labels=base.from_database_models(api_label.Label, model_repository.labels),
            user_role=user_role,
            created_at=model_repository.created_at,
            updated_at=model_repository.updated_at,
        )


class AddModelRepository(base.BaseModelWithBranchOrCommit):
    uri: str | None
    description: str | None
    code_repository_id: uuid.UUID | None

    def to_database_model(
        self,
        project_id: uuid.UUID,
        model_repository_id: uuid.UUID,
        exclude_unset: bool | None = False,
    ) -> database.model_repository.ModelRepository:
        return database.model_repository.ModelRepository(
            id=model_repository_id,
            project_id=project_id,
            **self.dict(exclude_unset=exclude_unset, convert_keys_to_camel_case=False)
        )


class ProjectsProjectIdModelsGet200Response(base.PaginationBaseModelWithUserRole):
    model_repositories: list[ModelRepository] | None


class ProjectsProjectIdModelsGet201Response(base.BaseModel):
    model_repository_id: uuid.UUID


ModelRepository.update_forward_refs()
AddModelRepository.update_forward_refs()
ProjectsProjectIdModelsGet200Response.update_forward_refs()
ProjectsProjectIdModelsGet201Response.update_forward_refs()
