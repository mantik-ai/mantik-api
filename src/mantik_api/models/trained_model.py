import datetime
import typing as t
import uuid

import pydantic

import mantik_api.database as database
import mantik_api.models.base as base


class TrainedModelBase(base.BaseModel):
    name: str = pydantic.Field(
        description="Name of the model. Unique within the project."
    )
    uri: str | None = pydantic.Field(
        description="Path to the model artifact. Required if runId not given.",
        example="mlflow-artifacts:/130/e439b9799e20403aba4e440bcf9f5c4e/artifacts/model",  # noqa
    )
    location: str | None = pydantic.Field(
        description="The location of the model. Required if runId not given.",
        example="JUWELS",
    )
    connection_id: uuid.UUID | None
    mlflow_parameters: dict | None
    run_id: uuid.UUID | None = pydantic.Field(
        description="Id of Run which produced this model. Required if uri and location are not given."  # noqa
    )
    status: database.trained_model.ContainerBuildStatus | None = pydantic.Field(
        description="Model container building status.",
        example=database.trained_model.ContainerBuildStatus.SUCCESSFUL.value,
    )


class UpdateTrainedModel(TrainedModelBase):
    def to_database_model(
        self,
        project_id: uuid.UUID,
        model_id: uuid.UUID,
        exclude_unset: bool | None = False,
    ) -> database.trained_model.TrainedModel:
        return database.trained_model.TrainedModel(
            id=model_id,
            project_id=project_id,
            **self.dict(
                exclude_unset=exclude_unset,
                convert_keys_to_camel_case=False,
            )
        )


class AddTrainedModel(UpdateTrainedModel):
    @pydantic.root_validator()
    def validate_run_id_or_uri_plus_location(
        cls, values: dict[str, str | None]
    ) -> dict[str, str | None]:
        if not values.get("run_id") and (
            not values.get("uri") or not values.get("location")
        ):
            raise ValueError("Specify either runId or uri + location")

        return values


class TrainedModel(TrainedModelBase, base.BaseModelWithUserRole):
    model_id: uuid.UUID
    run_name: str | None = pydantic.Field(
        None,
        description="Name of the run from which this model has been registered.",
        example="Run number 42",
    )
    created_at: datetime.datetime
    updated_at: datetime.datetime | None

    @classmethod
    def from_database_model(
        cls,
        trained_model: database.trained_model.TrainedModel,
        user_role: database.role_details.ProjectRole,
    ) -> t.Self:
        return cls(
            name=trained_model.name,
            model_id=trained_model.id,
            run_id=trained_model.run_id,
            run_name=trained_model.run.name if trained_model.run is not None else None,
            uri=trained_model.uri,
            connection_id=trained_model.connection_id,
            location=trained_model.location,
            mlflow_parameters=trained_model.mlflow_parameters,
            status=trained_model.status,
            user_role=user_role,
            created_at=trained_model.created_at,
            updated_at=trained_model.updated_at,
        )


class GetTrainedModelsResponse(base.PaginationBaseModelWithUserRole):
    models: list[TrainedModel]


class CreatedTrainedModelResponse(base.BaseModel):
    model_id: uuid.UUID


TrainedModel.update_forward_refs()
AddTrainedModel.update_forward_refs()
GetTrainedModelsResponse.update_forward_refs()
CreatedTrainedModelResponse.update_forward_refs()
