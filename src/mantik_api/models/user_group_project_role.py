import datetime
import typing as t
import uuid

import mantik_api.database as database
import mantik_api.models.base as base
import mantik_api.models.user_group as _user_group


class UserGroupProjectRole(base.BaseModel):
    user_group: _user_group.UserGroup
    project_id: uuid.UUID
    role: database.project.ProjectRole
    created_at: datetime.datetime
    updated_at: datetime.datetime | None

    @classmethod
    def from_database_model(
        cls,
        user_group_project_role: database.project.UserGroupProjectAssociationTable,
        client: database.client.main.Client,
    ) -> t.Self:
        user_group = client.get_one(
            database.user_group.UserGroup,
            constraints={
                database.user_group.UserGroup.id: user_group_project_role.user_group_id
            },
        )
        return cls(
            project_id=user_group_project_role.project_id,
            user_group=_user_group.UserGroup.from_database_model(user_group),
            role=user_group_project_role.role,
            created_at=user_group_project_role.created_at,
            updated_at=user_group_project_role.updated_at,
        )


class ProjectsProjectIdGroupsGet200Response(base.PaginationBaseModelWithUserRole):
    groups: list[UserGroupProjectRole] | None


UserGroupProjectRole.update_forward_refs()
ProjectsProjectIdGroupsGet200Response.update_forward_refs()
