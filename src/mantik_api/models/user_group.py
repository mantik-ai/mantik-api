import datetime
import typing as t
import uuid

import mantik_api.database.client as _client
import mantik_api.database.exceptions as _exceptions
import mantik_api.database.user as _user
import mantik_api.database.user_group as _user_group
import mantik_api.models.base as base
import mantik_api.models.user as user


class UserGroup(base.BaseModel):
    user_group_id: uuid.UUID
    name: str
    admin: user.User
    members: list[user.User] | None
    created_at: datetime.datetime
    updated_at: datetime.datetime | None

    @classmethod
    def from_database_model(cls, user_group: _user_group.UserGroup) -> t.Self:
        return cls(
            user_group_id=str(user_group.id),
            name=user_group.name,
            admin=user.User.from_database_model(user_group.admin),
            members=base.from_database_models(user.User, user_group.members),
            created_at=user_group.created_at,
            updated_at=user_group.updated_at,
        )


class AddUserGroup(base.BaseModel):
    name: str

    def to_database_model(
        self,
        user_group_id: uuid.UUID,
        admin: _user.User,
    ) -> _user_group.UserGroup:
        return _user_group.UserGroup(
            id=user_group_id, name=self.name, admin=admin, members=[admin]
        )


class UpdateUserGroup(base.BaseModel):
    name: str
    admin_id: uuid.UUID
    member_ids: list[uuid.UUID]

    def to_overwrite_kwargs(self, client: _client.main.Client) -> dict[str, t.Any]:
        try:
            admin = _user.get_user_by_id(client, self.admin_id)
        except _exceptions.NotFoundError:
            raise _exceptions.DatabaseExecutionError(
                f"Unable to change admin: user with ID {self.admin_id} not found"
            )

        members = [
            _user.get_user_by_id(client, member_id) for member_id in self.member_ids
        ]

        return {
            **self.dict(convert_keys_to_camel_case=False, exclude_unset=True),
            "admin": admin,
            "members": members,
        }


class GroupsGet200Response(base.PaginationBaseModel):
    user_groups: list[UserGroup] | None


class GroupsGet201Response(base.BaseModel):
    group_id: uuid.UUID


UserGroup.update_forward_refs()
AddUserGroup.update_forward_refs()
UpdateUserGroup.update_forward_refs()
GroupsGet200Response.update_forward_refs()
GroupsGet201Response.update_forward_refs()
