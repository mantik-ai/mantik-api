import typing as t

import mantik_api.models.base as base
from mantik_api.models.connection import Connection


class DeploymentInformation(base.BaseModel):
    deployment_id: str
    saved_model_repository_id: str
    connection: Connection | None
    resources: dict[str, t.Any] | None
    cron_string: str | None


class ProjectsProjectIdDeploymentsGet200Response(base.PaginationBaseModel):
    deployments: list[DeploymentInformation] | None


class ProjectsProjectIdDeploymentsGet201Response(base.BaseModel):
    """NOTE: This class is auto generated
    by OpenAPI Generator (https://openapi-generator.tech).

        Do not edit the class manually.

        ProjectsProjectIdDeploymentsGet201Response - a model defined in OpenAPI

            deplyoment_id: The deplyoment_id
            of this ProjectsProjectIdDeploymentsGet201Response [Optional].
    """

    deplyoment_id: str | None


DeploymentInformation.update_forward_refs()
ProjectsProjectIdDeploymentsGet200Response.update_forward_refs()

ProjectsProjectIdDeploymentsGet201Response.update_forward_refs()
