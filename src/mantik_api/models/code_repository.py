import datetime
import typing as t
import uuid

import pydantic

import mantik_api.database as database
import mantik_api.models.base as base
import mantik_api.models.label as label


class CodeRepository(base.BaseModelWithUserRole):
    code_repository_id: uuid.UUID
    code_repository_name: str | None
    uri: str
    connection_id: uuid.UUID | None = pydantic.Field(
        None, description="Connection id for private repositories."
    )
    description: str | None = None
    labels: list[label.Label] | None = None
    created_at: datetime.datetime
    updated_at: datetime.datetime | None
    platform: database.git_repository.Platform

    @classmethod
    def from_database_model(
        cls,
        code_repository: database.code_repository.CodeRepository,
        user_role: database.role_details.ProjectRole,
        user_id: uuid.UUID | None,
    ) -> t.Self:
        """Construct from database model.

        Notes
        -----
        Currently, the user permissions for a code repository are
        only defined by the user role in the project.

        """
        return cls(
            code_repository_id=str(code_repository.id),
            code_repository_name=code_repository.code_repository_name,
            uri=code_repository.uri,
            description=code_repository.description,
            labels=base.from_database_models(label.Label, code_repository.labels),
            user_role=user_role,
            created_at=code_repository.created_at,
            updated_at=code_repository.updated_at,
            platform=code_repository.platform,
            connection_id=code_repository.my_git_connection_id(user_id=user_id)
            if user_id is not None
            else None,
        )


class UpdateCodeRepository(base.BaseModel):
    code_repository_name: str | None
    uri: str
    description: str | None
    labels: list[uuid.UUID] | None = pydantic.Field(default_factory=list)
    platform: database.git_repository.Platform = pydantic.Field(
        ...,
        description="The platform of the code repository",
    )

    def to_database_model(
        self,
        project_id: uuid.UUID,
        code_repository_id: uuid.UUID,
        client: database.client.main.Client,
        exclude_unset: bool | None = False,
    ) -> database.code_repository.CodeRepository:
        return database.code_repository.CodeRepository(
            id=code_repository_id,
            project_id=project_id,
            labels=label.get_database_models(self.labels, client=client),
            **self.dict(
                exclude_unset=exclude_unset,
                convert_keys_to_camel_case=False,
                exclude={"labels"},
            ),
        )

    def to_overwrite_kwargs(
        self,
        client: database.client.main.Client,
        exclude_unset: bool | None = False,
    ) -> dict[str, t.Any]:
        return {
            "labels": label.get_database_models(self.labels, client=client),
            **self.dict(
                exclude_unset=exclude_unset,
                convert_keys_to_camel_case=False,
                exclude={"labels"},
            ),
        }


class AddCodeRepository(UpdateCodeRepository):
    connection_id: uuid.UUID | None = pydantic.Field(
        None, description="Connection id for private repositories."
    )

    def to_database_model(
        self,
        project_id: uuid.UUID,
        code_repository_id: uuid.UUID,
        client: database.client.main.Client,
        exclude_unset: bool | None = False,
    ) -> database.code_repository.CodeRepository:
        return database.code_repository.CodeRepository(
            id=code_repository_id,
            project_id=project_id,
            labels=label.get_database_models(self.labels, client=client),
            **self.dict(
                exclude_unset=exclude_unset,
                convert_keys_to_camel_case=False,
                exclude={"labels", "connection_id"},
            ),
        )


class ProjectsProjectIdCodeGet200Response(base.PaginationBaseModelWithUserRole):
    code_repositories: list[CodeRepository] | None


class ProjectsProjectIdCodeGet201Response(base.BaseModel):
    code_repository_id: uuid.UUID


CodeRepository.update_forward_refs()
AddCodeRepository.update_forward_refs()
ProjectsProjectIdCodeGet200Response.update_forward_refs()
ProjectsProjectIdCodeGet201Response.update_forward_refs()
