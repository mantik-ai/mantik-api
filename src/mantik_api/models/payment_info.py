import typing as t
import uuid

import mantik_api.database.user as _user
import mantik_api.models.base as base


class PaymentInfo(base.BaseModel):
    address: str | None
    payment_method: str | None
    payment_details: str | None

    @classmethod
    def from_database_model(cls, user: _user.User) -> t.Self:
        return cls(
            address=user.address,
            payment_method=user.payment_method,
            payment_details=user.payment_details,
        )


class AddPaymentInfo(base.BaseModel):
    address: str
    payment_method: str
    payment_details: str

    def to_database_model(
        self, user_id: uuid.UUID, exclude_unset: bool | None = False
    ) -> _user.User:
        return _user.User(
            id=user_id,
            **self.dict(convert_keys_to_camel_case=False, exclude_unset=exclude_unset)
        )


PaymentInfo.update_forward_refs()
AddPaymentInfo.update_forward_refs()
