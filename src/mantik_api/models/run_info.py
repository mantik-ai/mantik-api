import pathlib
import typing as t

import pydantic

import mantik_api.database as database
import mantik_api.models.base as base
import mantik_api.utils as utils


class Local(base.BaseModel):
    """Job details returned for local run."""

    infrastructure: str = "Local"
    logs: list[str]

    @classmethod
    def from_run(cls, run: database.run.Run):
        return cls(
            logs=run.get_logs()
            or (
                ["Run is still active, wait for the run to end to see the logs here"]
                if run.is_active()
                else ["Missing logs from local run, logs uploaded unsuccessfully"]
            )
        )


class UnicoreStatus(utils.enums.CaseInsensitiveStrEnum):
    """Job statuses returned by the UNICORE API."""

    STAGING_IN = "STAGINGIN"
    READY = "READY"
    QUEUED = "QUEUED"
    RUNNING = "RUNNING"
    STAGING_OUT = "STAGINGOUT"
    SUCCESSFUL = "SUCCESSFUL"
    FAILED = "FAILED"
    UNKNOWN = "UNDEFINED"

    def to_database_run_status(self) -> database.run.RunStatus | None:
        match self:
            case UnicoreStatus.STAGING_IN:
                return database.run.RunStatus.RUNNING
            case UnicoreStatus.READY:
                return database.run.RunStatus.RUNNING
            case UnicoreStatus.QUEUED:
                return database.run.RunStatus.SCHEDULED
            case UnicoreStatus.RUNNING:
                return database.run.RunStatus.RUNNING
            case UnicoreStatus.STAGING_OUT:
                return database.run.RunStatus.RUNNING
            case UnicoreStatus.SUCCESSFUL:
                return database.run.RunStatus.FINISHED
            case UnicoreStatus.FAILED:
                return database.run.RunStatus.FAILED
            case UnicoreStatus.UNKNOWN:
                return None
            case _:
                raise NotImplementedError(
                    f"Run status {self} not mapped to any database run status"
                )


class ConsumedTime(base.BaseModel):
    """Consumed time of a job."""

    total: str | None
    queued: str | None
    stage_in: str | None
    pre_command: str | None
    main: str | None
    post_command: str | None
    stage_out: str | None

    @classmethod
    def from_dict(cls, data: dict[str, str]) -> t.Self:
        return cls(
            total=data["total"],
            queued=data["queued"],
            stage_in=data["stage-in"],
            pre_command=data["preCommand"],
            main=data["main"],
            post_command=data["postCommand"],
            stage_out=data["stage-out"],
        )

    def to_dict(self):
        return {
            "total": self.total,
            "queued": self.queued,
            "stage-in": self.stage_in,
            "preCommand": self.pre_command,
            "main": self.main,
            "postCommand": self.post_command,
            "stage-out": self.stage_out,
        }


class Unicore(base.BaseModel):
    """UNICORE Job properties."""

    id: str
    status: UnicoreStatus
    type: str
    logs: list[str]
    owner: str
    site_name: str
    consumed_time: ConsumedTime
    current_time: str
    submission_time: str
    termination_time: str
    status_message: str
    tags: list[str]
    resource_status: str
    name: str
    queue: str
    submission_preferences: dict
    resource_status_message: str
    acl: list[str]
    batch_system_id: str
    bss_details: dict | None
    exit_code: str | None

    @classmethod
    def from_dict(
        cls,
        id_: str,
        data: dict[str, str | dict],
        bss_details: dict[str, str] | None = None,
    ) -> t.Self:
        if bss_details is None and "bssDetails" in data:
            bss_details = data["bssDetails"]
        return cls(
            id=id_,
            status=data["status"],
            type=data["jobType"],
            logs=data["log"],
            owner=data["owner"],
            site_name=data["siteName"],
            consumed_time=ConsumedTime.from_dict(data["consumedTime"]),
            current_time=data["currentTime"],
            submission_time=data["submissionTime"],
            termination_time=data["terminationTime"],
            status_message=data["statusMessage"],
            tags=data["tags"],
            resource_status=data["resourceStatus"],
            name=data["name"],
            queue=data["queue"],
            submission_preferences=data["submissionPreferences"],
            resource_status_message=data["resourceStatusMessage"],
            acl=data["acl"],
            batch_system_id=data["batchSystemID"],
            exit_code=data.get("exitCode"),
            bss_details=bss_details,
        )

    def to_json(self):
        return {
            "status": self.status.value,
            "jobType": self.type,
            "log": self.logs,
            "owner": self.owner,
            "siteName": self.site_name,
            "consumedTime": self.consumed_time.to_dict(),
            "currentTime": self.current_time,
            "submissionTime": self.submission_time,
            "terminationTime": self.termination_time,
            "statusMessage": self.status_message,
            "tags": self.tags,
            "resourceStatus": self.resource_status,
            "name": self.name,
            "queue": self.queue,
            "submissionPreferences": self.submission_preferences,
            "resourceStatusMessage": self.resource_status_message,
            "acl": self.acl,
            "batchSystemID": self.batch_system_id,
            "exitCode": self.exit_code,
            "bssDetails": self.bss_details,
        }


class FirecrestStatus(utils.enums.CaseInsensitiveStrEnum):
    """Job (Slurm) statuses returned by the firecREST API."""

    PENDING = "PENDING"
    CONFIGURING = "CONFIGURING"
    RUNNING = "RUNNING"
    COMPLETING = "COMPLETING"
    COMPLETED = "COMPLETED"
    FAILED = "FAILED"
    CANCELLED = "CANCELLED"

    def to_database_run_status(self) -> database.run.RunStatus | None:
        match self:
            case FirecrestStatus.PENDING:
                return database.run.RunStatus.SCHEDULED
            case FirecrestStatus.CONFIGURING:
                return database.run.RunStatus.RUNNING
            case FirecrestStatus.RUNNING:
                return database.run.RunStatus.RUNNING
            case FirecrestStatus.COMPLETING:
                return database.run.RunStatus.RUNNING
            case FirecrestStatus.COMPLETED:
                return database.run.RunStatus.FINISHED
            case FirecrestStatus.FAILED:
                return database.run.RunStatus.FAILED
            case FirecrestStatus.CANCELLED:
                return database.run.RunStatus.KILLED
            case _:
                raise NotImplementedError(
                    f"Run status {self} not mapped to any database run status"
                )


class Firecrest(base.BaseModel):
    """Job details returned by the external firecREST API."""

    id: str
    user: str
    state: FirecrestStatus = pydantic.Field(
        ...,
        description="Slurm job status",
        examples=FirecrestStatus.COMPLETED,
    )
    name: str = pydantic.Field(
        ...,
        description="Name of the job",
        examples="mantik-<run-id>",
    )
    nodelist: str = pydantic.Field(
        ...,
        description="List of nodes assigned to the job",
        examples="(Priority)",
    )
    nodes: int = pydantic.Field(
        ...,
        description="Number of nodes allocated for the job",
        examples=2,
    )
    partition: str = pydantic.Field(
        ...,
        description="Partition or queue in which the job is running",
        examples="normal",
    )
    cpu_time: str | None = pydantic.Field(
        None,
        description="Total CPU time used by the job.",
        examples="1:00:00",
    )
    elapsed_time: str | None = pydantic.Field(
        None,
        description="Elapsed time since the job started",
        examples="0:30:00",
    )
    start_time: str | None = pydantic.Field(
        None,
        description="Start time of the job.",
        examples="2023-01-01T12:00:00",
    )
    time: str | None = pydantic.Field(
        None,
        description="Scheduled duration of the job.",
        examples="2:00:00",
    )
    time_left: str | None = pydantic.Field(
        None,
        description="Estimated time left until job completion.",
        examples="2023-01-01T14:00:00",
    )
    termination_time: str | None = pydantic.Field(
        None,
        description="Scheduled termination time for the job.",
        examples="2023-01-01T15:00:00",
    )
    exit_code: str = pydantic.Field(
        ...,
        description="Exit code of the job. Typically '0' for success.",
        examples="0",
    )
    job_data_err: str | None = pydantic.Field(
        None,
        description="Standard error output of the job.",
        examples="An error occurred",
    )
    job_data_out: str | None = pydantic.Field(
        None,
        description="Standard output of the job.",
        examples="Job successfully submitted",
    )
    job_file: pathlib.Path | None = pydantic.Field(
        None,
        description="Path to the main script or executable of the job.",
        examples="/path/to/scratchdir/<user>/firecrest/<task-id>/script.batch",
    )
    job_file_err: pathlib.Path | None = pydantic.Field(
        None,
        description="Path to the error log file of the job.",
        examples="/path/to/scratchdir/<user>/mantik/<run-id>/mantik.log",
    )
    job_file_out: pathlib.Path | None = pydantic.Field(
        None,
        description="Path to the output log file of the job.",
        examples="/path/to/scratchdir/<user>/mantik/<run-id>/mantik.log",
    )
    job_info_extra: str | None = pydantic.Field(
        None,
        description="Additional information about the job.",
        examples="Job info returned successfully",
    )

    @classmethod
    def from_dict(cls, id_: str, data: dict[str, str | dict]):
        def to_path_or_none(value: str | None) -> pathlib.Path | None:
            return pathlib.Path(value) if value is not None else value

        return cls(
            id=id_,
            user=data["user"],
            state=data["state"],
            name=data["name"],
            nodelist=data["nodelist"],
            nodes=data["nodes"],
            partition=data["partition"],
            cpu_time=data["cpu_time"],
            elapsed_time=data["elapsed_time"],
            start_time=data["start_time"],
            time=data["time"],
            time_left=data["time_left"],
            termination_time=data["termination_time"],
            exit_code=data["exit_code"],
            job_data_err=data.get("job_data_err"),
            job_data_out=data.get("job_file_out"),
            job_file=to_path_or_none(data.get("job_file")),
            job_file_err=to_path_or_none(data.get("job_file_err")),
            job_file_out=to_path_or_none(data.get("job_file_out")),
            job_info_extra=data.get("job_info_extra"),
        )

    def to_json(self):
        def to_string_or_none(path: pathlib.Path | None) -> str | None:
            return path.as_posix() if path is not None else None

        return {
            "user": self.user,
            "state": self.state,
            "name": self.name,
            "nodelist": self.nodelist,
            "nodes": self.nodes,
            "partition": self.partition,
            "cpu_time": self.cpu_time,
            "elapsed_time": self.elapsed_time,
            "start_time": self.start_time,
            "time": self.time,
            "time_left": self.time_left,
            "termination_time": self.termination_time,
            "exit_code": self.exit_code,
            "job_data_err": self.job_data_err,
            "job_data_out": self.job_data_out,
            "job_file": to_string_or_none(self.job_file),
            "job_file_err": to_string_or_none(self.job_file_err),
            "job_file_out": to_string_or_none(self.job_file_out),
            "job_info_extra": self.job_info_extra,
        }


class SSHSlurmJobStatus(utils.enums.CaseInsensitiveStrEnum):
    """Job (Slurm) statuses returned through SSH

    Information on SLURM status code can be found here:
    https://curc.readthedocs.io/en/latest/running-jobs/squeue-status-codes.html
    """

    COMPLETED = "COMPLETED"
    COMPLETING = "COMPLETING"
    FAILED = "FAILED"
    PENDING = "PENDING"
    PREEMPTED = "PREEMPTED"
    RUNNING = "RUNNING"
    SUSPENDED = "SUSPENDED"
    STOPPED = "STOPPED"

    def to_database_run_status(self) -> database.run.RunStatus:
        match self:
            case SSHSlurmJobStatus.COMPLETED:
                return database.run.RunStatus.FINISHED
            case SSHSlurmJobStatus.RUNNING | SSHSlurmJobStatus.COMPLETING:
                return database.run.RunStatus.RUNNING
            case SSHSlurmJobStatus.FAILED:
                return database.run.RunStatus.FAILED
            case SSHSlurmJobStatus.PENDING:
                return database.run.RunStatus.SCHEDULED
            case SSHSlurmJobStatus.STOPPED | SSHSlurmJobStatus.PREEMPTED | SSHSlurmJobStatus.SUSPENDED:  # noqa E501
                return database.run.RunStatus.KILLED


class SSHSlurm(base.BaseModel):
    """SLURM Job properties.

    This is a subset of SLURM job properties.
    Documentation on all available properties can be found at:
    https://slurm.schedmd.com/sacct.html#OPT_ALL
    """

    id: str = pydantic.Field(..., description="JobID")
    state: SSHSlurmJobStatus = pydantic.Field(..., description="State")
    user: str = pydantic.Field(..., description="User")
    job_name: str = pydantic.Field(..., description="JobName")
    node_list: str = pydantic.Field(..., description="NodeList")
    nodes: str = pydantic.Field(..., description="NNodes")
    partition: str = pydantic.Field(..., description="Partition")
    cpu_time: str = pydantic.Field(..., description="CPUTime")
    elapsed_time: str | None = pydantic.Field(None, description="Elapsed")
    start_time: str | None = pydantic.Field(None, description="Start")
    end_time: str | None = pydantic.Field(None, description="End")
    exit_code: str | None = pydantic.Field(None, description="ExitCode")
    work_dir: str | None = pydantic.Field(None, description="WorkDir")
    consumed_energy: str | None = pydantic.Field(None, description="ConsumedEnergy")
    submit: str | None = pydantic.Field(None, description="Submit")

    @classmethod
    def from_dict(cls, id_: str, data: dict) -> t.Self:
        return cls(
            id=id_,
            state=SSHSlurmJobStatus(data["State"]),
            user=data["User"],
            job_name=data["JobName"],
            node_list=data["NodeList"],
            nodes=data["NNodes"],
            partition=data["Partition"],
            cpu_time=data["CPUTime"],
            elapsed_time=data["Elapsed"],
            start_time=data["Start"],
            end_time=data["End"],
            exit_code=data["ExitCode"],
            work_dir=data["WorkDir"],
            consumed_energy=data["ConsumedEnergy"],
            submit=data["Submit"],
        )

    def to_json(self):
        return {
            "JobID": self.id,
            "State": self.state.value,
            "User": self.user,
            "JobName": self.job_name,
            "NodeList": self.node_list,
            "NNodes": self.nodes,
            "Partition": self.partition,
            "CPUTime": self.cpu_time,
            "Elapsed": self.elapsed_time,
            "Start": self.start_time,
            "End": self.end_time,
            "ExitCode": self.exit_code,
            "WorkDir": self.work_dir,
            "ConsumedEnergy": self.consumed_energy,
            "Submit": self.submit,
        }

    @classmethod
    def sacct_fields(cls) -> list[str]:
        """Fields to be retrieved from SLURM accounting (sacct)"""

        return [
            "JobID",
            "State",
            "User",
            "JobName",
            "NodeList",
            "NNodes",
            "Partition",
            "CPUTime",
            "Elapsed",
            "Start",
            "End",
            "ExitCode",
            "WorkDir",
            "ConsumedEnergy",
            "Submit",
        ]


Unicore.update_forward_refs()
Firecrest.update_forward_refs()
SSHSlurm.update_forward_refs()
