import datetime
import typing as t
import uuid

import pydantic

import mantik_api.database as database
import mantik_api.models.base as base
import mantik_api.models.code_repository as code_repository
import mantik_api.models.data_repository as data_repository
import mantik_api.models.experiment_repository as experiment_repository
import mantik_api.models.label as label
import mantik_api.models.model_repository as model_repository
import mantik_api.models.user as _user


class Project(base.BaseModelWithUserRole):
    project_id: uuid.UUID
    name: str
    executive_summary: str | None
    detailed_description: str | None
    owner: _user.User
    code_repositories: list[code_repository.CodeRepository] | None
    experiment_repositories: list[experiment_repository.ExperimentRepository] | None
    model_repositories: list[model_repository.ModelRepository] | None
    data_repositories: list[data_repository.DataRepository] | None
    labels: list[label.Label] | None
    public: bool
    created_at: datetime.datetime
    updated_at: datetime.datetime | None

    @classmethod
    def from_database_model(
        cls,
        project: database.project.Project,
        user_role: database.role_details.ProjectRole,
        user_id: uuid.UUID | None,
    ) -> t.Self:
        return cls(
            project_id=project.id,
            name=project.name,
            executive_summary=project.executive_summary,
            detailed_description=project.detailed_description,
            owner=_user.User.from_database_model(project.owner),
            code_repositories=base.from_database_models(
                code_repository.CodeRepository,
                project.code_repositories,
                user_role=user_role,
                user_id=user_id,
            ),
            experiment_repositories=base.from_database_models(
                experiment_repository.ExperimentRepository,
                project.experiment_repositories,
                user_role=user_role,
            ),
            model_repositories=base.from_database_models(
                model_repository.ModelRepository,
                project.model_repositories,
                user_role=user_role,
            ),
            data_repositories=base.from_database_models(
                data_repository.DataRepository,
                project.data_repositories,
                user_role=user_role,
                user_id=user_id,
            ),
            labels=base.from_database_models(label.Label, project.labels),
            public=project.public,
            user_role=user_role,
            created_at=project.created_at,
            updated_at=project.updated_at,
        )


class AddProject(base.BaseModel):
    name: str
    executive_summary: str | None
    detailed_description: str | None
    public: bool | None = pydantic.Field(default=False)
    labels: list[uuid.UUID] | None = pydantic.Field(default_factory=list)

    def to_database_model(
        self,
        _uuid: uuid.UUID,
        owner_id: uuid.UUID,
        client: database.client.main.Client,
        exclude_unset: bool | None = False,
    ) -> database.project.Project:
        return database.project.Project(
            id=_uuid,
            owner_id=owner_id,
            labels=label.get_database_models(self.labels, client=client),
            **self.dict(
                convert_keys_to_camel_case=False,
                exclude_unset=exclude_unset,
                exclude={"labels"},
            ),
        )

    def to_overwrite_kwargs(
        self,
        client: database.client.main.Client,
        owner_id: uuid.UUID,
    ) -> dict[str, t.Any]:
        try:
            owner = database.user.get_user_by_id(client, owner_id)
        except database.exceptions.NotFoundError:
            raise database.exceptions.DatabaseExecutionError(
                f"Unable to change owner: user with ID {owner_id} not found"
            )

        return {
            "owner": owner,
            "labels": label.get_database_models(self.labels, client=client),
            **self.dict(
                convert_keys_to_camel_case=False, exclude_unset=True, exclude={"labels"}
            ),
        }


class ProjectsGet200Response(base.PaginationBaseModel):
    projects: list[Project] | None


class ProjectsGet201Response(base.BaseModel):
    project_id: uuid.UUID


Project.update_forward_refs()
AddProject.update_forward_refs()
ProjectsGet200Response.update_forward_refs()
ProjectsGet201Response.update_forward_refs()
