import pydantic

from mantik_api.models import base


class PresignedUrl(base.BaseModel):
    """Presigned S3 bucket URL."""

    url: str = pydantic.Field(
        ...,
        description="Pre-signed URL that allows access to a resource in an s3 bucket",
        example="https://demo.s3.region.amazonaws.com/img.png?X-Amz-Credential=AKWZ7",
    )
