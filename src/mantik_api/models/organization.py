import datetime
import typing as t
import uuid

import mantik_api.database.client as _client
import mantik_api.database.exceptions as _exceptions
import mantik_api.database.organization as _organization
import mantik_api.database.user as _user
import mantik_api.database.user_group as _user_group
import mantik_api.models.base as base
import mantik_api.models.user as user
import mantik_api.models.user_group as user_group


class Organization(base.BaseModel):
    organization_id: uuid.UUID
    name: str
    contact: user.User
    groups: list[user_group.UserGroup] | None
    members: list[user.User] | None
    created_at: datetime.datetime
    updated_at: datetime.datetime | None

    @classmethod
    def from_database_model(cls, organization: _organization.Organization) -> t.Self:
        return cls(
            organization_id=str(organization.id),
            name=organization.name,
            contact=user.User.from_database_model(organization.contact),
            groups=base.from_database_models(user_group.UserGroup, organization.groups),
            members=base.from_database_models(user.User, organization.members),
            created_at=organization.created_at,
            updated_at=organization.updated_at,
        )


class AddOrganization(base.BaseModel):
    name: str

    def to_database_model(
        self,
        organization_id: uuid.UUID,
        contact: _user.User,
    ) -> _organization.Organization:
        return _organization.Organization(
            id=organization_id, name=self.name, contact=contact, members=[contact]
        )


class UpdateOrganization(base.BaseModel):
    name: str
    contact_id: uuid.UUID
    member_ids: list[uuid.UUID]
    group_ids: list[uuid.UUID]

    def to_overwrite_kwargs(
        self,
        client: _client.main.Client,
    ) -> dict[str, t.Any]:
        try:
            contact = _user.get_user_by_id(client, self.contact_id)
        except _exceptions.NotFoundError:
            raise _exceptions.DatabaseExecutionError(
                f"Unable to change contact: user with ID {self.contact_id} not found"
            )

        members = [
            _user.get_user_by_id(client, member_id) for member_id in self.member_ids
        ]
        groups = [
            _user_group.get_group_by_id(client, group_id) for group_id in self.group_ids
        ]

        return {
            **self.dict(convert_keys_to_camel_case=False, exclude_unset=True),
            "contact": contact,
            "members": members,
            "groups": groups,
        }


class OrganizationsGet200Response(base.PaginationBaseModel):
    organizations: list[Organization] | None


class OrganizationsGet201Response(base.BaseModel):
    organization_id: uuid.UUID


Organization.update_forward_refs()
AddOrganization.update_forward_refs()
UpdateOrganization.update_forward_refs()
OrganizationsGet200Response.update_forward_refs()
OrganizationsGet201Response.update_forward_refs()
