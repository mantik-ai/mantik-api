import mantik_api.models.base as base


class GetAllUrlsOfVMsResponse(base.PaginationBaseModel):
    urls: list[str]

    class Config:
        schema_extra = {
            "example": {
                "totalRecords": 2,
                "pageRecords": 2,
                "urls": [
                    "http://s3.amazonaws.com/bucket/image1.vmdk",
                    "http://s3.amazonaws.com/bucket/image2.vmdk",
                ],
            }
        }


class UrlOfVmResponse(base.BaseModel):
    url: str

    class Config:
        schema_extra = {
            "example": {"url": "http://s3.amazonaws.com/bucket/image1.vmdk"}
        }
