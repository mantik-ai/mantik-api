import mantik_api.models.base as base
import mantik_api.models.connection as connection
import mantik_api.models.data_repository as data_repository
import mantik_api.models.experiment_repository as experiment_repository
import mantik_api.models.model_repository as model_repository


class RunsByDataRepositoryUsageInner(base.BaseModel):
    experiment_repository: experiment_repository.ExperimentRepository | None
    model_repository: model_repository.ModelRepository | None
    connection: connection.Connection | None


class RunsByDataRepository(base.BaseModel):
    data_repository: data_repository.DataRepository | None
    usage: list[RunsByDataRepositoryUsageInner] | None


RunsByDataRepository.update_forward_refs()


class ProjectsProjectIdUsageDataGet200Response(base.PaginationBaseModel):
    data_repositories_with_usage: list[RunsByDataRepository] | None


ProjectsProjectIdUsageDataGet200Response.update_forward_refs()
RunsByDataRepository.update_forward_refs()
RunsByDataRepositoryUsageInner.update_forward_refs()
