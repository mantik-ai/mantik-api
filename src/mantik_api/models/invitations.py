import datetime
import typing as t
import uuid

import pydantic

import mantik_api.database as database
import mantik_api.models.base as base


class AddInvitation(base.BaseModel):
    invited_id: uuid.UUID
    role: str | None = pydantic.Field(
        description="only necessary when invitedToType is PROJECT,"
        " and can be REPORTER, RESEARCHER, MAINTAINER or OWNER"
    )
    invited_to_id: uuid.UUID
    invited_to_type: database.invitation.InvitedToType = pydantic.Field(
        description="It can be GROUP or ORGANIZATION or PROJECT"
    )
    invited_type: database.invitation.InvitedType = pydantic.Field(
        description="It can be USER or GROUP or ORGANIZATION"
    )

    # This will be very ugly as long as we do not update pydantic
    @pydantic.validator("invited_type")
    def validate_invited_type_is_right_for_invited_to_type(cls, v, values, **kwargs):
        valid_types = {
            "GROUP": ["USER"],
            "ORGANIZATION": ["USER", "GROUP"],
            "PROJECT": ["USER", "GROUP", "ORGANIZATION"],
        }
        if (
            "invited_to_type" in values
            and v.name not in valid_types[values["invited_to_type"].name]
        ):
            raise ValueError("Invited type is not valid for invited to type")
        return v

    def to_database_model(
        self,
        invitation_id: uuid.UUID,
        inviter_id: uuid.UUID,
    ) -> database.invitation.Invitation:
        return database.invitation.Invitation(
            id=invitation_id,
            inviter_id=inviter_id,
            **self.dict(
                stringify_uuids=False,
                convert_keys_to_camel_case=False,
            ),
        )


class Invitation(base.BaseModel):
    invitation_id: uuid.UUID
    invited_id: uuid.UUID
    invited_type: str
    role: str | None
    invited_to_id: uuid.UUID
    invited_to_type: str | None
    created_at: datetime.datetime
    updated_at: datetime.datetime | None
    invited_name: str
    invited_to_name: str
    inviter_name: str

    @classmethod
    def from_database_model(
        cls,
        invited_name: str,
        invited_to_name: str,
        inviter_name: str,
        invitation: database.invitation.Invitation,
    ) -> t.Self:
        return cls(
            invitation_id=invitation.id,
            invited_id=invitation.invited_id,
            invited_type=invitation.invited_type.name,
            role=invitation.role.name if invitation.role else None,
            invited_to_id=invitation.invited_to_id,
            invited_to_type=invitation.invited_to_type.name,
            created_at=invitation.created_at,
            updated_at=invitation.updated_at,
            inviterName=inviter_name,
            invitedToName=invited_to_name,
            invited_name=invited_name,
        )


class UpdateInvitation(base.BaseModel):
    accepted: bool


class InvitationsGet200Response(base.PaginationBaseModel):
    invitations: list[Invitation] | None


class InvitationsPost201Response(base.BaseModel):
    invitation_id: uuid.UUID


AddInvitation.update_forward_refs()
Invitation.update_forward_refs()
UpdateInvitation.update_forward_refs()
InvitationsGet200Response.update_forward_refs()
InvitationsPost201Response.update_forward_refs()
