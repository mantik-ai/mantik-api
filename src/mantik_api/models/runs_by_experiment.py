import mantik_api.models.base as base
import mantik_api.models.connection as connection
import mantik_api.models.data_repository as data_repository
import mantik_api.models.experiment_repository as experiment_repository
import mantik_api.models.model_repository as model_repository


class RunsByExperimentUsageInner(base.BaseModel):
    model_repository: model_repository.ModelRepository | None
    data_repository: data_repository.DataRepository | None
    connection: connection.Connection | None


class RunsByExperiment(base.BaseModel):
    experiment_repository: experiment_repository.ExperimentRepository | None
    usage: list[RunsByExperimentUsageInner] | None


class ProjectsProjectIdUsageExperimentsGet200Response(base.PaginationBaseModel):
    experiments_with_usage: list[RunsByExperiment] | None


RunsByExperiment.update_forward_refs()
ProjectsProjectIdUsageExperimentsGet200Response.update_forward_refs()
RunsByExperimentUsageInner.update_forward_refs()
