import datetime
import typing as t
import uuid

import pydantic
import pytz

import mantik_api.aws.scheduler as scheduler
import mantik_api.database as database
import mantik_api.models.base as base
import mantik_api.models.connection as connection
import mantik_api.models.run as run
import mantik_api.models.user as user
import mantik_api.tokens.jwt as jwt
import mantik_api.utils.cron as cron


class AddRunSchedule(base.BaseModel):
    name: str
    owner_id: uuid.UUID
    run_id: uuid.UUID
    connection_id: uuid.UUID
    compute_budget_account: str
    cron_expression: str
    time_zone: str
    end_date: int

    @pydantic.validator("cron_expression")
    def must_be_valid_cron_expression(cls, value: str) -> str:
        cron.validate_cron_expression(value)
        return value

    @pydantic.validator("time_zone")
    def must_be_valid_time_zone(cls, value: str) -> str:
        cron.validate_time_zone(value)
        return value

    def to_database_model(
        self, id_: uuid.UUID, aws_schedule_arn: str, project_id: uuid.UUID
    ) -> database.run_schedule.RunSchedule:
        return database.run_schedule.RunSchedule(
            id=id_,
            aws_schedule_arn=aws_schedule_arn,
            name=self.name,
            project_id=project_id,
            owner_id=self.owner_id,
            run_id=self.run_id,
            connection_id=self.connection_id,
            compute_budget_account=self.compute_budget_account,
            cron_expression=self.cron_expression,
            time_zone=self.time_zone,
            end_date=self.end_date,
        )

    def to_overwrite_kwargs(
        self,
        aws_schedule_arn: str,
        client: database.client.main.Client,
    ) -> dict[str, t.Any]:
        try:
            owner = database.user.get_user_by_id(client, self.owner_id)
        except database.exceptions.NotFoundError:
            raise database.exceptions.DatabaseExecutionError(
                f"Unable to change owner: user with ID {self.owner_id} not found"
            )

        return {
            "owner": owner,
            "aws_schedule_arn": aws_schedule_arn,
            **self.dict(convert_keys_to_camel_case=False, exclude_unset=True),
        }

    def to_aws_run_schedule_properties(
        self,
        id_: uuid.UUID,
        project_id: uuid.UUID,
        token: jwt.JWT,
    ) -> scheduler.schedule.Properties:
        return scheduler.schedule.Properties(
            id=id_,
            name=self.name,
            project_id=project_id,
            run_id=self.run_id,
            token=token,
            connection_id=self.connection_id,
            compute_budget_account=self.compute_budget_account,
            cron_expression=self.cron_expression,
            time_zone=self.time_zone,
            end_date=datetime.datetime.fromtimestamp(
                self.end_date, tz=pytz.timezone(self.time_zone)
            ),
        )


class RunSchedule(base.BaseModelWithUserRole):
    run_schedule_id: uuid.UUID
    name: str
    owner: user.User
    run: run.Run
    connection: connection.Connection
    compute_budget_account: str
    cron_expression: str
    time_zone: str
    end_date: int
    created_at: datetime.datetime
    updated_at: datetime.datetime | None

    @pydantic.validator("cron_expression")
    def must_be_valid_cron_expression(cls, value: str) -> str:
        cron.validate_cron_expression(value)
        return value

    @pydantic.validator("time_zone")
    def must_be_valid_time_zone(cls, value: str) -> str:
        cron.validate_time_zone(value)
        return value

    @classmethod
    def from_database_model(
        cls,
        _run_schedule: database.run_schedule.RunSchedule,
        user_role: database.role_details.ProjectRole,
        user_id: uuid.UUID | None,
    ) -> t.Self:
        return cls(
            run_schedule_id=_run_schedule.id,
            name=_run_schedule.name,
            owner=user.User.from_database_model(_run_schedule.owner),
            run=run.Run.from_database_model(
                _run_schedule.run, user_role=user_role, user_id=user_id
            ),
            connection=connection.Connection.from_database_model(
                _run_schedule.connection
            ),
            compute_budget_account=_run_schedule.compute_budget_account,
            cron_expression=_run_schedule.cron_expression,
            time_zone=_run_schedule.time_zone,
            end_date=_run_schedule.end_date,
            user_role=user_role,
            created_at=_run_schedule.created_at,
            updated_at=_run_schedule.updated_at,
        )


class ProjectsProjectIdRunScheduleGet200Response(base.PaginationBaseModelWithUserRole):
    run_schedules: list[RunSchedule] | None


class ProjectsProjectIdRunScheduleGet201Response(base.BaseModel):
    run_schedule_id: uuid.UUID
    timestamp: str | None


RunSchedule.update_forward_refs()
ProjectsProjectIdRunScheduleGet200Response.update_forward_refs()
ProjectsProjectIdRunScheduleGet201Response.update_forward_refs()
