import dataclasses
import datetime
import typing as t
import uuid

import firecrest
import pyunicore.credentials

import mantik_api.database.connection as _connection
import mantik_api.models.base as base
import mantik_api.models.user as _user
import mantik_api.tokens.jwt as jwt
import mantik_api.utils.vault as vault


@dataclasses.dataclass
class SSHCredentials:
    username: str
    password: str | None
    private_key: str | None


class ConnectionId(base.BaseModel):
    connection_id: uuid.UUID


class Connection(base.BaseModel):
    connection_id: uuid.UUID
    user: _user.User
    connection_name: str
    connection_provider: _connection.ConnectionProvider
    auth_method: _connection.AuthMethod
    login_name: str | None
    password: str | None
    token: str | None
    private_key: str | None
    created_at: datetime.datetime
    updated_at: datetime.datetime | None

    @classmethod
    def from_database_model(
        cls,
        connection: _connection.Connection,
        credentials: vault.Credentials | None = None,
    ) -> t.Self:
        credentials = credentials or vault.Credentials()
        return cls(
            connection_id=connection.id,
            user=_user.User.from_database_model(connection.user),
            connection_name=connection.connection_name,
            connection_provider=connection.connection_provider,
            auth_method=connection.auth_method,
            login_name=credentials.login_name,
            password=credentials.password,
            token=credentials.token,
            private_key=credentials.private_key,
            created_at=connection.created_at,
            updated_at=connection.updated_at,
        )

    @classmethod
    def from_database_model_with_credentials(
        cls, connection: _connection.Connection, token: jwt.JWT
    ) -> t.Self:
        """Return instance with real credentials"""
        credentials = vault.get_credential(connection_id=connection.id, token=token)
        return cls.from_database_model(connection, credentials=credentials)

    def to_unicore_credentials(self) -> pyunicore.credentials.Credential:
        match self.auth_method:
            case _connection.AuthMethod.USERNAME_PASSWORD:
                return pyunicore.credentials.UsernamePassword(
                    username=self.login_name, password=self.password
                )
            case _connection.AuthMethod.TOKEN:
                return pyunicore.credentials.OIDCToken(token=self.token)

    def to_firecrest_credentials(
        self, token_url: str
    ) -> firecrest.ClientCredentialsAuth:
        match self.auth_method:
            case _connection.AuthMethod.USERNAME_PASSWORD:
                return firecrest.ClientCredentialsAuth(
                    client_id=self.login_name,
                    client_secret=self.password,
                    token_uri=token_url,
                )
            case _connection.AuthMethod.TOKEN:
                raise RuntimeError(
                    "FirecREST Connections only allow Username-Password Authentication"
                )

    def to_ssh_credentials(self) -> SSHCredentials:
        return SSHCredentials(
            username=self.login_name,
            password=self.password,
            private_key=self.private_key,
        )


class AddConnection(base.BaseModel):
    connection_name: str
    connection_provider: _connection.ConnectionProvider
    auth_method: _connection.AuthMethod
    login_name: str | None
    password: str | None
    token: str | None
    private_key: str | None

    def to_database_model(
        self, _uuid: uuid.UUID, user_id: uuid.UUID
    ) -> _connection.Connection:
        return _connection.Connection(
            id=_uuid,
            user_id=user_id,
            **self.dict(
                convert_keys_to_camel_case=False,
                include={"connection_name", "connection_provider", "auth_method"},
            ),
        )

    def to_vault_credentials(self) -> vault.Credentials:
        return vault.Credentials(
            login_name=self.login_name,
            password=self.password,
            token=self.token,
            private_key=self.private_key,
        )


class UsersUserIdSettingsConnectionsGet200Response(base.PaginationBaseModel):
    connections: list[Connection] | None


class UsersUserIdSettingsConnectionsPost201Response(base.BaseModel):
    connection_id: uuid.UUID


ConnectionId.update_forward_refs()
Connection.update_forward_refs()
AddConnection.update_forward_refs()
UsersUserIdSettingsConnectionsGet200Response.update_forward_refs()
UsersUserIdSettingsConnectionsPost201Response.update_forward_refs()
