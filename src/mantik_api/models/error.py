import mantik_api.models.base as base


class HTTPError(base.BaseModel):
    detail: str | dict
