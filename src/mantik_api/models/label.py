import datetime
import typing as t
import uuid

import mantik_api.database as database
import mantik_api.models.base as base


def get_database_models(
    ids: list[uuid.UUID], client: database.client.main.Client
) -> list[database.label.Label]:
    return [database.label.get_label_by_id(client, id_=id_) for id_ in ids]


def validate_labels_scope(
    label_ids: list[uuid.UUID],
    expected_scope: database.label.Scope,
    client: database.client.main.Client,
) -> None:
    labels = get_database_models(label_ids, client=client)
    for label in labels:
        if expected_scope not in label.scopes:
            raise ValueError(
                f"Unable to add label '{label.name}' to entity '{expected_scope}'. "
                f"Expected scopes are: {label.scopes}"
            )


class Label(base.BaseModel):
    label_id: uuid.UUID
    scopes: list[database.label.Scope]
    category: database.label.Category
    sub_category: database.label.SubCategory | None
    name: str
    created_at: datetime.datetime
    updated_at: datetime.datetime | None

    @classmethod
    def from_database_model(cls, label: database.label.Label) -> t.Self:
        return cls(
            label_id=label.id,
            scopes=label.scopes,
            category=label.category,
            sub_category=label.sub_category,
            name=label.name,
            created_at=label.created_at,
            updated_at=label.updated_at,
        )


class LabelsGet200Response(base.PaginationBaseModel):
    labels: list[Label] | None


class LabelsScopeGet200Response(base.PaginationBaseModel):
    labels: list[Label] | None


Label.update_forward_refs()
LabelsGet200Response.update_forward_refs()
LabelsScopeGet200Response.update_forward_refs()
