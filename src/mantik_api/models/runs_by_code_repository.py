import mantik_api.models.base as base
import mantik_api.models.code_repository as code_repository
import mantik_api.models.connection as connection
import mantik_api.models.data_repository as data_repository
import mantik_api.models.experiment_repository as experiment_repository
import mantik_api.models.model_repository as model_repository


class RunsByCodeRepositoryUsageInner(base.BaseModel):
    experiment_repository: experiment_repository.ExperimentRepository | None
    model_repository: model_repository.ModelRepository | None
    data_repository: data_repository.DataRepository | None
    connection: connection.Connection | None


class RunsByCodeRepository(base.BaseModel):
    code_repository: code_repository.CodeRepository | None
    usage: list[RunsByCodeRepositoryUsageInner] | None


class ProjectsProjectIdUsageCodeGet200Response(base.PaginationBaseModel):
    code_repositories_with_usage: list[RunsByCodeRepository] | None


RunsByCodeRepository.update_forward_refs()
ProjectsProjectIdUsageCodeGet200Response.update_forward_refs()
RunsByCodeRepositoryUsageInner.update_forward_refs()
