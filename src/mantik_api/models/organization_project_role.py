import datetime
import typing as t
import uuid

import mantik_api.database as database
import mantik_api.models.base as base
import mantik_api.models.organization as _organization


class OrganizationProjectRole(base.BaseModel):
    organization: _organization.Organization
    project_id: uuid.UUID
    role: database.project.ProjectRole
    created_at: datetime.datetime
    updated_at: datetime.datetime | None

    @classmethod
    def from_database_model(
        cls,
        organization_project_role: database.project.OrganizationProjectAssociationTable,
        client: database.client.main.Client,
    ) -> t.Self:
        organization = client.get_one(
            database.organization.Organization,
            constraints={
                database.organization.Organization.id: organization_project_role.organization_id  # noqa: E501
            },
        )
        return cls(
            project_id=organization_project_role.project_id,
            organization=_organization.Organization.from_database_model(organization),
            role=organization_project_role.role,
            created_at=organization_project_role.created_at,
            updated_at=organization_project_role.updated_at,
        )


class ProjectsProjectIdOrganizationGet200Response(base.PaginationBaseModelWithUserRole):
    organizations: list[OrganizationProjectRole] | None


OrganizationProjectRole.update_forward_refs()
ProjectsProjectIdOrganizationGet200Response.update_forward_refs()
