import uuid

import mantik_api.database as database
import mantik_api.models.base as base


class ProjectRole(base.BaseModel):
    role: str

    def to_user_project_role_model(
        self, project_id: uuid.UUID, user_id: uuid.UUID
    ) -> database.project.UserProjectAssociationTable:
        return database.project.UserProjectAssociationTable(
            project_id=project_id,
            user_id=user_id,
            role=database.project.ProjectRole[self.role],
        )

    def to_user_group_project_role_model(
        self, project_id: uuid.UUID, user_group_id: uuid.UUID
    ) -> database.project.UserGroupProjectAssociationTable:
        return database.project.UserGroupProjectAssociationTable(
            project_id=project_id,
            user_group_id=user_group_id,
            role=database.project.ProjectRole[self.role],
        )

    def to_organization_project_role_model(
        self, project_id: uuid.UUID, organization_id: uuid.UUID
    ) -> database.project.OrganizationProjectAssociationTable:
        return database.project.OrganizationProjectAssociationTable(
            project_id=project_id,
            organization_id=organization_id,
            role=database.project.ProjectRole[self.role],
        )


ProjectRole.update_forward_refs()
