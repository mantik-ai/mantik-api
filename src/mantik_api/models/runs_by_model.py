import mantik_api.models.base as base
import mantik_api.models.connection as connection
import mantik_api.models.data_repository as data_repository
import mantik_api.models.experiment_repository as experiment_repository
import mantik_api.models.model_repository as model_repository


class RunsByModelUsageInner(base.BaseModel):
    experiment_repository: experiment_repository.ExperimentRepository | None
    data_repository: data_repository.DataRepository | None
    connection: connection.Connection | None


class RunsByModel(base.BaseModel):
    model_repository: model_repository.ModelRepository | None
    usage: list[RunsByModelUsageInner] | None


class ProjectsProjectIdUsageModelsGet200Response(base.PaginationBaseModel):
    models_with_usage: list[RunsByModel] | None


ProjectsProjectIdUsageModelsGet200Response.update_forward_refs()


RunsByModel.update_forward_refs()
RunsByModelUsageInner.update_forward_refs()
