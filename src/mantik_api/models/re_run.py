import uuid

import mantik_api.models.base as base


class ReRun(base.BaseModel):
    connection_id: uuid.UUID | None
    compute_budget_account: str | None
