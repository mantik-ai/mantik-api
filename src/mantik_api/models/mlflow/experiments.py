import mantik_api.models.base as base


class ExperimentPermissionResponse(base.BaseModel):
    allowed: bool | None = None
    message: str | None = None
    experiment_ids: list[int] | None = None


class ExperimentPermissionRequest(base.BaseModel):
    endpoint: str
    experiment_id: int | None = None
    experiment_name: str | None = None


ExperimentPermissionResponse.update_forward_refs()
ExperimentPermissionRequest.update_forward_refs()
