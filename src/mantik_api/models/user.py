import datetime
import typing as t
import uuid

import mantik_api.database as database
import mantik_api.database.client as _client
import mantik_api.database.exceptions as _exceptions
import mantik_api.models.base as base


class User(base.BaseModel):
    user_id: uuid.UUID
    name: str
    created_at: datetime.datetime
    updated_at: datetime.datetime | None

    @classmethod
    def from_database_model(cls, user: database.user.User) -> t.Self:
        return cls(
            user_id=user.id,
            name=user.preferred_name,
            created_at=user.created_at,
            updated_at=user.updated_at,
        )


class UserDetails(User):
    email: str | None
    full_name: str | None
    info: str | None
    company: str | None
    job_title: str | None
    website_url: str | None

    @classmethod
    def from_database_model(
        cls, user: database.user.User, include_sensitive_data: bool = False
    ) -> t.Self:
        return cls(
            user_id=user.id,
            name=user.preferred_name,
            created_at=user.created_at,
            updated_at=user.updated_at,
            # Sensitive data
            email=user.email if include_sensitive_data else None,
            full_name=user.full_name if include_sensitive_data else None,
            info=user.info if include_sensitive_data else None,
            company=user.company if include_sensitive_data else None,
            job_title=user.job_title if include_sensitive_data else None,
            website_url=user.website_url if include_sensitive_data else None,
        )


class AddUser(base.BaseModel):
    password: str
    name: str
    email: str
    full_name: str | None
    info: str | None
    company: str | None
    job_title: str | None
    website_url: str | None

    def to_database_model(self, user_id: uuid.UUID) -> database.user.User:
        return database.user.User(
            id=user_id,
            cognito_name=self.name,
            preferred_name=self.name,
            email=self.email,
            full_name=self.full_name,
            info=self.info,
            company=self.company,
            job_title=self.job_title,
            website_url=self.website_url,
        )


class UpdateUser(base.BaseModel):
    name: str
    full_name: str | None
    info: str | None
    company: str | None
    job_title: str | None
    website_url: str | None

    def to_overwrite_kwargs(
        self, client: _client.main.Client, user: database.user.User
    ) -> dict[str, t.Any]:
        try:
            user = database.user.get_user_by_id(client, user.id)
        except _exceptions.NotFoundError:
            raise _exceptions.DatabaseExecutionError(
                f"Unable to update user details: user with ID {user.id} not found"
            )
        return {
            "user": user,
            **self.dict(convert_keys_to_camel_case=False, exclude_unset=True),
        }


class ChangePassword(base.BaseModel):
    old_password: str
    new_password: str


class UpdateEmail(base.BaseModel):
    new_email: str


class VerifyUpdateEmail(base.BaseModel):
    confirmation_code: str


class UsersGet200Response(base.PaginationBaseModel):
    users: list[User] | None


class UsersGet201Response(base.BaseModel):
    user_id: uuid.UUID


User.update_forward_refs()
AddUser.update_forward_refs()
UpdateUser.update_forward_refs()
UpdateEmail.update_forward_refs()
VerifyUpdateEmail.update_forward_refs()
ChangePassword.update_forward_refs()
UsersGet200Response.update_forward_refs()
UsersGet201Response.update_forward_refs()
UserDetails.update_forward_refs()
