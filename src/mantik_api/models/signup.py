import mantik_api.models.base as base


class ConfirmationCodeUserDetails(base.UsernameAndEmailBaseModel):
    pass


class SignupConfirmationCodeUserDetailsPost200Response(base.BaseModel):
    user_name: str


class EmailConfirmationDetails(base.BaseModel):
    confirmation_code: str
    user_name: str


ConfirmationCodeUserDetails.update_forward_refs()
SignupConfirmationCodeUserDetailsPost200Response.update_forward_refs()
EmailConfirmationDetails.update_forward_refs()
