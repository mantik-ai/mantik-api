import datetime
import typing as t
import uuid

import pydantic

import mantik_api.database as database
import mantik_api.models.base as base
import mantik_api.models.label as label


class DataRepository(base.BaseModelWithUserRole):
    data_repository_id: uuid.UUID
    data_repository_name: str | None
    uri: str
    description: str | None = None
    labels: list[label.Label] | None = pydantic.Field(default_factory=list)
    platform: database.git_repository.Platform
    data_repository_id: uuid.UUID
    is_dvc_enabled: bool = pydantic.Field(
        False,
        description="Whether the repository has DVC (Data Version Control) enabled.",
    )
    versions: dict[str, str] = pydantic.Field(
        default_factory=dict, description="Data versions"
    )
    dvc_connection_id: uuid.UUID | None = pydantic.Field(
        None, description="DVC backend connection id"
    )
    connection_id: uuid.UUID | None = pydantic.Field(
        None, description="Git connection id for private repositories."
    )
    created_at: datetime.datetime
    updated_at: datetime.datetime | None

    @classmethod
    def from_database_model(
        cls,
        data_repository: database.data_repository.DataRepository,
        user_role: database.role_details.ProjectRole,
        user_id: uuid.UUID | None,
    ) -> t.Self:
        return cls(
            data_repository_id=data_repository.id,
            data_repository_name=data_repository.data_repository_name,
            uri=data_repository.uri,
            description=data_repository.description,
            platform=data_repository.platform,
            labels=base.from_database_models(label.Label, data_repository.labels),
            user_role=user_role,
            created_at=data_repository.created_at,
            updated_at=data_repository.updated_at,
            is_dvc_enabled=data_repository.is_dvc_enabled,
            dvc_connection_id=data_repository.my_dvc_connection_id(user_id=user_id)
            if user_id is not None
            else None,
            connection_id=data_repository.my_git_connection_id(user_id=user_id)
            if user_id is not None
            else None,
            versions=data_repository.versions or {},
        )


class UpdateDataRepository(base.BaseModel):
    data_repository_name: str | None
    uri: str
    description: str | None = None
    labels: list[uuid.UUID] | None = pydantic.Field(default_factory=list)
    platform: database.git_repository.Platform
    is_dvc_enabled: bool = pydantic.Field(
        False,
        description="Whether the repository has DVC (Data Version Control) enabled.",
    )
    versions: dict[str, str] | None = pydantic.Field(None, description="Data versions")

    def to_database_model(
        self,
        project_id: uuid.UUID,
        data_repository_id: uuid.UUID,
        client: database.client.main.Client,
        exclude_unset: bool | None = False,
    ) -> database.data_repository.DataRepository:
        return database.data_repository.DataRepository(
            id=data_repository_id,
            project_id=project_id,
            labels=label.get_database_models(self.labels, client=client),
            **self.dict(
                exclude_unset=exclude_unset,
                convert_keys_to_camel_case=False,
                exclude={"labels"},
            ),
        )

    def to_overwrite_kwargs(
        self,
        client: database.client.main.Client,
        exclude_unset: bool | None = False,
    ) -> dict[str, t.Any]:
        return {
            "labels": label.get_database_models(self.labels, client=client),
            **self.dict(
                exclude_unset=exclude_unset,
                convert_keys_to_camel_case=False,
                exclude={"labels"},
            ),
        }


class AddDataRepository(UpdateDataRepository):
    connection_id: uuid.UUID | None = pydantic.Field(
        None, description="Git connection id for private repositories."
    )

    def to_database_model(
        self,
        project_id: uuid.UUID,
        data_repository_id: uuid.UUID,
        client: database.client.main.Client,
        exclude_unset: bool | None = False,
    ) -> database.data_repository.DataRepository:
        return database.data_repository.DataRepository(
            id=data_repository_id,
            project_id=project_id,
            labels=label.get_database_models(self.labels, client=client),
            **self.dict(
                exclude_unset=exclude_unset,
                convert_keys_to_camel_case=False,
                exclude={"labels", "connection_id"},
            ),
        )


class ProjectsProjectIdDataGet200Response(base.PaginationBaseModelWithUserRole):
    data_repositories: list[DataRepository] | None


class ProjectsProjectIdDataGet201Response(base.BaseModel):
    data_repository_id: uuid.UUID


DataRepository.update_forward_refs()
AddDataRepository.update_forward_refs()
ProjectsProjectIdDataGet200Response.update_forward_refs()
ProjectsProjectIdDataGet201Response.update_forward_refs()
