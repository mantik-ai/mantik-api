import datetime
import typing as t
import uuid

import mantik_api.database as database
import mantik_api.database.role_details as _role_details
import mantik_api.models.base as base
import mantik_api.models.user as _user


class ProjectMember(base.BaseModel):
    user: _user.User
    project_id: uuid.UUID
    role: _role_details.ProjectRole
    added_at: datetime.datetime | None
    updated_at: datetime.datetime | None

    @classmethod
    def from_database_model(
        cls,
        project_id: uuid.UUID,
        user: database.user.User,
        role_details: _role_details.RoleDetails,
    ) -> t.Self:
        return cls(
            project_id=project_id,
            user=_user.User.from_database_model(user),
            role=role_details.role,
            added_at=role_details.added_at,
            updated_at=role_details.updated_at,
        )


class ProjectsProjectIdMembersGet200Response(base.PaginationBaseModelWithUserRole):
    members: list[ProjectMember] | None


ProjectMember.update_forward_refs()
ProjectsProjectIdMembersGet200Response.update_forward_refs()
