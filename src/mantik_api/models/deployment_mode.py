import typing as t

import mantik_api.models.base as base


class DeploymentMode(base.BaseModel):
    name: str
    parameters: dict[str, t.Any] | None


DeploymentMode.update_forward_refs()
