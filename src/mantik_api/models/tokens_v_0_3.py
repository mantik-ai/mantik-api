import abc
import datetime
import typing as t

import mantik_api.aws.cognito as cognito
import mantik_api.database as database
import mantik_api.models.base as base


class _TokenRequest(base.BaseModel, abc.ABC):
    """Credentials required for a token request.

    Parameters
    ----------
    username : str
        Name of the user.

    """

    username: str

    @abc.abstractmethod
    def to_cognito_credentials(
        self, user: database.user.User
    ) -> cognito.credentials.Credentials:
        """Return as Cogito credentials."""


class CreateTokenRequest(_TokenRequest):
    """Credentials required to create a token."""

    password: str

    def to_cognito_credentials(
        self, user: database.user.User
    ) -> cognito.credentials.CreateTokenCredentials:
        return cognito.credentials.CreateTokenCredentials(
            cognito_username=user.cognito_name,
            password=self.password,
        )


class RefreshTokenRequest(_TokenRequest):
    """Credentials required to refresh a token."""

    refresh_token: str

    def to_cognito_credentials(
        self, user: database.user.User
    ) -> cognito.credentials.RefreshTokenCredentials:
        return cognito.credentials.RefreshTokenCredentials(
            cognito_username=user.cognito_name,
            refresh_token=self.refresh_token,
        )


class _TokenResponse(base.BaseModel):
    access_token: str
    expires_at: datetime.datetime

    @classmethod
    def from_cognito_tokens(cls, tokens: cognito.tokens.Tokens) -> t.Self:
        """Create from Cognito Tokens."""
        return cls(
            access_token=tokens.access_token,
            expires_at=tokens.expires_at,
        )


class CreateTokenResponse(_TokenResponse):
    """Access token with expiration date and a refresh token."""

    refresh_token: str

    @classmethod
    def from_cognito_tokens(cls, tokens: cognito.tokens.Tokens) -> t.Self:
        """Create from Cognito Tokens."""
        return cls(
            access_token=tokens.access_token,
            refresh_token=tokens.refresh_token,
            expires_at=tokens.expires_at,
        )


class RefreshTokenResponse(_TokenResponse):
    """Refreshed access token and it's expiration date."""


CreateTokenRequest.update_forward_refs()
RefreshTokenRequest.update_forward_refs()
CreateTokenResponse.update_forward_refs()
RefreshTokenResponse.update_forward_refs()
