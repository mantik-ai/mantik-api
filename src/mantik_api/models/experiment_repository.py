import datetime
import typing as t
import uuid

import pydantic

import mantik_api.database as database
import mantik_api.models.base as base
import mantik_api.models.label as label


class ExperimentRepository(base.BaseModelWithUserRole):
    experiment_repository_id: uuid.UUID
    mlflow_experiment_id: int
    name: str
    artifact_location: str | None
    lifecycle_stage: str | None
    last_update_time: int | None
    creation_time: int | None
    labels: list[label.Label] | None
    created_at: datetime.datetime
    updated_at: datetime.datetime | None

    @classmethod
    def from_database_model(
        cls,
        experiment_repository: database.experiment_repository.ExperimentRepository,
        user_role: database.role_details.ProjectRole,
    ) -> t.Self:
        return cls(
            experiment_repository_id=experiment_repository.id,
            mlflow_experiment_id=experiment_repository.mlflow_experiment_id,
            name=experiment_repository.name,
            artifact_location=experiment_repository.artifact_location,
            lifecycle_stage=experiment_repository.lifecycle_stage,
            last_update_time=experiment_repository.last_update_time,
            creation_time=experiment_repository.creation_time,
            labels=base.from_database_models(label.Label, experiment_repository.labels),
            user_role=user_role,
            created_at=experiment_repository.created_at,
            updated_at=experiment_repository.updated_at,
        )


class AddExperimentRepository(base.BaseModel):
    name: str
    labels: list[uuid.UUID] | None = pydantic.Field(default_factory=list)

    def to_database_model(
        self,
        project_id: uuid.UUID,
        experiment_repository_id: uuid.UUID,
        client: database.client.main.Client,
        mlflow_experiment_id: int | None = None,
        exclude_unset: bool | None = False,
    ) -> database.experiment_repository.ExperimentRepository:
        experiment = database.experiment_repository.ExperimentRepository(
            id=experiment_repository_id,
            project_id=project_id,
            labels=label.get_database_models(self.labels, client=client),
            **self.dict(
                exclude_unset=exclude_unset,
                convert_keys_to_camel_case=False,
                exclude={"labels"},
            )
        )
        if mlflow_experiment_id is not None:
            experiment.mlflow_experiment_id = mlflow_experiment_id
        return experiment

    def to_overwrite_kwargs(
        self,
        client: database.client.main.Client,
        exclude_unset: bool | None = False,
    ) -> dict[str, t.Any]:
        return {
            "labels": label.get_database_models(self.labels, client=client),
            **self.dict(
                exclude_unset=exclude_unset,
                convert_keys_to_camel_case=False,
                exclude={"labels"},
            ),
        }


class ProjectsProjectIdExperimentsGet200Response(base.PaginationBaseModelWithUserRole):
    experiment_repositories: list[ExperimentRepository] | None


class ProjectsProjectIdExperimentsGet201Response(base.BaseModel):
    experiment_repository_id: uuid.UUID


ExperimentRepository.update_forward_refs()
AddExperimentRepository.update_forward_refs()
ProjectsProjectIdExperimentsGet200Response.update_forward_refs()
ProjectsProjectIdExperimentsGet201Response.update_forward_refs()
