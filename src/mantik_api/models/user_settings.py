import typing as t

import mantik_api.database.user as user_db
import mantik_api.models.base as base
import mantik_api.models.payment_info as _payment_info
import mantik_api.models.user as _user


class UserSettings(base.BaseModel):
    user: _user.User
    payment_info: _payment_info.PaymentInfo | None

    @classmethod
    def from_database_model(cls, user: user_db.User) -> t.Self:
        return cls(
            user=_user.User.from_database_model(user),
            payment_info=_payment_info.PaymentInfo.from_database_model(user),
        )


UserSettings.update_forward_refs()
