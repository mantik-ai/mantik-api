import datetime
import enum
import pathlib
import typing as t
import uuid

import pydantic
import sqlalchemy

import mantik_api.database as database

DatabaseModel = t.TypeVar("DatabaseModel", bound=database.base.Base)


def _stringify(d: dict, encoder: t.Callable) -> dict:
    return {key: encoder(value) for key, value in d.items()}


def _uuid_encoder(value: t.Any) -> t.Any:
    if isinstance(value, dict):
        return _stringify(value, encoder=_uuid_encoder)
    elif isinstance(value, uuid.UUID):
        return str(value)
    elif isinstance(value, pathlib.Path):
        return value.as_posix()
    return value


def _path_encoder(value: t.Any) -> t.Any:
    if isinstance(value, dict):
        return _stringify(value, encoder=_path_encoder)
    elif isinstance(value, pathlib.Path):
        return value.as_posix()
    return value


def _datetime_encoder(value: t.Any) -> t.Any:
    if isinstance(value, dict):
        return _stringify(value, encoder=_datetime_encoder)
    elif isinstance(value, datetime.datetime):
        return value.isoformat()
    return value


def _enum_encoder(value: t.Any | enum.Enum) -> t.Any:
    if isinstance(value, enum.Enum):
        return value.value
    return value


def _to_camel_case(string: str) -> str:
    parts = string.split("_")
    first = parts[0]
    rest = parts[1:] if len(parts) > 1 else []
    return first + "".join(word.capitalize() for word in rest)


class BaseModel(pydantic.BaseModel):
    class Config:
        # Automatically generate alias in Camel Case for each property.
        # E.g. project_id would be converted to projectId automatically.
        # Request models and response models use these aliases.
        alias_generator = _to_camel_case
        allow_population_by_field_name = True
        # Enabling response by alias returns the model properties by their alias
        response_model_by_alias = True

    def dict(
        self,
        stringify_uuids: bool = True,
        stringify_paths: bool = True,
        isoformat_datetime: bool = True,
        convert_keys_to_camel_case: bool = True,
        *args,
        **kwargs,
    ) -> dict:
        kwargs["by_alias"] = kwargs.get("by_alias") or convert_keys_to_camel_case
        _dict = super().dict(*args, **kwargs)
        if stringify_uuids:
            _dict = _stringify(_dict, encoder=_uuid_encoder)
        if stringify_paths:
            _dict = _stringify(_dict, encoder=_path_encoder)
        if isoformat_datetime:
            _dict = _stringify(_dict, encoder=_datetime_encoder)
        _dict = _stringify(_dict, encoder=_enum_encoder)
        return _dict


class BaseModelWithUserRole(BaseModel):
    """Requires the role of a user for a project or an asset of a project.

    Used in ``GET`` endpoint responses to allow the frontend to render
    according to a user's permissions, i.e. their role.

    """

    user_role: database.role_details.ProjectRole = (
        database.role_details.ProjectRole.NO_ROLE
    )


class PaginationBaseModel(BaseModel):
    total_records: int
    page_records: int


class PaginationBaseModelWithUserRole(BaseModel):
    total_records: int
    page_records: int
    user_role: database.role_details.ProjectRole = (
        database.role_details.ProjectRole.NO_ROLE
    )


def from_database_models(
    model: type[BaseModel],
    database_models: list[DatabaseModel] | t.Iterator[DatabaseModel],
    *args,
    **kwargs,
) -> t.Iterator[database.base.Base]:
    return (
        model.from_database_model(database_model, *args, **kwargs)
        for database_model in database_models
    )


class UsernameAndEmailBaseModel(BaseModel):
    user_name: str | None
    email: str | None

    @property
    def no_option_given(self) -> bool:
        return self.user_name is None and self.email is None

    @property
    def all_options_given(self) -> bool:
        return self.user_name is not None and self.email is not None

    @property
    def option(self) -> str:
        if self.user_name is not None:
            return "name"
        elif self.email is not None:
            return "email"
        raise NotImplementedError

    @property
    def value(self) -> str:
        if self.user_name is not None:
            return self.user_name
        elif self.email is not None:
            return self.email
        raise NotImplementedError

    def create_user_constraint(self) -> dict[sqlalchemy.Column, str]:
        if self.user_name is not None:
            return {database.user.User.preferred_name: self.user_name}
        elif self.email is not None:
            return {database.user.User.email: self.email}
        else:
            raise NotImplementedError(
                f"No implementation to resend confirmation code for {self}"
            )


class BaseModelWithBranchOrCommit(BaseModel):
    """Defines branch and commit and validates them at creation."""

    branch: str | None
    commit: str | None
    code_repository_id: uuid.UUID | None

    @pydantic.root_validator()
    def validate_branch_and_commit_and_override_branch(
        cls,
        values: dict[str, str | uuid.UUID | None],
    ) -> dict[str, str | uuid.UUID | None]:
        """Validate branch/commit and override branch if required.

        1. Validates that branch or commit are given
        2. overrides branch to `None` if commit is given.

        """
        branch = values.get("branch") or None
        commit = values.get("commit") or None
        code_repository_id = values.get("code_repository_id") or None

        if code_repository_id:
            # If no branch or commit given, raise error.
            if branch is None and commit is None:
                raise ValueError("No branch name or commit hash given")
            elif commit is not None:
                # Set branch to `None` if commit given to prefer commit.
                values["branch"] = None
            else:
                # If no commit given (can be empty string), set commit to `None`.
                values["commit"] = None
        return values
