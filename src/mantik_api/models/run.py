import datetime
import enum
import pathlib
import typing as t
import uuid

import pydantic

import mantik_api.database as database
import mantik_api.models.base as base
import mantik_api.models.code_repository as code_repository
import mantik_api.models.connection as connection
import mantik_api.models.data_repository as data_repository
import mantik_api.models.environment as environment
import mantik_api.models.experiment_repository as experiment_repository
import mantik_api.models.model_repository as model_repository
import mantik_api.models.trained_model as trained_model
import mantik_api.models.user as user

_BACKEND_CONFIG_FIELD = pydantic.Field(
    ...,
    example={
        "UnicoreApiUrl": "https://zam2125.zam.kfa-juelich.de:9112/JUWELS/rest/core",
        "Environment": {"Apptainer": {"Path": "some/image/path.name", "Type": "local"}},
        "Resources": {"Queue": "devel", "Nodes": 1},
    },
)


class ProviderType(enum.Enum):
    JUPYTER = "Jupyter"
    COLAB = "Colab"


class GPUInfo(base.BaseModel):
    name: str
    id: str
    driver: str
    total_memory: str


class NoteBookSource(base.BaseModel):
    """Represents the source information of a notebook.

    Contains metadata about where a notebook is located and how it is executed.

    Parameters
    ----------
    location : str
        URL or filesystem path to the notebook
    version : str, optional
        Version identifier (e.g. git commit)
    provider : ProviderType
        Notebook environment provider (Jupyter/Colab)
    """

    location: str
    version: t.Optional[str]
    provider: ProviderType


class RunInfrastructure(base.BaseModel):
    os: str
    cpu_cores: int
    gpu_count: int
    gpu_info: list[GPUInfo]
    hostname: str
    memory_gb: str
    platform: str
    processor: str
    python_version: str
    python_executable: str


class Run(base.BaseModelWithUserRole):
    run_id: uuid.UUID
    name: str
    mlflow_run_id: uuid.UUID
    user: user.User
    experiment_repository: experiment_repository.ExperimentRepository
    model_repository: model_repository.ModelRepository | None
    saved_model: trained_model.TrainedModel | None
    data_repository: data_repository.DataRepository | None
    data_branch: str | None = pydantic.Field(
        None,
        description="Branch of data repository to download",
    )
    data_commit: str | None = pydantic.Field(
        None,
        description="Commit hash of data repository to download. "
        "Takes precedence over branch",
    )
    data_target_dir: str | None = pydantic.Field(
        None,
        description="Location where to download the data on the remote system.",
    )
    connection: connection.Connection | None
    compute_budget_account: str | None
    mlflow_mlproject_file_path: pathlib.Path | None
    entry_point: str | None
    mlflow_parameters: dict | None
    environment_id: uuid.UUID | None
    backend_config: dict | None = _BACKEND_CONFIG_FIELD
    status: database.run.RunStatus | None
    start_time: int | None
    end_time: int | None
    artifact_uri: str | None
    lifecycle_stage: str | None
    info: str | None
    infrastructure: RunInfrastructure | None
    notebook_source: NoteBookSource | None
    created_at: datetime.datetime
    updated_at: datetime.datetime | None

    @classmethod
    def from_database_model(
        cls,
        run: database.run.Run,
        user_role: database.role_details.ProjectRole,
        user_id: uuid.UUID | None,
    ) -> t.Self:
        _connection = (
            connection.Connection.from_database_model(run.connection)
            if run.connection_id is not None
            else None
        )

        return cls(
            run_id=run.id,
            name=run.name,
            mlflow_run_id=run.mlflow_run_id,
            model_repository=(
                None
                if run.model_repository is None
                else model_repository.ModelRepository.from_database_model(
                    run.model_repository, user_role=user_role
                )
            ),
            experiment_repository=experiment_repository.ExperimentRepository.from_database_model(  # noqa: E501
                run.experiment_repository, user_role=user_role
            ),
            saved_model=(
                None
                if run.saved_model_repository is None
                else trained_model.TrainedModel.from_database_model(
                    run.saved_model_repository, user_role=user_role
                )
            ),
            data_repository=(
                None
                if run.data_repository is None
                else data_repository.DataRepository.from_database_model(
                    run.data_repository, user_role=user_role, user_id=user_id
                )
            ),
            data_branch=run.data_branch,
            data_commit=run.data_commit,
            data_target_dir=run.data_target_dir,
            connection=_connection,
            user=user.User.from_database_model(run.user),
            status=run.status,
            start_time=run.start_time,
            end_time=run.end_time,
            artifact_uri=run.artifact_uri,
            lifecycle_stage=run.lifecycle_stage,
            mlflow_mlproject_file_path=(
                None
                if run.mlflow_mlproject_file_path is None
                else pathlib.Path(run.mlflow_mlproject_file_path)
            ),
            entry_point=run.entry_point,
            mlflow_parameters=run.mlflow_parameters,
            environment_id=run.environment_id,
            infrastructure=run.infrastructure,
            notebook_source=run.notebook_source,
            backend_config=run.backend_config_environment_resolved,
            compute_budget_account=run.compute_budget_account,
            user_role=user_role,
            created_at=run.created_at,
            updated_at=run.updated_at,
        )


class AddRun(base.BaseModelWithBranchOrCommit):
    name: str
    experiment_repository_id: uuid.UUID
    code_repository_id: uuid.UUID | None
    data_repository_id: uuid.UUID | None
    data_branch: str | None = pydantic.Field(
        None,
        description="Branch of data repository to download",
    )
    data_commit: str | None = pydantic.Field(
        None,
        description="Commit hash of data repository to download. T"
        "akes precedence over branch",
    )
    data_target_dir: str | None = pydantic.Field(
        None,
        description="Location where to download the data on the remote system.",
    )
    connection_id: uuid.UUID | None = pydantic.Field(
        None,
        description="Required to submit run for execution on remote infrastructure.",
    )
    compute_budget_account: str | None = pydantic.Field(
        None,
        description="Name of the compute budget account that is used for allocating "
        "compute resources on a remote compute system (e.g. HPC cluster)"
        "Required to submit run for execution on remote infrastructure.",
    )
    mlflow_run_id: uuid.UUID | None = pydantic.Field(
        None, description="Required when saving (not submitting) a run."
    )
    mlflow_mlproject_file_path: pathlib.Path | None
    entry_point: str | None
    mlflow_parameters: dict | None
    environment_id: uuid.UUID | None
    backend_config: dict | None = _BACKEND_CONFIG_FIELD

    @pydantic.validator("mlflow_mlproject_file_path")
    def validate_mlproject_file_path(cls, value: str | None) -> pathlib.Path | None:
        if value:
            return pathlib.Path(value)
        else:
            return None

    def to_database_model(
        self,
        mantik_run_id: uuid.UUID,
        mlflow_run_id: uuid.UUID,
        project_id: uuid.UUID,
        model_repository_id: uuid.UUID | None,
        user_id: uuid.UUID,
        remote_system_job_id: str | None,
        exclude_unset: bool | None = False,
    ) -> database.run.Run:
        return database.run.Run(
            id=mantik_run_id,
            mlflow_run_id=mlflow_run_id,
            remote_system_job_id=remote_system_job_id,
            project_id=project_id,
            model_repository_id=model_repository_id,
            user_id=user_id,
            **self.dict(
                convert_keys_to_camel_case=False,
                exclude_unset=exclude_unset,
                exclude={"code_repository_id", "branch", "commit", "mlflow_run_id"},
            ),
        )

    def to_run_configuration_database_model(
        self,
        run_configuration_id: uuid.UUID,
        project_id: uuid.UUID,
        user_id: uuid.UUID,
    ) -> database.run.RunConfiguration:
        return database.run.RunConfiguration(
            id=run_configuration_id,
            project_id=project_id,
            user_id=user_id,
            **self.dict(
                convert_keys_to_camel_case=False,
                exclude={"mlflow_run_id"},
            ),
        )


class RunConfiguration(base.BaseModelWithUserRole):
    run_configuration_id: uuid.UUID
    name: str
    user: user.User
    experiment_repository: experiment_repository.ExperimentRepository
    code_repository: code_repository.CodeRepository | None
    data_repository: data_repository.DataRepository | None
    branch: str | None
    commit: str | None
    data_branch: str | None = pydantic.Field(
        None,
        description="Branch of data repository to download",
    )
    data_commit: str | None = pydantic.Field(
        None,
        description="Commit hash of data repository to download. "
        "Takes precedence over branch",
    )
    data_target_dir: str | None = pydantic.Field(
        None,
        description="Location where to download the data on the remote system.",
    )
    connection: connection.Connection
    compute_budget_account: str
    mlflow_mlproject_file_path: pathlib.Path | None
    entry_point: str | None
    mlflow_parameters: dict | None
    backend_config: dict | None = _BACKEND_CONFIG_FIELD
    environment: environment.Environment | None
    created_at: datetime.datetime
    updated_at: datetime.datetime | None

    @classmethod
    def from_database_model(
        cls,
        run: database.run.RunConfiguration,
        user_role: database.role_details.ProjectRole,
        user_id: uuid.UUID | None,
    ) -> t.Self:
        return cls(
            run_configuration_id=run.id,
            name=run.name,
            user=user.User.from_database_model(run.user),
            code_repository=code_repository.CodeRepository.from_database_model(
                run.code_repository,
                user_role=user_role,
                user_id=user_id,
            ),
            experiment_repository=experiment_repository.ExperimentRepository.from_database_model(  # noqa: E501
                run.experiment_repository, user_role=user_role
            ),
            data_repository=(
                None
                if run.data_repository is None
                else data_repository.DataRepository.from_database_model(
                    run.data_repository, user_role=user_role, user_id=user_id
                )
            ),
            connection=connection.Connection.from_database_model(run.connection),
            compute_budget_account=run.compute_budget_account,
            branch=run.branch,
            commit=run.commit,
            data_commit=run.data_commit,
            data_branch=run.data_branch,
            data_target_dir=run.data_target_dir,
            mlflow_mlproject_file_path=pathlib.Path(run.mlflow_mlproject_file_path),
            entry_point=run.entry_point,
            mlflow_parameters=run.mlflow_parameters,
            backend_config=run.backend_config_environment_resolved,
            environment=(
                None
                if run.environment is None
                else environment.Environment.from_database_model(run.environment)
            ),
            user_role=user_role,
            updated_at=run.updated_at,
            created_at=run.created_at,
        )


class GetRunInfrastructureResponse(base.BaseModel):
    infrastructure: RunInfrastructure | None


class GetRunNoteBookResponse(base.BaseModel):
    notebook_source: NoteBookSource | None


class ProjectsProjectIdRunsGet200Response(base.PaginationBaseModelWithUserRole):
    runs: list[Run] | None


class ProjectsProjectIdRunsGet201Response(base.BaseModel):
    run_id: uuid.UUID
    timestamp: str | None


class ProjectsProjectIdRunConfigurationsGet200Response(
    base.PaginationBaseModelWithUserRole
):
    run_configurations: list[RunConfiguration] | None


class ProjectsProjectIdRunConfigurationsGet201Response(base.BaseModel):
    run_configuration_id: uuid.UUID


Run.update_forward_refs()
AddRun.update_forward_refs()
RunConfiguration.update_forward_refs()
ProjectsProjectIdRunsGet200Response.update_forward_refs()
ProjectsProjectIdRunsGet201Response.update_forward_refs()
ProjectsProjectIdRunConfigurationsGet201Response.update_forward_refs()
