import io
import typing as t

import requests


def stream_response_chunks_to_string(response: requests.Response) -> str:
    return _write_chunks(response.iter_content(chunk_size=1024))


def stream_response_chunks_to_bytest_io(response: requests.Response) -> io.BytesIO:
    return chunks_to_bytes(response.iter_content(chunk_size=1024))


def byte_chunks_to_string(chunks: t.Iterator[bytes]) -> str:
    return _write_chunks(chunks)


def _write_chunks(chunks: t.Iterator[bytes], to_string: bool = True) -> str | bytes:
    content = chunks_to_bytes(chunks)
    result = content.read()
    if to_string:
        return result.decode("utf-8")
    return result


def chunks_to_bytes(chunks: t.Iterator[bytes]) -> io.BytesIO:
    content = io.BytesIO()
    for chunk in chunks:
        content.write(chunk)
    content.seek(0)
    return content
