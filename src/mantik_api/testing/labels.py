import typing as t
import uuid

import mantik_api.database as database

UPDATED_LABEL_ID = uuid.uuid4()
DELETED_LABEL_ID = uuid.uuid4()

DATABASE_LABELS_BEFORE_CRUD = (
    database.label.Label(
        id=UPDATED_LABEL_ID,
        identifier="0000",
        # Project scope should be added
        scopes=[database.label.Scope.Code, database.label.Scope.Experiment],
        # Label should be moved to Tasks category
        category=database.label.Category.Domain,
        # Label should be moved to Tabular category
        # Name should be updated to PCA
        name="test-label-1",
    ),
    # Label should be deleted since missing in YAML
    database.label.Label(
        id=DELETED_LABEL_ID,
        identifier="2000",
        scopes=[database.label.Scope.Project],
        category=database.label.Category.Datasets,
        name="test-label-2",
    ),
)

DATABASE_LABELS_REFLECTED_IN_YAML = (
    database.label.Label(
        id=UPDATED_LABEL_ID,
        identifier="0000",
        scopes=[
            database.label.Scope.Project,
            database.label.Scope.Code,
            database.label.Scope.Experiment,
        ],
        category=database.label.Category.Tasks,
        sub_category=database.label.SubCategory.Tabular,
        name="PCA",
    ),
    database.label.Label(
        id=UPDATED_LABEL_ID,
        identifier="0100",
        scopes=[
            database.label.Scope.Experiment,
        ],
        category=database.label.Category.Tasks,
        sub_category=database.label.SubCategory.Multimodal,
        name="K-Means",
    ),
    database.label.Label(
        id=uuid.uuid4(),
        identifier="1000",
        scopes=[
            database.label.Scope.Project,
            database.label.Scope.Data,
            database.label.Scope.Code,
        ],
        category=database.label.Category.Domain,
        name="Meteorology",
    ),
)


def assert_labels_crud_correct(
    client: database.client.main.Client,
    expected: t.Iterable[database.label.Label] | None = None,
) -> None:
    expected = expected or DATABASE_LABELS_REFLECTED_IN_YAML

    with client.session():
        database_state: database.client.main.GetAllResponse = client.get_all(
            database.label.Label
        )

        result = list(database_state.entities)

        assert all(
            label.id != DELETED_LABEL_ID for label in result
        ), "Label not deleted from database"

        assert_labels_equal(result, expected)


def assert_labels_equal(
    result: list[database.label.Label], expected: t.Iterable[database.label.Label]
) -> None:
    for res, exp in zip(result, expected, strict=True):
        assert res.identifier == exp.identifier, "Identifier not equal"
        assert res.scopes == exp.scopes, "Scopes not equal"
        assert res.category == exp.category, "Category not equal"
        assert res.sub_category == exp.sub_category, "SubCategory not equal"
        assert res.name == exp.name, "Name not equal"
