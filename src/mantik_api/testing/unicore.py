import io
import pathlib

import pyunicore.client
import pyunicore.helpers
import requests

import mantik_api.compute_backend.unicore as unicore


def submit_job(
    client: unicore.client.Client,
    job_description: pyunicore.helpers.jobs.Description,
    input_files: list[pathlib.Path] | None = None,
) -> unicore.job.Job:
    """Submit a job to UNICORE."""
    job = client._client.new_job(
        job_description=job_description.to_dict(),
        inputs=input_files or [],
        autostart=True,
    )
    return unicore.job.Job(job)


class FakeJob:
    def __init__(
        self,
        job_url: str = "test-job",
        properties: dict | None = None,
        existing_files: list[str] | None = None,
        will_be_successful: bool = True,
    ):
        self.url = job_url
        self._properties = properties or {
            "jobType": "BATCH",
            "status": "SUCCESSFUL",
            "log": ["<UNICORE logs that span several characters>"],
            "owner": "owner",
            "siteName": "siteName",
            "consumedTime": {
                "total": "1",
                "queued": "2",
                "stage-in": "3",
                "preCommand": "4",
                "main": "5",
                "postCommand": "6",
                "stage-out": "7",
            },
            "currentTime": "2021-11-04T10:39:44+0100",
            "submissionTime": "2021-11-04T10:39:44+0100",
            "terminationTime": "2021-11-04T10:39:44+0100",
            "statusMessage": "statusMessage",
            "tags": [],
            "resourceStatus": "resourceStatus",
            "name": "name",
            "exitCode": "0",
            "queue": "queue",
            "submissionPreferences": {},
            "resourceStatusMessage": "N/A",
            "acl": [],
            "batchSystemID": "N/A",
        }
        self._successful = will_be_successful
        self.working_dir = FakeWorkDir(files=existing_files)
        self.started = False

    @property
    def properties(self) -> dict:
        return self._properties

    @property
    def job_id(self) -> str:
        return self.url

    def bss_details(self) -> dict[str, str]:
        return {"detail": "value"}

    def poll(self) -> None:
        self._properties["status"] = "SUCCESSFUL" if self._successful else "FAILED"

    def abort(self) -> None:
        pass

    def start(self) -> None:
        self.started = True


class FakeClient:
    def __init__(
        self,
        raises: Exception | None = None,
        login_successful: bool = False,
    ):
        self._raises = raises
        self._properties = {"client": {"xlogin": {}}}
        if login_successful:
            self.add_login_info({"test_login": "test_logged_in"})

    def __call__(self, *args, **kwargs):
        if self._raises is not None:
            raise self._raises

    @property
    def properties(self) -> dict:
        return {**self.__dict__, **self._properties}

    def add_login_info(self, login: dict) -> None:
        self._properties["client"]["xlogin"] = login


class FakeFileReference(pyunicore.client.PathFile):
    def __init__(self, content: str, name: str = "file"):
        self._content = content
        self.name = name

    def raw(self) -> io.BytesIO:
        return io.BytesIO(self._content.encode("utf-8"))


class FakeDirReference(pyunicore.client.PathDir):
    def __init__(self, files: list[FakeFileReference] | None = None):
        self.storage = FakeWorkDir(files or [])
        self.name = "fakedir"


class FakeWorkDir:
    def __init__(self, files: list[str] | None = None):
        self.directory = "."
        self._files = files or ["mantik.log"]

    @property
    def properties(self):
        return {"foo": "bar"}

    def listdir(self, base: str = "/") -> dict[str, FakeFileReference]:
        return {
            f"{base}file1": FakeFileReference(content="File 1"),
            f"{base}file2": FakeFileReference(content="File 2"),
        }

    def stat(self, filename) -> FakeFileReference | FakeDirReference:
        if filename == "fake_dir":
            return FakeDirReference()
        elif filename == "file_does_not_exist" or filename not in self._files:
            raise unicore.exceptions.NotFoundException()
        return FakeFileReference(content=f"Stat {filename}")


class FakeJobWithoutDetails(FakeJob):
    def bss_details(self):
        raise requests.exceptions.HTTPError(
            "500 Server Error: Could not get job details: java.lang.Exception: Getting job details on TSI failed: reply was TSI_FAILED: Command 'scontrol show jobid 47' failed with code 1: b'slurm_load_jobs error: Invalid job id specified\n'"  # noqa: E501
        )
