import dataclasses

import starlette.status as status

BASE_MLFLOW_URL = "https://test.com"


@dataclasses.dataclass
class MlflowResponse:
    end_point: str
    method: str = "POST"
    status_code: int = status.HTTP_200_OK
    json: dict | None = None

    def to_dict(self) -> dict:
        return {
            "method": self.method,
            "url": f"{BASE_MLFLOW_URL}/api/2.0/mlflow/{self.end_point}",
            "status": self.status_code,
            "json": self.json,
        }


def endpoint_request_count(requests_mock, path: str) -> int:
    return sum(
        1
        for response in requests_mock.responses
        if path in response.url and response.call_count > 0
    )


class FakeResponse:
    def __init__(self, status_code: int):
        super().__init__()
        self._status_code = status_code

    @property
    def status_code(self) -> int:
        return self._status_code
