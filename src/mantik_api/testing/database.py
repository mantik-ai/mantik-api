from __future__ import annotations

import enum
import typing as t
import uuid

import sqlalchemy.dialects as dialects
import sqlalchemy.orm
import sqlalchemy.orm as orm

import mantik_api.database.base as base
import mantik_api.database.client.sessions as sessions
import mantik_api.database.columns as columns

MODEL_UUID_STR = "c3bda37e-7624-11ed-a1eb-0242ac120002"
MODEL_UUID = uuid.UUID(MODEL_UUID_STR)


class OrderBy(base.OrderByBase):
    created = enum.auto()

    @property
    def column(self) -> sqlalchemy.Column:
        match self:
            case OrderBy.created:
                return Model.created_at


class Model(base.BaseWithId):
    __tablename__ = "test_model_table"
    id = columns.create_id_column("test_model_id")
    test_column = sqlalchemy.Column(sqlalchemy.String, nullable=False)
    test_relationship = orm.relationship("TestRelationship", back_populates="model")


class TestRelationship(base.BaseWithId):
    __tablename__ = "test_relationship_table"
    id = columns.create_id_column("test_relationship_id")
    model_id = columns.create_foreign_key_column(
        name="model_id",
        column="test_model_id",
        primary_key=False,
        table="test_model_table",
    )
    model = sqlalchemy.orm.relationship(
        Model.__name__, back_populates="test_relationship"
    )


class FakeEngine:
    def __init__(self):
        pass

    def dispose(self):
        pass


class FakeSessionHandler(sessions.AbstractSessionHandler):
    def __init__(self, existing_models: dict[str, list[base.Base]] | None = None):
        self._engine = _create_mock_engine()
        self.sessionmaker = sqlalchemy.orm.sessionmaker(bind=self._engine)

        self.existing_models = existing_models or create_empty_tables_dict()
        self.added_models = create_empty_tables_dict()
        self.executed_queries: list[str] = []

    def __enter__(self) -> t.Self:
        self.begin()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def begin(self) -> t.Self:
        self.clear()
        return self

    def commit(self) -> None:
        pass

    def rollback(self) -> None:
        self.clear()

    def close(self) -> None:
        self.clear()

    def add(self, model: base.Base) -> None:
        self.added_models[model.table.key].append(model)

    def add_with_children(self, model: base.Base) -> None:
        return self.add(model)

    def overwrite(self, original_orm: base.Base, updated_orm: base.Base) -> None:
        pass

    def execute_query(self, query: sqlalchemy.orm.Query) -> None:
        self.executed_queries.append(compile_query(query))

    def get_one_or_none(self, query: sqlalchemy.orm.Query) -> base.BaseWithId | None:
        self.executed_queries.append(compile_query(query))
        table_name = _get_target_table_name_from_query(query)
        if self.existing_models[table_name]:
            [result] = self.existing_models[table_name]
            return result
        return

    def get_all(self, query: sqlalchemy.orm.Query) -> list[t.Any]:
        table_name = _get_target_table_name_from_query(query)
        self.executed_queries.append(compile_query(query))
        return self.existing_models[table_name]

    def get_count(self, query: sqlalchemy.orm.Query) -> int:
        table_name = _get_target_table_name_from_query(query)
        return len(self.existing_models[table_name])

    def clear(self):
        self.added_models = create_empty_tables_dict()
        self.executed_queries = []


def _create_mock_engine(
    uri: str = "postgresql://",
) -> sqlalchemy.engine.mock.MockConnection:
    def mock_executor(query, *args, **kwargs):  # noqa
        return query

    engine = sqlalchemy.create_mock_engine(uri, echo=True, executor=mock_executor)

    return engine


def create_empty_tables_dict(
    models: list[base.Base] | None = None,
):
    return {
        Model.__tablename__: models or [],
        TestRelationship.__tablename__: models or [],
    }


def compile_query(query: sqlalchemy.orm.Query) -> str:
    """Compile a given query to a string."""
    return str(
        query.compile(
            dialect=dialects.postgresql.dialect(),
            compile_kwargs={"literal_binds": True},
        )
    ).replace("\n", "")


def all_equal(left: list, right: list, attributes: list[str]) -> bool:
    return all(getattr(left, attr) == getattr(right, attr) for attr in attributes)


def _get_target_table_name_from_query(query: sqlalchemy.orm.Query) -> str:
    [table] = query._raw_columns
    return table.key
