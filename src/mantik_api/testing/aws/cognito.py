"""Fakes for testing cognito interactions.

Reference:
https://docs.aws.amazon.com/code-samples/latest/catalog/python-test_tools-cognito_idp_stubber.py.html  # noqa: E501

"""
import contextlib
import typing as t
import unittest.mock

import boto3
import botocore.session

import mantik_api.aws.cognito as cognito
import mantik_api.testing.aws._boto as _boto
import mantik_api.tokens.jwt as jwt
import mantik_api.utils as utils

USERNAME = "test-name"
COGNITO_USERNAME = "test-cognito-name"
PASSWORD = "Test-Password-123"
EMAIL = "test-email"

REGION = "eu-central-1"
APP_CLIENT_SECRET = "test-app-client-secret"


@contextlib.contextmanager
def cognito_env_vars_set(user_pool_id: str, user_pool_client_id: str) -> None:
    env_vars = {
        cognito.client.COGNITO_REGION_ENV_VAR: REGION,
        cognito.client.COGNITO_USER_POOL_ID_ENV_VAR: user_pool_id,
        cognito.client.COGNITO_APP_CLIENT_ID_ENV_VAR: user_pool_client_id,
        cognito.client.COGNITO_APP_CLIENT_SECRET_ENV_VAR: APP_CLIENT_SECRET,
    }

    with utils.env.env_vars_set(env_vars):
        yield


def get_access_token_and_credentials(
    username: str = USERNAME, password: str = PASSWORD
) -> tuple[jwt.JWT, cognito.credentials.CreateTokenCredentials]:
    """For a moto-mocked cognito-idp client, create a valid token."""
    credentials = cognito.credentials.CreateTokenCredentials(
        cognito_username=username, password=password
    )
    response_token = cognito.tokens._get_tokens_from_cognito(credentials)
    token = jwt.JWT.from_token(response_token["AuthenticationResult"]["AccessToken"])
    return token, credentials


def get_error_code_and_message_from_json(
    json_response: dict,
) -> tuple[str | None, str | None]:
    """Create error code and message from JSON for configuring patching of
    methods via `MotoCognitoPatcherForUnimplemtedApis`.
    """
    if "Error" in json_response:
        error_code = json_response["Error"]["Code"]
        error_message = json_response["Error"]["Message"]
        return error_code, error_message
    return None, None


class MotoCognitoPatcherForUnimplemtedApis:
    """A patcher that supplements the moto testing library.

    It allows to patch methods that are not implemented in moto.

    """

    def __init__(self):
        # Holds the configured patched responses as
        #
        # dict[
        #     <AWS operation name>,
        #    list[
        #        dict[<arguments to `_boto.BotoClientPatcher.patch_response()`]
        #    ]
        # ]
        #
        # E.g.
        #
        # ```python
        # {
        #     "DeleteUser": [
        #         {
        #             "method": "delete_user",
        #             "expected_params": {"AccessToken": "<JWT>"},
        #         },
        #    ],
        # }
        # ```
        self.patched: dict[str, list[dict[str, t.Any]]] = {}

        self._cognito_client = boto3.client("cognito-idp", region_name="test-region")

    @contextlib.contextmanager
    def enable(self) -> t.Self:
        """Enable mocking the botocore API call method.

        Patch the botocore method that performs the API calls to

        - get a patched response if it has been configured
        - call the moto API instead of no patched response configured.

        """
        original = botocore.client.BaseClient._make_api_call

        def mock_make_api_call(base_client, operation_name, kwargs):
            if self.operation_is_patched(operation_name):
                # If operation was configured to be patched,
                # return patched response.
                return self.get_patched_response(
                    operation_name=operation_name, **kwargs
                )
            # If we haven't patched the API, return original moto response.
            return original(base_client, operation_name, kwargs)

        with unittest.mock.patch(
            "botocore.client.BaseClient._make_api_call", new=mock_make_api_call
        ):
            yield self

    def operation_is_patched(self, operation_name: str) -> bool:
        return operation_name in self.patched

    def get_patched_response(self, operation_name: str, **kwargs):
        with _boto.BotoClientPatcher(client=self._cognito_client) as patcher:
            try:
                # Pop first patched response in queue
                patched_kwargs = self.patched[operation_name].pop(0)
            except KeyError:
                raise RuntimeError(
                    f"No patched response for operation {operation_name}"
                )
            except IndexError:
                raise RuntimeError(
                    f"All patched response already consumed for {operation_name}"
                )
            else:
                # Important: Delete operation of no patched response left in
                # queue to avoid recursion.
                if not self.patched[operation_name]:
                    self.patched.pop(operation_name)

            # Get method name to patch
            method = patched_kwargs.pop("method")

            # Patch the method
            patcher.patch_response(method=method, **patched_kwargs)

            return getattr(patcher.client, method)(**kwargs)

        patcher.assert_no_pending_responses()

    def patch_initiate_auth_get_token(
        self,
        password: str,
        response: dict,
        user_name: str = COGNITO_USERNAME,
        client_id: str | None = None,
        secret_hash: str | None = None,
        error_code: str | None = None,
        error_message: str | None = None,
    ):
        expected_params = {
            "ClientId": _get_client_id(client_id),
            "AuthFlow": "USER_PASSWORD_AUTH",
            "AuthParameters": {
                "USERNAME": user_name,
                "PASSWORD": password,
                "SECRET_HASH": _get_secret_hash(
                    username=user_name, secret_hash=secret_hash
                ),
            },
        }
        self._add_patched_response(
            operation_name="InitiateAuth",
            kwargs=dict(
                method="initiate_auth",
                expected_params=expected_params,
                response=response,
                error_code=error_code,
                error_message=error_message,
            ),
        )

    def patch_initiate_auth_refresh_token(
        self,
        refresh_token: str,
        response: dict,
        client_id: str | None = None,
        secret_hash: str | None = None,
        error_code: str | None = None,
        error_message: str | None = None,
    ):
        expected_params = {
            "ClientId": _get_client_id(client_id),
            "AuthFlow": "REFRESH_TOKEN_AUTH",
            "AuthParameters": {
                "REFRESH_TOKEN": refresh_token,
                "SECRET_HASH": _get_secret_hash(
                    username=COGNITO_USERNAME, secret_hash=secret_hash
                ),
            },
        }
        self._add_patched_response(
            operation_name="InitiateAuth",
            kwargs=dict(
                method="initiate_auth",
                expected_params=expected_params,
                response=response,
                error_code=error_code,
                error_message=error_message,
            ),
        )

    def patch_resend_confirmation_code(
        self,
        username: str = COGNITO_USERNAME,
        client_id: str | None = None,
        secret_hash: str | None = None,
        error_code: str | None = None,
        error_message: str | None = None,
    ):
        expected_params = {
            "ClientId": _get_client_id(client_id),
            "SecretHash": _get_secret_hash(username=username, secret_hash=secret_hash),
            "Username": username,
        }
        self._add_patched_response(
            operation_name="ResendConfirmationCode",
            kwargs=dict(
                method="resend_confirmation_code",
                expected_params=expected_params,
                error_code=error_code,
                error_message=error_message,
            ),
        )

    def patch_delete_user(
        self,
        token: str,
        error_code: str | None = None,
        error_message: str | None = None,
    ):
        expected_params = {"AccessToken": token}
        self._add_patched_response(
            operation_name="DeleteUser",
            kwargs=dict(
                method="delete_user",
                expected_params=expected_params,
                error_code=error_code,
                error_message=error_message,
            ),
        )

    def patch_confirm_sign_up_raises_error(
        self,
        confirmation_code: str,
        error_code: str,
        error_message: str,
        username: str = COGNITO_USERNAME,
        client_id: str | None = None,
        secret_hash: str | None = None,
    ):
        expected_params = {
            "ClientId": _get_client_id(client_id),
            "SecretHash": _get_secret_hash(username=username, secret_hash=secret_hash),
            "Username": username,
            "ConfirmationCode": confirmation_code,
        }
        self._add_patched_response(
            operation_name="ConfirmSignUp",
            kwargs=dict(
                method="confirm_sign_up",
                expected_params=expected_params,
                error_code=error_code,
                error_message=error_message,
            ),
        )

    def patch_verify_user_attribute(
        self,
        attribute_name: str,
        confirmation_code: str,
        token: str,
        error_code: str | None = None,
        error_message: str | None = None,
    ):
        expected_params = {
            "AttributeName": attribute_name,
            "Code": confirmation_code,
            "AccessToken": token,
        }

        self._add_patched_response(
            operation_name="VerifyUserAttribute",
            kwargs=dict(
                method="verify_user_attribute",
                expected_params=expected_params,
                error_code=error_code,
                error_message=error_message,
            ),
        )

    def _add_patched_response(self, operation_name: str, kwargs):
        """Add a patched response for the operation to the queue."""
        if operation_name not in self.patched:
            self.patched[operation_name] = []

        self.patched[operation_name].append(kwargs)


def _get_client_id(client_id: str | None) -> str:
    if client_id is None:
        # Get client ID from env var or return testing app client ID.
        return utils.env.get_required_env_var(
            cognito.client.COGNITO_APP_CLIENT_ID_ENV_VAR
        )
    return client_id


def _get_secret_hash(username: str, secret_hash: str | None) -> str:
    if secret_hash is None:
        # Assume secret hash was generated from Cognito env vars.
        return cognito.client.create_secret_hash(username=username)
    return secret_hash
