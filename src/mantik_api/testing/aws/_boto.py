import boto3
import botocore.stub


class BotoClientPatcher(botocore.stub.Stubber):
    def __init__(self, client: type[boto3.client]):
        """
        Initializes the object with a specific client and configures it for
        stubbing or AWS passthrough.

        Parameters
        ----------
        client : boto3.client
            A Boto 3 service client.

        """
        self.region_name = client.meta.region_name
        self.client = client
        super().__init__(client)

    @property
    def exceptions(self):
        return self.client.exceptions

    def add_response(
        self, method: str, service_response, expected_params: dict | None = None
    ):
        """Add response to a method.

        Parameters
        ----------
        method : str
            Name of the client method to stub.
        service_response : dict
            Response data.
        expected_params: : dict
            A dictionary of the expected parameters to
            be called for the provided service response. The parameters match
            the names of keyword arguments passed to that client call. If
            any of the parameters differ a ``StubResponseError`` is thrown.
            You can use stub.ANY to indicate a particular parameter to ignore
            in validation. stub.ANY is only valid for top level params.

        """
        super().add_response(
            method=method,
            service_response=service_response,
            expected_params=expected_params,
        )

    def add_client_error(
        self,
        method,
        service_error_code: str = "",
        service_message: str = "",
        http_status_code: int = 400,
        service_error_meta: dict | None = None,
        expected_params: dict | None = None,
        response_meta: dict | None = None,
        modeled_fields: dict | None = None,
    ):
        """Adds a `ClientError` to the response queue.

        Parameters
        ----------
        method : str
            The name of the service method to return the error on.
        service_error_code : str
            The service error code to return.
            E.g. ``NoSuchBucket``.
        service_message : str
            The service message to return.
            E.g. 'The specified bucket does not exist.'
        http_status_code : int
            The HTTP status code to return
            E.g. 404, etc.
        service_error_meta : dict
            Additional keys to be added to the service error.
        expected_params : dict
            A dictionary of the expected parameters to
            be called for the provided service response. The parameters match
            the names of keyword arguments passed to that client call. If
            any of the parameters differ a ``StubResponseError`` is thrown.
            You can use stub.ANY to indicate a particular parameter to ignore
            in validation.
        response_meta : dict
            Additional keys to be added to the response's ResponseMetadata.
        modeled_fields : dict
            Additional keys to be added to the response based on fields that
            are modeled for the particular error code. These keys will be
            validated against the particular error shape designated by the
            error code.

        """
        super().add_client_error(
            method,
            service_error_code=service_error_code,
            service_message=service_message,
            http_status_code=http_status_code,
            service_error_meta=service_error_meta,
            expected_params=expected_params,
            response_meta=response_meta,
            modeled_fields=modeled_fields,
        )

    def patch_response(
        self,
        method: str,
        expected_params: dict | None = None,
        response: dict | None = None,
        error_code: str | None = None,
        error_message: str | None = None,
    ):
        """Patch a response of the given client method.

        Parameters
        ----------
        method : str
            Name of a client method.

            Example: for `boto3.client("cognito-idp")` this might
            be `initiate_auth`.
        expected_params : dict, optional
            The expected parameters for the method invocation.

            Example: for `boto3.client("cognito-idp").get_user()`
            being invoked as

            ```python
            token = "<JWT token string>"
            client.get_user(AccessToken=token)
            ```

            in production code, this might be

            ```python
            expected_params={"AccessToken": "<JWT token string>"}
            ```
        response : dict, optional
            The response as returned by the AWS API.

            For examples, see `src/tests/resources/cognito`.
        error_code : str, optional
            Name of a client's error code to be raised.

            Example: for `boto3.client("cognito-idp").get_user()`
            (see https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/cognito-idp/client/get_user.html) # noqa
            we might want the client to raise
            `CognitoIdentityProvider.Client.exceptions.UserNotFoundException`,
            which is catched in code via

            ```python
            try:
                client.get_user(token)
            except: client.exceptions.UserNotFoundException:
                ...
            ```

            Then `error_code="UserNotFoundException"`.
        error_message : str, optional
            Error message to return in the response body
            in `response["Error"]["Message"]`.

        """
        if expected_params is None:
            expected_params = {}
        if response is None:
            response = {}
        if error_code is None:
            self.add_response(
                method,
                expected_params=expected_params,
                service_response=response,
            )
        else:
            self.add_client_error(
                method,
                expected_params=expected_params,
                service_error_code=error_code,
                service_message=error_message,
            )
