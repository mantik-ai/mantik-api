import enum
import io
import pathlib
import time
import typing as t
import uuid
from collections.abc import Sequence

import firecrest
import firecrest.types as types

import mantik_api.compute_backend.firecrest.job as _job
import mantik_api.database as database
import mantik_api.models.run_info as run_info

TEST_USER = "test-user"
TEST_JOB_ID = "test-job-id"
TEST_RUN_ID = uuid.uuid4()
TEST_MACHINE = "test-machine"

TEST_API_URL = "test-firecrest-api-url"
TEST_TOKEN_URL = "test-firecrest-token-url"

# The testing module for firecREST simulates the following folder structure:
#
# /home/test-user/mantik/<run UUID>/
# ├─ mantik.log
# ├─ sub/
# │  ├─ folder/
# │  │  ├─ test.txt
#

# Files and contents
TEST_FILE_NAME = "test.txt"
TEST_FILE_CONTENT = b"test\n"
TEST_LOGS_FILE_NAME = "mantik.log"
TEST_LOGS_CONTENT = b"hello\n"
TEST_RUN_DIR = pathlib.Path("/home/test-user/test-run-dir") / TEST_JOB_ID

# Paths
TEST_RUN_DIR = pathlib.Path(f"/home/{TEST_USER}/mantik/{TEST_RUN_ID}")
TEST_LOGS_FILE_PATH = TEST_RUN_DIR / TEST_LOGS_FILE_NAME
TEST_FILE_PATH = TEST_RUN_DIR / f"sub/folder/{TEST_FILE_NAME}"


# Variables required for local testing with firecREST microservices
#
# See https://github.com/eth-cscs/firecrest/blob/master/deploy/demo/demo_client/config.py  # noqa: E501
# for details.
# firecREST creates the job directories at
# `/home/service-account-firecrest-sample`.
#
# The secrets for local testing are given in the firecREST git repo:
#
# - Client ID, secret and token URL: `deploy/demo/demo_client/client_secrets.json`  # noqa: E501
# - Machine and queue (Partitions `part01`/`part02`): `deploy/demo/demo_client/config.py`  # noqa: E501
#
LOCAL_API_URL = "http://localhost:8000"
LOCAL_CLIENT_ID = "firecrest-sample"
LOCAL_CLIENT_SECRET = "b391e177-fa50-4987-beaf-e6d33ca93571"
LOCAL_TOKEN_URI = "http://localhost:8080/auth/realms/kcrealm/protocol/openid-connect/token"  # noqa: E501
LOCAL_MACHINE = "cluster"
LOCAL_PARTITION = "part01"
LOCAL_RUN_DIR = "/home/service-account-firecrest-sample/mantik/{run_id}"


class FakeLsFile(types.LsFile):
    content: str | None


TEST_FILES = {
    TEST_LOGS_FILE_PATH: types.LsFile(
        group="test-group",
        last_modified="2024-02-22T16:50:14",
        link_target="",
        name=TEST_LOGS_FILE_NAME,
        permissions="rw-rw-r--",
        size="5",
        type="-",
        user=TEST_USER,
        content=TEST_LOGS_CONTENT,
    ),
    TEST_FILE_PATH: types.LsFile(
        group="test-group",
        last_modified="2024-02-22T16:50:14",
        link_target="",
        name=TEST_FILE_NAME,
        permissions="rw-rw-r--",
        size="5",
        type="-",
        user=TEST_USER,
        content=TEST_FILE_CONTENT,
    ),
}
TEST_LS_RESPONSES = {
    str(TEST_RUN_ID): [
        types.LsFile(
            group="test-group",
            last_modified="2024-02-22T16:50:14",
            link_target="",
            name="sub",
            permissions="rwxrwxr-x",
            size="4096",
            type="d",
            user=TEST_USER,
            content=None,
        ),
        TEST_FILES[TEST_LOGS_FILE_PATH],
    ],
    "sub": [
        types.LsFile(
            group="test-group",
            last_modified="2024-02-22T16:50:14",
            link_target="",
            name="folder",
            permissions="rwxrwxr-x",
            size="4096",
            type="d",
            user=TEST_USER,
            content=None,
        ),
    ],
    "folder": [
        TEST_FILES[TEST_FILE_PATH],
    ],
}


class DownloadStatus(enum.StrEnum):
    SUCCESS = "117"
    FAILED = "118"


class FakeParameter(types.Parameter):
    pass


class FakeParameters(types.Parameters):
    pass


class FakeExternalDownload(firecrest.ExternalDownload):
    def __init__(self, status: DownloadStatus, content: str):
        self._status = status
        self._in_progress = iter([True, False])
        self._content = content

    def status(self) -> str:
        return self._status.value

    def finish_download(self, target_path: io.BytesIO) -> None:
        target_path.write(self._content)
        return None

    @property
    def in_progress(self) -> bool:
        """Return True, then False."""
        return next(self._in_progress)


class FakeJobSubmit(types.JobSubmit):
    pass


class FakeJobAcct(types.JobAcct):
    pass


class FakeClient(firecrest.Firecrest):
    def __init__(
        self,
        firecrest_url: str = TEST_API_URL,
        authorization: t.Any = "test-authorization",
        verify: str | bool | None = None,
        sa_role: str = "firecrest-sa",
        job_status: str = run_info.FirecrestStatus.COMPLETED,
        parameters: FakeParameters | None = None,
        download_status: DownloadStatus = DownloadStatus.SUCCESS,
        simple_download_fails: bool = False,
        poll_fails: bool = False,
    ):
        self._firecrest_url = firecrest_url
        self._authorization = authorization
        self._verify = verify
        self._sa_role = sa_role
        self._job_status = job_status
        self._download_status = download_status
        self._parameters = parameters
        self._simple_download_fails = simple_download_fails
        self._poll_fails = poll_fails

    def mkdir(self, machine: str, target_path: str, p: bool = None) -> None:
        return None

    def parameters(self) -> FakeParameters:
        return self._parameters or FakeParameters(
            storage=[
                FakeParameter(
                    name="FILESYSTEMS",
                    unit="",
                    value=[
                        {
                            "mounted": [
                                "/scratch/snx3000",
                                "/project",
                                "/store",
                            ],
                            "system": "daint",
                        },
                        # firecREST may return duplicate file systems if
                        # configured incorrectly from the HPC side.
                        {
                            "mounted": [
                                "/scratch/snx3000",
                                "/project",
                                "/store",
                            ],
                            "system": "daint",
                        },
                        {
                            "mounted": ["/capstor/scratch/cscs"],
                            "system": "eiger",
                        },
                        # Add machine without scratch but with home dir
                        {
                            "mounted": [
                                "/home",
                            ],
                            "system": TEST_MACHINE,
                        },
                    ],
                    description="",
                ),
                FakeParameter(
                    name="OBJECT_STORAGE",
                    unit="",
                    value="swift",
                    description="",
                ),
                FakeParameter(
                    name="STORAGE_TEMPURL_EXP_TIME",
                    unit="seconds",
                    value="604800",
                    description="",
                ),
                FakeParameter(
                    name="STORAGE_MAX_FILE_SIZE",
                    unit="MB",
                    value="1024000",
                    description="",
                ),
            ],
            utilities=[FakeParameter(name="", unit="", value="", description="")],
        )

    def whoami(self, machine: str | None = None) -> str | None:
        return TEST_USER

    def file_type(self, machine: str, target_path: str) -> str:
        path = pathlib.Path(target_path)
        if path in TEST_FILES:
            return "ASCII text"
        elif path.name in TEST_LS_RESPONSES:
            return "directory"
        raise ValueError(f"{target_path} not a test file or directory")

    def list_files(
        self, machine: str, target_path: str, show_hidden: bool = False
    ) -> list[types.LsFile]:
        last_element = target_path.split("/")[-1]
        return TEST_LS_RESPONSES[last_element]

    def simple_download(
        self,
        machine: str,
        source_path: str,
        target_path: str | pathlib.Path | io.BytesIO,
    ) -> None:
        """Fake simple downloading of a file.

        Currently only supports `io.BytesIO`.
        """
        if self._simple_download_fails:
            raise firecrest.FirecrestException("Fake simple download failed")

        path = pathlib.Path(source_path)
        try:
            file = TEST_FILES[path]
        except KeyError as e:
            raise ValueError(f"File {source_path} not found in test files") from e
        target_path.write(file["content"])

    def external_download(
        self,
        machine: str,
        source_path: str,
    ) -> FakeExternalDownload:
        path = pathlib.Path(source_path)
        try:
            file = TEST_FILES[path]
        except KeyError as e:
            raise ValueError(f"File {source_path} not found in test files") from e
        return FakeExternalDownload(
            status=self._download_status, content=file["content"]
        )

    def submit(
        self,
        machine: str,
        job_script: str | None = None,
        local_file: bool | None = True,
        script_str: str | None = None,
        script_local_path: str | None = None,
        script_remote_path: str | None = None,
        account: str | None = None,
        env_vars: dict[str, t.Any] | None = None,
    ) -> FakeJobSubmit:
        return FakeJobSubmit(
            firecrest_taskid="test-task-id-submit",
            job_data_err="",
            job_data_out="",
            job_file="",
            job_file_err="",
            job_file_out="",
            jobid="test-job-id",
            result="",
        )

    def poll(
        self,
        machine: str,
        jobs: Sequence[str | int] | None = None,
        start_time: str | None = None,
        end_time: str | None = None,
        page_size: int | None = None,
        page_number: int | None = None,
    ) -> list[FakeJobAcct]:
        if self._poll_fails:
            raise firecrest.FirecrestException("Fake poll failed")

        if self._job_status == run_info.FirecrestStatus.PENDING:
            return [
                FakeJobAcct(
                    jobid="test-job-id",
                    name="test-job",
                    nodelist="None assigned",
                    nodes="1",
                    partition="test-partition",
                    user="test-user",
                    exit_code="0:0",
                    cpu_time="00:00:00",
                    elapsed_time="00:00:00",
                    start_time="Unknown",
                    state=self._job_status,
                    termination_time="Unknown",
                    time="00:00:00",
                    time_left="Unknown",
                )
            ]
        elif self._job_status in [
            run_info.FirecrestStatus.CONFIGURING,
            run_info.FirecrestStatus.RUNNING,
            run_info.FirecrestStatus.COMPLETING,
        ]:
            return [
                FakeJobAcct(
                    jobid="test-job-id",
                    name="test-job",
                    nodelist="None assigned",
                    nodes="1",
                    partition="test-partition",
                    user="test-user",
                    exit_code="0:0",
                    cpu_time="00:08:48",
                    elapsed_time="00:00:11",
                    start_time="2023-01-01T00:00:00",
                    state=self._job_status,
                    termination_time="Unknown",
                    time="00:08:48",
                    time_left="Unknown",
                )
            ]
        elif self._job_status in [
            run_info.FirecrestStatus.COMPLETED,
            run_info.FirecrestStatus.FAILED,
            run_info.FirecrestStatus.CANCELLED,
            "CANCELLED by 1234",
        ]:
            return [
                FakeJobAcct(
                    jobid="test-job-id",
                    name="test-job",
                    nodelist="None assigned",
                    nodes="1",
                    partition="test-partition",
                    user="test-user",
                    exit_code="0:0",
                    cpu_time="00:12:00",
                    elapsed_time="00:00:15",
                    start_time="2023-01-01T00:00:00",
                    state=self._job_status,
                    termination_time="2023-01-01T00:00:12",
                    time="00:12:00",
                    time_left="2023-01-01T00:00:12",
                )
            ]
        else:
            ValueError(f"Invalid job status: {self._job_status}")

    def poll_active(
        self, machine: str, jobs: t.Sequence[t.Union[str, int]] = None
    ) -> list[firecrest.types.JobQueue]:
        if self._poll_fails:
            raise firecrest.FirecrestException("Fake poll failed")

        if self._job_status == [
            run_info.FirecrestStatus.PENDING,
            run_info.FirecrestStatus.CONFIGURING,
            run_info.FirecrestStatus.RUNNING,
            run_info.FirecrestStatus.COMPLETING,
        ]:
            return [
                {
                    "job_data_err": "",
                    "job_data_out": "",
                    "job_file": "/scratch/path/to/script.bath",
                    "job_file_err": "/scratch/path/to/mantik.log",
                    "job_file_out": "/scratch/path/to/mantik.log",
                    "job_info_extra": "Job info returned successfully",
                    "jobid": "test-job-id",
                    "name": "test-job",
                    "nodelist": "None assigned",
                    "nodes": "1",
                    "partition": "normal",
                    "start_time": "0:00",
                    "state": self._job_status,
                    "time": "N/A",
                    "time_left": "30:00",
                    "user": "test-user",
                }
            ]
        return []

    def cancel(self, machine: str, job_id: str | int) -> str:
        self._job_status = run_info.FirecrestStatus.CANCELLED
        return "Success"


class FakeClientCredentialsAuth:
    def __init__(
        self,
        client_id: str = TEST_USER,
        client_secret: str = "test-password",
        token_uri: str = TEST_TOKEN_URL,
        login_successful: bool = True,
    ):
        self._client_id = client_id
        self._client_secret = client_secret
        self._token_uri = token_uri
        if login_successful:
            self.login_successful = True
        else:
            self.login_successful = False

    def get_access_token(self) -> str:
        if self.login_successful:
            return "test-token"
        else:
            raise firecrest.ClientsCredentialsException(responses=["test-response"])


def submit_job(
    run_id: uuid.UUID | None = None,
) -> tuple[uuid.UUID, pathlib.Path, _job.Job]:
    """Submit a job that echos content to files.

    1. echos 'hello' to stdout/stderr (`mantik.log`)
    2. creates subdirectory `sub/folder`
    3. echos `test` to `sub/folder/test.txt`

    """
    run_id = run_id or uuid.uuid4()
    run_dir = pathlib.Path(LOCAL_RUN_DIR.format(run_id=run_id))

    client = firecrest.Firecrest(
        firecrest_url=LOCAL_API_URL,
        authorization=firecrest.ClientCredentialsAuth(
            client_id=LOCAL_CLIENT_ID,
            client_secret=LOCAL_CLIENT_SECRET,
            token_uri=LOCAL_TOKEN_URI,
        ),
    )

    client.mkdir(
        machine=LOCAL_MACHINE,
        target_path=run_dir.as_posix(),
        p=True,
    )

    script = [
        "#!/bin/bash",
        f"#SBATCH --partition={LOCAL_PARTITION}",
        f"#SBATCH --output={run_dir}/mantik.log",
        f"#SBATCH --error={run_dir}/mantik.log",
        f"cd {run_dir}",
        "echo hello",
        "mkdir -p sub/folder",
        "echo test > sub/folder/test.txt",
    ]
    job_submit = client.submit(
        machine=LOCAL_MACHINE,
        script_str="\n".join(script),
        account="doesntmatter",
    )

    job_id = str(job_submit["jobid"])
    job = _job.Job(
        client=client,
        machine=LOCAL_MACHINE,
        job_id=job_id,
        run_dir=run_dir,
    )

    return run_id, run_dir, job


def wait_until_job_is_finished(job: _job.Job) -> None:
    while job.get_status() in [
        database.run.RunStatus.SCHEDULED,
        database.run.RunStatus.RUNNING,
    ]:
        time.sleep(0.1)
