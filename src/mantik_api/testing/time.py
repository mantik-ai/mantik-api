import datetime
import functools
import typing as t


DEFAULT_CREATED_AT = datetime.datetime(2000, 1, 1, tzinfo=datetime.UTC)


def freeze_time(func, day: int):
    """Freeze the time to 2000-01-<day>."""
    # Import here to prevent crashing of app at runtime,
    # where freezegun is not installed
    import freezegun

    time = datetime.datetime(2000, 1, day)

    @functools.wraps(func)
    def freeze(*args, **kwargs):
        with freezegun.freeze_time(time):
            return func(*args, **kwargs)

    return freeze


def have_created_at(targets: t.Sequence[dict]) -> bool:
    """Checks ``createdAt`` fields are iso-formatted datetime and removes it."""
    return all(has_created_at(target) for target in targets)


def has_created_at(target: dict) -> bool:
    """Checks ``createdAt`` field is iso-formatted datetime and removes it."""
    return _has_isoformatted_datetime(target, key="createdAt")


def has_updated_at(target: dict) -> bool:
    """Checks ``updatedAt`` field is iso-formatted datetime and removes it."""
    return _has_isoformatted_datetime(target, key="updatedAt")


def _has_isoformatted_datetime(target: dict, key: str) -> bool:
    """Checks ``createdAt`` field is iso-formatted datetime and removes it."""
    try:
        datetime.datetime.fromisoformat(target.pop(key))
    except ValueError:
        return False
    return True
