import uuid

import mantik_api.tokens.jwt as jwt
import mantik_api.utils.mlflow._api as _api


def create_run(experiment_id: int, name: str, token: jwt.JWT) -> uuid.UUID:
    """Create an MLflow run on the tracking server.

    Parameters
    ----------
    experiment_id : int
        Experiment ID for which to create the run.
    name : str
        Name of the run.
    token : mantik_api.tokens.jwt.JWT
        Token to send in the request header to the MLflow Tracking Server.

    Returns
    -------
    uuid.UUID
        ID of the run.


    References
    ----------
    https://mlflow.org/docs/latest/rest-api.html#create-experiment

    """

    result = _api.send_post_request(
        url_endpoint="runs/create",
        data={"experiment_id": str(experiment_id), "run_name": name},
        token=token,
    ).json()
    return uuid.UUID(result["run"]["info"]["run_id"])
