def message_in_caplog(message: str, caplog) -> bool:
    return any(message in m for m in caplog.messages)
