import base64
import dataclasses
import hashlib
import hmac
import logging
import typing as t

import boto3

import mantik_api.utils.env as env


logger = logging.getLogger(__name__)

COGNITO_IDP_URL = "https://cognito-idp.{region}.amazonaws.com/{user_pool_id}"
COGNITO_REGION_ENV_VAR = "COGNITO_REGION"
COGNITO_USER_POOL_ID_ENV_VAR = "COGNITO_USER_POOL_ID"
COGNITO_APP_CLIENT_ID_ENV_VAR = "COGNITO_APP_CLIENT_ID"
COGNITO_APP_CLIENT_SECRET_ENV_VAR = "COGNITO_APP_CLIENT_SECRET"


@dataclasses.dataclass
class Properties:
    """The required Cognito User Pool client properties."""

    region: str
    user_pool_id: str
    app_client_id: str
    app_client_secret: str | None

    @classmethod
    def from_env(cls, secret_required: bool = True) -> t.Self:
        """Construct from environment variables.

        Parameters
        ----------
        secret_required : bool, default=True
            Whether the app client secret is required.
            It is required to issue tokens from the client API.
            It is not required for authenticating tokens.

        """
        return cls(
            region=env.get_required_env_var(COGNITO_REGION_ENV_VAR),
            user_pool_id=env.get_required_env_var(COGNITO_USER_POOL_ID_ENV_VAR),
            app_client_id=env.get_required_env_var(COGNITO_APP_CLIENT_ID_ENV_VAR),
            app_client_secret=_read_app_client_secret_from_env_var(
                required=secret_required
            ),
        )

    @property
    def jwks_file_url(self) -> str:
        """Return the JWKS file URL."""
        return f"{self.idp_url}/.well-known/jwks.json"

    @property
    def idp_url(self) -> str:
        """Return the respective IDP URL."""
        return COGNITO_IDP_URL.format(
            region=self.region,
            user_pool_id=self.user_pool_id,
        )


def _read_app_client_secret_from_env_var(required: bool) -> str | None:
    if required:
        return env.get_required_env_var(COGNITO_APP_CLIENT_SECRET_ENV_VAR)
    return env.get_optional_env_var(COGNITO_APP_CLIENT_SECRET_ENV_VAR)


def create_cognito_connection(
    properties: Properties | None = None, return_properties: bool = False
) -> boto3.client.__class__ | tuple[boto3.client.__class__, Properties]:
    if properties is None:
        properties = Properties.from_env()

    client = boto3.client("cognito-idp", region_name=properties.region)

    if return_properties:
        return client, properties
    return client


def create_secret_hash(username: str, properties: Properties | None = None) -> str:
    """Create a secret hash for secret-protected clients.

    Notes
    -----
    A keyed-hash message authentication code (HMAC) calculated using
    the secret key of a user pool client and username plus the client
    ID in the message.

    See
    https://aws.amazon.com/premiumsupport/knowledge-center/cognito-unable-to-verify-secret-hash/  # noqa: E501

    """
    if properties is None:
        properties = Properties.from_env()

    message = (username + properties.app_client_id).encode("UTF-8")
    key = properties.app_client_secret.encode("UTF-8")
    dig = hmac.new(
        key,
        msg=message,
        digestmod=hashlib.sha256,
    ).digest()
    return base64.b64encode(dig).decode()
