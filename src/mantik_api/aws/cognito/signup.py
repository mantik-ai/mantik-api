import logging

import mantik_api.aws.cognito.client as _client
import mantik_api.aws.cognito.exceptions as exceptions

logger = logging.getLogger(__name__)


def resend_confirmation_code(username: str) -> None:
    client, properties = _client.create_cognito_connection(return_properties=True)

    try:
        client.resend_confirmation_code(
            ClientId=properties.app_client_id,
            SecretHash=_client.create_secret_hash(
                username=username, properties=properties
            ),
            Username=username,
        )
    except (
        client.exceptions.ResourceNotFoundException,
        client.exceptions.InvalidParameterException,
        client.exceptions.UnexpectedLambdaException,
        client.exceptions.UserLambdaValidationException,
        client.exceptions.NotAuthorizedException,
        client.exceptions.InvalidLambdaResponseException,
        client.exceptions.TooManyRequestsException,
        client.exceptions.LimitExceededException,
        client.exceptions.InvalidSmsRoleAccessPolicyException,
        client.exceptions.InvalidSmsRoleTrustRelationshipException,
        client.exceptions.InvalidEmailRoleAccessPolicyException,
        client.exceptions.CodeDeliveryFailureException,
        client.exceptions.UserNotFoundException,
        client.exceptions.InternalErrorException,
        client.exceptions.ForbiddenException,
    ) as e:
        logger.info(
            "Failed to resend confirmation code via Cognito: %s",
            e.response,
            exc_info=True,
        )
        raise exceptions.ConfirmationCodeUserDetailsFailedException(
            f"Failed to resend confirmation code: {e.response['Error']['Message']}"
        ) from e
    finally:
        client.close()


def verify_email(username: str, confirmation_code: str) -> None:
    client, properties = _client.create_cognito_connection(return_properties=True)

    try:
        client.confirm_sign_up(
            ClientId=properties.app_client_id,
            SecretHash=_client.create_secret_hash(
                username=username, properties=properties
            ),
            Username=username,
            ConfirmationCode=confirmation_code,
        )
    except client.exceptions.CodeMismatchException as e:
        raise exceptions.IncorrectConfirmationCodeException(
            "Incorrect confirmation code"
        ) from e
    except (
        client.exceptions.ResourceNotFoundException,
        client.exceptions.InvalidParameterException,
        client.exceptions.UnexpectedLambdaException,
        client.exceptions.UserLambdaValidationException,
        client.exceptions.NotAuthorizedException,
        client.exceptions.TooManyFailedAttemptsException,
        client.exceptions.ExpiredCodeException,
        client.exceptions.InvalidLambdaResponseException,
        client.exceptions.AliasExistsException,
        client.exceptions.TooManyRequestsException,
        client.exceptions.LimitExceededException,
        client.exceptions.UserNotFoundException,
        client.exceptions.InternalErrorException,
        client.exceptions.ForbiddenException,
    ) as e:
        logger.info(
            "Failed to verify user email via Cognito: %s",
            e.response,
            exc_info=True,
        )
        raise exceptions.EmailVerificationFailedException(
            f"Failed to verify email: {e.response['Error']['Message']}"
        ) from e
    finally:
        client.close()
