import abc
import dataclasses


@dataclasses.dataclass(frozen=True)
class Credentials(abc.ABC):
    """Credentials required for authentication.

    Parameters
    ----------
    cognito_username : str
        Name of the user.

    """

    cognito_username: str

    @property
    @abc.abstractmethod
    def auth_flow(self) -> str:
        """Return the auth flow type for these credentials."""

    @abc.abstractmethod
    def to_auth_parameters(self) -> dict:
        """Return as auth parameters as required by boto3."""


@dataclasses.dataclass(frozen=True)
class CreateTokenCredentials(Credentials):
    """Credentials required for user-password authentication.

    Parameters
    ----------
    password : str
        Password of the user.

    """

    password: str

    def __repr__(self) -> str:
        """Override to avoid password being shown."""
        return f"{self.__class__.__name__}({self.cognito_username})"

    def __str__(self) -> str:
        return self.__repr__()

    @property
    def auth_flow(self) -> str:
        return "USER_PASSWORD_AUTH"

    def to_auth_parameters(self) -> dict:
        """Return as auth parameters as required by boto3."""
        return {
            "USERNAME": self.cognito_username,
            "PASSWORD": self.password,
        }


@dataclasses.dataclass(frozen=True)
class RefreshTokenCredentials(Credentials):
    """Credentials required for refresh token authentication.

    Parameters
    ----------
    refresh_token : str
        The refresh token.

    """

    refresh_token: str

    @property
    def auth_flow(self) -> str:
        return "REFRESH_TOKEN_AUTH"

    def to_auth_parameters(self) -> dict:
        """Return as auth parameters as required by boto3."""
        return {
            "REFRESH_TOKEN": self.refresh_token,
        }
