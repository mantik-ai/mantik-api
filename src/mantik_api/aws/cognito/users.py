import dataclasses
import logging
import typing as t

import mantik_api.aws.cognito.client as _client
import mantik_api.aws.cognito.exceptions as exceptions
import mantik_api.tokens.jwt as jwt


logger = logging.getLogger(__name__)


@dataclasses.dataclass
class Attribute:
    name: str
    value: str

    @classmethod
    def from_json(cls, data: dict) -> t.Self:
        return cls(
            name=data["Name"],
            value=data["Value"],
        )


@dataclasses.dataclass
class GetUserResponse:
    user_name: str
    user_attributes: list[Attribute] = dataclasses.field(default_factory=list)
    mfa_options: list[dict[str, str]] = dataclasses.field(default_factory=list)
    preferred_mfa_settings: str = ""
    user_mfa_setting_list: list[str] = dataclasses.field(default_factory=list)

    @classmethod
    def from_json(cls, data: dict) -> t.Self:
        return cls(
            user_name=data["Username"],
            user_attributes=(
                list(map(Attribute.from_json, data["UserAttributes"]))
                if "UserAttributes" in data
                else []
            ),
            mfa_options=data["MFAOptions"] if "MFAOptions" in data else [],
            preferred_mfa_settings=(
                data["PreferredMfaSetting"] if "PreferredMfaSetting" in data else ""
            ),
            user_mfa_setting_list=(
                data["UserMFASettingList"] if "UserMFASettingList" in data else []
            ),
        )

    def get_attribute(self, name: str) -> str:
        """Get a specific user attribute.

        Examples
        --------
        Cognito returns the user attributes as a list of JSON objects with
        Name and Value fields.
        ```json
        [
            {
                'Name': 'string',
                'Value': 'string'
            },
        ]
        ```

        Resources
        ---------
        https://docs.aws.amazon.com/cognito/latest/developerguide/user-pool-settings-attributes.html  # noqa: E501

        """
        names = [attribute.name for attribute in self.user_attributes]
        if name not in names:
            raise LookupError(f"User attributes missing {name!r}: {self}")
        value = self.user_attributes[names.index(name)].value
        return value


def create_cognito_user(username: str, email: str, password: str) -> str:
    client, properties = _client.create_cognito_connection(return_properties=True)
    try:
        response = client.sign_up(
            ClientId=properties.app_client_id,
            Username=username,
            Password=password,
            UserAttributes=[{"Name": "email", "Value": email}],
            SecretHash=_client.create_secret_hash(
                username=username, properties=properties
            ),
        )
    except (
        client.exceptions.ResourceNotFoundException,
        client.exceptions.InvalidParameterException,
        client.exceptions.UnexpectedLambdaException,
        client.exceptions.UserLambdaValidationException,
        client.exceptions.NotAuthorizedException,
        client.exceptions.InvalidPasswordException,
        client.exceptions.InvalidLambdaResponseException,
        client.exceptions.TooManyRequestsException,
        client.exceptions.InternalErrorException,
        client.exceptions.InvalidSmsRoleAccessPolicyException,
        client.exceptions.InvalidSmsRoleTrustRelationshipException,
        client.exceptions.InvalidEmailRoleAccessPolicyException,
        client.exceptions.CodeDeliveryFailureException,
        client.exceptions.ForbiddenException,
    ) as e:
        logger.exception("Signup failed", exc_info=True)
        raise exceptions.UserCreationFailed(f"Unable to create user: {str(e)}")
    except client.exceptions.UsernameExistsException:
        logger.exception("Signup failed", exc_info=True)
        raise exceptions.UsernameAlreadyExists("Username is already taken.")
    finally:
        client.close()

    return response["UserSub"]


def get_cognito_user(token: jwt.JWT) -> GetUserResponse:
    """Get a user and their details.

    Parameters
    ----------
    token : mantik_api.tokens.jwt.JWT
        The AccessToken issued to the user by Cognito.

        This is equivalent to the token a user uses to access the mantik API.

    Returns
    -------
    GetUserResponse
        Example:

        ```json
        {
            'Username': 'string',
            'UserAttributes': [
                {
                    'Name': 'string',
                    'Value': 'string'
                },
            ],
            'MFAOptions': [
                {
                    'DeliveryMedium': 'SMS'|'EMAIL',
                    'AttributeName': 'string'
                },
            ],
            'PreferredMfaSetting': 'string',
            'UserMFASettingList': [
                'string',
            ]
        }
        ```

    """
    client = _client.create_cognito_connection()
    try:
        response = client.get_user(AccessToken=token.to_string())
    except (
        client.exceptions.ResourceNotFoundException,
        client.exceptions.InvalidParameterException,
        client.exceptions.NotAuthorizedException,
        client.exceptions.TooManyRequestsException,
        client.exceptions.PasswordResetRequiredException,
        client.exceptions.UserNotFoundException,
        client.exceptions.UserNotConfirmedException,
        client.exceptions.InternalErrorException,
        client.exceptions.ForbiddenException,
    ) as e:
        logger.exception(
            "Failed to get Cognito user with ID %s from token %s",
            token.user_id,
            token.to_string(),
            exc_info=True,
        )
        raise exceptions.UserFetchingFailedException(
            "Unable to get user from Cognito"
        ) from e
    finally:
        client.close()

    return GetUserResponse.from_json(response)


def delete_cognito_user(token: jwt.JWT) -> None:
    """Delete a user.

    Parameters
    ----------
    token : mantik_api.tokens.jwt.JWT
        The AccessToken issued to the user by Cognito.

        This is equivalent to the token a user uses to access the mantik API.

    """
    client = _client.create_cognito_connection()
    token_str = token.to_string()
    try:
        client.delete_user(AccessToken=token_str)
    except (
        client.exceptions.ResourceNotFoundException,
        client.exceptions.InvalidParameterException,
        client.exceptions.NotAuthorizedException,
        client.exceptions.TooManyRequestsException,
        client.exceptions.PasswordResetRequiredException,
        client.exceptions.UserNotFoundException,
        client.exceptions.UserNotConfirmedException,
        client.exceptions.InternalErrorException,
        client.exceptions.ForbiddenException,
    ) as e:
        logger.exception("Deleting a user at Cognito failed", exc_info=True)
        raise exceptions.UserDeletionFailedException(f"Unable to delete user: {str(e)}")
    finally:
        client.close()


def change_password_cognito_user(
    old_password: str, new_password: str, token: jwt.JWT
) -> None:
    """Change the password of a Cognito user.

    Parameters
    ----------
    old_password : str
        Old password.
    new_password : str
        New password.

        Will be validation by Cognito to conform to the
        specified criteria of the User Pool.
    token : mantik_api.tokens.jwt.JWT
        The AccessToken issued to the user by Cognito.

        This is equivalent to the token a user uses to access the mantik API.

    """
    client = _client.create_cognito_connection()
    try:
        client.change_password(
            PreviousPassword=old_password,
            ProposedPassword=new_password,
            AccessToken=token.to_string(),
        )
    except client.exceptions.NotAuthorizedException as e:
        logger.info(
            "Updating the user password at Cognito failed: %s",
            e.response,
            exc_info=True,
        )
        raise exceptions.UserPasswordUpdateFailedException(
            f"Unable to change user password: incorrect password ({str(e)})."
        ) from e
    except (
        client.exceptions.ResourceNotFoundException,
        client.exceptions.InvalidParameterException,
        client.exceptions.InvalidPasswordException,
        client.exceptions.TooManyRequestsException,
        client.exceptions.LimitExceededException,
        client.exceptions.PasswordResetRequiredException,
        client.exceptions.UserNotFoundException,
        client.exceptions.UserNotConfirmedException,
        client.exceptions.InternalErrorException,
        client.exceptions.ForbiddenException,
    ) as e:
        logger.info(
            "Updating the user password at Cognito failed: %s",
            e.response,
            exc_info=True,
        )
        raise exceptions.UserPasswordUpdateFailedException(
            f"Unable to change user password: {e.response['Error']['Message']}"
        ) from e
    finally:
        client.close()


def initiate_change_email_cognito_user(new_email: str, token: jwt.JWT) -> None:
    """Initiates the update process for the user email.

    Our Cognito is configured to keep the old email until the new email
    has been actively verified by the user.

    Parameters
    ----------
    new_email : str
        The new email for the user account.
    token : mantik_api.tokens.jwt.JWT
        The AccessToken issued to the user by Cognito.

        This is equivalent to the token a user uses to access the mantik API.

    """
    client = _client.create_cognito_connection()
    try:
        client.update_user_attributes(
            UserAttributes=[
                {
                    "Name": "email",
                    "Value": new_email,
                }
            ],
            AccessToken=token.to_string(),
        )
    except (
        client.exceptions.ResourceNotFoundException,
        client.exceptions.InvalidParameterException,
        client.exceptions.CodeMismatchException,
        client.exceptions.ExpiredCodeException,
        client.exceptions.NotAuthorizedException,
        client.exceptions.UnexpectedLambdaException,
        client.exceptions.UserLambdaValidationException,
        client.exceptions.InvalidLambdaResponseException,
        client.exceptions.TooManyRequestsException,
        client.exceptions.AliasExistsException,
        client.exceptions.InvalidEmailRoleAccessPolicyException,
        client.exceptions.CodeDeliveryFailureException,
        client.exceptions.PasswordResetRequiredException,
        client.exceptions.UserNotFoundException,
        client.exceptions.UserNotConfirmedException,
        client.exceptions.InternalErrorException,
        client.exceptions.ForbiddenException,
    ) as e:
        logger.info(
            "Updating the user email at Cognito failed: %s",
            e.response,
            exc_info=True,
        )
        raise exceptions.UserEmailUpdateFailedException(
            f"Unable to update email: {e.response['Error']['Message']}"
        ) from e
    finally:
        client.close()


def verify_updated_email(confirmation_code: str, token: jwt.JWT) -> None:
    """Verifies an updated email.

    Our Cognito is configured to keep the old email until the new email
    has been actively verified by the user.

    Parameters
    ----------
    confirmation_code : str
        The confirmation code sent to the user via email
    token : mantik_api.tokens.jwt.JWT
        The AccessToken issued to the user by Cognito.

        This is equivalent to the token a user uses to access the mantik API.

    """
    client = _client.create_cognito_connection()
    try:
        client.verify_user_attribute(
            AttributeName="email",
            Code=confirmation_code,
            AccessToken=token.to_string(),
        )
    except client.exceptions.CodeMismatchException as e:
        raise exceptions.IncorrectConfirmationCodeException(
            "Invalid confirmation code"
        ) from e
    except (
        client.exceptions.ResourceNotFoundException,
        client.exceptions.InvalidParameterException,
        client.exceptions.ExpiredCodeException,
        client.exceptions.NotAuthorizedException,
        client.exceptions.TooManyRequestsException,
        client.exceptions.LimitExceededException,
        client.exceptions.PasswordResetRequiredException,
        client.exceptions.UserNotFoundException,
        client.exceptions.UserNotConfirmedException,
        client.exceptions.InternalErrorException,
        client.exceptions.AliasExistsException,
        client.exceptions.ForbiddenException,
    ) as e:
        logger.info(
            "Verifying updated email at Cognito failed: %s",
            e.response,
            exc_info=True,
        )
        raise exceptions.EmailVerificationFailedException(
            f"Failed to verify updated email: {e.response['Error']['Message']}"
        ) from e
    finally:
        client.close()


def initiate_forgot_password(username: str) -> dict[str, str]:
    client, properties = _client.create_cognito_connection(return_properties=True)
    try:
        return client.forgot_password(
            ClientId=properties.app_client_id,
            Username=username,
            SecretHash=_client.create_secret_hash(
                username=username, properties=properties
            ),
        )
    except (
        client.exceptions.ResourceNotFoundException,
        client.exceptions.InvalidParameterException,
        client.exceptions.UnexpectedLambdaException,
        client.exceptions.UserLambdaValidationException,
        client.exceptions.NotAuthorizedException,
        client.exceptions.InvalidLambdaResponseException,
        client.exceptions.TooManyRequestsException,
        client.exceptions.LimitExceededException,
        client.exceptions.InvalidSmsRoleAccessPolicyException,
        client.exceptions.InvalidSmsRoleTrustRelationshipException,
        client.exceptions.InvalidEmailRoleAccessPolicyException,
        client.exceptions.CodeDeliveryFailureException,
        client.exceptions.UserNotFoundException,
        client.exceptions.InternalErrorException,
        client.exceptions.ForbiddenException,
    ) as e:
        logger.info(
            "User password recovery attempt failed: %s",
            e.response,
            exc_info=True,
        )
        raise exceptions.UserForgotPasswordFailedException(
            f"Unable to initiate password recovery: {e.response['Error']['Message']}"
        ) from e
    finally:
        client.close()


def confirm_forgot_password(
    username: str, confirmation_code: str, new_password: str
) -> None:
    client, properties = _client.create_cognito_connection(return_properties=True)
    try:
        client.confirm_forgot_password(
            ClientId=properties.app_client_id,
            Username=username,
            SecretHash=_client.create_secret_hash(
                username=username, properties=properties
            ),
            ConfirmationCode=confirmation_code,
            Password=new_password,
        )
    except (
        client.exceptions.ResourceNotFoundException,
        client.exceptions.UnexpectedLambdaException,
        client.exceptions.UserLambdaValidationException,
        client.exceptions.InvalidParameterException,
        client.exceptions.InvalidPasswordException,
        client.exceptions.NotAuthorizedException,
        client.exceptions.CodeMismatchException,
        client.exceptions.ExpiredCodeException,
        client.exceptions.TooManyFailedAttemptsException,
        client.exceptions.InvalidLambdaResponseException,
        client.exceptions.TooManyRequestsException,
        client.exceptions.LimitExceededException,
        client.exceptions.UserNotFoundException,
        client.exceptions.UserNotConfirmedException,
        client.exceptions.InternalErrorException,
        client.exceptions.ForbiddenException,
    ) as e:
        logger.info(
            "User password reset attempt failed: %s",
            e.response,
            exc_info=True,
        )
        raise exceptions.UserConfirmForgotPasswordFailedException(
            f"Unable to reset password: {e.response['Error']['Message']}"
        ) from e
    finally:
        client.close()
