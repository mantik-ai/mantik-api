import dataclasses
import datetime
import email.utils
import functools
import logging
import typing as t

import mantik_api.aws.cognito.client as _client
import mantik_api.aws.cognito.credentials as _credentials
import mantik_api.aws.cognito.exceptions as exceptions

logger = logging.getLogger(__name__)


@dataclasses.dataclass(frozen=True)
class Tokens:
    """Holds AWS Cognito auth tokens."""

    access_token: str
    refresh_token: str
    expires_at: datetime.datetime
    __encoding = "utf-8"

    @classmethod
    def from_json_response(
        cls, response: dict, refresh_token: str | None = None
    ) -> t.Self:
        """Create from JSON response."""
        auth_result = response["AuthenticationResult"]
        access_token = auth_result["AccessToken"]
        if refresh_token is None:
            refresh_token = auth_result["RefreshToken"]
        expires_at = _calculate_expiration_date_from_response(response)
        return cls(
            access_token=access_token,
            refresh_token=refresh_token,
            expires_at=expires_at,
        )


def _calculate_expiration_date_from_response(
    response: dict,
) -> datetime.datetime:
    expires_in_s = response["AuthenticationResult"]["ExpiresIn"]
    response_metadata_https_headers = response["ResponseMetadata"]["HTTPHeaders"]
    try:
        created_at_str = response_metadata_https_headers["date"]
    except KeyError:
        # When testing with moto, response HTTPHeaders
        # doesn't contain date
        created_at = datetime.datetime.now()
    else:
        created_at = email.utils.parsedate_to_datetime(created_at_str)
    expires_at = created_at + datetime.timedelta(seconds=expires_in_s)
    return expires_at


@functools.singledispatch
def get_tokens(credentials: _credentials.Credentials) -> Tokens:
    """Get the required tokens from the Cognito API."""
    return NotImplemented


@get_tokens.register
def _get_tokens(credentials: _credentials.CreateTokenCredentials) -> Tokens:
    response = _get_tokens_from_cognito(credentials)
    return Tokens.from_json_response(response)


@get_tokens.register
def _refresh_tokens(credentials: _credentials.RefreshTokenCredentials) -> Tokens:
    response = _get_tokens_from_cognito(credentials)
    return Tokens.from_json_response(
        response,
        refresh_token=credentials.refresh_token,
    )


def _get_tokens_from_cognito(credentials: _credentials.Credentials) -> dict:
    client, properties = _client.create_cognito_connection(return_properties=True)

    secret_hash = _client.create_secret_hash(
        username=credentials.cognito_username,
        properties=properties,
    )
    auth_parameters = {
        **credentials.to_auth_parameters(),
        "SECRET_HASH": secret_hash,
    }

    try:
        response = client.initiate_auth(
            ClientId=properties.app_client_id,
            AuthFlow=credentials.auth_flow,
            AuthParameters=auth_parameters,
        )
    except client.exceptions.NotAuthorizedException as e:
        message = e.response["Error"]["Message"]
        if "invalid refresh token" in message.lower():
            raise exceptions.AuthenticationFailedException(
                exceptions.REFRESH_TOKEN_INVALID_ERROR_MESSAGE
            )
        elif "refresh token has different client" in message.lower():
            raise exceptions.AuthenticationFailedException(
                exceptions.REFRESH_TOKEN_INVALID_ERROR_MESSAGE
            )
        raise exceptions.AuthenticationFailedException(message)
    except (
        client.exceptions.ResourceNotFoundException,
        client.exceptions.InvalidParameterException,
        client.exceptions.NotAuthorizedException,
        client.exceptions.TooManyRequestsException,
        client.exceptions.UnexpectedLambdaException,
        client.exceptions.InvalidUserPoolConfigurationException,
        client.exceptions.UserLambdaValidationException,
        client.exceptions.InvalidLambdaResponseException,
        client.exceptions.PasswordResetRequiredException,
        client.exceptions.UserNotFoundException,
        client.exceptions.UserNotConfirmedException,
        client.exceptions.InternalErrorException,
        client.exceptions.InvalidSmsRoleAccessPolicyException,
        client.exceptions.InvalidSmsRoleTrustRelationshipException,
        client.exceptions.ForbiddenException,
    ) as e:
        logger.exception("Failed to retrieve tokens via %s", credentials, exc_info=True)
        raise exceptions.AuthenticationFailedException(e.response["Error"]["Message"])
    finally:
        client.close()

    return response
