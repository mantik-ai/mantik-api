REFRESH_TOKEN_EXPIRED_ERROR_MESSAGE = "Refresh token has expired"
REFRESH_TOKEN_INVALID_ERROR_MESSAGE = "Refresh token is invalid"


class UserCreationFailed(Exception):
    """User creation (sign up) has failed."""


class UsernameAlreadyExists(Exception):
    """Username already exists."""


class UserFetchingFailedException(Exception):
    """Failed to get the user from the Cognito User Pool."""


class AuthenticationFailedException(Exception):
    """Authentication has failed."""


class UserDeletionFailedException(Exception):
    """Deleting a user at the Cognito instance failed."""


class UserEmailUpdateFailedException(Exception):
    """Updating the user's email at the Cognito instance failed."""


class UserPasswordUpdateFailedException(Exception):
    """Updating the user's password at the Cognito instance failed."""


class UserForgotPasswordFailedException(Exception):
    """User password recovery attempt failed."""


class UserConfirmForgotPasswordFailedException(Exception):
    """User password reset attempt failed."""


class EmailVerificationFailedException(Exception):
    """Email verification failed."""


class ConfirmationCodeUserDetailsFailedException(Exception):
    """Failed to resend the confirmation code for signup."""


class IncorrectConfirmationCodeException(Exception):
    """Email verification failed due to incorrect confirmation code."""
