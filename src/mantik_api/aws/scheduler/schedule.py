import dataclasses
import datetime
import json
import uuid

import pytz

import mantik_api.tokens.jwt as jwt


@dataclasses.dataclass(frozen=True)
class PropertiesBase:
    id: uuid.UUID

    @property
    def aws_name(self) -> str:
        """Use ID as AWS resource name.

        Notes
        -----
        The AWS resource name must be unique and is used as a sort-of ID to
        update/delete the schedule. Hence, we use the ID of the
        RunSchedule in our DB.

        """

        return f"{self.id}"


@dataclasses.dataclass(frozen=True)
class Properties(PropertiesBase):
    name: str
    project_id: uuid.UUID
    run_id: uuid.UUID
    token: jwt.JWT
    connection_id: uuid.UUID
    compute_budget_account: str
    cron_expression: str
    time_zone: str
    end_date: datetime.datetime
    start_date: datetime.datetime | None = None

    def __post_init__(self):
        """Set start date and ensure correct time zone."""
        start_date = datetime.datetime.now(tz=self.tz)
        object.__setattr__(self, "start_date", start_date)

        ensure_time_zone = self.end_date.astimezone(self.tz)
        object.__setattr__(self, "end_date", ensure_time_zone)

    @property
    def schedule_expression(self) -> pytz.timezone:
        """Return schedule expression (Cron expression).

        Notes
        -----
        AWS uses a cron expression with 6

        """
        return f"cron({self.cron_expression} *)"

    @property
    def tz(self) -> pytz.timezone:
        return pytz.timezone(self.time_zone)

    @property
    def input(self) -> str:
        """Create the input that is sent to the Lambda.

        Notes
        -----
        The body must conform to expected request body from the Lambda function.

        """
        body = {
            "projectId": str(self.project_id),
            "runId": str(self.run_id),
            "token": self.token.to_string(),
            "connectionId": str(self.connection_id),
            "computeBudgetAccount": self.compute_budget_account,
        }
        return json.dumps(body)


@dataclasses.dataclass(frozen=True)
class PropertiesDelete(PropertiesBase):
    """Properties required to delete a schedule."""
