import logging
import re
import typing as t

import boto3

import mantik_api.aws.scheduler.schedule as schedule
import mantik_api.utils.env as env

logger = logging.getLogger(__name__)

DEAD_LETTER_QUEUE_ARN_ENV_VAR = "RUN_SCHEDULER_DEAD_LETTER_QUEUE_ARN"
TARGET_ARN_ENV_VAR = "RUN_SCHEDULER_LAMBDA_ARN"
AWS_ACCESS_KEY_ID_ENV_VAR = "RUN_SCHEDULER_ACCESS_KEY_ID"
AWS_SECRET_ACCESS_KEY_ENV_VAR = "RUN_SCHEDULER_SECRET_ACCESS_KEY"
AWS_REGION_ENV_VAR = "RUN_SCHEDULER_REGION"
ROLE_ARN_ENV_VAR = "RUN_SCHEDULER_ROLE_ARN"


class SchedulerActionFailedException(Exception):
    """Attempt to CRUD a schedule failed."""


class Client:
    """Client for AWS EventBridge Scheduler.

    References
    ----------
    https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/scheduler.html  # noqa: E501

    """

    def __init__(self):
        self._target_arn = env.get_required_env_var(TARGET_ARN_ENV_VAR)
        self._role_arn = env.get_required_env_var(ROLE_ARN_ENV_VAR)
        self._dead_letter_queue_arn = env.get_required_env_var(
            DEAD_LETTER_QUEUE_ARN_ENV_VAR
        )
        self._client = None

    def __enter__(self) -> t.Self:
        self._client = boto3.client(
            "scheduler",
            region_name=env.get_required_env_var(AWS_REGION_ENV_VAR),
            aws_access_key_id=env.get_required_env_var(AWS_ACCESS_KEY_ID_ENV_VAR),
            aws_secret_access_key=env.get_required_env_var(
                AWS_SECRET_ACCESS_KEY_ENV_VAR
            ),
        )
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._client.close()
        self._client = None

    def create(self, properties: schedule.Properties) -> str:
        """Create schedule.

        Parameters
        ----------
        properties : RunScheduleProperties
            Properties of the run schedule.


        Returns
        -------
        str
            AWS resource ARN of the schedule.

        """
        logger.debug("Create schedule %s", properties)
        try:
            response = self._client.create_schedule(
                **self._create_schedule_kwargs(properties)
            )
        except (
            self._client.exceptions.ServiceQuotaExceededException,
            self._client.exceptions.ValidationException,
            self._client.exceptions.InternalServerException,
            self._client.exceptions.ConflictException,
            self._client.exceptions.ResourceNotFoundException,
            self._client.exceptions.ThrottlingException,
        ) as e:
            message = e.response["Error"]["Message"]
            logger.exception(
                "Adding run schedule to EventBridge Scheduler failed (%s): %s",
                e.response,
                message,
            )
            _process_response_error_message(message=message, properties=properties)
            raise SchedulerActionFailedException("Failed to create schedule")
        return response["ScheduleArn"]

    def update(self, properties: schedule.Properties) -> str:
        """Update schedule.

        Parameters
        ----------
        properties : RunScheduleProperties
            Properties of the run schedule.


        Returns
        -------
        str
            AWS resource ARN of the schedule.

        """
        logger.debug("Update schedule %s", properties)
        try:
            response = self._client.update_schedule(
                **self._create_schedule_kwargs(properties)
            )
        except (
            self._client.exceptions.ValidationException,
            self._client.exceptions.InternalServerException,
            self._client.exceptions.ConflictException,
            self._client.exceptions.ResourceNotFoundException,
            self._client.exceptions.ThrottlingException,
        ) as e:
            message = e.response["Error"]["Message"]
            logger.exception(
                "Updating run schedule to EventBridge Scheduler failed (%s): %s",
                e.response,
                message,
            )
            _process_response_error_message(message=message, properties=properties)
            raise SchedulerActionFailedException("Failed to update schedule")
        return response["ScheduleArn"]

    def delete(self, properties: schedule.PropertiesDelete) -> None:
        """Delete schedule.

        Parameters
        ----------
        properties : RunScheduleProperties
            Properties of the run schedule.

        """
        logger.debug("Delete schedule %s", properties)
        try:
            self._client.delete_schedule(
                Name=properties.aws_name,
            )
        except (
            self._client.exceptions.ValidationException,
            self._client.exceptions.InternalServerException,
            self._client.exceptions.ConflictException,
            self._client.exceptions.ResourceNotFoundException,
            self._client.exceptions.ThrottlingException,
        ) as e:
            message = e.response["Error"]["Message"]
            logger.exception(
                "Deleting run schedule to EventBridge Scheduler failed (%s): %s",
                e.response,
                message,
            )
            _process_response_error_message(message=message, properties=properties)
            raise SchedulerActionFailedException("Failed to delete schedule")

    def _create_schedule_kwargs(self, properties: schedule.Properties) -> dict:
        return {
            "Name": properties.aws_name,
            "Description": properties.name,
            "StartDate": properties.start_date,
            "EndDate": properties.end_date,
            "ScheduleExpression": properties.schedule_expression,
            "ScheduleExpressionTimezone": properties.time_zone,
            "State": "ENABLED",
            "FlexibleTimeWindow": {"MaximumWindowInMinutes": 15, "Mode": "FLEXIBLE"},
            "Target": {
                "Arn": self._target_arn,
                "Input": properties.input,
                "RetryPolicy": {
                    # Maximum time during which retries are attempted
                    "MaximumEventAgeInSeconds": 180,
                },
                "RoleArn": self._role_arn,
                "DeadLetterConfig": {
                    "Arn": self._dead_letter_queue_arn,
                },
            },
        }


def _process_response_error_message(
    message: str, properties: schedule.Properties | schedule.PropertiesDelete
) -> None:
    if "Invalid Schedule Expression" in message:
        returned_message = f"Invalid Cron expression '{properties.cron_expression}'"
    elif re.match(r"Schedule .* already exists", message):
        returned_message = message
    elif "Schedule not found" in message:
        returned_message = "Schedule not found"
    elif re.match(r"Schedule .* does not exist", message):
        returned_message = message
    elif "The StartDate you specify must come before the EndDate" in message:
        returned_message = (
            "The specified start date "
            f"({properties.start_date.isoformat()}) "
            "must come before the end date "
            f"({properties.end_date.isoformat()}) "
        )
    else:
        returned_message = None

    if returned_message is not None:
        raise SchedulerActionFailedException(returned_message)
