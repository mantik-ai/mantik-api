import logging

# The sqlalchemy loggers log all reads/writes on database tables, which
# creates a lot of log messages. This makes finding relevant messages difficult
# if set to DEBUG.
# Thus, set to WARNING.
DATABASE = logging.WARNING

# TODO (elia.boscaini): Distinguish between dev and prod.
APP = logging.DEBUG  # DEBUG on dev, WARNING on prod
