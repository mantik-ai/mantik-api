import io
import logging
import os
import pathlib
import re
import tempfile
import typing as t
import zipfile

import requests


logger = logging.getLogger(__name__)


def unzip_directory(
    archive_data: t.BinaryIO,
    path: pathlib.Path,
    dir_to_include: pathlib.Path | re.Pattern,
) -> pathlib.Path:
    """

    Parameters
    ----------
    archive_data : Zip file in memory
    path : Path to where the zip file has to be extracted
    dir_to_include : Subdirectory in the zip file that is extracted,
        all other files and folders are not included

    """
    logger.debug("Unzipping sub-directory %s of archive to %s", dir_to_include, path)

    with zipfile.ZipFile(archive_data, "r") as archive:
        for file in archive.namelist():
            match dir_to_include:
                case pathlib.Path():
                    if not file.startswith(dir_to_include.as_posix()):
                        continue

                    path_to_unzipped_directory = path / dir_to_include
                case re.Pattern():
                    if not dir_to_include.match(file):
                        continue

                    # Extract the matched part of the file name using the regex
                    matched_path = pathlib.Path(dir_to_include.match(file).group())
                    path_to_unzipped_directory = path / matched_path
                case _:
                    raise NotImplementedError(
                        f"Unsupported type for 'dir_to_include': {type(dir_to_include)}"
                    )
            archive.extract(file, path)
    return path_to_unzipped_directory


def get_file_from_uri(uri: str, headers: dict | None) -> t.BinaryIO:
    logger.debug("Fetching file from %s", uri)

    try:
        response = requests.get(uri, headers=headers)
        response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        raise FileNotFoundError(f"Not able to fetch file from URL {uri}") from e
    else:
        return io.BytesIO(response.content)


def zip_directory(path: pathlib.Path) -> t.BinaryIO:
    """Zip given directory."""
    with tempfile.TemporaryDirectory() as tmp_dir:
        with zipfile.ZipFile(tmp_dir + "/zip_tmp", "w") as zipwriter:
            for filepath in path.rglob("*"):
                zip_path = os.path.relpath(filepath, path)
                zipwriter.write(filepath, zip_path)
            zipwriter.close()
        with open(tmp_dir + "/zip_tmp", "rb") as f:
            content = f.read()
        return io.BytesIO(content)
