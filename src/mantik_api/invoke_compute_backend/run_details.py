import dataclasses
import typing as t
import uuid


@dataclasses.dataclass(frozen=True)
class RunDetails:
    """Details of a run submitted to the compute backend."""

    experiment_id: str
    run_id: uuid.UUID
    remote_system_job_id: str

    @classmethod
    def from_json(cls, json_data: dict) -> t.Self:
        """Create from JSON data."""
        return cls(
            experiment_id=json_data["experiment_id"],
            run_id=uuid.UUID(json_data["run_id"]),
            remote_system_job_id=json_data.get("unicore_job_id")
            or json_data.get("job_id"),
        )
