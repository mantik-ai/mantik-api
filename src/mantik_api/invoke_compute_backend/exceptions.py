class InvokeComputeBackendException(Exception):
    """Base class for exceptions in mantik_api.invoke_compute_backend."""
