import copy
import logging
import pathlib
import tempfile

import requests
import starlette.status as status

import mantik_api.database as database
import mantik_api.invoke_compute_backend._git_repo as _git_repo
import mantik_api.invoke_compute_backend.backend_config as backend_config
import mantik_api.invoke_compute_backend.exceptions as exceptions
import mantik_api.invoke_compute_backend.run_details as _run_details
import mantik_api.invoke_compute_backend.utils as utils
import mantik_api.models.run as run
import mantik_api.tokens.jwt as jwt
import mantik_api.utils.env as env

logger = logging.getLogger(__name__)
_COMPUTE_BACKEND_SERVER_URL_ENV_VAR = "COMPUTE_BACKEND_SERVER_URL"

COMPUTE_BACKEND_API_USER_PARAMETER = "hpc_api_user"
COMPUTE_BACKEND_API_PASSWORD_PARAMETER = "hpc_api_password"

COMPUTE_BACKEND_API_PRIVATE_KEY_PARAMETER = "ssh_private_key"
COMPUTE_BACKEND_API_SSH_PASSWORD_PARAMETER = "ssh_password"
COMPUTE_BACKEND_API_SSH_USERNAME_PARAMETER = "ssh_username"


def submit_run(
    token: jwt.JWT,
    run: run.AddRun,
    compute_backend_url: str,
    backend_config_filename: str,
    code_repository: database.code_repository.CodeRepository | None,
    data_repository: database.data_repository.DataRepository | None,
    code_connection_git_access_token: str | None,
    request_compute_backend_data: dict,
) -> _run_details.RunDetails:
    """
    Creates a temporary directory, where code and data for a run is gathered.
    IF git access token is provided, it is included in the headers of the request which
    fetches the code from the code repository as a zip file.
    Then sends the run to the compute backend microservice.
    """
    logger.info("Submitting run to Compute Backend: %s", run)

    with tempfile.TemporaryDirectory() as tmp_dir_name:
        tmp_dir = pathlib.Path(tmp_dir_name)
        git_repo = _git_repo.GitRepo(
            url=code_repository.uri,
            branch=run.branch,
            commit=run.commit,
            access_token=code_connection_git_access_token,
            mlflow_mlproject_file_path=run.mlflow_mlproject_file_path,
            platform=code_repository.platform,
        )
        unzipped_dir = _get_code_from_code_repository(
            tmp_dir=tmp_dir,
            git_repo=git_repo,
        )

        _get_data_from_data_repository(data_repository)

        backend_config.generate_backend_config_from_run(
            config=run.backend_config,
            git_ref=git_repo.ref,
            tmp_dir=unzipped_dir,
            filename=backend_config_filename,
        )

        return _submit_run_to_compute_backend(
            token=token,
            tmp_dir=unzipped_dir,
            url=compute_backend_url,
            data=request_compute_backend_data,
        )


def _get_code_from_code_repository(
    tmp_dir: pathlib.Path,
    git_repo: _git_repo.GitRepo,
) -> pathlib.Path:
    try:
        logger.debug(
            "Fetching git repository ZIP archive from %s", git_repo.zip_archive_url
        )
        return utils.unzip_directory(
            archive_data=utils.get_file_from_uri(
                uri=git_repo.zip_archive_url, headers=git_repo.headers
            ),
            path=tmp_dir,
            dir_to_include=git_repo.mlflow_project_path_in_zip,
        )
    except FileNotFoundError as e:
        raise exceptions.InvokeComputeBackendException(str(e))


def _get_data_from_data_repository(
    data_repository: database.data_repository.DataRepository,
):
    """Data is for now assumed to be present remotely directly."""


def _submit_run_to_compute_backend(
    token: jwt.JWT, tmp_dir: pathlib.Path, url: str, data: dict[str, str]
) -> _run_details.RunDetails:
    logger.debug("Preparing request content for Compute Backend")

    headers = _create_headers(token)

    # File name in the `files` dictionary must be same as expected by
    # Compute Backend API (`mlproject_zip`).
    files = {"mlproject_zip": utils.zip_directory(tmp_dir).read()}

    logger.debug(
        (
            "Sending request to Compute Backend with URL %s, "
            "headers %s, data %s and MLproject ZIP"
        ),
        url,
        headers,
        _mask_credentials_for_logging(data),
    )

    try:
        response = requests.post(
            url=url,
            headers=headers,
            data=data,
            files=files,
            stream=True,
        )
        logger.info("Request to Compute Backend sent, checking for HTTPError")
        response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        logger.exception(
            "HTTPError when sending request to Compute Backend (%s): %s",
            e.response,
            e.response.text,
        )
        if e.response.status_code == status.HTTP_413_REQUEST_ENTITY_TOO_LARGE:
            logger.warning(
                "Submission to Compute Backend Failed: %s", e.response.content
            )
            message = (
                "The files you submitted were too large. "
                "Please consider transferring large files "
                "(such as image files) manually, e.g. with scp. "
                "You will also have to change the backend "
                "configuration to use a remote image. "
                "Then you can try to re-submit the job."
            )
        else:
            message = f"Submission to Compute Backend failed: {e.response.text}"
        raise exceptions.InvokeComputeBackendException(message) from e
    else:
        data = response.json()
        logger.info("Received Compute Backend response: %s", data)

        details = _run_details.RunDetails.from_json(data)
        logger.info("Job submitted successfully to Compute Backend: %s", details)
        return details


def _create_compute_backend_url_for_unicore_or_firecrest(
    mlflow_experiment_id: int,
) -> str:
    return (
        f"{env.get_required_env_var(_COMPUTE_BACKEND_SERVER_URL_ENV_VAR)}/submit/"
        f"{mlflow_experiment_id}"
    )


def _create_compute_backend_url_for_ssh(mlflow_experiment_id: int) -> str:
    return (
        f"{env.get_required_env_var(_COMPUTE_BACKEND_SERVER_URL_ENV_VAR)}/ssh/submit/"
        f"{mlflow_experiment_id}"
    )


def _create_headers(token: jwt.JWT) -> dict[str, str]:
    return {
        "Authorization": f"Bearer {token.to_string()}",
        "Accept": "application/json",
    }


def _mask_credentials_for_logging(data: dict) -> dict:
    duplicated = copy.deepcopy(data)
    duplicated[COMPUTE_BACKEND_API_USER_PARAMETER] = "<masked>"
    duplicated[COMPUTE_BACKEND_API_PASSWORD_PARAMETER] = "<masked>"
    duplicated[COMPUTE_BACKEND_API_PRIVATE_KEY_PARAMETER] = "<masked>"
    duplicated[COMPUTE_BACKEND_API_SSH_PASSWORD_PARAMETER] = "<masked>"
    return duplicated
