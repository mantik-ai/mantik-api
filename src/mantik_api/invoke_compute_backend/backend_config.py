import json
import logging
import pathlib

import mantik_api.invoke_compute_backend.exceptions as exceptions

logger = logging.getLogger(__name__)


def generate_backend_config_from_run(
    config: dict,
    git_ref: str,
    tmp_dir: pathlib.Path,
    filename: str,
) -> None:
    """
    Generate a backend config file,
    this file is also added to the temporary directory
    where code and data for a run is gathered.


    NOTE: The backend config file added to the temporary directory
    is going to be called compute-backend-config.json

    Parameters
    ----------
    tmp_dir : pathlib.Path
        Path of temporary directory,
        in which all files needed for the project are stored.

    """
    logger.debug("Generating backend config file for config %s", config)

    with open(tmp_dir / filename, "w") as outfile:
        # Add `Environment` and `Environment.Variables` sections if not exist
        if "Environment" not in config:
            config["Environment"] = {}
        if "Variables" not in config["Environment"]:
            config["Environment"]["Variables"] = {}
        # Add env vars that should be available at runtime
        config["Environment"]["Variables"]["MANTIK_GIT_REF"] = git_ref

        if "Environment" in config:
            if _apptainer_environment_given(config) and _local_image_given(config):
                image = _get_image_path_from_config(config)
                if not _image_found(image, dir=tmp_dir):
                    files = _list_files(tmp_dir)
                    logger.debug(
                        "No image with path %s found in %s: %s", image, tmp_dir, files
                    )
                    raise exceptions.InvokeComputeBackendException(
                        f"Given Apptainer image {image!r} not found in "
                        f"MLproject directory. Following files found: {files}"
                    )
        json.dump(config, outfile)


def _apptainer_environment_given(config: dict) -> bool:
    return "Apptainer" in config["Environment"]


def _local_image_given(config: dict) -> bool:
    if isinstance(config["Environment"]["Apptainer"], str):
        # Only string given for `Apptainer` means type is local by default
        return True

    if "Type" not in config["Environment"]["Apptainer"]:
        raise exceptions.InvokeComputeBackendException(
            "Config is missing Environment.Apptainer.Type field"
        )

    return config["Environment"]["Apptainer"]["Type"].lower() == "local"


def _image_found(image: str, dir: pathlib.Path) -> bool:
    return any(image in path.name for path in dir.glob("**/*"))


def _get_image_path_from_config(config: dict) -> str:
    if isinstance(config["Environment"]["Apptainer"], str):
        return config["Environment"]["Apptainer"]
    return config["Environment"]["Apptainer"]["Path"]


def _list_files(dir: pathlib.Path) -> str:
    return ", ".join(f"'{p.relative_to(dir).as_posix()}'" for p in dir.glob("**/*"))
