import logging
import pathlib
import re

import giturlparse
import requests.utils

import mantik_api.database as database
import mantik_api.invoke_compute_backend.exceptions as exceptions
import mantik_api.utils.git_utils as git_utils

logger = logging.getLogger(__name__)


class GitRepo:
    """Details of a specific Git repository state.

    Parameters
    ----------
    url : str
        URL to a Git project.
    branch : str, default="main"
        Branch from which to pull the code.
    mlflow_mlproject_file_path : pathlib.Path
        Relative path to the MLflow MLproject file
    commit : str, optional
        Commit for which to pull the code.

    """

    def __init__(
        self,
        url: str,
        platform: database.git_repository.Platform,
        branch: str | None,
        commit: str | None,
        access_token: str | None,
        mlflow_mlproject_file_path: pathlib.Path,
    ):
        if branch is None and commit is None:
            raise ValueError("No branch or commit hash given")

        self._url = url
        self._branch = branch or None
        self._commit = commit or None
        self._access_token = access_token or None

        if self._branch is None and self._commit is None:
            raise ValueError("No branch or commit given")

        self._parsed_url = giturlparse.parse(url)
        self._platform = platform
        # The reference of the ZIP archive will be the long commit
        # hash - if given - or the branch name.
        self._ref = commit or branch
        self._mlflow_mlproject_file_path = mlflow_mlproject_file_path

        self._validate_mlflow_mlproject_file_path()

    def _validate_mlflow_mlproject_file_path(self) -> None:
        logger.debug(
            (
                "Validating MLflow project file of Git repo %s "
                "(branch: %s, commit: %s) at relative path %s"
            ),
            self._url,
            self._branch,
            self._commit,
            self._mlflow_mlproject_file_path,
        )
        match self._platform:
            case database.git_repository.Platform.GitLab:
                url = git_utils.raw_file_gitlab_url(
                    url=self._url,
                    filepath=str(self._mlflow_mlproject_file_path),
                    ref=self._ref,
                )
            case database.git_repository.Platform.GitHub:
                # First character of pathname is a /
                url = git_utils.raw_file_github_url(
                    repo=self._parsed_url.pathname[1:],
                    filepath=str(self._mlflow_mlproject_file_path),
                    ref=self._ref,
                )
            case database.git_repository.Platform.Bitbucket:
                url = git_utils.raw_file_bitbucket_url(
                    repo=self._parsed_url.url,
                    ref=self._ref,
                    filepath=str(self._mlflow_mlproject_file_path),
                    domain=self._parsed_url.domain,
                )

            case _:
                raise exceptions.InvokeComputeBackendException(
                    f"Unsupported Git platform {self._platform}"
                )

        logger.debug("Using URL %s for validation", url)

        try:
            response = requests.get(url, headers=self.headers)
            response.raise_for_status()
        except requests.HTTPError as e:
            logger.debug(
                "Unable to access raw content %s (%s): %s",
                url,
                e.response.status_code,
                e.response.text,
            )
            error_message = (
                "Invalid relative path to MLflow project file inside repository "
                f"{self._parsed_url.url} (ref: {self._ref}): "
                f"{self._mlflow_mlproject_file_path}"
            )

            if e.response.status_code in [403, 401]:
                error_message = (
                    "Cannot reach the MLflow project file inside repository"
                    f"{self._parsed_url.url} (ref: {self._ref}): "
                    f"{self._mlflow_mlproject_file_path}"
                    "Make sure the access token for the code repository is valid."
                )

            raise exceptions.InvokeComputeBackendException(error_message)

    @property
    def ref(self) -> str:
        return self._ref

    @property
    def headers(self) -> dict | None:
        if not self._access_token:
            return None
        return git_utils.git_authorization_header(
            platform=self._platform, access_token=self._access_token
        )

    @property
    def _zip_archive_path(self) -> str:
        match self._platform:
            case database.git_repository.Platform.GitLab:
                return f"-/archive/{self._ref}"
            case database.git_repository.Platform.GitHub:
                return "archive"
            case database.git_repository.Platform.Bitbucket:
                match self._parsed_url.domain:
                    case "bitbucket.org":
                        return "get"
                    case _:
                        # The URL download a Bitbucket Server repository has the
                        # following structure:
                        # https://<bitbucket url>/rest/api/latest/projects/<project key>/repos/<repo name>/archive?at=<commit or branch>&format=zip # noqa E501
                        # The git url parser is not able to recognize this structure
                        # therefore the pathname attribute is used.
                        return f"rest/api/latest{self._parsed_url.pathname}/archive?at="
            case _:
                raise exceptions.InvokeComputeBackendException(
                    f"Unsupported Git platform {self._platform}"
                )

    @property
    def _zip_file_name_in_url(self) -> str:
        match self._platform:
            case database.git_repository.Platform.GitLab:
                return f"{self._parsed_url.repo}-{self._ref}"
            case database.git_repository.Platform.GitHub:
                return self._ref
            case database.git_repository.Platform.Bitbucket:
                match self._parsed_url.domain:
                    case "bitbucket.org":
                        return self._commit
                    case _:
                        return f"{self._ref}"

            case _:
                raise exceptions.InvokeComputeBackendException(
                    f"Unsupported Git platform {self._platform}"
                )

    @property
    def zip_archive_url(self) -> str:
        """Return the ZIP archive URL.

        Notes
        -----
        Example ZIP archive URLs:

        GitLab:
        https://gitlab.com/<group>/<sub group>/<repo>/-/archive/<branch>/<repo>-<git branch>.zip  # noqa: E501
        https://gitlab.com/<group>/<sub group>/<repo>/-/archive/<short or long commit hash>/<repo>-<short or long commit hash>.zip  # noqa: E501

        GitHub:
        https://github.com/<owner>/<repo>/archive/<branch>.zip
        https://github.com/<owner>/<repo>/archive/<short or long commit hash>.zip

        Bitbucket.org:
        https://bitbucket.org/<owner>/<repo>/get/<short or long commit hash>.zip

        Self Hosted Bitbucket:
        https://<bitbucket url>/rest/api/latest/projects/<project key>/repos/<repo name>/archive?at=<short/long commit or branch>&format=zip # noqa: E501
        https://<bitbucket url>/rest/api/latest/projects/<project key>/repos/<repo name>/archive?at=<short/long commit or branch>&format=zip # noqa: E501

        Resulting ZIP archive name is ``<repo>-<long commit hash OR branch>.zip`` in all cases.

        """
        if (
            self._platform == database.git_repository.Platform.Bitbucket
            and "bitbucket.org" not in self._parsed_url.url
        ):
            return (
                f"{self._parsed_url.url}/"
                f"{self._zip_archive_path}"
                f"{self._zip_file_name_in_url}&format=zip"
            )
        else:
            return (
                f"{self._parsed_url.url}/"
                f"{self._zip_archive_path}/"
                f"{self._zip_file_name_in_url}.zip"
            )

    @property
    def zip_directory_name(self) -> re.Pattern:
        """Return the name of the directory in the ZIP archive.

        For all types of git repositories the download URL works when the short version
        of a hash commit is entered. However, for _GitHub_ and _Bitbucket org_ repos,
        the _zip directory name_ contains the _long hash commit_. For _Gitlab_ and
        _Bibucket self hosted_, the _zip directory name_ corresponds to the
        _user-entered version_ of the commit hash.
        """
        match self._platform:
            case database.git_repository.Platform.GitLab:
                return re.compile(rf"{self._parsed_url.repo}-{self._ref}")
            case database.git_repository.Platform.GitHub:
                return re.compile(rf"{self._parsed_url.repo}-{self._ref}.*?")
            case database.git_repository.Platform.Bitbucket:
                match self._parsed_url.domain:
                    case "bitbucket.org":
                        return re.compile(rf"{self._parsed_url.repo}-{self._commit}.*?")
                    case _:
                        # For self-hosted Bitbucket, the ZIP directory name
                        # _must_ contain the branch _and_ commit, whereas
                        # both cannot simultaneously be provided by the client.
                        # As a consequence, self-hosted Bitbucket repos can _not_
                        # be submitted when using a branch (i.e. commit not specified).
                        # Hence, we use a regex to match the file name using a
                        # commit and force self-hosted Bitbucket repos to _always_ be
                        # executed using a commit
                        # (see :func:`mantik_api.routes.project_routes.runs.validate_reference_bitbucket_repo`).  # noqa: E501
                        return re.compile(rf"{self._parsed_url.repo}-.*@{self._commit}")

    @property
    def mlflow_project_path_in_zip(self) -> re.Pattern:
        """Return the path of the MLflow project directory in the ZIP archive."""
        path_str = (
            f"{self.zip_directory_name.pattern}/"
            f"{self._mlflow_mlproject_file_path.parent}"
        )
        return re.compile(path_str)
