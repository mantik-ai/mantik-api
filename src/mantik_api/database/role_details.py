import dataclasses
import datetime
import enum

import mantik_api.database.project as project


class ProjectRole(enum.Enum):
    NO_ROLE = -1
    GUEST = project.ProjectRole.GUEST.value
    REPORTER = project.ProjectRole.REPORTER.value
    RESEARCHER = project.ProjectRole.RESEARCHER.value
    MAINTAINER = project.ProjectRole.MAINTAINER.value
    OWNER = project.ProjectRole.OWNER.value


@dataclasses.dataclass(frozen=True)
class RoleDetails:
    role: ProjectRole
    added_at: datetime.datetime | None = None
    updated_at: datetime.datetime | None = None
