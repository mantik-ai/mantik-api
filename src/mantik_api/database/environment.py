import enum
import uuid

import sqlalchemy.dialects
import sqlalchemy.orm as orm

import mantik_api.database.base as base
import mantik_api.database.columns as columns

TABLE_NAME = "environment"
ID_COLUMN_NAME = "environment_id"


class OrderBy(base.OrderByBase):
    name = enum.auto()
    type = enum.auto()
    created = enum.auto()
    updated = enum.auto()

    @property
    def column(self) -> sqlalchemy.Column:
        match self:
            case OrderBy.name:
                return Environment.name
            case OrderBy.type:
                return Environment.type
            case OrderBy.created:
                return Environment.created_at
            case OrderBy.updated:
                return Environment.updated_at
            case _:
                raise NotImplementedError(
                    f"{self} has no associated database table column"
                )


class Environment(base.BaseWithId):
    __tablename__ = TABLE_NAME

    id = columns.create_id_column(ID_COLUMN_NAME)

    name = sqlalchemy.Column(sqlalchemy.String, nullable=False)
    type = sqlalchemy.Column(sqlalchemy.String, nullable=False)
    variables = sqlalchemy.Column(sqlalchemy.dialects.postgresql.JSONB, nullable=False)

    data_repository_id = columns.create_foreign_key_column(
        name="data_repository_id",
        table="data_repository",
        nullable=False,
        ondelete="CASCADE",
        primary_key=False,
    )
    data_repository = orm.relationship("DataRepository", back_populates="environments")
    runs = orm.relationship("Run", back_populates="environment")
    run_configurations = orm.relationship(
        "RunConfiguration", back_populates="environment"
    )

    @property
    def project_id(self) -> uuid.UUID:
        return self.data_repository.project_id
