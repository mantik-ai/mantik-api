import enum

import sqlalchemy.dialects.postgresql
import sqlalchemy.orm as orm

import mantik_api.database.base as base
import mantik_api.database.columns as columns
import mantik_api.database.connection as connection
import mantik_api.database.run as run
import mantik_api.database.user as user

TABLE_NAME = "run_schedule"
ID_COLUMN_NAME = "run_schedule_id"


class OrderBy(base.OrderByBase):
    name = enum.auto()
    created = enum.auto()
    updated = enum.auto()

    @property
    def column(self) -> sqlalchemy.Column:
        match self:
            case OrderBy.name:
                return RunSchedule.name
            case OrderBy.created:
                return RunSchedule.created_at
            case OrderBy.updated:
                return RunSchedule.updated_at
            case _:
                raise NotImplementedError(
                    f"{self} has no associated database table column"
                )


class RunSchedule(base.BaseWithId):
    __tablename__ = TABLE_NAME

    id = columns.create_id_column(ID_COLUMN_NAME)
    aws_schedule_arn = sqlalchemy.Column(sqlalchemy.String, nullable=False)
    name = sqlalchemy.Column(sqlalchemy.String)
    owner_id = columns.create_foreign_key_column(
        name=user.ID_COLUMN_NAME,
        table=user.TABLE_NAME,
    )
    owner = orm.relationship(user.User.__name__, back_populates="run_schedules")
    run_id = columns.create_foreign_key_column(
        name=run.ID_COLUMN_NAME,
        table=run.TABLE_NAME,
        nullable=False,
        primary_key=False,
    )
    run = orm.relationship(run.Run.__name__, back_populates="run_schedules")
    connection_id = columns.create_foreign_key_column(
        name=connection.ID_COLUMN_NAME,
        table=connection.TABLE_NAME,
        nullable=False,
        primary_key=False,
    )
    connection = orm.relationship(
        connection.Connection.__name__, back_populates="run_schedules"
    )
    compute_budget_account = sqlalchemy.Column(sqlalchemy.String)
    cron_expression = sqlalchemy.Column(
        sqlalchemy.String,
        doc=(
            "Cron string that defines the periodicity "
            "of the run schedule in AWS cron format."
        ),
    )
    time_zone = sqlalchemy.Column(
        sqlalchemy.String,
        doc="Time zone for schedule, as given in the Time Zone Database",
    )
    end_date = sqlalchemy.Column(
        sqlalchemy.BigInteger, doc="Date until the scheduler is active"
    )
    project_id = columns.create_foreign_key_column(
        name="project_id",
        table="project",
        nullable=False,
        primary_key=False,
    )
