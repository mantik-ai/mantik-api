import enum
import typing as t
import uuid

import sqlalchemy
import sqlalchemy.orm as orm

import mantik_api.database.base as base
import mantik_api.database.columns as columns
import mantik_api.database.user as user
import mantik_api.database.user_group as user_group
from mantik_api.utils.functions import flat_map

TABLE_NAME = "organization"
ID_COLUMN_NAME = "organization_id"


class OrderBy(base.OrderByBase):
    name = enum.auto()
    created = enum.auto()
    updated = enum.auto()

    @property
    def column(self) -> sqlalchemy.Column:
        match self:
            case OrderBy.name:
                return Organization.name
            case OrderBy.created:
                return Organization.created_at
            case OrderBy.updated:
                return Organization.updated_at
            case _:
                raise NotImplementedError(
                    f"{self} has no associated database table column"
                )


class OrganizationUserAssociationTable(base.Base):
    __tablename__ = "organization_user_association_table"

    organization_id = columns.create_foreign_key_column(
        name=ID_COLUMN_NAME,
        table=TABLE_NAME,
        ondelete="CASCADE",
    )
    user_id = columns.create_foreign_key_column(
        name=user.ID_COLUMN_NAME,
        table=user.TABLE_NAME,
        ondelete="CASCADE",
    )


class OrganizationUserGroupAssociationTable(base.Base):
    __tablename__ = "organization_user_group_association_table"

    organization_id = columns.create_foreign_key_column(
        name=ID_COLUMN_NAME,
        table=TABLE_NAME,
        ondelete="CASCADE",
    )
    user_group_id = columns.create_foreign_key_column(
        name=user_group.ID_COLUMN_NAME,
        table=user_group.TABLE_NAME,
        ondelete="CASCADE",
    )


class Organization(base.BaseWithId):
    __tablename__ = TABLE_NAME

    id = columns.create_id_column(ID_COLUMN_NAME)
    name = sqlalchemy.Column(sqlalchemy.String, nullable=False)
    contact_id = columns.create_foreign_key_column(
        name="contact",
        table=user.TABLE_NAME,
        column=user.ID_COLUMN_NAME,
        nullable=False,
        primary_key=False,
    )
    contact = sqlalchemy.orm.relationship(
        user.User.__name__, back_populates="owned_organizations"
    )
    groups = orm.relationship(
        "UserGroup",
        secondary=OrganizationUserGroupAssociationTable.table,
        back_populates="organizations",
    )
    members = orm.relationship(
        "User",
        secondary=OrganizationUserAssociationTable.table,
        back_populates="organizations",
    )
    projects = orm.relationship(
        "Project",
        secondary="organization_project_association_table",
        back_populates="project_organizations",
    )

    def __repr__(self):
        return f"{self.__class__.__name__}({self.id=}, {self.name=})".replace(
            "self.", ""
        )

    @staticmethod
    def create_constraint_for_user_organizations(
        user_: user.User,
    ) -> sqlalchemy.sql.elements.BooleanClauseList:
        """Create a constraint that gets all organizations of a user."""

        def get_ids(column: sqlalchemy.Column) -> t.Iterator[uuid.UUID]:
            return (row.id for row in column)

        return sqlalchemy.or_(
            # User is in an organization
            Organization.id.in_(get_ids(user_.organizations)),
            # User owns an organization
            Organization.id.in_(get_ids(user_.owned_organizations)),
            # User is in an associated user group
            Organization.groups.any(user_group.UserGroup.id.in_(get_ids(user_.groups))),
            # User owns an associated user group
            Organization.groups.any(
                user_group.UserGroup.id.in_(get_ids(user_.owned_groups))
            ),
        )

    def contains_user(self, user_id: uuid.UUID) -> bool:
        organization_user_ids = [member.id for member in self.members] + [
            self.contact_id
        ]
        return user_id in organization_user_ids or any(
            group.contains_user(user_id) for group in self.groups
        )

    @property
    def all_members(self) -> list[user.User]:
        """Returns all unique members of the organization, which includes direct members
        and members of all groups belonging to the organization"""

        members_through_groups = flat_map([group.members for group in self.groups])
        flat_members_list = members_through_groups + self.members

        all_unique_organization_members = {
            _user.id: _user for _user in flat_members_list
        }.values()

        return all_unique_organization_members

    def contains_group(self, group_id: uuid.UUID) -> bool:
        return group_id in self.groups
