import enum
import uuid

import sqlalchemy
import sqlalchemy.orm as orm

import mantik_api.database.base as base
import mantik_api.database.columns as columns
import mantik_api.database.git_repository as git_repository
import mantik_api.database.label as label

TABLE_NAME = "code_repository"
ID_COLUMN_NAME = "code_repository_id"


class OrderBy(base.OrderByBase):
    name = enum.auto()
    uri = enum.auto()
    created = enum.auto()
    updated = enum.auto()

    @property
    def column(self) -> sqlalchemy.Column:
        match self:
            case OrderBy.name:
                return CodeRepository.code_repository_name
            case OrderBy.uri:
                return CodeRepository.uri
            case OrderBy.created:
                return CodeRepository.created_at
            case OrderBy.updated:
                return CodeRepository.updated_at
            case _:
                raise NotImplementedError(
                    f"{self} has no associated database table column"
                )


class CodeRepositoryLabelAssociationTable(base.BaseLabelsAssociation):
    __tablename__ = "code_repository_label_association_table"
    model_id = columns.create_foreign_key_column(
        name=ID_COLUMN_NAME,
        table=TABLE_NAME,
        ondelete="CASCADE",
    )


class CodeRepositoryAndConnectionAssociationTable(base.Base):
    __tablename__ = "code_repository_and_connection_association_table"
    id = columns.create_id_column("code_repository_and_connection_association_table_id")
    code_repository_id = columns.create_foreign_key_column(
        name=ID_COLUMN_NAME,
        table=TABLE_NAME,
        ondelete="CASCADE",
    )
    connection_id = columns.create_foreign_key_column(
        name="connection_id",
        table="connection",
        ondelete="CASCADE",
    )


class CodeRepository(base.BaseWithId):
    __tablename__ = TABLE_NAME

    id = columns.create_id_column(ID_COLUMN_NAME)
    uri = sqlalchemy.Column(sqlalchemy.String, nullable=False)
    code_repository_name = sqlalchemy.Column(sqlalchemy.String)
    description = sqlalchemy.Column(sqlalchemy.String)
    labels = orm.relationship(
        label.Label.__name__, secondary=CodeRepositoryLabelAssociationTable.table
    )
    model_repositories = orm.relationship(
        "ModelRepository",
        back_populates="code_repository",
        passive_deletes=True,
    )
    project_id = columns.create_foreign_key_column(
        name="project_id",
        table="project",
        nullable=False,
        primary_key=False,
        ondelete="CASCADE",
    )
    project = orm.relationship("Project", back_populates="code_repositories")
    run_configurations = orm.relationship(
        "RunConfiguration", back_populates="code_repository"
    )

    platform = sqlalchemy.Column(
        columns.create_string_enum(git_repository.Platform),
        nullable=False,
        default=git_repository.Platform.GitHub,
    )
    connections = orm.relationship(
        "Connection",
        back_populates="code_repositories",
        secondary=CodeRepositoryAndConnectionAssociationTable.table,
    )

    def my_git_connection_id(self, user_id: uuid.UUID) -> uuid.UUID | None:
        for _connection in self.connections:
            if (
                _connection.owned_by_user(user_id=user_id)
                and _connection.is_git_connection
            ):
                return _connection.id
        return None
