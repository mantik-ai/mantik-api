import mantik_api.utils as utils


class Platform(utils.enums.CaseInsensitiveStrEnum):
    GitHub = "GitHub"
    GitLab = "GitLab"
    Bitbucket = "Bitbucket"
