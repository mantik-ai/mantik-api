import abc
import dataclasses
import typing as t

import sqlalchemy
import sqlalchemy.orm as orm

import mantik_api.database.base as base
import mantik_api.database.client.query as base_query

OrmBase = t.TypeVar("OrmBase", bound=base.Base)
WhereExpressions = set[sqlalchemy.sql.expression.BooleanClauseList]


@dataclasses.dataclass
class Select(base_query.Query, abc.ABC, base_query.NeedsORMType):
    select_in_loading: dict[orm.relationship, dict] | None = None
    order_by: base.OrderByBase | None = None
    ascending: bool = False

    @abc.abstractmethod
    def _get_pre_query(self) -> sqlalchemy.sql.Select:
        """get query from select class"""

    def get_query(self) -> sqlalchemy.sql.Select:
        query = self._get_pre_query()
        query = self._add_select_in_relations(
            query=self._add_constrains(query),
        )
        return self._add_ordering(query)

    def _add_select_in_relations(
        self, query: sqlalchemy.sql.Select
    ) -> sqlalchemy.sql.Select:
        select_in_loading = (
            {} if self.select_in_loading is None else self.select_in_loading
        )
        for relation, subquery in select_in_loading.items():
            option = orm.selectinload(relation).options(*self._add_subqueries(subquery))
            query = query.options(option)

        return query

    def _add_subqueries(self, subqueries) -> list[dict[orm.relationship, dict]]:
        return [
            orm.selectinload(subquery).options(
                *self._add_subqueries(subqueries[subquery])
            )
            for subquery in subqueries
        ]

    def _add_ordering(self, query: sqlalchemy.sql.Select) -> sqlalchemy.sql.Select:
        if self.order_by is None:
            return query

        order_by = (
            self.order_by.column if self.ascending else self.order_by.column.desc()
        )
        return query.order_by(order_by)


@dataclasses.dataclass
class SelectOne(Select):
    def _get_pre_query(self):
        return sqlalchemy.select(self.orm_model_type)


@dataclasses.dataclass
class SelectAll(Select):
    def _get_pre_query(self):
        return sqlalchemy.select(self.orm_model_type)


@dataclasses.dataclass
class SelectMany(Select):
    offset: int = 0
    limit: int = 200

    def _get_pre_query(self):
        return (
            sqlalchemy.select(self.orm_model_type).offset(self.offset).limit(self.limit)
        )
