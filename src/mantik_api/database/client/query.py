import abc
import dataclasses
import typing as t

import sqlalchemy.sql.elements

import mantik_api.database.base as base

QueryType = sqlalchemy.sql.Select | sqlalchemy.sql.Update
OrmBase = t.TypeVar("OrmBase", bound=base.Base)

Constraints = (
    dict[sqlalchemy.Column, t.Any]
    | sqlalchemy.sql.elements.BooleanClauseList
    | sqlalchemy.sql.elements.BinaryExpression
    | None
)


@dataclasses.dataclass
class Query(abc.ABC):
    constraints: Constraints | None = None

    @abc.abstractmethod
    def get_query(self):
        """return query"""

    def _add_constrains(self, query: QueryType) -> QueryType:
        match self.constraints:
            case None:
                pass
            case {}:
                for constraint, condition in self.constraints.items():
                    query = query.where(constraint == condition)
            case (
                sqlalchemy.sql.elements.BooleanClauseList()
                | sqlalchemy.sql.elements.BinaryExpression()
            ):
                query = query.where(self.constraints)
            case _:
                raise TypeError(f"Invalid constraints type {type(self.constraints)}")
        return query


@dataclasses.dataclass
class NeedsORMType:
    orm_model_type: type[OrmBase]


@dataclasses.dataclass
class NeedsORM:
    orm_model: OrmBase


@dataclasses.dataclass
class NeedsORMorORMType:
    orm_model: type[OrmBase] | OrmBase
