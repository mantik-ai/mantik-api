import dataclasses

import sqlalchemy

import mantik_api.database.client.query as base_query


@dataclasses.dataclass
class Delete(base_query.Query, base_query.NeedsORMorORMType):
    def get_query(self) -> sqlalchemy.sql.Update:
        if isinstance(self.orm_model, type):
            return self._add_constrains(sqlalchemy.delete(self.orm_model))
        return self._add_constrains(sqlalchemy.delete(self.orm_model.__class__))
