import contextlib
import dataclasses
import functools
import logging
import typing as t
import uuid

import sqlalchemy
import sqlalchemy.orm as orm

import mantik_api.database.base as base
import mantik_api.database.client.delete as delete
import mantik_api.database.client.query as _query
import mantik_api.database.client.select as select
import mantik_api.database.client.sessions as sessions
import mantik_api.database.client.update as update
import mantik_api.utils.env as env

logger = logging.getLogger(__name__)


class NoActiveSessionException(Exception):
    """Client was not called correctly and thus there is no active session."""


def _raise_if_no_active_session(func):
    """Raise exception if client has not been called correctly.

    Notes
    -----
    Client has to be called via

    ```python
    with client.session():
        client.add(...)
        ...
    ```

    to establish a session and allow a rollback if one of the commands
    within the ``with`` block fails. If all transactions are successful,
    the transaction is committed.

    If any method is called outside the block, the ``SessionManager``
    raises e.g. ``AttributeError: 'NoneType' object has no attribute 'add'`,
    which means that the session is currently closed (no active session).

    """

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except AttributeError as e:
            if "'NoneType' object has no attribute" in str(e):
                raise NoActiveSessionException(
                    f"No active database session, use "
                    f"{Client.__name__}.{Client.session.__name__}() "
                    "context manager"
                ) from e
            raise e

    return wrapper


@dataclasses.dataclass
class GetAllResponse:
    total_count: int
    page_count: int
    entities: t.Iterator[select.OrmBase]


class Client:
    """Client for database interaction."""

    def __init__(
        self,
        engine: sqlalchemy.engine.Engine,
        session_handler: sessions.AbstractSessionHandler,
    ):
        self._engine = engine
        self._session_handler = session_handler

    @contextlib.contextmanager
    def session(self, raise_exception: bool = True) -> t.Self:
        try:
            self._session_handler.begin()
            yield self
        except:  # noqa
            logger.warning(
                "Database transaction failed, rolling back transaction",
                exc_info=True,
            )
            self._session_handler.rollback()

            if raise_exception:
                raise
        finally:
            self._session_handler.close()
            self._engine.dispose()

    def commit(self) -> None:
        self._session_handler.commit()

    @classmethod
    def from_env(cls) -> t.Self:
        uri = _get_uri_from_env()
        engine = sqlalchemy.create_engine(uri)
        session = sessions.SessionHandler(engine=engine)
        return cls(engine=engine, session_handler=session)

    @_raise_if_no_active_session
    def add(self, model: select.OrmBase) -> None:
        """Insert a new model into the database."""
        self._session_handler.add(model)

    @_raise_if_no_active_session
    def get_all(
        self,
        orm_model_type: type[select.OrmBase],
        constraints: _query.Constraints = None,
        select_in_loading: dict[orm.relationship, dict] | None = None,
        order_by: base.OrderByBase | None = None,
        ascending: bool = False,
    ) -> GetAllResponse:
        """Get all models."""
        select_all = select.SelectAll(
            orm_model_type=orm_model_type,
            constraints=constraints,
            select_in_loading=select_in_loading,
            order_by=order_by,
            ascending=ascending,
        )
        query = select_all.get_query()
        count = self._session_handler.get_count(query)
        return GetAllResponse(
            entities=self._session_handler.get_all(query),
            page_count=count,
            total_count=count,
        )

    @_raise_if_no_active_session
    def get_many(
        self,
        orm_model_type: type[select.OrmBase],
        order_by: base.OrderByBase,
        ascending: bool,
        constraints: _query.Constraints = None,
        select_in_loading: dict[orm.relationship, dict] | None = None,
        offset: int = 0,
        limit: int = 200,
    ) -> GetAllResponse:
        """Get multiple model based on the select parameters in the SelectMany class."""
        select_all_matches = select.SelectAll(
            orm_model_type=orm_model_type,
            constraints=constraints,
        )
        select_with_offset_and_limit = select.SelectMany(
            orm_model_type=orm_model_type,
            constraints=constraints,
            select_in_loading=select_in_loading,
            offset=offset,
            limit=limit,
            order_by=order_by,
            ascending=ascending,
        )
        query = select_with_offset_and_limit.get_query()
        return GetAllResponse(
            entities=self._session_handler.get_all(query),
            page_count=self._session_handler.get_count(query),
            total_count=self._session_handler.get_count(select_all_matches.get_query()),
        )

    @_raise_if_no_active_session
    def get_one_by_id(
        self, orm_model_type: type[base.BaseWithId], id_: uuid.UUID
    ) -> select.OrmBase:
        return self.get_one(
            orm_model_type=orm_model_type,
            constraints={orm_model_type.id: id_},
        )

    @_raise_if_no_active_session
    def get_one(
        self,
        orm_model_type: type[select.OrmBase],
        constraints: _query.Constraints = None,
        select_in_loading: dict[orm.relationship, dict] | None = None,
    ) -> select.OrmBase:
        """Get one model based on the select parameters in the SelectOne class.

        Raises
        ------
        exceptions.NotFoundError

        """
        select_one = select.SelectOne(
            orm_model_type=orm_model_type,
            constraints=constraints,
            select_in_loading=select_in_loading,
        )
        return self._session_handler.get_one_or_none(select_one.get_query())

    @_raise_if_no_active_session
    def update(
        self,
        model: select.OrmBase,
        constraints: dict[sqlalchemy.Column, t.Any],
        fields_to_update: dict | None = None,
    ) -> None:
        """Update a database model."""
        query = update.Update(
            orm_model=model, constraints=constraints, fields_to_update=fields_to_update
        )
        self._session_handler.execute_query(query.get_query())

    @_raise_if_no_active_session
    def overwrite(
        self,
        original_orm: select.OrmBase,
        fields_to_overwrite: dict[str, t.Any],
    ) -> None:
        """Overwrite a database model."""
        self._session_handler.overwrite(
            original_orm=original_orm, fields_to_overwrite=fields_to_overwrite
        )

    @_raise_if_no_active_session
    def delete(
        self,
        model: type[select.OrmBase] | select.OrmBase,
        constraints: dict[sqlalchemy.Column, t.Any] | None = None,
    ) -> None:
        """Delete rows from database."""
        if _would_delete_all_table_rows(constraints):
            if isinstance(model, base.Base):
                constraints = {model.__class__.id: model.id}
            else:
                raise ValueError(
                    "For deleting using a database table object, "
                    "constraints are required"
                )

        query = delete.Delete(orm_model=model, constraints=constraints)
        self._session_handler.execute_query(query.get_query())


def _would_delete_all_table_rows(
    constraints: dict[sqlalchemy.Column, t.Any] | None = None
) -> bool:
    return constraints is None or not constraints


@dataclasses.dataclass
class DBConnection:
    username: str
    password: str
    host: str
    port: str
    database: str

    @property
    def uri(self) -> str:
        return f"postgresql+psycopg2://{self.username}:{self.password}@{self.host}:{self.port}/{self.database}"  # noqa: E501


def get_database_connection_from_env() -> DBConnection:
    # Locally, the `USERNAME` env var cannot be overriden
    # and hence prevents testing e.g. with schemathesis.
    # Alternatively, one can set the `DATABASE_USERNAME` env var.
    username = env.get_optional_env_var(
        "DATABASE_USERNAME"
    ) or env.get_required_env_var("USERNAME")
    return DBConnection(
        username=username,
        password=env.get_required_env_var("PASSWORD"),
        host=env.get_required_env_var("HOST"),
        port=env.get_required_env_var("PORT"),
        database=env.get_required_env_var("DATABASE"),
    )


def _get_uri_from_env() -> str:
    return get_database_connection_from_env().uri
