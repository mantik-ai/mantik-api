import abc
import logging
import re
import typing as t

import sqlalchemy.exc
import sqlalchemy.orm as orm
import sqlalchemy.sql.selectable as sql_selectable

import mantik_api.database.base as base
import mantik_api.database.exceptions as exceptions
import mantik_api.logging_levels as logging_levels

logger = logging.getLogger(__name__)
# Logging configured according to
# https://docs.sqlalchemy.org/en/20/core/engines.html#configuring-logging
logging.getLogger("sqlalchemy.engine").setLevel(logging_levels.DATABASE)
logging.getLogger("sqlalchemy.pool").setLevel(logging_levels.DATABASE)

OrmBase = t.TypeVar("OrmBase", bound=base.Base)


class AbstractSessionHandler(abc.ABC):
    @abc.abstractmethod
    def __init__(self, engine: sqlalchemy.engine.Engine):  # noqa
        """Inializes the class."""

    @abc.abstractmethod
    def begin(self) -> t.Self:
        """Begin session."""
        raise NotImplementedError

    @abc.abstractmethod
    def commit(self) -> None:
        """Commit changes."""
        raise NotImplementedError

    @abc.abstractmethod
    def rollback(self) -> None:
        """Rollback changes."""
        raise NotImplementedError

    @abc.abstractmethod
    def close(self) -> None:
        """Try to commit and close session, rollback in case of error."""
        raise NotImplementedError

    @abc.abstractmethod
    def add(self, model: OrmBase) -> None:
        """Add model instance to database."""

    @abc.abstractmethod
    def overwrite(
        self,
        original_orm: OrmBase,
        fields_to_overwrite: dict[str, t.Any],
    ) -> None:
        """Overwrite attributes of a model in database."""

    @abc.abstractmethod
    def execute_query(self, query: sqlalchemy.orm.query) -> None:
        """Executes database query."""

    @abc.abstractmethod
    def get_all(self, query: sqlalchemy.orm.query) -> t.Iterator[sqlalchemy.engine.Row]:
        """Get all results from a select query."""

    @abc.abstractmethod
    def get_one_or_none(
        self, query: sqlalchemy.orm.query
    ) -> sqlalchemy.engine.Row | None:
        """Get result if model with such id exists else none."""

    @abc.abstractmethod
    def get_count(self, query: sqlalchemy.orm.query) -> int:
        """Get all results from a select query."""


class SessionHandler(AbstractSessionHandler):
    def __init__(self, engine: sqlalchemy.engine.Engine):
        self._engine = engine
        self._sessionmaker = orm.sessionmaker(bind=engine)
        self._session = None

    def begin(self) -> t.Self:
        self._session = self._sessionmaker(expire_on_commit=False)
        self._session.begin()
        return self

    def commit(self) -> None:
        self._session.commit()

    def rollback(self) -> None:
        self._session.rollback()

    def close(self) -> None:
        try:
            self._session.commit()
        except sqlalchemy.exc.DatabaseError as e:
            logger.error("Database transaction failed", exc_info=True)
            self._handle_database_error(e)
        finally:
            self._session.close()
            self._session = None

    def add(self, model: OrmBase) -> None:
        try:
            self._session.add(model)
        except sqlalchemy.exc.DatabaseError as e:
            logger.error("Adding of model %s failed", model, exc_info=True)
            self._handle_database_error(e)

    def overwrite(
        self,
        original_orm: OrmBase,
        fields_to_overwrite: dict[str, t.Any],
    ) -> None:
        try:
            for name, value in fields_to_overwrite.items():
                if name in original_orm.primary_key:
                    continue
                setattr(original_orm, name, value)
        except sqlalchemy.exc.DatabaseError as e:
            logger.error(
                "Updating fields %s on %s failed",
                fields_to_overwrite,
                original_orm,
                exc_info=True,
            )
            self._handle_database_error(e)

    def execute_query(self, query: sqlalchemy.orm.Query) -> None:
        try:
            self._session.execute(query)
        except sqlalchemy.exc.DatabaseError as e:
            logger.error(
                "Executing query %s failed", self._compile_query(query), exc_info=True
            )
            self._handle_database_error(e)

    def get_one_or_none(self, query: sql_selectable.Select):
        try:
            result = self._session.execute(query)
        except sqlalchemy.exc.DatabaseError as e:
            logger.error(
                "Getting single entity via query %s failed",
                self._compile_query(query),
                exc_info=True,
            )
            self._handle_database_error(e)
        else:
            result = result.scalar_one_or_none()
            if result is None:
                self.rollback()
                raise exceptions.NotFoundError(
                    f"Resource not found for query '{self._compile_query(query)}'"
                )
            return result

    def get_all(
        self, query: sql_selectable.Select
    ) -> t.Iterator[sqlalchemy.engine.Row]:
        try:
            result = self._session.execute(query)
        except sqlalchemy.exc.DatabaseError as e:
            logger.error(
                "Getting all entities via query %s failed",
                self._compile_query(query),
                exc_info=True,
            )
            self._handle_database_error(e)
        else:
            for r in result.all():
                # Rows are tuples of length 1
                yield r[0]

    def get_count(self, query: sql_selectable.Select) -> int:
        try:
            result = self._session.execute(
                sqlalchemy.select(sqlalchemy.func.count()).select_from(
                    query.alias("subquery")
                )
            )
        except sqlalchemy.exc.DatabaseError as e:
            logger.error(
                "Getting count via query %s failed",
                self._compile_query(query),
                exc_info=True,
            )
            self._handle_database_error(e)
        else:
            return result.scalar_one()

    def _compile_query(
        self, query: sqlalchemy.orm.Query | sql_selectable.Select
    ) -> str:
        return query.compile(self._engine)

    def _handle_database_error(self, exception: sqlalchemy.exc.DatabaseError) -> None:
        self.rollback()
        match exception:
            case sqlalchemy.exc.IntegrityError():
                exc_str = str(exception)
                if "violates foreign key constraint" in exc_str:
                    if "is still referenced from table" in exc_str:
                        [referenced_object] = re.findall(
                            r"is still referenced from table \"([A-z]*?)\"", exc_str
                        )
                        raise exceptions.ReferencedResourceExistsError(
                            "Resource still references "
                            f"{referenced_object.replace('_', ' ').title()}"
                        )
                    raise exceptions.DatabaseExecutionError("Linked resource not found")
                elif "duplicate key value violates unique constraint" in exc_str:
                    raise exceptions.DatabaseExecutionError("Value already present")
                raise exceptions.DatabaseExecutionError("Database error")
            case _:
                raise exceptions.DatabaseExecutionError("Database error")
