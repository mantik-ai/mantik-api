import dataclasses

import sqlalchemy

import mantik_api.database.client.query as base_query


@dataclasses.dataclass
class Update(base_query.Query, base_query.NeedsORM):
    fields_to_update: dict | None = None

    def get_query(self) -> sqlalchemy.sql.Update:
        return self._add_constrains(
            query=self._add_values_to_update(
                query=sqlalchemy.update(self.orm_model.__class__)
            )
        )

    def _add_values_to_update(
        self, query: sqlalchemy.sql.Update
    ) -> sqlalchemy.sql.Update:
        if self.fields_to_update is None:
            return query.values(self._create_default_update_query_kwargs())
        return query.values(self.fields_to_update)

    def _create_default_update_query_kwargs(self) -> dict[str, sqlalchemy.Column]:
        return {
            column.name: getattr(self.orm_model, column.name)
            for column in self.orm_model.columns
            if column.name not in self.orm_model.primary_key
            and getattr(self.orm_model, column.name)
        }
