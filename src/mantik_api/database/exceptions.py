import uuid


class MantikDatabaseError(Exception):
    """Base class for exceptions in mantik_api."""


class NotFoundError(MantikDatabaseError):
    """The query did not find any value."""


class InconsistentIdsError(MantikDatabaseError):
    """The request contains two different ids."""


class DatabaseExecutionError(MantikDatabaseError):
    """Something in the sql statement is wrong."""

    def __init__(self, message):
        self.message = message


class ReferencedResourceExistsError(MantikDatabaseError):
    """A referenced resource still exists."""


def check_uuids_consistency(first_uuid: uuid.UUID, second_uuid: uuid.UUID):
    if not first_uuid == second_uuid:
        raise InconsistentIdsError()
