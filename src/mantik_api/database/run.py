import enum
import uuid

import sqlalchemy.dialects.postgresql
import sqlalchemy.ext.declarative as declarative
import sqlalchemy.orm as orm
import sqlalchemy_utils as sql_utils

import mantik_api.database.base as base
import mantik_api.database.code_repository as code_repository
import mantik_api.database.columns as columns
import mantik_api.database.connection as connection
import mantik_api.database.data_repository as data_repository
import mantik_api.database.environment as environment
import mantik_api.database.experiment_repository as experiment_repository
import mantik_api.database.model_repository as model_repository
import mantik_api.database.trained_model as saved_model_repository
import mantik_api.database.user as user
import mantik_api.utils as utils

TABLE_NAME = "run"
ID_COLUMN_NAME = "run_id"


class OrderRunBy(base.OrderByBase):
    name = enum.auto()
    status = enum.auto()
    compute_budget_account = enum.auto()
    created = enum.auto()
    updated = enum.auto()

    @property
    def column(self) -> sqlalchemy.Column:
        match self:
            case OrderRunBy.name:
                return Run.name
            case OrderRunBy.status:
                return Run.status
            case OrderRunBy.compute_budget_account:
                return Run.compute_budget_account
            case OrderRunBy.created:
                return Run.created_at
            case OrderRunBy.updated:
                return Run.updated_at
            case _:
                raise NotImplementedError(
                    f"{self} has no associated database table column"
                )


class OrderRunConfigurationBy(base.OrderByBase):
    name = enum.auto()
    branch = enum.auto()
    commit = enum.auto()
    compute_budget_account = enum.auto()
    created = enum.auto()
    updated = enum.auto()

    @property
    def column(self) -> sqlalchemy.Column:
        match self:
            case OrderRunConfigurationBy.name:
                return RunConfiguration.name
            case OrderRunConfigurationBy.branch:
                return RunConfiguration.branch
            case OrderRunConfigurationBy.commit:
                return RunConfiguration.commit
            case OrderRunConfigurationBy.compute_budget_account:
                return RunConfiguration.compute_budget_account
            case OrderRunConfigurationBy.created:
                return RunConfiguration.created_at
            case OrderRunConfigurationBy.updated:
                return RunConfiguration.updated_at
            case _:
                raise NotImplementedError(
                    f"{self} has no associated database table column"
                )


class RunStatus(utils.enums.CaseInsensitiveStrEnum):
    SCHEDULED = "SCHEDULED"
    RUNNING = "RUNNING"
    FINISHED = "FINISHED"
    FAILED = "FAILED"
    KILLED = "KILLED"

    def is_active(self) -> bool:
        return self == RunStatus.SCHEDULED or self == RunStatus.RUNNING


class SupportedBackends(utils.enums.CaseInsensitiveStrEnum):
    UNICORE = enum.auto()
    FIRECREST = enum.auto()
    SSH = enum.auto()


class RunBase(base.Base):
    __abstract__ = True

    @classmethod
    def backpopulation_column(cls) -> str:
        return f"{cls.__tablename__}s"

    name = sqlalchemy.Column(sqlalchemy.String, nullable=False)

    @declarative.declared_attr
    def user_id(cls) -> sqlalchemy.Column:
        return columns.create_foreign_key_column(
            name=user.ID_COLUMN_NAME,
            table=user.TABLE_NAME,
            nullable=False,
        )

    @declarative.declared_attr
    def user(cls) -> sqlalchemy.Column:
        return orm.relationship(user.User.__name__)

    @declarative.declared_attr
    def project_id(cls) -> sqlalchemy.Column:
        return columns.create_foreign_key_column(
            name="project_id",
            table="project",
            primary_key=False,
            nullable=False,
        )

    @declarative.declared_attr
    def experiment_repository_id(cls) -> sqlalchemy.Column:
        return columns.create_foreign_key_column(
            name=experiment_repository.ID_COLUMN_NAME,
            table=experiment_repository.TABLE_NAME,
            nullable=False,
            primary_key=False,
            # According to MLflow, a run will be deleted
            # if its parent experiment gets deleted.
            ondelete="CASCADE",
        )

    @declarative.declared_attr
    def experiment_repository(cls) -> sqlalchemy.Column:
        return orm.relationship(
            experiment_repository.ExperimentRepository.__name__,
            back_populates=cls.backpopulation_column(),
        )

    @declarative.declared_attr
    def data_repository_id(cls) -> sqlalchemy.Column:
        return columns.create_foreign_key_column(
            name=data_repository.ID_COLUMN_NAME,
            table=data_repository.TABLE_NAME,
            nullable=True,
            primary_key=False,
        )

    @declarative.declared_attr
    def data_repository(cls) -> sqlalchemy.Column:
        return orm.relationship(
            data_repository.DataRepository.__name__,
            back_populates=cls.backpopulation_column(),
        )

    compute_budget_account = sqlalchemy.Column(
        sqlalchemy.String,
        doc="Account used for allocating compute resources on the remote system",
    )

    @declarative.declared_attr
    def connection_id(cls) -> sqlalchemy.Column:
        return columns.create_foreign_key_column(
            name=connection.ID_COLUMN_NAME,
            table=connection.TABLE_NAME,
            nullable=True,
            primary_key=False,
            doc="""Connection ID used for submitting the run to a remote system.

            Can be `None` (NULL) for local runs.
            """,
        )

    @declarative.declared_attr
    def connection(cls) -> sqlalchemy.Column:
        return orm.relationship(
            connection.Connection.__name__, back_populates=cls.backpopulation_column()
        )

    mlflow_mlproject_file_path = sqlalchemy.Column(sqlalchemy.String, nullable=True)
    entry_point = sqlalchemy.Column(sqlalchemy.String, nullable=True)
    mlflow_parameters = sqlalchemy.Column(sqlalchemy.dialects.postgresql.JSONB)
    backend_config = sqlalchemy.Column(
        sqlalchemy.dialects.postgresql.JSONB, nullable=False
    )

    @declarative.declared_attr
    def environment_id(cls) -> sqlalchemy.Column:
        return columns.create_foreign_key_column(
            name=environment.ID_COLUMN_NAME,
            table=environment.TABLE_NAME,
            nullable=True,
            primary_key=False,
            ondelete="SET NULL",
        )

    @declarative.declared_attr
    def environment(cls) -> sqlalchemy.Column:
        return orm.relationship(
            environment.Environment.__name__, back_populates=cls.backpopulation_column()
        )


class Run(RunBase, base.BaseWithId):
    __tablename__ = TABLE_NAME

    id = columns.create_id_column(ID_COLUMN_NAME)
    mlflow_run_id = sqlalchemy.Column(sql_utils.UUIDType(binary=False), nullable=False)
    status = sqlalchemy.Column(sqlalchemy.Enum(RunStatus, native_enum=False))
    logs = sqlalchemy.Column(sqlalchemy.String)
    allowed_to_update_logs = sqlalchemy.Column(
        sqlalchemy.Boolean, nullable=False, default=True
    )
    artifact_uri = sqlalchemy.Column(sqlalchemy.String)
    lifecycle_stage = sqlalchemy.Column(sqlalchemy.String)
    model_repository_id = columns.create_foreign_key_column(
        name=model_repository.ID_COLUMN_NAME,
        table=model_repository.TABLE_NAME,
        nullable=True,
        primary_key=False,
    )
    model_repository = orm.relationship(
        model_repository.ModelRepository.__name__, back_populates="runs"
    )
    saved_model_repository = orm.relationship(  # trained model
        saved_model_repository.TrainedModel.__name__,
        back_populates="run",
        uselist=False,
    )
    data_branch = sqlalchemy.Column(sqlalchemy.String, nullable=True)
    data_commit = sqlalchemy.Column(sqlalchemy.String, nullable=True)
    data_target_dir = sqlalchemy.Column(sqlalchemy.String, nullable=True)
    start_time = sqlalchemy.Column(sqlalchemy.BigInteger)
    end_time = sqlalchemy.Column(sqlalchemy.BigInteger)
    remote_system_job_id = sqlalchemy.Column(
        sqlalchemy.String,
        doc="The Job ID on the remote compute system. E.g. UNICORE job ID",
    )
    run_schedules = orm.relationship("RunSchedule", back_populates="run")
    info = sqlalchemy.Column(sqlalchemy.dialects.postgresql.JSONB)
    infrastructure = sqlalchemy.Column(
        sqlalchemy.dialects.postgresql.JSONB, nullable=True
    )
    notebook_source = sqlalchemy.Column(
        sqlalchemy.dialects.postgresql.JSONB, nullable=True
    )

    @property
    def backend_config_environment_resolved(self) -> dict:
        if self.environment_id is not None:
            return inject_environment_into_backend_config(
                self.environment, self.backend_config
            )
        return self.backend_config

    def owned_by_user(self, user_id: uuid.UUID) -> bool:
        return self.user_id == user_id

    def to_overwrite_kwargs(
        self, status: RunStatus, info: dict | None = None
    ) -> dict[str, RunStatus | sqlalchemy.Column]:
        kwargs = {"status": status}
        if info is None:
            return kwargs
        return kwargs | {"info": info}

    def is_immutable_and_has_info(self) -> bool:
        return self.info is not None and not self.is_active()

    def is_immutable_and_has_logs(self) -> bool:
        return self.logs is not None and not self.is_active()

    def is_active(self) -> bool:
        if self.status is None:
            # Run is considered active until status is known.
            return True
        return self.status.is_active()

    def is_local(self) -> bool:
        return (
            self.remote_system_job_id is None
            and self.connection_id is None
            and self.compute_budget_account is None
        )

    def get_logs(self) -> list[str] | None:
        if self.logs:
            return self.logs.split("\n")

    @property
    def backend_type(self) -> SupportedBackends:
        """Infer the backend type from the backend config.

        Returns
        -------
        SupportedBackends
            The Backend type.

        Raises
        ------
        ValueError
            Unable to read backend configuration from backend config

        """
        if "UnicoreApiUrl" in self.backend_config:
            return SupportedBackends.UNICORE
        elif "Firecrest" in self.backend_config:
            return SupportedBackends.FIRECREST
        elif "SSH" in self.backend_config:
            return SupportedBackends.SSH
        raise ValueError(
            "Unable to assume backend: No backend configuration found in "
            "backend config {self.backend_config}"
        )


class RunConfiguration(RunBase, base.BaseWithId):
    __tablename__ = "run_configuration"

    id = columns.create_id_column("run_configuration_id")
    code_repository_id = columns.create_foreign_key_column(
        name=code_repository.ID_COLUMN_NAME,
        table=code_repository.TABLE_NAME,
        nullable=True,
        primary_key=False,
    )
    code_repository = orm.relationship(
        code_repository.CodeRepository.__name__, back_populates="run_configurations"
    )
    branch = sqlalchemy.Column(sqlalchemy.String, nullable=True)
    commit = sqlalchemy.Column(sqlalchemy.String, nullable=True)
    data_branch = sqlalchemy.Column(sqlalchemy.String, nullable=True)
    data_commit = sqlalchemy.Column(sqlalchemy.String, nullable=True)
    data_target_dir = sqlalchemy.Column(sqlalchemy.String, nullable=True)

    @property
    def backend_config_environment_resolved(self) -> dict:
        if self.environment_id is not None:
            return inject_environment_into_backend_config(
                self.environment, self.backend_config
            )
        return self.backend_config


def inject_environment_into_backend_config(
    environment: environment, backend_config: dict
) -> dict:
    location = "remote" if is_remote(environment.data_repository.uri) else "local"
    _environment = {
        environment.type: {"Path": environment.data_repository.uri, "Type": location},
        "Variables": environment.variables,
    }

    return backend_config | {"Environment": _environment}


def is_remote(uri: str) -> bool:
    return uri.lower().startswith(REMOTE_PREFIXES)


REMOTE_PREFIXES = ("unicore", "remote", "hpc", "ssh")  # to be extended in the future
