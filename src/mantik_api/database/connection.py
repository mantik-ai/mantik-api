import enum
import uuid
from enum import Enum

import sqlalchemy
import sqlalchemy.orm as orm

import mantik_api.database.base as base
import mantik_api.database.code_repository as code_repository
import mantik_api.database.columns as columns
import mantik_api.database.data_repository as data_repository
import mantik_api.database.user as user
import mantik_api.utils as utils

TABLE_NAME = "connection"
ID_COLUMN_NAME = "connection_id"


class ConnectionProvider(str, Enum):
    GITHUB = "GitHub"
    GITLAB = "GitLab"
    HPC_JSC = "Jülich Supercomputing Centre (JSC)"
    HPC_E4 = "E4 Computer Engineering (E4)"
    HPC_CSCS = "Swiss National Supercomputing Centre (CSCS)"
    S3 = "S3"
    REMOTE_COMPUTE_SYSTEM = "Remote Compute System"


class OrderBy(base.OrderByBase):
    name = enum.auto()
    provider = enum.auto()
    created = enum.auto()
    updated = enum.auto()

    @property
    def column(self) -> sqlalchemy.Column:
        match self:
            case OrderBy.name:
                return Connection.connection_name
            case OrderBy.provider:
                return Connection.connection_provider
            case OrderBy.created:
                return Connection.created_at
            case OrderBy.updated:
                return Connection.updated_at
            case _:
                raise NotImplementedError(
                    f"{self} has no associated database table column"
                )


class AuthMethod(utils.enums.CaseInsensitiveStrEnum):
    USERNAME_PASSWORD = "Username-Password"
    TOKEN = "Token"
    SSH_KEY = "SSH-Key"


class Connection(base.Base):
    __tablename__ = TABLE_NAME

    id = columns.create_id_column(ID_COLUMN_NAME)
    user_id = columns.create_foreign_key_column(
        name="user_id",
        table=user.TABLE_NAME,
        nullable=False,
        primary_key=False,
        ondelete="CASCADE",
    )
    user = orm.relationship("User", back_populates="connections")
    connection_name = sqlalchemy.Column(sqlalchemy.String, nullable=False)
    connection_provider = sqlalchemy.Column(
        sqlalchemy.Enum(
            ConnectionProvider,
            native_enum=False,
            values_callable=lambda connection_enum_list: [
                connection_enum.value for connection_enum in connection_enum_list
            ],
        ),
        nullable=False,
    )
    auth_method = sqlalchemy.Column(
        sqlalchemy.Enum(
            AuthMethod,
            native_enum=False,
            values_callable=lambda auth_method_enum_list: [
                auth_method_enum.value for auth_method_enum in auth_method_enum_list
            ],
        ),
        nullable=False,
    )
    runs = orm.relationship("Run", back_populates="connection")
    run_configurations = orm.relationship(
        "RunConfiguration", back_populates="connection"
    )
    run_schedules = orm.relationship("RunSchedule", back_populates="connection")
    trained_models = orm.relationship("TrainedModel", back_populates="connection")

    data_repositories = orm.relationship(
        data_repository.DataRepository.__name__,
        back_populates="connections",
        secondary="data_repository_and_connection_association_table",
    )
    code_repositories = orm.relationship(
        code_repository.CodeRepository.__name__,
        back_populates="connections",
        secondary="code_repository_and_connection_association_table",
    )

    def __repr__(self):
        return (
            f"Connection("
            f"id=UUID('{self.id}'), "
            f"connection_name='{self.connection_name}', "
            f"connection_provider='{self.connection_provider.value}')"
        )

    def owned_by_user(self, user_id: uuid.UUID) -> bool:
        return self.user_id == user_id

    @property
    def is_dvc_connection(self) -> bool:
        return self.connection_provider in [ConnectionProvider.S3]

    @property
    def is_git_connection(self) -> bool:
        return self.connection_provider in [
            ConnectionProvider.GITHUB,
            ConnectionProvider.GITLAB,
        ]
