import enum
import typing as t
import uuid

import sqlalchemy
import sqlalchemy.orm as orm

import mantik_api.database.base as base
import mantik_api.database.client.main as main
import mantik_api.database.columns as columns
import mantik_api.database.user as user

TABLE_NAME = "user_group"
ID_COLUMN_NAME = "user_group_id"


class OrderBy(base.OrderByBase):
    name = enum.auto()
    created = enum.auto()
    updated = enum.auto()

    @property
    def column(self) -> sqlalchemy.Column:
        match self:
            case OrderBy.name:
                return UserGroup.name
            case OrderBy.created:
                return UserGroup.created_at
            case OrderBy.updated:
                return UserGroup.updated_at
            case _:
                raise NotImplementedError(
                    f"{self} has no associated database table column"
                )


class UserGroupUserAssociationTable(base.Base):
    __tablename__ = "user_group_user_association_table"
    user_group_id = columns.create_foreign_key_column(
        name=ID_COLUMN_NAME,
        table=TABLE_NAME,
        ondelete="CASCADE",
    )
    user_id = columns.create_foreign_key_column(
        name=user.ID_COLUMN_NAME,
        table=user.TABLE_NAME,
        ondelete="CASCADE",
    )


class UserGroup(base.BaseWithId):
    __tablename__ = TABLE_NAME

    id = columns.create_id_column(ID_COLUMN_NAME)
    name = sqlalchemy.Column(sqlalchemy.String, nullable=False)
    admin_id = columns.create_foreign_key_column(
        name=user.ID_COLUMN_NAME,
        table=user.TABLE_NAME,
        nullable=False,
        primary_key=False,
    )
    admin = sqlalchemy.orm.relationship(
        user.User.__name__, back_populates="owned_groups"
    )
    members = orm.relationship(
        "User",
        secondary=UserGroupUserAssociationTable.table,
        back_populates="groups",
    )
    organizations = orm.relationship(
        "Organization",
        secondary="organization_user_group_association_table",
        back_populates="groups",
    )
    projects = orm.relationship(
        "Project",
        secondary="user_group_project_association_table",
        back_populates="project_user_groups",
    )

    def __repr__(self):
        return f"{self.__class__.__name__}({self.id=}, {self.name=})".replace(
            "self.", ""
        )

    @staticmethod
    def create_constraint_for_user_groups(
        user_: user.User,
    ) -> sqlalchemy.sql.elements.BooleanClauseList:
        """Create a constraint that gets all groups of a user."""

        def get_ids(column: sqlalchemy.Column) -> t.Iterator[uuid.UUID]:
            return (row.id for row in column)

        return sqlalchemy.or_(
            # User is in a group
            UserGroup.id.in_(get_ids(user_.groups)),
            # User owns a group
            UserGroup.id.in_(get_ids(user_.owned_groups)),
        )

    def contains_user(self, user_id: uuid.UUID) -> bool:
        group_user_ids = [member.id for member in self.members] + [self.admin_id]
        return user_id in group_user_ids


def get_group_by_id(db: main.Client, group_id: uuid.UUID) -> UserGroup:
    return db.get_one(
        orm_model_type=UserGroup,
        constraints={UserGroup.id: group_id},
    )
