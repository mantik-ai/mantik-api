import enum

import sqlalchemy
import sqlalchemy_utils as sql_utils


def create_id_column(name: str) -> sqlalchemy.Column:
    """Create an ID column."""
    return sqlalchemy.Column(
        sql_utils.UUIDType(binary=False), name=name, primary_key=True, unique=True
    )


def create_foreign_key_column(
    name: str,
    table: str,
    column: str | None = None,
    primary_key: bool = True,
    nullable: bool = False,
    ondelete: str | None = None,
    doc: str | None = None,
) -> sqlalchemy.Column:
    """Create a foreign key to a given table column."""
    return sqlalchemy.Column(
        sqlalchemy.ForeignKey(f"{table}.{column or name}", ondelete=ondelete),
        name=name,
        primary_key=primary_key,
        nullable=nullable,
        doc=doc,
    )


def create_created_timestamp_column() -> sqlalchemy.Column:
    return sqlalchemy.Column(
        sqlalchemy.DateTime(timezone=True),
        name="created_at",
        server_default=sqlalchemy.sql.func.now(),
    )


def create_updated_timestamp_column() -> sqlalchemy.Column:
    return sqlalchemy.Column(
        sqlalchemy.DateTime(timezone=True),
        name="updated_at",
        onupdate=sqlalchemy.sql.func.now(),
    )


def create_string_enum(values: type[enum.StrEnum]) -> sqlalchemy.Enum:
    return sqlalchemy.Enum(
        values,
        native_enum=False,
    )
