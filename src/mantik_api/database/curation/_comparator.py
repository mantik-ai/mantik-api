import typing as t

import mantik_api.database.curation._model as _model
import mantik_api.database.label as _label


def get_identifiers(
    labels: t.Iterable[_label.Label] | t.Iterable[_model.Label],
) -> set[str]:
    return {label.identifier for label in labels}


def get_labels_to_delete(
    yaml_identifiers: set[str], database_identifiers: set[str]
) -> set[str]:
    return database_identifiers - yaml_identifiers
