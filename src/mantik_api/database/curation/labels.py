import logging
import pathlib

import mantik_api.database as database
import mantik_api.database.curation._comparator as _comparator
import mantik_api.database.curation._model as _model
import mantik_api.database.curation._parse as _parse
import mantik_api.utils.env as env


logger = logging.getLogger(__name__)

LABELS_YAML_PATH_ENV_VAR = "LABELS_YAML_PATH"


def crud_labels() -> None:
    logger.info("Attempting to curate labels")

    path_as_str = env.get_optional_env_var(LABELS_YAML_PATH_ENV_VAR)

    if path_as_str is None:
        logger.warning(
            "Skipping curation of labels since '%s' env var unset",
            LABELS_YAML_PATH_ENV_VAR,
        )
        return

    path = pathlib.Path(path_as_str)

    if not path.exists():
        logger.exception("No labels YAML found at given path %s", path)
        return

    try:
        client = database.client.main.Client.from_env()
        _crud_labels_from_yaml(path=path, client=client)
    except:  # noqa: E722
        logger.exception("Labels curation failed", exc_info=True)


def _crud_labels_from_yaml(
    path: pathlib.Path, client: database.client.main.Client
) -> None:
    yaml_labels = _parse.parse_yaml_to_labels(path)

    logger.debug("Parsed labels from YAML: %s", yaml_labels)

    with client.session():
        database_identifiers = _get_database_labels_identifiers(client)
        _delete_labels(
            labels=yaml_labels, database_identifiers=database_identifiers, client=client
        )
        _update_or_create_labels(
            labels=yaml_labels,
            database_identifiers=database_identifiers,
            client=client,
        )


def _get_database_labels_identifiers(client: database.client.main.Client) -> set[str]:
    result: database.client.main.GetAllResponse = client.get_all(database.label.Label)
    return _comparator.get_identifiers(result.entities)


def _delete_labels(
    labels: list[_model.Label],
    database_identifiers: set[str],
    client: database.client.main.Client,
) -> None:
    yaml_identifiers = _comparator.get_identifiers(labels)
    to_delete = _comparator.get_labels_to_delete(
        yaml_identifiers=yaml_identifiers, database_identifiers=database_identifiers
    )
    _delete_labels_from_database(to_delete, client=client)


def _delete_labels_from_database(
    identifiers: set[str], client: database.client.main.Client
) -> None:
    logger.debug("Deleting labels with identifiers %s", identifiers)
    for identifier in identifiers:
        client.delete(
            database.label.Label,
            constraints={database.label.Label.identifier: identifier},
        )


def _update_or_create_labels(
    labels: list[_model.Label],
    database_identifiers: set[str],
    client: database.client.main.Client,
) -> None:
    for label in labels:
        if _label_exists(label, database_identifiers=database_identifiers):
            _update_existing_label(label, client=client)
        else:
            _add_new_label(label, client=client)


def _update_existing_label(
    label: _model.Label, client: database.client.main.Client
) -> None:
    old_label = client.get_one(
        database.label.Label,
        constraints={database.label.Label.identifier: label.identifier},
    )
    logger.debug("Updating label %s to %s", old_label, label)
    client.overwrite(
        old_label,
        fields_to_overwrite=label.to_overwrite_kwargs(),
    )


def _add_new_label(label: _model.Label, client: database.client.main.Client) -> None:
    logger.debug("Adding new label %s", label)
    db_model = label.to_database_model()
    client.add(db_model)


def _label_exists(label: _model.Label, database_identifiers: set[str]) -> bool:
    return label.identifier in database_identifiers
