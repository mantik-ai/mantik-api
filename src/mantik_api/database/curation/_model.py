import typing as t
import uuid

import pydantic

import mantik_api.database as database


class Label(pydantic.BaseModel):
    identifier: str
    scopes: list[database.label.Scope]
    category: database.label.Category
    sub_category: database.label.SubCategory | None
    name: str

    def __hash__(self) -> int:
        return hash(self.identifier)

    def __eq__(self, other: t.Self) -> bool:
        return self.identifier == other.identifier

    def to_database_model(self) -> database.label.Label:
        return database.label.Label(
            id=uuid.uuid4(),
            identifier=self.identifier,
            scopes=self.scopes,
            category=self.category,
            sub_category=self.sub_category,
            name=self.name,
        )

    def to_overwrite_kwargs(self) -> dict[str, t.Any]:
        return self.dict(exclude_unset=False, exclude={"identifier"})
