import pathlib

import yaml

import mantik_api.database.curation._model as _model

N_CHARS_IDENTIFIER = 4


def parse_yaml_to_labels(path: pathlib.Path) -> list[_model.Label]:
    data = _read_yaml(path)
    return _parse_labels(data)


def _label_identifiers_unique(labels: list[_model.Label]) -> bool:
    return len(set(labels)) == len(labels)


def _read_yaml(path: pathlib.Path) -> dict:
    with open(path) as f:
        return yaml.safe_load(f.read())


def _parse_labels(data: dict) -> list[_model.Label]:
    labels = [
        label
        for category, sections in data.items()
        for label in _parse_category_labels(category=category, sections=sections)
    ]
    if not _label_identifiers_unique(labels):
        duplicates = {label.identifier for label in labels if labels.count(label) > 1}
        raise ValueError(f"Label identifiers must be unique. Duplicates: {duplicates}")
    return labels


def _parse_category_labels(category: str, sections: dict) -> list[_model.Label]:
    scopes = sections["scopes"]
    if "subCategories" in sections:
        return _parse_sub_categories(
            sub_categories=sections["subCategories"],
            category=category,
            scopes=scopes,
        )
    return _parse_labels_section(
        sections["labels"],
        category=category,
        scopes=scopes,
    )


def _parse_sub_categories(
    sub_categories: dict, category: str, scopes: list[str]
) -> list[_model.Label]:
    return [
        label
        for sub_category, sections in sub_categories.items()
        for label in _parse_labels_section(
            section=sections["labels"],
            scopes=scopes if "scopes" not in sections else sections["scopes"],
            category=category,
            sub_category=sub_category,
        )
    ]


def _parse_labels_section(
    section: dict,
    category: str,
    scopes: list[str],
    sub_category: str | None = None,
) -> list[_model.Label]:
    return [
        _parse_label(
            entry=entry,
            scopes=scopes,
            category=category,
            sub_category=sub_category,
        )
        for entry in section
    ]


def _parse_label(
    entry: dict,
    scopes: list[str],
    category: str,
    sub_category: str | None = None,
) -> _model.Label:
    identifier = entry["identifier"]
    if not isinstance(identifier, str):
        raise ValueError(
            f"Identifier must be string, {type(identifier)} "
            f"given for identifier {identifier}"
        )
    elif not len(identifier) == N_CHARS_IDENTIFIER:
        raise ValueError(
            f"Identifier {identifier} must have {N_CHARS_IDENTIFIER} characters"
        )
    return _model.Label(
        identifier=identifier,
        scopes=scopes,
        category=category,
        sub_category=sub_category,
        name=entry["name"],
    )
