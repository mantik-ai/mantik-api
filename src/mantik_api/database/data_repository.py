import enum
import uuid

import sqlalchemy.dialects
import sqlalchemy.orm as orm

import mantik_api.database.base as base
import mantik_api.database.columns as columns
import mantik_api.database.git_repository as git_repository
import mantik_api.database.label as label

TABLE_NAME = "data_repository"
ID_COLUMN_NAME = "data_repository_id"


class OrderBy(base.OrderByBase):
    name = enum.auto()
    uri = enum.auto()
    created = enum.auto()
    updated = enum.auto()

    @property
    def column(self) -> sqlalchemy.Column:
        match self:
            case OrderBy.name:
                return DataRepository.data_repository_name
            case OrderBy.uri:
                return DataRepository.uri
            case OrderBy.created:
                return DataRepository.created_at
            case OrderBy.updated:
                return DataRepository.updated_at
            case _:
                raise NotImplementedError(
                    f"{self} has no associated database table column"
                )


class DataRepositoryLabelAssociationTable(base.BaseLabelsAssociation):
    __tablename__ = "data_repository_label_association_table"
    model_id = columns.create_foreign_key_column(
        name=ID_COLUMN_NAME,
        table=TABLE_NAME,
        ondelete="CASCADE",
    )


class DataRepositoryAndConnectionAssociationTable(base.Base):
    __tablename__ = "data_repository_and_connection_association_table"
    id = columns.create_id_column("data_repository_and_connection_association_table_id")
    data_repository_id = columns.create_foreign_key_column(
        name=ID_COLUMN_NAME,
        table=TABLE_NAME,
        ondelete="CASCADE",
    )
    connection_id = columns.create_foreign_key_column(
        name="connection_id",
        table="connection",
        ondelete="CASCADE",
    )


class DataRepository(base.BaseWithId):
    __tablename__ = TABLE_NAME

    id = columns.create_id_column(ID_COLUMN_NAME)
    uri = sqlalchemy.Column(sqlalchemy.String, nullable=False)
    data_repository_name = sqlalchemy.Column(sqlalchemy.String)
    access_token = sqlalchemy.Column(sqlalchemy.String)
    description = sqlalchemy.Column(sqlalchemy.String)
    labels = orm.relationship(
        label.Label.__name__, secondary=DataRepositoryLabelAssociationTable.table
    )
    project_id = columns.create_foreign_key_column(
        name="project_id",
        table="project",
        nullable=False,
        primary_key=False,
        ondelete="CASCADE",
    )
    project = orm.relationship("Project", back_populates="data_repositories")
    runs = orm.relationship("Run", back_populates="data_repository")
    run_configurations = orm.relationship(
        "RunConfiguration", back_populates="data_repository"
    )
    environments = orm.relationship("Environment", back_populates="data_repository")
    platform = sqlalchemy.Column(
        columns.create_string_enum(git_repository.Platform),
        nullable=False,
        default=git_repository.Platform.GitHub,
    )
    is_dvc_enabled = sqlalchemy.Column(
        sqlalchemy.Boolean, nullable=False, default=False
    )
    versions = sqlalchemy.Column(sqlalchemy.dialects.postgresql.JSONB, nullable=True)
    connections = orm.relationship(
        "Connection",
        back_populates="data_repositories",
        secondary=DataRepositoryAndConnectionAssociationTable.table,
    )

    def my_git_connection_id(self, user_id: uuid.UUID) -> uuid.UUID | None:
        for _connection in self.connections:
            if (
                _connection.owned_by_user(user_id=user_id)
                and _connection.is_git_connection
            ):
                return _connection.id
        return None

    def my_dvc_connection_id(self, user_id: uuid.UUID) -> uuid.UUID | None:
        for _connection in self.connections:
            if (
                _connection.owned_by_user(user_id=user_id)
                and _connection.is_dvc_connection
            ):
                return _connection.id
        return None
