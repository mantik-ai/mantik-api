import enum

import sqlalchemy
import sqlalchemy_utils as sql_utils

import mantik_api.database.base as base
import mantik_api.database.columns as columns
import mantik_api.database.organization as organization
import mantik_api.database.project as project
import mantik_api.database.user as user
import mantik_api.database.user_group as user_group

TABLE_NAME = "invitation"
ID_COLUMN_NAME = "invitation_id"


class OrderBy(base.OrderByBase):
    type = enum.auto()
    invited_to = enum.auto()
    role = enum.auto()
    sent = enum.auto()
    updated = enum.auto()

    @property
    def column(self) -> sqlalchemy.Column:
        match self:
            case OrderBy.type:
                return Invitation.invited_type
            case OrderBy.invited_to:
                return Invitation.invited_to_type
            case OrderBy.role:
                return Invitation.role
            case OrderBy.sent:
                return Invitation.created_at
            case OrderBy.updated:
                return Invitation.updated_at
            case _:
                raise NotImplementedError(
                    f"{self} has no associated database table column"
                )


class InvitedToType(enum.Enum):
    PROJECT = "PROJECT"
    ORGANIZATION = "ORGANIZATION"
    GROUP = "GROUP"


class InvitedType(enum.Enum):
    USER = "USER"
    ORGANIZATION = "ORGANIZATION"
    GROUP = "GROUP"


class Invitation(base.BaseWithId):
    __tablename__ = TABLE_NAME

    id = columns.create_id_column(ID_COLUMN_NAME)
    # Store ID of invitation issuer
    inviter_id = columns.create_foreign_key_column(
        name="inviter_id",
        column=user.ID_COLUMN_NAME,
        table=user.TABLE_NAME,
        nullable=False,
        primary_key=False,
        ondelete="CASCADE",
    )
    # Store ID of invited user (or admin of group, organization contact)
    invited_id = sqlalchemy.Column(
        sql_utils.UUIDType(binary=False), primary_key=False, nullable=False
    )
    invited_type = sqlalchemy.Column(
        sqlalchemy.Enum(InvitedType),
        nullable=False,
        server_default=InvitedType.USER.name,
    )
    # Store type of inviting entity
    invited_to_type = sqlalchemy.Column(sqlalchemy.Enum(InvitedToType), nullable=False)
    role = sqlalchemy.Column(sqlalchemy.Enum(project.ProjectRole))
    invited_to_id = sqlalchemy.Column(
        sql_utils.UUIDType(binary=False), primary_key=False, nullable=False
    )
    __table_args__ = (
        sqlalchemy.UniqueConstraint(
            "invited_id",
            "invited_type",
            "invited_to_type",
            "invited_to_id",
            name="unique_invitation_constraint",
        ),
    )

    def to_association_table(self):
        match (self.invited_type, self.invited_to_type):
            case (InvitedType.USER, InvitedToType.GROUP):
                return user_group.UserGroupUserAssociationTable(
                    user_group_id=self.invited_to_id, user_id=self.invited_id
                )
            case (InvitedType.USER, InvitedToType.ORGANIZATION):
                return organization.OrganizationUserAssociationTable(
                    organization_id=self.invited_to_id, user_id=self.invited_id
                )
            case (InvitedType.USER, InvitedToType.PROJECT):
                return project.UserProjectAssociationTable(
                    project_id=self.invited_to_id,
                    user_id=self.invited_id,
                    role=self.role,
                )
            case (InvitedType.GROUP, InvitedToType.ORGANIZATION):
                return organization.OrganizationUserGroupAssociationTable(
                    user_group_id=self.invited_id, organization_id=self.invited_to_id
                )
            case (InvitedType.GROUP, InvitedToType.PROJECT):
                return project.UserGroupProjectAssociationTable(
                    user_group_id=self.invited_id,
                    project_id=self.invited_to_id,
                    role=self.role,
                )
            case (InvitedType.ORGANIZATION, InvitedToType.PROJECT):
                return project.OrganizationProjectAssociationTable(
                    organization_id=self.invited_id,
                    project_id=self.invited_to_id,
                    role=self.role,
                )
            case _:
                raise ValueError(
                    f"{self.invited_type} cannot be invited to {self.invited_to_type}."
                )
