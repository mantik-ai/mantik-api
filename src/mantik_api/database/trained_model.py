import enum

import sqlalchemy.dialects.postgresql
import sqlalchemy.ext.declarative as declarative
import sqlalchemy.orm as orm

import mantik_api.database.base as base
import mantik_api.database.columns as columns
import mantik_api.database.connection as connection
import mantik_api.utils as utils

TABLE_NAME = "trained_model"
ID_COLUMN_NAME = "model_id"


class ContainerBuildStatus(utils.enums.CaseInsensitiveStrEnum):
    PENDING = "PENDING"
    BUILDING = "BUILDING"
    SUCCESSFUL = "SUCCESSFUL"
    FAILED = "FAILED"


class OrderBy(base.OrderByBase):
    uri = enum.auto()
    location = enum.auto()
    created = enum.auto()
    updated = enum.auto()

    @property
    def column(self) -> sqlalchemy.Column:
        match self:
            case OrderBy.uri:
                return TrainedModel.uri
            case OrderBy.location:
                return TrainedModel.location
            case OrderBy.created:
                return TrainedModel.created_at
            case OrderBy.updated:
                return TrainedModel.updated_at
            case _:
                raise NotImplementedError(
                    f"{self} has no associated database table column"
                )


class TrainedModel(base.BaseWithId):
    """Trained Model"""

    __tablename__ = TABLE_NAME

    __table_args__ = (
        sqlalchemy.UniqueConstraint(
            "name",
            "project_id",
            name="unique_model_name_in_project_constraint",
        ),
    )

    id = columns.create_id_column(ID_COLUMN_NAME)
    name = sqlalchemy.Column(sqlalchemy.String, nullable=False)
    run_id = columns.create_foreign_key_column(
        name="run_id",
        table="run",
        nullable=True,
        primary_key=False,
    )
    run = orm.relationship(
        "Run",
        back_populates="saved_model_repository",
        uselist=False,
    )
    uri = sqlalchemy.Column(sqlalchemy.String)
    location = sqlalchemy.Column(sqlalchemy.String, nullable=False)
    mlflow_parameters = sqlalchemy.Column(
        sqlalchemy.dialects.postgresql.JSONB, nullable=True
    )
    status = sqlalchemy.Column(
        sqlalchemy.Enum(ContainerBuildStatus, native_enum=False), nullable=True
    )

    @declarative.declared_attr
    def connection_id(cls) -> sqlalchemy.Column:
        return columns.create_foreign_key_column(
            name=connection.ID_COLUMN_NAME,
            table=connection.TABLE_NAME,
            nullable=True,
            primary_key=False,
        )

    @declarative.declared_attr
    def connection(cls) -> sqlalchemy.Column:
        return orm.relationship(
            connection.Connection.__name__, back_populates="trained_models"
        )

    project_id = columns.create_foreign_key_column(
        name="project_id",
        table="project",
        nullable=False,
        primary_key=False,
        ondelete="CASCADE",
    )
    project = orm.relationship("Project", back_populates="trained_models")
