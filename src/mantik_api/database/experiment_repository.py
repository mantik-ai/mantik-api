import enum

import sqlalchemy
import sqlalchemy.orm as orm

import mantik_api.database.base as base
import mantik_api.database.columns as columns
import mantik_api.database.label as label

TABLE_NAME = "experiment_repository"
ID_COLUMN_NAME = "experiment_repository_id"


class OrderBy(base.OrderByBase):
    name = enum.auto()
    mlflow_experiment_id = enum.auto()
    created = enum.auto()
    updated = enum.auto()

    @property
    def column(self) -> sqlalchemy.Column:
        match self:
            case OrderBy.name:
                return ExperimentRepository.name
            case OrderBy.mlflow_experiment_id:
                return ExperimentRepository.mlflow_experiment_id
            case OrderBy.created:
                return ExperimentRepository.created_at
            case OrderBy.updated:
                return ExperimentRepository.updated_at
            case _:
                raise NotImplementedError(
                    f"{self} has no associated database table column"
                )


class ExperimentLabelAssociationTable(base.BaseLabelsAssociation):
    __tablename__ = "experiment_repository_label_association_table"
    model_id = columns.create_foreign_key_column(
        name=ID_COLUMN_NAME,
        table=TABLE_NAME,
        ondelete="CASCADE",
    )


class ExperimentRepository(base.BaseWithId):
    __tablename__ = TABLE_NAME

    id = columns.create_id_column(ID_COLUMN_NAME)
    mlflow_experiment_id = sqlalchemy.Column(sqlalchemy.Integer, nullable=False)
    name = sqlalchemy.Column(sqlalchemy.String, nullable=False)
    artifact_location = sqlalchemy.Column(sqlalchemy.String)
    lifecycle_stage = sqlalchemy.Column(sqlalchemy.String)
    last_update_time = sqlalchemy.Column(sqlalchemy.BigInteger)
    creation_time = sqlalchemy.Column(sqlalchemy.BigInteger)
    labels = orm.relationship(
        label.Label.__name__, secondary=ExperimentLabelAssociationTable.table
    )
    project_id = columns.create_foreign_key_column(
        name="project_id",
        table="project",
        nullable=False,
        primary_key=False,
        ondelete="CASCADE",
    )
    project = orm.relationship("Project", back_populates="experiment_repositories")
    runs = orm.relationship("Run", back_populates="experiment_repository")
    run_configurations = orm.relationship(
        "RunConfiguration", back_populates="experiment_repository"
    )
