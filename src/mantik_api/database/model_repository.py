import enum

import sqlalchemy
import sqlalchemy.orm as orm

import mantik_api.database.base as base
import mantik_api.database.code_repository as code_repository
import mantik_api.database.columns as columns
import mantik_api.database.label as label

TABLE_NAME = "model_repository"
ID_COLUMN_NAME = "model_repository_id"


class OrderBy(base.OrderByBase):
    uri = enum.auto()
    branch = enum.auto()
    commit = enum.auto()
    created = enum.auto()
    updated = enum.auto()

    @property
    def column(self) -> sqlalchemy.Column:
        match self:
            case OrderBy.uri:
                return ModelRepository.uri
            case OrderBy.branch:
                return ModelRepository.branch
            case OrderBy.commit:
                return ModelRepository.commit
            case OrderBy.created:
                return ModelRepository.created_at
            case OrderBy.updated:
                return ModelRepository.updated_at
            case _:
                raise NotImplementedError(
                    f"{self} has no associated database table column"
                )


class ModelRepositoryLabelAssociationTable(base.BaseLabelsAssociation):
    __tablename__ = "model_repository_label_association_table"
    model_id = columns.create_foreign_key_column(
        name=ID_COLUMN_NAME,
        table=TABLE_NAME,
        ondelete="CASCADE",
    )


class ModelRepository(base.BaseWithId):
    __tablename__ = TABLE_NAME

    id = columns.create_id_column(ID_COLUMN_NAME)
    uri = sqlalchemy.Column(sqlalchemy.String)
    description = sqlalchemy.Column(sqlalchemy.String)
    code_repository_id = columns.create_foreign_key_column(
        name=code_repository.ID_COLUMN_NAME,
        table=code_repository.TABLE_NAME,
        nullable=True,
        primary_key=False,
    )
    code_repository = orm.relationship(
        "CodeRepository", back_populates="model_repositories"
    )
    branch = sqlalchemy.Column(sqlalchemy.String)
    commit = sqlalchemy.Column(sqlalchemy.String)
    labels = sqlalchemy.orm.relationship(
        label.Label.__name__,
        secondary=ModelRepositoryLabelAssociationTable.table,
    )
    project_id = columns.create_foreign_key_column(
        name="project_id",
        table="project",
        nullable=False,
        primary_key=False,
        ondelete="CASCADE",
    )
    project = orm.relationship("Project", back_populates="model_repositories")
    runs = orm.relationship("Run", back_populates="model_repository")
