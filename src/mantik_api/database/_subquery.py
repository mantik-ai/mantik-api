import sqlalchemy
import sqlalchemy.dialects.postgresql as postgresql

import mantik_api.database.base as base


def create_subquery_for_project_entity_with_labels(
    table: type[base.BaseWithId],
    labels_association_table: type[base.BaseLabelsAssociation],
    name: str,
) -> sqlalchemy.sql.Subquery:
    """Create a subquery that groups all labels in an array per entitiy.

    Returns
    -------
    sqlalchemy.sql.Subquery
        SQL subquery with columns:

        - ``<name>_id``: ID of the entity.
        - ``project_id``: ID of the project that entity is assigned to.
        - ``<name>_label_ids``: IDs of all labels assigned to that entity.

    Notes
    -----
    Each entity **must** be related to a project per `project_id` attribute.

    """
    # ``BaseLabelsAssociation.model_id`` is _not_ a property, but a column
    model_id: sqlalchemy.Column = labels_association_table.model_id
    return (
        sqlalchemy.select(
            model_id.label(f"{name}_id"),
            table.project_id.label("project_id"),
            postgresql.array_agg(
                sqlalchemy.distinct(labels_association_table.label_id)
            ).label(f"{name}_label_ids"),
        )
        .join(
            table,
            table.id == labels_association_table.model_id,
        )
        .group_by(
            labels_association_table.model_id,
            table.project_id,
        )
        .subquery()
    )
