import abc
import typing as t

import sqlalchemy
import sqlalchemy.orm as orm

import mantik_api.database.columns as columns
import mantik_api.utils as utils

Query = t.TypeVar(
    "Query",
    sqlalchemy.sql.Insert,
    sqlalchemy.sql.Select,
    sqlalchemy.sql.Update,
    sqlalchemy.sql.Delete,
)


class OrderByBase(utils.enums.CaseInsensitiveStrEnum):
    @property
    @abc.abstractmethod
    def column(self) -> sqlalchemy.Column:
        raise NotImplementedError


class DeclarativeABC(orm.DeclarativeMeta, abc.ABC):
    """Allows to add :class:`abc.ABC` to the declarative base class."""


class Base(orm.declarative_base(metaclass=DeclarativeABC)):
    """ABC for database models."""

    __abstract__ = True

    created_at = columns.create_created_timestamp_column()
    updated_at = columns.create_updated_timestamp_column()

    @classmethod
    @property
    def table(cls) -> sqlalchemy.Table:
        """Return the :class:`sqlalchemy.Table` of the model."""
        return cls.__table__

    @classmethod
    @property
    def columns(cls) -> sqlalchemy.sql.base.ImmutableColumnCollection:
        """Return the table's columns."""
        return cls.table.columns

    @classmethod
    @property
    def primary_key(cls) -> sqlalchemy.sql.base.ImmutableColumnCollection:
        """Return the table's primary key columns."""
        return cls.table.primary_key


class BaseWithId(Base):
    """ABC for database models with an ID column as primary key."""

    __abstract__ = True

    def __repr__(self):
        return f"{self.__class__.__name__}(" f"{self.id=}" f")".replace("self.", "")

    @property
    @abc.abstractmethod
    def id(self) -> sqlalchemy.Column:
        """The ID column."""


class BaseLabelsAssociation(Base):
    """ABC for association tables."""

    __abstract__ = True

    @orm.declared_attr
    def label_id(self) -> sqlalchemy.Column:
        return columns.create_foreign_key_column(
            table="label",
            name="label_id",
            # Setting `ondelete="CASCADE"` here, will delete
            # the label reference in a related object when
            # the label is deleted, but the related object
            # will be retained.
            ondelete="CASCADE",
        )

    @property
    @abc.abstractmethod
    def model_id(self) -> sqlalchemy.Column:
        """Define the name of the model associated with the label."""
