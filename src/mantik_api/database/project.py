import enum
import typing as t
import uuid

import sqlalchemy.dialects.postgresql as postgresql
import sqlalchemy.orm as orm
import sqlalchemy.sql.elements

import mantik_api.database._subquery as _subquery
import mantik_api.database.base as base
import mantik_api.database.code_repository as code_repository
import mantik_api.database.columns as columns
import mantik_api.database.data_repository as data_repository
import mantik_api.database.experiment_repository as experiment_repository
import mantik_api.database.label as label
import mantik_api.database.model_repository as model_repository
import mantik_api.database.organization as organization
import mantik_api.database.trained_model as saved_model_repository
import mantik_api.database.user as user
import mantik_api.database.user_group as user_group
from mantik_api.utils.functions import flat_map

TABLE_NAME = "project"
ID_COLUMN_NAME = "project_id"


class ProjectRole(enum.Enum):
    GUEST = enum.auto()
    REPORTER = enum.auto()
    RESEARCHER = enum.auto()
    MAINTAINER = enum.auto()
    OWNER = enum.auto()


class OrderBy(base.OrderByBase):
    name = enum.auto()
    created = enum.auto()
    updated = enum.auto()

    @property
    def column(self) -> sqlalchemy.Column:
        match self:
            case OrderBy.name:
                return Project.name
            case OrderBy.created:
                return Project.created_at
            case OrderBy.updated:
                return Project.updated_at
            case _:
                raise NotImplementedError(
                    f"{self} has no associated database table column"
                )


class OrderMembersBy(base.OrderByBase):
    role = enum.auto()
    added = enum.auto()
    updated = enum.auto()

    @property
    def column(self) -> sqlalchemy.Column:
        match self:
            case OrderMembersBy.role:
                return UserProjectAssociationTable.role
            case OrderMembersBy.added:
                return UserProjectAssociationTable.created_at
            case OrderMembersBy.updated:
                return UserProjectAssociationTable.updated_at
            case _:
                raise NotImplementedError(
                    f"{self} has no associated database table column"
                )


class OrderUserGroupsBy(base.OrderByBase):
    role = enum.auto()
    added = enum.auto()
    updated = enum.auto()

    @property
    def column(self) -> sqlalchemy.Column:
        match self:
            case OrderUserGroupsBy.role:
                return UserGroupProjectAssociationTable.role
            case OrderUserGroupsBy.added:
                return UserGroupProjectAssociationTable.created_at
            case OrderUserGroupsBy.updated:
                return UserGroupProjectAssociationTable.updated_at
            case _:
                raise NotImplementedError(
                    f"{self} has no associated database table column"
                )


class OrderOrganizationsBy(base.OrderByBase):
    role = enum.auto()
    added = enum.auto()
    updated = enum.auto()

    @property
    def column(self) -> sqlalchemy.Column:
        match self:
            case OrderOrganizationsBy.role:
                return OrganizationProjectAssociationTable.role
            case OrderOrganizationsBy.added:
                return OrganizationProjectAssociationTable.created_at
            case OrderOrganizationsBy.updated:
                return OrganizationProjectAssociationTable.updated_at
            case _:
                raise NotImplementedError(
                    f"{self} has no associated database table column"
                )


class UserProjectAssociationTable(base.Base):
    __tablename__ = "user_project_association_table"

    role = sqlalchemy.Column(sqlalchemy.Enum(ProjectRole))
    user_id = columns.create_foreign_key_column(
        name=user.ID_COLUMN_NAME,
        table=user.TABLE_NAME,
        ondelete="CASCADE",
    )
    project_id = columns.create_foreign_key_column(
        name=ID_COLUMN_NAME,
        table=TABLE_NAME,
        ondelete="CASCADE",
    )


class UserGroupProjectAssociationTable(base.Base):
    __tablename__ = "user_group_project_association_table"

    role = sqlalchemy.Column(sqlalchemy.Enum(ProjectRole))
    user_group_id = columns.create_foreign_key_column(
        name=user_group.ID_COLUMN_NAME,
        table=user_group.TABLE_NAME,
        ondelete="CASCADE",
    )
    project_id = columns.create_foreign_key_column(
        name=ID_COLUMN_NAME,
        table=TABLE_NAME,
        ondelete="CASCADE",
    )


class OrganizationProjectAssociationTable(base.Base):
    __tablename__ = "organization_project_association_table"

    role = sqlalchemy.Column(sqlalchemy.Enum(ProjectRole))
    organization_id = columns.create_foreign_key_column(
        name=organization.ID_COLUMN_NAME,
        table=organization.TABLE_NAME,
        ondelete="CASCADE",
    )
    project_id = columns.create_foreign_key_column(
        name=ID_COLUMN_NAME,
        table=TABLE_NAME,
        ondelete="CASCADE",
    )


class ProjectLabelAssociationTable(base.BaseLabelsAssociation):
    __tablename__ = "project_label_association_table"
    model_id = columns.create_foreign_key_column(
        name=ID_COLUMN_NAME,
        table=TABLE_NAME,
        ondelete="CASCADE",
    )


class Project(base.BaseWithId):
    __tablename__ = TABLE_NAME

    id = columns.create_id_column(ID_COLUMN_NAME)
    name = sqlalchemy.Column(sqlalchemy.String, nullable=False)
    owner_id = columns.create_foreign_key_column(
        name="owner_id",
        column=user.ID_COLUMN_NAME,
        table=user.TABLE_NAME,
        nullable=False,
        primary_key=False,
    )
    owner = sqlalchemy.orm.relationship(
        user.User.__name__, back_populates="owned_projects"
    )
    executive_summary = sqlalchemy.Column(sqlalchemy.String)
    detailed_description = sqlalchemy.Column(sqlalchemy.String)
    labels = sqlalchemy.orm.relationship(
        label.Label.__name__, secondary=ProjectLabelAssociationTable.table
    )
    public = sqlalchemy.Column(sqlalchemy.Boolean, nullable=False, default=False)
    code_repositories = orm.relationship(
        code_repository.CodeRepository.__name__,
        back_populates="project",
        passive_deletes=True,
    )
    experiment_repositories = orm.relationship(
        experiment_repository.ExperimentRepository.__name__,
        back_populates="project",
        passive_deletes=True,
    )
    model_repositories = orm.relationship(
        model_repository.ModelRepository.__name__,
        back_populates="project",
        passive_deletes=True,
    )
    trained_models = orm.relationship(
        saved_model_repository.TrainedModel.__name__,
        back_populates="project",
        passive_deletes=True,
    )
    data_repositories = orm.relationship(
        data_repository.DataRepository.__name__,
        back_populates="project",
        passive_deletes=True,
    )
    project_members = orm.relationship(
        user.User.__name__,
        secondary=UserProjectAssociationTable.table,
        back_populates="projects",
        passive_deletes=True,
    )
    project_user_groups = orm.relationship(
        user_group.UserGroup.__name__,
        secondary=UserGroupProjectAssociationTable.table,
        back_populates="projects",
        passive_deletes=True,
    )
    project_organizations = orm.relationship(
        organization.Organization.__name__,
        secondary=OrganizationProjectAssociationTable.table,
        back_populates="projects",
        passive_deletes=True,
    )

    @staticmethod
    def create_constraint_for_user_accessible_projects_with_query_parameter(
        user_: user.User, labels: list[uuid.UUID] | None, words: list[str] | None
    ) -> sqlalchemy.sql.elements.BooleanClauseList:
        return sqlalchemy.and_(
            Project.create_constraint_for_user_accessible_projects(user_),
            Project.matches_words(words),
            Project.matches_labels(labels),
        )

    @staticmethod
    def create_constraint_for_user_accessible_projects(
        user_: user.User,
    ) -> sqlalchemy.sql.elements.BooleanClauseList:
        """Create a constraint that filters all projects accessible to a user."""
        return sqlalchemy.or_(
            # Project is public
            Project.public.is_(True),
            Project.create_constraint_for_user_projects(user_),
        )

    @staticmethod
    def create_constraint_for_user_projects(
        user_: user.User,
    ) -> sqlalchemy.sql.elements.BooleanClauseList:
        """Create a constraint that gets all projects of a user."""

        def get_ids(column: sqlalchemy.Column) -> t.Iterator[uuid.UUID]:
            return (row.id for row in column)

        return sqlalchemy.or_(
            # User is owner
            Project.owner_id == user_.id,
            # User is a member
            Project.project_members.any(user.User.id == user_.id),
            # User is in an associated user group
            Project.project_user_groups.any(
                user_group.UserGroup.id.in_(get_ids(user_.groups))
            ),
            # User owns an associated user group
            Project.project_user_groups.any(
                user_group.UserGroup.id.in_(get_ids(user_.owned_groups))
            ),
            # User is in an associated organization that is associated
            # with user group
            Project.project_organizations.any(
                organization.Organization.groups.any(
                    user_group.UserGroup.id.in_(get_ids(user_.groups))
                )
            ),
            # User owns an associated organization that is associated
            # with user group
            Project.project_organizations.any(
                organization.Organization.groups.any(
                    user_group.UserGroup.id.in_(get_ids(user_.owned_groups))
                )
            ),
            # User is in an associated organization
            Project.project_organizations.any(
                organization.Organization.id.in_(get_ids(user_.organizations))
            ),
            # User owns an associated organization
            Project.project_organizations.any(
                organization.Organization.id.in_(get_ids(user_.owned_organizations))
            ),
        )

    @staticmethod
    def create_constraint_for_public_projects_with_query_parameter(
        labels: list[uuid.UUID] | None, words: list[str] | None
    ) -> sqlalchemy.sql.elements.BooleanClauseList:
        return sqlalchemy.and_(
            Project.public.is_(True),
            Project.matches_words(words),
            Project.matches_labels(labels),
        )

    @staticmethod
    def matches_words(
        words: list[str] | None,
    ) -> sqlalchemy.sql.elements.BooleanClauseList | bool:
        """
        Create a constraint that filters all projects
        that contain all searched words in either name,
        executive summary or detailed description.
        """
        if words is None:
            return True
        return sqlalchemy.or_(
            sqlalchemy.and_(Project.name.contains(word) for word in words),
            sqlalchemy.and_(Project.executive_summary.contains(word) for word in words),
            sqlalchemy.and_(
                Project.detailed_description.contains(word) for word in words
            ),
        )

    @staticmethod
    def matches_labels(
        labels: list[uuid.UUID] | None = None,
    ) -> sqlalchemy.sql.elements.BooleanClauseList | bool:
        """Create constraint to check if a project has labels assigned.

        Returns
        -------
        sqlalchemy.sql.elements.BooleanClauseList or bool
            True if no labels given, else constraint to check if labels
            are assigned to the project or any of its related entities
            (code, data, experiment repository).

        Notes
        -----
        This constraint also checks if these labels are assigned to any code,
        data, or experiment repository assigned to the project.


        """
        if labels is None:
            return True
        projects_with_all_labels = Project.labels_per_project_with_entity_labels()
        projects_containing_expected_labels = sqlalchemy.select(
            projects_with_all_labels.c.project_id
        ).where(projects_with_all_labels.c.all_project_label_ids.contains(labels))
        return Project.id.in_(projects_containing_expected_labels)

    @staticmethod
    def labels_per_project_with_entity_labels() -> sqlalchemy.sql.Subquery:
        """Group all labels including entity labels as an array by project ID.

        Returns
        -------
        sqlalchemy.sql.Subquery
            SQL subquery with columns:

            - ``project_id``: ID of the project
            - ``project_project_label_ids``: all label IDs of labels assigned to
              - the project itself
              - any code, data, and experiment repository of the project

        """
        projects_with_labels = Project.labels_per_project()
        code_with_labels = _subquery.create_subquery_for_project_entity_with_labels(
            table=code_repository.CodeRepository,
            labels_association_table=code_repository.CodeRepositoryLabelAssociationTable,  # noqa: E501
            name="code_repository",
        )
        data_with_labels = _subquery.create_subquery_for_project_entity_with_labels(
            table=data_repository.DataRepository,
            labels_association_table=data_repository.DataRepositoryLabelAssociationTable,  # noqa: E501
            name="data_repository",
        )
        experiment_with_labels = _subquery.create_subquery_for_project_entity_with_labels(  # noqa: E501
            table=experiment_repository.ExperimentRepository,
            labels_association_table=experiment_repository.ExperimentLabelAssociationTable,  # noqa: E501
            name="experiment_repository",
        )
        # **MUST** left join, otherwise projects that have no
        # labels will be excluded
        left_join = True
        return (
            sqlalchemy.select(
                Project.id.label("project_id"),
                (
                    projects_with_labels.c.project_label_ids.op("||")(
                        code_with_labels.c.code_repository_label_ids
                    )
                    .op("||")(data_with_labels.c.data_repository_label_ids)
                    .op("||")(experiment_with_labels.c.experiment_repository_label_ids)
                ).label("all_project_label_ids"),
            )
            .join(
                projects_with_labels,
                projects_with_labels.c.project_id == Project.id,
                isouter=left_join,
            )
            .join(
                code_with_labels,
                code_with_labels.c.project_id == Project.id,
                isouter=left_join,
            )
            .join(
                data_with_labels,
                data_with_labels.c.project_id == Project.id,
                isouter=left_join,
            )
            .join(
                experiment_with_labels,
                experiment_with_labels.c.project_id == Project.id,
                isouter=left_join,
            )
            .group_by(
                Project.id,
                projects_with_labels.c.project_id,
                projects_with_labels.c.project_label_ids,
                code_with_labels.c.code_repository_id,
                code_with_labels.c.code_repository_label_ids,
                data_with_labels.c.data_repository_id,
                data_with_labels.c.data_repository_label_ids,
                experiment_with_labels.c.experiment_repository_id,
                experiment_with_labels.c.experiment_repository_label_ids,
            )
            .subquery()
        )

    @staticmethod
    def labels_per_project() -> sqlalchemy.sql.Subquery:
        """Group all labels as an array by project ID.

        Returns
        -------
        sqlalchemy.sql.Subquery
            SQL subquery with columns:

            - ``project_id``: ID of the project.
            - ``project_label_ids``: all label IDs of labels assigned
              to the project itself (excluding code, data, and experiment
              repositories assigned to it).

        """
        return (
            sqlalchemy.select(
                ProjectLabelAssociationTable.model_id.label("project_id"),
                postgresql.array_agg(
                    sqlalchemy.distinct(
                        ProjectLabelAssociationTable.label_id,
                    )
                ).label("project_label_ids"),
            )
            .group_by(ProjectLabelAssociationTable.model_id)
            .subquery("project_with_labels")
        )

    def contains_user(self, user_id: uuid.UUID) -> bool:
        members_user_ids = [member.id for member in self.project_members] + [
            self.owner_id
        ]
        return (
            user_id in members_user_ids
            or any(group.contains_user(user_id) for group in self.project_user_groups)
            or any(org.contains_user(user_id) for org in self.project_organizations)
        )

    @property
    def all_members(self) -> list[user.User]:
        """Returns all unique members of the project, which includes direct members,
        members through groups and organizations belonging to the project"""

        members_through_organizations = flat_map(
            [organization.all_members for organization in self.project_organizations]
        )

        members_through_groups = flat_map(
            [group.members for group in self.project_user_groups]
        )
        direct_members = self.project_members

        _all_members = (
            members_through_organizations
            + members_through_groups
            + direct_members
            + [self.owner]
        )
        all_unique_organization_members = {
            _user.id: _user for _user in _all_members
        }.values()

        return all_unique_organization_members

    def contains_group(self, group_id: uuid.UUID) -> bool:
        return group_id in self.project_user_groups

    def contains_organization(self, organization_id: uuid.UUID) -> bool:
        return organization_id in self.project_organizations
