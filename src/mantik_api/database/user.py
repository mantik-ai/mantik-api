import enum
import uuid

import sqlalchemy.dialects
import sqlalchemy.orm as orm
from sqlalchemy.orm import validates

import mantik_api.database.base as base
import mantik_api.database.client as client
import mantik_api.database.columns as columns


TABLE_NAME = "mantik_user"
ID_COLUMN_NAME = "user_id"


class OrderBy(base.OrderByBase):
    preferred_name = enum.auto()
    created = enum.auto()
    updated = enum.auto()

    @property
    def column(self) -> sqlalchemy.Column:
        match self:
            case OrderBy.preferred_name:
                return User.preferred_name
            case OrderBy.created:
                return User.created_at
            case OrderBy.updated:
                return User.updated_at
            case _:
                raise NotImplementedError(
                    f"{self} has no associated database table column"
                )


class User(base.BaseWithId):
    __tablename__ = TABLE_NAME

    id = columns.create_id_column(ID_COLUMN_NAME)
    cognito_name = sqlalchemy.Column(
        sqlalchemy.String,
        nullable=False,
        unique=True,
        comment="""Reflects the name of the Congito user and is _not_ mutable.

        It's only used for internal interaction with Cognito.
        The API should _always_ just return the `preferred_name`.
        """,
    )
    preferred_name = sqlalchemy.Column(
        sqlalchemy.String,
        nullable=False,
        unique=True,
        comment="""Allows a user to change their username.

        Cognito does _not_ allow to change the initial username.
        """,
    )
    email = sqlalchemy.Column(
        sqlalchemy.String,
        nullable=False,
        unique=True,
    )
    address = sqlalchemy.Column(sqlalchemy.String)
    payment_method = sqlalchemy.Column(sqlalchemy.String)
    payment_details = sqlalchemy.Column(sqlalchemy.String)
    full_name = sqlalchemy.Column(sqlalchemy.String(length=101))
    info = sqlalchemy.Column(sqlalchemy.String)
    company = sqlalchemy.Column(sqlalchemy.String(length=50))
    job_title = sqlalchemy.Column(sqlalchemy.String(length=50))
    website_url = sqlalchemy.Column(sqlalchemy.String(length=200))
    connections = orm.relationship("Connection")
    groups = orm.relationship(
        "UserGroup",
        secondary="user_group_user_association_table",
        back_populates="members",
    )
    owned_groups = orm.relationship(
        "UserGroup",
        back_populates="admin",
    )

    organizations = orm.relationship(
        "Organization",
        secondary="organization_user_association_table",
        back_populates="members",
    )
    projects = orm.relationship(
        "Project",
        secondary="user_project_association_table",
        back_populates="project_members",
    )
    owned_projects = orm.relationship("Project", back_populates="owner")
    owned_organizations = orm.relationship(
        "Organization",
        back_populates="contact",
    )
    runs = orm.relationship("Run", back_populates="user")
    run_schedules = orm.relationship("RunSchedule", back_populates="owner")
    # TODO: revisit vm_urls in the future and maybe make a relationship
    vm_urls = sqlalchemy.Column(sqlalchemy.dialects.postgresql.ARRAY(sqlalchemy.String))

    def __repr__(self):
        return (
            f"{self.__class__.__name__}("
            f"{self.id=}, "
            f"{self.preferred_name=}, "
            f"{self.cognito_name=}, "
            f"{self.email=}"
            f")".replace("self.", "")
        )

    @validates("cognito_name")
    def validates_cognito_name(self, key, new_name):
        """
        Prevents the cognito username from being changed after user creation,
        since Amazon Cognito does not allow to overwrite this attribute
        """
        if self.cognito_name is not None:
            raise ValueError("Cognito name cannot be modified.")
        return new_name


def get_user_by_id(db: client.main.Client, user_id: uuid.UUID) -> User:
    return db.get_one_by_id(
        orm_model_type=User,
        id_=user_id,
    )
