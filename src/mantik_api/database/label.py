import enum
import uuid

import sqlalchemy.dialects.postgresql as postgresql
import sqlalchemy.orm

import mantik_api.database.base as base
import mantik_api.database.client as client
import mantik_api.database.columns as columns

TABLE_NAME = "label"
ID_COLUMN_NAME = "label_id"


class OrderBy(base.OrderByBase):
    name = enum.auto()
    category = enum.auto()
    sub_category = enum.auto()
    created = enum.auto()
    updated = enum.auto()

    @property
    def column(self) -> sqlalchemy.Column:
        match self:
            case OrderBy.name:
                return Label.name
            case OrderBy.category:
                return Label.category
            case OrderBy.sub_category:
                return Label.sub_category
            case OrderBy.created:
                return Label.created_at
            case OrderBy.updated:
                return Label.updated_at
            case _:
                return Label.created_at


class Scope(enum.StrEnum):
    Project = "project"
    Code = "code"
    Data = "data"
    Experiment = "experiment"


class Category(enum.StrEnum):
    Tasks = "Tasks"
    Software = "Software"
    Datasets = "Datasets"
    Domain = "Domain"
    Licenses = "Licenses"
    Models = "Models"


class SubCategory(enum.StrEnum):
    # Tasks
    Multimodal = "Multimodal"
    Tabular = "Tabular"
    NaturalLanguageProcessing = "Natural Language Processing"
    ComputerVision = "Computer Vision"
    Audio = "Audio"
    ReinforcementLearning = "Reinforcement Learning"
    # Software
    ProgrammingLanguage = "Programming Language"
    Library = "Library"
    # Dataset
    Size = "Size"
    Source = "Source"
    Databases = "Databases"
    # License
    Software = "Software"
    Data = "Data"
    # Models
    NeuralNetworks = "Neural Networks"
    Clustering = "Clustering"
    Classification = "Classification"
    Linear = "Linear"
    Ensemble = "Ensemble"
    DimensionalityReduction = "Dimensionality Reduction"
    Manifold = "Manifold"
    NLP = "NLP"
    ImageProcessing = "Image Processing"


class Label(base.BaseWithId):
    __tablename__ = TABLE_NAME

    id = columns.create_id_column(ID_COLUMN_NAME)
    identifier = sqlalchemy.Column(
        sqlalchemy.String,
        unique=True,
        doc=(
            "The identifier is used for curating the labels. "
            "It allows to change label names both on prod and dev"
            "without requiring knowledge of the actual UUIDs."
        ),
    )
    scopes = sqlalchemy.Column(
        postgresql.ARRAY(columns.create_string_enum(Scope)), nullable=False
    )
    category = sqlalchemy.Column(columns.create_string_enum(Category), nullable=False)
    sub_category = sqlalchemy.Column(
        columns.create_string_enum(SubCategory), nullable=True
    )
    name = sqlalchemy.Column(sqlalchemy.String, nullable=False)


def get_label_by_id(db: client.main.Client, id_: uuid.UUID) -> Label:
    return db.get_one_by_id(
        orm_model_type=Label,
        id_=id_,
    )
