import abc
import io
import logging
import pathlib
import typing as t
import zipfile

import mantik_api.compute_backend.content as _content
import mantik_api.compute_backend.file as _file
import mantik_api.utils as utils


logger = logging.getLogger(__name__)


class Directory(abc.ABC):
    """Folder on an external storage."""

    @property
    @abc.abstractmethod
    def path(self) -> pathlib.Path:
        """Return the base path of the directory."""
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def zip_archive_name(self) -> str:
        """Return the name of the directory as in a ZIP file."""
        raise NotImplementedError

    def download(self, path: pathlib.Path | None = None) -> _content.DownloadResponse:
        """Download the directory or files in a subdirectory into a ZIP file."""
        files = self.get_files_recursively(path)
        return _download_files_into_zip(
            files, base=self.path, archive_name=self.zip_archive_name
        )

    @abc.abstractmethod
    def get_files_recursively(self: pathlib.Path | None = None) -> list[_file.File]:
        """Return all files in the directory and its sub-directories.

        Parameters
        ----------
        path : pathlib.Path or None
            Path to the file or directory.

            If ``None``, the directory itself is returned.

        Returns
        -------
        list[File]

        """
        raise NotImplementedError


def _download_files_into_zip(
    files: list[_file.File], base: pathlib.Path, archive_name: str
) -> _content.DownloadResponse:
    zip_base_dir = pathlib.Path(archive_name)
    zip_stream = _download_and_stream_files_to_zip(
        files, base=base, zip_base_dir=zip_base_dir
    )
    return _content.DownloadResponse(
        content=zip_stream,
        filename=zip_base_dir.with_suffix(".zip").as_posix(),
    )


def _download_and_stream_files_to_zip(
    files: list[_file.File], base: pathlib.Path, zip_base_dir: pathlib.Path
) -> t.Callable[..., t.Iterator[bytes]]:
    def stream_zip_files() -> t.Iterator[bytes]:
        stream = utils.stream.UnseekableBytesStream()
        with zipfile.ZipFile(stream, mode="w", compression=zipfile.ZIP_DEFLATED) as f:
            for file in files:
                path = _create_file_path_in_archive(
                    file, base=base, zip_base_dir=zip_base_dir
                )
                content = _download_and_write_file_to_bytes(file)
                f.writestr(path.as_posix(), data=content.read())
                yield stream.get()
        yield stream.get()

    return stream_zip_files


def _download_and_write_file_to_bytes(file: _file.File) -> io.BytesIO:
    content = io.BytesIO()
    for chunk in file.download().get_content():
        content.write(chunk)
    content.seek(0)
    return content


def _create_file_path_in_archive(
    file: _file.File, base: pathlib.Path, zip_base_dir: pathlib.Path
) -> pathlib.Path:
    """Creates a relative path for a file to the base dir of a ZIP archive.

    E.g. a file might have the total path ``path/to/file.txt``,
    and a user requested to download the ``path/to`` folder.
    Then, the archive base folder should be ``to``.
    Hence, we must make the files path relative to the
    requested base folder.

    """
    relative_path = file.path.relative_to(base)
    return zip_base_dir / relative_path
