import abc
import pathlib

import mantik_api.compute_backend.content as _content


class File(abc.ABC):
    """A file from an external storage."""

    @property
    @abc.abstractmethod
    def path(self) -> pathlib.Path:
        """Return the file path."""
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def content(self) -> str:
        """Return the file content."""
        raise NotImplementedError

    @abc.abstractmethod
    def download(self) -> _content.DownloadResponse:
        """Download the file from the external storage."""
        raise NotImplementedError
