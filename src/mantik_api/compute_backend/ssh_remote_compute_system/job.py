import logging
import pathlib

import mantik_api.compute_backend.content as _content
import mantik_api.compute_backend.job as _job
import mantik_api.compute_backend.ssh_remote_compute_system.client as ssh_client
import mantik_api.database as database
import mantik_api.models

logger = logging.getLogger(__name__)


class Job(_job.JobBase):
    def __init__(
        self,
        ssh_client: ssh_client.Client,
        job_id: str,
    ):
        self._ssh_client = ssh_client
        self._job_id = job_id

    @property
    def job_dir(self) -> pathlib.Path:
        return pathlib.Path(f"mantik-runs/{self._job_id}")

    @property
    def id(self) -> str:
        """Return the job's SLURM ID."""
        return self._job_id

    @property
    def log_file_path(self) -> pathlib.Path:
        return self.job_dir / "mantik.log"

    def get_status(self) -> database.run.RunStatus:
        return self._ssh_client.get_job_status(job_id=self.id).to_database_run_status()

    def get_logs(self) -> list[str]:
        return self._ssh_client.get_job_logs(log_file_path=self.log_file_path)

    def cancel(self):
        self._ssh_client.cancel_job(job_id=self._job_id)

    def download(self, path: pathlib.Path | None = None) -> _content.DownloadResponse:
        return self._ssh_client.download_from_path(path=self.job_dir / path)

    def get_info(self) -> mantik_api.models.run_info.SSHSlurm:
        return self._ssh_client.get_job_info(job_id=self._job_id)
