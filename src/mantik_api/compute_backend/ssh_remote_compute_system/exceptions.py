import mantik_api.compute_backend.exceptions as exceptions


class SSHError(exceptions.ComputeBackendException):
    """Generic class for all ssh errors."""


class AuthenticationFailedException(SSHError):
    """User authentication has failed."""


class JobInformationException(SSHError):
    """Fetching SLURM job status failed."""
