import contextlib
import io
import logging
import pathlib
import typing as t
import zipfile
from pathlib import Path
from stat import S_ISDIR
from stat import S_ISREG

import paramiko

import mantik_api.compute_backend.content as _content
import mantik_api.compute_backend.ssh_remote_compute_system.exceptions as _exceptions
import mantik_api.models.run_info

logger = logging.getLogger(__name__)


class Client:
    def __init__(
        self,
        username: str,
        hostname: str,
        port: t.Optional[int] = None,
        password: t.Optional[str] = None,
        private_key: t.Optional[str] = None,
        timeout: t.Optional[int] = 5,
    ):
        self._hostname = hostname
        self._username = username
        self._port = port
        self._password = password
        self._private_key = private_key
        self._timeout = timeout

        self.client = paramiko.SSHClient()
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    @contextlib.contextmanager
    def _connect(self):
        """Connect to, and disconnect from an SSH server."""
        kwargs = {
            "hostname": self._hostname,
            "username": self._username,
            "auth_timeout": self._timeout,
            "timeout": self._timeout,
        }

        if self._port:
            kwargs["port"] = self._port
        if self._private_key:
            kwargs["pkey"] = get_private_key_from_key_string(
                private_key=self._private_key
            )
        elif not self._private_key and self._password:
            kwargs["password"] = self._password

        try:
            self.client.connect(**kwargs)
        except Exception as e:
            logger.error(e)
            raise _exceptions.AuthenticationFailedException(e)

        try:
            yield self.client
        finally:
            if self.client:
                self.client.close()

    def get_job_status(
        self, job_id: str
    ) -> mantik_api.models.run_info.SSHSlurmJobStatus:
        with self._connect() as connected_ssh_client:
            try:
                return get_job_status(
                    job_id=job_id, connected_ssh_client=connected_ssh_client
                )
            except _exceptions.JobInformationException as e:
                logger.error(e)
                raise _exceptions.SSHError(
                    f"Could not retrieve job status for job_id=`{job_id}`"
                )

    def get_job_info(self, job_id: str) -> mantik_api.models.run_info.SSHSlurm:
        with self._connect() as connected_ssh_client:
            try:
                return get_job_info(
                    job_id=job_id, connected_ssh_client=connected_ssh_client
                )
            except _exceptions.JobInformationException as e:
                logger.error(e)
                raise _exceptions.SSHError(
                    f"Could not retrieve job information for job_id=`{job_id}`"
                )

    def get_job_logs(self, log_file_path: pathlib.Path) -> list[str]:
        with self._connect() as connected_ssh_client:
            return get_job_logs(
                log_file_path=log_file_path, connected_ssh_client=connected_ssh_client
            )

    def cancel_job(self, job_id: str) -> None:
        with self._connect() as connected_client:
            cancel_job(job_id=job_id, connected_ssh_client=connected_client)

    def download_from_path(self, path: pathlib.Path) -> _content.DownloadResponse:
        with self._connect() as connected_ssh_client:
            return download_from_remote_path(
                path=path, connected_ssh_client=connected_ssh_client
            )


def get_job_logs(
    log_file_path: pathlib.Path, connected_ssh_client: paramiko.SSHClient
) -> list[str]:
    """Get the logs of a SLURM job."""
    sftp = connected_ssh_client.open_sftp()
    try:
        with sftp.open(str(log_file_path), "r") as log_file:
            return log_file.readlines()
    except FileNotFoundError:
        raise _exceptions.SSHError(f"Log file not found at {str(log_file_path)}")
    finally:
        sftp.close()


def cancel_job(job_id: str, connected_ssh_client: paramiko.SSHClient) -> None:
    """Cancel a SLURM job."""
    connected_ssh_client.exec_command(f"scancel {job_id}")


def run_directory(run_id: str) -> pathlib.Path:
    return Path(f"mantik-runs/{run_id}")


def get_job_status(
    job_id: str, connected_ssh_client: paramiko.SSHClient
) -> mantik_api.models.run_info.SSHSlurmJobStatus:
    """Get the status of a SLURM job."""

    try:
        job_status = get_job_status_from_squeue(
            job_id=job_id, connected_ssh_client=connected_ssh_client
        )
    except _exceptions.JobInformationException as e:
        logger.info(e)
        job_status = get_job_status_from_sacct(
            job_id=job_id, connected_ssh_client=connected_ssh_client
        )

    logger.info(f"Retrieved job status: {job_status}")
    return mantik_api.models.run_info.SSHSlurmJobStatus(job_status)


def get_job_status_from_squeue(
    job_id: str, connected_ssh_client: paramiko.SSHClient
) -> str:
    """Get the status of a SLURM job from squeue"""
    # options: return just the status, and don't return the header

    command = f"squeue -j {job_id} --format=%T -h"
    stdin, stdout, stderr = connected_ssh_client.exec_command(command)
    output, error = (
        stdout.read().decode("utf-8").strip(),
        stderr.read().decode("utf-8").strip(),
    )
    if error or not output:
        raise _exceptions.JobInformationException(
            f"Error executing `{command}`. output: `{output}`. error: `{error}`"
        )
    logger.info(f"output: {output}")

    return output


def get_job_status_from_sacct(
    job_id: str, connected_ssh_client: paramiko.SSHClient
) -> str:
    """Get the status of a SLURM job from saact"""
    # options: separate using `|`, and don't return the header
    command = f"sacct -j {job_id} --format=State -P -n"
    stdin, stdout, stderr = connected_ssh_client.exec_command(command)
    output, error = (
        stdout.read().decode("utf-8").strip(),
        stderr.read().decode("utf-8").strip(),
    )

    if error or not output:
        raise _exceptions.JobInformationException(
            f"Error executing `{command}`. output: `{output}`. error: `{error}`"
        )
    logger.info(f"output: {output}")

    return parse_job_state_from_sacct(output=output)


def parse_job_state_from_sacct(output: str) -> str:
    """Extract State for a command like `sacct -j {job_id} --format=State -P -n`

    Sometimes the output can be something like 'FAILED\nFAILED'
    """
    job_status = output.splitlines()
    if len(job_status) > 1:
        logger.warning(
            f"There are multiple statuses for the "
            f"job: `{job_status}`. Returning first one: `{job_status[0]}`"
        )

    return job_status[0]


def get_job_info(
    job_id: str, connected_ssh_client: paramiko.SSHClient
) -> mantik_api.models.run_info.SSHSlurm:
    """Get a subset of the SLURM job info using `saact`"""
    sacct_fields = mantik_api.models.run_info.SSHSlurm.sacct_fields()
    _sacct_fields_formatted = ",".join(sacct_fields)

    command = f"sacct -j {job_id} --format={_sacct_fields_formatted} -P -n"
    stdin, stdout, stderr = connected_ssh_client.exec_command(command)

    output, error = (
        stdout.read().decode("utf-8").strip(),
        stderr.read().decode("utf-8").strip(),
    )
    if not output or error:
        raise _exceptions.JobInformationException(
            f"Error executing `{command}`. output: `{output}`. error: `{error}`"
        )

    data = {
        field: value.strip() for field, value in zip(sacct_fields, output.split("|"))
    }

    return mantik_api.models.run_info.SSHSlurm.from_dict(id_=job_id, data=data)


def get_private_key_from_key_string(
    private_key: str,
) -> t.Union[paramiko.RSAKey, paramiko.DSSKey, paramiko.ECDSAKey, paramiko.Ed25519Key]:
    """Parse a private key string into a paramiko key."""
    key_classes = [
        paramiko.RSAKey,
        paramiko.DSSKey,
        paramiko.ECDSAKey,
        paramiko.Ed25519Key,
    ]

    for key_class in key_classes:
        try:
            return key_class.from_private_key(io.StringIO(private_key))
        except paramiko.SSHException:
            pass
        except Exception:
            pass

    raise _exceptions.SSHError("Private key is invalid.")


def download_from_remote_path(
    path: pathlib.Path, connected_ssh_client: paramiko.SSHClient
) -> _content.DownloadResponse:
    """Download a file or a folder (zipped) from an SSH server

    to local memory stream."""
    sftp = connected_ssh_client.open_sftp()
    try:
        # Check if the remote path is a file or a directory
        remote_file_attr = sftp.lstat(str(path))
        filename = path.name

        if S_ISREG(remote_file_attr.st_mode):  # it's a file
            data_stream_generator = lambda: download_file_in_memory(  # noqa E731
                sftp_client=sftp, remote_file_path=path
            )
        else:  # it's a directory
            data_stream_generator = (
                lambda: download_directory_as_zip_to_memory(  # noqa E731
                    sftp_client=sftp, remote_dir_path=path
                )
            )
            filename += ".zip"
        return _content.DownloadResponse(
            content=data_stream_generator, filename=filename
        )
    except FileNotFoundError:
        raise _exceptions.SSHError(
            f"Filepath: `{path}` not found on the remote system!"
        )
    except Exception as e:
        raise _exceptions.SSHError(e)
    finally:
        sftp.close()


def download_file_in_memory(
    sftp_client: paramiko.SFTPClient, remote_file_path: Path
) -> io.BytesIO:
    """
    Downloads a single file from the remote server into an in-memory stream.

    :param sftp_client: The active Paramiko SFTP client connection.
    :param remote_file_path: The remote file path to download as a Path object.
    :return: An in-memory stream containing the file data.
    """
    file_stream = io.BytesIO()
    sftp_client.getfo(str(remote_file_path), file_stream)
    file_stream.seek(0)  # Rewind the stream to the beginning
    return file_stream


def download_directory_as_zip_to_memory(
    sftp_client: paramiko.SFTPClient, remote_dir_path: Path
) -> io.BytesIO:
    """
    Downloads a directory from the remote server into an in-memory zip archive.

    :param sftp_client: The active Paramiko SFTP client connection.
    :param remote_dir_path: The remote directory path to download as a Path object.
    :return: An in-memory stream containing the zip archive of the directory.
    """
    zip_stream = io.BytesIO()
    with zipfile.ZipFile(zip_stream, "w", zipfile.ZIP_DEFLATED) as zip_file:
        _add_directory_to_zip(
            sftp_client, remote_dir_path, zip_file, remote_dir_path.name
        )

    zip_stream.seek(0)  # Rewind the stream to the beginning
    return zip_stream


def _add_directory_to_zip(
    sftp_client: paramiko.SFTPClient,
    current_path: Path,
    zip_file: zipfile.ZipFile,
    archive_path: str,
) -> None:
    """
    Recursively adds files and directories from the remote server to a zip archive.

    :param sftp_client: The active Paramiko SFTP client connection.
    :param current_path: The current remote path to process.
    :param zip_file: The in-memory zip file object.
    :param archive_path: The relative path within the zip archive.
    """
    for entry in sftp_client.listdir_attr(str(current_path)):
        remote_entry_path = current_path / entry.filename
        if S_ISDIR(entry.st_mode):
            # Recursively add directory contents
            _add_directory_to_zip(
                sftp_client,
                remote_entry_path,
                zip_file,
                archive_path + "/" + entry.filename,
            )
        else:
            # Add file to the zip archive
            file_stream = download_file_in_memory(sftp_client, remote_entry_path)
            zip_file.writestr(archive_path + "/" + entry.filename, file_stream.read())
