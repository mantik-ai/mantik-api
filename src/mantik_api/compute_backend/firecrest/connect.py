import firecrest

import mantik_api.compute_backend.firecrest.exceptions as exceptions


def connect_to_firecrest_api(
    api_url: str, auth: firecrest.ClientCredentialsAuth
) -> firecrest.Firecrest:
    try:
        auth.get_access_token()
    except firecrest.ClientsCredentialsException:
        raise exceptions.AuthenticationFailedException(
            f"Failed to connect to {auth._token_uri} -- "
            "check if client ID and secret are correct"
        )
    return firecrest.Firecrest(
        firecrest_url=api_url,
        authorization=auth,
    )
