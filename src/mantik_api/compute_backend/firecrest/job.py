import logging
import pathlib
import typing as t
import uuid

import firecrest.types

import mantik_api.compute_backend.content as _content
import mantik_api.compute_backend.firecrest._directory as _directory
import mantik_api.compute_backend.firecrest._file as _file
import mantik_api.compute_backend.firecrest.exceptions as exceptions
import mantik_api.compute_backend.job as _job
import mantik_api.database as database
import mantik_api.models as models


logger = logging.getLogger(__name__)


class Job(_job.JobBase):
    """A job submitted to a firecREST API.

    Parameters
    ----------
    client : firecrest.Firecrest
        The firecREST API client.
    machine : str
        The firecREST machine this job was submitted to.
    job_id : str
        The Slurm job ID.
    mlflow_run_id : uuid.UUID, optional
        The run's MLflow run ID.

        Required to infer the run directory on the firecREST machine storage.
    run_dir : pathlib.Path, optional
        The run directory on the firecREST storage.

        If not given, this is inferred from the `mlflow_run_id`.

    """

    def __init__(
        self,
        client: firecrest.Firecrest,
        machine: str,
        job_id: str,
        mlflow_run_id: uuid.UUID | None = None,
        run_dir: pathlib.Path | None = None,
    ):
        self._client = client
        self._machine = machine
        self._job_id = job_id
        self._mlflow_run_id = mlflow_run_id

        if run_dir is None and mlflow_run_id is None:
            raise ValueError(
                "Either MLflow run ID or run directory has to be given "
                "to infer run directory from firecREST API"
            )

        # The Mantik Compute Backend creates a run directory
        # at `<systems' scratch path>/<user>/mantik/<MLflow run ID>`.
        #
        # The firecREST API doesn't return that run directory
        # for inactive jobs (that have succeeded/failed). (For active jobs,
        # it returns the path of the stdout/stderr file, which is located
        # in that directory, as `job_file_out` stored in
        # `mantik_api.models.run_info.Firecrest.job_file_out`.
        # However, if this is unknown, we have to infer the run directory
        # from the machine's scratch path and the MLflow run ID.
        self._run_dir = run_dir or _infer_run_dir(
            client=client,
            machine=machine,
            mlflow_run_id=mlflow_run_id,
        )

    @classmethod
    def from_run_info(
        cls,
        info: models.run_info.Firecrest | None,
        client: firecrest.Firecrest,
        machine: str,
        job_id: str,
        mlflow_run_id: uuid.UUID | None = None,
    ) -> t.Self:
        """Construct from run info returned by the firecREST API if available.

        If that info was fetched in a state where the job was active,
        (QUEUED or RUNNING), then the info contain the `job_file_out` (stdout).
        The parent directory of that file gives us the run directory.

        """
        if info is not None:
            return cls(
                client=client,
                machine=machine,
                job_id=job_id,
                mlflow_run_id=mlflow_run_id,
                run_dir=info.job_file_out.parent
                if info.job_file_out is not None
                else None,
            )
        return cls(
            client=client,
            machine=machine,
            job_id=job_id,
            mlflow_run_id=mlflow_run_id,
        )

    @property
    def id(self) -> str:
        return self._job_id

    def get_status(self) -> database.run.RunStatus:
        response = self._client.poll(machine=self._machine, jobs=[self._job_id])
        logger.info("FirecREST API response: %s", response)

        try:
            [details] = response
        except ValueError as e:
            raise exceptions.FirecrestException(
                f"No job with Slurm ID {self._job_id} found "
                f"on machine {self._machine!r}."
            ) from e

        firecrest_status = details["state"]
        # If a job was cancelled, firecREST returns "CANCELLED by <num>".
        # We parse this to "CANCELLED" for consistent status representation.
        firecrest_status = (
            "CANCELLED" if "CANCELLED" in firecrest_status else firecrest_status
        )
        return models.run_info.FirecrestStatus(
            firecrest_status
        ).to_database_run_status()

    def get_info(self, previous_info: dict | None = None) -> models.run_info.Firecrest:
        previous_info = previous_info or {}

        try:
            # `poll_active()` only returns something if the job is currently
            # running (i.e. in the queue).
            details_active_response: list[
                firecrest.types.JobQueue
            ] = self._client.poll_active(  # noqa: E501
                machine=self._machine, jobs=[self._job_id]
            )
        except firecrest.FirecrestException:
            logger.exception(
                "Unable to get active job info for job %s on machine %s",
                self._job_id,
                self._machine,
            )
            details_active = {}
        else:
            details_active = (
                details_active_response[0] if details_active_response else {}
            )

        try:
            details_response: list[firecrest.types.JobAcct] = self._client.poll(
                machine=self._machine, jobs=[self._job_id]
            )
        except firecrest.FirecrestException:
            logger.exception(
                "Unable to get job info for job %s on machine %s",
                self._job_id,
                self._machine,
            )
            details = {}
        else:
            details = details_response[0] if details_response else {}

        if all(not d for d in [previous_info, details_active, details]):
            raise exceptions.FirecrestException(
                f"Unable to fetch job info for job with Slurm ID {self._job_id}"
                f"on machine {self._machine}: sacct and squeue returned an "
                "empty response."
            )

        # Merge with previous info with `poll_active()` and `poll()` response.
        # This allows to e.g. keep the previously stored `job_file_out` field,
        # which holds the stdout file of the job. This file also allows to
        # extract the run directory.
        merged = previous_info | details_active | details
        return models.run_info.Firecrest.from_dict(id_=self._job_id, data=merged)

    def get_logs(self, previous_info: dict | None = None) -> list[str]:
        """Get the application logs.

        Parameters
        ----------
        prev_info : dict, optional
            If given, will be passed to `Job.get_info()` to assemt

        """
        path = self._get_logs_file_path(previous_info)
        file = _file.File(client=self._client, machine=self._machine, path=path)
        return file.content.split("\n")

    def _get_logs_file_path(self, previous_info: dict | None = None) -> pathlib.Path:
        # If log file path already in previous info, use it.
        if previous_info is not None and "job_file_out" in previous_info:
            return pathlib.Path(previous_info["job_file_out"])
        # Else, attempt to fetch info.
        # If it fails, assume logs file path from inferred run directory.
        else:
            try:
                info = self.get_info(previous_info)
            except exceptions.FirecrestException:
                logger.exception(
                    (
                        "Unable to fetch run info for Slurm job with ID %s "
                        "on machine %s, assuming log file to be present in run "
                        "directory %s"
                    ),
                    self._job_id,
                    self._machine,
                    self._run_dir,
                )
                path = None
            else:
                path = info.job_file_out
            # Even if info fetch succeeds, the `job_file_out` may still be
            # unknown. If that is the case, assume the logs file to be present
            # in the run directory inferred from the MLflow run ID.
            return path or self._run_dir / _job.APPLICATION_LOGS_FILE

    def download(self, path: pathlib.Path | None = None) -> _content.DownloadResponse:
        path = path or pathlib.Path(".")

        if path.is_absolute():
            # If a path is absolute, the `/` operation will not append it
            # to the left operand.
            path = path.relative_to("/")

        # Append run directory to given path.
        path = self._run_dir / path

        if self._is_directory(path):
            return _directory.Directory(
                client=self._client,
                machine=self._machine,
                path=path,
            ).download()
        return _file.File(
            client=self._client, machine=self._machine, path=path
        ).download()

    def _is_directory(self, path: pathlib.Path) -> bool:
        try:
            type_ = self._client.file_type(
                machine=self._machine, target_path=path.as_posix()
            )
        except firecrest.FirecrestException as e:
            raise exceptions.FirecrestException(str(e))
        logger.info("firecREST path %s is of type %s", path, type_)
        # `firecrest.Firecrest.file_type()` returns the output of
        # `file -b <path>`. For directories that is `directory`.
        return type_ == "directory"

    def cancel(self) -> None:
        self._client.cancel(machine=self._machine, job_id=self._job_id)


def _infer_run_dir(
    client: firecrest.Firecrest, machine: str, mlflow_run_id: uuid.UUID
) -> pathlib.Path:
    """If not given, assume the path of the run directory.

    The below implementation is almost identical to that implemented in the
    Mantik Compute Backend, see

    https://gitlab.com/mantik-ai/mantik/-/blob/7ca87e903a45db27a12ee28adaa957ec58578d53/mantik-compute-backend/mantik_compute_backend/firecrest/client.py#L49  # noqa: E501

    as reference.

    """
    logger.warning(
        (
            "No run directory given for run with MLflow run ID %s on "
            "firecREST machine %s, assuming run directory from firecREST API",
        ),
        mlflow_run_id,
        machine,
    )

    client_parameters = client.parameters()
    [storages] = [
        entry
        for entry in client_parameters["storage"]
        if entry["name"] == "FILESYSTEMS"
    ]
    machine_storage = [
        storage for storage in storages["value"] if storage["system"] == machine
    ]
    if len(machine_storage) > 1:
        logger.warning(
            (
                "Found more than one firecREST machine storage for machine "
                "%s (%s), choosing first storage for finding scratch dir"
            ),
            machine,
            machine_storage,
        )

    machine_storage = machine_storage[0]

    mounted_dirs = machine_storage["mounted"]

    # Extract first matching path with scratch _or_ home and use
    # as scratch directory path.
    def get_dir(name: str) -> str:
        return next((path for path in mounted_dirs if name in path), None)

    scratch = get_dir("scratch") or get_dir("home")
    if scratch is None:
        raise exceptions.FirecrestException(
            "Unable to detect a scratch or home directory for run with MLflow"
            f"run ID {mlflow_run_id} on machine {machine!r}: {machine_storage}"
        )

    scratch_path = pathlib.Path(scratch)

    # FirecREST at CSCS only allows to perform `mkdir` on the scratch file
    # system in a sub-directory named after the user.
    username = client.whoami(machine=machine)
    scratch_user_dir = scratch_path / username

    # For firecREST, the Mantik Compute Backend creates a run directory at
    # `<scratch>/<user>/mantik/<run ID hex (without dashes)>` _or_,
    # if no scratch path is given, at `<home>/mantik/<run ID>`.
    run_dir = scratch_user_dir / "mantik" / mlflow_run_id.hex
    logger.warning("Assuming run directory to be %s", run_dir)
    return run_dir
