import io
import logging
import pathlib
import typing as t

import firecrest

import mantik_api.compute_backend.content as _content
import mantik_api.compute_backend.exceptions as exceptions
import mantik_api.compute_backend.file as _file


logger = logging.getLogger(__name__)


class File(_file.File):
    """File on an external firecREST storage."""

    def __init__(self, client: firecrest.Firecrest, machine: str, path: pathlib.Path):
        self._client = client
        self._machine = machine
        self._path = path

    @property
    def path(self) -> pathlib.Path:
        return self._path

    @property
    def content(self) -> str:
        """Return the file's content."""
        return self._download_content().read().decode("utf-8")

    def download(self) -> _content.DownloadResponse:
        """Download the file content.

        Returns
        -------
        mantik_api.compute_backend.content.DownloadResponse
            The file content as a stream.

        Raises
        ------
        mantik_api.compute_backend.firecrest.exceptions.FirecrestException
            The file download failed.

        """
        stream = self._download_content()

        def iter_content() -> t.Iterator[bytes]:
            yield stream.read(1024)

        return _content.DownloadResponse(
            content=iter_content,
            filename=self._path.name,
        )

    def _download_content(self) -> io.BytesIO:
        logger.info("Downloading file %s from machine %s", self._path, self._machine)
        return self._try_simple_download() or self._try_external_download()

    def _try_simple_download(self) -> io.BytesIO | None:
        """Download file via `firecrest.Firecrest.simple_download()`.

        This method allows to download small files up to a certain limit,
        and is faster than `firecrest.Firecrest.external_download()`.

        Returns
        -------
        io.BytesIO or None
            File content if download succeeded, else `None`.

        """
        stream = io.BytesIO()
        try:
            self._client.simple_download(
                machine=self._machine,
                source_path=self._path.as_posix(),
                target_path=stream,
            )
        except firecrest.FirecrestException:
            logger.exception(
                "Unable to simple download file %s from machine %s",
                self._path,
                self._machine,
            )
            return None
        stream.seek(0)
        return stream

    def _try_external_download(self) -> io.BytesIO:
        """Download file via `firecrest.Firecrest.external_download()`.

        Returns
        -------
        io.BytesIO
            File content.

        Raises
        ------
        FirecrestException
            File could not be downloaded.

        """
        stream = io.BytesIO()
        try:
            download = self._client.external_download(
                machine=self._machine, source_path=self._path.as_posix()
            )
            download.finish_download(stream)
        except firecrest.FirecrestException as e:
            raise exceptions.FirecrestException(str(e)) from e
        stream.seek(0)
        return stream
