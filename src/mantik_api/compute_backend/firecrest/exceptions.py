import mantik_api.compute_backend.exceptions as exceptions


class FirecrestException(exceptions.ComputeBackendException):
    """An error occurred when interacting with firecREST."""


class AuthenticationFailedException(FirecrestException):
    """Invalid client credentials"""
