import pathlib

import firecrest

import mantik_api.compute_backend.directory as _directory
import mantik_api.compute_backend.firecrest._file as _file


class Directory(_directory.Directory):
    """Directory on an external firecREST storage."""

    def __init__(self, client: firecrest.Firecrest, machine: str, path: pathlib.Path):
        self._client = client
        self._machine = machine
        self._path = path

    @property
    def path(self) -> pathlib.Path:
        return self._path

    @property
    def zip_archive_name(self) -> str:
        return self._path.name

    def get_files_recursively(
        self, path: pathlib.Path | None = None
    ) -> list[_file.File]:
        path = self._path / (path or "")

        def prepend_path(name: str) -> pathlib.Path:
            return path / name

        result = self._client.list_files(
            machine=self._machine,
            target_path=path.as_posix(),
        )

        files = [
            _file.File(
                client=self._client,
                machine=self._machine,
                path=prepend_path(element["name"]),
            )
            for element in result
            # For files, firecREST returns type `-`
            if element["type"] == "-"
        ]
        directories = [
            Directory(
                client=self._client,
                machine=self._machine,
                path=prepend_path(element["name"]),
            )
            for element in result
            # For directories, firecREST returns type `d`
            if element["type"] == "d"
        ]

        if directories:
            files_in_subfolders = [
                file
                for directory in directories
                for file in directory.get_files_recursively()
            ]
            files.extend(files_in_subfolders)

        return files
