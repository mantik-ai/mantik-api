import dataclasses
import enum
import typing as t


class MediaTypes(enum.StrEnum):
    FILE = "application/octet-stream"
    ZIP = "application/x-zip-compressed"


@dataclasses.dataclass
class DownloadResponse:
    content: t.Callable[..., t.Iterator[bytes]]
    filename: str
    media_type: MediaTypes = None
    headers: dict[str, str] | None = None

    def __post_init__(self):
        self.media_type = MediaTypes.ZIP if ".zip" in self.filename else MediaTypes.FILE
        self.headers = {
            "Content-Disposition": f"attachment;filename={self.filename}",
        }

    def get_content(self) -> t.Iterator[bytes]:
        yield from self.content()
