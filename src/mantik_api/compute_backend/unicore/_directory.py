import logging
import pathlib

import pyunicore.client

import mantik_api.compute_backend.directory as _directory
import mantik_api.compute_backend.unicore._file as _file
import mantik_api.compute_backend.unicore.exceptions as exceptions

logger = logging.getLogger(__name__)

StorageListDirResponse = dict[str, pyunicore.client.PathFile | pyunicore.client.PathDir]


class Directory(_directory.Directory):
    """A working directory of a `Job`,
    wrapper class around pyunicore.client.Storage."""

    def __init__(
        self, storage: pyunicore.client.Storage, path: pathlib.Path, name: str
    ):
        """Wrap storage and use the path for ZIP archive name when downloading."""
        self._storage = storage
        self._path = path
        self._name = name

    @property
    def path(self) -> pathlib.Path:
        return self._path

    @property
    def zip_archive_name(self) -> str:
        return self._name

    def get_directory_or_file(
        self, path: pathlib.Path | None
    ) -> _directory.Directory | _file.File:
        """Get a directory or a file from the job's working directory.

        Parameters
        ----------
        path : pathlib.Path or None
            Path to the file or directory.

            If ``None``, the root of the job's working directory is returned.

        Returns
        -------
        Directory or File

        """
        obj = self._open_path(path)
        return _to_file_or_directory(obj)

    def _open_path(
        self, path: pathlib.Path | None
    ) -> pyunicore.client.PathFile | pyunicore.client.PathDir:
        path = self._prepare_path(path)

        try:
            pyunicore_object = self._storage.stat(path.as_posix())
        except Exception as e:  # noqa: B902
            raise exceptions.NotFoundException(f"{path} does not exist") from e
        return pyunicore_object

    def get_files_recursively(
        self, path: pathlib.Path | None = None
    ) -> list[_file.File]:
        path = self._prepare_path(path)

        content: StorageListDirResponse = self._storage.listdir(path.as_posix())

        files: list[_file.File] = []
        for obj in content.values():
            file_or_dir = _to_file_or_directory(obj)
            if isinstance(file_or_dir, _file.File):
                files.append(file_or_dir)
            elif isinstance(file_or_dir, Directory):
                files.extend(file_or_dir.get_files_recursively())
            else:
                raise NotImplementedError(
                    f"Not a file or directory: {type(file_or_dir)}"
                )
        return files

    def _prepare_path(self, path: pathlib.Path | None) -> pathlib.Path:
        return path or self._path


def _to_file_or_directory(
    obj: pyunicore.client.PathFile | pyunicore.client.PathDir,
) -> _file.File | _directory.Directory:
    if isinstance(obj, pyunicore.client.PathFile):
        return _file.File(obj)
    elif isinstance(obj, pyunicore.client.PathDir):
        # `pyunicore.client.PathDir.name` holds the full _relative_
        # path to a job's working directory root. This should be the name
        # of the directory _without_ the job ID.
        path = pathlib.Path(obj.name)
        return Directory(storage=obj.storage, path=path, name=path.name)
    raise NotImplementedError(
        f"Cannot convert object of type {type(obj)} from pyunicore"
    )
