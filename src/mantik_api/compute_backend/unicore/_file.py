import contextlib
import logging
import pathlib
import typing as t

import pyunicore.client

import mantik_api.compute_backend.content as _content

logger = logging.getLogger(__name__)


class File:
    """A file in a working directory of a `Job`.

    Wrapper class around pyunicore.client.PathFile.

    """

    def __init__(self, pyunicore_path_file: pyunicore.client.PathFile):
        self._file = pyunicore_path_file

    @property
    def path(self) -> pathlib.Path:
        return pathlib.Path(self._file.name)

    @property
    def content(self) -> str:
        """Return the file's content."""
        return self._file.raw().read().decode("utf-8")

    def download(self) -> _content.DownloadResponse:
        """Download the file.

        Notes
        -----
        Implementation similar to ``pyunicore.client.PathFile``,
        but adapted to allow streaming.

        See https://github.com/HumanBrainProject/pyunicore/blob/436ae46ffdf8cae21cdb4be3f59d91fdd6378eb8/pyunicore/client.py#L858  # noqa: E501

        """

        def iter_content() -> t.Iterator[bytes]:
            logger.debug("Downloading file %s", self._file.resource_url)
            with contextlib.closing(
                self._file.transport.get(
                    url=self._file.resource_url,
                    headers={"Accept": "application/octet-stream"},
                    stream=True,
                    to_json=False,
                )
            ) as response:
                yield from response.iter_content(chunk_size=1024)

        return _content.DownloadResponse(
            content=iter_content,
            filename=self.path.name,
        )
