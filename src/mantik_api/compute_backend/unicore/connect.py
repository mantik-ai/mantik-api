import logging

import pyunicore.client
import pyunicore.credentials
import requests
import starlette.status as status

import mantik_api.compute_backend.unicore.client as _client
import mantik_api.compute_backend.unicore.exceptions as exceptions

logger = logging.getLogger(__name__)


def connect_to_unicore_api(
    url: str,
    credentials: pyunicore.credentials.Credential,
) -> _client.Client:
    """Create a connection a UNICORE API.

    Parameters
    ----------
    url : str
        REST API URL to the cluster's UNICORE server.
    credentials : pyunicore.credentials.Credential
        User credentials for the API.

    Raises
    ------
    AuthenticationFailedException
        Authentication on the cluster failed.

    Returns
    -------
    pyunicore.client.Client

    """
    logger.info("Attempting to connect to UNICORE API %s", url)
    client = _connect_to_api(url=url, credentials=credentials)
    if _authentication_failed(client):
        raise exceptions.AuthenticationFailedException(
            f"Failed to authenticate at UNICORE API {url}"
        )
    return _client.Client(client)


def _connect_to_api(
    url: str, credentials: pyunicore.credentials.Credential
) -> pyunicore.client.Client:
    try:
        return pyunicore.client.Client(transport=credentials, site_url=url)
    except requests.exceptions.ConnectionError as e:
        logger.debug("Failed to connect to UNICORE API %s: %s", url, str(e))
        raise exceptions.AuthenticationFailedException(
            f"Failed to connect to UNICORE API {url}"
        ) from e
    except pyunicore.credentials.AuthenticationFailedException as e:
        logger.debug("Failed to authenticate at UNICORE API %s: %s", url, str(e))
        raise exceptions.AuthenticationFailedException(str(e)) from e
    except requests.exceptions.HTTPError as e:
        if e.response.status_code == status.HTTP_403_FORBIDDEN:
            raise exceptions.AuthenticationFailedException(
                f"Incorrect username or password provided for {url}. "
                "The credentials used in the run's connection might be invalid."
            ) from e
        raise e


def _authentication_failed(client: pyunicore.client.Client) -> bool:
    return False if client.properties["client"]["xlogin"] else True
