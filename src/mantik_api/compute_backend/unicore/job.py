import logging
import pathlib

import pyunicore.client
import requests
import starlette.status as status

import mantik_api.compute_backend.content as _content
import mantik_api.compute_backend.job as _job
import mantik_api.compute_backend.unicore._directory as _directory
import mantik_api.compute_backend.unicore.exceptions as exceptions
import mantik_api.database as database
import mantik_api.models as models

logger = logging.getLogger(__name__)


class Job(_job.JobBase):
    """Job submitted to unicore, wrapper class around pyunicore.client.Job"""

    def __init__(self, job: pyunicore.client.Job):
        self._job = job
        self._check_if_job_id_exists()

        # For UNICORE, a job's working directory root is by an empty path.
        # Subdirectories are always relative paths within that directory.
        # Hence, the ZIP archive name should be the job ID for the
        # working directory.
        self._working_directory = _directory.Directory(
            self._job.working_dir, path=pathlib.Path(""), name=self.id
        )

    @property
    def id(self) -> str:
        """Return the job's UNICORE ID."""
        return self._job.job_id

    def _check_if_job_id_exists(self) -> None:
        try:
            # Fetch job properties to test whether URL exists, i.e.
            # if job ID exists.
            _ = self._job.properties
        except requests.exceptions.HTTPError as e:
            if e.response.status_code == status.HTTP_404_NOT_FOUND:
                raise exceptions.JobNotFoundException(
                    f"UNICORE Job with ID {self._job.job_id} not found"
                )
            raise e

    def get_status(self) -> database.run.RunStatus:
        """Return the job status."""
        logger.debug("Getting job status of UNICORE job %s", self.id)
        return self.get_info().status.to_database_run_status()

    def get_info(self, previous_info: dict | None = None) -> models.run_info.Unicore:
        """Return detailed information about the job"""
        logger.debug("Getting job properties of UNICORE job %s", self.id)
        # This property might change with time.
        # Accessing pyunicore.client.Job.properties sends a request
        # at each call and caches the result for a certain amount of time.

        # self._job.bss_details() return a JSON containing
        # the low-level batch system details, but
        # details are only available when the job is running
        # and some minutes afterwards,
        # but after that an error like:
        # {"errorMessage":"Could not get job details:
        # java.lang.Exception: Getting job details on TSI failed: reply was TSI_FAILED:
        # Command 'scontrol show jobid 60' failed with code 1:
        # b'slurm_load_jobs error: Invalid job id specified\\n'\n","status":500}
        # is raised.
        bss_details = None
        try:
            bss_details = self._job.bss_details()
        except requests.exceptions.HTTPError as e:
            if "Could not get job details" not in str(e):
                raise exceptions.UnicoreException(
                    "Getting job batch system details failed"
                ) from e

        return models.run_info.Unicore.from_dict(
            id_=self.id, data=self._job.properties, bss_details=bss_details
        )

    def get_logs(self, previous_info: dict | None = None) -> list[str]:
        """Return the UNICORE logs of the job.

        Parameters
        ----------
        prev_info : dict, optional
            Will be ignored.

            UNICORE always allows direct access to the run directory, where
            the logs file is written. Hence, we don't require any previous
            info about the job.

        Notes
        -----
        Returns both the UNICORE API logs and the application logs
        (assumed to be written to `mantik.log`).

        Concatenates both logs as

        ```
        ==== UNICORE API LOGS ===
        <...>
        ==== APPLICATION LOGS ===
        <...>
        ```

        """
        logger.debug("Getting all logs of UNICORE job %s", self.id)
        unicore_api_logs = self._get_unicore_api_logs()
        application_logs = self._get_application_logs()
        max_line_length = max(len(line) for line in unicore_api_logs + application_logs)
        # Set maximum line length for logs separator to 80
        max_length = min(80, max_line_length)
        return [
            " UNICORE API LOGS ".center(max_length, "="),
            *unicore_api_logs,
            " APPLICATION LOGS ".center(max_length, "="),
            *application_logs,
        ]

    def _get_unicore_api_logs(self) -> list[str]:
        result = []
        logs = self.get_info().logs
        for line in logs:
            result.extend(line.split("\n"))
        return result

    def _get_application_logs(self) -> list[str]:
        """Return the logs of the executed application."""
        logger.debug("Getting application logs of UNICORE job %s", self.id)
        try:
            application_logs = self._working_directory.get_directory_or_file(
                pathlib.Path(_job.APPLICATION_LOGS_FILE)
            )
        except exceptions.NotFoundException as e:
            if f"{_job.APPLICATION_LOGS_FILE} does not exist" in str(e):
                logger.debug(
                    "Application file of job %s missing", self.id, exc_info=True
                )
                return [
                    "Run not yet executed or failed!",
                    (
                        f"Assumed logs (stdout and stderr) to be written to "
                        f"{_job.APPLICATION_LOGS_FILE}, but no such file was found "
                        "in the working directory of the job."
                    ),
                    (
                        "Check above UNICORE API logs, job status and "
                        "submission info, or contact your system administrator."
                    ),
                ]
            raise e
        return application_logs.content.split("\n")

    def cancel(self):
        """Cancel the job."""
        logger.debug("Cancelling application logs of UNICORE job %s", self.id)
        self._job.abort()

    def download(self, path: pathlib.Path | None = None) -> _content.DownloadResponse:
        """Download a file or folder from the job's working directory.

        Parameters
        ----------
        path : pathlib.Path or None
            Path to the file or folder to download.

            If ``None``, the entire working directory of the job will be
            recursively downloaded.

        Returns
        -------
        DownloadResponse
            Either the file as streamed bytes or the zipped directory.

        """
        if path is None or path.as_posix() == "/":
            return self._working_directory.download()
        if path.is_absolute():
            # An absolute path must be converted to a relative path.
            path = path.relative_to("/")
        return self._working_directory.get_directory_or_file(path).download()
