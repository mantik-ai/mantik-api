import logging

import pyunicore.client
import pyunicore.credentials

import mantik_api.compute_backend.unicore.job as _job

logger = logging.getLogger(__name__)


class Client:
    def __init__(self, client: pyunicore.client.Client):
        self._client = client

    @property
    def properties(self) -> dict:
        return self._client.properties

    def get_job(self, job_id: str) -> _job.Job:
        """Get a job by ID."""
        url = f"{self._client.links['jobs']}/{job_id}"
        job = pyunicore.client.Job(security=self._client.transport, job_url=url)
        return _job.Job(job)
