import mantik_api.compute_backend.exceptions as exceptions


class UnicoreException(exceptions.ComputeBackendException):
    """Generic class for all unicore errors."""


class AuthenticationFailedException(UnicoreException):
    """User authentication has failed.

    Unfortunately the response by the server does not give any detailed
    information why the authentication fails.

    """


class NotFoundException(UnicoreException):
    """Resource not found."""


class JobNotFoundException(UnicoreException):
    """Job not found."""
