import abc
import pathlib

import mantik_api.compute_backend.content as _content
import mantik_api.database as database
import mantik_api.models.run_info as run_info

APPLICATION_LOGS_FILE = "mantik.log"


class JobBase(abc.ABC):
    """
    Base class representing a job submitted to either UNICORE
    or firecREST API
    """

    @abc.abstractmethod
    def id(self) -> str:
        raise NotImplementedError

    @abc.abstractmethod
    def get_status(self) -> database.run.RunStatus:
        """Return current status of the job."""
        raise NotImplementedError

    @abc.abstractmethod
    def get_info(
        self, previous_info: dict | None = None
    ) -> run_info.Unicore | run_info.Firecrest | run_info.SSHSlurm:
        """Return detailed information about the job from the external API.

        Parameters
        ----------
        prev_info : dict
            Previous info (`database.run.Run.info`) from a prior API call.

            Passing this allows to merge new info with previous info
            to override updated fields and keep outdated fields.

            Some APIs might not always return the same fields, but old fields
            should still be kept.
        Returns
        -------
        run_info.Unicore | run_info.Firecrest | run_info.SSHSlurm
            The API information about the submitted job.

        """
        raise NotImplementedError

    @abc.abstractmethod
    def get_logs(self, previous_info: dict | None = None) -> list[str]:
        """Return the logs of the application running in the job.

        Parameters
        ----------
        prev_info : dict
            Previous info (`database.run.Run.info`) from a prior API call.

            E.g. for firecREST, these info may contain the path to the logs
            file. If it's unknown, the logs file path will be inferred.

        Returns
        -------
        list[str]
            The application logs.

        """
        raise NotImplementedError

    @abc.abstractmethod
    def download(self, path: pathlib.Path | None = None) -> _content.DownloadResponse:
        """Download a file or folder from the job's working directory.

        Parameters
        ----------
        path : pathlib.Path or None
            Path to the file or folder to download.

            If ``None``, the entire working directory of the job will be
            recursively downloaded.

        Returns
        -------
        DownloadResponse
            Either the file as streamed bytes or the zipped directory.

        """
        raise NotImplementedError

    @abc.abstractmethod
    def cancel(self) -> None:
        """Cancel the job."""
        raise NotImplementedError
