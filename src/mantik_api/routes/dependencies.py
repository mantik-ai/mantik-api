import mantik_api.database.client as _client


def get_db_client() -> _client.main.Client:
    client = _client.main.Client.from_env()
    yield client
