import fastapi
import starlette.status as status

import mantik_api.aws.cognito as cognito
import mantik_api.database as database
import mantik_api.models as models
import mantik_api.routes.dependencies as dependencies


router = fastapi.APIRouter()


@router.post(
    "/forgot-password/initiate",
    responses={
        status.HTTP_200_OK: {
            "model": models.forgot_password.InitiateForgotPasswordUserDetailsPost200Response,  # noqa E501
            "description": "User name",
        },
        status.HTTP_422_UNPROCESSABLE_ENTITY: {
            "model": models.error.HTTPError,
            "description": "Invalid request data",
        },
        status.HTTP_404_NOT_FOUND: {
            "model": models.error.HTTPError,
            "description": "User not found",
        },
    },
    tags=["account recovery"],
    summary="Initiates password recovery",
)
async def forgot_password_initiate_post(
    user_details: models.forgot_password.InitiateForgotPasswordUserDetails = fastapi.Body(  # noqa E501
        ..., description="Username or email required for password recovery"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.forgot_password.InitiateForgotPasswordUserDetailsPost200Response:
    """Initiate password recovery by sending a recovery code.

    Triggers sending of the recovery code that is required
    change the user's password and returns the user name of the
    respective user.

    **Note:** Either user name _or_ email must be provided.
    Both is _not_ possible and will be rejected.

    """
    if user_details.no_option_given:
        raise fastapi.HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail="Either user name or email must be provided",
        )
    elif user_details.all_options_given:
        raise fastapi.HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail="Either user name or email must be provided, not both",
        )

    with db.session():
        try:
            user: database.user.User = db.get_one(
                database.user.User,
                constraints=user_details.create_user_constraint(),
            )
        except database.exceptions.NotFoundError:
            raise fastapi.HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=(
                    f"User with {user_details.option} {user_details.value} not found"
                ),
            )
        else:
            cognito_name = user.cognito_name

    try:
        cognito.users.initiate_forgot_password(cognito_name)
    except cognito.exceptions.UserForgotPasswordFailedException as e:
        raise fastapi.HTTPException(
            status_code=status.HTTP_424_FAILED_DEPENDENCY,
            detail=str(e),
        )

    return models.forgot_password.InitiateForgotPasswordUserDetailsPost200Response(
        user_name=user.preferred_name
    )


@router.put(
    "/forgot-password/confirm",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully reset password",
        },
        status.HTTP_424_FAILED_DEPENDENCY: {
            "model": models.error.HTTPError,
            "description": "Invalid request data",
        },
        status.HTTP_404_NOT_FOUND: {
            "model": models.error.HTTPError,
            "description": "User not found",
        },
    },
    tags=["account recovery"],
    summary="Resets password to the given password",
)
async def forgot_password_confirm_put(
    user_details: models.forgot_password.ConfirmForgotPasswordUserDetails = fastapi.Body(  # noqa E501
        ...,
        description="The user name, confirmation code and new password required for "
        "password reset",
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    """
    Confirms and resets the forgotten password of a user. It requires the user to
    provide their username, a valid confirmation code, and a new password.
    """
    with db.session():
        try:
            user: database.user.User = db.get_one(
                database.user.User,
                constraints={database.user.User.preferred_name: user_details.user_name},
            )
        except database.exceptions.NotFoundError:
            raise fastapi.HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"User with user name {user_details.user_name} not found",
            )
        else:
            cognito_name = user.cognito_name

    try:
        cognito.users.confirm_forgot_password(
            username=cognito_name,
            confirmation_code=user_details.confirmation_code,
            new_password=user_details.new_password,
        )
    except cognito.exceptions.UserConfirmForgotPasswordFailedException as e:
        raise fastapi.HTTPException(
            status_code=status.HTTP_424_FAILED_DEPENDENCY,
            detail=str(e),
        )
