import fastapi
import starlette.status as status

import mantik_api.aws.cognito as cognito
import mantik_api.database as database
import mantik_api.models as models
import mantik_api.routes.dependencies as dependencies

router = fastapi.APIRouter()


@router.post(
    "/signup/resend-confirmation-code",
    responses={
        status.HTTP_200_OK: {
            "model": models.signup.SignupConfirmationCodeUserDetailsPost200Response,
            "description": "User name",
        },
        status.HTTP_404_NOT_FOUND: {
            "model": models.error.HTTPError,
            "description": "User not found",
        },
        status.HTTP_422_UNPROCESSABLE_ENTITY: {
            "model": models.error.HTTPError,
            "description": "Invalid request data",
        },
    },
    tags=["signup"],
    summary="Resends the confirmation code",
)
async def signup_resend_confirmation_code_post(
    user_details: models.signup.ConfirmationCodeUserDetails = fastapi.Body(
        ..., description="The user name or email to resend the code"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.signup.SignupConfirmationCodeUserDetailsPost200Response:
    """Resend the confirmation code to complete the register process.

    Triggers resending of the confirmation code that is required to verify
    an account's email and returns the user name of the respective user.

    **Note:** Either user name _or_ email must be provided.
    Both is _not_ possible and will be rejected.

    """
    if user_details.no_option_given:
        raise fastapi.HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail="Either user name or email must be provided",
        )
    elif user_details.all_options_given:
        raise fastapi.HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail="Either user name or email must be provided, not both",
        )

    with db.session():
        try:
            user: database.user.User = db.get_one(
                database.user.User,
                constraints=user_details.create_user_constraint(),
            )
        except database.exceptions.NotFoundError:
            raise fastapi.HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=(
                    f"User with {user_details.option} {user_details.value} not found"
                ),
            )
        else:
            cognito_name = user.cognito_name

    try:
        cognito.signup.resend_confirmation_code(cognito_name)
    except cognito.exceptions.ConfirmationCodeUserDetailsFailedException as e:
        raise fastapi.HTTPException(
            status_code=status.HTTP_424_FAILED_DEPENDENCY,
            detail=str(e),
        )

    return models.signup.SignupConfirmationCodeUserDetailsPost200Response(
        user_name=user.preferred_name
    )


@router.post(
    "/signup/verify-email",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Email verification successful",
        },
        status.HTTP_404_NOT_FOUND: {
            "model": models.error.HTTPError,
            "description": "User not found",
        },
        status.HTTP_422_UNPROCESSABLE_ENTITY: {
            "model": models.error.HTTPError,
            "description": "Invalid confirmation code",
        },
        status.HTTP_424_FAILED_DEPENDENCY: {
            "model": models.error.HTTPError,
            "description": "Verification failed",
        },
    },
    tags=["signup"],
    summary="Verifies the email",
)
async def signup_verify_email_post(
    details: models.signup.EmailConfirmationDetails = fastapi.Body(
        ..., description="The confirmation code and respective user name"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    """Verify email address.

    Upon signup, the user's email address is unverified. It has to be verified in order
    to be able to log in (i.e. create access and refresh tokens).

    """
    with db.session():
        try:
            user: database.user.User = db.get_one(
                database.user.User,
                constraints={database.user.User.preferred_name: details.user_name},
            )
        except database.exceptions.NotFoundError:
            raise fastapi.HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=(f"User {details.user_name!r} not found"),
            )
        else:
            cognito_name = user.cognito_name

    try:
        cognito.signup.verify_email(
            username=cognito_name, confirmation_code=details.confirmation_code
        )
    except cognito.exceptions.IncorrectConfirmationCodeException as e:
        raise fastapi.HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail=str(e),
        )
    except cognito.exceptions.EmailVerificationFailedException as e:
        raise fastapi.HTTPException(
            status_code=status.HTTP_424_FAILED_DEPENDENCY,
            detail=str(e),
        )
