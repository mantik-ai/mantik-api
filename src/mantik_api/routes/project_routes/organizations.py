import uuid

import fastapi
import starlette.status as status

import mantik_api.database as database
import mantik_api.models as models
import mantik_api.routes._parameters as _parameters
import mantik_api.routes.dependencies as dependencies
import mantik_api.routes.rbac.authorization as authorization
import mantik_api.routes.rbac.bearer as bearer
import mantik_api.routes.rbac.roles as roles

router = fastapi.APIRouter(dependencies=[fastapi.Depends(bearer.OptionalJWTBearer())])


@router.get(
    "/projects/{projectId}/organizations",
    responses={
        status.HTTP_200_OK: {
            "model": models.organization_project_role.ProjectsProjectIdOrganizationGet200Response,  # noqa: E501
            "description": "Organization entries for given project",
        },
    },
    tags=["project collaboration"],
    summary="Gives organization entries for given project",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.PROJECT_ROLES)
async def projects_project_id_organization_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    startindex: int = _parameters.start_index(),
    pagelength: int = _parameters.page_length(),
    order_by: database.project.OrderOrganizationsBy = _parameters.order_by(
        default=database.project.OrderOrganizationsBy.added
    ),
    ascending: bool = _parameters.ascending(),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.organization_project_role.ProjectsProjectIdOrganizationGet200Response:
    with db.session():
        result: database.client.main.GetAllResponse = db.get_many(
            orm_model_type=database.project.OrganizationProjectAssociationTable,
            constraints={
                database.project.OrganizationProjectAssociationTable.project_id: projectId,  # noqa: E501
            },
            offset=startindex,
            limit=pagelength,
            order_by=order_by,
            ascending=ascending,
        )
        return models.organization_project_role.ProjectsProjectIdOrganizationGet200Response(  # noqa: E501
            total_records=result.total_count,
            page_records=result.page_count,
            organizations=models.base.from_database_models(
                model=models.organization_project_role.OrganizationProjectRole,
                database_models=result.entities,
                client=db,
            ),
            user_role=roles.get_request_user_role_in_project(
                project_id=projectId, request=request, client=db
            ).role,
        )


@router.get(
    "/projects/{projectId}/organizations/{organizationId}",
    responses={
        status.HTTP_200_OK: {
            "model": models.organization_project_role.OrganizationProjectRole,
            "description": "Project role for given organization in a project",
        },
    },
    tags=["project collaboration"],
    summary="Returns project role for given organization in a project",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.PROJECT_ROLES)
async def projects_project_id_organization_organization_id_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    organizationId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.organization_project_role.OrganizationProjectRole:
    with db.session():
        project_role = db.get_one(
            orm_model_type=database.project.OrganizationProjectAssociationTable,
            constraints={
                database.project.OrganizationProjectAssociationTable.organization_id: organizationId,  # noqa: E501
                database.project.OrganizationProjectAssociationTable.project_id: projectId,  # noqa: E501
            },
        )
        return models.organization_project_role.OrganizationProjectRole.from_database_model(  # noqa: E501
            project_role,
            client=db,
        )


@router.put(
    "/projects/{projectId}/organizations/{organizationId}",
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully updated project role"
        },
    },
    tags=["project collaboration"],
    summary="Update project role for given organization in a project",
    status_code=status.HTTP_204_NO_CONTENT,
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.OWNER_ROLE
)
async def projects_project_id_organization_organization_id_put(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    organizationId: uuid.UUID,
    project_role: models.project_role.ProjectRole = fastapi.Body(  # noqa: E501
        ..., description="Update project role"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        # This checks that the user is already a member of the project.
        db.get_one(
            orm_model_type=database.project.OrganizationProjectAssociationTable,
            constraints={
                database.project.OrganizationProjectAssociationTable.organization_id: organizationId,  # noqa: E501
                database.project.OrganizationProjectAssociationTable.project_id: projectId,  # noqa: E501
            },
        )
        db.update(
            project_role.to_organization_project_role_model(
                organization_id=organizationId, project_id=projectId
            ),
            constraints={
                database.project.OrganizationProjectAssociationTable.organization_id: organizationId,  # noqa: E501
                database.project.OrganizationProjectAssociationTable.project_id: projectId,  # noqa: E501
            },
        )


@router.delete(
    "/projects/{projectId}/organizations/{organizationId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully removed organization"
        },
    },
    tags=["project collaboration"],
    summary="Remove organization from project",
    description=(
        "Removing an organization from the project will remove access rights"
        "of all members of the respective organization."
        ""
        "This doesn't apply if:"
        ""
        "- one of the organization's members is added explicitly as a project member"
        "- one of the organization's members is added implicitly as a group member"
    ),
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.OWNER_ROLE
)
async def projects_project_id_organization_organization_id_delete(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    organizationId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        # This checks that the user is already a member of the project.
        db.delete(
            database.project.OrganizationProjectAssociationTable,
            constraints={
                database.project.OrganizationProjectAssociationTable.organization_id: organizationId,  # noqa: E501
                database.project.OrganizationProjectAssociationTable.project_id: projectId,  # noqa: E501
            },
        )
