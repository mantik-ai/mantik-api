import uuid

import fastapi
import starlette.status as status

import mantik_api.database as database
import mantik_api.models as models
import mantik_api.routes._parameters as _parameters
import mantik_api.routes.dependencies as dependencies
import mantik_api.routes.rbac.authorization as authorization
import mantik_api.routes.rbac.bearer as bearer


router = fastapi.APIRouter(dependencies=[fastapi.Depends(bearer.OptionalJWTBearer())])


@router.delete(
    "/projects/{projectId}/deployments/{deploymentId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {"description": "Successfully deleted deployment"},
    },
    tags=["deployment"],
    summary="Delete deployment",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_deployments_deployment_id_delete(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    deploymentId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    ...


@router.get(
    "/projects/{projectId}/deployments/{deploymentId}/download",
    responses={
        status.HTTP_200_OK: {
            "model": models.packaged_deployment.PackagedDeployment,
            "description": "Model entry for given ID",
        },
    },
    tags=["deployment"],
    summary="Download packaged deployment",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.WORKING_ROLES)
async def projects_project_id_deployments_deployment_id_download_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    deploymentId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.packaged_deployment.PackagedDeployment:
    ...


@router.get(
    "/projects/{projectId}/deployments/{deploymentId}",
    responses={
        status.HTTP_200_OK: {
            "model": models.deployment_information.DeploymentInformation,
            "description": "Model entry for given ID",
        },
    },
    tags=["deployment"],
    summary="Get deployment information",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.ALL_ROLES)
async def projects_project_id_deployments_deployment_id_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    deploymentId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.deployment_information.DeploymentInformation:
    ...


@router.put(
    "/projects/{projectId}/deployments/{deploymentId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {"description": "Successfully updated deployment"},
    },
    tags=["deployment"],
    summary="Update deployment",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.HEAD_ROLES
)
async def projects_project_id_deployments_deployment_id_put(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    deploymentId: uuid.UUID,
    deployment_information: models.deployment_information.DeploymentInformation = fastapi.Body(  # noqa: E501
        ..., description="Deployment configuration"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    ...


@router.get(
    "/projects/{projectId}/deployments/{deploymentId}/predict",
    responses={
        status.HTTP_200_OK: {
            "model": dict[str, object],
            "description": "Model entry for given ID",
        },
    },
    tags=["deployment"],
    summary="Get deployment information",
    response_model=dict[str, object],
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.PROJECT_ROLES)
async def projects_project_id_deployments_deployment_id_predict_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    deploymentId: uuid.UUID,
    request_body: dict[str, object] = fastapi.Body(
        ..., description="Prediction request, including data"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> dict[str, object]:
    ...


@router.get(
    "/projects/{projectId}/deployments",
    responses={
        status.HTTP_200_OK: {
            "model": models.deployment_information.ProjectsProjectIdDeploymentsGet200Response,  # noqa: E501
            "description": "Model entry for given ID",
        },
    },
    tags=["deployment"],
    summary="list all deployments",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.ALL_ROLES)
async def projects_project_id_deployments_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    startindex: int = _parameters.start_index(),
    pagelength: int = _parameters.page_length(),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.deployment_information.ProjectsProjectIdDeploymentsGet200Response:
    ...


@router.put(
    "/projects/{projectId}/deployments",
    status_code=status.HTTP_201_CREATED,
    responses={
        status.HTTP_201_CREATED: {
            "model": models.deployment_information.ProjectsProjectIdDeploymentsGet201Response,  # noqa: E501
            "description": "Successfully deployed model",
        },
    },
    tags=["deployment"],
    summary="Create deployment",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.HEAD_ROLES
)
async def projects_project_id_deployments_put(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    deployment_information: models.deployment_information.DeploymentInformation = fastapi.Body(  # noqa: E501
        ..., description="Deployment configuration"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.deployment_information.ProjectsProjectIdDeploymentsGet201Response:
    ...
