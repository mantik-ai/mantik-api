import logging
import uuid

import fastapi
import starlette.status as status

import mantik_api.checks as checks
import mantik_api.database as database
import mantik_api.models as models
import mantik_api.routes._parameters as _parameters
import mantik_api.routes.dependencies as dependencies
import mantik_api.routes.project_routes.run_schedules._crud as _crud
import mantik_api.routes.rbac.authorization as authorization
import mantik_api.routes.rbac.bearer as bearer
import mantik_api.routes.rbac.roles as roles

logger = logging.getLogger(__name__)

router = fastapi.APIRouter(dependencies=[fastapi.Depends(bearer.OptionalJWTBearer())])


@router.get(
    "/projects/{projectId}/run-schedules",
    responses={
        status.HTTP_200_OK: {
            "model": models.run_schedule.ProjectsProjectIdRunScheduleGet200Response,
            "description": "OK",
        },
    },
    tags=["run schedules"],
    summary="Returns all run schedules for a given project",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.ALL_ROLES)
async def projects_project_id_run_schedules_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    startindex: int = _parameters.start_index(),
    pagelength: int = _parameters.page_length(),
    order_by: database.run_schedule.OrderBy = _parameters.order_by(
        default=database.run_schedule.OrderBy.created
    ),
    ascending: bool = _parameters.ascending(),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.run_schedule.ProjectsProjectIdRunScheduleGet200Response:
    with db.session():
        result: database.client.main.GetAllResponse = db.get_many(
            orm_model_type=database.run_schedule.RunSchedule,
            constraints={
                database.run_schedule.RunSchedule.project_id: projectId,
            },
            offset=startindex,
            limit=pagelength,
            order_by=order_by,
            ascending=ascending,
        )
        role_details = roles.get_request_user_role_in_project(
            project_id=projectId, request=request, client=db
        )
        return models.run_schedule.ProjectsProjectIdRunScheduleGet200Response(
            total_records=result.total_count,
            page_records=result.page_count,
            run_schedules=models.base.from_database_models(
                models.run_schedule.RunSchedule,
                database_models=result.entities,
                user_role=role_details.role,
                user_id=request.token.user_id if request.token else None,
            ),
            user_role=role_details.role,
        )


@router.get(
    "/projects/{projectId}/run-schedules/{runScheduleId}",
    responses={
        status.HTTP_200_OK: {
            "model": models.run_schedule.RunSchedule,
            "description": "Run schedule for given ID",
        },
    },
    tags=["run schedules"],
    summary="Returns model entry for given ID",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.ALL_ROLES)
async def projects_project_id_runs_schedules_run_schedule_id_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    runScheduleId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.run_schedule.RunSchedule:
    with db.session():
        run_schedule = db.get_one(
            orm_model_type=database.run_schedule.RunSchedule,
            constraints={
                database.run_schedule.RunSchedule.id: runScheduleId,
                database.run_schedule.RunSchedule.project_id: projectId,
            },
            select_in_loading={
                database.run_schedule.RunSchedule.run: {
                    database.run.Run.experiment_repository: {
                        database.experiment_repository.ExperimentRepository.labels: {}
                    },
                    database.run.Run.saved_model_repository: {},
                    database.run.Run.model_repository: {
                        database.model_repository.ModelRepository.labels: {},
                        database.model_repository.ModelRepository.code_repository: {
                            database.code_repository.CodeRepository.labels: {}
                        },
                    },
                    database.run.Run.data_repository: {
                        database.data_repository.DataRepository.labels: {}
                    },
                    database.run.Run.connection: {},
                },
                database.run_schedule.RunSchedule.connection: {},
            },
        )
        role_details = roles.get_request_user_role_in_project(
            project_id=projectId, request=request, client=db
        )
        return models.run_schedule.RunSchedule.from_database_model(
            run_schedule,
            user_role=role_details.role,
            user_id=request.token.user_id if request.token else None,
        )


@router.post(
    "/projects/{projectId}/run-schedules",
    status_code=status.HTTP_201_CREATED,
    responses={
        status.HTTP_201_CREATED: {
            "description": "Created new run schedule",
        },
    },
    tags=["run schedules"],
    summary="Creates a new run schedule",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.HEAD_ROLES
)
async def projects_project_id_runs_schedules_post(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    add_run: models.run_schedule.AddRunSchedule = fastapi.Body(
        ..., description="Run schedule information"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.run_schedule.ProjectsProjectIdRunScheduleGet201Response:
    new_run_schedule_id = uuid.uuid4()
    with db.session():
        checks.user_owns_connection(
            connection_id=add_run.connection_id, user_id=add_run.owner_id, db=db
        )
        _crud.add_run_schedule(
            id_=new_run_schedule_id,
            project_id=projectId,
            add_run=add_run,
            token=request.token,
            db=db,
        )
        return models.run_schedule.ProjectsProjectIdRunScheduleGet201Response(
            run_schedule_id=new_run_schedule_id
        )


@router.put(
    "/projects/{projectId}/run-schedules/{runScheduleId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully updated run schedule",
        },
    },
    tags=["run schedules"],
    summary="Update run schedule for a specific run schedule in a project",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.HEAD_ROLES,
)
async def projects_project_id_runs_schedules_run_schedule_id_put(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    runScheduleId: uuid.UUID,
    add_run: models.run_schedule.AddRunSchedule = fastapi.Body(
        ..., description="Run schedule information"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        checks.user_owns_connection(
            connection_id=add_run.connection_id, user_id=add_run.owner_id, db=db
        )
        _crud.update_run_schedule(
            id_=runScheduleId,
            project_id=projectId,
            add_run=add_run,
            token=request.token,
            db=db,
        )


@router.delete(
    "/projects/{projectId}/run-schedules/{runScheduleId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully deleted run schedule",
        },
    },
    tags=["run schedules"],
    summary="Delete run schedule",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.HEAD_ROLES,
)
async def projects_project_id_runs_schedules_run_schedule_id_delete(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    runScheduleId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        _crud.delete_run_schedule(
            run_schedule_id=runScheduleId,
            project_id=projectId,
            db=db,
        )
