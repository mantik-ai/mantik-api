import uuid

import mantik_api.aws.scheduler as scheduler
import mantik_api.database as database
import mantik_api.models as models
import mantik_api.tokens.jwt as jwt


def add_run_schedule(
    id_: uuid.UUID,
    project_id: uuid.UUID,
    add_run: models.run_schedule.AddRunSchedule,
    token: jwt.JWT,
    db: database.client.main.Client,
) -> None:
    aws_schedule_arn = _create_run_schedule_on_aws(
        id_=id_, add_run=add_run, project_id=project_id, token=token
    )
    _add_run_schedule_to_db(
        id_=id_,
        aws_schedule_arn=aws_schedule_arn,
        project_id=project_id,
        add_run=add_run,
        db=db,
    )


def _create_run_schedule_on_aws(
    id_: uuid.UUID,
    add_run: models.run_schedule.AddRunSchedule,
    project_id: uuid.UUID,
    token: jwt.JWT,
) -> str:
    properties = add_run.to_aws_run_schedule_properties(
        id_=id_, project_id=project_id, token=token
    )
    with scheduler.client.Client() as client:
        return client.create(properties)


def _add_run_schedule_to_db(
    id_: uuid.UUID,
    aws_schedule_arn: str,
    project_id: uuid.UUID,
    add_run: models.run_schedule.AddRunSchedule,
    db: database.client.main.Client,
) -> None:
    model = add_run.to_database_model(
        id_=id_, aws_schedule_arn=aws_schedule_arn, project_id=project_id
    )
    db.add(model)


def update_run_schedule(
    id_: uuid.UUID,
    project_id: uuid.UUID,
    add_run: models.run_schedule.AddRunSchedule,
    token: jwt.JWT,
    db: database.client.main.Client,
) -> None:
    aws_schedule_arn = _update_run_schedule_on_aws(
        id_=id_, add_run=add_run, project_id=project_id, token=token
    )
    _update_run_schedule_in_db(
        id_=id_,
        aws_schedule_arn=aws_schedule_arn,
        project_id=project_id,
        add_run=add_run,
        db=db,
    )


def _update_run_schedule_on_aws(
    id_: uuid.UUID,
    add_run: models.run_schedule.AddRunSchedule,
    project_id: uuid.UUID,
    token: jwt.JWT,
) -> str:
    properties = add_run.to_aws_run_schedule_properties(
        id_=id_, project_id=project_id, token=token
    )
    with scheduler.client.Client() as client:
        return client.update(properties)


def _update_run_schedule_in_db(
    id_: uuid.UUID,
    aws_schedule_arn: str,
    project_id: uuid.UUID,
    add_run: models.run_schedule.AddRunSchedule,
    db: database.client.main.Client,
) -> None:
    old_run_schedule = db.get_one(
        orm_model_type=database.run_schedule.RunSchedule,
        constraints={
            database.run_schedule.RunSchedule.id: id_,
            database.run_schedule.RunSchedule.project_id: project_id,
        },
    )
    db.overwrite(
        old_run_schedule,
        fields_to_overwrite=add_run.to_overwrite_kwargs(
            aws_schedule_arn=aws_schedule_arn, client=db
        ),
    )


def delete_run_schedule(
    run_schedule_id: uuid.UUID, project_id: uuid.UUID, db: database.client.main.Client
) -> None:
    _delete_run_schedule_from_aws(run_schedule_id)
    _delete_run_schedule_from_db(
        run_schedule_id=run_schedule_id,
        project_id=project_id,
        db=db,
    )


def _delete_run_schedule_from_aws(id_: uuid.UUID) -> None:
    with scheduler.client.Client() as client:
        properties = scheduler.schedule.PropertiesDelete(id=id_)
        client.delete(properties)


def _delete_run_schedule_from_db(
    run_schedule_id: uuid.UUID, project_id: uuid.UUID, db: database.client.main.Client
) -> None:
    db.delete(
        database.run_schedule.RunSchedule,
        constraints={
            database.run_schedule.RunSchedule.id: run_schedule_id,
            database.run_schedule.RunSchedule.project_id: project_id,
        },
    )
