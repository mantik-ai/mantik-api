import uuid

import fastapi
import starlette.status as status

import mantik_api.database as database
import mantik_api.models as models
import mantik_api.routes._parameters as _parameters
import mantik_api.routes.dependencies as dependencies
import mantik_api.routes.rbac.authorization as authorization
import mantik_api.routes.rbac.bearer as bearer
import mantik_api.routes.rbac.roles as roles
import mantik_api.utils.git_utils as git_utils

router = fastapi.APIRouter(dependencies=[fastapi.Depends(bearer.OptionalJWTBearer())])


@router.get(
    "/projects/{projectId}/code/is-unlocked",
    responses={
        status.HTTP_200_OK: {
            "description": "Repositories checked successfully",
        },
    },
    tags=["code repository"],
    summary="Returns whether repositories are accessible to user.",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.PROJECT_ROLES)
async def projects_project_id_code_is_unlocked_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    codeRepositoryIds: list[uuid.UUID] = fastapi.Query(
        ..., description="IDs of code repositories to check"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> dict[uuid.UUID, bool]:
    with db.session():
        return git_utils.check_if_repos_are_unlocked(
            db=db,
            project_id=projectId,
            repo_ids=codeRepositoryIds,
            orm=database.code_repository.CodeRepository,
            token=request.token,
        )


@router.get(
    "/projects/{projectId}/code/{codeRepositoryId}",
    responses={
        status.HTTP_200_OK: {
            "model": models.code_repository.CodeRepository,
            "description": "Code entry for given project",
        },
    },
    tags=["code repository"],
    summary="Returns code entry for given project",
    description="The `connectionId` here refers to a user's git connection. "
    "There can be multiple git connections (from multiple different users)"
    "assigned to the repo. However, only the caller's connection is returned.",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.ALL_ROLES)
async def projects_project_id_code_code_repository_id_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    codeRepositoryId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.code_repository.CodeRepository:
    with db.session():
        code_repository = db.get_one(
            orm_model_type=database.code_repository.CodeRepository,
            constraints={
                database.code_repository.CodeRepository.id: codeRepositoryId,
                database.code_repository.CodeRepository.project_id: projectId,
            },
            select_in_loading={
                database.code_repository.CodeRepository.labels: {},
                database.code_repository.CodeRepository.connections: {},
            },
        )
        role_details = roles.get_request_user_role_in_project(
            project_id=projectId, request=request, client=db
        )
        return models.code_repository.CodeRepository.from_database_model(
            code_repository,
            user_role=role_details.role,
            user_id=request.token.user_id if request.token else None,
        )


@router.put(
    "/projects/{projectId}/code/{codeRepositoryId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully updated code repository"
        },
        status.HTTP_400_BAD_REQUEST: {
            "model": models.error.HTTPError,
            "description": "Label assignment failed due to invalid scope",
        },
    },
    tags=["code repository"],
    summary="Updates code repository",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_code_code_repository_id_put(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    codeRepositoryId: uuid.UUID,
    updated_code_repository_payload: models.code_repository.UpdateCodeRepository = fastapi.Body(  # noqa: E501
        ...,
        description="Updated code repository information",
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        try:
            models.label.validate_labels_scope(
                updated_code_repository_payload.labels,
                expected_scope=database.label.Scope.Code,
                client=db,
            )
        except ValueError as e:
            raise fastapi.HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail=str(e)
            )
        code_repository = db.get_one(
            orm_model_type=database.code_repository.CodeRepository,
            constraints={
                database.code_repository.CodeRepository.id: codeRepositoryId,
                database.code_repository.CodeRepository.project_id: projectId,
            },
        )
        db.overwrite(
            code_repository,
            fields_to_overwrite=updated_code_repository_payload.to_overwrite_kwargs(
                client=db,
                exclude_unset=False,
            ),
        )


@router.delete(
    "/projects/{projectId}/code/{codeRepositoryId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully deleted Code Repository"
        },
    },
    tags=["code repository"],
    summary="Delete Code Repository",
    description=(
        "Deletion of a Code Repository is prevented when there is still at least "
        "one of the following objects referencing the respective Code Repository:"
        ""
        "- Model Repository"
        "- Run (implicitly through Model Repository)"
        ""
        "Each respective instance has to be manually deleted."
    ),
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_code_code_repository_id_delete(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    codeRepositoryId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        db.delete(
            database.code_repository.CodeRepository,
            constraints={
                database.code_repository.CodeRepository.id: codeRepositoryId,
                database.code_repository.CodeRepository.project_id: projectId,
            },
        )


@router.get(
    "/projects/{projectId}/code",
    responses={
        status.HTTP_200_OK: {
            "model": models.code_repository.ProjectsProjectIdCodeGet200Response,
            "description": "Code entries for given project",
        },
    },
    tags=["code repository"],
    summary="Returns code entries for given project",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.ALL_ROLES)
async def projects_project_id_code_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    startindex: int = _parameters.start_index(),
    pagelength: int = _parameters.page_length(),
    order_by: database.code_repository.OrderBy = _parameters.order_by(
        default=database.code_repository.OrderBy.created
    ),
    ascending: bool = _parameters.ascending(),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.code_repository.ProjectsProjectIdCodeGet200Response:
    with db.session():
        result: database.client.main.GetAllResponse = db.get_many(
            orm_model_type=database.code_repository.CodeRepository,
            constraints={
                database.code_repository.CodeRepository.project_id: projectId,
            },
            select_in_loading={database.code_repository.CodeRepository.labels: {}},
            offset=startindex,
            limit=pagelength,
            order_by=order_by,
            ascending=ascending,
        )
        role_details = roles.get_request_user_role_in_project(
            project_id=projectId, request=request, client=db
        )
        return models.code_repository.ProjectsProjectIdCodeGet200Response(
            total_records=result.total_count,
            page_records=result.page_count,
            code_repositories=models.base.from_database_models(
                models.code_repository.CodeRepository,
                database_models=result.entities,
                user_role=role_details.role,
                user_id=request.token.user_id if request.token else None,
            ),
            user_role=role_details.role,
        )


@router.post(
    "/projects/{projectId}/code",
    status_code=status.HTTP_201_CREATED,
    responses={
        status.HTTP_201_CREATED: {
            "model": models.code_repository.ProjectsProjectIdCodeGet201Response,
            "description": "Successfully added code repository",
        },
        status.HTTP_400_BAD_REQUEST: {
            "model": models.error.HTTPError,
            "description": "Label assignment failed due to invalid scope",
        },
    },
    tags=["code repository"],
    summary="Add code repository",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_code_post(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    new_code_repository: models.code_repository.AddCodeRepository = fastapi.Body(
        ..., description="Added code repository information"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.code_repository.ProjectsProjectIdCodeGet201Response:
    new_code_repository_id = uuid.uuid4()
    with db.session():
        try:
            models.label.validate_labels_scope(
                new_code_repository.labels, database.label.Scope.Code, db
            )
        except ValueError as e:
            raise fastapi.HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail=str(e)
            )
        db.add(
            new_code_repository.to_database_model(
                project_id=projectId,
                code_repository_id=new_code_repository_id,
                client=db,
            )
        )
        if new_code_repository.connection_id:
            connection = db.get_one(
                orm_model_type=database.connection.Connection,
                constraints={
                    database.connection.Connection.id: new_code_repository.connection_id,  # noqa 501
                    database.connection.Connection.user_id: request.token.user_id,
                },
            )
            git_utils.verify_platform_matches(
                repository_platform=new_code_repository.platform,
                connection_platform=connection.connection_provider,
            )
            db.commit()
            db.add(
                database.code_repository.CodeRepositoryAndConnectionAssociationTable(
                    id=uuid.uuid4(),
                    code_repository_id=new_code_repository_id,
                    connection_id=new_code_repository.connection_id,
                )
            )
    return models.code_repository.ProjectsProjectIdCodeGet201Response(
        code_repository_id=new_code_repository_id
    )


@router.post(
    "/projects/{projectId}/code/{codeRepositoryId}/connection",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully added connection",
        },
    },
    tags=["code repository"],
    summary="Add a git connection",
    description="Add a git connection to the repository. Only a connection "
    "with a GIT-based connection provider is accepted. Also, "
    "only 1 git connection is accepted per user.",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_code_code_repository_id_connection(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    codeRepositoryId: uuid.UUID,
    repo_connector: models.connection.ConnectionId = fastapi.Body(
        ..., description="Added a new connection to repository"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    with db.session():
        code_repository = db.get_one(
            orm_model_type=database.code_repository.CodeRepository,
            constraints={
                database.code_repository.CodeRepository.id: codeRepositoryId,
                database.code_repository.CodeRepository.project_id: projectId,
            },
            select_in_loading={
                database.code_repository.CodeRepository.connections: {},
            },
        )
        connection = db.get_one(
            orm_model_type=database.connection.Connection,
            constraints={
                database.connection.Connection.id: repo_connector.connection_id,
                database.connection.Connection.user_id: request.token.user_id,
            },
            select_in_loading={
                database.connection.Connection.code_repositories: {},
            },
        )

        if connection.is_git_connection and code_repository.my_git_connection_id(
            request.token.user_id
        ):
            raise fastapi.HTTPException(
                422, detail="Code repository already has a git connection."
            )
        git_utils.verify_platform_matches(
            repository_platform=code_repository.platform,
            connection_platform=connection.connection_provider,
        )
        db.add(
            database.code_repository.CodeRepositoryAndConnectionAssociationTable(
                id=uuid.uuid4(),
                code_repository_id=code_repository.id,
                connection_id=repo_connector.connection_id,
            )
        )


@router.delete(
    "/projects/{projectId}/code/{codeRepositoryId}/connection/{connectionId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully removed a connection from code repository"
        },
    },
    tags=["code repository"],
    summary="Remove a connection from a code repository",
    description="Remove a connection from a code repository. You can only remove your "
    "git connection. Other's connections are not accessible to the caller.",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_code_code_repository_id_connection_id_delete(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    codeRepositoryId: uuid.UUID,
    connectionId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        # ensure user owns the connection
        db.get_one(
            orm_model_type=database.connection.Connection,
            constraints={
                database.connection.Connection.id: connectionId,
                database.connection.Connection.user_id: request.token.user_id,
            },
        )

        code_repo_connection = db.get_one(
            orm_model_type=database.code_repository.CodeRepositoryAndConnectionAssociationTable,  # noqa E501
            constraints={
                database.code_repository.CodeRepositoryAndConnectionAssociationTable.connection_id: connectionId,  # noqa E501
                database.code_repository.CodeRepositoryAndConnectionAssociationTable.code_repository_id: codeRepositoryId,  # noqa E501
            },
        )
        db.delete(code_repo_connection)
