import uuid

import fastapi
import starlette.status as status

import mantik_api.database as database
import mantik_api.models as models
import mantik_api.routes._parameters as _parameters
import mantik_api.routes.dependencies as dependencies
import mantik_api.routes.rbac.authorization as authorization
import mantik_api.routes.rbac.bearer as bearer

router = fastapi.APIRouter(dependencies=[fastapi.Depends(bearer.OptionalJWTBearer())])


@router.get(
    "/projects/{projectId}/usage/code",
    responses={
        status.HTTP_200_OK: {
            "model": models.runs_by_code_repository.ProjectsProjectIdUsageCodeGet200Response,  # noqa: E501
            "description": "CodeRepos for given project with their respective runs",
        },
    },
    tags=["usage"],
    summary="Returns all codeRepos for given project "
    "and runs that the codeRepos were used in",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.ALL_ROLES)
async def projects_project_id_usage_code_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    startindex: int = _parameters.start_index(),
    pagelength: int = _parameters.page_length(),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.runs_by_code_repository.ProjectsProjectIdUsageCodeGet200Response:
    ...


@router.get(
    "/projects/{projectId}/usage/data",
    responses={
        status.HTTP_200_OK: {
            "model": models.runs_by_data_repository.ProjectsProjectIdUsageDataGet200Response,  # noqa: E501
            "description": "Data entries for given project with their respective runs",
        },
    },
    tags=["usage"],
    summary="Returns data entries for given project and runs grouped by the data",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.ALL_ROLES)
async def projects_project_id_usage_data_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    startindex: int = _parameters.start_index(),
    pagelength: int = _parameters.page_length(),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.runs_by_data_repository.ProjectsProjectIdUsageDataGet200Response:
    ...


@router.get(
    "/projects/{projectId}/usage/experiments",
    responses={
        status.HTTP_200_OK: {
            "model": models.runs_by_experiment.ProjectsProjectIdUsageExperimentsGet200Response,  # noqa: E501
            "description": "Experiments for given project with their respective runs",
        },
    },
    tags=["usage"],
    summary="Returns all experiments for given project "
    "and runs that the experiments were used in",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.ALL_ROLES)
async def projects_project_id_usage_experiments_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    startindex: int = _parameters.start_index(),
    pagelength: int = _parameters.page_length(),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.runs_by_experiment.ProjectsProjectIdUsageExperimentsGet200Response:
    ...


@router.get(
    "/projects/{projectId}/usage/models",
    responses={
        status.HTTP_200_OK: {
            "model": models.runs_by_model.ProjectsProjectIdUsageModelsGet200Response,
            "description": "models for given project with their respective runs",
        },
    },
    tags=["usage"],
    summary="Returns all models for given project and "
    "runs that the models were created in",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.ALL_ROLES)
async def projects_project_id_usage_models_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    startindex: int = _parameters.start_index(),
    pagelength: int = _parameters.page_length(),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.runs_by_model.ProjectsProjectIdUsageModelsGet200Response:
    ...
