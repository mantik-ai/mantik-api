import uuid

import fastapi
import starlette.status as status

import mantik_api.database as database
import mantik_api.models as models
import mantik_api.routes._parameters as _parameters
import mantik_api.routes.dependencies as dependencies
import mantik_api.routes.rbac.authorization as authorization
import mantik_api.routes.rbac.bearer as bearer
import mantik_api.routes.rbac.roles as roles
import mantik_api.utils.mlflow as mlflow

router = fastapi.APIRouter(dependencies=[fastapi.Depends(bearer.OptionalJWTBearer())])


@router.get(
    "/projects/{projectId}/experiments/{experimentRepositoryId}",
    responses={
        status.HTTP_200_OK: {
            "model": models.experiment_repository.ExperimentRepository,
            "description": "Experiment entry for given project",
        },
    },
    tags=["experiment repository"],
    summary="Returns experiment entry for given project",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.ALL_ROLES)
async def projects_project_id_experiments_experiment_repository_id_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    experimentRepositoryId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.experiment_repository.ExperimentRepository:
    with db.session():
        experiment_repository = db.get_one(
            orm_model_type=database.experiment_repository.ExperimentRepository,
            constraints={
                database.experiment_repository.ExperimentRepository.id: experimentRepositoryId,  # noqa: E501
                database.experiment_repository.ExperimentRepository.project_id: projectId,  # noqa: E501
            },
            select_in_loading={
                database.experiment_repository.ExperimentRepository.labels: {}
            },
        )
        role_details = roles.get_request_user_role_in_project(
            project_id=projectId, request=request, client=db
        )
        return models.experiment_repository.ExperimentRepository.from_database_model(
            experiment_repository, user_role=role_details.role
        )


@router.put(
    "/projects/{projectId}/experiments/{experimentRepositoryId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully updated experiment repository"
        },
        status.HTTP_400_BAD_REQUEST: {
            "model": models.error.HTTPError,
            "description": "Label assignment failed due to invalid scope",
        },
    },
    tags=["experiment repository"],
    summary="Update experiment repository",
    description=(
        "Experiment names must be unique. If a non-unique name is provided, "
        "a unique integer suffix will be automatically added to the experiment name."
    ),
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_experiments_experiment_repository_id_put(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    experimentRepositoryId: uuid.UUID,
    updated_experiment_repository: models.experiment_repository.AddExperimentRepository = fastapi.Body(  # noqa: E501
        ..., description="Updated data repository information"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        try:
            models.label.validate_labels_scope(
                updated_experiment_repository.labels,
                expected_scope=database.label.Scope.Experiment,
                client=db,
            )
        except ValueError as e:
            raise fastapi.HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail=str(e)
            )
        updated_experiment_repository = _create_unique_name(
            experiment_repository=updated_experiment_repository,
            request=request,
        )
        experiment_repository = db.get_one(
            orm_model_type=database.experiment_repository.ExperimentRepository,
            constraints={
                database.experiment_repository.ExperimentRepository.id: experimentRepositoryId,  # noqa: E501
                database.experiment_repository.ExperimentRepository.project_id: projectId,  # noqa: E501
            },
        )
        db.overwrite(
            experiment_repository,
            fields_to_overwrite=updated_experiment_repository.to_overwrite_kwargs(
                client=db,
                exclude_unset=False,
            ),
        )
    mlflow.experiments.update(
        experiment_id=experiment_repository.mlflow_experiment_id,
        experiment_name=updated_experiment_repository.name,
        token=request.token,
    )


@router.delete(
    "/projects/{projectId}/experiments/{experimentRepositoryId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully deleted Experiment Repository"
        },
    },
    tags=["experiment repository"],
    summary="Delete Experiment Repository",
    description=(
        "Deleting an Experiment Repository will implicitly remove all of the following "
        "related objects:"
        ""
        "- Runs"
        "- MLflow runs, including logged parameters, metrics, artifacts, models, etc."
    ),
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_experiments_experiment_repository_id_delete(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    experimentRepositoryId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        experiment_repository = db.get_one(
            database.experiment_repository.ExperimentRepository,
            constraints={
                database.experiment_repository.ExperimentRepository.id: experimentRepositoryId,  # noqa: E501
                database.experiment_repository.ExperimentRepository.project_id: projectId,  # noqa: E501
            },
        )
        mlflow_experiment_id = experiment_repository.mlflow_experiment_id
        db.delete(experiment_repository)
        mlflow.experiments.delete(
            experiment_id=mlflow_experiment_id,
            token=request.token,
        )


@router.get(
    "/projects/{projectId}/experiments",
    responses={
        status.HTTP_200_OK: {
            "model": models.experiment_repository.ProjectsProjectIdExperimentsGet200Response,  # noqa: E501
            "description": "Experiment entries for given project",
        },
    },
    tags=["experiment repository"],
    summary="Returns experiment entries for given project",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.ALL_ROLES)
async def projects_project_id_experiments_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    startindex: int = _parameters.start_index(),
    pagelength: int = _parameters.page_length(),
    order_by: database.experiment_repository.OrderBy = _parameters.order_by(
        default=database.experiment_repository.OrderBy.created
    ),
    ascending: bool = _parameters.ascending(),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.experiment_repository.ProjectsProjectIdExperimentsGet200Response:
    with db.session():
        result: database.client.main.GetAllResponse = db.get_many(
            orm_model_type=database.experiment_repository.ExperimentRepository,
            constraints={
                database.experiment_repository.ExperimentRepository.project_id: projectId,  # noqa: E501
            },
            select_in_loading={
                database.experiment_repository.ExperimentRepository.labels: {}
            },
            offset=startindex,
            limit=pagelength,
            order_by=order_by,
            ascending=ascending,
        )
        role_details = roles.get_request_user_role_in_project(
            project_id=projectId, request=request, client=db
        )
        return models.experiment_repository.ProjectsProjectIdExperimentsGet200Response(
            total_records=result.total_count,
            page_records=result.page_count,
            experiment_repositories=models.base.from_database_models(
                models.experiment_repository.ExperimentRepository,
                database_models=result.entities,
                user_role=role_details.role,
            ),
            user_role=role_details.role,
        )


@router.post(
    "/projects/{projectId}/experiments",
    status_code=status.HTTP_201_CREATED,
    responses={
        status.HTTP_201_CREATED: {
            "model": models.experiment_repository.ProjectsProjectIdExperimentsGet201Response,  # noqa: E501
            "description": "Successfully added experiment repository",
        },
        status.HTTP_400_BAD_REQUEST: {
            "model": models.error.HTTPError,
            "description": "Label assignment failed due to invalid scope",
        },
    },
    tags=["experiment repository"],
    summary="Add experiment repository",
    description=(
        "Experiment names must be unique. If a non-unique name is provided, "
        "a unique integer suffix will be automatically added to the experiment name."
    ),
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_experiments_post(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    new_experiment_repository: models.experiment_repository.AddExperimentRepository = fastapi.Body(  # noqa: E501
        ..., description="Added data repository information"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.experiment_repository.ProjectsProjectIdExperimentsGet201Response:
    with db.session():
        try:
            models.label.validate_labels_scope(
                new_experiment_repository.labels,
                expected_scope=database.label.Scope.Experiment,
                client=db,
            )
        except ValueError as e:
            raise fastapi.HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail=str(e)
            )

        new_experiment_repository = _create_unique_name(
            experiment_repository=new_experiment_repository,
            request=request,
        )
        mlflow_experiment_id = mlflow.experiments.create(
            experiment_name=new_experiment_repository.name,
            token=request.token,
        )
        new_experiment_repository_id = uuid.uuid4()

        db.add(
            new_experiment_repository.to_database_model(
                project_id=projectId,
                experiment_repository_id=new_experiment_repository_id,
                mlflow_experiment_id=mlflow_experiment_id,
                client=db,
            )
        )
    return models.experiment_repository.ProjectsProjectIdExperimentsGet201Response(
        experiment_repository_id=new_experiment_repository_id
    )


@router.get(
    "/projects/{projectId}/experiments/{experimentRepositoryId}/unique-mlflow-run-name",
    responses={
        status.HTTP_200_OK: {
            "model": str,
            "description": "Unique run name for this experiment",
        },
        status.HTTP_404_NOT_FOUND: {
            "model": models.error.HTTPError,
            "description": "Experiment not found",
        },
        status.HTTP_422_UNPROCESSABLE_ENTITY: {
            "model": models.error.HTTPError,
            "description": "Unprocessable entity",
        },
    },
    tags=["experiment repository"],
    summary="Returns a mlflow unique run name for this experiment",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_experiments_experiment_repository_id_run_unique_name_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    experimentRepositoryId: uuid.UUID,
    runName: str = fastapi.Query(None, description="Run name"),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> str:
    """Run names must be unique for each experiment in mlflow.
    This endpoint returns a unique name for a run in this experiment."""
    with db.session():
        experiment_repository = db.get_one(
            orm_model_type=database.experiment_repository.ExperimentRepository,
            constraints={
                database.experiment_repository.ExperimentRepository.id: experimentRepositoryId,  # noqa: E501
                database.experiment_repository.ExperimentRepository.project_id: projectId,  # noqa: E501
            },
        )
    return mlflow.runs.create_unique_name(
        experiment_id=experiment_repository.mlflow_experiment_id,
        name=runName,
        token=request.token,
    )


def _create_unique_name(
    experiment_repository: models.experiment_repository.AddExperimentRepository,
    request: bearer.ModifiedRequest,
) -> models.experiment_repository.AddExperimentRepository:
    name = mlflow.experiments.create_unique_name(
        name=experiment_repository.name,
        token=request.token,
    )
    experiment_repository.name = name
    return experiment_repository
