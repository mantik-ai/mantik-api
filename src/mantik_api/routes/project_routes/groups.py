import uuid

import fastapi
import starlette.status as status

import mantik_api.database as database
import mantik_api.models as models
import mantik_api.routes._parameters as _parameters
import mantik_api.routes.dependencies as dependencies
import mantik_api.routes.rbac.authorization as authorization
import mantik_api.routes.rbac.bearer as bearer
import mantik_api.routes.rbac.roles as roles


router = fastapi.APIRouter(dependencies=[fastapi.Depends(bearer.OptionalJWTBearer())])


@router.get(
    "/projects/{projectId}/groups",
    responses={
        status.HTTP_200_OK: {
            "model": models.user_group_project_role.ProjectsProjectIdGroupsGet200Response,  # noqa: E501
            "description": "Groups entries for given project",
        },
    },
    tags=["project collaboration"],
    summary="Gives groups entries for given project",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.PROJECT_ROLES)
async def projects_project_id_groups_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    startindex: int = _parameters.start_index(),
    pagelength: int = _parameters.page_length(),
    order_by: database.project.OrderUserGroupsBy = _parameters.order_by(
        default=database.project.OrderUserGroupsBy.added
    ),
    ascending: bool = _parameters.ascending(),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.user_group_project_role.ProjectsProjectIdGroupsGet200Response:
    with db.session():
        result: database.client.main.GetAllResponse = db.get_many(
            orm_model_type=database.project.UserGroupProjectAssociationTable,
            constraints={
                database.project.UserGroupProjectAssociationTable.project_id: projectId,
            },
            offset=startindex,
            limit=pagelength,
            order_by=order_by,
            ascending=ascending,
        )
        return models.user_group_project_role.ProjectsProjectIdGroupsGet200Response(
            total_records=result.total_count,
            page_records=result.page_count,
            groups=models.base.from_database_models(
                model=models.user_group_project_role.UserGroupProjectRole,
                database_models=result.entities,
                client=db,
            ),
            user_role=roles.get_request_user_role_in_project(
                project_id=projectId, request=request, client=db
            ).role,
        )


@router.get(
    "/projects/{projectId}/groups/{groupId}",
    responses={
        status.HTTP_200_OK: {
            "model": models.user_group_project_role.UserGroupProjectRole,
            "description": "Project role for given group in a project",
        },
    },
    tags=["project collaboration"],
    summary="Returns project role for given group in a project",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.PROJECT_ROLES)
async def projects_project_id_groups_group_id_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    groupId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.user_group_project_role.UserGroupProjectRole:
    with db.session():
        project_role = db.get_one(
            orm_model_type=database.project.UserGroupProjectAssociationTable,
            constraints={
                database.project.UserGroupProjectAssociationTable.user_group_id: groupId,  # noqa: E501
                database.project.UserGroupProjectAssociationTable.project_id: projectId,  # noqa: E501
            },
        )
        return models.user_group_project_role.UserGroupProjectRole.from_database_model(
            project_role, client=db
        )


@router.put(
    "/projects/{projectId}/groups/{groupId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully updated project role"
        },
    },
    tags=["project collaboration"],
    summary="Update project role for given group in a project",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.OWNER_ROLE
)
async def projects_project_id_groups_group_id_put(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    groupId: uuid.UUID,
    project_role: models.project_role.ProjectRole = fastapi.Body(  # noqa: E501
        ..., description="Update project role"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        # This checks that the user is already a member of the project.
        db.get_one(
            orm_model_type=database.project.UserGroupProjectAssociationTable,
            constraints={
                database.project.UserGroupProjectAssociationTable.user_group_id: groupId,  # noqa: E501
                database.project.UserGroupProjectAssociationTable.project_id: projectId,  # noqa: E501
            },
        )
        db.update(
            project_role.to_user_group_project_role_model(
                user_group_id=groupId, project_id=projectId
            ),
            constraints={
                database.project.UserGroupProjectAssociationTable.user_group_id: groupId,  # noqa: E501
                database.project.UserGroupProjectAssociationTable.project_id: projectId,  # noqa: E501
            },
        )


@router.delete(
    "/projects/{projectId}/groups/{groupId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {"description": "Successfully removed user group"},
    },
    tags=["project collaboration"],
    summary="Remove user group from project",
    description=(
        "Removing a user group from the project will remove access rights"
        "of all members of the respective user group."
        ""
        "This doesn't apply if:"
        ""
        "- one of the group's members is added explicitly as a project member"
        "- one of the group's members is added implicitly as an organization member"
    ),
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.OWNER_ROLE
)
async def projects_project_id_groups_group_id_delete(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    groupId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        db.delete(
            database.project.UserGroupProjectAssociationTable,
            constraints={
                database.project.UserGroupProjectAssociationTable.user_group_id: groupId,  # noqa: E501
                database.project.UserGroupProjectAssociationTable.project_id: projectId,  # noqa: E501
            },
        )
