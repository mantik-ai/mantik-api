import uuid

import fastapi
import starlette.status as status

import mantik_api.database as database
import mantik_api.models as models
import mantik_api.models.environment as env_models
import mantik_api.routes._parameters as _parameters
import mantik_api.routes.dependencies as dependencies
import mantik_api.routes.rbac.authorization as authorization
import mantik_api.routes.rbac.bearer as bearer
import mantik_api.routes.rbac.roles as roles
import mantik_api.service.environment as env_service

router = fastapi.APIRouter(dependencies=[fastapi.Depends(bearer.OptionalJWTBearer())])
TAGS = ["environments"]


@router.delete(
    "/projects/{projectId}/environments/{environmentId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {"description": "Successfully deleted environment"},
    },
    tags=TAGS,
    summary="Delete environment from given project",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_environments_environment_id_delete(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    environmentId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        env_service.get_environment(
            project_id=projectId, environment_id=environmentId, db=db
        )
        db.delete(
            database.environment.Environment,
            constraints={
                database.environment.Environment.id: environmentId,
            },
        )


@router.get(
    "/projects/{projectId}/environments/{environmentId}",
    status_code=status.HTTP_200_OK,
    responses={
        status.HTTP_200_OK: {
            "model": env_models.Environment,
            "description": "Environment for given project",
        },
    },
    tags=TAGS,
    summary="Returns environment for given project",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.WORKING_ROLES)
async def projects_project_id_environments_environment_id_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    environmentId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> env_models.Environment:
    with db.session():
        environment = env_service.get_environment(
            project_id=projectId, environment_id=environmentId, db=db
        )
        role_details = roles.get_request_user_role_in_project(
            project_id=projectId, request=request, client=db
        )
        return env_models.Environment.from_database_model(
            environment,
            user_role=role_details.role,
            user_id=request.token.user_id if request.token else None,
        )


@router.put(
    "/projects/{projectId}/environments/{environmentId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {"description": "Successfully updated environment"},
    },
    tags=TAGS,
    summary="Update environment of given project",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_environments_environment_id_put(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    environmentId: uuid.UUID,
    updated_environment_payload: env_models.AddEnvironment = fastapi.Body(
        ..., description="Updated environment information"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        env_service.get_environment(
            project_id=projectId, environment_id=environmentId, db=db
        )
        db.update(
            updated_environment_payload.to_database_model(environment_id=environmentId),
            constraints={
                database.environment.Environment.id: environmentId,
            },
        )


@router.get(
    "/projects/{projectId}/environments",
    status_code=status.HTTP_200_OK,
    responses={
        status.HTTP_200_OK: {
            "model": env_models.ProjectsProjectIdEnvironmentsGet200Response,
            "description": "OK",
        },
    },
    tags=TAGS,
    summary="Returns all environments for a given project",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.WORKING_ROLES)
async def projects_project_id_environments_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    startindex: int = _parameters.start_index(),
    pagelength: int = _parameters.page_length(),
    order_by: database.environment.OrderBy = _parameters.order_by(
        default=database.environment.OrderBy.created
    ),
    ascending: bool = _parameters.ascending(),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> env_models.ProjectsProjectIdEnvironmentsGet200Response:
    with db.session():
        result_all: database.client.main.GetAllResponse = db.get_many(
            orm_model_type=database.environment.Environment,
            select_in_loading={
                database.environment.Environment.data_repository: {},
            },
            offset=startindex,
            limit=pagelength,
            order_by=order_by,
            ascending=ascending,
        )

        # Filter for environments with correct projectId
        environments = [
            environment
            for environment in result_all.entities
            if environment.data_repository.project_id == projectId
        ]

        role_details = roles.get_request_user_role_in_project(
            project_id=projectId, request=request, client=db
        )
        return env_models.ProjectsProjectIdEnvironmentsGet200Response(
            total_records=result_all.total_count,
            page_records=len(environments),
            environments=models.base.from_database_models(
                env_models.Environment,
                database_models=environments,
                user_role=role_details.role,
                user_id=request.token.user_id if request.token else None,
            ),
            user_role=role_details.role,
        )


@router.post(
    "/projects/{projectId}/environments",
    status_code=status.HTTP_201_CREATED,
    responses={
        status.HTTP_201_CREATED: {
            "model": env_models.ProjectsProjectIdEnvironmentsPost201Response,
            "description": "Created new environment",
        },
    },
    tags=TAGS,
    summary="Creates a new environment",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_environments_post(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    environment_payload: env_models.AddEnvironment = fastapi.Body(
        ..., description="Environment information"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> env_models.ProjectsProjectIdEnvironmentsPost201Response:
    with db.session():
        # Check that data repository exists
        data_repository = db.get_one(
            orm_model_type=database.data_repository.DataRepository,
            constraints={
                database.data_repository.DataRepository.id: environment_payload.data_repository_id,  # noqa
            },
        )
        # Check that data repository has correct projectId
        database.exceptions.check_uuids_consistency(
            projectId, data_repository.project_id
        )

        new_environment_id = uuid.uuid4()
        db.add(environment_payload.to_database_model(new_environment_id))

    return env_models.ProjectsProjectIdEnvironmentsPost201Response(
        environment_id=new_environment_id
    )
