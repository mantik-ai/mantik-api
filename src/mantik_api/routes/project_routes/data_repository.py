import uuid

import fastapi
import starlette.status as status

import mantik_api.database as database
import mantik_api.models as models
import mantik_api.routes._parameters as _parameters
import mantik_api.routes.dependencies as dependencies
import mantik_api.routes.rbac.authorization as authorization
import mantik_api.routes.rbac.bearer as bearer
import mantik_api.routes.rbac.roles as roles
import mantik_api.utils.git_utils as git_utils

router = fastapi.APIRouter(dependencies=[fastapi.Depends(bearer.OptionalJWTBearer())])


@router.get(
    "/projects/{projectId}/data/is-unlocked",
    responses={
        status.HTTP_200_OK: {
            "description": "Repositories checked successfully",
        },
    },
    tags=["data repository"],
    summary="Returns whether repositories are accessible to user",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.PROJECT_ROLES)
async def projects_project_id_data_is_unlocked_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    dataRepositoryIds: list[uuid.UUID] = fastapi.Query(
        ..., description="IDs of data repositories to check."
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> dict[uuid.UUID, bool]:
    with db.session():
        return git_utils.check_if_repos_are_unlocked(
            db=db,
            project_id=projectId,
            repo_ids=dataRepositoryIds,
            orm=database.data_repository.DataRepository,
            token=request.token,
        )


@router.get(
    "/projects/{projectId}/data/{dataRepositoryId}",
    responses={
        status.HTTP_200_OK: {
            "model": models.data_repository.DataRepository,
            "description": "Data entry for given project",
        },
    },
    tags=["data repository"],
    summary="Returns data entry for given project",
    description="The `connectionId` here refers to a user's git connection. "
    "There can be multiple git connections (from multiple different users) "
    "assigned to the repo. However, only the caller's connection is returned. "
    ""
    "The `dvcConnectionId` is related to an optional remote storage. "
    "Only the caller's dvc connection is returned here, even though "
    "other users may have their own dvc connections assigned.",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.ALL_ROLES)
async def projects_project_id_data_data_repository_id_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    dataRepositoryId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.data_repository.DataRepository:
    with db.session():
        data_repository = db.get_one(
            orm_model_type=database.data_repository.DataRepository,
            constraints={
                database.data_repository.DataRepository.id: dataRepositoryId,
                database.data_repository.DataRepository.project_id: projectId,
            },
            select_in_loading={
                database.data_repository.DataRepository.labels: {},
                database.data_repository.DataRepository.connections: {},
            },
        )
        role_details = roles.get_request_user_role_in_project(
            project_id=projectId, request=request, client=db
        )
        return models.data_repository.DataRepository.from_database_model(
            data_repository,
            user_role=role_details.role,
            user_id=request.token.user_id if request.token else None,
        )


@router.put(
    "/projects/{projectId}/data/{dataRepositoryId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully updated code repository"
        },
        status.HTTP_400_BAD_REQUEST: {
            "model": models.error.HTTPError,
            "description": "Label assignment failed due to invalid scope",
        },
    },
    tags=["data repository"],
    summary="Update data repository",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_data_data_repository_id_put(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    dataRepositoryId: uuid.UUID,
    updated_data_repository_payload: models.data_repository.UpdateDataRepository = fastapi.Body(  # noqa: E501
        ..., description="Updated data repository information"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        try:
            models.label.validate_labels_scope(
                updated_data_repository_payload.labels,
                expected_scope=database.label.Scope.Data,
                client=db,
            )
        except ValueError as e:
            raise fastapi.HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail=str(e)
            )
        data_repository = db.get_one(
            orm_model_type=database.data_repository.DataRepository,
            constraints={
                database.data_repository.DataRepository.id: dataRepositoryId,
                database.data_repository.DataRepository.project_id: projectId,
            },
        )
        db.overwrite(
            data_repository,
            fields_to_overwrite=updated_data_repository_payload.to_overwrite_kwargs(
                client=db,
                exclude_unset=False,
            ),
        )


@router.delete(
    "/projects/{projectId}/data/{dataRepositoryId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully deleted Code Repository"
        },
    },
    tags=["data repository"],
    summary="Delete Data Repository",
    description=(
        "Deletion of a Data Repository is prevented when there is still at least one "
        "of the following objects referencing the respective Data Repository:"
        ""
        "- Run"
        ""
        "Each respective instance has to be manually deleted."
    ),
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_data_data_repository_id_delete(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    dataRepositoryId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        db.delete(
            database.data_repository.DataRepository,
            constraints={
                database.data_repository.DataRepository.id: dataRepositoryId,
                database.data_repository.DataRepository.project_id: projectId,
            },
        )


@router.get(
    "/projects/{projectId}/data",
    responses={
        status.HTTP_200_OK: {
            "model": models.data_repository.ProjectsProjectIdDataGet200Response,
            "description": "Data entries for given project",
        },
    },
    tags=["data repository"],
    summary="Returns data entries for given project",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.ALL_ROLES)
async def projects_project_id_data_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    startindex: int = _parameters.start_index(),
    pagelength: int = _parameters.page_length(),
    order_by: database.data_repository.OrderBy = _parameters.order_by(
        default=database.data_repository.OrderBy.created
    ),
    ascending: bool = _parameters.ascending(),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.data_repository.ProjectsProjectIdDataGet200Response:
    with db.session():
        result: database.client.main.GetAllResponse = db.get_many(
            orm_model_type=database.data_repository.DataRepository,
            constraints={
                database.data_repository.DataRepository.project_id: projectId,
            },
            select_in_loading={database.data_repository.DataRepository.labels: {}},
            offset=startindex,
            limit=pagelength,
            order_by=order_by,
            ascending=ascending,
        )
        role_details = roles.get_request_user_role_in_project(
            project_id=projectId, request=request, client=db
        )
        return models.data_repository.ProjectsProjectIdDataGet200Response(
            total_records=result.total_count,
            page_records=result.page_count,
            data_repositories=models.base.from_database_models(
                models.data_repository.DataRepository,
                database_models=result.entities,
                user_role=role_details.role,
                user_id=request.token.user_id if request.token else None,
            ),
            user_role=role_details.role,
        )


@router.post(
    "/projects/{projectId}/data",
    status_code=status.HTTP_201_CREATED,
    responses={
        status.HTTP_201_CREATED: {
            "model": models.data_repository.ProjectsProjectIdDataGet201Response,
            "description": "Successfully added data repository",
        },
        status.HTTP_400_BAD_REQUEST: {
            "model": models.error.HTTPError,
            "description": "Label assignment failed due to invalid scope",
        },
    },
    tags=["data repository"],
    summary="Add data repository",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_data_post(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    new_data_repository: models.data_repository.AddDataRepository = fastapi.Body(
        ..., description="Added data repository information"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.data_repository.ProjectsProjectIdDataGet201Response:
    new_data_repository_id = uuid.uuid4()

    with db.session():
        try:
            models.label.validate_labels_scope(
                new_data_repository.labels, database.label.Scope.Data, db
            )
        except ValueError as e:
            raise fastapi.HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail=str(e)
            )
        db.add(
            new_data_repository.to_database_model(
                project_id=projectId,
                data_repository_id=new_data_repository_id,
                client=db,
            )
        )
        if new_data_repository.connection_id:
            connection = db.get_one(
                orm_model_type=database.connection.Connection,
                constraints={
                    database.connection.Connection.id: new_data_repository.connection_id,  # noqa 501
                    database.connection.Connection.user_id: request.token.user_id,
                },
            )
            git_utils.verify_platform_matches(
                repository_platform=new_data_repository.platform,
                connection_platform=connection.connection_provider,
            )
            db.commit()
            db.add(
                database.data_repository.DataRepositoryAndConnectionAssociationTable(
                    id=uuid.uuid4(),
                    data_repository_id=new_data_repository_id,
                    connection_id=new_data_repository.connection_id,
                )
            )

    return models.data_repository.ProjectsProjectIdDataGet201Response(
        data_repository_id=new_data_repository_id
    )


@router.post(
    "/projects/{projectId}/data/{dataRepositoryId}/connection",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully added connection",
        },
    },
    tags=["data repository"],
    summary="Add dvc or git connection to the repo.",
    description="Add a *git* or *dvc* connection to the repository. The type of the "
    "connection is derived automatically from the connection object (linked by "
    "`connectionId`). You can only add 1 dvc and 1 git connection per data repo, "
    "per user.",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_data_data_repository_id_connection(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    dataRepositoryId: uuid.UUID,
    repo_connector: models.connection.ConnectionId = fastapi.Body(
        ..., description="Added a new connection to repository"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    with db.session():
        data_repository = db.get_one(
            orm_model_type=database.data_repository.DataRepository,
            constraints={
                database.data_repository.DataRepository.id: dataRepositoryId,
                database.data_repository.DataRepository.project_id: projectId,
            },
            select_in_loading={
                database.data_repository.DataRepository.connections: {},
            },
        )

        connection = db.get_one(
            orm_model_type=database.connection.Connection,
            constraints={
                database.connection.Connection.id: repo_connector.connection_id,
                database.connection.Connection.user_id: request.token.user_id,
            },
            select_in_loading={
                database.connection.Connection.data_repositories: {},
            },
        )

        if (
            connection.is_dvc_connection
            and data_repository.my_dvc_connection_id(request.token.user_id) is not None
        ):
            raise fastapi.HTTPException(
                422, detail="Data repository already has a DVC connection!"
            )

        if (
            connection.is_git_connection
            and data_repository.my_git_connection_id(request.token.user_id) is not None
        ):
            raise fastapi.HTTPException(
                422, detail="Data repository already has a git connection."
            )
        if not connection.is_dvc_connection:
            git_utils.verify_platform_matches(
                repository_platform=data_repository.platform,
                connection_platform=connection.connection_provider,
            )
        db.add(
            database.data_repository.DataRepositoryAndConnectionAssociationTable(
                id=uuid.uuid4(),
                data_repository_id=dataRepositoryId,
                connection_id=repo_connector.connection_id,
            )
        )


@router.delete(
    "/projects/{projectId}/data/{dataRepositoryId}/connection/{connectionId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully removed a connection from data repository"
        },
    },
    tags=["data repository"],
    summary="Remove a connection from a data repository",
    description="Remove a *dvc* or *git* connection from a data repository. You can "
    "only remove your connections. Other's connections are not accessible to "
    "the caller.",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_data_data_repository_id_connection_id_delete(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    dataRepositoryId: uuid.UUID,
    connectionId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        # ensure user owns the connection
        db.get_one(
            orm_model_type=database.connection.Connection,
            constraints={
                database.connection.Connection.id: connectionId,
                database.connection.Connection.user_id: request.token.user_id,
            },
        )

        data_repo_connection = db.get_one(
            orm_model_type=database.data_repository.DataRepositoryAndConnectionAssociationTable,  # noqa E501
            constraints={
                database.data_repository.DataRepositoryAndConnectionAssociationTable.connection_id: connectionId,  # noqa E501
                database.data_repository.DataRepositoryAndConnectionAssociationTable.data_repository_id: dataRepositoryId,  # noqa E501
            },
        )
        db.delete(data_repo_connection)
