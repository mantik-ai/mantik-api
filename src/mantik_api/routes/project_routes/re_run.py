import logging
import uuid

import fastapi
import starlette.status as status

import mantik_api.database as database
import mantik_api.models as models
import mantik_api.routes.dependencies as dependencies
import mantik_api.routes.project_routes._compute_backend as _compute_backend
import mantik_api.routes.project_routes.runs as runs
import mantik_api.routes.rbac.authorization as authorization
import mantik_api.routes.rbac.bearer as bearer
import mantik_api.service as service

logger = logging.getLogger(__name__)

router = fastapi.APIRouter(dependencies=[fastapi.Depends(bearer.OptionalJWTBearer())])


@router.post(
    "/projects/{projectId}/runs/{runId}/reruns",
    status_code=status.HTTP_201_CREATED,
    responses={
        status.HTTP_201_CREATED: {
            "model": models.run.ProjectsProjectIdRunsGet201Response,
            "description": "Re-submitted saved run",
        },
    },
    tags=["runs"],
    summary="Re-submit a saved run and saves it.",
    deprecated=True,
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_runs_run_id_reruns_post(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    runId: uuid.UUID,
    re_run: models.re_run.ReRun = fastapi.Body(..., description="Re-run information"),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.run.ProjectsProjectIdRunsGet201Response:
    with db.session():
        caller_user_id = request.token.user_id
        logger.debug(
            "Attempting to rerun for user %s and project %s and run %s",
            caller_user_id,
            projectId,
            runId,
        )
        db_run = db.get_one(
            orm_model_type=database.run.Run,
            constraints={
                database.run.Run.id: runId,
                database.run.Run.project_id: projectId,
                database.run.Run.user_id: caller_user_id,
            },
        )
        old_run = models.run.Run.from_database_model(
            # Role of the user in the old run here not relevant
            # since the old run is only use to construct the new run.
            db_run,
            user_role=database.role_details.ProjectRole.NO_ROLE,
            user_id=request.token.user_id if request.token else None,
        )

        if re_run.connection_id is None and old_run.connection is None:
            raise fastapi.HTTPException(
                status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                detail=(
                    "Triggering a re-run requires a connection: "
                    "no connection given and the run to re-execute "
                    "doesn't define a connection"
                ),
            )

        new_connection_id = determine_validate_new_connection_id(
            re_run=re_run, db=db, caller_user_id=caller_user_id, old_run=old_run
        )

        new_compute_budget_account = (
            re_run.compute_budget_account or old_run.compute_budget_account
        )

        new_add_run = _create_add_run_from_run(
            run=old_run,
            connection_id=new_connection_id,
            compute_budget_account=new_compute_budget_account,
        )
        runs.validate_reference_bitbucket_repo(db, new_add_run)

        logger.debug("Creating re-run for project %s: %s", projectId, new_add_run)

        run = service.runs.trigger_run(
            db=db,
            add_run=new_add_run,
            token=request.token,
            project_id=projectId,
            caller_user_id=caller_user_id,
        )

        # Close session to persist re-run.

    # Don't raise if status fetch fails: it's not essential for submission!
    with db.session(raise_exception=False):
        _compute_backend.get_and_write_run_status_and_info(
            request=request, run=run, db=db, raise_exception=False
        )

    return models.run.ProjectsProjectIdRunsGet201Response(run_id=run.id)


def _create_add_run_from_run(
    run: models.run.Run, connection_id: uuid.UUID, compute_budget_account: str
) -> models.run.AddRun:
    data_repository_id = (
        run.data_repository.data_repository_id
        if run.data_repository is not None
        else None
    )
    return models.run.AddRun(
        name=run.name,
        experiment_repository_id=run.experiment_repository.experiment_repository_id,
        code_repository_id=run.model_repository.code_repository.code_repository_id,
        branch=run.model_repository.branch,
        commit=run.model_repository.commit,
        data_repository_id=data_repository_id,
        connection_id=connection_id,
        compute_budget_account=compute_budget_account,
        mlflow_mlproject_file_path=run.mlflow_mlproject_file_path,
        entry_point=run.entry_point,
        mlflow_parameters=run.mlflow_parameters,
        backend_config=run.backend_config,
    )


def determine_validate_new_connection_id(
    re_run: models.re_run.ReRun,
    db: database.client.main.Client,
    caller_user_id: uuid.UUID,
    old_run: models.run.Run,
) -> uuid.UUID:
    (
        new_connection_id,
        connection_owner_id,
    ) = _get_new_connection_and_connection_owner_id(
        re_run=re_run,
        old_run=old_run,
        db=db,
    )
    logger.debug(
        "User ID is %s and connection owner is %s",
        caller_user_id,
        connection_owner_id,
    )
    if caller_user_id != connection_owner_id:
        raise fastapi.HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="User triggering a re-run must be owner of the connection",
        )

    return new_connection_id


def _get_new_connection_and_connection_owner_id(
    re_run: models.re_run.ReRun,
    db: database.client.main.Client,
    old_run: models.run.Run,
) -> tuple[uuid.UUID, uuid.UUID]:
    if re_run.connection_id is not None:
        new_connection_id = re_run.connection_id
        connection_owner_id = _get_connection_owner_id(new_connection_id, db=db)
        logger.debug(
            "Re-run connection id is None: setting new connection ID to %s",
            new_connection_id,
        )
    else:
        new_connection_id = old_run.connection.connection_id
        connection_owner_id = old_run.connection.user.user_id
    return new_connection_id, connection_owner_id


def _get_connection_owner_id(
    connection_id: uuid.UUID, db: database.client.main.Client
) -> uuid.UUID:
    db_connection = db.get_one(
        orm_model_type=database.connection.Connection,
        constraints={database.connection.Connection.id: connection_id},
    )
    connection = models.connection.Connection.from_database_model(db_connection)
    return connection.user.user_id
