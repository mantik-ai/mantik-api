import os
import uuid

import fastapi
import starlette.status as status

import mantik_api.database as database
import mantik_api.models as models
import mantik_api.routes._parameters as _parameters
import mantik_api.routes.dependencies as dependencies
import mantik_api.routes.rbac.authorization as authorization
import mantik_api.routes.rbac.bearer as bearer
import mantik_api.routes.rbac.roles as roles
import mantik_api.service.model_repository
import mantik_api.utils.mlflow.runs
import mantik_api.utils.s3 as s3


router = fastapi.APIRouter(dependencies=[fastapi.Depends(bearer.OptionalJWTBearer())])
MODEL_ENDPOINTS_TAG = "models"
ONE_HOUR_IN_SECONDS = 3600
CODE_BUILD_ENV_NAME = "MODEL_CONTAINER_SERVICE_CODEBUILD_PROJECT_NAME"
BUCKET_NAME_ENV_NAME = "MODEL_CONTAINER_STORE_S3_BUCKET_NAME"


@router.get(
    "/projects/{projectId}/models",
    responses={
        status.HTTP_200_OK: {
            "model": models.model_repository.ProjectsProjectIdModelsGet200Response,
            "description": "Model entries for given project",
        },
    },
    tags=[MODEL_ENDPOINTS_TAG],
    summary="Returns model entries for given project",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.ALL_ROLES)
async def projects_project_id_models_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    startindex: int = _parameters.start_index(),
    pagelength: int = _parameters.page_length(),
    order_by: database.model_repository.OrderBy = _parameters.order_by(
        default=database.model_repository.OrderBy.created
    ),
    ascending: bool = _parameters.ascending(),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.model_repository.ProjectsProjectIdModelsGet200Response:
    with db.session():
        result: database.client.main.GetAllResponse = db.get_many(
            orm_model_type=database.model_repository.ModelRepository,
            constraints={
                database.model_repository.ModelRepository.project_id: projectId,
            },
            select_in_loading={
                database.model_repository.ModelRepository.labels: {},
                database.model_repository.ModelRepository.code_repository: {
                    database.code_repository.CodeRepository.labels: {}
                },
            },
            offset=startindex,
            limit=pagelength,
            order_by=order_by,
            ascending=ascending,
        )
        role_details = roles.get_request_user_role_in_project(
            project_id=projectId, request=request, client=db
        )
        return models.model_repository.ProjectsProjectIdModelsGet200Response(
            total_records=result.total_count,
            page_records=result.page_count,
            model_repositories=models.base.from_database_models(
                models.model_repository.ModelRepository,
                database_models=result.entities,
                user_role=role_details.role,
            ),
            user_role=role_details.role,
        )


@router.get(
    "/projects/{projectId}/models/trained",
    responses={
        status.HTTP_200_OK: {
            "model": models.trained_model.GetTrainedModelsResponse,
            "description": "Trained models",
        },
    },
    tags=[MODEL_ENDPOINTS_TAG],
    summary="Returns all trained models",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.ALL_ROLES)
async def get_all_trained_models_of_project(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    startindex: int = _parameters.start_index(),
    pagelength: int = _parameters.page_length(),
    order_by: database.trained_model.OrderBy = _parameters.order_by(
        default=database.trained_model.OrderBy.created
    ),
    ascending: bool = _parameters.ascending(),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.trained_model.GetTrainedModelsResponse:
    with db.session():
        result: database.client.main.GetAllResponse = db.get_many(
            orm_model_type=database.trained_model.TrainedModel,
            constraints={
                database.trained_model.TrainedModel.project_id: projectId,
            },
            select_in_loading={
                database.trained_model.TrainedModel.run: {},
            },
            offset=startindex,
            limit=pagelength,
            order_by=order_by,
            ascending=ascending,
        )
        role_details = roles.get_request_user_role_in_project(
            project_id=projectId, request=request, client=db
        )
        return models.trained_model.GetTrainedModelsResponse(
            total_records=result.total_count,
            page_records=result.page_count,
            models=models.base.from_database_models(
                models.trained_model.TrainedModel,
                database_models=result.entities,
                user_role=role_details.role,
            ),
            user_role=role_details.role,
        )


@router.get(
    "/projects/{projectId}/models/{modelRepositoryId}",
    responses={
        status.HTTP_200_OK: {
            "model": models.model_repository.ModelRepository,
            "description": "Model entry for given ID",
        },
    },
    tags=[MODEL_ENDPOINTS_TAG],
    summary="Returns model entry for given ID",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.ALL_ROLES)
async def projects_project_id_models_model_repository_id_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    modelRepositoryId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.model_repository.ModelRepository:
    with db.session():
        model_repository = db.get_one(
            orm_model_type=database.model_repository.ModelRepository,
            constraints={
                database.model_repository.ModelRepository.id: modelRepositoryId,
                database.model_repository.ModelRepository.project_id: projectId,
            },
            select_in_loading={
                database.model_repository.ModelRepository.labels: {},
                database.model_repository.ModelRepository.code_repository: {
                    database.code_repository.CodeRepository.labels: {}
                },
            },
        )
        role_details = roles.get_request_user_role_in_project(
            project_id=projectId, request=request, client=db
        )
        return models.model_repository.ModelRepository.from_database_model(
            model_repository, user_role=role_details.role
        )


@router.put(
    "/projects/{projectId}/models/trained/{modelId}",
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully updated trained model"
        },
    },
    tags=[MODEL_ENDPOINTS_TAG],
    summary="Update trained model",
    status_code=status.HTTP_204_NO_CONTENT,
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def update_trained_model(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    modelId: uuid.UUID,
    updated_model_payload: models.trained_model.UpdateTrainedModel = fastapi.Body(  # noqa: E501
        ..., description="Updated trained model information"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        updated_model = updated_model_payload.to_database_model(
            project_id=projectId, model_id=modelId, exclude_unset=True
        )
        db.update(
            updated_model,
            constraints={
                database.trained_model.TrainedModel.id: modelId,
                database.trained_model.TrainedModel.project_id: projectId,
            },
            fields_to_update=updated_model_payload.dict(
                convert_keys_to_camel_case=False, exclude_unset=True
            ),
        )


@router.put(
    "/projects/{projectId}/models/trained/{modelId}/status",
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully updated trained model build status"
        },
        status.HTTP_404_NOT_FOUND: {
            "model": models.error.HTTPError,
            "description": "Unable to find the model",
        },
    },
    tags=[MODEL_ENDPOINTS_TAG],
    summary="Update trained model container build status",
    status_code=status.HTTP_204_NO_CONTENT,
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def update_trained_model_build_status(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    modelId: uuid.UUID,
    status: database.trained_model.ContainerBuildStatus = fastapi.Body(  # noqa: E501
        ...
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        trained_model: database.trained_model.TrainedModel = db.get_one(
            database.trained_model.TrainedModel,
            constraints={
                database.trained_model.TrainedModel.id: modelId,
                database.trained_model.TrainedModel.project_id: projectId,
            },
        )

        db.overwrite(trained_model, {"status": status})


@router.put(
    "/projects/{projectId}/models/{modelRepositoryId}",
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully updated model repository"
        },
    },
    tags=[MODEL_ENDPOINTS_TAG],
    summary="Update model repository",
    status_code=status.HTTP_204_NO_CONTENT,
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_models_model_repository_id_put(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    modelRepositoryId: uuid.UUID,
    updated_model_repository_payload: models.model_repository.AddModelRepository = fastapi.Body(  # noqa: E501
        ..., description="Updated model repository information"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    model = updated_model_repository_payload.to_database_model(
        project_id=projectId, model_repository_id=modelRepositoryId, exclude_unset=True
    )
    with db.session():
        db.update(
            model,
            constraints={
                database.model_repository.ModelRepository.id: modelRepositoryId,
                database.model_repository.ModelRepository.project_id: projectId,
            },
        )


@router.delete(
    "/projects/{projectId}/models/trained/{modelId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully deleted trained model"
        },
    },
    tags=[MODEL_ENDPOINTS_TAG],
    summary="Delete a trained model",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def delete_trained_model(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    modelId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    s3.delete_key(
        bucket_name=os.environ[BUCKET_NAME_ENV_NAME],
        key=f"{projectId}/{modelId}-docker.tar.gz",
    )
    with db.session():
        db.delete(
            database.trained_model.TrainedModel,
            constraints={
                database.trained_model.TrainedModel.id: modelId,
                database.trained_model.TrainedModel.project_id: projectId,
            },
        )


@router.delete(
    "/projects/{projectId}/models/{modelRepositoryId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully deleted Model Repository"
        },
    },
    tags=[MODEL_ENDPOINTS_TAG],
    summary="Delete model repository",
    description=(
        "Deleting a Model Repository from the project is not permitted if there"
        "are still Runs references the respective Model Repository."
        "Each respective Run has to be manually deleted."
    ),
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_models_model_repository_id_delete(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    modelRepositoryId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        db.delete(
            database.model_repository.ModelRepository,
            constraints={
                database.model_repository.ModelRepository.id: modelRepositoryId,
                database.model_repository.ModelRepository.project_id: projectId,
            },
        )


@router.post(
    "/projects/{projectId}/models",
    status_code=status.HTTP_201_CREATED,
    responses={
        status.HTTP_201_CREATED: {
            "model": models.model_repository.ProjectsProjectIdModelsGet201Response,
            "description": "Successfully added model repository",
        },
    },
    tags=[MODEL_ENDPOINTS_TAG],
    summary="Add model repository",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_models_post(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    new_model_repository: models.model_repository.AddModelRepository = fastapi.Body(
        ..., description="Added model repository information"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.model_repository.ProjectsProjectIdModelsGet201Response:
    new_model_repository_id = uuid.uuid4()
    with db.session():
        db.add(
            new_model_repository.to_database_model(
                project_id=projectId, model_repository_id=new_model_repository_id
            )
        )
    return models.model_repository.ProjectsProjectIdModelsGet201Response(
        model_repository_id=new_model_repository_id
    )


@router.post(
    "/projects/{projectId}/models/trained",
    status_code=status.HTTP_201_CREATED,
    responses={
        status.HTTP_201_CREATED: {
            "model": models.trained_model.CreatedTrainedModelResponse,
            "description": "Successfully added trained model",
        },
    },
    tags=[MODEL_ENDPOINTS_TAG],
    summary="Add trained model",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def create_new_trained_model_entry(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    new_trained_model: models.trained_model.AddTrainedModel = fastapi.Body(
        ..., description="Added trained model information"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.trained_model.CreatedTrainedModelResponse:
    new_trained_model_id = uuid.uuid4()

    if new_trained_model.run_id is not None:
        with db.session():
            run = db.get_one(
                orm_model_type=database.run.Run,
                constraints={
                    database.run.Run.id: new_trained_model.run_id,
                    database.run.Run.project_id: projectId,
                },
            )

            if run.status != database.run.RunStatus.FINISHED:
                raise fastapi.HTTPException(
                    status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                    detail=f"Run status needs to be {database.run.RunStatus.FINISHED}. "
                    f"Currently is {run.status}",
                )

            new_trained_model.location = "Artifact Store"
            new_trained_model.uri = mantik_api.utils.mlflow.runs.get_model_uri(
                mlflow_run_id=run.mlflow_run_id, token=request.token
            )

    with db.session():
        db.add(
            new_trained_model.to_database_model(
                project_id=projectId, model_id=new_trained_model_id
            )
        )
    return models.trained_model.CreatedTrainedModelResponse(
        model_id=new_trained_model_id
    )


@router.get(
    "/projects/{projectId}/models/trained/{modelId}",
    responses={
        status.HTTP_200_OK: {
            "model": models.trained_model.TrainedModel,
            "description": "Trained Model entry for given ID",
        },
    },
    tags=[MODEL_ENDPOINTS_TAG],
    summary="Returns trained model entry for given ID",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.ALL_ROLES)
async def get_trained_model(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    modelId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.trained_model.TrainedModel:
    with db.session():
        trained_model = db.get_one(
            orm_model_type=database.trained_model.TrainedModel,
            constraints={
                database.trained_model.TrainedModel.id: modelId,
                database.trained_model.TrainedModel.project_id: projectId,  # noqa
            },
            select_in_loading={
                database.trained_model.TrainedModel.run: {},
            },
        )
        role_details = roles.get_request_user_role_in_project(
            project_id=projectId, request=request, client=db
        )
        return models.trained_model.TrainedModel.from_database_model(
            trained_model, user_role=role_details.role
        )


@router.post(
    "/projects/{projectId}/models/trained/{modelId}/docker/build",
    status_code=status.HTTP_201_CREATED,
    responses={
        status.HTTP_201_CREATED: {
            "description": "Successfully sent model for building",
        },
    },
    tags=[MODEL_ENDPOINTS_TAG],
    summary="Build docker container for the trained model.",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def build_container_from_trained_model(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    modelId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    with db.session():
        trained_model = db.get_one(
            orm_model_type=database.trained_model.TrainedModel,
            constraints={
                database.trained_model.TrainedModel.id: modelId,
                database.trained_model.TrainedModel.project_id: projectId,
            },
        )
        db.overwrite(
            trained_model,
            {"status": database.trained_model.ContainerBuildStatus.PENDING},
        )

    token = request.token.to_string()

    mantik_api.service.model_repository.containerize_trained_model(
        model_id=modelId,
        project_id=projectId,
        token=token,
        trained_model_uri=trained_model.uri,
        code_build_project_name=os.environ[CODE_BUILD_ENV_NAME],
    )


@router.get(
    "/projects/{projectId}/models/trained/{modelId}/docker",
    status_code=status.HTTP_200_OK,
    responses={
        status.HTTP_200_OK: {
            "model": models.presigned_url.PresignedUrl,
            "description": "Download URL for a containerized model.",
        },
    },
    tags=[MODEL_ENDPOINTS_TAG],
    summary="Return the download url for containerized model.",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.PROJECT_ROLES)
async def create_and_return_model_container_presigned_url(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    modelId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    with db.session():
        db.get_one(
            orm_model_type=database.trained_model.TrainedModel,
            constraints={
                database.trained_model.TrainedModel.id: modelId,
                database.trained_model.TrainedModel.project_id: projectId,
            },
        )

    container_name = f"{projectId}/{modelId}-docker.tar.gz"

    url = s3.generate_pre_signed_url(
        bucket_name=os.environ[BUCKET_NAME_ENV_NAME],
        key=container_name,
        expires_in_seconds=ONE_HOUR_IN_SECONDS,
    )

    return models.presigned_url.PresignedUrl(url=url)
