import enum
import uuid

import fastapi
import starlette.status as status

import mantik_api.database as database
import mantik_api.database.exceptions as exceptions
import mantik_api.models as models
import mantik_api.routes._parameters as _parameters
import mantik_api.routes.dependencies as dependencies
import mantik_api.routes.rbac._project as _project
import mantik_api.routes.rbac.authorization as authorization
import mantik_api.routes.rbac.bearer as bearer
import mantik_api.routes.rbac.roles as roles

router = fastapi.APIRouter(dependencies=[fastapi.Depends(bearer.OptionalJWTBearer())])


class OrderMembersBy(enum.StrEnum):
    role = enum.auto()
    added = enum.auto()
    updated = enum.auto()

    @property
    def attribute(self) -> str:
        match self:
            case OrderMembersBy.role:
                return "role"
            case OrderMembersBy.added:
                return "added_at"
            case OrderMembersBy.updated:
                return "updated_at"
            case _:
                raise NotImplementedError(
                    f"Order members by {self} has to associated attribute of "
                    f"{models.project_member.ProjectMember}"
                )


@router.get(
    "/projects/{projectId}/members",
    responses={
        status.HTTP_200_OK: {
            "model": models.project_member.ProjectsProjectIdMembersGet200Response,
            "description": "Member entries for given project",
        },
    },
    tags=["project collaboration"],
    summary="Returns member entries for given project",
    description="Returns member entries for given project, with their derived "
    "user roles. The role is the user's highest role in the "
    "project, derived through direct membership, group membership, "
    "and organization membership.",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.PROJECT_ROLES)
async def projects_project_id_members_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    startindex: int = _parameters.start_index(),
    pagelength: int = _parameters.page_length(),
    order_by: OrderMembersBy = fastapi.Query(
        OrderMembersBy.added,
        description="Property to use for ordering the results",
    ),
    ascending: bool = _parameters.ascending(),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.project_member.ProjectsProjectIdMembersGet200Response:
    with db.session():
        project_with_roles = _project.get_project_with_roles(
            project_id=projectId,
            client=db,
        )

        result = [
            models.project_member.ProjectMember.from_database_model(
                project_id=projectId,
                user=member,
                role_details=_project.get_user_role_in_project(
                    project=project_with_roles, user_id=member.id, client=db
                ),
            )
            for member in project_with_roles.all_members
        ]
        ordered = list(
            sorted(
                result,
                key=lambda x: getattr(x, order_by.attribute),
                reverse=not ascending,
            )
        )

        # paginate response
        entities_on_page = ordered[
            (startindex * pagelength) : ((startindex + 1) * pagelength)  # noqa
        ]

        return models.project_member.ProjectsProjectIdMembersGet200Response(
            total_records=len(result),
            page_records=len(entities_on_page),
            members=entities_on_page,
            user_role=roles.get_request_user_role_in_project(
                project_id=projectId, request=request, client=db
            ).role,
        )


@router.get(
    "/projects/{projectId}/members/{userId}",
    responses={
        status.HTTP_200_OK: {
            "model": models.project_member.ProjectMember,
            "description": "Project role for given user in a project",
        },
    },
    tags=["project collaboration"],
    summary="Returns project role for given user in a project",
    description="Returns the user's highest role in the project, "
    "derived through direct membership, group "
    "membership, and organization membership.",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.PROJECT_ROLES)
async def projects_project_id_members_user_id_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    userId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.project_member.ProjectMember:
    with db.session():
        user = db.get_one(
            database.user.User,
            constraints={database.user.User.id: userId},
        )

        project_with_roles = _project.get_project_with_roles(
            project_id=projectId, client=db
        )
        role_details = _project.get_user_role_in_project(
            project=project_with_roles, user_id=userId, client=db
        )
        if role_details.role is database.role_details.ProjectRole.NO_ROLE:
            raise database.exceptions.NotFoundError("User has no roles in the project")

        return models.project_member.ProjectMember.from_database_model(
            project_id=projectId,
            user=user,
            role_details=role_details,
        )


@router.put(
    "/projects/{projectId}/members/{userId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully updated project role"
        },
    },
    tags=["project collaboration"],
    summary="Update project role for given user in a project",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.OWNER_ROLE
)
async def projects_project_id_members_user_id_put(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    userId: uuid.UUID,
    project_role: models.project_role.ProjectRole = fastapi.Body(
        ..., description="Update project role"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        # This checks that the user is already a member of the project.
        db.get_one(
            orm_model_type=database.project.UserProjectAssociationTable,
            constraints={
                database.project.UserProjectAssociationTable.user_id: userId,
                database.project.UserProjectAssociationTable.project_id: projectId,  # noqa: E501
            },
        )
        db.update(
            project_role.to_user_project_role_model(
                user_id=userId, project_id=projectId
            ),
            constraints={
                database.project.UserProjectAssociationTable.user_id: userId,
                database.project.UserProjectAssociationTable.project_id: projectId,  # noqa: E501
            },
        )


@router.delete(
    "/projects/{projectId}/members/{userId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {"description": "Successfully removed member"},
        status.HTTP_422_UNPROCESSABLE_ENTITY: {
            "model": models.error.HTTPError,
            "description": "Unable to remove member",
        },
    },
    tags=["project collaboration"],
    summary="Remove member from project",
    description=(
        "Removing a member from the project will remove their access rights to "
        "the project."
        ""
        "This doesn't apply if:"
        ""
        "- the members is added implicitly as a group member"
        "- the members is added implicitly as an organization member"
    ),
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.OWNER_ROLE
)
async def projects_project_id_members_user_id_delete(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    userId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        try:
            db.get_one(
                orm_model_type=database.project.UserProjectAssociationTable,
                constraints={
                    database.project.UserProjectAssociationTable.user_id: userId,
                    database.project.UserProjectAssociationTable.project_id: projectId,
                },
            )
        except exceptions.NotFoundError:
            project = db.get_one(
                orm_model_type=database.project.Project,
                constraints={
                    database.project.Project.id: projectId,
                },
            )
            if project.owner_id == userId:
                raise fastapi.HTTPException(
                    status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                    detail="User owner can not be deleted from project",
                )
            else:
                raise fastapi.HTTPException(
                    status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                    detail="Members is added implicitly as "
                    "a group/organization member, "
                    "either remove it from this group/organization, or "
                    "remove the entire group/organization from the project",
                )
        db.delete(
            database.project.UserProjectAssociationTable,
            constraints={
                database.project.UserProjectAssociationTable.user_id: userId,
                database.project.UserProjectAssociationTable.project_id: projectId,  # noqa: E501
            },
        )
