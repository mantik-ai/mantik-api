import logging
import pathlib
import urllib.parse
import uuid

import fastapi
import starlette.status as status

import mantik_api.checks as checks
import mantik_api.compute_backend as compute_backend
import mantik_api.database as database
import mantik_api.models as models
import mantik_api.routes._parameters as _parameters
import mantik_api.routes.dependencies as dependencies
import mantik_api.routes.project_routes._compute_backend as _compute_backend
import mantik_api.routes.rbac.authorization as authorization
import mantik_api.routes.rbac.bearer as bearer
import mantik_api.routes.rbac.roles as roles
import mantik_api.service as service
import mantik_api.utils.mlflow as mlflow

logger = logging.getLogger(__name__)

router = fastapi.APIRouter(dependencies=[fastapi.Depends(bearer.OptionalJWTBearer())])

RUNS_TAG = "runs"
RUN_CONFIGURATION_TAG = "run configurations"


@router.get(
    "/projects/{projectId}/runs",
    responses={
        status.HTTP_200_OK: {
            "model": models.run.ProjectsProjectIdRunsGet200Response,
            "description": "OK",
        },
    },
    tags=[RUNS_TAG],
    summary="Returns all runs for a given project",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.ALL_ROLES)
async def projects_project_id_runs_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    startindex: int = _parameters.start_index(),
    pagelength: int = _parameters.page_length(),
    order_by: database.run.OrderRunBy = _parameters.order_by(
        default=database.run.OrderRunBy.created
    ),
    ascending: bool = _parameters.ascending(),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.run.ProjectsProjectIdRunsGet200Response:
    with db.session():
        result: database.client.main.GetAllResponse = db.get_many(
            orm_model_type=database.run.Run,
            constraints={
                database.run.Run.project_id: projectId,
            },
            select_in_loading={
                database.run.Run.experiment_repository: {
                    database.experiment_repository.ExperimentRepository.labels: {}
                },
                database.run.Run.saved_model_repository: {},
                database.run.Run.model_repository: {
                    database.model_repository.ModelRepository.labels: {},
                    database.model_repository.ModelRepository.code_repository: {
                        database.code_repository.CodeRepository.labels: {}
                    },
                },
                database.run.Run.data_repository: {
                    database.data_repository.DataRepository.labels: {}
                },
                database.run.Run.connection: {},
            },
            offset=startindex,
            limit=pagelength,
            order_by=order_by,
            ascending=ascending,
        )
        role_details = roles.get_request_user_role_in_project(
            project_id=projectId, request=request, client=db
        )
        return models.run.ProjectsProjectIdRunsGet200Response(
            total_records=result.total_count,
            page_records=result.page_count,
            runs=models.base.from_database_models(
                models.run.Run,
                database_models=[
                    service.runs.fix_endless_running_jobs(
                        run, token=request.token, client=db
                    )
                    for run in result.entities
                ],
                user_role=role_details.role,
                user_id=request.token.user_id if request.token else None,
            ),
            user_role=role_details.role,
        )


@router.post(
    "/projects/{projectId}/runs",
    status_code=status.HTTP_201_CREATED,
    responses={
        status.HTTP_201_CREATED: {
            "model": models.run.ProjectsProjectIdRunsGet201Response,
            "description": "Created new run",
        },
    },
    tags=[RUNS_TAG],
    summary="Creates and optionally submits a new run",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_runs_post(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    submit: bool = fastapi.Query(
        True, description="Whether to submit the run to the compute backend"
    ),
    add_run: models.run.AddRun = fastapi.Body(..., description="Run information"),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.run.ProjectsProjectIdRunsGet201Response:
    """

    Add and optionally start a run.

    **Note:**
    If a run with the same name is present in the same experiment,
    a numerical suffix is going to be appended to the name.

    """
    caller_user_id = request.token.user_id
    logger.debug("Creating run for project %s: %s", projectId, add_run)

    logger.debug(
        "Fetching experiment repository with ID %s",
        add_run.experiment_repository_id,
    )
    if submit:
        with db.session():
            validate_reference_bitbucket_repo(db, add_run)
            run = service.runs.trigger_run(
                db=db,
                add_run=add_run,
                token=request.token,
                project_id=projectId,
                caller_user_id=caller_user_id,
            )
            # Close session to persist run

        # Don't raise if status fetch fails: it's not essential for submission!
        with db.session(raise_exception=False):
            _compute_backend.get_and_write_run_status_and_info(
                request=request, run=run, db=db, raise_exception=False
            )
    else:
        if add_run.mlflow_run_id is None:
            raise fastapi.HTTPException(
                status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                detail="Saving a run requires mlflowRunId",
            )

        with db.session():
            if add_run.connection_id:
                checks.user_owns_connection(
                    connection_id=add_run.connection_id,
                    user_id=caller_user_id,
                    db=db,
                )

            run = service.runs.add_run_details_to_database(
                db=db,
                add_run=add_run,
                project_id=projectId,
                caller_user_id=caller_user_id,
                mlflow_run_id=add_run.mlflow_run_id,
                remote_system_job_id=None,
            )

    return models.run.ProjectsProjectIdRunsGet201Response(run_id=run.id)


@router.get(
    "/projects/{projectId}/runs/{runId}",
    responses={
        status.HTTP_200_OK: {
            "model": models.run.Run,
            "description": "Run details",
        },
    },
    tags=[RUNS_TAG],
    summary="Returns the Run details",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.ALL_ROLES)
async def projects_project_id_runs_run_id_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    runId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.run.Run:
    with db.session():
        run = db.get_one(
            orm_model_type=database.run.Run,
            constraints={
                database.run.Run.id: runId,
                database.run.Run.project_id: projectId,
            },
            select_in_loading={
                database.run.Run.experiment_repository: {
                    database.experiment_repository.ExperimentRepository.labels: {}
                },
                database.run.Run.model_repository: {
                    database.model_repository.ModelRepository.labels: {},
                    database.model_repository.ModelRepository.code_repository: {
                        database.code_repository.CodeRepository.labels: {}
                    },
                },
                database.run.Run.data_repository: {
                    database.data_repository.DataRepository.labels: {}
                },
                database.run.Run.connection: {},
            },
        )
        role_details = roles.get_request_user_role_in_project(
            project_id=projectId, request=request, client=db
        )
        return models.run.Run.from_database_model(
            service.runs.fix_endless_running_jobs(run, token=request.token, client=db),
            user_role=role_details.role,
            user_id=request.token.user_id if request.token else None,
        )


@router.put(
    "/projects/{projectId}/runs/{runId}/name",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {"description": "Successfully updated run name"},
        status.HTTP_403_FORBIDDEN: {
            "model": models.error.HTTPError,
            "description": "Only run owner can change run name",
        },
        status.HTTP_404_NOT_FOUND: {
            "model": models.error.HTTPError,
            "description": "Unable to find the run",
        },
    },
    tags=[RUNS_TAG],
    summary="Update run name",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.HEAD_ROLES
)
async def run_name_put(
    request: bearer.ModifiedRequest,
    runId: uuid.UUID,
    projectId: uuid.UUID,
    new_run_name: str = fastapi.Body(..., description="New run name"),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        run: database.run.Run = db.get_one(
            orm_model_type=database.run.Run,
            constraints={
                database.run.Run.id: runId,
                database.project.Project.id: projectId,
            },
        )

        if not run.owned_by_user(request.token.user_id):
            raise fastapi.HTTPException(
                status_code=fastapi.status.HTTP_403_FORBIDDEN,
                detail="Only run owner can change the run name",
            )

        # Atomic transaction for run name and mlflow name
        old_run_name = str(run.name)
        try:
            db.overwrite(run, {"name": new_run_name})
            mlflow.runs.edit_run_name(
                mlflow_run_id=str(run.mlflow_run_id),
                name=new_run_name,
                token=request.token,
            )
        except Exception as e:
            db.overwrite(run, {"name": old_run_name})
            logger.error(str(e))
            raise fastapi.HTTPException(
                status_code=fastapi.status.HTTP_400_BAD_REQUEST,
                detail="Could not change both names in Mantik and Mlflow.",
            )


@router.delete(
    "/projects/{projectId}/runs/{runId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {"description": "Successfully deleted run"},
    },
    tags=[RUNS_TAG],
    summary="Delete a Run",
    description=(
        "Deleting a Run will remove the associated run in the respective Experiment "
        "Repository. This also deletes the respective MLflow run, including logged "
        "parameters, metrics, artifacts, models, etc."
    ),
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_runs_run_id_delete(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    runId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    """Delete a run from an experiment.

    If the run is still active (`SCHEDULED` or `RUNNING`), the run can only be
    deleted by the run owner.

    """
    with db.session():
        run = db.get_one(
            database.run.Run,
            constraints={
                database.run.Run.id: runId,
                database.run.Run.project_id: projectId,
            },
        )

        # If run is still active and is not a local run,
        # check if request user is run owner,
        # then cancel the run.
        if run.is_active() and not run.is_local():
            job = _compute_backend.get_job_from_run_and_raise_for_connection_ownership(
                run=run,
                request=request,
                error_message="Only run owner can delete an active run",
                db=db,
            )
            job.cancel()

        mlflow_run_id = run.mlflow_run_id
        db.delete(run)
        mlflow.runs.delete(run_id=mlflow_run_id, token=request.token)


@router.put(
    "/projects/{projectId}/runs/{runId}/cancel",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {"description": "Successfully cancelled run"},
        status.HTTP_403_FORBIDDEN: {
            "model": models.error.HTTPError,
            "description": "Only run owner can cancel the run",
        },
        status.HTTP_404_NOT_FOUND: {
            "model": models.error.HTTPError,
            "description": "Unable to find the run or the job on the remote system",
        },
        status.HTTP_424_FAILED_DEPENDENCY: {
            "model": models.error.HTTPError,
            "description": "A request to the external UNICORE API has failed",
        },
    },
    tags=[RUNS_TAG],
    summary="Cancel a run",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_runs_run_id_cancel_put(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    runId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    """Cancel a run on the remote system.

    Only the run owner is able to cancel the run.

    **Warning:** This will kill all running processes launched in the run!

    """
    with db.session():
        run: database.run.Run = db.get_one(
            database.run.Run,
            constraints={
                database.run.Run.id: runId,
                database.run.Run.project_id: projectId,
            },
        )
        job = _compute_backend.get_job_from_run_and_raise_for_connection_ownership(
            run=run,
            request=request,
            error_message="Only run owner can cancel the run",
            db=db,
        )
        job.cancel()

        # Set run status to KILLED
        db.overwrite(
            run,
            fields_to_overwrite=run.to_overwrite_kwargs(
                status=database.run.RunStatus.KILLED
            ),
        )


@router.get(
    "/projects/{projectId}/runs/{runId}/info",
    responses={
        status.HTTP_200_OK: {
            "model": models.run_info.Unicore,
            "description": "Successfully fetched the run information",
        },
        status.HTTP_403_FORBIDDEN: {
            "model": models.error.HTTPError,
            "description": "Only run owner can fetch the run information",
        },
        status.HTTP_404_NOT_FOUND: {
            "model": models.error.HTTPError,
            "description": "Unable to find the run or the job on the remote system",
        },
        status.HTTP_424_FAILED_DEPENDENCY: {
            "model": models.error.HTTPError,
            "description": "A request to the external UNICORE API has failed",
        },
    },
    tags=[RUNS_TAG],
    summary="Get the run information",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_runs_run_id_info_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    runId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> (
    models.run_info.Unicore
    | models.run_info.Firecrest
    | models.run_info.SSHSlurm
    | models.run_info.Local
):  # noqa E501
    """Get the run information.

    If the run is in a mutable state, only the run owner is allowed to fetch the run
    information. If it is in an immutable state (`FINISHED` or `FAILED`), any user
    with the respective role can get the run information.

    """
    with db.session():
        run: database.run.Run = db.get_one(
            database.run.Run,
            constraints={
                database.run.Run.id: runId,
                database.run.Run.project_id: projectId,
            },
        )
        if run.is_local():
            return models.run_info.Local.from_run(run=run)
        if run.is_immutable_and_has_info():
            logger.debug("Fetching run info from database")
            match run.backend_type:
                case database.run.SupportedBackends.UNICORE:
                    return models.run_info.Unicore.from_dict(
                        id_=run.remote_system_job_id, data=run.info
                    )
                case database.run.SupportedBackends.FIRECREST:
                    return models.run_info.Firecrest.from_dict(
                        id_=run.remote_system_job_id, data=run.info
                    )
                case database.run.SupportedBackends.SSH:
                    return models.run_info.SSHSlurm.from_dict(
                        id_=run.remote_system_job_id, data=run.info
                    )

        logger.debug("Fetching run info from remote system")

        job = _compute_backend.get_job_from_run_and_raise_for_connection_ownership(
            run=run,
            request=request,
            error_message="Only run owner can fetch the run information",
            db=db,
        )
        info = job.get_info(run.info)
        db.overwrite(
            original_orm=run,
            fields_to_overwrite=run.to_overwrite_kwargs(
                status=job.get_status(),
                info=info.to_json(),
            ),
        )
        return info


@router.get(
    "/projects/{projectId}/runs/{runId}/status",
    status_code=status.HTTP_200_OK,
    responses={
        status.HTTP_200_OK: {
            "model": database.run.RunStatus,
            "description": "Successfully fetched the run status",
        },
        status.HTTP_403_FORBIDDEN: {
            "model": models.error.HTTPError,
            "description": "Only run owner can fetch the run status",
        },
        status.HTTP_404_NOT_FOUND: {
            "model": models.error.HTTPError,
            "description": "Unable to find the run or the job on the remote system",
        },
        status.HTTP_424_FAILED_DEPENDENCY: {
            "model": models.error.HTTPError,
            "description": "A request to the external UNICORE API has failed",
        },
    },
    tags=[RUNS_TAG],
    summary="Get the run status",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_runs_run_id_status_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    runId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> database.run.RunStatus:
    """Get the run status from the job on the remote system.

    Only the run owner is able to fetch the run status if it is in a mutable state.
    If a run has succeeded or failed, other users with the required role can also
    fetch the run status

    The status returned here might be different from the status returned
    in the run info since the submission info originates from the
    external API used (i.e. UNICORE), and Mantik might use a different set
    of possible run statuses.

    """
    with db.session():
        run: database.run.Run = db.get_one(
            database.run.Run,
            constraints={
                database.run.Run.id: runId,
                database.run.Run.project_id: projectId,
            },
        )

        if not run.is_active():
            logger.debug("Run is not active, reading run status from database")
            return run.status

        if run.is_local():
            logger.debug("Run is local, reading run status from database")
            return run.status

        logger.debug("Run status mutable, fetching run status from remote system")
        run = _compute_backend.get_and_write_run_status_and_info(
            request=request,
            run=run,
            error_message=(
                "Run still in a mutable state: "
                "only the run owner can fetch the run status"
            ),
            db=db,
        )
    return run.status


@router.put(
    "/projects/{projectId}/runs/{runId}/status",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {"description": "Successfully modified run status"},
        status.HTTP_403_FORBIDDEN: {
            "model": models.error.HTTPError,
            "description": "Only run owner can modify the run status",
        },
        status.HTTP_404_NOT_FOUND: {
            "model": models.error.HTTPError,
            "description": "Unable to find the run",
        },
    },
    tags=[RUNS_TAG],
    summary="Modify the run status for local runs.",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_runs_run_id_status_put(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    runId: uuid.UUID,
    new_status: database.run.RunStatus = fastapi.Body(
        None,
        description="Modify the run status from the job running locally. "
        "Only the run owner is able to modify the run status. ",
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    """Modify the run status for a run executed locally.

    Only the run owner is able to modify the run status if it is in a mutable state.
    If the run status is FINISHED, KILLED or FAILED it cannot be modified further.
    """
    with db.session():
        run: database.run.Run = db.get_one(
            database.run.Run,
            constraints={
                database.run.Run.id: runId,
                database.run.Run.project_id: projectId,
            },
        )

        if not run.owned_by_user(request.token.user_id):
            raise fastapi.HTTPException(
                status_code=fastapi.status.HTTP_403_FORBIDDEN,
                detail="Only run owner can change the run status",
            )

        if not run.is_active():
            raise fastapi.HTTPException(
                status_code=fastapi.status.HTTP_422_UNPROCESSABLE_ENTITY,
                detail=(
                    "A run status can only be modified "
                    f"if it the latest known status was "
                    f"{database.run.RunStatus.SCHEDULED} or "
                    f"{database.run.RunStatus.RUNNING}"
                ),
            )

        if not run.is_local():
            raise fastapi.HTTPException(
                status_code=fastapi.status.HTTP_422_UNPROCESSABLE_ENTITY,
                detail="Cannot manually modify status of remote job",
            )

        db.overwrite(
            run, fields_to_overwrite=run.to_overwrite_kwargs(status=new_status)
        )


@router.put(
    "/projects/{projectId}/runs/{runId}/infrastructure",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully modified run infrastructure"
        },
        status.HTTP_403_FORBIDDEN: {
            "model": models.error.HTTPError,
            "description": "Only run owner can modify the run infrastructure",
        },
        status.HTTP_404_NOT_FOUND: {
            "model": models.error.HTTPError,
            "description": "Unable to find the run",
        },
    },
    tags=[RUNS_TAG],
    summary="Record the run infrastructure.",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_runs_run_id_infrastructure_put(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    runId: uuid.UUID,
    run_infrastructure: models.run.RunInfrastructure,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        run: database.run.Run = db.get_one(
            database.run.Run,
            constraints={
                database.run.Run.id: runId,
                database.run.Run.project_id: projectId,
            },
        )

        if not run.owned_by_user(request.token.user_id):
            raise fastapi.HTTPException(
                status_code=fastapi.status.HTTP_403_FORBIDDEN,
                detail="Only run owner can change the run infrastructure",
            )

        db.overwrite(
            run,
            fields_to_overwrite={
                "infrastructure": run_infrastructure.dict(
                    convert_keys_to_camel_case=False
                )
            },
        )


@router.get(
    "/projects/{projectId}/runs/{runId}/infrastructure",
    status_code=status.HTTP_200_OK,
    responses={
        status.HTTP_200_OK: {
            "description": "Successfully retrieved run infrastructure"
        },
        status.HTTP_404_NOT_FOUND: {
            "model": models.error.HTTPError,
            "description": "Unable to find the run",
        },
    },
    tags=[RUNS_TAG],
    summary="Get the run infrastructure.",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.ALL_ROLES)
async def projects_project_id_runs_run_id_infrastructure_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    runId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.run.GetRunInfrastructureResponse:
    with db.session():
        run: database.run.Run = db.get_one(
            database.run.Run,
            constraints={
                database.run.Run.id: runId,
                database.run.Run.project_id: projectId,
            },
        )
        run_infrastructure = (
            models.run.RunInfrastructure(**run.infrastructure)
            if run.infrastructure
            else None
        )

        return models.run.GetRunInfrastructureResponse(
            infrastructure=run_infrastructure
        )


@router.put(
    "/projects/{projectId}/runs/{runId}/notebook-source",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully modified notebook source information"
        },
        status.HTTP_403_FORBIDDEN: {
            "model": models.error.HTTPError,
            "description": "Only run owner can modify the notebook source "
            "information",
        },
        status.HTTP_404_NOT_FOUND: {
            "model": models.error.HTTPError,
            "description": "Unable to find the run",
        },
    },
    tags=[RUNS_TAG],
    summary="Record the notebook source information.",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_runs_run_id_notebook_source_put(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    runId: uuid.UUID,
    notebook_source: models.run.NoteBookSource,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    """Update notebook source information for a run.

    Parameters
    ----------
    request : bearer.ModifiedRequest
        The authenticated request object
    projectId : uuid.UUID
        Project identifier
    runId : uuid.UUID
        Run identifier
    notebook_source : models.run.NoteBookSource
        Notebook source information to update
    db : database.client.main.Client
        Database client instance

    Returns
    -------
    None

    Raises
    ------
    HTTPException
        403 if user is not run owner
        404 if run not found
    """
    with db.session():
        run: database.run.Run = db.get_one(
            database.run.Run,
            constraints={
                database.run.Run.id: runId,
                database.run.Run.project_id: projectId,
            },
        )

        if not run.owned_by_user(request.token.user_id):
            raise fastapi.HTTPException(
                status_code=status.HTTP_403_FORBIDDEN,
                detail="Only run owner can change the notebook source " "information",
            )

        db.overwrite(
            run,
            fields_to_overwrite={
                "notebook_source": notebook_source.dict(
                    convert_keys_to_camel_case=False
                )
            },
        )


@router.get(
    "/projects/{projectId}/runs/{runId}/notebook-source",
    status_code=status.HTTP_200_OK,
    responses={
        status.HTTP_200_OK: {
            "description": "Successfully retrieved notebook source information"
        },
        status.HTTP_404_NOT_FOUND: {
            "model": models.error.HTTPError,
            "description": "Unable to find the run",
        },
    },
    tags=[RUNS_TAG],
    summary="Get the run notebook source information.",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.ALL_ROLES)
async def projects_project_id_runs_run_id_notebook_source_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    runId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.run.GetRunNoteBookResponse:
    """Retrieve notebook source information for a specific run.

    Fetches metadata about the notebook environment where this run originated,
    including the notebook type (Jupyter/Colab) and its location. Returns None
    if no notebook source information has been recorded.

    Parameters
    ----------
    request : bearer.ModifiedRequest
        The authenticated request object containing user token and role information
    projectId : uuid.UUID
        Identifier of the project containing the run
    runId : uuid.UUID
        Identifier of the run to fetch notebook source information for
    db : database.client.main.Client
        Database client for data access operations

    Returns
    -------
    models.run.GetRunNoteBookResponse
        Response object containing the notebook source information if it exists,
        or None if no source information is recorded

    Raises
    ------
    fastapi.HTTPException
        404 if the specified run cannot be found
    """
    with db.session():
        run: database.run.Run = db.get_one(
            database.run.Run,
            constraints={
                database.run.Run.id: runId,
                database.run.Run.project_id: projectId,
            },
        )
        notebook_source = (
            models.run.NoteBookSource(**run.notebook_source)
            if run.notebook_source
            else None
        )

        return models.run.GetRunNoteBookResponse(notebook_source=notebook_source)


@router.get(
    "/projects/{projectId}/runs/{runId}/logs",
    status_code=status.HTTP_200_OK,
    responses={
        status.HTTP_200_OK: {
            "model": list[str],
            "description": "Successfully fetched the run logs",
        },
        status.HTTP_403_FORBIDDEN: {
            "model": models.error.HTTPError,
            "description": "Only run owner can fetch the run logs",
        },
        status.HTTP_404_NOT_FOUND: {
            "model": models.error.HTTPError,
            "description": "Unable to find the run or the job on the remote system",
        },
        status.HTTP_424_FAILED_DEPENDENCY: {
            "model": models.error.HTTPError,
            "description": "A request to the external UNICORE API has failed",
        },
    },
    tags=[RUNS_TAG],
    summary="Get the run logs",
    response_model=list[str],
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_runs_run_id_logs_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    runId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> list[str]:
    """Get the logs of the remote system's API as well as the application logs
    of the application executed in the run.

    Only the run owner is able to fetch the run logs for active remote runs.

    """
    with db.session():
        run: database.run.Run = db.get_one(
            database.run.Run,
            constraints={
                database.run.Run.id: runId,
                database.run.Run.project_id: projectId,
            },
        )
        if run.is_local():
            return models.run_info.Local.from_run(run=run).logs
        if run.is_immutable_and_has_logs():
            logger.debug("Fetching run logs from database")
            return run.get_logs()

        logger.debug("Fetching run logs from remote system")

        job = _compute_backend.get_job_from_run_and_raise_for_connection_ownership(  # noqa E501
            run=run,
            request=request,
            error_message="Only run owner can fetch the run logs",
            db=db,
        )
        logs = job.get_logs()
        db.overwrite(
            run,
            {
                "logs": "\n".join(logs),
                "allowed_to_update_logs": run.is_active(),
            },
        )
        return logs


@router.put(
    "/projects/{projectId}/runs/{runId}/logs",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {"description": "Successfully modified run logs"},
        status.HTTP_403_FORBIDDEN: {
            "model": models.error.HTTPError,
            "description": "Only run owner can modify the run logs",
        },
        status.HTTP_404_NOT_FOUND: {
            "model": models.error.HTTPError,
            "description": "Unable to find the run",
        },
    },
    tags=[RUNS_TAG],
    summary="Modify the run logs for local runs.",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_runs_run_id_logs_put(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    runId: uuid.UUID,
    new_logs: str = fastapi.Body(
        None,
        description="Modify the run logs from the job running locally. "
        "Only the run owner is able to modify the run logs. ",
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    """Modify the run logs for a run executed locally.

    Only the run owner is able to modify the run logs if it is active.
    """
    with db.session():
        run: database.run.Run = db.get_one(
            database.run.Run,
            constraints={
                database.run.Run.id: runId,
                database.run.Run.project_id: projectId,
            },
        )

        if not run.owned_by_user(request.token.user_id):
            raise fastapi.HTTPException(
                status_code=fastapi.status.HTTP_403_FORBIDDEN,
                detail="Only run owner can change the run logs",
            )

        if not run.allowed_to_update_logs:
            raise fastapi.HTTPException(
                status_code=fastapi.status.HTTP_422_UNPROCESSABLE_ENTITY,
                detail=("The run logs can only be modified for an active run"),
            )

        if not run.is_local():
            raise fastapi.HTTPException(
                status_code=fastapi.status.HTTP_422_UNPROCESSABLE_ENTITY,
                detail="Cannot manually modify logs of remote job",
            )
        db.overwrite(run, {"logs": new_logs, "allowed_to_update_logs": run.is_active()})


@router.get(
    "/projects/{projectId}/runs/{runId}/download",
    status_code=status.HTTP_200_OK,
    responses={
        status.HTTP_200_OK: {
            "model": bytes,
            "description": "File or zipped folder",
            "content": {
                compute_backend.content.MediaTypes.FILE: {},
                compute_backend.content.MediaTypes.ZIP: {},
            },
        },
        status.HTTP_403_FORBIDDEN: {
            "model": models.error.HTTPError,
            "description": "Only run owner can access the run's working directory",
        },
        status.HTTP_404_NOT_FOUND: {
            "model": models.error.HTTPError,
            "description": "Unable to find the run or the job on the remote system",
        },
        status.HTTP_424_FAILED_DEPENDENCY: {
            "model": models.error.HTTPError,
            "description": "A request to the external UNICORE API has failed",
        },
    },
    tags=[RUNS_TAG],
    summary="Download a file or folder from the run's working directory",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_runs_run_id_download_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    runId: uuid.UUID,
    path: str
    | None = fastapi.Query(
        None,
        description=(
            "Defines the relative path inside the run's working directory to "
            "the file or folder that should be downloaded."
        ),
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> fastapi.responses.StreamingResponse:
    """Downloads a file or folder from the run's working directory.

    Depending on the given path, the response contains one of the following:

    - If no path is given, the entire working directory of the run will be
      recursively downloaded as a ZIP file
    - If the given path is a folder, the entire folder is recursively
      downloaded as a ZIP file.
    - If the given path is a file, the file will be downloaded.

    Response contains the following headers:

    1. `Content-Type` (`application/octet-stream` or `application/x-zip-compressed`):
       - `application/octet-stream`: response contains a single file
       - `application/x-zip-compressed`: response contains an entire folder as
         a ZIP file
    2. `Content-Disposition`: Contains the file name as
       `attachment;filename=<filename>`.

    **Note:** Only the run owner is allowed to access the run's working directory.

    """
    if path is not None:
        # If `path` e.g. contains an encoded slash (`%2F`), it must be
        # decoded to a literal `/`.
        path = pathlib.Path(urllib.parse.unquote(path))

    with db.session():
        run: database.run.Run = db.get_one(
            database.run.Run,
            constraints={
                database.run.Run.id: runId,
                database.run.Run.project_id: projectId,
            },
        )
        job = _compute_backend.get_job_from_run_and_raise_for_connection_ownership(
            run=run,
            request=request,
            error_message="Only run owner can access the run's working directory",
            db=db,
        )
    download = job.download(path)
    return fastapi.responses.StreamingResponse(
        content=download.get_content(),
        status_code=status.HTTP_200_OK,
        media_type=download.media_type,
        headers=download.headers,
    )


# TODO: Extend to allow adding a data repo version to the run config
@router.post(
    "/projects/{projectId}/run-configurations",
    status_code=status.HTTP_201_CREATED,
    responses={
        status.HTTP_201_CREATED: {
            "model": models.run.ProjectsProjectIdRunsGet201Response,
            "description": "Configured a new run",
        },
    },
    tags=[RUN_CONFIGURATION_TAG],
    summary="Configures a new run",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_runs_configure_run(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    new_run_configuration: models.run.AddRun = fastapi.Body(
        ..., description="Run configuration"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.run.ProjectsProjectIdRunConfigurationsGet201Response:
    new_run_configuration_id = uuid.uuid4()
    with db.session():
        checks.user_owns_connection(
            connection_id=new_run_configuration.connection_id,
            user_id=request.token.user_id,
            db=db,
        )
        db.add(
            new_run_configuration.to_run_configuration_database_model(
                user_id=request.token.user_id,
                project_id=projectId,
                run_configuration_id=new_run_configuration_id,
            )
        )
    return models.run.ProjectsProjectIdRunConfigurationsGet201Response(
        run_configuration_id=new_run_configuration_id
    )


@router.get(
    "/projects/{projectId}/run-configurations",
    responses={
        status.HTTP_200_OK: {
            "model": models.run.ProjectsProjectIdRunConfigurationsGet200Response,
            "description": "OK",
        },
    },
    tags=[RUN_CONFIGURATION_TAG],
    summary="Returns all configured runs for a given project",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.ALL_ROLES)
async def projects_project_id_run_configurations_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    startindex: int = _parameters.start_index(),
    pagelength: int = _parameters.page_length(),
    order_by: database.run.OrderRunConfigurationBy = _parameters.order_by(
        default=database.run.OrderRunConfigurationBy.created
    ),
    ascending: bool = _parameters.ascending(),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.run.ProjectsProjectIdRunConfigurationsGet200Response:
    with db.session():
        result: database.client.main.GetAllResponse = db.get_many(
            orm_model_type=database.run.RunConfiguration,
            constraints={
                database.run.RunConfiguration.project_id: projectId,
            },
            select_in_loading={
                database.run.RunConfiguration.data_repository: {
                    database.data_repository.DataRepository.labels: {}
                },
                database.run.RunConfiguration.code_repository: {
                    database.code_repository.CodeRepository.labels: {}
                },
            },
            offset=startindex,
            limit=pagelength,
            order_by=order_by,
            ascending=ascending,
        )
        role_details = roles.get_request_user_role_in_project(
            project_id=projectId, request=request, client=db
        )
        return models.run.ProjectsProjectIdRunConfigurationsGet200Response(
            total_records=result.total_count,
            page_records=result.page_count,
            run_configurations=models.base.from_database_models(
                models.run.RunConfiguration,
                database_models=result.entities,
                user_role=role_details.role,
                user_id=request.token.user_id if request.token else None,
            ),
            user_role=role_details.role,
        )


@router.get(
    "/projects/{projectId}/run-configurations/{runConfigurationId}",
    responses={
        status.HTTP_200_OK: {
            "model": models.run.RunConfiguration,
            "description": "OK",
        },
    },
    tags=[RUN_CONFIGURATION_TAG],
    summary="Returns a configured run for a given project",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.ALL_ROLES)
async def projects_project_id_run_configurations_run_configuration_id_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    runConfigurationId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.run.RunConfiguration:
    with db.session():
        result: database.run.RunConfiguration = db.get_one(
            orm_model_type=database.run.RunConfiguration,
            constraints={
                database.run.RunConfiguration.id: runConfigurationId,
                database.run.RunConfiguration.project_id: projectId,
            },
            select_in_loading={
                database.run.RunConfiguration.data_repository: {
                    database.data_repository.DataRepository.labels: {}
                },
                database.run.RunConfiguration.code_repository: {
                    database.code_repository.CodeRepository.labels: {}
                },
            },
        )
        role_details = roles.get_request_user_role_in_project(
            project_id=projectId, request=request, client=db
        )
        return models.run.RunConfiguration.from_database_model(
            run=result,
            user_role=role_details.role,
            user_id=request.token.user_id if request.token else None,
        )


@router.post(
    "/projects/{projectId}/run-configurations/{runConfigurationId}/trigger",
    status_code=status.HTTP_201_CREATED,
    responses={
        status.HTTP_201_CREATED: {
            "model": models.run.ProjectsProjectIdRunsGet201Response,
            "description": "Submitted new run",
        },
    },
    tags=[RUN_CONFIGURATION_TAG],
    summary="Triggers a previously configured run",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_run_configurations_run_configuration_id_trigger_post(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    runConfigurationId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.run.ProjectsProjectIdRunsGet201Response:
    with db.session():
        run_configuration = db.get_one(
            orm_model_type=database.run.RunConfiguration,
            constraints={
                database.run.RunConfiguration.id: runConfigurationId,
                database.run.RunConfiguration.project_id: projectId,
                database.run.RunConfiguration.user_id: request.token.user_id,
            },
        )
        add_run = _create_add_run(run_configuration)
        validate_reference_bitbucket_repo(db, add_run)
        run = service.runs.trigger_run(
            db=db,
            add_run=add_run,
            token=request.token,
            project_id=projectId,
            caller_user_id=request.token.user_id,
        )

        # Close session to persist run.

    # Don't raise if status fetch fails: it's not essential for submission!
    with db.session(raise_exception=False):
        _compute_backend.get_and_write_run_status_and_info(
            request=request, run=run, db=db, raise_exception=False
        )

    return models.run.ProjectsProjectIdRunsGet201Response(run_id=run.id)


def _create_add_run(
    run_configuration: database.run.RunConfiguration,
) -> models.run.AddRun:
    return models.run.AddRun(
        name=run_configuration.name,
        experiment_repository_id=run_configuration.experiment_repository.id,
        code_repository_id=run_configuration.code_repository.id,
        branch=run_configuration.branch,
        commit=run_configuration.commit,
        data_repository=(
            run_configuration.data_repository.id
            if run_configuration.data_repository is not None
            else None
        ),
        connection_id=run_configuration.connection.id,
        compute_budget_account=run_configuration.compute_budget_account,
        mlflow_mlproject_file_path=run_configuration.mlflow_mlproject_file_path,
        entry_point=run_configuration.entry_point,
        mlflow_parameters=run_configuration.mlflow_parameters,
        backend_config=run_configuration.backend_config,
        environment_id=(
            run_configuration.environment.id
            if run_configuration.environment is not None
            else None
        ),
    )


@router.delete(
    "/projects/{projectId}/run-configurations/{runConfigurationId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully deleted run configuration"
        },
    },
    tags=[RUN_CONFIGURATION_TAG],
    summary="Delete run configuration",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.WORKING_ROLES
)
async def projects_project_id_run_configurations_run_configuration_id_delete(
    request: bearer.ModifiedRequest,
    runConfigurationId: uuid.UUID,
    projectId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        db.delete(
            database.run.RunConfiguration,
            constraints={
                database.run.RunConfiguration.id: runConfigurationId,
                database.run.RunConfiguration.project_id: projectId,
                database.run.RunConfiguration.user_id: request.token.user_id,
            },
        )


def validate_reference_bitbucket_repo(
    db: database.client.main.Client, add_run: models.run.AddRun
):
    """For a given Code Repository, if it is a Bitbucket repo, validate that
    a commit hash is given.

    For details, see :func:`mantik_api.invoke_compute_backend._git_repo.GitRepo.zip_directory_name`.  # noqa: E501
    """
    code_repository = db.get_one(
        orm_model_type=database.code_repository.CodeRepository,
        constraints={
            database.code_repository.CodeRepository.id: add_run.code_repository_id
        },
    )
    if code_repository.platform == database.git_repository.Platform.Bitbucket:
        if add_run.commit is None:
            raise fastapi.HTTPException(
                status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                detail="For Bitbucket Code Repositories, please provide a commit hash",
            )


@router.get(
    "/projects/{projectId}/runs/{runId}/artifacts",
    status_code=status.HTTP_200_OK,
    responses={
        status.HTTP_200_OK: {
            "model": models.presigned_url.PresignedUrl,
            "description": "Download URL for zipped run artifacts.",
        },
        status.HTTP_401_UNAUTHORIZED: {
            "model": models.error.HTTPError,
            "description": "Unauthorized",
        },
        status.HTTP_404_NOT_FOUND: {
            "model": models.error.HTTPError,
            "description": "Resource not found",
        },
    },
    tags=[RUNS_TAG],
    summary="Return the download url for zipped run artifacts.",
)
@authorization.authorized_project_get_roles(
    allowed_roles=authorization.PROJECT_ROLES, allowed_for_public_projects=False
)
async def projects_project_id_runs_run_id_artifacts_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    runId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    with db.session():
        run = db.get_one(
            orm_model_type=database.run.Run,
            constraints={
                database.run.Run.id: runId,
                database.run.Run.project_id: projectId,
            },
            select_in_loading={
                database.run.Run.experiment_repository: {},
            },
        )
        try:
            url = mlflow.runs.generate_pre_signed_run_artifact_url(
                mlflow_run_id=run.mlflow_run_id,
                mlflow_experiment_id=run.experiment_repository.mlflow_experiment_id,
            )
        except mlflow.exceptions.RunArtifactsNotFoundException:
            raise fastapi.HTTPException(
                status_code=fastapi.status.HTTP_404_NOT_FOUND,
                detail="No artifacts exist for selected run.",
            )
    return models.presigned_url.PresignedUrl(url=url)
