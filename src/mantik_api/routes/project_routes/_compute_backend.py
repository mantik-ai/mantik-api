import logging

import fastapi
import starlette.status as status

import mantik_api.checks as checks
import mantik_api.compute_backend.firecrest as firecrest
import mantik_api.compute_backend.job as _job
import mantik_api.compute_backend.ssh_remote_compute_system as _ssh
import mantik_api.compute_backend.unicore as unicore
import mantik_api.database as database
import mantik_api.models as models
import mantik_api.routes.rbac.bearer as bearer

logger = logging.getLogger(__name__)


def get_and_write_run_status_and_info(
    request: bearer.ModifiedRequest,
    run: database.run.Run,
    db: database.client.main.Client,
    raise_exception: bool = True,
    error_message: str = "Only the run owner can fetch the run status",
) -> database.run.Run:
    job = get_job_from_run_and_raise_for_connection_ownership(
        run=run,
        request=request,
        error_message=error_message,
        db=db,
    )

    logger.info(
        "Fetching initial status and info for run %s with backend config %s",
        run,
        run.backend_config,
    )

    try:
        run.status = job.get_status()
        run.info = job.get_info().to_json()
        db.update(
            run,
            constraints={database.run.Run.id: run.id},
            fields_to_update={"status": run.status, "info": run.info},
        )
    except (
        unicore.exceptions.UnicoreException,
        firecrest.FirecrestException,
        _ssh.exceptions.SSHError,
    ):
        logger.exception(
            "Failed to fetch and write run status and info after run submission",
            exc_info=True,
        )

        if raise_exception:
            raise

    return run


def get_job_from_run_and_raise_for_connection_ownership(
    run: database.run.Run,
    request: bearer.ModifiedRequest,
    error_message: str,
    db: database.client.main.Client,
) -> _job.JobBase:
    logger.info("Getting job from run %s and checking ownership", run)

    if not run.owned_by_user(request.token.user_id):
        raise fastapi.HTTPException(
            status_code=fastapi.status.HTTP_403_FORBIDDEN,
            detail=error_message,
        )

    connection = checks.user_owns_connection(
        connection_id=run.connection_id,
        user_id=request.token.user_id,
        db=db,
        message=error_message,
    )
    api_connection = models.connection.Connection.from_database_model_with_credentials(
        connection=connection, token=request.token
    )

    logger.info("Initializing job for run backend type %s", run.backend_type)

    match run.backend_type:
        case database.run.SupportedBackends.UNICORE:
            logger.info("Initializing UNICORE job")

            try:
                client = unicore.connect.connect_to_unicore_api(
                    url=run.backend_config["UnicoreApiUrl"],
                    credentials=api_connection.to_unicore_credentials(),
                )
                return client.get_job(job_id=run.remote_system_job_id)
            except unicore.exceptions.JobNotFoundException as e:
                raise fastapi.HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND,
                    detail=(
                        f"No job found on the remote system for run "
                        f"with ID {run.id!s}: {str(e)}. "
                        "The job and it's working directory might have been deleted. "
                        "Some systems frequently delete unused directories."
                    ),
                )
            except Exception:
                raise fastapi.HTTPException(
                    status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
                    detail=(
                        "Unexpected error when fetching Unicore job, "
                        "check that Unicore is reachable."
                    ),
                )
        case database.run.SupportedBackends.FIRECREST:
            logger.info("Initializing firecREST job")

            firecrest_backend_config = run.backend_config["Firecrest"]
            client = firecrest.connect.connect_to_firecrest_api(
                api_url=firecrest_backend_config["ApiUrl"],
                auth=api_connection.to_firecrest_credentials(
                    token_url=firecrest_backend_config["TokenUrl"]
                ),
            )
            # If run info is available, it might yield the run directory.
            # Otherwise, it will be inferred when initializing the class.
            return firecrest.job.Job.from_run_info(
                client=client,
                job_id=run.remote_system_job_id,
                machine=firecrest_backend_config["Machine"],
                mlflow_run_id=run.mlflow_run_id,
                info=models.run_info.Firecrest.from_dict(
                    id_=run.remote_system_job_id, data=run.info
                )
                if run.info is not None
                else None,
            )
        case database.run.SupportedBackends.SSH:
            try:
                ssh_config = run.backend_config["SSH"]
                ssh_credentials = api_connection.to_ssh_credentials()
                ssh_client = _ssh.client.Client(
                    hostname=ssh_config["Hostname"],
                    port=ssh_config.get("Port"),
                    username=ssh_credentials.username,
                    password=ssh_credentials.password,
                    private_key=ssh_credentials.private_key,
                )
                return _ssh.job.Job(
                    ssh_client=ssh_client, job_id=run.remote_system_job_id
                )
            except _ssh.exceptions.AuthenticationFailedException:
                raise fastapi.HTTPException(
                    status_code=status.HTTP_401_UNAUTHORIZED,
                    detail="Unauthorized to fetch job from remote system.",
                )
            except Exception as e:
                logger.error(e)
                raise fastapi.HTTPException(
                    status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
                    detail="Unexpected error when fetching SSH job,",
                )

        case _:
            raise ValueError(
                f"Unknown backend type in backend config: {run.backend_config}"
            )
