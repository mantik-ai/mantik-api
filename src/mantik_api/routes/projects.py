import typing as t
import uuid

import fastapi
import sqlalchemy
import starlette.status as status

import mantik_api.database as database
import mantik_api.models as models
import mantik_api.routes._parameters as _parameters
import mantik_api.routes.dependencies as dependencies
import mantik_api.routes.rbac.authorization as authorization
import mantik_api.routes.rbac.bearer as bearer
import mantik_api.routes.rbac.roles as roles
import mantik_api.service as service
import mantik_api.tokens as tokens
import mantik_api.utils as utils

router = fastapi.APIRouter(dependencies=[fastapi.Depends(bearer.OptionalJWTBearer())])


@router.get(
    "/projects",
    responses={
        status.HTTP_200_OK: {
            "model": models.project.ProjectsGet200Response,
            "description": "OK",
        },
    },
    tags=["projects"],
    summary="Returns all projects",
)
async def projects_get(
    request: bearer.ModifiedRequest,
    startindex: int = _parameters.start_index(),
    pagelength: int = _parameters.page_length(),
    labels: list[uuid.UUID] | None = _parameters.labels(),
    words: list[str] | None = _parameters.words(),
    order_by: database.project.OrderBy = _parameters.order_by(
        default=database.project.OrderBy.created
    ),
    ascending: bool = _parameters.ascending(),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.project.ProjectsGet200Response:
    """Returns all projects accessible to a user.

    Only returns public projects and projects where the user is a member.
    Projects will be filtered according to the search query parameters:

    - `words`
    - `labels`

    ## Search by Words

    All words must be present in either one of:

    - `name`
    - `executiveSummary`
    - `detailedDescription`

    For example, if you are searching for `Mantik` and `classification`
    and this project is present:

    ```json
    {
      "name": "Mantik",
      "executiveSummary": "Classification model for chocolate bars",
      "detailedDescription": "This project classifies chocolate bars using ..."
    }
    ```

    Since `Mantik` and `classification` are not present together in any of the
    fields, this project will not be among the filtered ones.

    ## Search by labels

    _All_ given labels _must_ be assigned either to the project,
    _or_ any of it's related entities such as Code, Data, and Experiment
    Repository.

    For example, if you query using `?labels=<label 1>,<label 2>`,
    all projects will be returned that have

    - `<label 1>` and `<label 2>` assigned
    - _or_ that have only `<label 1>` assigned and one of its Data
      Repositories has `<label 2>` assigned,
    - _or_ that have no label assigned, but e.g. one of its Code
      Repositories has both `<label 1>` and `<label 2>` assigned.

    """
    select_in_loading = {
        database.project.Project.experiment_repositories: {
            database.experiment_repository.ExperimentRepository.labels: {}
        },
        database.project.Project.code_repositories: {
            database.code_repository.CodeRepository.labels: {}
        },
        database.project.Project.model_repositories: {
            database.model_repository.ModelRepository.labels: {},
            database.model_repository.ModelRepository.code_repository: {},
        },
        database.project.Project.data_repositories: {
            database.data_repository.DataRepository.labels: {}
        },
        database.project.Project.labels: {},
        database.project.Project.owner: {},
    }

    with db.session():
        match request.token:
            # Case: authenticated user
            case tokens.jwt.JWT():
                select_in_loading |= {
                    database.project.Project.project_members: {},
                    database.project.Project.project_user_groups: {},
                    database.project.Project.project_organizations: {},
                }
                user = db.get_one(
                    orm_model_type=database.user.User,
                    constraints={database.user.User.id: request.token.user_id},
                    select_in_loading={
                        database.user.User.groups: {},
                        database.user.User.owned_groups: {},
                        database.user.User.organizations: {},
                        database.user.User.owned_organizations: {},
                    },
                )
                result: database.client.main.GetAllResponse = db.get_many(
                    orm_model_type=database.project.Project,
                    constraints=database.project.Project.create_constraint_for_user_accessible_projects_with_query_parameter(  # noqa: E501
                        user_=user, words=words, labels=labels
                    ),
                    select_in_loading=select_in_loading,
                    limit=pagelength,
                    offset=startindex,
                    order_by=order_by,
                    ascending=ascending,
                )
                return models.project.ProjectsGet200Response(
                    total_records=result.total_count,
                    page_records=result.page_count,
                    projects=_create_api_models_with_user_roles(
                        result.entities,
                        request=request,
                        db=db,
                    ),
                )
            # Case: unauthenticated user
            case _:
                result: database.client.main.GetAllResponse = db.get_many(
                    orm_model_type=database.project.Project,
                    constraints=database.project.Project.create_constraint_for_public_projects_with_query_parameter(  # noqa: E501
                        words=words, labels=labels
                    ),
                    select_in_loading=select_in_loading,
                    limit=pagelength,
                    offset=startindex,
                    order_by=order_by,
                    ascending=ascending,
                )
                return models.project.ProjectsGet200Response(
                    total_records=result.total_count,
                    page_records=result.page_count,
                    projects=models.base.from_database_models(
                        model=models.project.Project,
                        database_models=result.entities,
                        user_role=database.role_details.ProjectRole.NO_ROLE,
                        user_id=None,  # unauthenticated
                    ),
                )


def _create_api_models_with_user_roles(
    projects: t.Iterator[database.project.Project],
    request: bearer.ModifiedRequest,
    db: database.client.main.Client,
) -> t.Iterator[models.project.Project]:
    return (
        models.project.Project.from_database_model(
            project=project,
            user_role=roles.get_request_user_role_in_project(
                project_id=project.id, request=request, client=db
            ).role,
            user_id=request.token.user_id if request.token else None,
        )
        for project in projects
    )


@router.get(
    "/projects/{projectId}",
    responses={
        status.HTTP_200_OK: {
            "model": models.project.Project,
            "description": "Information on a specific project",
        },
    },
    tags=["projects"],
    summary="Returns the information on a specific project",
)
@authorization.authorized_project_get_roles(allowed_roles=authorization.ALL_ROLES)
async def projects_project_id_get(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.project.Project:
    with db.session():
        project = db.get_one(
            orm_model_type=database.project.Project,
            constraints={database.project.Project.id: projectId},
            select_in_loading={
                database.project.Project.experiment_repositories: {
                    database.experiment_repository.ExperimentRepository.labels: {}
                },
                database.project.Project.code_repositories: {
                    database.code_repository.CodeRepository.labels: {}
                },
                database.project.Project.model_repositories: {
                    database.model_repository.ModelRepository.labels: {},
                    database.model_repository.ModelRepository.code_repository: {
                        database.code_repository.CodeRepository.labels: {}
                    },
                },
                database.project.Project.data_repositories: {
                    database.data_repository.DataRepository.labels: {}
                },
                database.project.Project.labels: {},
                database.project.Project.owner: {},
            },
        )
        role_details = roles.get_request_user_role_in_project(
            project_id=project.id, request=request, client=db
        )
        return models.project.Project.from_database_model(
            project,
            user_role=role_details.role,
            user_id=request.token.user_id if request.token else None,
        )


@router.put(
    "/projects/{projectId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully updated project information"
        },
        status.HTTP_400_BAD_REQUEST: {
            "model": models.error.HTTPError,
            "description": "Label assignment failed due to invalid scope",
        },
    },
    tags=["projects"],
    summary="Update project",
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.HEAD_ROLES
)
async def projects_project_id_put(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    updated_project_payload: models.project.AddProject = fastapi.Body(
        ..., description="Updated project information"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        try:
            models.label.validate_labels_scope(
                updated_project_payload.labels,
                expected_scope=database.label.Scope.Project,
                client=db,
            )
        except ValueError as e:
            raise fastapi.HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail=str(e)
            )

        old_project = db.get_one(
            orm_model_type=database.project.Project,
            constraints={database.project.Project.id: projectId},
        )

        db.overwrite(
            old_project,
            fields_to_overwrite=updated_project_payload.to_overwrite_kwargs(
                client=db,
                # NOTE it is not possible to update the owner of a project
                owner_id=old_project.owner_id,
            ),
        )


@router.post(
    "/projects",
    status_code=status.HTTP_201_CREATED,
    responses={
        status.HTTP_201_CREATED: {
            "model": models.project.ProjectsGet201Response,  # noqa: E501
            "description": "Created new project",
        },
        status.HTTP_400_BAD_REQUEST: {
            "model": models.error.HTTPError,
            "description": "Label assignment failed due to invalid scope",
        },
    },
    tags=["projects"],
    summary="Creates a new project",
)
async def projects_post(
    request: bearer.ModifiedRequest,
    new_project_payload: models.project.AddProject = fastapi.Body(
        ..., description="Project information"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.project.ProjectsGet201Response:
    with db.session():
        try:
            models.label.validate_labels_scope(
                new_project_payload.labels, database.label.Scope.Project, db
            )
        except ValueError as e:
            raise fastapi.HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail=str(e)
            )
        project_id = uuid.uuid4()
        db.add(
            new_project_payload.to_database_model(
                _uuid=project_id, owner_id=request.token.user_id, client=db
            )
        )
    return models.project.ProjectsGet201Response(project_id=project_id)


@router.delete(
    "/projects/{projectId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully deleted project",
        },
    },
    tags=["projects"],
    summary="Delete a project",
    description=(
        "Deleting a project will result in a cascade deletion of all of the following"
        "objects related to the respective project:"
        ""
        "- Code Repositories"
        "- Data Repositories"
        "- Experiment Repositories"
        "- Model Repositories"
        "- Saved Model Repositories"
        "- Runs"
        "- Scheduled Runs"
    ),
)
@authorization.authorized_project_put_post_delete_roles(
    allowed_roles=authorization.OWNER_ROLE
)
async def projects_delete(
    request: bearer.ModifiedRequest,
    projectId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        mlflow_experiment_ids = _get_mlflow_experiment_ids(db, project_id=projectId)
        db.delete(
            database.project.Project,
            constraints={database.project.Project.id: projectId},
        )
        for experiment_id in mlflow_experiment_ids:
            utils.mlflow.experiments.delete(
                experiment_id=experiment_id,
                token=request.token,
            )
        service.invitations.delete_related_invitations(
            db=db, model=database.project.Project, model_id=projectId
        )


def _get_mlflow_experiment_ids(
    db: database.client.main.Client, project_id: uuid.UUID
) -> list[int]:
    """Get MLflow experiment IDs of all experiment repositories of a project.

    Notes
    -----
    This is required to delete all respective MLflow experiments upon successful
    deletion of the project and its experiment repositories.

    """
    result = db.get_all(
        database.experiment_repository.ExperimentRepository,
        constraints={
            database.experiment_repository.ExperimentRepository.project_id: project_id
        },
    )
    return [
        experiment_repository.mlflow_experiment_id
        for experiment_repository in result.entities
    ]


@router.get(
    "/projects/user/{userId}",
    responses={
        status.HTTP_200_OK: {
            "model": models.project.ProjectsGet200Response,
            "description": "OK",
        },
    },
    tags=["projects"],
    summary="Returns all projects for user with userId",
)
async def projects_user_user_id_get(
    request: bearer.ModifiedRequest,
    userId: uuid.UUID,
    startindex: int = _parameters.start_index(),
    pagelength: int = _parameters.page_length(),
    labels: list[uuid.UUID] | None = _parameters.labels(),
    words: list[str] | None = _parameters.words(),
    order_by: database.project.OrderBy = _parameters.order_by(
        default=database.project.OrderBy.created
    ),
    ascending: bool = _parameters.ascending(),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.project.ProjectsGet200Response:
    """Returns all projects of a user that are accessible to the request user.

    Only returns public projects and projects where the user is a member.
    Projects will be filtered according to the search query parameters:

    - `words`
    - `labels`

    ## Search by Words

    All words must be present in either one of:

    - `name`
    - `executiveSummary`
    - `detailedDescription`

    For example, if you are searching for `Mantik` and `classification`
    and this project is present:

    ```json
    {
      "name": "Mantik",
      "executiveSummary": "Classification model for chocolate bars",
      "detailedDescription": "This project classifies chocolate bars using ..."
    }
    ```

    Since `Mantik` and `classification` are not present together in any of the
    fields, this project will not be among the filtered ones.

    ## Search by labels

    _All_ given labels _must_ be assigned either to the project,
    _or_ any of it's related entities such as Code, Data, and Experiment
    Repository.

    For example, if you query using `?labels=<label 1>,<label 2>`,
    all projects will be returned that have

    - `<label 1>` and `<label 2>` assigned
    - _or_ that have only `<label 1>` assigned and one of its Data
      Repositories has `<label 2>` assigned,
    - _or_ that have no label assigned, but e.g. one of its Code
      Repositories has both `<label 1>` and `<label 2>` assigned.

    """
    with db.session():
        user_to_be_viewed = db.get_one(
            orm_model_type=database.user.User,
            constraints={database.user.User.id: userId},
            select_in_loading={
                database.user.User.groups: {},
                database.user.User.owned_groups: {},
                database.user.User.organizations: {},
                database.user.User.owned_organizations: {},
            },
        )
        select_in_loading = {
            database.project.Project.experiment_repositories: {
                database.experiment_repository.ExperimentRepository.labels: {}
            },
            database.project.Project.code_repositories: {
                database.code_repository.CodeRepository.labels: {}
            },
            database.project.Project.model_repositories: {
                database.model_repository.ModelRepository.labels: {},
                database.model_repository.ModelRepository.code_repository: {},
            },
            database.project.Project.data_repositories: {
                database.data_repository.DataRepository.labels: {}
            },
            database.project.Project.labels: {},
            database.project.Project.owner: {},
        }
        match request.token:
            case tokens.jwt.JWT():
                request_user = db.get_one(
                    orm_model_type=database.user.User,
                    constraints={database.user.User.id: request.token.user_id},
                    select_in_loading={
                        database.user.User.groups: {},
                        database.user.User.owned_groups: {},
                        database.user.User.organizations: {},
                        database.user.User.owned_organizations: {},
                    },
                )
                result: database.client.main.GetAllResponse = db.get_many(
                    orm_model_type=database.project.Project,
                    constraints=sqlalchemy.and_(
                        database.project.Project.create_constraint_for_user_projects(
                            user_to_be_viewed
                        ),
                        database.project.Project.create_constraint_for_user_accessible_projects_with_query_parameter(  # noqa: E501
                            user_=request_user, words=words, labels=labels
                        ),
                    ),
                    select_in_loading=select_in_loading,
                    limit=pagelength,
                    offset=startindex,
                    order_by=order_by,
                    ascending=ascending,
                )
                return models.project.ProjectsGet200Response(
                    total_records=result.total_count,
                    page_records=result.page_count,
                    projects=_create_api_models_with_user_roles(
                        result.entities,
                        request=request,
                        db=db,
                    ),
                )
            case _:
                result: database.client.main.GetAllResponse = db.get_many(
                    orm_model_type=database.project.Project,
                    constraints=sqlalchemy.and_(
                        database.project.Project.create_constraint_for_user_projects(
                            user_to_be_viewed
                        ),
                        database.project.Project.create_constraint_for_public_projects_with_query_parameter(  # noqa: E501
                            labels=labels, words=words
                        ),
                    ),
                    select_in_loading=select_in_loading,
                    limit=pagelength,
                    offset=startindex,
                    order_by=order_by,
                    ascending=ascending,
                )
                return models.project.ProjectsGet200Response(
                    total_records=result.total_count,
                    page_records=result.page_count,
                    projects=models.base.from_database_models(
                        model=models.project.Project,
                        database_models=result.entities,
                        user_role=database.role_details.ProjectRole.NO_ROLE,
                        user_id=None,
                    ),
                )
