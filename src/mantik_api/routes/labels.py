import uuid

import fastapi
import starlette.status as status

import mantik_api.database as database
import mantik_api.models as models
import mantik_api.routes._parameters as _parameters
import mantik_api.routes.dependencies as dependencies
import mantik_api.routes.rbac.bearer as bearer


router = fastapi.APIRouter(dependencies=[fastapi.Depends(bearer.OptionalJWTBearer())])


@router.get(
    "/labels",
    responses={
        status.HTTP_200_OK: {
            "model": models.label.LabelsGet200Response,
            "description": "List of labels",
        },
        status.HTTP_422_UNPROCESSABLE_ENTITY: {"description": "Invalid scope"},
    },
    tags=["labels"],
    summary="Get all labels",
)
async def labels_get(
    scope: database.label.Scope
    | None = fastapi.Query(None, description="Scope for which to fetch the labels"),
    order_by: database.label.OrderBy = _parameters.order_by(
        default=database.label.OrderBy.created
    ),
    ascending: bool = _parameters.ascending(),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.label.LabelsGet200Response:
    """Get all labels.

    Labels have the following characteristics:

    1. A label can be assigned to a project and code, data, and experiment
       repositories, which is reflected by the `scopes` of a label.
    2. A label can have multiple scopes, but has at least one scope.
    3. The possible scopes of labels are:

       - `project`: Project
       - `code`: Code Repository
       - `data`: Data Repository
       - `experiment`: Experiment Repository

    4. Labels are grouped into categories and sub-categories.
    5. Labels within a specific category can be assigned to the same entity,
       i.e. have the same scopes.

       For example, a label from the `Software > Programming Language` category
       might be assignable to a Code Repository (`scopes = ['code']`), while a
       label from `Licenses > Data` is only assignable to a Data Repository
       (`scopes = ['data']`).

    For convenience, we partly use imported labels from
    [Huggingface](https://huggingface.co/).
    Hence, the naming and grouping convention is partly identical.
    The given labels may change in the future as we might import labels from
    other sources.

    """
    constraint = (
        database.label.Label.scopes.contains([scope]) if scope is not None else None
    )
    with db.session():
        result: database.client.main.GetAllResponse = db.get_all(
            orm_model_type=database.label.Label,
            constraints=constraint,
            order_by=order_by,
            ascending=ascending,
        )
        return models.label.LabelsGet200Response(
            total_records=result.total_count,
            page_records=result.page_count,
            labels=models.base.from_database_models(
                models.label.Label, result.entities
            ),
        )


@router.get(
    "/labels/{labelId}",
    responses={
        status.HTTP_200_OK: {
            "model": models.label.Label,
            "description": "Get Label information",
        },
    },
    tags=["labels"],
    summary="Get Label information",
)
async def labels_label_id_get(
    labelId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.label.Label:
    """Get a label by ID."""
    with db.session():
        label = db.get_one(
            orm_model_type=database.label.Label,
            constraints={database.label.Label.id: labelId},
        )
        return models.label.Label.from_database_model(label)
