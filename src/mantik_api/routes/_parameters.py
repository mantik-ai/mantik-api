import uuid

import fastapi

import mantik_api.database as database


def start_index(default: int = 0, ge: int = 0) -> fastapi.Query:
    return fastapi.Query(default, description="Start index for paging", ge=ge)


def page_length(default: int = 50, ge: int = 1, le: int = 200) -> fastapi.Query:
    return fastapi.Query(
        default, description="Number of records to return", ge=ge, le=le
    )


def labels(default: list[uuid.UUID] | None = None) -> fastapi.Query:
    return fastapi.Query(
        default,
        description="Label IDs of labels that must be assigned to the project "
        "or any of its related objects "
        "(e.g. Code, Data, Experiment Repository)",
    )


def words(default: list[str] | None = None) -> fastapi.Query:
    return fastapi.Query(
        default,
        description="Words to search for in the project name, "
        "executive summary and detailed description",
    )


def order_by(default: database.base.OrderByBase) -> fastapi.Query:
    return fastapi.Query(
        default,
        description="Property to use for ordering the results",
    )


def ascending(default: bool = False) -> fastapi.Query:
    return fastapi.Query(
        default,
        description=(
            "Whether to sort ascending (`true`) or descending (`false`). "
            "Default is descending."
        ),
    )
