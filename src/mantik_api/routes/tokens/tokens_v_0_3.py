import fastapi
import starlette.status as status

import mantik_api.aws.cognito as cognito
import mantik_api.database as database
import mantik_api.models as models
import mantik_api.routes.dependencies as dependencies

router = fastapi.APIRouter()


@router.post(
    "/tokens/create",
    status_code=status.HTTP_201_CREATED,
    responses={
        status.HTTP_201_CREATED: {
            "model": models.tokens_v_0_3.CreateTokenResponse,
            "description": "Access token with expiration date and refresh token",
        },
        status.HTTP_401_UNAUTHORIZED: {
            "model": models.error.HTTPError,
            "description": "Unable to process user",
        },
        status.HTTP_403_FORBIDDEN: {
            "model": models.error.HTTPError,
            "description": "User is not confirmed",
        },
        status.HTTP_404_NOT_FOUND: {
            "model": models.error.HTTPError,
            "description": "User not found",
        },
    },
    tags=["tokens"],
    summary="Create a new access and refresh token",
)
async def create_token(
    token_request: models.tokens_v_0_3.CreateTokenRequest = fastapi.Body(
        ..., description="Username or email address and password"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.tokens_v_0_3.CreateTokenResponse:
    """Create new access and refresh token.

    Allows username or email address.

    """
    with db.session():
        try:
            user = db.get_one(
                orm_model_type=database.user.User,
                constraints={database.user.User.preferred_name: token_request.username},
            )
        except database.exceptions.NotFoundError:
            # If not found by username, attempt to find by email
            user = db.get_one(
                orm_model_type=database.user.User,
                constraints={database.user.User.email: token_request.username},
            )
        credentials = token_request.to_cognito_credentials(user)

    try:
        tokens = cognito.tokens.get_tokens(credentials)
    except cognito.exceptions.AuthenticationFailedException as e:
        message = str(e)
        if "user is not confirmed" in message.lower():
            raise fastapi.HTTPException(status.HTTP_403_FORBIDDEN, detail=message)
        else:
            raise fastapi.HTTPException(status.HTTP_401_UNAUTHORIZED, detail=message)

    return models.tokens_v_0_3.CreateTokenResponse.from_cognito_tokens(tokens)


@router.post(
    "/tokens/refresh",
    status_code=status.HTTP_201_CREATED,
    responses={
        status.HTTP_201_CREATED: {
            "model": models.tokens_v_0_3.RefreshTokenResponse,
            "description": "Access token with expiration date",
        },
        status.HTTP_404_NOT_FOUND: {
            "model": models.error.HTTPError,
            "description": "User not found",
        },
    },
    tags=["tokens"],
    summary="Refresh an access token",
)
async def refresh_token(
    token_request: models.tokens_v_0_3.RefreshTokenRequest = fastapi.Body(
        ..., description="User name and refresh token"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.tokens_v_0_3.RefreshTokenResponse:
    """Refresh an access token."""
    with db.session():
        user = db.get_one(
            orm_model_type=database.user.User,
            constraints={database.user.User.preferred_name: token_request.username},
        )
        credentials = token_request.to_cognito_credentials(user)

    tokens = cognito.tokens.get_tokens(credentials)
    return models.tokens_v_0_3.RefreshTokenResponse.from_cognito_tokens(tokens)
