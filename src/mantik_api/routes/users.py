import uuid

import fastapi
import starlette.status as status

import mantik_api.aws.cognito.users as cognito
import mantik_api.database as database
import mantik_api.database.exceptions as exceptions
import mantik_api.models as models
import mantik_api.routes._parameters as _parameters
import mantik_api.routes.dependencies as dependencies
import mantik_api.routes.rbac.authorization as authorization
import mantik_api.routes.rbac.bearer as bearer
import mantik_api.service as service
import mantik_api.utils.vault as vault

router_no_auth_required = fastapi.APIRouter()
router = fastapi.APIRouter(dependencies=[fastapi.Depends(bearer.JWTBearer())])


@router_no_auth_required.get(
    "/users",
    responses={
        status.HTTP_200_OK: {
            "model": models.user.UsersGet200Response,
            "description": "OK",
        },
    },
    tags=["users"],
    summary="List of all users",
)
async def users_get(
    search_string: str
    | None = fastapi.Query(
        None, description="Search string to find users by matching user name"
    ),
    startindex: int = _parameters.start_index(),
    pagelength: int = _parameters.page_length(),
    order_by: database.user.OrderBy = _parameters.order_by(
        default=database.user.OrderBy.created
    ),
    ascending: bool = _parameters.ascending(),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.user.UsersGet200Response:
    """List of all users."""
    constraints = (
        database.user.User.preferred_name.contains(search_string)
        if search_string is not None
        else None
    )
    with db.session():
        result: database.client.main.GetAllResponse = db.get_many(
            orm_model_type=database.user.User,
            constraints=constraints,
            limit=pagelength,
            offset=startindex,
            order_by=order_by,
            ascending=ascending,
        )
        return models.user.UsersGet200Response(
            total_records=result.total_count,
            page_records=result.page_count,
            users=models.base.from_database_models(models.user.User, result.entities),
        )


@router_no_auth_required.post(
    "/users",
    status_code=status.HTTP_201_CREATED,
    responses={
        status.HTTP_201_CREATED: {
            "model": models.user.UsersGet201Response,
            "description": "Successfully created user",
        },
        status.HTTP_409_CONFLICT: {
            "model": models.error.HTTPError,
            "description": "Email already in use",
        },
        status.HTTP_424_FAILED_DEPENDENCY: {
            "model": models.error.HTTPError,
            "description": "User creation failed",
        },
    },
    tags=["users"],
    summary="Creates a new user",
)
async def users_post(
    add_user: models.user.AddUser = fastapi.Body(..., description="Creates a new user"),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.user.UsersGet201Response:
    # Same email is checked here
    with db.session():
        try:
            db.get_one(
                orm_model_type=database.user.User,
                constraints={database.user.User.email: add_user.email},
            )
            raise fastapi.HTTPException(
                status_code=status.HTTP_409_CONFLICT,
                detail="This email is already in use.",
            )
        except exceptions.NotFoundError:
            pass
    # Same name is checked by Cognito
    try:
        user_id = cognito.create_cognito_user(
            username=add_user.name, password=add_user.password, email=add_user.email
        )
    except cognito.exceptions.UserCreationFailed as e:
        raise fastapi.HTTPException(
            status_code=status.HTTP_424_FAILED_DEPENDENCY,
            detail=str(e),
        )
    except cognito.exceptions.UsernameAlreadyExists:
        raise fastapi.HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="Username is already taken.",
        )
    with db.session():
        db.add(model=add_user.to_database_model(uuid.UUID(user_id)))
    return models.user.UsersGet201Response(user_id=user_id)


@router.get(
    "/users/{userId}",
    responses={
        status.HTTP_200_OK: {
            "model": models.user.UserDetails,
            "description": "The user information",
        },
    },
    tags=["users"],
    summary="Return the information of a specific user",
)
async def users_user_id_get(
    request: bearer.ModifiedRequest,
    userId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.user.UserDetails:
    """Return the user information.

    Sensitive data are optional and only returned if a user requests their
    own information.

    """
    with db.session():
        user = db.get_one(
            orm_model_type=database.user.User,
            constraints={database.user.User.id: userId},
        )
        # If user requests their own data, return all user details
        # including sensitive data.
        if request.token.user_id == userId:
            return models.user.UserDetails.from_database_model(
                user, include_sensitive_data=True
            )
        return models.user.UserDetails.from_database_model(
            user, include_sensitive_data=False
        )


@router.put(
    "/users/{userId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {"description": "Successfully updated user"},
    },
    tags=["users"],
    summary="Update user info",
)
@authorization.reject_unauthorized_user
async def users_user_id_put(
    request: bearer.ModifiedRequest,
    userId: uuid.UUID,
    updated_user: models.user.UpdateUser = fastapi.Body(
        None, description="Updated user info"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        old_user = db.get_one(
            orm_model_type=database.user.User,
            constraints={database.user.User.id: userId},
        )

        db.overwrite(
            old_user,
            fields_to_overwrite=updated_user.to_overwrite_kwargs(
                client=db, user=old_user
            ),
        )


@router.delete(
    "/users/{userId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {"description": "Successfully deleted user"},
        status.HTTP_409_CONFLICT: {
            "model": models.error.HTTPError,
            "description": "Deleting failed",
        },
    },
    tags=["users"],
    summary="Delete user",
)
@authorization.reject_unauthorized_user
async def users_user_id_delete(
    request: bearer.ModifiedRequest,
    userId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        try:
            db.delete(
                database.connection.Connection,
                constraints={database.connection.Connection.user_id: userId},
            )
            db.delete(
                database.user.User,
                constraints={database.user.User.id: userId},
            )
        except database.exceptions.ReferencedResourceExistsError as e:
            raise database.exceptions.ReferencedResourceExistsError(
                "Owned projects, groups and organizations must be deleted "
                f"or transferred to another user: {str(e)}"
            )

        try:
            cognito.delete_cognito_user(request.token)
        except cognito.exceptions.UserDeletionFailedException as e:
            raise fastapi.HTTPException(
                status_code=status.HTTP_409_CONFLICT,
                detail=str(e),
            )
        service.invitations.delete_related_invitations(
            db=db, model=database.user.User, model_id=userId
        )


@router.get(
    "/users/{userId}/settings",
    responses={
        status.HTTP_200_OK: {
            "model": models.user_settings.UserSettings,
            "description": "OK",
        },
    },
    tags=["users"],
    summary="Returns the settings for a specific user",
)
@authorization.reject_unauthorized_user
async def users_user_id_settings_get(
    request: bearer.ModifiedRequest,
    userId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.user_settings.UserSettings:
    with db.session():
        user = db.get_one(
            orm_model_type=database.user.User,
            constraints={database.user.User.id: userId},
            select_in_loading={database.user.User.connections: {}},
        )
        return models.user_settings.UserSettings.from_database_model(user)


@router.get(
    "/users/{userId}/groups",
    responses={
        status.HTTP_200_OK: {
            "model": models.user_group.GroupsGet200Response,
            "description": "Get all user groups of user",
        },
    },
    tags=["users"],
    summary="Get all User groups of user",
)
@authorization.reject_unauthorized_user
async def users_user_id_groups_get(
    request: bearer.ModifiedRequest,
    userId: uuid.UUID,
    startindex: int = _parameters.start_index(),
    pagelength: int = _parameters.page_length(),
    order_by: database.user_group.OrderBy = _parameters.order_by(
        default=database.user_group.OrderBy.created,
    ),
    ascending: bool = _parameters.ascending(),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.user_group.GroupsGet200Response:
    with db.session():
        request_user = db.get_one(
            orm_model_type=database.user.User,
            constraints={database.user.User.id: userId},
            select_in_loading={
                database.user.User.groups: {},
                database.user.User.owned_groups: {},
            },
        )
        result: database.client.main.GetAllResponse = db.get_many(
            orm_model_type=database.user_group.UserGroup,
            constraints=database.user_group.UserGroup.create_constraint_for_user_groups(
                request_user
            ),
            limit=pagelength,
            offset=startindex,
            order_by=order_by,
            ascending=ascending,
        )
        return models.user_group.GroupsGet200Response(
            total_records=result.total_count,
            page_records=result.page_count,
            user_groups=models.base.from_database_models(
                model=models.user_group.UserGroup, database_models=result.entities
            ),
        )


@router.get(
    "/users/{userId}/organizations",
    responses={
        status.HTTP_200_OK: {
            "model": models.organization.OrganizationsGet200Response,
            "description": "Get all user organizations of user",
        },
    },
    tags=["users"],
    summary="Get all user organizations of user",
)
@authorization.reject_unauthorized_user
async def users_user_id_organizations_get(
    request: bearer.ModifiedRequest,
    userId: uuid.UUID,
    startindex: int = _parameters.start_index(),
    pagelength: int = _parameters.page_length(),
    order_by: database.organization.OrderBy = _parameters.order_by(
        default=database.organization.OrderBy.created,
    ),
    ascending: bool = _parameters.ascending(),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.organization.OrganizationsGet200Response:
    with db.session():
        request_user = db.get_one(
            orm_model_type=database.user.User,
            constraints={database.user.User.id: userId},
            select_in_loading={
                database.user.User.groups: {},
                database.user.User.owned_groups: {},
                database.user.User.organizations: {},
                database.user.User.owned_organizations: {},
            },
        )
        result: database.client.main.GetAllResponse = db.get_many(
            orm_model_type=database.organization.Organization,
            constraints=database.organization.Organization.create_constraint_for_user_organizations(  # noqa: E501
                request_user
            ),
            limit=pagelength,
            offset=startindex,
            order_by=order_by,
            ascending=ascending,
        )
        return models.organization.OrganizationsGet200Response(
            total_records=result.total_count,
            page_records=result.page_count,
            organizations=models.base.from_database_models(
                model=models.organization.Organization,
                database_models=result.entities,
            ),
        )


@router.post(
    "/users/{userId}/password",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {"description": "Successfully changed password"},
        status.HTTP_400_BAD_REQUEST: {
            "model": models.error.HTTPError,
            "description": "Changing password failed",
        },
        status.HTTP_422_UNPROCESSABLE_ENTITY: {
            "model": models.error.HTTPError,
            "description": "New password must be different from old password",
        },
    },
    tags=["users"],
    summary="Change the user password",
)
@authorization.reject_unauthorized_user
async def users_user_id_password_post(
    request: bearer.ModifiedRequest,
    userId: uuid.UUID,
    update_password: models.user.ChangePassword = fastapi.Body(
        ..., description="Contains the old and new user passwords"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    if update_password.old_password == update_password.new_password:
        raise fastapi.HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail="New password must be different from old password",
        )
    try:
        cognito.change_password_cognito_user(
            old_password=update_password.old_password,
            new_password=update_password.new_password,
            token=request.token,
        )
    except cognito.exceptions.UserPasswordUpdateFailedException as e:
        raise fastapi.HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=str(e)
        )


@router.get(
    "/users/{userId}/settings/connections",
    responses={
        status.HTTP_200_OK: {
            "model": models.connection.UsersUserIdSettingsConnectionsGet200Response,
            "description": "All user connections",
        },
    },
    tags=["user connections"],
    summary="Get all user connections",
)
@authorization.reject_unauthorized_user
async def users_user_id_settings_connections_get(
    request: bearer.ModifiedRequest,
    userId: uuid.UUID,
    startindex: int = _parameters.start_index(),
    pagelength: int = _parameters.page_length(),
    order_by: database.connection.OrderBy = _parameters.order_by(
        default=database.connection.OrderBy.created,
    ),
    ascending: bool = _parameters.ascending(),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.connection.UsersUserIdSettingsConnectionsGet200Response:
    with db.session():
        result: database.client.main.GetAllResponse = db.get_many(
            orm_model_type=database.connection.Connection,
            constraints={database.connection.Connection.user_id: userId},
            offset=startindex,
            limit=pagelength,
            order_by=order_by,
            ascending=ascending,
        )
        connections = (
            models.connection.Connection.from_database_model_with_credentials(
                connection, token=request.token
            )
            for connection in result.entities
        )
        return models.connection.UsersUserIdSettingsConnectionsGet200Response(
            total_records=result.total_count,
            page_records=result.page_count,
            connections=connections,
        )


@router.post(
    "/users/{userId}/settings/connections",
    status_code=status.HTTP_201_CREATED,
    responses={
        status.HTTP_201_CREATED: {
            "model": models.connection.UsersUserIdSettingsConnectionsPost201Response,
            "description": "Successfully added connection",
        },
    },
    tags=["user connections"],
    summary="Adds user connection",
)
@authorization.reject_unauthorized_user
async def users_user_id_settings_connections_post(
    request: bearer.ModifiedRequest,
    userId: uuid.UUID,
    add_connection: models.connection.AddConnection = fastapi.Body(
        ..., description="Add user connection"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.connection.UsersUserIdSettingsConnectionsPost201Response:
    new_connection_id = uuid.uuid4()
    with db.session():
        db.add(add_connection.to_database_model(new_connection_id, user_id=userId))
        vault.create_or_update_credential(
            connection_id=new_connection_id,
            credentials=add_connection.to_vault_credentials(),
            token=request.token,
        )
    return models.connection.UsersUserIdSettingsConnectionsPost201Response(
        connection_id=new_connection_id
    )


@router.get(
    "/users/{userId}/settings/connections/{connectionId}",
    responses={
        status.HTTP_200_OK: {
            "model": models.connection.Connection,
            "description": "User connection",
        },
    },
    tags=["user connections"],
    summary="Get user connection",
)
@authorization.reject_unauthorized_user
async def users_user_id_settings_connections_connection_id_get(
    request: bearer.ModifiedRequest,
    userId: uuid.UUID,
    connectionId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.connection.Connection:
    with db.session():
        connection = db.get_one(
            orm_model_type=database.connection.Connection,
            constraints={
                database.connection.Connection.id: connectionId,
                database.connection.Connection.user_id: userId,
            },
            select_in_loading={database.connection.Connection.user: {}},
        )
        return models.connection.Connection.from_database_model_with_credentials(
            connection, token=request.token
        )


@router.put(
    "/users/{userId}/settings/connections/{connectionId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {"description": "Successfully updated connection"},
    },
    tags=["user connections"],
    summary="Updated user connection",
)
@authorization.reject_unauthorized_user
async def users_user_id_settings_connections_connection_id_put(
    request: bearer.ModifiedRequest,
    userId: uuid.UUID,
    connectionId: uuid.UUID,
    add_connection: models.connection.AddConnection = fastapi.Body(
        ..., description="Updated user connection"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        db.update(
            add_connection.to_database_model(user_id=userId, _uuid=connectionId),
            constraints={
                database.connection.Connection.id: connectionId,
                database.connection.Connection.user_id: userId,
            },
        )
        vault.create_or_update_credential(
            connection_id=connectionId,
            credentials=add_connection.to_vault_credentials(),
            token=request.token,
        )


@router.delete(
    "/users/{userId}/settings/connections/{connectionId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {"description": "Successfully deleted connection"},
    },
    tags=["user connections"],
    summary="Delete user connection",
)
@authorization.reject_unauthorized_user
async def users_user_id_settings_connections_connection_id_delete(
    request: bearer.ModifiedRequest,
    userId: uuid.UUID,
    connectionId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        db.delete(
            database.connection.Connection,
            constraints={database.connection.Connection.id: connectionId},
        )
        vault.delete_credential(connection_id=connectionId, token=request.token)


@router.put(
    "/users/{userId}/settings/paymentInfos",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully updated payment info"
        },
    },
    tags=["users"],
    summary="Updates payment info",
)
@authorization.reject_unauthorized_user
async def users_user_id_settings_payment_infos_put(
    request: bearer.ModifiedRequest,
    userId: uuid.UUID,
    add_payment_info: models.payment_info.AddPaymentInfo = fastapi.Body(
        ..., description="Updates payment info"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        db.update(
            add_payment_info.to_database_model(
                user_id=userId,
            ),
            constraints={database.user.User.id: userId},
        )


@router.delete(
    "/users/{userId}/settings/paymentInfos",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully deleted payment info"
        },
    },
    tags=["users"],
    summary="Delete payment info",
)
@authorization.reject_unauthorized_user
async def users_user_id_settings_payment_infos_delete(
    request: bearer.ModifiedRequest,
    userId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        user = db.get_one(
            database.user.User,
            constraints={database.user.User.id: userId},
        )
        db.update(
            user,
            constraints={database.user.User.id: userId},
            fields_to_update={
                "address": None,
                "payment_method": None,
                "payment_details": None,
            },
        )
