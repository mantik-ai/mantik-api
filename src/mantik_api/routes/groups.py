import uuid

import fastapi
import starlette.status as status

import mantik_api.aws.cognito.exceptions as exceptions
import mantik_api.database as database
import mantik_api.models as models
import mantik_api.routes._parameters as _parameters
import mantik_api.routes.dependencies as dependencies
import mantik_api.routes.rbac.bearer as bearer
import mantik_api.service as service

router = fastapi.APIRouter(dependencies=[fastapi.Depends(bearer.JWTBearer())])


@router.get(
    "/groups",
    responses={
        status.HTTP_200_OK: {
            "model": models.user_group.GroupsGet200Response,
            "description": "Get all user groups",
        },
    },
    tags=["groups"],
    summary="Get all User group",
)
async def groups_get(
    startindex: int = _parameters.start_index(),
    pagelength: int = _parameters.page_length(),
    order_by: database.user_group.OrderBy = _parameters.order_by(
        default=database.user_group.OrderBy.created,
    ),
    ascending: bool = _parameters.ascending(),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.user_group.GroupsGet200Response:
    """List of all user groups."""
    with db.session():
        result: database.client.main.GetAllResponse = db.get_many(
            orm_model_type=database.user_group.UserGroup,
            limit=pagelength,
            offset=startindex,
            order_by=order_by,
            ascending=ascending,
        )

        return models.user_group.GroupsGet200Response(
            total_records=result.total_count,
            page_records=result.page_count,
            user_groups=models.base.from_database_models(
                models.user_group.UserGroup, result.entities
            ),
        )


@router.get(
    "/groups/{groupId}",
    responses={
        status.HTTP_200_OK: {
            "model": models.user_group.UserGroup,
            "description": "Get group information",
        },
    },
    tags=["groups"],
    summary="Get Group information",
)
async def groups_group_id_get(
    groupId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.user_group.UserGroup:
    with db.session():
        user_group = db.get_one(
            orm_model_type=database.user_group.UserGroup,
            constraints={database.user_group.UserGroup.id: groupId},
            select_in_loading={
                database.user_group.UserGroup.members: {},
                database.user_group.UserGroup.admin: {},
            },
        )
        return models.user_group.UserGroup.from_database_model(user_group)


@router.put(
    "/groups/{groupId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {"description": "Successfully updated user group"},
    },
    tags=["groups"],
    summary="Update group",
)
async def groups_group_id_put(
    request: bearer.ModifiedRequest,
    groupId: uuid.UUID,
    updated_user_group_payload: models.user_group.UpdateUserGroup = fastapi.Body(
        ..., description="Updated user group information"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    caller_user_id = request.token.user_id
    with db.session():
        existing_user_group = db.get_one(
            orm_model_type=database.user_group.UserGroup,
            constraints={database.user_group.UserGroup.id: groupId},
            select_in_loading={database.user_group.UserGroup.members: {}},
        )
        if existing_user_group.admin_id != caller_user_id:
            raise exceptions.AuthenticationFailedException(
                "Only current Group's Admin can modify the Group information."
            )
        db.overwrite(
            existing_user_group,
            fields_to_overwrite=updated_user_group_payload.to_overwrite_kwargs(db),
        )


@router.post(
    "/groups",
    status_code=status.HTTP_201_CREATED,
    responses={
        status.HTTP_201_CREATED: {
            "model": models.user_group.GroupsGet201Response,
            "description": "Created",
        },
    },
    tags=["groups"],
    summary="Create a new user group",
)
async def groups_post(
    request: bearer.ModifiedRequest,
    new_user_group_payload: models.user_group.AddUserGroup = fastapi.Body(
        ...,
        description="Create user group. The user creating the group becomes the admin.",
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.user_group.GroupsGet201Response:
    caller_user_id = request.token.user_id
    with db.session():
        admin: database.user.User = database.user.get_user_by_id(
            db, user_id=caller_user_id
        )

        new_group_id = uuid.uuid4()
        db.add(
            model=new_user_group_payload.to_database_model(
                user_group_id=new_group_id, admin=admin
            )
        )

    return models.user_group.GroupsGet201Response(group_id=new_group_id)


@router.delete(
    "/groups/{groupId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {"description": "Successfully deleted user group"},
        status.HTTP_401_UNAUTHORIZED: {
            "model": models.error.HTTPError,
            "description": "Only user group admin can delete the group",
        },
    },
    tags=["groups"],
    summary="Delete a user group",
    description=(
        "Deleting a user group will implicitly remove all access rights"
        "of all members to the user group."
        "The deletion of a user group is only permitted to admin of the user group."
    ),
)
async def groups_group_id_delete(
    request: bearer.ModifiedRequest,
    groupId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        group = db.get_one(
            database.user_group.UserGroup,
            constraints={database.user_group.UserGroup.id: groupId},
        )
        if not _called_by_group_admin(request, group=group):
            raise exceptions.AuthenticationFailedException(
                "Only user group admin can delete the group"
            )
        db.delete(group)
        service.invitations.delete_related_invitations(
            db=db, model=database.user_group.UserGroup, model_id=groupId
        )


@router.delete(
    "/groups/{groupId}/members/{userId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully removed member from user group"
        },
        status.HTTP_401_UNAUTHORIZED: {
            "model": models.error.HTTPError,
            "description": (
                "Member can only be removed by the "
                "user group admin or the respective member"
            ),
        },
        status.HTTP_422_UNPROCESSABLE_ENTITY: {
            "model": models.error.HTTPError,
            "description": "User group admin cannot be removed",
        },
    },
    tags=["groups"],
    summary="Remove member from user group",
    description=(
        "Removing a member from the user group will remove all access rights"
        "of the member to the user group."
        "The removal of members is only permitted to the member themselves"
        "or the admin of the user group."
        "The Admin cannot be removed from the group."
    ),
)
async def groups_group_member_id_delete(
    request: bearer.ModifiedRequest,
    groupId: uuid.UUID,
    userId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        group = db.get_one(
            database.user_group.UserGroup,
            constraints={database.user_group.UserGroup.id: groupId},
            select_in_loading={database.user_group.UserGroup.members: {}},
        )
        if not _called_by_member(
            request, user_id=userId
        ) and not _called_by_group_admin(request, group=group):
            raise exceptions.AuthenticationFailedException(
                "Only the member themselves or the admin can "
                "remove the member from the user group"
            )
        if userId == group.admin_id:
            raise fastapi.HTTPException(
                status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                detail="The group admin cannot be removed from the group",
            )
        db.delete(
            database.user_group.UserGroupUserAssociationTable,
            constraints={
                database.user_group.UserGroupUserAssociationTable.user_group_id: groupId,  # noqa: E501
                database.user_group.UserGroupUserAssociationTable.user_id: userId,  # noqa: E501
            },
        )


def _called_by_member(request: bearer.ModifiedRequest, user_id: uuid.UUID) -> bool:
    return request.token.user_id == user_id


def _called_by_group_admin(
    request: bearer.ModifiedRequest, group: database.user_group.UserGroup
) -> bool:
    return request.token.user_id == group.admin_id
