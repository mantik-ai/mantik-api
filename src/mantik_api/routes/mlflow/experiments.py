import uuid

import fastapi
import starlette.status as status

import mantik_api.database as database
import mantik_api.models.mlflow.experiments as models
import mantik_api.routes.dependencies as dependencies
import mantik_api.routes.rbac.bearer as bearer

EXPERIMENTS_PATH = "/2.0/mlflow/experiments/"

router = fastapi.APIRouter(dependencies=[fastapi.Depends(bearer.JWTBearer())])


@router.post(
    "/mlflow/permissions/experiments",
    responses={
        status.HTTP_200_OK: {
            "model": models.ExperimentPermissionResponse,
            "description": "MLflow endpoint permissions",
        },
    },
    tags=["mlflow"],
    summary="Ask for permission to MLflow API call",
)
async def mlflow_permissions_experiments_post(
    request: bearer.ModifiedRequest,
    experiment_permission_request: models.ExperimentPermissionRequest = fastapi.Body(
        ..., description="MLflow API call request body"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.ExperimentPermissionResponse:
    if EXPERIMENTS_PATH not in experiment_permission_request.endpoint:
        return models.ExperimentPermissionResponse(
            allowed=False,
            message="Mlflow endpoint does not seem like a experiment endpoint.",
        )
    # Match on the last value in the experiment endpoint path
    match experiment_permission_request.endpoint.split("/")[-1]:
        case "create":
            # Since we do not have the project id
            # we need to enforce the creation of an experiment in Mantik.
            return models.ExperimentPermissionResponse(
                allowed=False,
                message="Experiments can only be created on the Mantik platform.",
            )
        case "search":
            with db.session():
                return models.ExperimentPermissionResponse(
                    allowed=True,
                    message="Allowed experiments are passed "
                    "in the experimentIds field.",
                    experiment_ids=_get_experiment_attribute_from_user_accessible_projects(  # noqa: E501
                        db=db, user_id=request.token.user_id
                    ),
                )
        case "get":
            if experiment_permission_request.experiment_id is None:
                return models.ExperimentPermissionResponse(
                    allowed=False,
                    message="Experiment ID missing "
                    "from the experiment permission request.",
                )
            with db.session():
                if int(
                    experiment_permission_request.experiment_id
                ) in _get_experiment_attribute_from_user_accessible_projects(
                    db=db, user_id=request.token.user_id
                ):
                    return models.ExperimentPermissionResponse(
                        allowed=True,
                        message="User is allowed to see the experiment with ID "
                        f"{experiment_permission_request.experiment_id}.",
                    )
                return models.ExperimentPermissionResponse(
                    allowed=False,
                    message=f"User is not allowed to see the experiment with ID "
                    f"{experiment_permission_request.experiment_id}.",
                )
        case "get-by-name":
            if experiment_permission_request.experiment_name is None:
                return models.ExperimentPermissionResponse(
                    allowed=False,
                    message="Experiment name missing "
                    "from the experiment permission request.",
                )
            with db.session():
                if (
                    experiment_permission_request.experiment_name
                    in _get_experiment_attribute_from_user_accessible_projects(
                        db=db,
                        user_id=request.token.user_id,
                        experiment_attribute="name",
                    )
                ):
                    return models.ExperimentPermissionResponse(
                        allowed=True,
                        message="User is allowed to see the experiment with name "
                        f"{experiment_permission_request.experiment_name}.",
                    )
                return models.ExperimentPermissionResponse(
                    allowed=False,
                    message=f"User is not allowed to see the experiment with name "
                    f"{experiment_permission_request.experiment_name}.",
                )
        case "delete":
            # Since we do not have the project ID
            # we need to enforce the deletion of an experiment in Mantik.
            return models.ExperimentPermissionResponse(
                allowed=False,
                message="Experiments can only be deleted on the Mantik platform.",
            )
        case "restore":
            # NOTE: We need to decide whether
            # it can be possible to restore experiments.
            return models.ExperimentPermissionResponse(
                allowed=False,
                message="Restoring an experiment is not yet possible in Mantik.",
            )
        case "update":
            # We need to enforce experiment's updates in Mantik.
            return models.ExperimentPermissionResponse(
                allowed=False,
                message="Experiments can only be updated on the Mantik platform.",
            )
        case "set-experiment-tag":
            # Use Mantik labels instead.
            return models.ExperimentPermissionResponse(
                allowed=False,
                message="MLflow tags are not supported, use Mantik labels instead.",
            )
        case _:
            return models.ExperimentPermissionResponse(
                allowed=False,
                message="Not yet supported by Mantik.",
            )


def _get_experiment_attribute_from_user_accessible_projects(
    db: database.client.main.Client,
    user_id: uuid.UUID,
    experiment_attribute: str = "mlflow_experiment_id",
):
    user = db.get_one(
        orm_model_type=database.user.User,
        constraints={database.user.User.id: user_id},
        select_in_loading={
            database.user.User.groups: {},
            database.user.User.owned_groups: {},
            database.user.User.organizations: {},
            database.user.User.owned_organizations: {},
        },
    )
    projects = db.get_all(
        select_in_loading={
            database.project.Project.experiment_repositories: {},
        },
        orm_model_type=database.project.Project,
        constraints=database.project.Project.create_constraint_for_user_accessible_projects(  # noqa: E501
            user_=user
        ),
    )
    return (
        getattr(experiment, experiment_attribute)
        for project in projects.entities
        for experiment in project.experiment_repositories
    )
