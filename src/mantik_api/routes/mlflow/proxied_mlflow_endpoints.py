from mantik_api.routes.mlflow.tracking_server_router import AJAX_API_PREFIX
from mantik_api.routes.mlflow.tracking_server_router import REST_API_PREFIX

runs_endpoints = [
    # rest-api
    (f"{REST_API_PREFIX}/2.0/mlflow/runs/create", "POST"),
    (f"{REST_API_PREFIX}/2.0/mlflow/runs/delete", "POST"),
    (f"{REST_API_PREFIX}/2.0/mlflow/runs/restore", "POST"),
    (f"{REST_API_PREFIX}/2.0/mlflow/runs/get", "GET"),
    (f"{REST_API_PREFIX}/2.0/mlflow/runs/log-metric", "POST"),
    (f"{REST_API_PREFIX}/2.0/mlflow/runs/log-batch", "POST"),
    (f"{REST_API_PREFIX}/2.0/mlflow/runs/log-model", "POST"),
    (f"{REST_API_PREFIX}/2.0/mlflow/runs/log-inputs", "POST"),
    (f"{REST_API_PREFIX}/2.0/mlflow/runs/set-tag", "POST"),
    (f"{REST_API_PREFIX}/2.0/mlflow/runs/delete-tag", "POST"),
    (f"{REST_API_PREFIX}/2.0/mlflow/runs/log-parameter", "POST"),
    (f"{REST_API_PREFIX}/2.0/mlflow/runs/search", "POST"),
    (f"{REST_API_PREFIX}/2.0/mlflow/runs/update", "POST"),
    # ajax-api
    (f"{AJAX_API_PREFIX}/2.0/mlflow/runs/get", "GET"),
    (f"{AJAX_API_PREFIX}/2.0/mlflow/runs/search", "POST"),
]
experiment_endpoints = [
    # rest-api
    (f"{REST_API_PREFIX}/2.0/mlflow/experiments/create", "POST"),
    (f"{REST_API_PREFIX}/2.0/mlflow/experiments/search", "POST"),
    (f"{REST_API_PREFIX}/2.0/mlflow/experiments/search", "GET"),
    (f"{REST_API_PREFIX}/2.0/mlflow/experiments/get", "GET"),
    (f"{REST_API_PREFIX}/2.0/mlflow/experiments/get-by-name", "GET"),
    (f"{REST_API_PREFIX}/2.0/mlflow/experiments/delete", "POST"),
    (f"{REST_API_PREFIX}/2.0/mlflow/experiments/restore", "POST"),
    (f"{REST_API_PREFIX}/2.0/mlflow/experiments/update", "POST"),
    (f"{REST_API_PREFIX}/2.0/mlflow/experiments/set-experiment-tag", "POST"),
    # ajax-api
    (f"{AJAX_API_PREFIX}/2.0/mlflow/experiments/search", "POST"),
    (f"{AJAX_API_PREFIX}/2.0/mlflow/experiments/search", "GET"),
    (f"{AJAX_API_PREFIX}/2.0/mlflow/experiments/search-datasets", "POST"),
    (f"{AJAX_API_PREFIX}/2.0/mlflow/experiments/get", "GET"),
    (f"{AJAX_API_PREFIX}/2.0/mlflow/experiments/get-by-name", "GET"),
]
others = [
    # rest-api
    ("/health", "GET"),
    ("/version", "GET"),
    (f"{REST_API_PREFIX}/2.0/mlflow/artifacts/list", "GET"),
    (f"{REST_API_PREFIX}/2.0/mlflow/metrics/get-history", "GET"),
    (f"{REST_API_PREFIX}/2.0/mlflow-artifacts/artifacts", "GET"),
    (
        REST_API_PREFIX
        + "/2.0/mlflow-artifacts/artifacts/"
        + "{mlflow_experiment_id:int}/{rest_of_path:path}",
        "PUT",
    ),
    (
        REST_API_PREFIX
        + "/2.0/mlflow-artifacts/artifacts/"
        + "{mlflow_experiment_id:int}/{rest_of_path:path}",
        "GET",
    ),
    (
        REST_API_PREFIX
        + "/2.0/mlflow-artifacts/artifacts/"
        + "{mlflow_experiment_id:int}/{rest_of_path:path}",
        "POST",
    ),
    (
        REST_API_PREFIX
        + "/2.0/mlflow-artifacts/artifacts/"
        + "{mlflow_experiment_id:int}/{rest_of_path:path}",
        "DELETE",
    ),
    # ajax-api
    (f"{AJAX_API_PREFIX}/2.0/mlflow/artifacts/list", "GET"),
    (f"{AJAX_API_PREFIX}/2.0/mlflow-artifacts/artifacts", "GET"),
    (
        AJAX_API_PREFIX
        + "/2.0/mlflow-artifacts/artifacts/"
        + "{mlflow_experiment_id:int}/{rest_of_path:path}",
        "PUT",
    ),
    (
        AJAX_API_PREFIX
        + "/2.0/mlflow-artifacts/artifacts/"
        + "{mlflow_experiment_id:int}/{rest_of_path:path}",
        "GET",
    ),
    (
        AJAX_API_PREFIX
        + "/2.0/mlflow-artifacts/artifacts/"
        + "{mlflow_experiment_id:int}/{rest_of_path:path}",
        "POST",
    ),
    (
        AJAX_API_PREFIX
        + "/2.0/mlflow-artifacts/artifacts/"
        + "{mlflow_experiment_id:int}/{rest_of_path:path}",
        "DELETE",
    ),
    (f"{AJAX_API_PREFIX}/2.0/mlflow/metrics/get-history", "GET"),
    (f"{AJAX_API_PREFIX}/2.0/mlflow/metrics/get-history-bulk", "GET"),
    (f"{AJAX_API_PREFIX}/2.0/mlflow/metrics/get-history-bulk-interval", "GET"),
    (f"{AJAX_API_PREFIX}/2.0/mlflow/gateway-proxy", "GET"),
]

# MLflow endpoints which exist, but we don't expose
# because we have our own model registry:
# models_endpoints = [
#     "/2.0/mlflow/registered-models/create",
#     "/2.0/mlflow/registered-models/get",
#     "/2.0/mlflow/registered-models/rename",
#     "/2.0/mlflow/registered-models/update",
#     "/2.0/mlflow/registered-models/delete",
#     "/2.0/mlflow/registered-models/get-latest-versions",
#     "/2.0/mlflow/model-versions/create",
#     "/2.0/mlflow/model-versions/get",
#     "/2.0/mlflow/model-versions/update",
#     "/2.0/mlflow/model-versions/delete",
#     "/2.0/mlflow/model-versions/search",
#     "/2.0/mlflow/model-versions/get-download-uri",
#     "/2.0/mlflow/model-versions/transition-stage",
#     "/2.0/mlflow/registered-models/search",
#     "/2.0/mlflow/registered-models/set-tag",
#     "/2.0/mlflow/model-versions/set-tag",
#     "/2.0/mlflow/registered-models/delete-tag",
#     "/2.0/mlflow/model-versions/delete-tag",
#     "/2.0/mlflow/registered-models/alias",  # DELETE
#     "/2.0/mlflow/registered-models/alias",  # GET
#     "/2.0/mlflow/registered-models/alias",  # POST
# ]


endpoints_to_proxy = runs_endpoints + experiment_endpoints + others
