import fastapi
import requests

import mantik_api.database as database
import mantik_api.routes.dependencies as dependencies
import mantik_api.routes.mlflow.utils as utils
import mantik_api.routes.rbac.authorization as authorization
from mantik_api.routes.mlflow.tracking_server_router import AJAX_API_PREFIX
from mantik_api.routes.mlflow.tracking_server_router import get_tracking_server_url
from mantik_api.routes.mlflow.tracking_server_router import REST_API_PREFIX
from mantik_api.routes.mlflow.tracking_server_router import router


@router.get("/health")
async def health(request: fastapi.Request):
    url = f"{get_tracking_server_url()}/health"

    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.get("/version")
async def version(request: fastapi.Request):
    url = f"{get_tracking_server_url()}/version"

    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.get("/get-artifact")
@utils.mlflow_rbac_decorator(
    project_finder_function=utils.ProjectFetcherFunction.from_mlflow_run_id,
    allowed_roles=authorization.ALL_ROLES,
    allowed_for_public_projects=False,
)
async def get_artifact(
    request: fastapi.Request,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = f"{get_tracking_server_url()}/get-artifact"
    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        headers=request.headers,
    )

    return fastapi.Response(content=response.content, status_code=response.status_code)


@router.get(f"{AJAX_API_PREFIX}/2.0/mlflow/gateway-proxy")
async def gateway_proxy(request: fastapi.Request):
    """We do not use any proxies in MLFlow currently and therefore can
    simply return an empty payload to satisfy the MLFlow UI
    """

    payload = {"endpoints": []}
    return fastapi.responses.JSONResponse(payload)


@router.get(f"{REST_API_PREFIX}/2.0/mlflow/artifacts/list")
@utils.mlflow_rbac_decorator(
    project_finder_function=utils.ProjectFetcherFunction.from_mlflow_run_id,
    allowed_roles=authorization.ALL_ROLES,
    allowed_for_public_projects=True,
)
async def artifacts_list(
    request: fastapi.Request,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = f"{get_tracking_server_url()}{REST_API_PREFIX}/2.0/mlflow/artifacts/list"
    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.get(f"{AJAX_API_PREFIX}/2.0/mlflow/artifacts/list")
@utils.mlflow_rbac_decorator(
    project_finder_function=utils.ProjectFetcherFunction.from_mlflow_run_id,
    allowed_roles=authorization.ALL_ROLES,
    allowed_for_public_projects=True,
)
async def ajax_api_artifacts_list(
    request: fastapi.Request,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = f"{get_tracking_server_url()}{AJAX_API_PREFIX}/2.0/mlflow/artifacts/list"

    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.get(f"{REST_API_PREFIX}/2.0/mlflow-artifacts/artifacts")
@utils.mlflow_rbac_decorator(
    project_finder_function=utils.ProjectFetcherFunction.from_mlflow_artifact_path,
    allowed_roles=authorization.ALL_ROLES,
    allowed_for_public_projects=False,
)
async def mlflow_artifacts_list(
    request: fastapi.Request,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = f"{get_tracking_server_url()}{REST_API_PREFIX}/2.0/mlflow-artifacts/artifacts"
    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.get(f"{AJAX_API_PREFIX}/2.0/mlflow-artifacts/artifacts")
@utils.mlflow_rbac_decorator(
    project_finder_function=utils.ProjectFetcherFunction.from_mlflow_artifact_path,
    allowed_roles=authorization.ALL_ROLES,
    allowed_for_public_projects=False,
)
async def mlflow_artifacts_list_ajax(
    request: fastapi.Request,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = f"{get_tracking_server_url()}{AJAX_API_PREFIX}/2.0/mlflow-artifacts/artifacts"
    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.put(
    REST_API_PREFIX
    + "/2.0/mlflow-artifacts/artifacts/{mlflow_experiment_id:int}/{rest_of_path:path}"
)
@utils.mlflow_path_rbac_decorator(
    project_finder_function=utils.ProjectFetcherFunction.from_mlflow_experiment_id,
    allowed_roles=authorization.ALL_ROLES,
    allowed_for_public_projects=False,
)
async def mlflow_artifact_update(
    request: fastapi.Request,
    mlflow_experiment_id: int,
    rest_of_path: str,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    return await artifacts_rest_api_endpoint(
        mlflow_experiment_id=mlflow_experiment_id,
        rest_of_path=rest_of_path,
        request=request,
    )


@router.put(
    AJAX_API_PREFIX
    + "/2.0/mlflow-artifacts/artifacts/{mlflow_experiment_id:int}/{rest_of_path:path}"
)
@utils.mlflow_path_rbac_decorator(
    project_finder_function=utils.ProjectFetcherFunction.from_mlflow_experiment_id,
    allowed_roles=authorization.ALL_ROLES,
    allowed_for_public_projects=False,
)
async def mlflow_artifact_update_ajax(
    request: fastapi.Request,
    mlflow_experiment_id: int,
    rest_of_path: str,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    return await artifacts_rest_api_endpoint(
        mlflow_experiment_id=mlflow_experiment_id,
        rest_of_path=rest_of_path,
        request=request,
    )


@router.get(
    REST_API_PREFIX
    + "/2.0/mlflow-artifacts/artifacts/{mlflow_experiment_id:int}/{rest_of_path:path}"
)
@utils.mlflow_path_rbac_decorator(
    project_finder_function=utils.ProjectFetcherFunction.from_mlflow_experiment_id,
    allowed_roles=authorization.ALL_ROLES,
    allowed_for_public_projects=False,
)
async def mlflow_artifact_get(
    request: fastapi.Request,
    mlflow_experiment_id: int,
    rest_of_path: str,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    return await artifacts_rest_api_endpoint(
        mlflow_experiment_id=mlflow_experiment_id,
        rest_of_path=rest_of_path,
        request=request,
    )


@router.get(
    AJAX_API_PREFIX
    + "/2.0/mlflow-artifacts/artifacts/{mlflow_experiment_id:int}/{rest_of_path:path}"
)
@utils.mlflow_path_rbac_decorator(
    project_finder_function=utils.ProjectFetcherFunction.from_mlflow_experiment_id,
    allowed_roles=authorization.ALL_ROLES,
    allowed_for_public_projects=False,
)
async def mlflow_artifact_get_ajax(
    request: fastapi.Request,
    mlflow_experiment_id: int,
    rest_of_path: str,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    return await artifacts_rest_api_endpoint(
        mlflow_experiment_id=mlflow_experiment_id,
        rest_of_path=rest_of_path,
        request=request,
    )


@router.post(
    REST_API_PREFIX
    + "/2.0/mlflow-artifacts/artifacts/{mlflow_experiment_id:int}/{rest_of_path:path}"
)
@utils.mlflow_path_rbac_decorator(
    project_finder_function=utils.ProjectFetcherFunction.from_mlflow_experiment_id,
    allowed_roles=authorization.ALL_ROLES,
    allowed_for_public_projects=False,
)
async def mlflow_artifact_post(
    request: fastapi.Request,
    mlflow_experiment_id: int,
    rest_of_path: str,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    return await artifacts_rest_api_endpoint(
        mlflow_experiment_id=mlflow_experiment_id,
        rest_of_path=rest_of_path,
        request=request,
    )


@router.post(
    AJAX_API_PREFIX
    + "/2.0/mlflow-artifacts/artifacts/{mlflow_experiment_id:int}/{rest_of_path:path}"
)
@utils.mlflow_path_rbac_decorator(
    project_finder_function=utils.ProjectFetcherFunction.from_mlflow_experiment_id,
    allowed_roles=authorization.ALL_ROLES,
    allowed_for_public_projects=False,
)
async def mlflow_artifact_post_ajax(
    request: fastapi.Request,
    mlflow_experiment_id: int,
    rest_of_path: str,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    return await artifacts_rest_api_endpoint(
        mlflow_experiment_id=mlflow_experiment_id,
        rest_of_path=rest_of_path,
        request=request,
    )


@router.delete(
    REST_API_PREFIX
    + "/2.0/mlflow-artifacts/artifacts/{mlflow_experiment_id:int}/{rest_of_path:path}"
)
@utils.mlflow_path_rbac_decorator(
    project_finder_function=utils.ProjectFetcherFunction.from_mlflow_experiment_id,
    allowed_roles=authorization.ALL_ROLES,
    allowed_for_public_projects=False,
)
async def mlflow_artifact_delete(
    request: fastapi.Request,
    mlflow_experiment_id: int,
    rest_of_path: str,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    return await artifacts_rest_api_endpoint(
        mlflow_experiment_id=mlflow_experiment_id,
        rest_of_path=rest_of_path,
        request=request,
    )


@router.delete(
    AJAX_API_PREFIX
    + "/2.0/mlflow-artifacts/artifacts/{mlflow_experiment_id:int}/{rest_of_path:path}"
)
@utils.mlflow_path_rbac_decorator(
    project_finder_function=utils.ProjectFetcherFunction.from_mlflow_experiment_id,
    allowed_roles=authorization.ALL_ROLES,
    allowed_for_public_projects=False,
)
async def mlflow_artifact_delete_ajax(
    request: fastapi.Request,
    mlflow_experiment_id: int,
    rest_of_path: str,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    return await artifacts_rest_api_endpoint(
        mlflow_experiment_id=mlflow_experiment_id,
        rest_of_path=rest_of_path,
        request=request,
    )


@router.get(f"{REST_API_PREFIX}/2.0/mlflow/metrics/get-history")
@utils.mlflow_rbac_decorator(
    project_finder_function=utils.ProjectFetcherFunction.from_mlflow_run_id,
    allowed_roles=authorization.ALL_ROLES,
    allowed_for_public_projects=False,
)
async def metrics_get_history(
    request: fastapi.Request,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = f"{get_tracking_server_url()}{REST_API_PREFIX}/2.0/mlflow/metrics/get-history"
    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.get(f"{AJAX_API_PREFIX}/2.0/mlflow/metrics/get-history")
@utils.mlflow_rbac_decorator(
    project_finder_function=utils.ProjectFetcherFunction.from_mlflow_run_id,
    allowed_roles=authorization.ALL_ROLES,
    allowed_for_public_projects=False,
)
async def ajax_api_metrics_get_history(
    request: fastapi.Request,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = f"{get_tracking_server_url()}{AJAX_API_PREFIX}/2.0/mlflow/metrics/get-history"

    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.get(f"{AJAX_API_PREFIX}/2.0/mlflow/metrics/get-history-bulk")
@utils.mlflow_rbac_decorator(
    project_finder_function=utils.ProjectFetcherFunction.from_mlflow_run_id,
    allowed_roles=authorization.ALL_ROLES,
    allowed_for_public_projects=False,
)
async def ajax_api_metrics_get_history_bulk(
    request: fastapi.Request,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = (
        f"{get_tracking_server_url()}{AJAX_API_PREFIX}"
        f"/2.0/mlflow/metrics/get-history-bulk"
    )
    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.get(f"{AJAX_API_PREFIX}/2.0/mlflow/metrics/get-history-bulk-interval")
@utils.mlflow_rbac_decorator(
    project_finder_function=utils.ProjectFetcherFunction.from_mlflow_run_id,
    allowed_roles=authorization.ALL_ROLES,
    allowed_for_public_projects=False,
)
async def ajax_api_metrics_get_history_bulk_interval(
    request: fastapi.Request,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = (
        f"{get_tracking_server_url()}{AJAX_API_PREFIX}"
        f"/2.0/mlflow/metrics/get-history-bulk-interval"
    )
    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


async def artifacts_rest_api_endpoint(mlflow_experiment_id, rest_of_path, request):
    url = (
        f"{get_tracking_server_url()}{AJAX_API_PREFIX}/2.0/"
        f"mlflow-artifacts/artifacts/{mlflow_experiment_id}/{rest_of_path}"
    )
    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        headers=request.headers,
        data=await request.body(),
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)
