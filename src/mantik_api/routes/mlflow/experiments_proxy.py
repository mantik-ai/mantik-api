import json

import fastapi
import requests

import mantik_api.database as database
import mantik_api.routes.dependencies as dependencies
import mantik_api.routes.mlflow.utils as mlflow_utils
import mantik_api.routes.rbac.authorization as authorization
from mantik_api.routes.mlflow.tracking_server_router import AJAX_API_PREFIX
from mantik_api.routes.mlflow.tracking_server_router import get_tracking_server_url
from mantik_api.routes.mlflow.tracking_server_router import REST_API_PREFIX
from mantik_api.routes.mlflow.tracking_server_router import router

EXPERIMENT_ATTRIBUTE = "mlflow_experiment_id"


@router.post(f"{REST_API_PREFIX}/2.0/mlflow/experiments/create")
async def experiments_create(
    request: fastapi.Request,
):
    """
    No check required for this call, since MLFlow has no concept of projects.
    """
    url = f"{get_tracking_server_url()}{REST_API_PREFIX}/2.0/mlflow/experiments/create"
    try:
        json_body = await request.json()
    except json.JSONDecodeError:
        json_body = None

    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        json=json_body,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.post(f"{REST_API_PREFIX}/2.0/mlflow/experiments/search")
async def experiments_search(
    request: fastapi.Request,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    return await _experiments_search(request=request, db=db, url_prefix=REST_API_PREFIX)


@router.get(f"{REST_API_PREFIX}/2.0/mlflow/experiments/search")
async def experiments_search_get(
    request: fastapi.Request,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    return await _experiments_search(request=request, db=db, url_prefix=REST_API_PREFIX)


@router.get(f"{REST_API_PREFIX}/2.0/mlflow/experiments/get")
@mlflow_utils.mlflow_rbac_decorator(
    project_finder_function=mlflow_utils.ProjectFetcherFunction.from_mlflow_experiment_id,  # noqa E501
    allowed_roles=authorization.ALL_ROLES,
    allowed_for_public_projects=True,
)
async def experiments_get(
    request: fastapi.Request,
    json_body: dict | None = None,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = f"{get_tracking_server_url()}{REST_API_PREFIX}/2.0/mlflow/experiments/get"

    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        json=json_body,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.get(f"{REST_API_PREFIX}/2.0/mlflow/experiments/get-by-name")
@mlflow_utils.mlflow_rbac_decorator(
    project_finder_function=mlflow_utils.ProjectFetcherFunction.from_mlflow_experiment_name,  # noqa E501
    allowed_roles=authorization.ALL_ROLES,
    allowed_for_public_projects=True,
)
async def experiments_get_by_name(
    request: fastapi.Request,
    json_body: dict | None = None,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = (
        f"{get_tracking_server_url()}{REST_API_PREFIX}"
        f"/2.0/mlflow/experiments/get-by-name"
    )

    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        json=json_body,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


# TODO: Use the decorator to check for access
@router.post(f"{REST_API_PREFIX}/2.0/mlflow/experiments/delete")
@mlflow_utils.mlflow_rbac_decorator(
    project_finder_function=mlflow_utils.ProjectFetcherFunction.from_mlflow_experiment_id,  # noqa E501
    allowed_roles=authorization.WORKING_ROLES,
    allowed_for_public_projects=False,
)
async def experiments_delete(
    request: fastapi.Request,
    json_body: dict | None,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = f"{get_tracking_server_url()}{REST_API_PREFIX}/2.0/mlflow/experiments/delete"

    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        json=json_body,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.post(f"{REST_API_PREFIX}/2.0/mlflow/experiments/restore")
@mlflow_utils.mlflow_rbac_decorator(
    project_finder_function=mlflow_utils.ProjectFetcherFunction.from_mlflow_experiment_id,  # noqa E501
    allowed_roles=authorization.WORKING_ROLES,
    allowed_for_public_projects=False,
)
async def experiments_restore(
    request: fastapi.Request,
    json_body: dict | None,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = f"{get_tracking_server_url()}{REST_API_PREFIX}/2.0/mlflow/experiments/restore"

    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        json=json_body,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.post(f"{REST_API_PREFIX}/2.0/mlflow/experiments/update")
@mlflow_utils.mlflow_rbac_decorator(
    project_finder_function=mlflow_utils.ProjectFetcherFunction.from_mlflow_experiment_id,  # noqa E501
    allowed_roles=authorization.WORKING_ROLES,
    allowed_for_public_projects=False,
)
async def experiments_update(
    request: fastapi.Request,
    json_body: dict | None,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = f"{get_tracking_server_url()}{REST_API_PREFIX}/2.0/mlflow/experiments/update"

    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        json=json_body,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.post(f"{REST_API_PREFIX}/2.0/mlflow/experiments/set-experiment-tag")
@mlflow_utils.mlflow_rbac_decorator(
    project_finder_function=mlflow_utils.ProjectFetcherFunction.from_mlflow_experiment_id,  # noqa E501
    allowed_roles=authorization.WORKING_ROLES,
    allowed_for_public_projects=False,
)
async def experiments_set_experiment_tag(
    request: fastapi.Request,
    json_body: dict | None,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = (
        f"{get_tracking_server_url()}{REST_API_PREFIX}"
        f"/2.0/mlflow/experiments/set-experiment-tag"
    )

    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        json=json_body,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.post(f"{AJAX_API_PREFIX}/2.0/mlflow/experiments/search")
async def ajax_api_experiments_search(
    request: fastapi.Request,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    return await _experiments_search(request=request, db=db, url_prefix=REST_API_PREFIX)


@router.get(f"{AJAX_API_PREFIX}/2.0/mlflow/experiments/search")
async def ajax_api_experiments_search_get(
    request: fastapi.Request,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    return await _experiments_search(request=request, db=db, url_prefix=REST_API_PREFIX)


@router.post(f"{AJAX_API_PREFIX}/2.0/mlflow/experiments/search-datasets")
async def ajax_api_experiments_search_datasets(request: fastapi.Request):
    """We do not use data sets in MLFlow currently and therefore can
    simply return an empty payload to satisfy the MLFlow UI
    """

    payload = {"data_summaries": []}
    return fastapi.responses.JSONResponse(payload)


@router.get(f"{AJAX_API_PREFIX}/2.0/mlflow/experiments/get")
@mlflow_utils.mlflow_rbac_decorator(
    project_finder_function=mlflow_utils.ProjectFetcherFunction.from_mlflow_experiment_id,  # noqa E501
    allowed_roles=authorization.ALL_ROLES,
    allowed_for_public_projects=True,
)
async def ajax_api_experiments_get(
    request: fastapi.Request,
    json_body: dict | None = None,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = f"{get_tracking_server_url()}{AJAX_API_PREFIX}/2.0/mlflow/experiments/get"

    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.get(f"{AJAX_API_PREFIX}/2.0/mlflow/experiments/get-by-name")
@mlflow_utils.mlflow_rbac_decorator(
    project_finder_function=mlflow_utils.ProjectFetcherFunction.from_mlflow_experiment_name,  # noqa E501
    allowed_roles=authorization.ALL_ROLES,
    allowed_for_public_projects=True,
)
async def ajax_api_experiments_get_by_name(
    request: fastapi.Request,
    json_body: dict | None = None,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = (
        f"{get_tracking_server_url()}{AJAX_API_PREFIX}"
        f"/2.0/mlflow/experiments/get-by-name"
    )

    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


async def _experiments_search(
    request: fastapi.Request, db: database.client.main.Client, url_prefix: str
):
    """Fetches mlflow experiments,

    filtering out experiments which the caller does not have rights to see"""

    url = f"{get_tracking_server_url()}{url_prefix}/2.0/mlflow/experiments/search"
    try:
        json_body = await request.json()
    except json.JSONDecodeError:
        json_body = None

    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        json=json_body,
        headers=request.headers,
    )

    allowed_mlflow_experiment_ids = mlflow_utils.get_experiment_attribute_from_user_accessible_projects(  # noqa E501
        db=db,
        user_id=request.token.user_id,
        experiment_attribute=EXPERIMENT_ATTRIBUTE,
    )
    # if no experiments  are present, MLFlow returns {} which is undocumented
    try:
        response_filtered = {
            "experiments": [
                experiment
                for experiment in json.loads(response.text)["experiments"]
                if int(experiment["experiment_id"]) in allowed_mlflow_experiment_ids
            ]
        }
    except KeyError:
        response_filtered = {}

    return fastapi.Response(
        json.dumps(response_filtered), status_code=response.status_code
    )
