import json

import fastapi
import requests

import mantik_api.database as database
import mantik_api.routes.dependencies as dependencies
import mantik_api.routes.mlflow.utils as mlflow_utils
import mantik_api.routes.rbac.authorization as authorization
from mantik_api.routes.mlflow.tracking_server_router import AJAX_API_PREFIX
from mantik_api.routes.mlflow.tracking_server_router import get_tracking_server_url
from mantik_api.routes.mlflow.tracking_server_router import REST_API_PREFIX
from mantik_api.routes.mlflow.tracking_server_router import router

RUN_ID_ATTRIBUTE = "mlflow_run_id"


@router.post(f"{REST_API_PREFIX}/2.0/mlflow/runs/create")
@mlflow_utils.mlflow_rbac_decorator(
    project_finder_function=mlflow_utils.ProjectFetcherFunction.from_mlflow_experiment_id,  # noqa E501
    allowed_roles=authorization.WORKING_ROLES,
    allowed_for_public_projects=False,
)
async def runs_create(
    request: fastapi.Request,
    json_body: dict | None,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = f"{get_tracking_server_url()}{REST_API_PREFIX}/2.0/mlflow/runs/create"

    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        json=json_body,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.post(f"{REST_API_PREFIX}/2.0/mlflow/runs/delete")
@mlflow_utils.mlflow_rbac_decorator(
    project_finder_function=mlflow_utils.ProjectFetcherFunction.from_mlflow_run_id,
    allowed_roles=authorization.WORKING_ROLES,
    allowed_for_public_projects=False,
)
async def runs_delete(
    request: fastapi.Request,
    json_body: dict | None,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = f"{get_tracking_server_url()}{REST_API_PREFIX}/2.0/mlflow/runs/delete"

    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        json=json_body,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.post(f"{REST_API_PREFIX}/2.0/mlflow/runs/restore")
@mlflow_utils.mlflow_rbac_decorator(
    project_finder_function=mlflow_utils.ProjectFetcherFunction.from_mlflow_run_id,
    allowed_roles=authorization.WORKING_ROLES,
    allowed_for_public_projects=False,
)
async def runs_restore(
    request: fastapi.Request,
    json_body: dict | None,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = f"{get_tracking_server_url()}{REST_API_PREFIX}/2.0/mlflow/runs/restore"

    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        json=json_body,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.get(f"{REST_API_PREFIX}/2.0/mlflow/runs/get")
@mlflow_utils.mlflow_rbac_decorator(
    project_finder_function=mlflow_utils.ProjectFetcherFunction.from_mlflow_run_id,
    allowed_roles=authorization.ALL_ROLES,
    allowed_for_public_projects=True,
)
async def runs_get(
    request: fastapi.Request,
    json_body: dict | None = None,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = f"{get_tracking_server_url()}{REST_API_PREFIX}/2.0/mlflow/runs/get"

    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        json=json_body,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.post(f"{REST_API_PREFIX}/2.0/mlflow/runs/log-metric")
@mlflow_utils.mlflow_rbac_decorator(
    project_finder_function=mlflow_utils.ProjectFetcherFunction.from_mlflow_run_id,
    allowed_roles=authorization.WORKING_ROLES,
    allowed_for_public_projects=False,
)
async def runs_log_metric(
    request: fastapi.Request,
    json_body: dict | None,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = f"{get_tracking_server_url()}{REST_API_PREFIX}/2.0/mlflow/runs/log-metric"

    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        json=json_body,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.post(f"{REST_API_PREFIX}/2.0/mlflow/runs/log-batch")
@mlflow_utils.mlflow_rbac_decorator(
    project_finder_function=mlflow_utils.ProjectFetcherFunction.from_mlflow_run_id,
    allowed_roles=authorization.WORKING_ROLES,
    allowed_for_public_projects=False,
)
async def runs_log_batch(
    request: fastapi.Request,
    json_body: dict | None,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = f"{get_tracking_server_url()}{REST_API_PREFIX}/2.0/mlflow/runs/log-batch"

    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        json=json_body,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.post(f"{REST_API_PREFIX}/2.0/mlflow/runs/log-model")
@mlflow_utils.mlflow_rbac_decorator(
    project_finder_function=mlflow_utils.ProjectFetcherFunction.from_mlflow_run_id,
    allowed_roles=authorization.WORKING_ROLES,
    allowed_for_public_projects=False,
)
async def runs_log_model(
    request: fastapi.Request,
    json_body: dict | None,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = f"{get_tracking_server_url()}{REST_API_PREFIX}/2.0/mlflow/runs/log-model"

    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        json=json_body,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.post(f"{REST_API_PREFIX}/2.0/mlflow/runs/log-inputs")
@mlflow_utils.mlflow_rbac_decorator(
    project_finder_function=mlflow_utils.ProjectFetcherFunction.from_mlflow_run_id,
    allowed_roles=authorization.WORKING_ROLES,
    allowed_for_public_projects=False,
)
async def runs_log_inputs(
    request: fastapi.Request,
    json_body: dict | None,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = f"{get_tracking_server_url()}{REST_API_PREFIX}/2.0/mlflow/runs/log-inputs"

    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        json=json_body,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.post(f"{REST_API_PREFIX}/2.0/mlflow/runs/set-tag")
@mlflow_utils.mlflow_rbac_decorator(
    project_finder_function=mlflow_utils.ProjectFetcherFunction.from_mlflow_run_id,
    allowed_roles=authorization.WORKING_ROLES,
    allowed_for_public_projects=False,
)
async def runs_set_tag(
    request: fastapi.Request,
    json_body: dict | None,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = f"{get_tracking_server_url()}{REST_API_PREFIX}/2.0/mlflow/runs/set-tag"

    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        json=json_body,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.post(f"{REST_API_PREFIX}/2.0/mlflow/runs/delete-tag")
@mlflow_utils.mlflow_rbac_decorator(
    project_finder_function=mlflow_utils.ProjectFetcherFunction.from_mlflow_run_id,
    allowed_roles=authorization.WORKING_ROLES,
    allowed_for_public_projects=False,
)
async def runs_delete_tag(
    request: fastapi.Request,
    json_body: dict | None,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = f"{get_tracking_server_url()}{REST_API_PREFIX}/2.0/mlflow/runs/delete-tag"

    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        json=json_body,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.post(f"{REST_API_PREFIX}/2.0/mlflow/runs/log-parameter")
@mlflow_utils.mlflow_rbac_decorator(
    project_finder_function=mlflow_utils.ProjectFetcherFunction.from_mlflow_run_id,
    allowed_roles=authorization.WORKING_ROLES,
    allowed_for_public_projects=False,
)
async def runs_log_parameter(
    request: fastapi.Request,
    json_body: dict | None,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = f"{get_tracking_server_url()}{REST_API_PREFIX}/2.0/mlflow/runs/log-parameter"

    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        json=json_body,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.post(f"{REST_API_PREFIX}/2.0/mlflow/runs/search")
async def runs_search(
    request: fastapi.Request,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    return await _runs_search(request=request, db=db, url_prefix=REST_API_PREFIX)


@router.post(f"{REST_API_PREFIX}/2.0/mlflow/runs/update")
@mlflow_utils.mlflow_rbac_decorator(
    project_finder_function=mlflow_utils.ProjectFetcherFunction.from_mlflow_run_id,
    allowed_roles=authorization.WORKING_ROLES,
    allowed_for_public_projects=False,
)
async def runs_update(
    request: fastapi.Request,
    json_body: dict | None,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = f"{get_tracking_server_url()}{REST_API_PREFIX}/2.0/mlflow/runs/update"

    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        json=json_body,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.get(f"{AJAX_API_PREFIX}/2.0/mlflow/runs/get")
@mlflow_utils.mlflow_rbac_decorator(
    project_finder_function=mlflow_utils.ProjectFetcherFunction.from_mlflow_run_id,
    allowed_roles=authorization.ALL_ROLES,
    allowed_for_public_projects=True,
)
async def ajax_api_runs_get(
    request: fastapi.Request,
    json_body: dict | None = None,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    url = f"{get_tracking_server_url()}{AJAX_API_PREFIX}/2.0/mlflow/runs/get"

    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        json=json_body,
        headers=request.headers,
    )

    return fastapi.Response(content=response.text, status_code=response.status_code)


@router.post(f"{AJAX_API_PREFIX}/2.0/mlflow/runs/search")
async def ajax_api_runs_search_(
    request: fastapi.Request,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    return await _runs_search(request=request, db=db, url_prefix=AJAX_API_PREFIX)


async def _runs_search(
    request: fastapi.Request,
    db: database.client.main.Client,
    url_prefix: str,
):
    """Fetches mlflow runs,

    filtering out runs which the caller does not have rights to see."""

    url = f"{get_tracking_server_url()}{url_prefix}/2.0/mlflow/runs/search"
    try:
        json_body = await request.json()
    except json.JSONDecodeError:
        json_body = None
    response = requests.request(
        method=request.method,
        url=url,
        params=request.query_params,
        json=json_body,
        headers=request.headers,
    )

    allowed_mlflow_run_ids = [
        mlflow_run_id.hex
        for mlflow_run_id in mlflow_utils.get_run_attribute_from_user_accessible_projects(  # noqa E501
            db=db, user_id=request.token.user_id, run_attribute=RUN_ID_ATTRIBUTE
        )
    ]
    # if no runs are present, MLFlow returns {} which is undocumented
    try:
        response_filtered = {
            "runs": [
                run
                for run in json.loads(response.text)["runs"]
                if run["info"]["run_id"] in allowed_mlflow_run_ids
            ]
        }
    except KeyError:
        response_filtered = {}

    return fastapi.Response(
        content=json.dumps(response_filtered), status_code=response.status_code
    )
