import enum
import functools
import json
import logging
import operator
import typing as t
import uuid

import mantik_api.database as database
import mantik_api.routes.rbac.authorization as authorization

logger = logging.getLogger(__name__)


def get_experiment_attribute_from_user_accessible_projects(
    db: database.client.main.Client,
    user_id: uuid.UUID,
    experiment_attribute: str = "mlflow_experiment_id",
) -> list[t.Any]:
    """
    Get a specific attribute of all experiments from user's accessible projects.
    """
    with db.session():
        user = db.get_one(
            orm_model_type=database.user.User,
            constraints={database.user.User.id: user_id},
            select_in_loading={
                database.user.User.groups: {},
                database.user.User.owned_groups: {},
                database.user.User.organizations: {},
                database.user.User.owned_organizations: {},
            },
        )
        projects = db.get_all(
            select_in_loading={
                database.project.Project.experiment_repositories: {},
            },
            orm_model_type=database.project.Project,
            constraints=database.project.Project.create_constraint_for_user_accessible_projects(  # noqa: E501
                user_=user
            ),
        )
        return [
            getattr(experiment, experiment_attribute)
            for project in projects.entities
            for experiment in project.experiment_repositories
        ]


def get_run_attribute_from_user_accessible_projects(
    db: database.client.main.Client,
    user_id: uuid.UUID,
    run_attribute: str = "mlflow_run_id",
) -> list[t.Any]:
    """
    Get a specific attribute of all runs from user's accessible projects.
    """
    with db.session():
        user = db.get_one(
            orm_model_type=database.user.User,
            constraints={database.user.User.id: user_id},
            select_in_loading={
                database.user.User.groups: {},
                database.user.User.owned_groups: {},
                database.user.User.organizations: {},
                database.user.User.owned_organizations: {},
            },
        )

        projects = db.get_all(
            select_in_loading={
                database.project.Project.experiment_repositories: {
                    database.experiment_repository.ExperimentRepository.runs: {}
                },
            },
            orm_model_type=database.project.Project,
            constraints=database.project.Project.create_constraint_for_user_accessible_projects(  # noqa: E501
                user_=user
            ),
        )
        user_accessible_experiments = functools.reduce(
            operator.add,
            [project.experiment_repositories for project in projects.entities],
        )
        user_accessible_runs = functools.reduce(
            operator.add,
            [experiment.runs for experiment in user_accessible_experiments],
        )

        return [getattr(run, run_attribute) for run in user_accessible_runs]


def get_project_from_mlflow_experiment_id(
    db: database.client.main.Client, mlflow_experiment_id: str
) -> database.project.Project:
    """Retrieve a mantik project, using the mlflow experiment id"""
    experiment = db.get_one(
        orm_model_type=database.experiment_repository.ExperimentRepository,
        constraints={
            database.experiment_repository.ExperimentRepository.mlflow_experiment_id: mlflow_experiment_id,  # noqa: E501
        },
        select_in_loading={
            database.experiment_repository.ExperimentRepository.project: {}
        },
    )
    return experiment.project


def get_project_from_mlflow_run_id(
    db: database.client.main.Client, mlflow_run_id: str
) -> database.project.Project:
    """Retrieve a mantik project, using the mlflow run id"""
    run = db.get_one(
        orm_model_type=database.run.Run,
        constraints={
            database.run.Run.mlflow_run_id: uuid.UUID(mlflow_run_id),
        },
        select_in_loading={
            database.run.Run.experiment_repository: {
                database.experiment_repository.ExperimentRepository.project: {}
            }
        },
    )
    return run.experiment_repository.project


def get_project_from_mlflow_experiment_name(
    db: database.client.main.Client, experiment_name: str
) -> database.project.Project:
    """Retrieve a mantik project, using the mlflow experiment name"""
    experiment = db.get_one(
        orm_model_type=database.experiment_repository.ExperimentRepository,
        constraints={
            database.experiment_repository.ExperimentRepository.name: experiment_name,
            # noqa: E501
        },
        select_in_loading={
            database.experiment_repository.ExperimentRepository.project: {}
        },
    )
    return experiment.project


def get_project_from_mlflow_artifact_path(
    db: database.client.main.Client, path: str
) -> database.project.Project:
    """Retrieve a mantik project, using the artifact path query parameter"""
    return get_project_from_mlflow_experiment_id(
        db=db, mlflow_experiment_id=path.split("/")[0]
    )


class ProjectFetcherFunction(enum.Enum):
    from_mlflow_experiment_id = get_project_from_mlflow_experiment_id
    from_mlflow_run_id = get_project_from_mlflow_run_id
    from_mlflow_experiment_name = get_project_from_mlflow_experiment_name
    from_mlflow_artifact_path = get_project_from_mlflow_artifact_path

    def mlflow_attribute(cls):
        match cls:
            case ProjectFetcherFunction.from_mlflow_experiment_id:
                return ["experiment_id"]
            case ProjectFetcherFunction.from_mlflow_run_id:
                return ["run_id", "run_uuid", "run_ids"]
            case ProjectFetcherFunction.from_mlflow_experiment_name:
                return ["experiment_name"]
            case ProjectFetcherFunction.from_mlflow_artifact_path:
                return ["path"]


def mlflow_rbac_decorator(
    project_finder_function: ProjectFetcherFunction,
    allowed_roles: list[str],
    allowed_for_public_projects: bool = True,
):
    """Protects mlflow proxy endpoint with the mantik RBAC."""

    def decorator_auth(func):
        @functools.wraps(func)
        async def wrapper_auth(*args, **kwargs):
            request = kwargs["request"]
            db = kwargs["db"]
            json_body = None

            if request.method == "POST":
                try:
                    json_body = await request.json()
                except json.JSONDecodeError:
                    pass

                kwargs["json_body"] = json_body

            arguments_dict = dict(json_body or {}) | dict(request.query_params)
            logger.debug("RBAC decorator: request arguments: %s", str(arguments_dict))

            for key in ProjectFetcherFunction.mlflow_attribute(project_finder_function):
                mlflow_attribute_to_find_project_value = arguments_dict.get(key, None)
                if mlflow_attribute_to_find_project_value:
                    break

            with db.session():
                project = project_finder_function(
                    db, mlflow_attribute_to_find_project_value
                )
                authorization.check_if_user_has_eligible_role(
                    request=request,
                    project=project,
                    allowed_roles=allowed_roles,
                    action_allowed_for_public_projects=allowed_for_public_projects,
                    unauthorized_message=(
                        "Your role is not eligible to view this project"
                    ),
                    client=db,
                )

            return await func(*args, **kwargs)

        return wrapper_auth

    return decorator_auth


def mlflow_path_rbac_decorator(
    project_finder_function: ProjectFetcherFunction,
    allowed_roles: list[str],
    allowed_for_public_projects: bool = True,
):
    """Decorator for functions that need to infer ownership
    from path instead of params or payload
    """

    def decorator_auth(func):
        @functools.wraps(func)
        async def wrapper_auth(*args, **kwargs):
            request = kwargs["request"]
            db = kwargs["db"]
            mlflow_experiment_id = kwargs["mlflow_experiment_id"]
            logger.debug(
                "RBAC decorator: mlflow_experiment_id: %s", mlflow_experiment_id
            )

            with db.session():
                project = project_finder_function(db, mlflow_experiment_id)
                authorization.check_if_user_has_eligible_role(
                    request=request,
                    project=project,
                    allowed_roles=allowed_roles,
                    action_allowed_for_public_projects=allowed_for_public_projects,
                    unauthorized_message=(
                        "Your role is not eligible to view this project"
                    ),
                    client=db,
                )

            return await func(*args, **kwargs)

        return wrapper_auth

    return decorator_auth
