import fastapi

import mantik_api.routes.rbac.bearer as bearer
import mantik_api.utils.env as env

TRACKING_SERVER_URL_INTERNAL_ENV_VAR = "MLFLOW_TRACKING_URI_INTERNAL"
TRACKING_SERVER_URL_PUBLIC_ENV_VAR = "MLFLOW_TRACKING_URI"
MLFLOW_ROUTER_PREFIX = "/tracking"
AJAX_API_PREFIX = "/ajax-api"
REST_API_PREFIX = "/api"


def get_tracking_server_url():
    return env.get_required_env_var(TRACKING_SERVER_URL_INTERNAL_ENV_VAR)


router = fastapi.APIRouter(
    prefix=MLFLOW_ROUTER_PREFIX,
    dependencies=[fastapi.Depends(bearer.MlflowJWTBearer())],
    include_in_schema=False,
)
