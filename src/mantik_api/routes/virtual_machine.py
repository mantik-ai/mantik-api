import fastapi
import requests
import starlette.status as status

import mantik_api.database as database
import mantik_api.database.exceptions
import mantik_api.models as models
import mantik_api.routes.dependencies as dependencies
import mantik_api.routes.rbac.bearer as bearer
import mantik_api.service as service


router = fastapi.APIRouter(
    dependencies=[fastapi.Depends(bearer.OptionalJWTBearer())],
    prefix="/virtual-machine",
    tags=["virtual machine image handling"],
)


@router.get(
    "/urls",
    responses={
        status.HTTP_200_OK: {
            "model": models.virtual_machine.GetAllUrlsOfVMsResponse,
            "description": "OK",
        },
    },
    summary="List VM urls",
    description="Add a url of a virtual machine image to the list belonging to the user making the request.",  # noqa: E501
    response_model=models.virtual_machine.GetAllUrlsOfVMsResponse,
)
async def get_urls_of_virtual_machine(
    request: bearer.ModifiedRequest,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.virtual_machine.GetAllUrlsOfVMsResponse:
    with db.session():
        current_user: database.client.main.GetAllResponse = db.get_one(
            orm_model_type=database.user.User,
            constraints={database.user.User.id: request.token.user_id},
        )
        urls_of_current_user = current_user.vm_urls or []
        n_urls = len(urls_of_current_user)
        return models.virtual_machine.GetAllUrlsOfVMsResponse(
            total_records=n_urls, page_records=n_urls, urls=urls_of_current_user
        )


@router.post(
    "/urls",
    status_code=status.HTTP_201_CREATED,
    responses={
        status.HTTP_201_CREATED: {
            "model": models.virtual_machine.UrlOfVmResponse,
            "description": "Url of virtual machine image added.",
        },
    },
    summary="Add a url of a VM",
    description="Add a url of a virtual machine image to the list belonging to the user making the request.",  # noqa: E501
    response_model=models.virtual_machine.UrlOfVmResponse,
)
async def add_new_url_of_virtual_machine(
    request: bearer.ModifiedRequest,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
    url: str = fastapi.Body(..., example="http://s3.amazonaws.com/bucket/image1.vmdk"),
) -> models.virtual_machine.UrlOfVmResponse:
    with db.session():
        service.virtual_machine.append_new_vm_url_for_user(
            db_client=db, user_id=request.token.user_id, url=url
        )
        return models.virtual_machine.UrlOfVmResponse(url=url)


@router.delete(
    "/urls",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {"description": "Successfully deleted url"},
        status.HTTP_404_NOT_FOUND: {
            "model": models.error.HTTPError,
            "description": "URL not found therefore not deleted",
        },
    },
    summary="Delete url of VM",
    description="Delete a url of a virtual machine image belonging to the user making the request.",  # noqa: E501
)
async def delete_url_of_virtual_machine(
    request: bearer.ModifiedRequest,
    url: str,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        service.virtual_machine.delete_vm_url_for_user(
            db_client=db, user_id=request.token.user_id, url=url
        )


@router.get(
    "/images",
    responses={
        status.HTTP_200_OK: {
            "model": bytes,
            "description": "OK",
            "content": {"application/octet-stream": {"example": "string"}},
        },
        status.HTTP_403_FORBIDDEN: {
            "model": models.error.HTTPError,
            "description": "Not authorized",
        },
        status.HTTP_404_NOT_FOUND: {
            "model": models.error.HTTPError,
            "description": "Image not found",
        },
    },
    summary="Fetch VM image",
    description="Fetch VM image from a URL.",
    response_class=fastapi.responses.StreamingResponse,
)
async def get_virtual_machine_image(request: bearer.ModifiedRequest, url: str):
    if service.virtual_machine.is_s3_url(url):
        if not (
            service.virtual_machine.extract_user_id_from_s3_url(url)
            == str(request.token.user_id)
        ):
            raise fastapi.HTTPException(status.HTTP_403_FORBIDDEN, "Not authorized")

        s3_object = service.virtual_machine.get_s3_object(url)
        return fastapi.responses.StreamingResponse(s3_object["Body"].iter_chunks())

    try:
        response = requests.get(url, stream=True)
        return fastapi.responses.StreamingResponse(response.iter_content(1024))
    except requests.exceptions.RequestException:
        raise fastapi.HTTPException(status.HTTP_403_FORBIDDEN, "Not authorized")


@router.post(
    "/images",
    status_code=status.HTTP_201_CREATED,
    responses={
        status.HTTP_201_CREATED: {
            "description": "Successfully uploaded a virtual machine image.",
        },
        status.HTTP_403_FORBIDDEN: {
            "model": models.error.HTTPError,
            "description": "Not authorized",
        },
    },
    summary="Upload VM image",
    description="Upload a virtual machine image to a url. Note: do not upload large files, upload files < 100 MB",  # noqa: E501
    response_model=models.virtual_machine.UrlOfVmResponse,
)
async def upload_new_virtual_machine_image(
    request: bearer.ModifiedRequest,
    baseUrl: str = fastapi.Query(..., example="s3://bucket-name"),
    file: fastapi.UploadFile = fastapi.File(...),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
):
    with db.session():
        url_of_new_image = service.virtual_machine.upload_image_to_url(
            file=file,
            base_url=baseUrl,
            current_user_id=request.token.user_id,
            db_client=db,
        )

        return models.virtual_machine.UrlOfVmResponse(url=url_of_new_image)


@router.delete(
    "/images",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully deleted virtual machine image"
        },
        status.HTTP_404_NOT_FOUND: {
            "model": models.error.HTTPError,
            "description": "Image not found therefore not deleted",
        },
        status.HTTP_403_FORBIDDEN: {
            "model": models.error.HTTPError,
            "description": "Not authorized to delete resource.",
        },
    },
    summary="Delete VM image ",
    description="Delete a virtual machine image at a URL.",
)
async def delete_virtual_machine_from_url(
    url: str,
    request: bearer.ModifiedRequest,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    calling_user_id = request.token.user_id
    url_is_authorized = service.virtual_machine.is_s3_url(url) and (
        service.virtual_machine.extract_user_id_from_s3_url(url) == str(calling_user_id)
    )

    if not url_is_authorized:
        raise fastapi.HTTPException(403, f"Not authorized to delete {url}")

    service.virtual_machine.delete_s3_resource(s3_url=url)

    try:
        with db.session():
            service.virtual_machine.delete_vm_url_for_user(
                db_client=db, user_id=calling_user_id, url=url
            )
    except mantik_api.database.exceptions.NotFoundError:
        # Note: try to delete the entry, but if it doesn't exist, don't return 404.
        pass
