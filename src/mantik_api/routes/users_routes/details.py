import fastapi
import starlette.status as status

import mantik_api.database as database
import mantik_api.models as models
import mantik_api.routes.dependencies as dependencies
import mantik_api.routes.rbac.bearer as bearer

router = fastapi.APIRouter(dependencies=[fastapi.Depends(bearer.JWTBearer())])


@router.get(
    "/user-details",
    responses={
        status.HTTP_200_OK: {
            "model": models.user.User,
            "description": "User details",
        },
    },
    tags=["users"],
    summary="Get user information from an access token (JWT)",
)
async def user_details_get(
    request: bearer.ModifiedRequest,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.user.User:
    """Return the user information based on the access token (JWT).

    The access token must be given in the `Authorization` header with `Bearer`
    scheme, e.g. `Authorization: Bearer <access token>`.

    """
    with db.session():
        user = db.get_one(
            orm_model_type=database.user.User,
            constraints={database.user.User.id: request.token.user_id},
        )
        return models.user.User.from_database_model(user)
