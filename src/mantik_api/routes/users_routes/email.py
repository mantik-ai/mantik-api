import uuid

import fastapi
import starlette.status as status

import mantik_api.aws.cognito as cognito
import mantik_api.database as database
import mantik_api.models as models
import mantik_api.routes.dependencies as dependencies
import mantik_api.routes.rbac.authorization as authorization
import mantik_api.routes.rbac.bearer as bearer

router = fastapi.APIRouter(dependencies=[fastapi.Depends(bearer.JWTBearer())])


@router.put(
    "/users/{userId}/email",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully initiated updating of user email"
        },
        status.HTTP_400_BAD_REQUEST: {
            "model": models.error.HTTPError,
            "description": "Updating of user email failed",
        },
    },
    tags=["users"],
    summary="Initiates updating of user email",
)
@authorization.reject_unauthorized_user
async def users_user_id_email_put(
    request: bearer.ModifiedRequest,
    userId: uuid.UUID,
    update_email: models.user.UpdateEmail = fastapi.Body(
        ..., description="Contains the new user email"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    """Initiate the process of updating a user's email address.

    Verification of the new update is required to eventually update to the new email.
    The old email is kept until the new email has been successfully verified.

    """
    try:
        cognito.users.initiate_change_email_cognito_user(
            new_email=update_email.new_email,
            token=request.token,
        )
    except cognito.exceptions.UserEmailUpdateFailedException as e:
        raise fastapi.HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=str(e)
        )


@router.post(
    "/users/{userId}/email/verify-update",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Email verification and update successful"
        },
        status.HTTP_404_NOT_FOUND: {
            "model": models.error.HTTPError,
            "description": "User not found",
        },
        status.HTTP_422_UNPROCESSABLE_ENTITY: {
            "model": models.error.HTTPError,
            "description": "Invalid confirmation code",
        },
        status.HTTP_424_FAILED_DEPENDENCY: {
            "model": models.error.HTTPError,
            "description": "Verification failed",
        },
    },
    tags=["users"],
    summary="Verify updated email",
)
@authorization.reject_unauthorized_user
async def users_user_id_email_verify_update_post(
    request: bearer.ModifiedRequest,
    userId: uuid.UUID,
    verify_update_email: models.user.VerifyUpdateEmail = fastapi.Body(
        ..., description="Contains the confirmation code"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    """Verify an updated email.

    Verification of the new update is required to eventually update to the new email.
    The old email is kept until the new email has been successfully verified.

    """
    with db.session():
        user: database.user.User = db.get_one(
            database.user.User, constraints={database.user.User.id: userId}
        )

        try:
            cognito.users.verify_updated_email(
                confirmation_code=verify_update_email.confirmation_code,
                token=request.token,
            )
        except cognito.exceptions.IncorrectConfirmationCodeException as e:
            raise fastapi.HTTPException(
                status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                detail=str(e),
            )
        except cognito.exceptions.EmailVerificationFailedException as e:
            raise fastapi.HTTPException(
                status_code=status.HTTP_424_FAILED_DEPENDENCY,
                detail=str(e),
            )

        try:
            cognito_user = cognito.users.get_cognito_user(request.token)
        except cognito.exceptions.UserFetchingFailedException as e:
            raise fastapi.HTTPException(
                status_code=status.HTTP_424_FAILED_DEPENDENCY,
                detail=str(e),
            )
        else:
            new_email = cognito_user.get_attribute("email")
            db.overwrite(user, fields_to_overwrite={"email": new_email})
