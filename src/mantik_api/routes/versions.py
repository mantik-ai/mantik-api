import mantik_api.routes.forgot_password as forgot_password
import mantik_api.routes.groups as groups
import mantik_api.routes.invitations as invitations
import mantik_api.routes.labels as labels
import mantik_api.routes.mlflow as mlflow_routes
import mantik_api.routes.mlflow.experiments_proxy as experiments_proxy
import mantik_api.routes.mlflow.others_proxy as others_proxy
import mantik_api.routes.mlflow.runs_proxy as runs_proxy
import mantik_api.routes.organizations as organizations
import mantik_api.routes.project_routes as project_routes
import mantik_api.routes.projects as projects
import mantik_api.routes.signup as signup
import mantik_api.routes.tokens as tokens
import mantik_api.routes.users as users
import mantik_api.routes.users_routes as users_routes
import mantik_api.routes.virtual_machine as virtual_machine

#
# Routes of the current API version.
#
# A route that has breaking changes should go to
# `NEXT_MINOR_VERSION_ROUTERS`.
#
# Note: The order of the routes affects their order in the API docs!
#
V_0_2_ROUTERS = [
    signup.router,
    forgot_password.router,
    tokens.tokens.router,
    users.router,
    users.router_no_auth_required,
    users_routes.details.router,
    users_routes.email.router,
    groups.router,
    organizations.router,
    labels.router,
    projects.router,
    project_routes.code_repository.router,
    project_routes.data_repository.router,
    project_routes.experiment_repository.router,
    project_routes.runs.router,
    project_routes.re_run.router,
    project_routes.run_schedules.routes.router,
    project_routes.model_repository.router,
    project_routes.members.router,
    project_routes.organizations.router,
    project_routes.groups.router,
    project_routes.deployment.router,
    project_routes.environment.router,
    project_routes.usage.router,
    invitations.router,
    mlflow_routes.experiments.router,
    virtual_machine.router,
    others_proxy.router,
    experiments_proxy.router,
    runs_proxy.router,
]

#
# Routes for the next minor version:
#
# New routes or routes with breaking changes.
#
V_0_3_ROUTERS = [
    tokens.tokens_v_0_3.router,
]
