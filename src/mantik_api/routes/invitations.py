import typing as t
import uuid

import fastapi
import sqlalchemy
import starlette.status as status

import mantik_api.aws.cognito.exceptions as exceptions
import mantik_api.database as database
import mantik_api.models.invitations as invitations
import mantik_api.routes._parameters as _parameters
import mantik_api.routes.dependencies as dependencies
import mantik_api.routes.rbac.bearer as bearer
import mantik_api.service as service

router = fastapi.APIRouter(dependencies=[fastapi.Depends(bearer.JWTBearer())])


@router.get(
    "/invitations",
    responses={
        status.HTTP_200_OK: {
            "model": invitations.InvitationsGet200Response,
            "description": "List of invitations",
        },
    },
    tags=["invitations"],
    summary="Get list of invitations",
)
async def invitations_get(
    request: bearer.ModifiedRequest,
    startindex: int = _parameters.start_index(),
    pagelength: int = _parameters.page_length(),
    order_by: database.invitation.OrderBy = _parameters.order_by(
        default=database.invitation.OrderBy.sent,
    ),
    ascending: bool = _parameters.ascending(),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> invitations.InvitationsGet200Response:
    caller_user_id = request.token.user_id
    with db.session():
        user = db.get_one(
            database.user.User,
            constraints={database.user.User.id: caller_user_id},
            select_in_loading={
                database.user.User.owned_groups: {},
                database.user.User.owned_organizations: {},
            },
        )

        def get_ids(column: sqlalchemy.Column) -> t.List[uuid.UUID]:
            return [row.id for row in column]

        constraint = database.invitation.Invitation.invited_id.in_(
            get_ids(user.owned_organizations)
            + get_ids(user.owned_groups)
            + [caller_user_id]
        )
        result: database.client.main.GetAllResponse = db.get_many(
            database.invitation.Invitation,
            constraints=constraint,
            offset=startindex,
            limit=pagelength,
            order_by=order_by,
            ascending=ascending,
        )
        return invitations.InvitationsGet200Response(
            total_records=result.total_count,
            page_records=result.page_count,
            invitations=[
                invitations.Invitation.from_database_model(
                    invited_name=service.invitations.get_invited_name(
                        db=db,
                        invited_type=invite.invited_type,
                        invited_id=invite.invited_id,
                        invited_user_name=user.preferred_name,
                    ),
                    invited_to_name=service.invitations.get_invited_to_name(
                        db=db,
                        invited_to_id=invite.invited_to_id,
                        invited_to_type=invite.invited_to_type,
                    ),
                    inviter_name=service.invitations.get_inviter_name(
                        db=db, inviter_id=invite.inviter_id
                    ),
                    invitation=invite,
                )
                for invite in result.entities
            ],
        )


@router.delete(
    "/invitations/{invitationId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {"description": "Successfully deleted invitation"},
    },
    tags=["invitations"],
    summary="Delete invitation",
)
async def invitations_invitation_id_delete(
    request: bearer.ModifiedRequest,
    invitationId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    caller_user_id = request.token.user_id
    with db.session():
        invitation = db.get_one(
            database.invitation.Invitation,
            constraints={database.invitation.Invitation.id: invitationId},
        )
        if invitation.inviter_id != caller_user_id:
            raise exceptions.AuthenticationFailedException(
                "Only the inviting user can delete an invitation"
            )
        db.delete(invitation)


@router.put(
    "/invitations/{invitationId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully updated invitation info"
        },
    },
    tags=["invitations"],
    summary="Update invitation info",
)
async def invitations_invitation_id_put(
    request: bearer.ModifiedRequest,
    invitationId: str = uuid.UUID,
    update_invitation: invitations.UpdateInvitation = fastapi.Body(
        ..., description="Updated invitation info"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    caller_user_id = request.token.user_id
    with db.session():
        invitation = db.get_one(
            database.invitation.Invitation,
            constraints={database.invitation.Invitation.id: invitationId},
        )

        user_ = db.get_one(
            database.user.User,
            constraints={database.user.User.id: caller_user_id},
            select_in_loading={
                database.user.User.owned_groups: {},
                database.user.User.owned_organizations: {},
            },
        )

        def get_ids(column: sqlalchemy.Column) -> t.List[uuid.UUID]:
            return [row.id for row in column]

        relevant_ids = set(
            get_ids(user_.owned_organizations)
            + get_ids(user_.owned_groups)
            + [caller_user_id]
        )
        if invitation.invited_id not in relevant_ids:
            raise exceptions.AuthenticationFailedException(
                "Only the invited user can update an invitation"
            )
        db.delete(invitation)
        if update_invitation.accepted:
            db.add(invitation.to_association_table())


@router.post(
    "/invitations",
    status_code=status.HTTP_201_CREATED,
    responses={
        status.HTTP_201_CREATED: {
            "model": invitations.InvitationsPost201Response,
            "description": "Successfully added invitations",
        },
    },
    tags=["invitations"],
    summary="Add invitations",
)
async def invitations_post(
    request: bearer.ModifiedRequest,
    add_invitation: invitations.AddInvitation = fastapi.Body(
        ..., description="Invitation info"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> invitations.InvitationsPost201Response:
    """Send an invitation to a USER, GROUP or ORGANIZATION to join your
    GROUP, ORGANIZATION, PROJECT.

    if you want to invite to a GROUP (invitedToType="GROUP"):
    - you can only invite a USER (invitedType="USER")

    if you want to invite to a ORGANIZATION (invitedToType="ORGANIZATION"):
    - you can invite a USER (invitedType="USER")
    - or a GROUP (invitedType="GROUP")

    if you want to invite to a PROJECT (invitedToType="PROJECT"):
    - you can invite a USER (invitedType="USER")
    - or a GROUP (invitedType="GROUP")
    - or a ORGANIZATION (invitedType="ORGANIZATION")

    the role is only necessary when invitedToType is PROJECT,
    and can be REPORTER, RESEARCHER, MAINTAINER or OWNER
    """
    caller_user_id = request.token.user_id
    new_invitation_id = uuid.uuid4()

    service.invitations.validate_and_add_invitation(
        add_invitation=add_invitation,
        db=db,
        caller_user_id=caller_user_id,
        new_invitation_id=new_invitation_id,
    )
    return invitations.InvitationsPost201Response(invitation_id=new_invitation_id)
