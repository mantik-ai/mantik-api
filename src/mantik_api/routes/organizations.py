import uuid

import fastapi
import starlette.status as status

import mantik_api.aws.cognito.exceptions as exceptions
import mantik_api.database as database
import mantik_api.models as models
import mantik_api.routes._parameters as _parameters
import mantik_api.routes.dependencies as dependencies
import mantik_api.routes.rbac.bearer as bearer
import mantik_api.service as service

router = fastapi.APIRouter(dependencies=[fastapi.Depends(bearer.JWTBearer())])


@router.get(
    "/organizations",
    responses={
        status.HTTP_200_OK: {
            "model": models.organization.OrganizationsGet200Response,
            "description": "List of organizations",
        },
    },
    tags=["organizations"],
    summary="Get list of organizations",
)
async def organizations_get(
    startindex: int = _parameters.start_index(),
    pagelength: int = _parameters.page_length(),
    order_by: database.organization.OrderBy = _parameters.order_by(
        default=database.organization.OrderBy.created
    ),
    ascending: bool = _parameters.ascending(),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.organization.OrganizationsGet200Response:
    with db.session():
        result: database.client.main.GetAllResponse = db.get_many(
            orm_model_type=database.organization.Organization,
            select_in_loading={
                database.organization.Organization.contact: {},
                database.organization.Organization.groups: {
                    database.user_group.UserGroup.admin: {},
                    database.user_group.UserGroup.members: {},
                },
                database.organization.Organization.members: {},
                database.organization.Organization.projects: {},
            },
            limit=pagelength,
            offset=startindex,
            order_by=order_by,
            ascending=ascending,
        )

        return models.organization.OrganizationsGet200Response(
            total_records=result.total_count,
            page_records=result.page_count,
            organizations=models.base.from_database_models(
                models.organization.Organization, result.entities
            ),
        )


@router.get(
    "/organizations/{organizationId}",
    responses={
        status.HTTP_200_OK: {
            "model": models.organization.Organization,
            "description": "Successfully retrieved organization info",
        },
    },
    tags=["organizations"],
    summary="Get info on specified organization",
)
async def organizations_organization_id_get(
    organizationId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.organization.Organization:
    with db.session():
        organization = db.get_one(
            orm_model_type=database.organization.Organization,
            constraints={database.organization.Organization.id: organizationId},
            select_in_loading={
                database.organization.Organization.contact: {},
                database.organization.Organization.groups: {
                    database.user_group.UserGroup.admin: {},
                    database.user_group.UserGroup.members: {},
                },
                database.organization.Organization.members: {},
                database.organization.Organization.projects: {},
            },
        )
        return models.organization.Organization.from_database_model(organization)


@router.put(
    "/organizations/{organizationId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully updated organization info"
        },
    },
    tags=["organizations"],
    summary="Update organization info",
)
async def organizations_organization_id_put(
    request: bearer.ModifiedRequest,
    organizationId: uuid.UUID,
    updated_organization_payload: models.organization.UpdateOrganization = fastapi.Body(
        ..., description="Updated organization info"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    caller_user_id = request.token.user_id
    with db.session():
        existing_organization = db.get_one(
            orm_model_type=database.organization.Organization,
            constraints={database.organization.Organization.id: organizationId},
            select_in_loading={
                database.organization.Organization.members: {},
                database.organization.Organization.groups: {},
            },
        )
        if existing_organization.contact_id != caller_user_id:
            raise exceptions.AuthenticationFailedException(
                "Only current Organization's Contact "
                "can modify the Organization information"
            )

        db.overwrite(
            original_orm=existing_organization,
            fields_to_overwrite=updated_organization_payload.to_overwrite_kwargs(db),
        )


@router.post(
    "/organizations",
    status_code=status.HTTP_201_CREATED,
    responses={
        status.HTTP_201_CREATED: {
            "model": models.organization.OrganizationsGet201Response,
            "description": "Sucessfully added organization",
        },
    },
    tags=["organizations"],
    summary="Add organization",
)
async def organizations_post(
    request: bearer.ModifiedRequest,
    new_organization_payload: models.organization.AddOrganization = fastapi.Body(
        ..., description="models.organization.Organization info"
    ),
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> models.organization.OrganizationsGet201Response:
    caller_user_id = request.token.user_id
    with db.session():
        contact: database.user.User = database.user.get_user_by_id(
            db, user_id=caller_user_id
        )
        new_organization_id = uuid.uuid4()
        db.add(
            model=new_organization_payload.to_database_model(
                organization_id=new_organization_id, contact=contact
            )
        )

        return models.organization.OrganizationsGet201Response(
            organization_id=new_organization_id
        )


@router.delete(
    "/organizations/{organizationId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully deleted organization"
        },
        status.HTTP_401_UNAUTHORIZED: {
            "model": models.error.HTTPError,
            "description": "Only organization contact can delete the organization",
        },
    },
    tags=["organizations"],
    summary="Delete an organization",
    description=(
        "Deleting an organization will implicitly remove all access rights"
        "of all members and user groups to the organization."
        "The deletion of an organization is only permitted to the contact person"
        "of the organization."
    ),
)
async def organizations_organization_id_delete(
    request: bearer.ModifiedRequest,
    organizationId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        organization = db.get_one(
            database.organization.Organization,
            constraints={database.organization.Organization.id: organizationId},
        )
        if not _called_by_organization_contact(request, organization=organization):
            raise exceptions.AuthenticationFailedException(
                "Only the contact person of the organization"
                "can delete the organization"
            )
        db.delete(organization)
        service.invitations.delete_related_invitations(
            db=db, model=database.organization.Organization, model_id=organizationId
        )


@router.delete(
    "/organizations/{organizationId}/members/{userId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully removed member from organization"
        },
        status.HTTP_401_UNAUTHORIZED: {
            "model": models.error.HTTPError,
            "description": (
                "Member can only be removed by the organization "
                "contact or the respective member"
            ),
        },
        status.HTTP_422_UNPROCESSABLE_ENTITY: {
            "model": models.error.HTTPError,
            "description": "Organization contact cannot be removed",
        },
    },
    tags=["organizations"],
    summary="Remove member from organization",
    description=(
        "Removing a user from the organization will remove all access rights"
        "of the user to the organization"
        "The removal of a member is only permitted to the member themselves"
        "or the contact person of the organization."
        "The contact person of the organization cannot be removed."
    ),
)
async def organizations_organization_member_id_delete(
    request: bearer.ModifiedRequest,
    organizationId: uuid.UUID,
    userId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        organization = db.get_one(
            database.organization.Organization,
            constraints={database.organization.Organization.id: organizationId},
        )
        if not _called_by_member(
            request, user_id=userId
        ) and not _called_by_organization_contact(request, organization=organization):
            raise exceptions.AuthenticationFailedException(
                "Only the member themselves or the contact person of the organization"
                "can remove the member from the organization"
            )
        if userId == organization.contact_id:
            raise fastapi.HTTPException(
                status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                detail="The contact cannot be removed from the organization",
            )

        db.delete(
            database.organization.OrganizationUserAssociationTable,
            constraints={
                database.organization.OrganizationUserAssociationTable.organization_id: organizationId,  # noqa: E501
                database.organization.OrganizationUserAssociationTable.user_id: userId,
            },
        )


@router.delete(
    "/organizations/{organizationId}/groups/{groupId}",
    status_code=status.HTTP_204_NO_CONTENT,
    responses={
        status.HTTP_204_NO_CONTENT: {
            "description": "Successfully removed user group from organization"
        },
        status.HTTP_401_UNAUTHORIZED: {
            "description": (
                "Only organization contact or user group " "admin can remove user group"
            )
        },
    },
    tags=["organizations"],
    summary="Remove user group from organization",
    description=(
        "Removing a user group from the organization will remove all access rights"
        "of the user group and its users to the organization"
        "The removal of a user group is only permitted to the user group admin"
        "or the contact person of the organization."
    ),
)
async def organizations_organization_user_group_id_delete(
    request: bearer.ModifiedRequest,
    organizationId: uuid.UUID,
    groupId: uuid.UUID,
    db: database.client.main.Client = fastapi.Depends(dependencies.get_db_client),
) -> None:
    with db.session():
        organization = db.get_one(
            database.organization.Organization,
            constraints={database.organization.Organization.id: organizationId},
        )
        group = db.get_one(
            database.user_group.UserGroup,
            constraints={database.user_group.UserGroup.id: groupId},
        )

        if not _called_by_group_admin(
            request, group=group
        ) and not _called_by_organization_contact(request, organization=organization):
            raise exceptions.AuthenticationFailedException(
                "Only the user group admin or the contact of the organization "
                "can remove the user group from the organization"
            )

        db.delete(
            database.organization.OrganizationUserGroupAssociationTable,
            constraints={
                database.organization.OrganizationUserGroupAssociationTable.organization_id: organizationId,  # noqa: E501
                database.organization.OrganizationUserGroupAssociationTable.user_group_id: groupId,  # noqa: E501
            },
        )


def _called_by_member(request: bearer.ModifiedRequest, user_id: uuid.UUID) -> bool:
    return request.token.user_id == user_id


def _called_by_group_admin(
    request: bearer.ModifiedRequest, group: database.user_group.UserGroup
) -> bool:
    return request.token.user_id == group.admin_id


def _called_by_organization_contact(
    request: bearer.ModifiedRequest, organization: database.organization.Organization
) -> bool:
    return request.token.user_id == organization.contact_id
