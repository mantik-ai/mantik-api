import typing as t
import uuid

import mantik_api.database as database


def get_user_role_in_project(
    project: database.project.Project,
    user_id: uuid.UUID,
    client: database.client.main.Client,
) -> database.role_details.RoleDetails:
    if project.owner_id == user_id:
        return database.role_details.RoleDetails(
            role=database.role_details.ProjectRole.OWNER,
            # TODO (fabian.emmerich): As soon as projects can be transferred
            # to another user, this should be the date where the user was
            # added as a member and the date where the ownership was transferred
            # to them.
            added_at=project.created_at,
            updated_at=project.updated_at,
        )
    role_details = _get_all_user_roles(
        project=project,
        user_id=user_id,
        client=client,
    )

    added_at = [
        details.added_at for details in role_details if details.added_at is not None
    ]
    updated_at = [
        details.updated_at for details in role_details if details.updated_at is not None
    ]

    return database.role_details.RoleDetails(
        role=database.role_details.ProjectRole(
            max(details.role.value for details in role_details)
        ),
        added_at=max(added_at) if added_at else None,
        updated_at=max(updated_at) if updated_at else None,
    )


def _get_all_user_roles(
    project: database.project.Project,
    user_id: uuid.UUID,
    client: database.client.main.Client,
) -> list[database.role_details.RoleDetails]:
    group_roles = _get_group_roles(
        project=project,
        user_id=user_id,
        client=client,
    )
    organization_roles = _get_organization_roles(
        project=project,
        user_id=user_id,
        client=client,
    )
    user_role = _get_user_role(
        project=project,
        user_id=user_id,
        client=client,
    )
    return [user_role] + group_roles + organization_roles


def _get_group_roles(
    project: database.project.Project,
    user_id: uuid.UUID,
    client: database.client.main.Client,
) -> list[database.role_details.RoleDetails]:
    groups: t.Iterator[database.project.UserGroupProjectAssociationTable] = (
        client.get_one(
            orm_model_type=database.project.UserGroupProjectAssociationTable,
            constraints={
                database.project.UserGroupProjectAssociationTable.user_group_id: str(  # noqa: E501
                    group.id
                ),
                database.project.UserGroupProjectAssociationTable.project_id: project.id,  # noqa: E501
            },
        )
        for group in project.project_user_groups
        if group.contains_user(user_id)
    )
    return [
        database.role_details.RoleDetails(
            role=group.role,
            added_at=group.created_at,
            updated_at=group.updated_at,
        )
        for group in groups
    ]


def _get_organization_roles(
    project: database.project.Project,
    user_id: uuid.UUID,
    client: database.client.main.Client,
) -> list[database.role_details.RoleDetails]:
    organizations: t.Iterator[database.project.OrganizationProjectAssociationTable] = (
        client.get_one(
            orm_model_type=database.project.OrganizationProjectAssociationTable,
            constraints={
                database.project.OrganizationProjectAssociationTable.organization_id: str(  # noqa: E501
                    organization.id
                ),
                database.project.OrganizationProjectAssociationTable.project_id: project.id,  # noqa: E501
            },
        )
        for organization in project.project_organizations
        if organization.contains_user(user_id)
    )
    return [
        database.role_details.RoleDetails(
            role=organization.role,
            added_at=organization.created_at,
            updated_at=organization.updated_at,
        )
        for organization in organizations
    ]


def _get_user_role(
    project: database.project.Project,
    user_id: uuid.UUID,
    client: database.client.main.Client,
) -> database.role_details.RoleDetails:
    try:
        association: database.project.UserProjectAssociationTable = client.get_one(
            orm_model_type=database.project.UserProjectAssociationTable,
            constraints={
                database.project.UserProjectAssociationTable.user_id: user_id,
                database.project.UserProjectAssociationTable.project_id: project.id,
            },
        )
    except database.exceptions.NotFoundError:
        return database.role_details.RoleDetails(
            role=database.role_details.ProjectRole.NO_ROLE
        )
    else:
        return database.role_details.RoleDetails(
            role=association.role,
            added_at=association.created_at,
            updated_at=association.updated_at,
        )


def get_project_with_roles(
    project_id: str | uuid.UUID, client: database.client.main.Client
) -> database.project.Project:
    return client.get_one(
        orm_model_type=database.project.Project,
        constraints={database.project.Project.id: project_id},
        select_in_loading={
            database.project.Project.project_members: {},
            database.project.Project.project_user_groups: {
                database.user_group.UserGroup.members: {},
            },
            database.project.Project.project_organizations: {
                database.organization.Organization.members: {},
                database.organization.Organization.groups: {
                    database.user_group.UserGroup.members: {}
                },
            },
        },
    )
