import logging
import os
import typing as t

import fastapi
import fastapi.security as security
import starlette.requests
import starlette.requests as requests

import mantik_api.aws.cognito.exceptions as exceptions
import mantik_api.tokens as _tokens

logger = logging.getLogger(__name__)


Token = _tokens.jwt.JWT
OptionalToken = Token | None

MLFLOW_EXTENDED_VALIDITY_IN_DAYS = 7


class ModifiedRequest(starlette.requests.Request):
    """The request enriched with additional attributes/information."""

    @property
    def token(self) -> OptionalToken:
        """The token contained in the request header."""
        raise NotImplementedError


class JWTBearer(security.HTTPBearer):
    """Reads the bearer token from the header and verifies it.

    Notes
    -----
    Adds a `token` attribute to the ``request``.

    This request can then be accessed in the routes by

    ```python
    @router.get(...)
    async def get_method(request: ModifiedRequest, ...):
        print(request.user_id)
        ...
    ```

    """

    async def __call__(self, request: requests.Request) -> Token:
        """Verify the bearer token in the request header.

        Parameters
        ----------
        request: starlette.requests.Request
            The request with request headers.

        Raises
        ------
        mantik_api.tokens.cognito.exceptions.VerificationFailedException
            If no JWT given or given JWT is invalid.

        Returns
        -------
        mantik_api.tokens.jwt.JWT
            If a valid JWT is given in the header.

        """
        token = await self._get_token_and_verify(request)
        # Adding attributes to the request makes them accessible in all
        # endpoints that possess this dependency.
        request.token = token
        return token

    async def _get_token_and_verify(self, request: requests.Request) -> Token:
        token = await self._get_token_from_request_header(request)
        return _verify_token(token)

    async def _get_token_from_request_header(
        self, request: requests.Request
    ) -> str | None:
        try:
            credentials: t.Optional[
                security.HTTPAuthorizationCredentials
            ] = await super().__call__(request)
        except fastapi.HTTPException:
            raise exceptions.AuthenticationFailedException(
                "Request missing 'Authorization' header "
                "or incorrect scheme used ('Bearer' required)"
            )

        if credentials is None:
            raise exceptions.AuthenticationFailedException(
                "Token missing in 'Authorization' header"
            )

        return credentials.credentials


class MlflowJWTBearer(JWTBearer):

    """
    When a user submits a job that stays in the queue for 24h or more,
    the JWT set as MLFLOW_TRACKING_TOKEN becomes invalid (i.e. expires)
    and thus any request to the Mantik tracking server fail.

    We want that any Mantik API requests coming from a job running on HPC
    succeed even after longer queueing and job runtimes (max. 7 days).

    Here the extra validity time is defined in MLFLOW_EXTENDED_VALIDITY_IN_DAYS
    """

    async def _get_token_and_verify(self, request: requests.Request) -> Token:
        token = await self._get_token_from_request_header(request)
        return _verify_token(token, buffer_time_days=MLFLOW_EXTENDED_VALIDITY_IN_DAYS)


class OptionalJWTBearer(JWTBearer):
    """Reads an optional bearer token from the header and verifies it.

    Notes
    -----
    The token is only considered optional (i.e. not always verified)
    for ``GET`` requests. For all other types of requests, the token
    is **always** verified.

    """

    async def __call__(self, request: requests.Request) -> OptionalToken:
        """Verify the bearer token in the request header if given.

        Parameters
        ----------
        request: starlette.requests.Request
            The request with request headers.

        Raises
        ------
        mantik_api.tokens.cognito.exceptions.VerificationFailedException
            If no JWT given or given JWT is invalid.
            Not raised for ``GET`` requests.

        Returns
        -------
        mantik_api.tokens.jwt.JWT
            If a valid JWT is given in the header.
        None
            If no JWT given or given JWT is invalid.
            This is only returned for ``GET`` requests.

        """
        return await super().__call__(request)

    async def _get_token_and_verify(self, request: requests.Request) -> OptionalToken:
        """Get the token from the request header and verify it.

        Notes
        -----
        The token is loosely verified for ``GET`` requests. The token
        returned is then ``None``.

        """
        match request.method:
            case "GET":
                authorization_header: str | None = request.headers.get("Authorization")
                if authorization_header is None:
                    return None
                return await super()._get_token_and_verify(request)
            case _:
                return await super()._get_token_and_verify(request)


def _verify_token(token: str, buffer_time_days: int = 0) -> _tokens.jwt.JWT:
    verifier = _get_token_verifier()
    try:
        return verifier.verify_token(token, buffer_time_days=buffer_time_days)
    except _tokens.exceptions.VerificationFailedException as e:
        raise exceptions.AuthenticationFailedException(str(e))


def _get_token_verifier() -> _tokens.verifier.TokenVerifier:
    is_testing = os.environ.get("FAKE_TOKEN_VALIDATION", False)
    if is_testing:
        import mantik_api.testing.token_verifier as test_token_verifier

        logger.warning("Using faked token validation.")
        return test_token_verifier.FakeTokenVerifier(secret_required=False)
    return _tokens.verifier.TokenVerifier(secret_required=False)
