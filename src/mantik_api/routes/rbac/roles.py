import logging
import uuid

import mantik_api.database as database
import mantik_api.routes.rbac._project as _project
import mantik_api.routes.rbac.bearer as bearer
import mantik_api.tokens.jwt as jwt

logger = logging.getLogger(__name__)


def get_request_user_role_in_project(
    project_id: uuid.UUID,
    request: bearer.ModifiedRequest | None,
    client: database.client.main.Client | None,
) -> database.role_details.RoleDetails:
    if request is None:
        return database.role_details.RoleDetails(
            role=database.role_details.ProjectRole.NO_ROLE
        )

    match request.token:
        case jwt.JWT():
            if client is None:
                raise RuntimeError(
                    "If request is authorized, database client must be "
                    "provided to fetch user role"
                )
            project = _project.get_project_with_roles(project_id, client=client)
            return _project.get_user_role_in_project(
                project=project,
                user_id=request.token.user_id,
                client=client,
            )
        case _:
            return database.role_details.RoleDetails(
                role=database.role_details.ProjectRole.NO_ROLE
            )
