import functools
import uuid

import fastapi
import starlette.status as status

import mantik_api.aws.cognito.exceptions as cognito_exceptions
import mantik_api.database as database
import mantik_api.routes.rbac._project as _project
import mantik_api.routes.rbac.bearer as bearer
import mantik_api.tokens.jwt as jwt

ALL_ROLES = ["GUEST", "REPORTER", "RESEARCHER", "MAINTAINER", "OWNER"]
PROJECT_ROLES = ["REPORTER", "RESEARCHER", "MAINTAINER", "OWNER"]
WORKING_ROLES = ["RESEARCHER", "MAINTAINER", "OWNER"]
HEAD_ROLES = ["MAINTAINER", "OWNER"]
OWNER_ROLE = ["OWNER"]


def reject_unauthorized_user(func):
    """Reject request if token owner is not authorized.

    Parameters
    ----------
    request : mantik_api.routes.bearer.ModifiedRequest
        The request sent to the API endpoint.

        The request contains the token, which holds the information
        of the user who sent the request. `request.token.user_id`)
        tells which user the given token has been issued for.
    userId : uuid.UUID
        User ID in the path of the request.

        This is the user's ID whose sensitive data have been requested.

    Notes
    -----
    E.g. a request contains a token issued for user A,
    but the request tries to fetch sensitive data for user B.
    Then, the request will be rejectected because the token
    was not issued for user B.

    """

    @functools.wraps(func)
    async def wrapper(*args, **kwargs):
        request: bearer.ModifiedRequest = kwargs["request"]
        user_id: uuid.UUID = kwargs["userId"]

        if user_id != request.token.user_id:
            raise cognito_exceptions.AuthenticationFailedException(
                "Token issued for unauthorized user"
            )

        return await func(*args, **kwargs)

    return wrapper


def authorized_project_get_roles(
    allowed_roles: list[str], allowed_for_public_projects: bool = True
):
    def decorator_auth(func):
        @functools.wraps(func)
        async def wrapper_auth(*args, **kwargs):
            request, project_id, client = _get_required_arguments(kwargs)

            with client.session():
                project = _project.get_project_with_roles(project_id, client=client)
                check_if_user_has_eligible_role(
                    request=request,
                    project=project,
                    allowed_roles=allowed_roles,
                    action_allowed_for_public_projects=allowed_for_public_projects,
                    unauthorized_message=(
                        "Your role is not eligible to view this project"
                    ),
                    client=client,
                )

            return await func(*args, **kwargs)

        return wrapper_auth

    return decorator_auth


def authorized_project_put_post_delete_roles(allowed_roles: list[str]):
    # Ensure that GUEST isn't allowed to perform any actions for PUT/POST/DELETE.
    if "GUEST" in allowed_roles:
        allowed_roles.remove("GUEST")

    def decorator_auth(func):
        @functools.wraps(func)
        async def wrapper_auth(*args, **kwargs):
            request, project_id, client = _get_required_arguments(kwargs)

            with client.session():
                project = _project.get_project_with_roles(project_id, client=client)
                check_if_user_has_eligible_role(
                    request=request,
                    project=project,
                    allowed_roles=allowed_roles,
                    action_allowed_for_public_projects=False,
                    unauthorized_message=(
                        "Your role is not eligible to perform this action"
                    ),
                    client=client,
                )

            return await func(*args, **kwargs)

        return wrapper_auth

    return decorator_auth


def _get_required_arguments(
    kwargs: dict,
) -> tuple[bearer.ModifiedRequest, str, database.client.main.Client]:
    request: bearer.ModifiedRequest = kwargs["request"]
    project_id: str = kwargs["projectId"]
    client: database.client.main.Client = kwargs["db"]
    return request, project_id, client


def check_if_user_has_eligible_role(
    request: bearer.ModifiedRequest,
    project: database.project.Project,
    allowed_roles: list[str],
    action_allowed_for_public_projects: bool,
    unauthorized_message: str,
    client: database.client.main.Client,
) -> None:
    """Check whether the given user has an eligible role for the project."""
    match request.token:
        # Case: JWT present, user is logged in
        case jwt.JWT():
            if not user_has_sufficient_rights(
                request=request,
                project=project,
                allowed_roles=allowed_roles,
                action_allowed_for_public_projects=action_allowed_for_public_projects,
                client=client,
            ):
                raise fastapi.HTTPException(
                    status_code=status.HTTP_403_FORBIDDEN,
                    detail=unauthorized_message,
                )
        # Case: No JTW present, user is not logged in
        case _:
            # If project is public, but action is not allowed for
            # public projects (PUT/POST/DELETE)
            if project.public and not action_allowed_for_public_projects:
                raise fastapi.HTTPException(
                    status_code=status.HTTP_403_FORBIDDEN,
                    detail=unauthorized_message,
                )
            elif not project.public:
                raise fastapi.HTTPException(
                    status_code=status.HTTP_403_FORBIDDEN,
                    detail="Project is private",
                )


def user_has_sufficient_rights(
    request: bearer.ModifiedRequest,
    project: database.project.Project,
    allowed_roles: list[str],
    action_allowed_for_public_projects: bool,
    client: database.client.main.Client,
) -> bool:
    """Return whether the given user has view rights for the project."""
    if action_allowed_for_public_projects and project.public:
        return True

    role_details = _project.get_user_role_in_project(
        project=project, user_id=request.token.user_id, client=client
    )
    return role_details.role.name in allowed_roles
