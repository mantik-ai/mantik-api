import uuid

import fastapi
import starlette.status as status

import mantik_api.database as database


def user_owns_connection(
    connection_id: uuid.UUID,
    user_id: uuid.UUID,
    db: database.client.main.Client,
    message: str = "Only personal credentials can be used",
) -> database.connection.Connection:
    """Check whether a connection is owned by the given user."""
    connection: database.connection.Connection = db.get_one(
        database.connection.Connection,
        constraints={
            database.connection.Connection.id: connection_id,
        },
    )
    if not connection.owned_by_user(user_id):
        raise fastapi.HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail=message,
        )
    return connection
