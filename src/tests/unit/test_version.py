import pytest

import mantik_api.version as _version


@pytest.mark.parametrize(
    ("version", "expected"),
    [
        ("v0.1.0", "v0.1"),
        ("v0.1.1", "v0.1"),
        ("v1.1.1", "v1.1"),
    ],
)
def test_get_current_minor_version(version, expected):
    result = _version._get_current_minor_version(version)

    assert result == expected


@pytest.mark.parametrize(
    ("version", "expected"),
    [
        ("v0.1.0", "v0.2"),
        ("v0.1.1", "v0.2"),
        ("v1.1.1", "v1.2"),
    ],
)
def test_get_next_minor_version(version, expected):
    result = _version._get_next_minor_version(version)

    assert result == expected


def test_create_version_path():
    version = "v0.1"
    expected = "v0-1"

    result = _version._create_version_path(version)

    assert result == expected
