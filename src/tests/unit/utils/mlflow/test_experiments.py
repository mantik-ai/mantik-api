import pytest

import mantik_api.testing as testing
import mantik_api.utils.mlflow as mlflow

TEST_NAME = "test"


@pytest.mark.parametrize(
    ("responses", "expected"),
    [
        (
            [
                {},
            ],
            [],
        ),
        (
            [
                {
                    "experiments": [
                        {"experiment_id": "0", "name": f"{TEST_NAME}"},
                    ]
                },
            ],
            [
                mlflow.experiments.Experiment(
                    id=0,
                    name=f"{TEST_NAME}",
                ),
            ],
        ),
        (
            [
                {
                    "next_page_token": "eyJvZmZzZXQiOiAxfQ==",
                    "experiments": [
                        {
                            "experiment_id": "0",
                            "name": f"{TEST_NAME}1",
                        },
                    ],
                },
                {
                    "experiments": [
                        {"experiment_id": "1", "name": f"{TEST_NAME}2"},
                    ]
                },
            ],
            [
                mlflow.experiments.Experiment(
                    id=0,
                    name=f"{TEST_NAME}1",
                ),
                mlflow.experiments.Experiment(
                    id=1,
                    name=f"{TEST_NAME}2",
                ),
            ],
        ),
    ],
)
def test_search(set_mlflow_tracking_url, responses, expected):
    with set_mlflow_tracking_url(
        responses=[
            testing.requests.MlflowResponse(
                end_point="experiments/search",
                json=data,
            )
            for data in responses
        ]
    ):
        result = mlflow.experiments.search(
            token=testing.token_verifier.create_token("test"),
            filter_by="expression",
        )

    assert result == expected
