import json

import pytest

import mantik_api.testing as testing
import mantik_api.testing.token_verifier as token_verifier
import mantik_api.utils.mlflow._api as _api


@pytest.mark.parametrize(
    ("endpoint", "actual_endpoint"),
    [
        ("/starts/with/slash", "starts/with/slash"),
        ("starts/without/slash", "starts/without/slash"),
    ],
)
def test_no_double_backslash(set_mlflow_tracking_url, endpoint, actual_endpoint):
    with set_mlflow_tracking_url(
        responses=[
            testing.requests.MlflowResponse(
                end_point=actual_endpoint,
            ),
        ]
    ):
        payload = {"name": "bill"}
        response = _api.send_post_request(
            token_verifier.create_token("test"), payload, endpoint
        )
        assert json.loads(response.request.body.decode()) == payload
        assert response.request.url.endswith(actual_endpoint)
        assert not response.request.url.split(actual_endpoint)[0].endswith("//")
        assert response.request.url.split(actual_endpoint)[0].endswith("/")
