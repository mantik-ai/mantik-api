import pytest

import mantik_api.utils.mlflow as mlflow

TEST_NAME = "test"


@pytest.mark.parametrize(
    ("given", "expected"),
    [
        ("name", "name"),
        ("name-1", "name"),
        ("name-11", "name"),
        ("name-111", "name"),
        ("name-1111", "name"),
        ("name-11111", "name"),
    ],
)
def test_strip_suffix(given, expected):
    result = mlflow.names.strip_suffix(given)

    assert result == expected


@pytest.mark.parametrize(
    ("given", "names", "expected"),
    # All below test cases must at least contain an experiment that exactly
    # matches the test experiment name.
    [
        (
            TEST_NAME,
            [TEST_NAME],
            f"{TEST_NAME}-1",
        ),
        (
            f"{TEST_NAME}-1",
            [
                f"{TEST_NAME}-1",
            ],
            TEST_NAME,
        ),
        (
            f"{TEST_NAME}-1",
            [
                f"{TEST_NAME}",
                f"{TEST_NAME}-1",
            ],
            f"{TEST_NAME}-2",
        ),
        (
            TEST_NAME,
            [
                f"{TEST_NAME}",
                f"{TEST_NAME}-2",
            ],
            f"{TEST_NAME}-1",
        ),
        (
            TEST_NAME,
            [
                f"{TEST_NAME}",
                f"{TEST_NAME}-2-1",
            ],
            f"{TEST_NAME}-1",
        ),
        (
            TEST_NAME,
            [
                f"{TEST_NAME}",
                f"{TEST_NAME}_3-2",
            ],
            f"{TEST_NAME}-1",
        ),
        (
            TEST_NAME,
            [
                f"{TEST_NAME}",
                f"{TEST_NAME}--2",
            ],
            f"{TEST_NAME}-1",
        ),
        (
            TEST_NAME,
            [
                f"{TEST_NAME}",
                f"{TEST_NAME}1",
                f"{TEST_NAME}-2",
                f"{TEST_NAME}-1",
                f"{TEST_NAME}-2a",
                f"{TEST_NAME}-2-asdf",
            ],
            f"{TEST_NAME}-3",
        ),
    ],
)
def test_append_suffix(given, names, expected):
    result = mlflow.names.append_suffix(name=given, names=names)

    assert result == expected
