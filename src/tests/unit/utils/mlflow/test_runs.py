import uuid

import pytest

import mantik_api.testing as testing
import mantik_api.utils.mlflow.runs as runs

TEST_NAME = "test"


def test_prepare_uuid():
    run_id = uuid.UUID("0968b0af-db10-4050-90e2-77f031f907d8")
    expected = "0968b0afdb10405090e277f031f907d8"

    result = runs._prepare_uuid(run_id)

    assert result == expected


@pytest.mark.parametrize(
    ("responses", "expected"),
    [
        (
            [
                {},
            ],
            [],
        ),
        (
            [
                {
                    "runs": [
                        {
                            "info": {
                                "run_id": "ee2c7870128448f38c1d2a764e2566f8",
                                "run_name": f"{TEST_NAME}",
                            },
                        }
                    ]
                },
            ],
            [
                runs.Run(
                    id=uuid.UUID("ee2c7870128448f38c1d2a764e2566f8"),
                    name=f"{TEST_NAME}",
                ),
            ],
        ),
        (
            [
                {
                    "next_page_token": "eyJvZmZzZXQiOiAxfQ==",
                    "runs": [
                        {
                            "info": {
                                "run_id": "ee2c7870128448f38c1d2a764e2566f8",
                                "run_name": f"{TEST_NAME}1",
                            },
                        }
                    ],
                },
                {
                    "runs": [
                        {
                            "info": {
                                "run_id": "375fb2a199c74c7691e672e4b2a15a2f",
                                "run_name": f"{TEST_NAME}2",
                            }
                        },
                    ]
                },
            ],
            [
                runs.Run(
                    id=uuid.UUID("ee2c7870128448f38c1d2a764e2566f8"),
                    name=f"{TEST_NAME}1",
                ),
                runs.Run(
                    id=uuid.UUID("375fb2a199c74c7691e672e4b2a15a2f"),
                    name=f"{TEST_NAME}2",
                ),
            ],
        ),
    ],
)
def test_search(set_mlflow_tracking_url, responses, expected):
    with set_mlflow_tracking_url(
        responses=[
            testing.requests.MlflowResponse(
                end_point="runs/search",
                json=data,
            )
            for data in responses
        ]
    ):
        result = runs.search(
            experiment_id=1,
            token=testing.token_verifier.create_token("test"),
            filter_by="expression",
        )

    assert result == expected
