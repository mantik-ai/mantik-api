import pytest

import mantik_api.utils.enums as enums


class Enum(enums.CaseInsensitiveStrEnum):
    tEsT = "test"


class TestCaseInsensitiveStrEnum:
    @pytest.mark.parametrize("value", ["TEST", "test", "tEsT", "TeSt", "tEST"])
    def test_case_insensitive(self, value):
        result = Enum(value)

        assert result == Enum.tEsT
        assert result.value == "test"
