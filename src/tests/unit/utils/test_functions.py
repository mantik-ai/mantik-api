import typing as t

import pytest

import mantik_api.utils.functions as functions


@pytest.mark.parametrize(
    "list2d,expected_flat_list",
    [([[1, 2, 3]], [1, 2, 3]), ([[1, 2], [3]], [1, 2, 3]), ([], [])],
)
def test_flat_map(list2d: list[list[t.Any]], expected_flat_list: list[t.Any]):
    assert functions.flat_map(list2d) == expected_flat_list
