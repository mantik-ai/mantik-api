import json
import pathlib

import fastapi
import pytest
import starlette.status as status

import mantik_api.utils.cron as cron

FILE_PATH = pathlib.Path(__file__).parent
TZ_DATABASE_FILE = FILE_PATH / "../../resources/tz_database.json"


@pytest.mark.parametrize(
    ("expression", "expected"),
    [
        ("0 12 * * *", None),
        (
            "* * * * NotValid",
            fastapi.HTTPException(status.HTTP_422_UNPROCESSABLE_ENTITY),
        ),
        # Test cases: prevent execution interval < 1h
        ("* * * * *", fastapi.HTTPException(status.HTTP_422_UNPROCESSABLE_ENTITY)),
        ("*/10 * * * *", fastapi.HTTPException(status.HTTP_422_UNPROCESSABLE_ENTITY)),
        ("1,10 * * * *", fastapi.HTTPException(status.HTTP_422_UNPROCESSABLE_ENTITY)),
        ("1-10 * * * *", fastapi.HTTPException(status.HTTP_422_UNPROCESSABLE_ENTITY)),
    ],
)
def test_validate_cron_expression(expect_raise_if_exception, expression, expected):
    with expect_raise_if_exception(expected):
        cron.validate_cron_expression(expression)


@pytest.mark.parametrize(
    ("time_zone", "expected"),
    [
        ("Not Valid", fastapi.HTTPException(status.HTTP_422_UNPROCESSABLE_ENTITY)),
        (
            "* * * * NotValid",
            fastapi.HTTPException(status.HTTP_422_UNPROCESSABLE_ENTITY),
        ),
    ],
)
def test_validate_time_zone(expect_raise_if_exception, time_zone, expected):
    with expect_raise_if_exception(expected):
        cron.validate_time_zone(time_zone)


def test_validate_time_zone_from_tz_database(expect_raise_if_exception):
    with open(TZ_DATABASE_FILE) as f:
        time_zones = json.loads(f.read())

    for time_zone in time_zones:
        cron.validate_time_zone(time_zone)
