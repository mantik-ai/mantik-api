import contextlib

import pytest
import responses as _responses
import starlette.status as status

import mantik_api.routes.mlflow.tracking_server_router as tracking_server_router
import mantik_api.testing as testing
import mantik_api.utils.env as env


@pytest.fixture()
def set_mlflow_tracking_url(tmp_path):
    """Set MLflow tracking URI to local directory.

    In production, this is a URL. Locally, this is mimicked by creating an mlruns
    folder and setting its path as the URI. The MLflow API will then locally
    create e.g. experiments and runs inside that folder.

    """

    @contextlib.contextmanager
    def mlflow_setup_and_destroy(
        responses: list[testing.requests.MlflowResponse] = None,
    ) -> None:
        responses = responses or []

        env_vars = {
            tracking_server_router.TRACKING_SERVER_URL_INTERNAL_ENV_VAR: testing.requests.BASE_MLFLOW_URL,  # noqa E501
            tracking_server_router.TRACKING_SERVER_URL_PUBLIC_ENV_VAR: testing.requests.BASE_MLFLOW_URL,  # noqa E501
        }
        with _responses.RequestsMock(
            registry=_responses.registries.OrderedRegistry
        ) as m, env.env_vars_set(env_vars):
            m.responses = [m.add(**response.to_dict()) for response in responses]
            yield m

    return mlflow_setup_and_destroy


@pytest.fixture
def mock_compute_backend_api_unicore_or_firecrest_unit_testing(
    mock_gitlab_zip_download,
    fake_compute_backend_url,
    api_experiment_repository,
    second_api_experiment_repository,
    create_mlflow_run,
):
    mock_gitlab_zip_download.post(
        (
            f"{fake_compute_backend_url}/submit/"
            f"{api_experiment_repository.mlflow_experiment_id}"
        ),
        json=lambda *args, **kwargs: {
            "experiment_id": api_experiment_repository.mlflow_experiment_id,
            "run_id": str(create_mlflow_run()),
            "unicore_job_id": "test-unicore-job-id",
        },
        status_code=status.HTTP_201_CREATED,
    )
    yield mock_gitlab_zip_download


@pytest.fixture
def mock_compute_backend_ssh_unit_testing(
    mock_gitlab_zip_download,
    fake_compute_backend_url,
    api_experiment_repository,
    second_api_experiment_repository,
    create_mlflow_run,
):
    mock_gitlab_zip_download.post(
        (
            f"{fake_compute_backend_url}/ssh/submit/"
            f"{api_experiment_repository.mlflow_experiment_id}"
        ),
        json=lambda *args, **kwargs: {
            "experiment_id": api_experiment_repository.mlflow_experiment_id,
            "run_id": str(create_mlflow_run()),
            "job_id": "test-unicore-job-id",
        },
        status_code=status.HTTP_201_CREATED,
    )
    yield mock_gitlab_zip_download
