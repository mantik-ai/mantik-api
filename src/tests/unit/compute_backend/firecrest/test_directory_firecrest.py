import mantik_api.compute_backend.firecrest._directory as _directory
import mantik_api.testing as testing


class TestDirectory:
    def test_list_all_files_recursively(self):
        expected = [
            testing.firecrest.TEST_RUN_DIR / testing.firecrest.TEST_LOGS_FILE_NAME,
            testing.firecrest.TEST_RUN_DIR
            / f"sub/folder/{testing.firecrest.TEST_FILE_NAME}",
        ]
        directory = _directory.Directory(
            client=testing.firecrest.FakeClient(),
            machine=testing.firecrest.TEST_MACHINE,
            path=testing.firecrest.TEST_RUN_DIR,
        )

        files = directory.get_files_recursively()
        result = [file.path for file in files]

        assert result == expected
