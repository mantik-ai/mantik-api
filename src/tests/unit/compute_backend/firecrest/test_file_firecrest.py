import pytest

import mantik_api.compute_backend.firecrest._file as _file
import mantik_api.testing as testing


class TestFile:
    @pytest.mark.parametrize(
        "simple_download_fails",
        [False, True],
        ids=[
            "Download via `firecrest.Firecrest.simple_download()`",
            "Download via `firecrest.Firecrest.external_download()`",
        ],
    )
    def test_download_via_external_download(self, simple_download_fails):
        file = _file.File(
            client=testing.firecrest.FakeClient(
                simple_download_fails=simple_download_fails
            ),
            machine=testing.firecrest.TEST_MACHINE,
            path=testing.firecrest.TEST_LOGS_FILE_PATH,
        )
        expected = testing.firecrest.TEST_LOGS_CONTENT.decode("utf-8")

        download = file.download()

        result = testing.stream.byte_chunks_to_string(download.get_content())

        assert result == expected, "Incorrect file content"
        assert download.media_type == "application/octet-stream", "Incorrect media type"
        assert (
            download.headers["Content-Disposition"]
            == f"attachment;filename={testing.firecrest.TEST_LOGS_FILE_NAME}"
        ), "Incorrect file name"
