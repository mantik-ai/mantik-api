import pathlib
import uuid
import zipfile

import pytest

import mantik_api.compute_backend.firecrest.job as _job
import mantik_api.database as database
import mantik_api.models as models
import mantik_api.testing as testing


def create_firecrest_run_info(
    state, cpu_time, elapsed_time, start_time, time, time_left, termination_time
):
    return models.run_info.Firecrest(
        id="test-job-id",
        name="test-job",
        nodelist="None assigned",
        nodes="1",
        partition="test-partition",
        user="test-user",
        exit_code="0:0",
        state=state,
        cpu_time=cpu_time,
        elapsed_time=elapsed_time,
        start_time=start_time,
        time=time,
        time_left=time_left,
        termination_time=termination_time,
    )


class TestJob:
    @pytest.mark.parametrize(
        ("status", "expected"),
        [
            ("PENDING", database.run.RunStatus.SCHEDULED),
            ("RUNNING", database.run.RunStatus.RUNNING),
            ("COMPLETING", database.run.RunStatus.RUNNING),
            ("COMPLETED", database.run.RunStatus.FINISHED),
            ("FAILED", database.run.RunStatus.FAILED),
            # firecREST returns some number with the CANCELLED status.
            ("CANCELLED by 1234", database.run.RunStatus.KILLED),
        ],
    )
    def test_status(self, status: str, expected: database.run.RunStatus):
        job = _job.Job(
            client=testing.firecrest.FakeClient(
                job_status=status,
            ),
            machine=testing.firecrest.TEST_MACHINE,
            job_id=testing.firecrest.TEST_JOB_ID,
            run_dir=testing.firecrest.TEST_RUN_DIR,
        )

        result = job.get_status()

        assert result == expected

    @pytest.mark.parametrize(
        ("job_status", "expected_time_parameters"),
        [
            (
                "PENDING",
                {
                    "cpu_time": "00:00:00",
                    "elapsed_time": "00:00:00",
                    "start_time": "Unknown",
                    "termination_time": "Unknown",
                    "time": "00:00:00",
                    "time_left": "Unknown",
                },
            ),
            (
                "RUNNING",
                {
                    "cpu_time": "00:08:48",
                    "elapsed_time": "00:00:11",
                    "start_time": "2023-01-01T00:00:00",
                    "termination_time": "Unknown",
                    "time": "00:08:48",
                    "time_left": "Unknown",
                },
            ),
            (
                "COMPLETED",
                {
                    "cpu_time": "00:12:00",
                    "elapsed_time": "00:00:15",
                    "start_time": "2023-01-01T00:00:00",
                    "termination_time": "2023-01-01T00:00:12",
                    "time": "00:12:00",
                    "time_left": "2023-01-01T00:00:12",
                },
            ),
        ],
    )
    def test_get_info(self, job_status, expected_time_parameters):
        job = _job.Job(
            client=testing.firecrest.FakeClient(
                job_status=job_status,
            ),
            machine=testing.firecrest.TEST_MACHINE,
            job_id=testing.firecrest.TEST_JOB_ID,
            run_dir=testing.firecrest.TEST_RUN_DIR,
        )

        expected = create_firecrest_run_info(
            state=job_status, **expected_time_parameters
        )

        result = job.get_info()

        assert result == expected

    @pytest.mark.parametrize(
        "simple_download_fails",
        [False, True],
        ids=[
            "Download via `firecrest.Firecrest.simple_download()`",
            "Download via `firecrest.Firecrest.external_download()`",
        ],
    )
    def test_get_logs(self, simple_download_fails):
        job = _job.Job(
            client=testing.firecrest.FakeClient(
                simple_download_fails=simple_download_fails
            ),
            machine=testing.firecrest.TEST_MACHINE,
            job_id=testing.firecrest.TEST_JOB_ID,
            run_dir=testing.firecrest.TEST_RUN_DIR,
        )
        expected = testing.firecrest.TEST_LOGS_CONTENT.decode("utf-8").split("\n")

        result = job.get_logs()

        assert result == expected

    def test_get_logs_when_get_info_fails(self):
        job = _job.Job(
            client=testing.firecrest.FakeClient(poll_fails=True),
            machine=testing.firecrest.TEST_MACHINE,
            job_id=testing.firecrest.TEST_JOB_ID,
            run_dir=testing.firecrest.TEST_RUN_DIR,
        )
        expected = testing.firecrest.TEST_LOGS_CONTENT.decode("utf-8").split("\n")

        result = job.get_logs()

        assert result == expected

    @pytest.mark.parametrize(
        ("path", "expected_file_name", "expected_content"),
        [
            (
                testing.firecrest.TEST_LOGS_FILE_NAME,
                testing.firecrest.TEST_LOGS_FILE_NAME,
                testing.firecrest.TEST_LOGS_CONTENT,
            ),
            (
                f"sub/folder/{testing.firecrest.TEST_FILE_NAME}",
                testing.firecrest.TEST_FILE_NAME,
                testing.firecrest.TEST_FILE_CONTENT,
            ),
        ],
    )
    def test_download_file(self, path, expected_file_name, expected_content):
        expected_content = expected_content.decode()

        job = _job.Job(
            client=testing.firecrest.FakeClient(
                download_status=testing.firecrest.DownloadStatus.SUCCESS,
            ),
            machine=testing.firecrest.TEST_MACHINE,
            job_id=testing.firecrest.TEST_JOB_ID,
            run_dir=testing.firecrest.TEST_RUN_DIR,
        )

        download = job.download(pathlib.Path(path))

        result = testing.stream.byte_chunks_to_string(download.get_content())

        assert result == expected_content
        assert download.media_type == "application/octet-stream"
        assert (
            download.headers["Content-Disposition"]
            == f"attachment;filename={expected_file_name}"
        )

    @pytest.mark.parametrize("path", [None, pathlib.Path("/")])
    def test_download_entire_job_folder(self, path):
        expected_archive_name = testing.firecrest.TEST_RUN_ID
        expected_logs = b"hello\n"
        expected_txt = testing.firecrest.TEST_FILE_CONTENT
        expected_files = [
            f"{testing.firecrest.TEST_RUN_ID}/{testing.firecrest.TEST_LOGS_FILE_NAME}",
            f"{testing.firecrest.TEST_RUN_ID}/sub/folder/{testing.firecrest.TEST_FILE_NAME}",  # noqa: E501
        ]

        job = _job.Job(
            client=testing.firecrest.FakeClient(
                download_status=testing.firecrest.DownloadStatus.SUCCESS,
            ),
            machine=testing.firecrest.TEST_MACHINE,
            job_id=testing.firecrest.TEST_JOB_ID,
            run_dir=testing.firecrest.TEST_RUN_DIR,
        )
        download = job.download(path)

        result = testing.stream.chunks_to_bytes(download.get_content())

        with zipfile.ZipFile(result) as zip_file:
            result_files = zip_file.namelist()
            with zip_file.open(f"{expected_archive_name}/mantik.log") as log_file:
                result_logs = log_file.read()
            with zip_file.open(
                f"{expected_archive_name}/sub/folder/test.txt"
            ) as txt_file:
                result_txt_file = txt_file.read()

        assert all(file in result_files for file in expected_files)
        assert result_logs == expected_logs
        assert result_txt_file == expected_txt
        assert download.media_type == "application/x-zip-compressed"
        assert (
            download.headers["Content-Disposition"]
            == f"attachment;filename={expected_archive_name}.zip"
        )

    def test_cancel(self):
        job = _job.Job(
            client=testing.firecrest.FakeClient(),
            machine=testing.firecrest.TEST_MACHINE,
            job_id=testing.firecrest.TEST_JOB_ID,
            run_dir=testing.firecrest.TEST_RUN_DIR,
        )
        expected = database.run.RunStatus.KILLED

        job.cancel()

        result = job.get_status()
        assert result == expected


@pytest.mark.parametrize(
    ("machine", "expected"),
    [
        ("daint", "/scratch/snx3000"),
        ("eiger", "/capstor/scratch/cscs"),
        (testing.firecrest.TEST_MACHINE, "/home"),
    ],
)
def test_infer_run_dir(machine, expected):
    run_id = uuid.uuid4()
    expected = (
        pathlib.Path(expected) / f"{testing.firecrest.TEST_USER}/mantik/{run_id.hex}"
    )

    client = testing.firecrest.FakeClient()

    result = _job._infer_run_dir(client=client, machine=machine, mlflow_run_id=run_id)

    assert result == expected
