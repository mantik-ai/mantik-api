import functools

import firecrest
import pytest

import mantik_api.compute_backend.firecrest.connect as _connect
import mantik_api.compute_backend.firecrest.exceptions as _exceptions
import mantik_api.testing as testing


@pytest.mark.parametrize(
    ("login_successful", "expected"),
    [
        (False, _exceptions.AuthenticationFailedException()),
        (True, testing.firecrest.FakeClient),
    ],
)
def test_connect_to_firecrest_api(
    monkeypatch, expect_raise_if_exception, login_successful, expected, api_connection
):
    monkeypatch.setattr(
        firecrest,
        "Firecrest",
        testing.firecrest.FakeClient,
    )
    monkeypatch.setattr(
        firecrest,
        "ClientCredentialsAuth",
        functools.partial(
            testing.firecrest.FakeClientCredentialsAuth,
            login_successful=login_successful,
        ),
    )
    with expect_raise_if_exception(expected):
        result = _connect.connect_to_firecrest_api(
            api_url="test-api-url",
            auth=api_connection.to_firecrest_credentials(token_url="test-token-url"),
        )
        assert isinstance(result, expected)
