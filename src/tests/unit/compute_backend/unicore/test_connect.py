import pytest
import pyunicore.client
import pyunicore.credentials
import requests.exceptions
import starlette.status as status

import mantik_api.compute_backend.unicore as unicore
import mantik_api.testing as testing


@pytest.mark.parametrize(
    ("exception", "expected"),
    [
        (
            requests.exceptions.ConnectionError(),
            unicore.exceptions.AuthenticationFailedException(),
        ),
        (
            pyunicore.credentials.AuthenticationFailedException(),
            unicore.exceptions.AuthenticationFailedException(),
        ),
        (
            requests.exceptions.HTTPError(
                response=testing.requests.FakeResponse(
                    status_code=status.HTTP_403_FORBIDDEN
                )
            ),
            unicore.exceptions.AuthenticationFailedException(),
        ),
        (
            requests.exceptions.HTTPError(
                response=testing.requests.FakeResponse(
                    status_code=status.HTTP_404_NOT_FOUND
                )
            ),
            requests.exceptions.HTTPError(),
        ),
    ],
)
def test_connect_to_unicore_api_raises_errors(
    monkeypatch, expect_raise_if_exception, api_connection, exception, expected
):
    monkeypatch.setattr(
        pyunicore.client, "Client", testing.unicore.FakeClient(raises=exception)
    )
    with expect_raise_if_exception(expected):
        unicore.connect.connect_to_unicore_api(
            url="test-url", credentials=api_connection.to_unicore_credentials()
        )


@pytest.mark.parametrize(
    ("login", "expected"),
    [
        ({}, True),
        ({"test_login_info": "test_login"}, False),
    ],
)
def test_authentication_failed(login, expected):
    client = testing.unicore.FakeClient()
    client.add_login_info(login)

    result = unicore.connect._authentication_failed(client)

    assert result == expected
