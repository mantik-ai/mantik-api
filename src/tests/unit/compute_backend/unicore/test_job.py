import pytest

import mantik_api.compute_backend as compute_backend
import mantik_api.compute_backend.unicore as unicore
import mantik_api.models as models
import mantik_api.testing as testing


@pytest.fixture()
def job() -> unicore.job.Job:
    unicore_job = testing.unicore.FakeJob()
    return unicore.job.Job(unicore_job)


@pytest.fixture()
def job_without_details() -> unicore.job.Job:
    unicore_job = testing.unicore.FakeJobWithoutDetails()
    return unicore.job.Job(unicore_job)


class TestJob:
    def test_id(self, job):
        assert job.id == "test-job"

    def test_get_status(self, job):
        job.get_status()

    def test_get_properties(self, job):
        result = job.get_info()

        assert result.status == models.run_info.UnicoreStatus.SUCCESSFUL

    @pytest.mark.parametrize(
        ("existing_files", "expected"),
        [
            (
                [compute_backend.job.APPLICATION_LOGS_FILE],
                [
                    "============= UNICORE API LOGS ============",
                    "<UNICORE logs that span several characters>",
                    "============= APPLICATION LOGS ============",
                    "Stat mantik.log",
                ],
            ),
            (
                ["logs-missing"],
                [
                    "=============================== UNICORE API LOGS ===============================",  # noqa: E501
                    "<UNICORE logs that span several characters>",
                    "=============================== APPLICATION LOGS ===============================",  # noqa: E501
                    "Run not yet executed or failed!",
                    "Assumed logs (stdout and stderr) to be written to mantik.log, but no such file was found in the working directory of the job.",  # noqa: E501
                    "Check above UNICORE API logs, job status and submission info, or contact your system administrator.",  # noqa: E501
                ],
            ),
        ],
    )
    def test_get_logs(self, existing_files, expected):
        unicore_job = testing.unicore.FakeJob(existing_files=existing_files)
        job = unicore.job.Job(unicore_job)

        result = job.get_logs()

        assert result == expected

    def test_get_properties_without_details(self, job_without_details):
        result = job_without_details.get_info()

        assert result.status == models.run_info.UnicoreStatus.SUCCESSFUL
