import mantik_api.aws.scheduler as scheduler


class TestClient:
    def test_create(self, scheduler_client, run_schedule_properties):
        scheduler_client.create(run_schedule_properties)

    def test_update(self, scheduler_client, run_schedule_properties):
        scheduler_client.create(run_schedule_properties)
        scheduler_client.update(run_schedule_properties)

    def test_delete(self, scheduler_client, run_schedule_properties):
        scheduler_client.create(run_schedule_properties)
        scheduler_client.delete(
            scheduler.schedule.PropertiesDelete(id=run_schedule_properties.id)
        )
