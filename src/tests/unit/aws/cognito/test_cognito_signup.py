import mantik_api.aws.cognito as cognito
import mantik_api.testing as testing


def test_resend_confirmation_code(cognito_patcher):
    cognito_patcher.patch_resend_confirmation_code(
        username=testing.aws.cognito.COGNITO_USERNAME
    )
    cognito.signup.resend_confirmation_code(
        username=testing.aws.cognito.COGNITO_USERNAME
    )
