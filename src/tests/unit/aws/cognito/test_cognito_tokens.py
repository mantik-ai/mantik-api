import pytest

import mantik_api.aws.cognito as cognito
import mantik_api.aws.cognito.tokens as tokens
import mantik_api.testing as testing


@pytest.mark.parametrize(
    (
        "cognito_response",
        "expected",
    ),
    [
        (
            "cognito_auth_response",
            None,
        ),
        (
            "cognito_user_not_found_response",
            cognito.exceptions.AuthenticationFailedException(),
        ),
        (
            "cognito_incorrect_login_credentials_response",
            cognito.exceptions.AuthenticationFailedException(),
        ),
    ],
)
def test_cognito_auth_response_get_token(
    cognito_patcher,
    request,
    cognito_response,
    expected,
    expect_raise_if_exception,
):
    credentials = cognito.credentials.CreateTokenCredentials(
        cognito_username=testing.aws.cognito.COGNITO_USERNAME,
        password="test-password",
    )

    fake_response = request.getfixturevalue(cognito_response)
    (
        error_code,
        error_message,
    ) = testing.aws.cognito.get_error_code_and_message_from_json(fake_response)

    cognito_patcher.patch_initiate_auth_get_token(
        user_name=testing.aws.cognito.COGNITO_USERNAME,
        password=credentials.password,
        response=fake_response,
        error_code=error_code,
        error_message=error_message,
    )

    with expect_raise_if_exception(expected):
        result = tokens.get_tokens(credentials)

        expected = tokens.Tokens.from_json_response(fake_response)
        assert result == expected


@pytest.mark.parametrize(
    (
        "cognito_response",
        "expected",
    ),
    [
        (
            "cognito_refresh_response",
            None,
        ),
        (
            "cognito_refresh_token_expired_response",
            cognito.exceptions.AuthenticationFailedException(),
        ),
    ],
)
def test_cognito_auth_response_refresh_token(
    cognito_patcher,
    request,
    cognito_response,
    expected,
    expect_raise_if_exception,
):
    credentials = cognito.credentials.RefreshTokenCredentials(
        cognito_username=testing.aws.cognito.COGNITO_USERNAME,
        refresh_token="test-refresh-token",
    )

    fake_response = request.getfixturevalue(cognito_response)
    (
        error_code,
        error_message,
    ) = testing.aws.cognito.get_error_code_and_message_from_json(fake_response)
    cognito_patcher.patch_initiate_auth_refresh_token(
        refresh_token=credentials.refresh_token,
        response=fake_response,
        error_code=error_code,
        error_message=error_message,
    )

    with expect_raise_if_exception(expected):
        result = tokens.get_tokens(credentials)

        expected = tokens.Tokens.from_json_response(
            fake_response, refresh_token=credentials.refresh_token
        )
        assert result == expected
