import uuid

import pytest

import mantik_api.aws.cognito as cognito
import mantik_api.testing as testing


@pytest.fixture()
def user_attributes() -> cognito.users.GetUserResponse:
    return cognito.users.GetUserResponse(
        user_name="test-user",
        user_attributes=[
            cognito.users.Attribute.from_json(attr)
            for attr in [
                {"Name": "name", "Value": "username"},
                {"Name": "email", "Value": "user@email.com"},
            ]
        ],
    )


class TestGetUserResponse:
    @pytest.mark.parametrize(
        ("name", "expected"), [("name", "username"), ("email", "user@email.com")]
    )
    def test_get_user_attribute(self, user_attributes, name, expected):
        result = user_attributes.get_attribute(name)

        assert result == expected

    def test_get_user_attribute_fails(self, expect_raise_if_exception, user_attributes):
        with expect_raise_if_exception(LookupError()):
            user_attributes.get_attribute("doesnt-exist")


def test_create_cognito_user(moto_user_pool):
    data = {
        "username": "test-user",
        "password": "Password.1234",
        "email": "test-email",
    }

    new_user_id = cognito.users.create_cognito_user(**data)
    assert isinstance(new_user_id, str)

    uuid.UUID(new_user_id)  # confirm that casting to uuid doesn't fail


def test_change_password_cognito_user(
    boto3_cognito_idp_client, create_and_confirm_fake_cognito_user
):
    username = create_and_confirm_fake_cognito_user["username"]
    old_password = create_and_confirm_fake_cognito_user["password"]
    new_password = "NewPassword.1234"
    token, credentials = testing.aws.cognito.get_access_token_and_credentials(
        username=username, password=old_password
    )
    cognito.users.change_password_cognito_user(
        old_password=old_password,
        new_password=new_password,
        token=token,
    )

    new_credentials = cognito.credentials.CreateTokenCredentials(
        cognito_username=username, password=new_password
    )

    # Check that access token can be retrieved with new credentials
    cognito.tokens.get_tokens(new_credentials)


@pytest.mark.parametrize(
    (
        "old_password",
        "new_password",
    ),
    [(testing.aws.cognito.PASSWORD, "1"), ("IncorrectPassword", "Password.1234")],
)
def test_change_password_cognito_user_fails(
    boto3_cognito_idp_client,
    old_password,
    new_password,
    expect_raise_if_exception,
    create_and_confirm_fake_cognito_user,
):
    username = create_and_confirm_fake_cognito_user["username"]
    password = create_and_confirm_fake_cognito_user["password"]

    with expect_raise_if_exception(
        cognito.exceptions.UserPasswordUpdateFailedException()
    ):
        token, credentials = testing.aws.cognito.get_access_token_and_credentials(
            username, password
        )
        cognito.users.change_password_cognito_user(
            old_password=old_password,
            new_password=new_password,
            token=token,
        )


def test_change_email(boto3_cognito_idp_client, create_and_confirm_fake_cognito_user):
    username = create_and_confirm_fake_cognito_user["username"]
    password = create_and_confirm_fake_cognito_user["password"]
    token, credentials = testing.aws.cognito.get_access_token_and_credentials(
        username=username, password=password
    )
    new_email = "test-email-new"
    cognito.users.initiate_change_email_cognito_user(
        new_email=new_email,
        token=token,
    )
    user = boto3_cognito_idp_client.get_user(AccessToken=token.to_string())
    result = None
    for attribute in user["UserAttributes"]:
        if attribute["Name"] == "email":
            result = attribute["Value"]
            break
    assert result == new_email


def test_initiate_forgot_password(
    boto3_cognito_idp_client, create_and_confirm_fake_cognito_user
):
    username = create_and_confirm_fake_cognito_user["username"]
    response = cognito.users.initiate_forgot_password(username=username)
    assert response["CodeDeliveryDetails"]["DeliveryMedium"] == "EMAIL"


def test_confirm_forgot_password_cognito_user(
    boto3_cognito_idp_client, create_and_confirm_fake_cognito_user
):
    username = create_and_confirm_fake_cognito_user["username"]
    new_password = "New-Test-Password-123"

    initiate_recovery_response = cognito.users.initiate_forgot_password(
        username=username
    )

    confirmation_code = initiate_recovery_response["ResponseMetadata"]["HTTPHeaders"][
        "x-moto-forgot-password-confirmation-code"
    ]
    cognito.users.confirm_forgot_password(
        username=username,
        confirmation_code=confirmation_code,
        new_password=new_password,
    )

    new_credentials = cognito.credentials.CreateTokenCredentials(
        cognito_username=username, password=new_password
    )
    response_user_auth = cognito.tokens._get_tokens_from_cognito(new_credentials)

    assert response_user_auth["ResponseMetadata"]["HTTPStatusCode"] == 200


def test_confirm_forgot_password_cognito_user_invalid(
    boto3_cognito_idp_client,
    create_and_confirm_fake_cognito_user,
    expect_raise_if_exception,
):
    username = create_and_confirm_fake_cognito_user["username"]
    with expect_raise_if_exception(
        cognito.exceptions.UserConfirmForgotPasswordFailedException()
    ):
        cognito.users.confirm_forgot_password(
            username=username,
            confirmation_code="moto-confirmation-code:123invalid",
            new_password="Password.123",
        )
