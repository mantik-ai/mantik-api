import freezegun
import pytest

import mantik_api.aws.cognito as _cognito
import mantik_api.testing as testing
import mantik_api.tokens.exceptions as exceptions
import mantik_api.tokens.verifier as _verifier


@pytest.fixture()
def verifier() -> _verifier.TokenVerifier:
    cognito = _cognito.client.Properties(
        region=testing.token_verifier.FAKE_COGNITO_REGION,
        user_pool_id=testing.token_verifier.FAKE_COGNITO_USER_POOL_ID,
        app_client_id=testing.token_verifier.FAKE_COGNITO_APP_CLIENT_ID,
        app_client_secret=testing.token_verifier.FAKE_COGNITO_APP_CLIENT_SECRET,
    )
    jwks = testing.token_verifier.FakeJWKS.create()
    return _verifier.TokenVerifier(
        cognito=cognito,
        jwks=jwks,
    )


class TestTokenVerifier:
    @pytest.mark.parametrize(
        ("current_time", "token", "expected"),
        [
            (
                testing.token_verifier.NON_EXPIRED_DATETIME,
                testing.token_verifier.FAKE_JWT,
                None,
            ),
            (
                testing.token_verifier.EXPIRED_DATETIME,
                testing.token_verifier.FAKE_JWT,
                exceptions.TokenExpiredException(),
            ),
            (
                testing.token_verifier.NON_EXPIRED_DATETIME,
                testing.token_verifier.FAKE_JWT_INVALID_SIGNATURE,
                exceptions.InvalidSignatureException(),
            ),
            (
                testing.token_verifier.NON_EXPIRED_DATETIME,
                testing.token_verifier.FAKE_JWT_INVALID_CLIENT_ID,
                # Currently unable to test for the expected dedicated exception
                # (`exceptions.InvalidClientException`) since it seems to be
                # impossible to change the payload without breaking the
                # signature.
                exceptions.InvalidSignatureException(),
            ),
            (
                testing.token_verifier.NON_EXPIRED_DATETIME,
                testing.token_verifier.FAKE_JWT_INVALID_ISSUER,
                # See above, dedicated exception here is
                # `exceptions.InvalidIssuerException`
                exceptions.InvalidSignatureException(),
            ),
            (
                testing.token_verifier.NON_EXPIRED_DATETIME,
                testing.token_verifier.FAKE_JWT_INVALID_TOKEN_TYPE,
                # See above, dedicated exception here is
                # `exceptions.IncorrectTokenTypeException`
                exceptions.InvalidSignatureException(),
            ),
        ],
    )
    def test_verify_token(
        self, verifier, current_time, token, expected, expect_raise_if_exception
    ):
        with freezegun.freeze_time(current_time):
            with expect_raise_if_exception(expected):
                verifier.verify_token(token)
