import pytest

import mantik_api.tokens.exceptions as exceptions
import mantik_api.tokens.jwt as jwt


def test_separate_message_and_signature():
    input_string = "foo.bar.baz"
    result = jwt._separate_message_and_signature(input_string)
    expected = ("foo.bar", "baz")
    assert result == expected

    with pytest.raises(exceptions.VerificationFailedException) as e:
        jwt._separate_message_and_signature("abc")
        assert str(e) == "Invalid token"
