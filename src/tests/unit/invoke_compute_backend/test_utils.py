import pathlib

import pytest

import mantik_api.database.git_repository as git_repository
import mantik_api.invoke_compute_backend._git_repo as _git_repo
import mantik_api.invoke_compute_backend.utils as utils


@pytest.mark.parametrize(
    "dir_to_unzip",
    (
        "should-be-unzipped",
        "should-be-unzipped/",
        "./should-be-unzipped",
        "./should-be-unzipped/",
    ),
)
def test_unzip_directory_path(tmp_path, fake_zip_file_path_main, dir_to_unzip):
    """Test that only given sub-folder is unzipped.

    For folder structure of test ZIP file see `src/tests/resources/zip/README.md`.

    """
    dir_to_include = fake_zip_file_path_main.with_suffix("").name / pathlib.Path(
        dir_to_unzip
    )

    with open(fake_zip_file_path_main, "rb") as file:
        result = utils.unzip_directory(
            archive_data=file, path=tmp_path, dir_to_include=dir_to_include
        )

    assert result == tmp_path / dir_to_include
    assert (tmp_path / dir_to_include).exists()
    assert (tmp_path / dir_to_include / "should-be-unzipped.txt").exists()
    assert not (tmp_path / "should-not-be-unzipped.txt").exists()


@pytest.mark.parametrize(
    "dir_to_unzip",
    (
        "should-be-unzipped",
        "should-be-unzipped/",
        "./should-be-unzipped",
        "./should-be-unzipped/",
    ),
)
@pytest.mark.parametrize(
    ("git_url", "platform", "commit", "branch", "fake_zip_path"),
    [
        (
            "fake_github_repo_url",
            git_repository.Platform.GitHub,
            "commit-hash",
            None,
            "fake_zip_file_path_commit_hash_long",
        ),
        (
            "fake_github_repo_url",
            git_repository.Platform.GitHub,
            "commit-hash-long",
            None,
            "fake_zip_file_path_commit_hash_long",
        ),
        (
            "fake_github_repo_url",
            git_repository.Platform.GitHub,
            None,
            "main",
            "fake_zip_file_path_main",
        ),
        (
            "fake_gitlab_repo_url",
            git_repository.Platform.GitLab,
            "commit-hash",
            None,
            "fake_zip_file_path_commit_hash",
        ),
        (
            "fake_gitlab_repo_url",
            git_repository.Platform.GitLab,
            "commit-hash-long",
            None,
            "fake_zip_file_path_commit_hash_long",
        ),
        (
            "fake_gitlab_repo_url",
            git_repository.Platform.GitLab,
            None,
            "main",
            "fake_zip_file_path_main",
        ),
        (
            "fake_bitbucket_org_repo_url",
            git_repository.Platform.Bitbucket,
            "commit-hash",
            None,
            "fake_zip_file_path_commit_hash_long",
        ),
        (
            "fake_bitbucket_org_repo_url",
            git_repository.Platform.Bitbucket,
            "commit-hash-long",
            None,
            "fake_zip_file_path_commit_hash_long",
        ),
        (
            "fake_self_hosted_repo_url",
            git_repository.Platform.Bitbucket,
            "commit-hash",
            "main",
            "fake_zip_file_path_self_hosted",
        ),
    ],
)
def test_unzip_directory(
    monkeypatch,
    mock_mlflow_mlproject_file_path_validation,
    git_url,
    platform,
    commit,
    branch,
    mlflow_mlproject_file_path,
    tmp_path,
    dir_to_unzip,
    fake_zip_path,
    request,
):
    """Test for all types of supported git repositories that the unzipping function
    can find the downloaded directories. Also test that only given sub-folder is
    unzipped, when the zip directory name has type.

    For folder structure of test ZIP file see `src/tests/resources/zip/README.md`.

    """
    fake_zip_path = request.getfixturevalue(fake_zip_path)
    git_url = request.getfixturevalue(git_url)
    monkeypatch.setattr(
        utils, "get_file_from_uri", lambda uri: open(fake_zip_path, "rb")
    )

    with mock_mlflow_mlproject_file_path_validation(
        platform=platform,
        git_url=git_url,
        branch=branch,
        commit=commit,
        path=mlflow_mlproject_file_path,
    ):
        git_repo = _git_repo.GitRepo(
            url=git_url,
            branch=branch,
            commit=commit,
            mlflow_mlproject_file_path=mlflow_mlproject_file_path,
            platform=platform,
            access_token=None,
        )
        result = utils.unzip_directory(
            archive_data=utils.get_file_from_uri(git_repo.zip_archive_url),
            path=tmp_path,
            dir_to_include=git_repo.mlflow_project_path_in_zip,
        )

    dir_to_include_path = fake_zip_path.with_suffix("").name / pathlib.Path(
        dir_to_unzip
    )
    assert result == tmp_path / dir_to_include_path
    assert (tmp_path / dir_to_include_path).exists()
    assert (tmp_path / dir_to_include_path / "should-be-unzipped.txt").exists()
    assert not (tmp_path / "should-not-be-unzipped.txt").exists()
