import pathlib
import uuid

import pytest
import starlette.status as status

import mantik_api.database as database
import mantik_api.invoke_compute_backend._git_repo as _git_repo
import mantik_api.invoke_compute_backend.exceptions as exceptions
import mantik_api.invoke_compute_backend.submit_run as submit_run
import mantik_api.models as models
import mantik_api.testing as testing
from mantik_api.service.runs import _create_request_data_for_unicore_or_firecrest

BACKEND_CONFIG_FILENAME = "compute-backend-config.json"


@pytest.fixture(autouse=True)
def _mock_mlproject_file_path_validation(
    api_add_run_configuration,
    mock_mlflow_mlproject_file_path_validation,
    fake_gitlab_repo_url,
    fake_branch,
    mlflow_mlproject_file_path,
    set_mlflow_tracking_url,
):
    with mock_mlflow_mlproject_file_path_validation(
        git_url=fake_gitlab_repo_url,
        branch=api_add_run_configuration.branch,
        commit=api_add_run_configuration.commit,
        path=mlflow_mlproject_file_path,
    ), set_mlflow_tracking_url():
        yield


def test_get_code_from_code_repository(
    tmp_path, mock_gitlab_zip_download, fake_gitlab_repo_url, mlflow_mlproject_file_path
):
    git_repo = _git_repo.GitRepo(
        fake_gitlab_repo_url,
        branch="main",
        commit=None,
        mlflow_mlproject_file_path=mlflow_mlproject_file_path,
        platform=database.git_repository.Platform.GitLab,
        access_token=None,
    )
    expected = [
        tmp_path / pathlib.Path("test-repo-main"),
        tmp_path / pathlib.Path("test-repo-main/should-be-unzipped"),
        tmp_path
        / pathlib.Path("test-repo-main/should-be-unzipped/should-be-unzipped.txt"),
        tmp_path / pathlib.Path("test-repo-main/should-be-unzipped/MLproject"),
    ]

    submit_run._get_code_from_code_repository(
        tmp_dir=tmp_path,
        git_repo=git_repo,
    )
    result = sorted(tmp_path.glob("**/*"))

    assert result == sorted(expected)


@pytest.fixture
def mock_compute_backend_api_too_large_file_response(
    mock_gitlab_zip_download, fake_compute_backend_url
):
    mock_gitlab_zip_download.post(
        f"{fake_compute_backend_url}/submit/0",
        text="test too large file",
        status_code=status.HTTP_413_REQUEST_ENTITY_TOO_LARGE,
    )


@pytest.fixture
def mock_compute_backend_api_submission_failed(
    mock_gitlab_zip_download, fake_compute_backend_url
):
    mock_gitlab_zip_download.post(
        f"{fake_compute_backend_url}/submit/0",
        text="Error message from Compute Backend API",
        status_code=status.HTTP_400_BAD_REQUEST,
    )


@pytest.fixture()
def fake_add_run(
    fake_gitlab_repo_url,
    experiment_repository_id_str,
    code_repository_id_str,
    connection_id_str,
    user_id_str,
    mlflow_mlproject_file_path,
) -> models.run.AddRun:
    return models.run.AddRun(
        name="test-run",
        experiment_repository_id=experiment_repository_id_str,
        code_repository_id=code_repository_id_str,
        user_id=uuid.UUID(user_id_str),
        branch="main",
        connection_id=connection_id_str,
        mlflow_run_id=uuid.uuid4(),
        compute_budget_account="test-compute-budged-account",
        mlflow_mlproject_file_path=mlflow_mlproject_file_path,
        entry_point="main",
        mlflow_parameters={},
        backend_config={
            "Environment": {
                "Apptainer": {"Path": "/remote/image.sif", "Type": "remote"}
            }
        },
    )


@pytest.fixture()
def request_data_for_unicore_or_firecrest(
    fake_token, fake_add_run, database_connection
):
    request_compute_backend_data = _create_request_data_for_unicore_or_firecrest(
        token=fake_token,
        backend_config_filename=BACKEND_CONFIG_FILENAME,
        run=fake_add_run,
        connection=database_connection,
        environment_variables={"foo": "bar"},
    )

    return request_compute_backend_data


@pytest.fixture()
def request_data_for_ssh(fake_token, fake_add_run, database_connection):
    request_compute_backend_data = _create_request_data_for_unicore_or_firecrest(
        token=fake_token,
        backend_config_filename=BACKEND_CONFIG_FILENAME,
        run=fake_add_run,
        connection=database_connection,
        environment_variables={"foo": "bar"},
    )

    return request_compute_backend_data


def test_submit_run_for_ssh(
    mock_compute_backend_ssh_unit_testing,
    fake_token,
    fake_add_run,
    caplog,
    database_code_repository,
    database_data_repository,
    mock_hvac,
    fake_compute_backend_url,
    request_data_for_ssh,
    api_experiment_repository,
):
    result = submit_run.submit_run(
        token=fake_token,
        run=fake_add_run,
        compute_backend_url=(
            f"{fake_compute_backend_url}/ssh/submit/"
            f"{api_experiment_repository.mlflow_experiment_id}"
        ),
        backend_config_filename=BACKEND_CONFIG_FILENAME,
        code_repository=database_code_repository,
        data_repository=database_data_repository,
        code_connection_git_access_token=None,
        request_compute_backend_data=request_data_for_ssh,
    )
    assert isinstance(result.run_id, uuid.UUID)

    # Assert request was logged
    assert testing.logs.message_in_caplog(
        "Sending request to Compute Backend", caplog
    ), "Request to compute backend not logged"
    # Assert no credentials logged
    assert testing.logs.message_in_caplog(
        f"'{submit_run.COMPUTE_BACKEND_API_PRIVATE_KEY_PARAMETER}': '<masked>'", caplog
    ), "CRITICAL: Private Key password logged"
    assert testing.logs.message_in_caplog(
        f"'{submit_run.COMPUTE_BACKEND_API_SSH_PASSWORD_PARAMETER}': '<masked>'", caplog
    ), "CRITICAL: SSH password logged"


def test_submit_run_for_unicore_or_firecrest(
    mock_compute_backend_api_unicore_or_firecrest_unit_testing,
    fake_token,
    fake_add_run,
    caplog,
    database_code_repository,
    database_data_repository,
    mock_hvac,
    fake_compute_backend_url,
    request_data_for_unicore_or_firecrest,
    api_experiment_repository,
):
    result = submit_run.submit_run(
        token=fake_token,
        run=fake_add_run,
        compute_backend_url=(
            f"{fake_compute_backend_url}/submit/"
            f"{api_experiment_repository.mlflow_experiment_id}"
        ),
        backend_config_filename=BACKEND_CONFIG_FILENAME,
        code_repository=database_code_repository,
        data_repository=database_data_repository,
        code_connection_git_access_token=None,
        request_compute_backend_data=request_data_for_unicore_or_firecrest,
    )
    assert isinstance(result.run_id, uuid.UUID)

    # Assert request was logged
    assert testing.logs.message_in_caplog(
        "Sending request to Compute Backend", caplog
    ), "Request to compute backend not logged"
    # Assert no credentials logged
    assert testing.logs.message_in_caplog(
        f"'{submit_run.COMPUTE_BACKEND_API_USER_PARAMETER}': '<masked>'", caplog
    ), "CRITICAL: External HPC API password logged"
    assert testing.logs.message_in_caplog(
        f"'{submit_run.COMPUTE_BACKEND_API_PASSWORD_PARAMETER}': '<masked>'", caplog
    ), "CRITICAL: External HPC API password logged"


def test_submit_run_too_large_file_response(
    mock_compute_backend_api_too_large_file_response,
    fake_token,
    fake_add_run,
    caplog,
    database_code_repository,
    database_data_repository,
    mock_hvac,
    fake_compute_backend_url,
    api_experiment_repository,
    request_data_for_unicore_or_firecrest,
):
    with pytest.raises(exceptions.InvokeComputeBackendException) as exc:
        submit_run.submit_run(
            token=fake_token,
            run=fake_add_run,
            code_repository=database_code_repository,
            data_repository=database_data_repository,
            code_connection_git_access_token=None,
            compute_backend_url=(
                f"{fake_compute_backend_url}/submit/"
                f"{api_experiment_repository.mlflow_experiment_id}"
            ),
            backend_config_filename=BACKEND_CONFIG_FILENAME,
            request_compute_backend_data=request_data_for_unicore_or_firecrest,
        )

    assert (
        "The files you submitted were too large. "
        "Please consider transferring large files "
        "(such as image files) manually, e.g. with scp. "
        "You will also have to change the backend "
        "configuration to use a remote image. "
        "Then you can try to re-submit the job." in str(exc.value)
    )


@pytest.mark.usefixtures("mock_compute_backend_api_submission_failed")
def test_submit_run_submission_failed(
    fake_token,
    fake_add_run,
    fake_compute_backend_url,
    caplog,
    database_code_repository,
    database_data_repository,
    database_connection,
    mock_hvac,
    request_data_for_unicore_or_firecrest,
    api_experiment_repository,
):
    with pytest.raises(exceptions.InvokeComputeBackendException) as exc:
        submit_run.submit_run(
            token=fake_token,
            run=fake_add_run,
            compute_backend_url=(
                f"{fake_compute_backend_url}/submit/"
                f"{api_experiment_repository.mlflow_experiment_id}"
            ),
            code_repository=database_code_repository,
            data_repository=database_data_repository,
            code_connection_git_access_token=None,
            request_compute_backend_data=request_data_for_unicore_or_firecrest,
            backend_config_filename=BACKEND_CONFIG_FILENAME,
        )

    assert "Error message from Compute Backend" in str(exc.value)


def test_mask_credentials_for_logging():
    """Test that the mask function doesn't alter the original data.

    If it alters the original data, this would also affect the data send
    with the request to the Compute Backend.

    """
    data = {
        submit_run.COMPUTE_BACKEND_API_USER_PARAMETER: "test-user",
        submit_run.COMPUTE_BACKEND_API_PASSWORD_PARAMETER: "test-password",
    }

    result = submit_run._mask_credentials_for_logging(data)

    assert result[submit_run.COMPUTE_BACKEND_API_USER_PARAMETER] == "<masked>"
    assert data[submit_run.COMPUTE_BACKEND_API_USER_PARAMETER] == "test-user"

    assert result[submit_run.COMPUTE_BACKEND_API_USER_PARAMETER] == "<masked>"
    assert data[submit_run.COMPUTE_BACKEND_API_PASSWORD_PARAMETER] == "test-password"
