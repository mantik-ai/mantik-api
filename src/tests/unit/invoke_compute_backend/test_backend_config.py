import json
import pathlib

import pytest

import mantik_api.database as database
import mantik_api.database.run
import mantik_api.invoke_compute_backend.backend_config as backend_config
import mantik_api.invoke_compute_backend.exceptions as exceptions

FAKE_SIF_FILE = "image.sif"
BACKEND_CONFIG_FILENAME = "compute-backend-config.json"


@pytest.mark.parametrize(
    "config",
    (
        {},
        {
            "Environment": {
                "Variables": {"MANTIK_GIT_REF": "https://gitlab.com/test-uri"}
            },
        },
    ),
)
def test_generate_backend_config_without_execution_environment(tmp_path, config):
    backend_config.generate_backend_config_from_run(
        config=config,
        git_ref="test-commit",
        tmp_dir=tmp_path,
        filename=BACKEND_CONFIG_FILENAME,
    )

    _assert_config_correct(tmp_path, expected=config)


def test_generate_backend_config_with_python_env(tmp_path):
    config = {
        "Environment": {
            "Python": "venv",
            "Variables": {"MANTIK_GIT_REF": "test-commit"},
        },
    }

    backend_config.generate_backend_config_from_run(
        config=config,
        git_ref="test-commit",
        tmp_dir=tmp_path,
        filename=BACKEND_CONFIG_FILENAME,
    )

    _assert_config_correct(tmp_path, expected=config)


def _assert_config_correct(tmp_path: pathlib.Path, expected: dict) -> None:
    assert (tmp_path / "compute-backend-config.json").exists()
    assert (tmp_path / "compute-backend-config.json").is_file()

    with open(tmp_path / "compute-backend-config.json") as config_file:
        result = json.load(config_file)
        assert result == expected


def test_generate_backend_config_from_run_with_remote_image(tmp_path):
    config = {
        "Environment": {
            "Apptainer": {
                "Path": "is-remote",
                "Type": "remote",
            },
            "Variables": {"MANTIK_GIT_REF": "test-commit"},
        },
    }

    backend_config.generate_backend_config_from_run(
        config=config,
        git_ref="test-commit",
        tmp_dir=tmp_path,
        filename=BACKEND_CONFIG_FILENAME,
    )

    _assert_config_correct(tmp_path, expected=config)


@pytest.mark.parametrize(
    "config",
    (
        {
            "Environment": {
                "Apptainer": {
                    "Path": FAKE_SIF_FILE,
                    "Type": "local",
                },
                "Variables": {
                    "MLFLOW_RUN_ID": "5707c97a-1e3f-409e-9527-be5ce7255723",
                    "MANTIK_GIT_REF": "test-commit",
                },
            },
        },
        {
            "Environment": {"Apptainer": FAKE_SIF_FILE},
        },
    ),
)
def test_generate_backend_config_from_run_with_local_image_in_repo(tmp_path, config):
    open(tmp_path / FAKE_SIF_FILE, "a").close()

    backend_config.generate_backend_config_from_run(
        config=config,
        git_ref="test-commit",
        tmp_dir=tmp_path,
        filename=BACKEND_CONFIG_FILENAME,
    )

    _assert_config_correct(tmp_path, expected=config)


@pytest.fixture()
def run_database_model_with_test_env_var(run_database_model):
    run_database_model.backend_config = {
        "UnicoreApiUrl": "https://zam2125.zam.kfa-juelich.de:9112/JUWELS/rest/core",
        "Environment": {
            "Apptainer": {
                "Path": "/remote/image.sif",
                "Type": "remote",
            },
            "Variables": {
                "MLFLOW_RUN_ID": "5707c97a-1e3f-409e-9527-be5ce7255723",
                "TEST": "test",
                "Variables": {"MANTIK_GIT_REF": "test-commit"},
            },
        },
        "Resources": {"Queue": "devel", "Nodes": 1},
    }
    return run_database_model


@pytest.mark.parametrize(
    "config",
    (
        {
            "Environment": {
                "Apptainer": {
                    "Path": FAKE_SIF_FILE,
                    "Type": "local",
                },
                "Variables": {"MANTIK_GIT_REF": "test-commit"},
            },
        },
        {
            "Environment": {
                "Apptainer": FAKE_SIF_FILE,
                "Variables": {"MANTIK_GIT_REF": "test-commit"},
            },
        },
    ),
)
def test_generate_backend_config_from_run_with_missing_image(tmp_path, config):
    expected = (
        "Given Apptainer image 'image.sif' not found in MLproject directory. "
        "Following files found: 'compute-backend-config.json'"
    )

    with pytest.raises(exceptions.InvokeComputeBackendException) as exc:
        backend_config.generate_backend_config_from_run(
            config=config,
            git_ref="test-commit",
            tmp_dir=tmp_path,
            filename=BACKEND_CONFIG_FILENAME,
        )

    assert expected in str(exc.value)


def test_inject_environment_into_backend_config(
    database_environment: database.environment.Environment,
) -> None:
    config = {
        "Environment": {
            "Apptainer": {
                "Path": "is-remote",
                "Type": "remote",
            }
        },
    }
    expected = {
        "Environment": {
            "Apptainer": {"Path": "https://gitlab.com/test-uri", "Type": "local"},
            "Variables": {"ENV_VAR_1": "VALUE_1"},
        }
    }

    result = mantik_api.database.run.inject_environment_into_backend_config(
        environment=database_environment, backend_config=config
    )
    assert result == expected
