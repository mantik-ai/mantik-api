import pathlib

import pytest

import mantik_api.database.git_repository as git_repository
import mantik_api.invoke_compute_backend._git_repo as _git_repo
import mantik_api.invoke_compute_backend.exceptions as exceptions


class TestGitRepo:
    @pytest.mark.deprecated(
        "branch and commit are now optional because code " "repository is."
    )
    @pytest.mark.parametrize(
        ("branch", "commit", "expected"),
        [
            (None, None, "No branch or commit given"),
            ("", None, "No branch or commit given"),
            (None, "", "No branch or commit given"),
            ("", "", "No branch or commit given"),
        ],
    )
    def test_branch_and_commit_cannot_be_emtpy(
        self, expect_raise_if_exception, branch, commit, expected
    ):
        with expect_raise_if_exception(ValueError()) as e:
            _git_repo.GitRepo(
                url="test-url",
                branch=branch,
                commit=commit,
                mlflow_mlproject_file_path=pathlib.Path("test/path"),
                platform=git_repository.Platform.GitLab,
                access_token=None,
            )

            assert expected in str(e)

    @pytest.mark.parametrize(
        ("git_url", "commit", "platform", "expected_raw_url_path"),
        (
            (
                "fake_gitlab_repo_url",
                None,
                git_repository.Platform.GitLab,
                "https://gitlab.com/{git_repo}/raw/{ref}/{mlflow_mlproject_file_path}",
            ),
            (
                "fake_gitlab_repo_url",
                "test-commit",
                git_repository.Platform.GitLab,
                "https://gitlab.com/{git_repo}/raw/{ref}/{mlflow_mlproject_file_path}",
            ),
            (
                "fake_github_repo_url",
                None,
                git_repository.Platform.GitHub,
                "https://raw.githubusercontent.com/{git_repo}/{ref}/{mlflow_mlproject_file_path}",  # noqa: E501
            ),
            (
                "fake_github_repo_url",
                "test-commit",
                git_repository.Platform.GitHub,
                "https://raw.githubusercontent.com/{git_repo}/{ref}/{mlflow_mlproject_file_path}",  # noqa: E501
            ),
            (
                "fake_bitbucket_org_repo_url",
                "test-commit",
                git_repository.Platform.Bitbucket,
                "https://bitbucket.org/{git_repo}/raw/{mlflow_mlproject_file_path}?at={ref}",  # noqa: E501
            ),
            (
                "fake_self_hosted_repo_url",
                "test-commit",
                git_repository.Platform.Bitbucket,
                "https://test-host/browse/{mlflow_mlproject_file_path}?at={ref}",
            ),
        ),
    )
    def test_validate_mlproject_file_path_on_construction(
        self,
        request,
        mock_mlflow_mlproject_file_path_validation,
        fake_branch,
        mlflow_mlproject_file_path,
        git_url,
        commit,
        platform,
        expected_raw_url_path,
        caplog,
    ):
        git_url = request.getfixturevalue(git_url)

        expected_error = (
            "Invalid relative path to MLflow project file inside repository"
            # Commit should always be preferred over branch.
            f" {git_url} (ref: {commit or fake_branch}): "
            f"{mlflow_mlproject_file_path}"
        )

        expected_raw_url_path = expected_raw_url_path.format(
            mlflow_mlproject_file_path=mlflow_mlproject_file_path,
            ref=commit,
            git_repo="test-account/test-repo",
        )

        with mock_mlflow_mlproject_file_path_validation(
            git_url=git_url,
            platform=platform,
            branch=fake_branch,
            commit=commit,
            status_code=404,
        ) as mocked:
            with pytest.raises(exceptions.InvokeComputeBackendException) as e:
                _git_repo.GitRepo(
                    url=git_url,
                    branch=fake_branch,
                    commit=commit,
                    platform=platform,
                    mlflow_mlproject_file_path=mlflow_mlproject_file_path,
                    access_token=None,
                )
                assert expected_raw_url_path in caplog.messages

            assert str(e.value) == expected_error

            assert mocked.call_count == 1

    @pytest.mark.parametrize(
        ("git_url", "branch", "commit", "platform", "expected"),
        (
            (
                "fake_gitlab_repo_url",
                "test-branch",
                None,
                git_repository.Platform.GitLab,
                "test-repo-test-branch",
            ),
            (
                "fake_gitlab_repo_url",
                "other-branch",
                "test-commit",
                git_repository.Platform.GitLab,
                "test-repo-test-commit",
            ),
            (
                "fake_github_repo_url",
                "test-branch",
                None,
                git_repository.Platform.GitHub,
                "test-branch",
            ),
            (
                "fake_github_repo_url",
                "test-branch",
                "test-commit",
                git_repository.Platform.GitHub,
                "test-commit",
            ),
            (
                "fake_bitbucket_org_repo_url",
                "test-branch",
                "test-commit",
                git_repository.Platform.Bitbucket,
                "test-commit",
            ),
            (
                "fake_self_hosted_repo_url",
                "test-branch",
                "test-commit",
                git_repository.Platform.Bitbucket,
                "test-commit",
            ),
        ),
    )
    def test_zip_file_name_in_url(
        self,
        request,
        mock_mlflow_mlproject_file_path_validation,
        mlflow_mlproject_file_path,
        git_url,
        branch,
        commit,
        expected,
        platform,
    ):
        git_url = request.getfixturevalue(git_url)

        with mock_mlflow_mlproject_file_path_validation(
            platform=platform, git_url=git_url, branch=branch, commit=commit
        ):
            git_repo = _git_repo.GitRepo(
                url=git_url,
                branch=branch,
                commit=commit,
                mlflow_mlproject_file_path=mlflow_mlproject_file_path,
                platform=platform,
                access_token=None,
            )

        result = git_repo._zip_file_name_in_url

        assert result == expected

    @pytest.mark.parametrize(
        ("git_url", "branch", "commit", "platform", "expected"),
        [
            (
                "fake_gitlab_repo_url",
                "main",
                None,
                git_repository.Platform.GitLab,
                "{url}/-/archive/main/test-repo-main.zip",
            ),
            (
                "fake_gitlab_repo_url",
                "main",
                "test-commit",
                git_repository.Platform.GitLab,
                "{url}/-/archive/test-commit/test-repo-test-commit.zip",
            ),
            (
                "fake_github_repo_url",
                "main",
                None,
                git_repository.Platform.GitHub,
                "{url}/archive/main.zip",
            ),
            (
                "fake_github_repo_url",
                "main",
                "test-commit",
                git_repository.Platform.GitHub,
                "{url}/archive/test-commit.zip",
            ),
            (
                "fake_bitbucket_org_repo_url",
                "main",
                "test-commit",
                git_repository.Platform.Bitbucket,
                "{url}/get/test-commit.zip",
            ),
            (
                "fake_self_hosted_repo_url",
                "main",
                None,
                git_repository.Platform.Bitbucket,
                "{url}/rest/api/latest/test-owner/test-repo/archive?at=main&format=zip",
            ),
        ],
    )
    def test_zip_archive_url(
        self,
        request,
        mock_mlflow_mlproject_file_path_validation,
        mlflow_mlproject_file_path,
        git_url,
        branch,
        commit,
        platform,
        expected,
        expect_raise_if_exception,
    ):
        git_url = request.getfixturevalue(git_url)
        expected = expected.format(url=git_url)

        with mock_mlflow_mlproject_file_path_validation(
            git_url=git_url, branch=branch, commit=commit, platform=platform
        ):
            git_repo = _git_repo.GitRepo(
                url=git_url,
                branch=branch,
                commit=commit,
                mlflow_mlproject_file_path=mlflow_mlproject_file_path,
                platform=platform,
                access_token=None,
            )

        with expect_raise_if_exception(expected):
            result = git_repo.zip_archive_url
            assert result == expected

    @pytest.mark.parametrize(
        ("git_url", "branch", "commit", "expected"),
        (
            (
                "fake_gitlab_repo_url",
                "test-branch",
                None,
                "test-repo-test-branch",
            ),
            (
                "fake_gitlab_repo_url",
                "other-branch",
                "test-commit",
                "test-repo-test-commit",
            ),
            (
                "fake_gitlab_repo_url",
                "test-branch",
                None,
                "test-repo-test-branch",
            ),
            (
                "fake_gitlab_repo_url",
                "main",
                "test-commit",
                "test-repo-test-commit",
            ),
        ),
    )
    def test_zip_directory_name(
        self,
        request,
        mock_mlflow_mlproject_file_path_validation,
        mlflow_mlproject_file_path,
        git_url,
        branch,
        commit,
        expected,
    ):
        git_url = request.getfixturevalue(git_url)

        with mock_mlflow_mlproject_file_path_validation(
            git_url=git_url,
            branch=branch,
            commit=commit,
            platform=git_repository.Platform.GitLab,
        ):
            git_repo = _git_repo.GitRepo(
                url=git_url,
                branch=branch,
                commit=commit,
                mlflow_mlproject_file_path=mlflow_mlproject_file_path,
                platform=git_repository.Platform.GitLab,
                access_token=None,
            )

        result = git_repo.zip_directory_name

        assert result.match(expected)

    @pytest.mark.parametrize(
        ("git_url", "platform", "commit", "expected"),
        (
            (
                "fake_gitlab_repo_url",
                git_repository.Platform.GitLab,
                None,
                "test-repo-test-branch/test/path",
            ),
            (
                "fake_github_repo_url",
                git_repository.Platform.GitHub,
                None,
                "test-repo-test-branch/test/path",
            ),
            (
                "fake_gitlab_repo_url",
                git_repository.Platform.GitLab,
                "test-commit",
                "test-repo-test-commit/test/path",
            ),
            (
                "fake_github_repo_url",
                git_repository.Platform.GitHub,
                "test-commit",
                "test-repo-test-commit/test/path",
            ),
            (
                "fake_bitbucket_org_repo_url",
                git_repository.Platform.Bitbucket,
                "test-commit",
                "test-repo-test-commit/test/path",
            ),
            (
                "fake_self_hosted_repo_url",
                git_repository.Platform.Bitbucket,
                "test-commit",
                "test-repo-test-branch@test-commit/test/path",
            ),
        ),
    )
    def test_mlflow_project_path_in_zip(
        self,
        request,
        mock_mlflow_mlproject_file_path_validation,
        fake_branch,
        commit,
        git_url,
        platform,
        expected,
    ):
        git_url = request.getfixturevalue(git_url)
        mlflow_mlproject_file_path = pathlib.Path("test/path/MLproject")

        with mock_mlflow_mlproject_file_path_validation(
            git_url=git_url,
            branch=fake_branch,
            commit=commit,
            path=mlflow_mlproject_file_path,
            platform=platform,
        ):
            git_repo = _git_repo.GitRepo(
                url=git_url,
                branch=fake_branch,
                commit=commit,
                mlflow_mlproject_file_path=mlflow_mlproject_file_path,
                platform=platform,
                access_token=None,
            )

        result = git_repo.mlflow_project_path_in_zip

        assert result.match(expected)
