import datetime
import uuid

import freezegun
import jwcrypto.jwk
import jwcrypto.jwt
import pytest
import starlette.requests

import mantik_api
import mantik_api.routes.rbac.bearer as bearer


@pytest.fixture()
def generated_token(mocker):
    def generate_token(date: int):
        kid = "test-12345"
        key = jwcrypto.jwk.JWK.generate(kty="RSA", size=2048, alg="RS256", kid=kid)
        token = jwcrypto.jwt.JWT(
            header={"alg": "RS256", "kid": kid},
            claims={
                "sub": str(uuid.uuid4()),
                "iss": "Test-fixture",
                "client_id": "test-client-id",
                "token_use": "access",
                "exp": date,
                "username": "test-username",
            },
        )

        token.make_signed_token(key)

        public_key = key.export_public()

        class PublicKeyResponse:
            @staticmethod
            def json():
                import json

                return {"keys": [json.loads(public_key)]}

        mock_public_key_fetch = mocker.patch("requests.get")
        mock_public_key_fetch.return_value = PublicKeyResponse
        mocker.patch("mantik_api.aws.cognito.client.Properties.from_env")
        mocker.patch("mantik_api.tokens.verifier._verify_client", return_value=True)
        mocker.patch("mantik_api.tokens.verifier._verify_issuer", return_value=True)

        return token.serialize()

    return generate_token


@freezegun.freeze_time("2012-01-14")
@pytest.mark.parametrize(
    ("date", "valid"),
    [
        (datetime.datetime(2012, 1, 14), True),
        (
            datetime.datetime(
                2012, 1, 14 - bearer.MLFLOW_EXTENDED_VALIDITY_IN_DAYS + 1
            ),
            True,
        ),
        (
            datetime.datetime(
                2012, 1, 14 - bearer.MLFLOW_EXTENDED_VALIDITY_IN_DAYS - 1
            ),
            False,
        ),
    ],
)
@pytest.mark.asyncio
async def test_mlflow_barer_extended_validity(generated_token, date, valid):
    mlflow_bearer = bearer.MlflowJWTBearer()
    token = generated_token(int(date.timestamp()))
    if valid:
        assert (
            await mlflow_bearer(
                request=starlette.requests.Request(
                    scope={
                        "type": "http",
                        "headers": [(b"authorization", f"Bearer {token}".encode())],
                    }
                )
            )
        ).to_string() == token
    else:
        with pytest.raises(
            mantik_api.aws.cognito.exceptions.AuthenticationFailedException
        ):
            await mlflow_bearer(
                request=starlette.requests.Request(
                    scope={
                        "type": "http",
                        "headers": [(b"authorization", f"Bearer {token}".encode())],
                    }
                )
            )
