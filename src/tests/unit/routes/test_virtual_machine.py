import pytest

import mantik_api.service.virtual_machine as service


@pytest.mark.parametrize(
    "url,expected",
    [
        ("s3://some-bucket/123", True),
        ("http://www.example.com/123", False),
        ("http://www.example.com/s3/123", False),
    ],
)
def test_is_s3_url(url: str, expected: bool) -> None:
    assert service.is_s3_url(url) == expected


@pytest.mark.parametrize(
    "s3_base_url,expected_bucket_name",
    [
        ("s3://some-bucket", "some-bucket"),
        ("s3://other-bucket", "other-bucket"),
    ],
)
def test_extract_bucket_name_from_s3_url(
    s3_base_url: str, expected_bucket_name: str
) -> None:
    assert (
        service.extract_bucket_name_from_s3_url(s3_url=s3_base_url)
        == expected_bucket_name
    )


def test_extract_user_id_from_s3_url() -> None:
    assert (
        service.extract_user_id_from_s3_url("s3://some-bucket/123/testfile.txt")
        == "123"
    )


def test_extract_file_key_from_s3_url() -> None:
    assert (
        service.extract_file_key_from_s3_url("s3://some-bucket/123/testfile.txt")
        == "123/testfile.txt"
    )
