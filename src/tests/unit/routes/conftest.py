import pytest


@pytest.fixture
def mock_get_user_id_from_token(mocker):
    mocker.patch(
        "mantik_api.tokens.jwt.JWT.user_id",
        return_value="user_id",
        new_callable=mocker.PropertyMock,
    )


@pytest.fixture
def mock_get_project(mocker, database_project_private):
    mocker.patch(
        "mantik_api.routes.authorization._get_project_from_db",
        return_value=database_project_private,
    )
