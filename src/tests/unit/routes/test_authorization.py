import dataclasses
import enum

import fastapi.exceptions
import pytest

import mantik_api.database as database
import mantik_api.database.project as project
import mantik_api.routes.rbac.authorization as authorization
import mantik_api.testing as testing
import mantik_api.tokens as tokens


@pytest.fixture
def fake_roles():
    class FakeRoles(enum.Enum):
        MyRole = enum.auto()
        NotMyRole = enum.auto()

    return FakeRoles


@pytest.fixture
def mock_my_role(mocker, fake_roles):
    mocker.patch(
        "mantik_api.routes.rbac._project.get_user_role_in_project",
        return_value=database.role_details.RoleDetails(role=fake_roles.MyRole),
    )


@pytest.fixture
def mock_not_my_role(mocker, fake_roles):
    mocker.patch(
        "mantik_api.routes.rbac._project.get_user_role_in_project",
        return_value=database.role_details.RoleDetails(role=fake_roles.NotMyRole),
    )


@pytest.fixture
def mock_guest(mocker):
    mocker.patch(
        "mantik_api.routes.rbac._project.get_user_role_in_project",
        return_value=database.role_details.RoleDetails(role=project.ProjectRole.GUEST),
    )


@dataclasses.dataclass
class FakeRequest:
    token: tokens.jwt.JWT | None = dataclasses.field(
        default_factory=lambda: testing.token_verifier.create_token("test-token")
    )


@pytest.fixture()
def fake_db_with_public_project(database_project):
    return database.client.main.Client(
        engine=testing.database.FakeEngine(),
        session_handler=testing.database.FakeSessionHandler(
            existing_models={"project": [database_project]}
        ),
    )


@pytest.fixture()
def fake_db_with_private_project(database_project_private):
    return database.client.main.Client(
        engine=testing.database.FakeEngine(),
        session_handler=testing.database.FakeSessionHandler(
            existing_models={"project": [database_project_private]}
        ),
    )


EXPECTED_EXCEPTION = fastapi.HTTPException(401)


@pytest.mark.parametrize(
    ("allowed_roles", "request_with_token", "mock_role", "fake_db", "expected"),
    [
        # Test case: authorized project role in public project
        (
            ["MyRole"],
            FakeRequest(),
            "mock_my_role",
            "fake_db_with_public_project",
            None,
        ),
        # Test case: unauthorized project role in public project
        (
            ["MyRole"],
            FakeRequest(),
            "mock_not_my_role",
            "fake_db_with_public_project",
            None,
        ),
        # Test case: unauthorized guest in public project
        (
            ["MyRole"],
            FakeRequest(token=None),
            "mock_guest",
            "fake_db_with_public_project",
            None,
        ),
        # Test case: authorized project role in private project
        (
            ["MyRole"],
            FakeRequest(),
            "mock_my_role",
            "fake_db_with_private_project",
            None,
        ),
        # Test case: unauthorized project role in private project
        (
            ["MyRole"],
            FakeRequest(),
            "mock_not_my_role",
            "fake_db_with_private_project",
            EXPECTED_EXCEPTION,
        ),
        # Test case: unauthorized guest in private project
        (
            ["MyRole"],
            FakeRequest(token=None),
            "mock_guest",
            "fake_db_with_private_project",
            EXPECTED_EXCEPTION,
        ),
    ],
)
@pytest.mark.asyncio
async def test_authorized_project_get_roles(
    request,
    expect_raise_if_exception,
    mock_get_user_id_from_token,
    allowed_roles,
    request_with_token,
    mock_role,
    fake_db,
    expected,
):
    request.getfixturevalue(mock_role)
    db = request.getfixturevalue(fake_db)

    @authorization.authorized_project_get_roles(allowed_roles)
    async def func(request, projectId, db):
        return "authorised"

    with expect_raise_if_exception(expected):
        result = await func(request=request_with_token, projectId="project_id", db=db)

        assert result == "authorised"


@pytest.mark.parametrize(
    ("allowed_roles", "request_with_token", "mock_role", "fake_db", "expected"),
    [
        # Test case: authorized project role in public project
        (
            ["MyRole"],
            FakeRequest(),
            "mock_my_role",
            "fake_db_with_public_project",
            None,
        ),
        # Test case: unauthorized project role in public project
        (
            ["MyRole"],
            FakeRequest(),
            "mock_not_my_role",
            "fake_db_with_public_project",
            EXPECTED_EXCEPTION,
        ),
        # Test case: unauthorized guest in public project
        (
            ["MyRole"],
            FakeRequest(token=None),
            "mock_guest",
            "fake_db_with_public_project",
            EXPECTED_EXCEPTION,
        ),
        # Test case: avoid GUEST to be accidentally in allowed roles
        (
            ["GUEST"],
            FakeRequest(token=None),
            "mock_guest",
            "fake_db_with_public_project",
            EXPECTED_EXCEPTION,
        ),
        # Test case: authorized project role in private project
        (
            ["MyRole"],
            FakeRequest(),
            "mock_my_role",
            "fake_db_with_private_project",
            None,
        ),
        # Test case: unauthorized project role in private project
        (
            ["MyRole"],
            FakeRequest(),
            "mock_not_my_role",
            "fake_db_with_private_project",
            EXPECTED_EXCEPTION,
        ),
        # Test case: unauthorized guest in private project
        (
            ["MyRole"],
            FakeRequest(token=None),
            "mock_guest",
            "fake_db_with_private_project",
            EXPECTED_EXCEPTION,
        ),
        # Test case: avoid GUEST to be accidentally in allowed roles
        (
            ["GUEST"],
            FakeRequest(token=None),
            "mock_guest",
            "fake_db_with_private_project",
            EXPECTED_EXCEPTION,
        ),
    ],
)
@pytest.mark.asyncio
async def test_authorized_project_put_post_delete_roles(
    request,
    expect_raise_if_exception,
    mock_get_user_id_from_token,
    allowed_roles,
    request_with_token,
    mock_role,
    fake_db,
    expected,
):
    request.getfixturevalue(mock_role)
    db = request.getfixturevalue(fake_db)

    @authorization.authorized_project_put_post_delete_roles(allowed_roles)
    async def func(request, projectId, db):
        return "authorised"

    with expect_raise_if_exception(expected):
        result = await func(request=request_with_token, projectId="project_id", db=db)

        assert result == "authorised"
