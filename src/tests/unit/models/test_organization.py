import uuid

import mantik_api.models as models
import mantik_api.testing as testing


class TestOrganization:
    def test_from_database(self, database_organization, api_organization):
        expected = api_organization

        result = models.organization.Organization.from_database_model(
            database_organization
        )
        assert result == expected


class TestAddOrganization:
    def test_to_database(
        self, database_organization, api_add_organization, organization_id_str
    ):
        result = api_add_organization.to_database_model(
            organization_id=uuid.UUID(organization_id_str),
            contact=database_organization.contact,
        )

        assert testing.database.all_equal(result, database_organization, ["id", "name"])
