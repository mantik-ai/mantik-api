import mantik_api.models as models


class TestUserGroup:
    def test_from_database(self, database_user_group, api_user_group):
        expected = api_user_group

        result = models.user_group.UserGroup.from_database_model(database_user_group)

        assert result == expected


def test_from_database_models(database_user_group, api_user_group):
    expected = 3 * [api_user_group]

    result = models.base.from_database_models(
        models.user_group.UserGroup, 3 * [database_user_group]
    )
    assert list(result) == expected
