import mantik_api.models.label as _label


class TestLabel:
    def test_from_database_model(self, database_label_1, api_label_1):
        expected = api_label_1

        result = _label.Label.from_database_model(database_label_1)

        assert result == expected
