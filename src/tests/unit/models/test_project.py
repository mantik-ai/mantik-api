import mantik_api.database as database
import mantik_api.models as models


class TestProject:
    def test_from_database(self, database_project, api_project):
        expected = api_project

        result = models.project.Project.from_database_model(
            database_project,
            user_role=database.role_details.ProjectRole.NO_ROLE,
            user_id=None,
        )
        assert result == expected
