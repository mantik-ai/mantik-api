import mantik_api.models as models


class TestConnection:
    def test_from_database(self, database_connection, api_connection, mock_hvac):
        expected = models.connection.Connection(
            connection_id=api_connection.connection_id,
            user=api_connection.user,
            connection_name=api_connection.connection_name,
            connection_provider=api_connection.connection_provider,
            auth_method=api_connection.auth_method,
            created_at=database_connection.created_at,
            updated_at=api_connection.updated_at,
        )

        result = models.connection.Connection.from_database_model(database_connection)

        assert result == expected
