import pathlib
import uuid

import pytest

import mantik_api.database as database
import mantik_api.models as models


@pytest.fixture
def database_run_without_data_repository(database_run):
    database_run.data_repository_id = None
    database_run.data_repository = None
    return database_run


@pytest.fixture
def database_run_without_status(database_run):
    database_run.status = None
    return database_run


@pytest.fixture
def database_run_running(database_run):
    database_run.status = database.run.RunStatus.RUNNING
    return database_run


class TestAddRun:
    def test_validate_mlproject_file_path(
        self,
        database_run,
        api_run,
        compute_budget_account,
        entry_point,
        mlflow_parameters,
        backend_config,
    ):
        model = models.run.AddRun(
            name="test",
            experiment_repository_id=uuid.uuid4(),
            code_repository_id=uuid.uuid4(),
            branch="main",
            commit=None,
            data_repository_id=uuid.uuid4(),
            connection_id=uuid.uuid4(),
            user_id=uuid.uuid4(),
            compute_budget_account=compute_budget_account,
            mlflow_mlproject_file_path="test/path/MLproject",
            entry_point=entry_point,
            mlflow_parameters=mlflow_parameters,
            backend_config=backend_config,
        )
        assert isinstance(model.mlflow_mlproject_file_path, pathlib.Path)


class TestRun:
    @pytest.mark.parametrize(
        "database_model", ["database_run", "database_run_without_data_repository"]
    )
    def test_from_database_model(self, request, database_model, api_run):
        database_model = request.getfixturevalue(database_model)

        expected = api_run

        result = models.run.Run.from_database_model(
            database_model,
            user_role=database.role_details.ProjectRole.NO_ROLE,
            user_id=None,
        )

        assert result.run_id == expected.run_id
        assert result.status == expected.status

    @pytest.mark.parametrize(
        ("database_model", "expected"),
        [
            ("database_run", False),
            ("database_run_without_status", True),
            ("database_run_running", True),
        ],
        ids=[
            "Finished run is not active",
            "Run without status stored is considered active",
            "Running run is active",
        ],
    )
    def test_is_active(self, request, database_model, expected):
        database_model: database.run.Run = request.getfixturevalue(database_model)

        result = database_model.is_active()

        assert result == expected


class TestAddMinimalRun:
    def test_create_with_minimal_required_fields(self):
        minimal_run = models.run.AddRun(
            name="test-minimal-run",
            experiment_repository_id=uuid.uuid4(),
            backend_config={},
        )

        assert minimal_run.name == "test-minimal-run"
        assert isinstance(minimal_run.experiment_repository_id, uuid.UUID)
        assert minimal_run.branch is None
        assert minimal_run.commit is None
        assert minimal_run.data_repository_id is None
        assert minimal_run.connection_id is None
        assert minimal_run.compute_budget_account is None
        assert minimal_run.mlflow_parameters is None
        assert minimal_run.backend_config == {}
