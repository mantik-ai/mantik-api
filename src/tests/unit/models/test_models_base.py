import uuid

import pytest

import mantik_api.models.base as base


class ChildModel(base.BaseModel):
    some_field: str
    uuid_field: uuid.UUID

    def from_database_model(cls):
        raise NotImplementedError

    def to_database_model(self):
        raise NotImplementedError


class NestedModel(base.BaseModel):
    some_field: str
    uuid_field: uuid.UUID
    child: ChildModel

    def from_database_model(cls):
        raise NotImplementedError

    def to_database_model(self):
        raise NotImplementedError


class SimpleChildModelOne(base.BaseModel):
    camel_case: str | None

    def from_database_model(cls):
        raise NotImplementedError

    def to_database_model(self):
        raise NotImplementedError


class SimpleChildModelTwo(base.BaseModel):
    child_models: list[SimpleChildModelOne]

    def from_database_model(cls):
        raise NotImplementedError

    def to_database_model(self):
        raise NotImplementedError


class SimpleChildModelThree(base.BaseModel):
    child_models: list[SimpleChildModelTwo]

    def from_database_model(cls):
        raise NotImplementedError

    def to_database_model(self):
        raise NotImplementedError


class ModelWithEnvVars(base.BaseModel):
    my_variables: dict[str, str]


class TestBaseModel:
    def test_uuid_stringification(self) -> None:
        test_model = NestedModel(
            some_field="some-value-1",
            uuid_field=uuid.UUID("d0da8960-5108-402f-8203-8f3db00910fe"),
            child=ChildModel(
                some_field="some-value-2",
                uuid_field=uuid.UUID("99cd96ec-55ee-4e58-b77c-b57b8a65250b"),
            ),
        )
        expected = {
            "child": {
                "someField": "some-value-2",
                "uuidField": "99cd96ec-55ee-4e58-b77c-b57b8a65250b",
            },
            "someField": "some-value-1",
            "uuidField": "d0da8960-5108-402f-8203-8f3db00910fe",
        }

        result = test_model.dict()

        assert result == expected

    def test_to_camel_case(self) -> None:
        test_model = SimpleChildModelThree(
            child_models=[SimpleChildModelTwo(child_models=[SimpleChildModelOne()])]
        )
        expected = {
            "childModels": [{"childModels": [{"camelCase": None}]}],
        }

        result = test_model.dict()

        assert result == expected

    def test_to_camel_case_leaves_dictionaries_intact(self) -> None:
        test_model = ModelWithEnvVars(
            my_variables={"my_key_1": "my_value_1", "my_key_2": "my_value_2"}
        )
        expected = {
            "myVariables": {"my_key_1": "my_value_1", "my_key_2": "my_value_2"},
        }

        result = test_model.dict()

        assert result == expected

    def test_to_camel_case_nested(self, api_organization, created_at):
        created_at = created_at.isoformat()

        expected = {
            "contact": {
                "name": "test-name",
                "userId": "6641ab6b-750c-4cb6-ac15-4699601300c1",
                "createdAt": created_at,
                "updatedAt": None,
            },
            "groups": [
                {
                    "admin": {
                        "name": "test-name",
                        "userId": "6641ab6b-750c-4cb6-ac15-4699601300c1",
                        "createdAt": created_at,
                        "updatedAt": None,
                    },
                    "members": [
                        {
                            "name": "test-name",
                            "userId": "6641ab6b-750c-4cb6-ac15-4699601300c1",
                            "createdAt": created_at,
                            "updatedAt": None,
                        },
                        {
                            "name": "test-name-2",
                            "userId": "6641ab6b-750c-4cb6-ac15-4699601300c2",
                            "createdAt": created_at,
                            "updatedAt": None,
                        },
                    ],
                    "name": "test-name",
                    "userGroupId": "7641ab6b-750c-4cb6-ac15-4699601300c1",
                    "createdAt": created_at,
                    "updatedAt": None,
                }
            ],
            "members": [
                {
                    "name": "test-name-3",
                    "userId": "6641ab6b-750c-4cb6-ac15-4699601300c3",
                    "createdAt": created_at,
                    "updatedAt": None,
                }
            ],
            "name": "foo",
            "organizationId": "1989f1db-7489-4afc-93c5-5f369c204224",
            "createdAt": created_at,
            "updatedAt": None,
        }

        result = api_organization.dict()
        assert result == expected


class TestBaseModelWithBranchOrCommit:
    @pytest.mark.parametrize(
        ("code_repository_id", "branch", "commit", "expected"),
        [
            (
                "DEADBEEF-DEAD-BEEF-DEAD-BEEFDEADBEEF",
                None,
                None,
                "No branch name or commit hash given",
            ),
            (
                "DEADBEEF-DEAD-BEEF-DEAD-BEEFDEADBEEF",
                "",
                "",
                "No branch name or commit hash given",
            ),
            (
                "DEADBEEF-DEAD-BEEF-DEAD-BEEFDEADBEEF",
                "",
                None,
                "No branch name or commit hash given",
            ),
            (
                "DEADBEEF-DEAD-BEEF-DEAD-BEEFDEADBEEF",
                None,
                "",
                "No branch name or commit hash given",
            ),
        ],
    )
    def test_validate_branch_and_commit(
        self, expect_raise_if_exception, branch, commit, expected, code_repository_id
    ):
        with expect_raise_if_exception(ValueError()) as e:
            base.BaseModelWithBranchOrCommit(
                branch=branch,
                commit=commit,
                code_repository_id=code_repository_id,
            )

            assert expected in str(e), "Unexpected error message"

    def test_commit_can_be_empty_if_branch_given(self):
        result = base.BaseModelWithBranchOrCommit(
            branch="given",
            commit="",
            code_repository_id="DEADBEEF-DEAD-BEEF-DEAD-BEEFDEADBEEF",
        )

        assert result.branch == "given"
        assert result.commit is None

    def test_commit_preferred_over_branch(self):
        result = base.BaseModelWithBranchOrCommit(
            branch="given",
            commit="preferred",
            code_repository_id="DEADBEEF-DEAD-BEEF-DEAD-BEEFDEADBEEF",
        )

        assert result.branch is None
        assert result.commit == "preferred"

    def test_should_pass_with_empty_repository(self):
        result = base.BaseModelWithBranchOrCommit(
            branch=None,
            commit=None,
            code_repository_id=None,
        )

        assert result.branch is None
        assert result.commit is None
