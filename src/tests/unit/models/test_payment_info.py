import mantik_api.models as models


class TestPaymentInfo:
    def test_from_database(self, database_user, api_payment_info):
        expected = api_payment_info

        result = models.payment_info.PaymentInfo.from_database_model(database_user)

        assert expected == result
