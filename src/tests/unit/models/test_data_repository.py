import mantik_api.database as database
import mantik_api.models as models


class TestDataRepository:
    def test_from_database_model(self, database_data_repository, api_data_repository):
        expected = api_data_repository

        result = models.data_repository.DataRepository.from_database_model(
            database_data_repository,
            user_role=database.role_details.ProjectRole.NO_ROLE,
            user_id=None,
        )

        assert result == expected
