import mantik_api.database as database
import mantik_api.models as models


class TestRun:
    def test_from_database_model(
        self, database_saved_model_repository, api_saved_model_repository
    ):
        expected = api_saved_model_repository

        result = models.trained_model.TrainedModel.from_database_model(
            database_saved_model_repository,
            user_role=database.role_details.ProjectRole.NO_ROLE,
        )

        assert result.model_id == expected.model_id
        assert result.uri == expected.uri
