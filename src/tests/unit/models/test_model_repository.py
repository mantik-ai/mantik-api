import mantik_api.database as database
import mantik_api.models as models


class TestModelRepository:
    def test_from_database_model(self, database_model_repository, api_model_repository):
        expected = api_model_repository

        result = models.model_repository.ModelRepository.from_database_model(
            database_model_repository,
            user_role=database.role_details.ProjectRole.NO_ROLE,
        )

        assert result == expected
