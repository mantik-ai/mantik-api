import mantik_api.models as models


class TestUserSettings:
    def test_from_database(self, database_user, api_user_settings, mock_hvac):
        assert (
            api_user_settings
            == models.user_settings.UserSettings.from_database_model(database_user)
        )
