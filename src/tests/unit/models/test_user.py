import mantik_api.models as models


class TestUser:
    def test_from_database(self, database_user, api_user):
        expected = api_user

        result = models.user.User.from_database_model(database_user)
        assert result == expected
