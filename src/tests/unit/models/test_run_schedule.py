import uuid

import fastapi

import mantik_api.database as database
import mantik_api.models as models

_VALID_CRON_EXPRESSION = "0 12 15 * ?"


class TestAddRunSchedule:
    def test_invalid_cron_expression(self, expect_raise_if_exception):
        with expect_raise_if_exception(fastapi.HTTPException(422)):
            models.run_schedule.AddRunSchedule(
                name="test",
                owner_id=uuid.uuid4(),
                run_id=uuid.uuid4(),
                connection_id=uuid.uuid4(),
                compute_budget_account="test-compute-budget-account",
                cron_expression="Invalid",
                time_zone="Europe/Amsterdam",
                end_date=1680013251,
            )

    def test_invalid_time_zone(self, expect_raise_if_exception):
        with expect_raise_if_exception(fastapi.HTTPException(422)):
            models.run_schedule.AddRunSchedule(
                name="test",
                owner_id=uuid.uuid4(),
                run_id=uuid.uuid4(),
                connection_id=uuid.uuid4(),
                compute_budget_account="test-compute-budget-account",
                cron_expression=_VALID_CRON_EXPRESSION,
                time_zone="Invalid",
                end_date=1680013251,
            )


class TestRunSchedule:
    def test_invalid_cron_expression(
        self,
        expect_raise_if_exception,
        run_schedule_id_str,
        run_schedule_name_str,
        run_schedule_aws_arn,
        api_user,
        api_run,
        api_connection,
    ):
        with expect_raise_if_exception(fastapi.HTTPException(422)):
            models.run_schedule.RunSchedule(
                run_schedule_id=run_schedule_id_str,
                name=run_schedule_name_str,
                owner=api_user,
                run=api_run,
                connection=api_connection,
                compute_budget_account="test-compute-budget-account",
                cron_expression="Invalid",
                time_zone="Europe/Amsterdam",
                end_date=1680013251,
            )

    def test_invalid_time_zone(
        self,
        expect_raise_if_exception,
        run_schedule_id_str,
        run_schedule_name_str,
        run_schedule_aws_arn,
        api_user,
        api_run,
        api_connection,
    ):
        with expect_raise_if_exception(fastapi.HTTPException(422)):
            models.run_schedule.RunSchedule(
                run_schedule_id=run_schedule_id_str,
                name=run_schedule_name_str,
                owner=api_user,
                run=api_run,
                connection=api_connection,
                compute_budget_account="test-compute-budget-account",
                cron_expression=_VALID_CRON_EXPRESSION,
                time_zone="Invalid",
                end_date=1680013251,
            )

    def test_from_database_model(
        self, database_run_schedule, api_run_schedule, mock_hvac
    ):
        expected = api_run_schedule

        result = models.run_schedule.RunSchedule.from_database_model(
            database_run_schedule,
            user_role=database.role_details.ProjectRole.NO_ROLE,
            user_id=None,
        )

        assert result.run_schedule_id == expected.run_schedule_id
