import mantik_api.database as database
import mantik_api.models as models


class TestExperimentRepository:
    def test_from_database_model(
        self, database_experiment_repository, api_experiment_repository
    ):
        expected = api_experiment_repository

        result = models.experiment_repository.ExperimentRepository.from_database_model(
            database_experiment_repository,
            user_role=database.role_details.ProjectRole.NO_ROLE,
        )

        assert result.experiment_repository_id == expected.experiment_repository_id
        assert result.labels == expected.labels
