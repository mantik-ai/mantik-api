import pytest


class TestRunInfo:
    @pytest.mark.parametrize(
        ("api_run_info", "api_run_info_as_json"),
        [
            ("api_run_info_unicore", "api_run_info_as_json_unicore"),
            ("api_run_info_firecrest", "api_run_info_as_json_firecrest"),
        ],
    )
    def test_to_json(self, api_run_info, api_run_info_as_json, request):
        api_run_info = request.getfixturevalue(api_run_info)
        api_run_info_as_json = request.getfixturevalue(api_run_info_as_json)
        assert api_run_info.to_json() == api_run_info_as_json
