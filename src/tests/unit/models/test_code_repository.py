import uuid

import mantik_api.database as database
import mantik_api.models as models


class TestCodeRepository:
    def test_from_database_model(self, database_code_repository, api_code_repository):
        result = models.code_repository.CodeRepository.from_database_model(
            database_code_repository,
            user_role=database.role_details.ProjectRole.NO_ROLE,
            user_id=uuid.uuid4(),
        )
        assert api_code_repository == result
