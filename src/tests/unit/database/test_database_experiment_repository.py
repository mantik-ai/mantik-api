import pytest

import mantik_api.database.experiment_repository as experiment_repository
import mantik_api.testing as testing


class TestOrderBy:
    @pytest.mark.parametrize(
        ("order_by", "expected"),
        [
            (
                experiment_repository.OrderBy.name,
                experiment_repository.ExperimentRepository.name,
            ),
            (
                experiment_repository.OrderBy.mlflow_experiment_id,
                experiment_repository.ExperimentRepository.mlflow_experiment_id,
            ),
            (
                experiment_repository.OrderBy.created,
                experiment_repository.ExperimentRepository.created_at,
            ),
            (
                experiment_repository.OrderBy.updated,
                experiment_repository.ExperimentRepository.updated_at,
            ),
        ],
    )
    def test_column(self, order_by, expected):
        result = order_by.column

        assert result == expected


class TestExperimentRepository:
    def test_repr(self):
        model = experiment_repository.ExperimentRepository(
            id=testing.database.MODEL_UUID,
        )

        assert model.id == testing.database.MODEL_UUID
