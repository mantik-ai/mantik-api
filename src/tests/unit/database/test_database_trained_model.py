import uuid

import pytest

import mantik_api.database.trained_model as trained_model


class TestOrderBy:
    @pytest.mark.parametrize(
        ("order_by", "expected"),
        [
            (trained_model.OrderBy.uri, trained_model.TrainedModel.uri),
            (trained_model.OrderBy.location, trained_model.TrainedModel.location),
            (trained_model.OrderBy.created, trained_model.TrainedModel.created_at),
            (trained_model.OrderBy.updated, trained_model.TrainedModel.updated_at),
        ],
    )
    def test_column(self, order_by, expected):
        result = order_by.column

        assert result == expected


class TestLabel:
    def test_init(self, database_label_1, label_id_1_str):
        assert database_label_1.id == uuid.UUID(label_id_1_str)
