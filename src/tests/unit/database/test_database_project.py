import pytest

import mantik_api.database.project as project


class TestOrderBy:
    @pytest.mark.parametrize(
        ("order_by", "expected"),
        [
            (project.OrderBy.name, project.Project.name),
            (project.OrderBy.created, project.Project.created_at),
            (project.OrderBy.updated, project.Project.updated_at),
        ],
    )
    def test_column(self, order_by, expected):
        result = order_by.column

        assert result == expected


class TestOrderMembersBy:
    @pytest.mark.parametrize(
        ("order_by", "expected"),
        [
            (project.OrderMembersBy.role, project.UserProjectAssociationTable.role),
            (
                project.OrderMembersBy.added,
                project.UserProjectAssociationTable.created_at,
            ),
            (
                project.OrderMembersBy.updated,
                project.UserProjectAssociationTable.updated_at,
            ),
        ],
    )
    def test_column(self, order_by, expected):
        result = order_by.column

        assert result == expected


class TestOrderUserGroupsBy:
    @pytest.mark.parametrize(
        ("order_by", "expected"),
        [
            (
                project.OrderUserGroupsBy.role,
                project.UserGroupProjectAssociationTable.role,
            ),
            (
                project.OrderUserGroupsBy.added,
                project.UserGroupProjectAssociationTable.created_at,
            ),
            (
                project.OrderUserGroupsBy.updated,
                project.UserGroupProjectAssociationTable.updated_at,
            ),
        ],
    )
    def test_column(self, order_by, expected):
        result = order_by.column

        assert result == expected


class TestOrderOrganizationsBy:
    @pytest.mark.parametrize(
        ("order_by", "expected"),
        [
            (
                project.OrderOrganizationsBy.role,
                project.OrganizationProjectAssociationTable.role,
            ),
            (
                project.OrderOrganizationsBy.added,
                project.OrganizationProjectAssociationTable.created_at,
            ),
            (
                project.OrderOrganizationsBy.updated,
                project.OrganizationProjectAssociationTable.updated_at,
            ),
        ],
    )
    def test_column(self, order_by, expected):
        result = order_by.column

        assert result == expected


class TestUserSettings:
    def test_repr(self, database_project, project_id_str):
        expected = f"Project(id=UUID({project_id_str!r}))"
        result = str(database_project)
        assert result == expected
