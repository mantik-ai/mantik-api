import uuid

import pytest

import mantik_api.database.invitation as invitation


class TestOrderBy:
    @pytest.mark.parametrize(
        ("order_by", "expected"),
        [
            (invitation.OrderBy.type, invitation.Invitation.invited_type),
            (invitation.OrderBy.invited_to, invitation.Invitation.invited_to_type),
            (invitation.OrderBy.role, invitation.Invitation.role),
            (invitation.OrderBy.sent, invitation.Invitation.created_at),
            (invitation.OrderBy.updated, invitation.Invitation.updated_at),
        ],
    )
    def test_column(self, order_by, expected):
        result = order_by.column

        assert result == expected


class TestLabel:
    def test_init(self, database_label_1, label_id_1_str):
        assert database_label_1.id == uuid.UUID(label_id_1_str)
