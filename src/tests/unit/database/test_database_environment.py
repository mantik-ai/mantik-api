import uuid

import pytest

import mantik_api.database.environment as environment


class TestOrderBy:
    @pytest.mark.parametrize(
        ("order_by", "expected"),
        [
            (environment.OrderBy.name, environment.Environment.name),
            (environment.OrderBy.type, environment.Environment.type),
            (environment.OrderBy.created, environment.Environment.created_at),
            (environment.OrderBy.updated, environment.Environment.updated_at),
        ],
    )
    def test_column(self, order_by, expected):
        result = order_by.column

        assert result == expected


class TestLabel:
    def test_init(self, database_label_1, label_id_1_str):
        assert database_label_1.id == uuid.UUID(label_id_1_str)
