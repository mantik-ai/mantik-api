import pytest

import mantik_api.database.user as user


class TestOrderBy:
    @pytest.mark.parametrize(
        ("order_by", "expected"),
        [
            (user.OrderBy.preferred_name, user.User.preferred_name),
            (user.OrderBy.created, user.User.created_at),
            (user.OrderBy.updated, user.User.updated_at),
        ],
    )
    def test_column(self, order_by, expected):
        result = order_by.column

        assert result == expected


class TestUser:
    def test_repr(self, database_user, user_id_str):
        expected = (
            f"User(id=UUID('{user_id_str}'), "
            "preferred_name='test-name', "
            "cognito_name='test-cognito-name', "
            "email='test-email')"
        )
        result = str(database_user)
        assert result == expected

    def test_overwrite_cognito_name_fails(
        self, database_user, expect_raise_if_exception
    ):
        with expect_raise_if_exception(ValueError()):
            database_user.cognito_name = "another-cognito-name"
