import uuid

import pytest

import mantik_api.database.label as label


class TestOrderBy:
    @pytest.mark.parametrize(
        ("order_by", "expected"),
        [
            (label.OrderBy.name, label.Label.name),
            (label.OrderBy.category, label.Label.category),
            (label.OrderBy.sub_category, label.Label.sub_category),
            (label.OrderBy.created, label.Label.created_at),
            (label.OrderBy.updated, label.Label.updated_at),
        ],
    )
    def test_column(self, order_by, expected):
        result = order_by.column

        assert result == expected


class TestLabel:
    def test_init(self, database_label_1, label_id_1_str):
        assert database_label_1.id == uuid.UUID(label_id_1_str)
