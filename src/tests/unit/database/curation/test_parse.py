import pytest

import mantik_api.database as database
import mantik_api.database.curation._model as _model
import mantik_api.database.curation._parse as _parse
import mantik_api.testing as testing


def test_parse_yaml_to_labels(labels_yaml_test_path, labels):
    expected = labels

    result = _parse.parse_yaml_to_labels(labels_yaml_test_path)

    testing.labels.assert_labels_equal(result, expected)


def test_parse_yaml_to_database_models_unique_labels():
    labels = {
        "Tasks": {
            "scopes": ["project", "code", "experiment"],
            "subCategories": {
                "Tabular": {
                    "labels": [
                        {"identifier": "0001", "name": "PCA"},
                    ],
                }
            },
        },
        "Domain": {
            "scopes": ["project", "data", "code"],
            "labels": [{"identifier": "0001", "name": "Meteorology"}],
        },
    }
    with pytest.raises(ValueError):
        _parse._parse_labels(labels)


@pytest.mark.parametrize(
    "labels",
    [
        # Test case: invalid category
        {
            "Invalid": {
                "scopes": ["project"],
                "labels": [{"identifier": "0001", "name": "Meteorology"}],
            },
        },
        # Test case: invalid sub-category
        {
            "Tasks": {
                "scopes": ["project"],
                "subCategories": {
                    "Invalid": {
                        "labels": [{"identifier": "0001", "name": "Meteorology"}],
                    }
                },
            },
        },
        # Test case: invalid scope
        {
            "Domain": {
                "scopes": ["invalid"],
                "labels": [{"identifier": "0001", "name": "Meteorology"}],
            },
        },
    ],
)
def test_parse_yaml_to_database_models_invalid(labels):
    with pytest.raises(ValueError):
        _parse._parse_labels(labels)


@pytest.mark.parametrize(
    ("label", "expected"),
    [
        (
            {
                "identifier": 1,
                "name": "test-label",
            },
            ValueError(),
        ),
        (
            {
                "identifier": "000",
                "name": "test-label",
            },
            ValueError(),
        ),
        (
            {
                "identifier": "0000",
                "name": "test-label",
            },
            _model.Label(
                identifier="0000",
                scopes=[database.label.Scope.Code],
                category=database.label.Category.Tasks,
                name="test-label",
            ),
        ),
    ],
)
def test_parse_label(expect_raise_if_exception, label, expected):
    with expect_raise_if_exception(expected):
        result = _parse._parse_label(
            label,
            scopes=[database.label.Scope.Code],
            category=database.label.Category.Tasks,
        )

        assert result == expected
