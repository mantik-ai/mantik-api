import mantik_api.database.curation._comparator as _comparator


def test_get_label_identifiers_for_yaml(labels):
    expected = {"0000", "0100", "1000"}

    result = _comparator.get_identifiers(labels)

    assert result == expected


def test_get_label_identifiers_for_database(database_labels):
    expected = {"0000", "1000"}

    result = _comparator.get_identifiers(database_labels)

    assert result == expected


def test_get_labels_to_delete(labels):
    yaml_identifiers = {"0000", "0001", "0003"}
    database_identifiers = {"0000", "0001", "0002"}
    expected = {"0002"}

    result = _comparator.get_labels_to_delete(
        yaml_identifiers=yaml_identifiers, database_identifiers=database_identifiers
    )

    assert result == expected
