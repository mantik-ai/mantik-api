import uuid

import pytest

import mantik_api.database as database
import mantik_api.database.curation._model as _model


@pytest.fixture()
def labels() -> list[_model.Label]:
    return [
        _model.Label(
            identifier="0000",
            scopes=[
                database.label.Scope.Project,
                database.label.Scope.Code,
                database.label.Scope.Experiment,
            ],
            category=database.label.Category.Tasks,
            sub_category=database.label.SubCategory.Tabular,
            name="PCA",
        ),
        _model.Label(
            identifier="0100",
            scopes=[
                database.label.Scope.Experiment,
            ],
            category=database.label.Category.Tasks,
            sub_category=database.label.SubCategory.Multimodal,
            name="K-Means",
        ),
        _model.Label(
            identifier="1000",
            scopes=[
                database.label.Scope.Project,
                database.label.Scope.Data,
                database.label.Scope.Code,
            ],
            category=database.label.Category.Domain,
            name="Meteorology",
        ),
    ]


@pytest.fixture()
def database_labels() -> list[database.label.Label]:
    label_1 = database.label.Label(
        id=uuid.uuid4(),
        identifier="0000",
        scopes=[database.label.Scope.Code, database.label.Scope.Experiment],
        category=database.label.Category.Domain,
        name="test-label-1",
    )
    label_2 = database.label.Label(
        id=uuid.uuid4(),
        identifier="1000",
        scopes=[database.label.Scope.Project],
        category=database.label.Category.Datasets,
        sub_category=database.label.SubCategory.Size,
        name="test-label-2",
    )
    return [label_1, label_2]
