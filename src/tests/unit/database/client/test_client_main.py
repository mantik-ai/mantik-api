import os

import pytest

import mantik_api.database.client.main as _client
import mantik_api.testing as testing


class TestClient:
    def test_add(self, model):
        with testing.database.FakeSessionHandler() as fake_session_handler:
            client = _client.Client(
                engine=testing.database.FakeEngine(),
                session_handler=fake_session_handler,
            )
            client.add(
                model=model,
            )
            assert fake_session_handler.added_models[model.table.key] == [model]

    def test_get_one(self, model):
        id_ = testing.database.MODEL_UUID_STR
        expected_query = (
            "SELECT test_model_table.test_model_id, "
            "test_model_table.created_at, "
            "test_model_table.updated_at, "
            "test_model_table.test_column "
            "FROM test_model_table "
            "WHERE test_model_table.test_model_id = "
            f"'{id_}'"
        )

        with testing.database.FakeSessionHandler(
            existing_models=testing.database.create_empty_tables_dict(models=[model])
        ) as fake_session_handler:
            client = _client.Client(
                engine=testing.database.FakeEngine(),
                session_handler=fake_session_handler,
            )
            result = client.get_one(
                orm_model_type=testing.database.Model,
                constraints={testing.database.Model.id: id_},
            )

            [result_query] = fake_session_handler.executed_queries

            assert result_query == expected_query
            assert result == model

    def test_get_many(self, model):
        id_ = testing.database.MODEL_UUID_STR
        expected_query = (
            "SELECT test_model_table.test_model_id, "
            "test_model_table.created_at, "
            "test_model_table.updated_at, "
            "test_model_table.test_column "
            "FROM test_model_table "
            "WHERE test_model_table.test_model_id = "
            f"'{id_}' "
            "ORDER BY test_model_table.created_at DESC "
            # For some reason there's an additional space before LIMIT
            " LIMIT 30 OFFSET 0"
        )

        with testing.database.FakeSessionHandler(
            existing_models=testing.database.create_empty_tables_dict(
                models=[model, model]
            )
        ) as fake_session_handler:
            client = _client.Client(
                engine=testing.database.FakeEngine(),
                session_handler=fake_session_handler,
            )
            result = client.get_many(
                orm_model_type=testing.database.Model,
                constraints={testing.database.Model.id: id_},
                select_in_loading={testing.database.Model.test_relationship: {}},
                limit=30,
                offset=0,
                order_by=testing.database.OrderBy.created,
                ascending=False,
            )

            [result_query] = fake_session_handler.executed_queries

            expected = _client.GetAllResponse(
                total_count=2, page_count=2, entities=[model, model]
            )

            assert result_query == expected_query
            assert result == expected

    def test_update_model(self, model_updated):
        expected = (
            "UPDATE test_model_table "
            "SET "
            "updated_at=now(), "
            "test_column='test_value_updated' "
            "WHERE test_model_table.test_model_id = "
            f"'{testing.database.MODEL_UUID}'"
        )
        with testing.database.FakeSessionHandler() as fake_session_handler:
            client = _client.Client(
                engine=testing.database.FakeEngine(),
                session_handler=fake_session_handler,
            )
            client.update(
                model=model_updated,
                constraints={testing.database.Model.id: model_updated.id},
            )
            [result] = fake_session_handler.executed_queries

            assert result == expected

    @pytest.mark.parametrize(
        ("to_delete", "use_constraints"),
        [
            ("model", True),
            ("model", False),
            (testing.database.Model, True),
            (testing.database.Model, False),
        ],
    )
    def test_delete(self, request, to_delete, use_constraints):
        if isinstance(to_delete, str):
            model = request.getfixturevalue(to_delete)
            constraints = (
                None
                if not use_constraints
                else {testing.database.Model.id: testing.database.MODEL_UUID}
            )
        else:
            model = to_delete
            constraints = {testing.database.Model.id: testing.database.MODEL_UUID}

        expected = (
            "DELETE FROM test_model_table "
            "WHERE test_model_table.test_model_id = "
            f"'{testing.database.MODEL_UUID}'"
        )

        with testing.database.FakeSessionHandler() as fake_session_handler:
            client = _client.Client(
                engine=testing.database.FakeEngine(),
                session_handler=fake_session_handler,
            )
            client.delete(
                model=model,
                constraints=constraints,
            )
            [result] = fake_session_handler.executed_queries

            assert result == expected

    @pytest.mark.parametrize("constraints", [None, {}])
    def test_delete_all_rows_prevented(self, constraints):
        with pytest.raises(
            ValueError
        ), testing.database.FakeSessionHandler() as fake_session_handler:
            client = _client.Client(
                engine=testing.database.FakeEngine(),
                session_handler=fake_session_handler,
            )
            client.delete(
                model=testing.database.Model,
                constraints=constraints,
            )


def test_get_uri_from_env() -> None:
    username = "test-username"
    password = "test-password"
    host = "test-host0.0.0.0"
    port = "8000"
    database = "test-database"
    os.environ["USERNAME"] = username
    os.environ["PASSWORD"] = password
    os.environ["HOST"] = host
    os.environ["PORT"] = port
    os.environ["DATABASE"] = database
    assert (
        _client._get_uri_from_env()
        == f"postgresql+psycopg2://{username}:{password}@{host}:{port}/{database}"
    )
    os.environ.pop("USERNAME", None)
    os.environ.pop("PASSWORD", None)
    os.environ.pop("HOST", None)
    os.environ.pop("PORT", None)
    os.environ.pop("DATABASE", None)
