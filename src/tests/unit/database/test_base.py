import uuid

import pytest

import mantik_api.testing as testing


@pytest.fixture(params=[testing.database.MODEL_UUID, testing.database.MODEL_UUID_STR])
def id_(request) -> str | uuid.UUID:
    return request.param


class TestBase:
    def test_id(self, model):
        result = model.id

        assert result == testing.database.MODEL_UUID
