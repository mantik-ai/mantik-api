import uuid

import pytest

import mantik_api.database.run as run


class TestOrderRunBy:
    @pytest.mark.parametrize(
        ("order_by", "expected"),
        [
            (run.OrderRunBy.name, run.Run.name),
            (run.OrderRunBy.status, run.Run.status),
            (run.OrderRunBy.compute_budget_account, run.Run.compute_budget_account),
            (run.OrderRunBy.created, run.Run.created_at),
            (run.OrderRunBy.updated, run.Run.updated_at),
        ],
    )
    def test_column(self, order_by, expected):
        result = order_by.column

        assert result == expected


class TestOrderRunConfigurationBy:
    @pytest.mark.parametrize(
        ("order_by", "expected"),
        [
            (run.OrderRunConfigurationBy.name, run.RunConfiguration.name),
            (run.OrderRunConfigurationBy.branch, run.RunConfiguration.branch),
            (run.OrderRunConfigurationBy.commit, run.RunConfiguration.commit),
            (
                run.OrderRunConfigurationBy.compute_budget_account,
                run.RunConfiguration.compute_budget_account,
            ),
            (run.OrderRunConfigurationBy.created, run.RunConfiguration.created_at),
            (run.OrderRunConfigurationBy.updated, run.RunConfiguration.updated_at),
        ],
    )
    def test_column(self, order_by, expected):
        result = order_by.column

        assert result == expected


class TestLabel:
    def test_init(self, database_label_1, label_id_1_str):
        assert database_label_1.id == uuid.UUID(label_id_1_str)
