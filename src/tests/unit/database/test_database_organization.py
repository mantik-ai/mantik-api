import pytest

import mantik_api.database.organization as organization


class TestOrderBy:
    @pytest.mark.parametrize(
        ("order_by", "expected"),
        [
            (organization.OrderBy.name, organization.Organization.name),
            (organization.OrderBy.created, organization.Organization.created_at),
            (organization.OrderBy.updated, organization.Organization.updated_at),
        ],
    )
    def test_column(self, order_by, expected):
        result = order_by.column

        assert result == expected


class TestOrganization:
    def test_repr(self, database_organization, organization_id_str):
        assert (
            str(database_organization)
            == f"Organization(id=UUID('{organization_id_str}'), name='foo')"
        )
