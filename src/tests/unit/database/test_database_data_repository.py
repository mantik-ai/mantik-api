import pytest

import mantik_api.database.data_repository as data_repository
import mantik_api.testing as testing


class TestOrderBy:
    @pytest.mark.parametrize(
        ("order_by", "expected"),
        [
            (
                data_repository.OrderBy.name,
                data_repository.DataRepository.data_repository_name,
            ),
            (data_repository.OrderBy.uri, data_repository.DataRepository.uri),
            (
                data_repository.OrderBy.created,
                data_repository.DataRepository.created_at,
            ),
            (
                data_repository.OrderBy.updated,
                data_repository.DataRepository.updated_at,
            ),
        ],
    )
    def test_column(self, order_by, expected):
        result = order_by.column

        assert result == expected


class TestDataRepository:
    def test_init(self):
        model = data_repository.DataRepository(
            id=testing.database.MODEL_UUID, uri="test_uri"
        )

        assert model.id == testing.database.MODEL_UUID
