from __future__ import annotations

import pytest

import mantik_api.database.label as _label
import mantik_api.testing as testing


@pytest.fixture()
def model() -> testing.database.Model:
    return testing.database.Model(
        id=testing.database.MODEL_UUID, test_column="test_value"
    )


@pytest.fixture()
def labels() -> list[_label.Label]:
    return [
        _label.Label(id="test_label_id_1", scope="test_scope_1", name="test_label_1"),
        _label.Label(id="test_label_id_2", scope="test_scope_2", name="test_label_2"),
    ]


@pytest.fixture()
def model_updated() -> testing.database:
    return testing.database.Model(
        id=testing.database.MODEL_UUID, test_column="test_value_updated"
    )


@pytest.fixture()
def model_with_labels(labels) -> testing.database.ModelWithLabels:
    return testing.database.ModelWithLabels(
        id=testing.database.MODEL_UUID,
        test_column="test_value",
        labels=labels,
    )


@pytest.fixture()
def labels_updated(labels) -> list[_label.Label]:
    return [
        labels[0],
        _label.Label(id="test_label_id_3", scope="test_scope_3", name="test_label_3"),
    ]


@pytest.fixture()
def model_with_labels_updated(labels_updated) -> testing.database:
    return testing.database.ModelWithLabels(
        id=testing.database.MODEL_UUID,
        test_column="test_value_updated",
        labels=labels_updated,
    )
