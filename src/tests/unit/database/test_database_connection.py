import pytest

import mantik_api.database.connection as connection


class TestOrderBy:
    @pytest.mark.parametrize(
        ("order_by", "expected"),
        [
            (connection.OrderBy.name, connection.Connection.connection_name),
            (connection.OrderBy.provider, connection.Connection.connection_provider),
            (connection.OrderBy.created, connection.Connection.created_at),
            (connection.OrderBy.updated, connection.Connection.updated_at),
        ],
    )
    def test_column(self, order_by, expected):
        result = order_by.column

        assert result == expected


class TestConnections:
    def test_repr(self, database_connection, connection_id_str):
        expected = (
            f"Connection(id=UUID('{connection_id_str}'),"
            f" connection_name='test-connection-name',"
            f" connection_provider='{connection.ConnectionProvider.HPC_JSC.value}')"
        )
        result = str(database_connection)
        assert result == expected
