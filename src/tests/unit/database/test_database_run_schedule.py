import uuid

import pytest

import mantik_api.database.run_schedule as run_schedule


class TestOrderBy:
    @pytest.mark.parametrize(
        ("order_by", "expected"),
        [
            (run_schedule.OrderBy.name, run_schedule.RunSchedule.name),
            (run_schedule.OrderBy.created, run_schedule.RunSchedule.created_at),
            (run_schedule.OrderBy.updated, run_schedule.RunSchedule.updated_at),
        ],
    )
    def test_column(self, order_by, expected):
        result = order_by.column

        assert result == expected


class TestLabel:
    def test_init(self, database_label_1, label_id_1_str):
        assert database_label_1.id == uuid.UUID(label_id_1_str)
