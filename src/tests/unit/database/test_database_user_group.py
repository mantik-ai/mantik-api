import pytest

import mantik_api.database.user_group as user_group


class TestOrderBy:
    @pytest.mark.parametrize(
        ("order_by", "expected"),
        [
            (user_group.OrderBy.name, user_group.UserGroup.name),
            (user_group.OrderBy.created, user_group.UserGroup.created_at),
            (user_group.OrderBy.updated, user_group.UserGroup.updated_at),
        ],
    )
    def test_column(self, order_by, expected):
        result = order_by.column

        assert result == expected


class TestUserGroup:
    def test_repr(self, database_user_group, user_group_id_str):
        assert (
            str(database_user_group)
            == f"UserGroup(id=UUID('{user_group_id_str}'), name='test-name')"
        )
