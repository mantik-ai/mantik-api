import pytest

import mantik_api.database.model_repository as model_repository


class TestOrderBy:
    @pytest.mark.parametrize(
        ("order_by", "expected"),
        [
            (model_repository.OrderBy.uri, model_repository.ModelRepository.uri),
            (model_repository.OrderBy.branch, model_repository.ModelRepository.branch),
            (model_repository.OrderBy.commit, model_repository.ModelRepository.commit),
            (
                model_repository.OrderBy.created,
                model_repository.ModelRepository.created_at,
            ),
            (
                model_repository.OrderBy.updated,
                model_repository.ModelRepository.updated_at,
            ),
        ],
    )
    def test_column(self, order_by, expected):
        result = order_by.column

        assert result == expected


class TestModelRepository:
    def test_repr(self, database_model_repository, model_repository_id_str):
        expected = f"ModelRepository(id=UUID('{model_repository_id_str}'))"

        result = str(database_model_repository)

        assert result == expected
