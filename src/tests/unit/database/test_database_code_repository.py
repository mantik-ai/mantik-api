import pytest

import mantik_api.database.code_repository as code_repository
import mantik_api.testing as testing


class TestOrderBy:
    @pytest.mark.parametrize(
        ("order_by", "expected"),
        [
            (
                code_repository.OrderBy.name,
                code_repository.CodeRepository.code_repository_name,
            ),
            (code_repository.OrderBy.uri, code_repository.CodeRepository.uri),
            (
                code_repository.OrderBy.created,
                code_repository.CodeRepository.created_at,
            ),
            (
                code_repository.OrderBy.updated,
                code_repository.CodeRepository.updated_at,
            ),
        ],
    )
    def test_column(self, order_by, expected):
        result = order_by.column

        assert result == expected


class TestCodeRepository:
    def test_init(self):
        model = code_repository.CodeRepository(
            id=testing.database.MODEL_UUID, uri="test_uri"
        )

        assert model.id == testing.database.MODEL_UUID
