import fastapi.testclient as testclient
import pytest
import starlette.status as status

import mantik_api.aws.cognito as cognito


@pytest.mark.parametrize(("option"), [("name"), ("email")])
def test_forgot_password_initiate_post(
    client: testclient.TestClient,
    add_example_user,
    database_user,
    option,
    moto_user_pool,
    create_and_confirm_fake_cognito_user,
    boto3_cognito_idp_client,
):
    if option == "name":
        data = {"userName": database_user.preferred_name}
    elif option == "email":
        data = {"email": database_user.email}

    response = client.post("/forgot-password/initiate", json=data)

    assert response.status_code == status.HTTP_200_OK, response.text
    result = response.json()
    expected = {"userName": database_user.preferred_name}
    assert result == expected


@pytest.mark.parametrize(
    ("data", "expected_status_code", "expected_message"),
    [
        (
            {
                "userName": "doesnt-exist",
            },
            status.HTTP_404_NOT_FOUND,
            """User with name doesnt-exist not found""",
        ),
        (
            {
                "email": "doesnt-exist",
            },
            status.HTTP_404_NOT_FOUND,
            """User with email doesnt-exist not found""",
        ),
        (
            {
                "userName": "cannot-provide-both",
                "email": "cannot-provide-both",
            },
            status.HTTP_422_UNPROCESSABLE_ENTITY,
            "Either user name or email must be provided, not both",
        ),
        (
            {},
            status.HTTP_422_UNPROCESSABLE_ENTITY,
            "Either user name or email must be provided",
        ),
    ],
)
def test_forgot_password_initiate_post_fails(
    client: testclient.TestClient,
    add_example_user,
    database_user,
    data,
    expected_status_code,
    expected_message,
    moto_user_pool,
    create_and_confirm_fake_cognito_user,
    boto3_cognito_idp_client,
):
    response = client.post("/forgot-password/initiate", json=data)

    assert response.status_code == expected_status_code, response.text

    if expected_message is not None:
        assert expected_message in response.text, "Incorrect error message in response"


def test_forgot_password_confirm_put(
    client: testclient.TestClient,
    boto3_cognito_idp_client,
    create_and_confirm_fake_cognito_user,
    database_user,
    add_example_user,
):
    username = create_and_confirm_fake_cognito_user["username"]
    new_password = "New-Test-Password-123"

    initiate_recovery_response = cognito.users.initiate_forgot_password(
        username=username
    )
    confirmation_code = initiate_recovery_response["ResponseMetadata"]["HTTPHeaders"][
        "x-moto-forgot-password-confirmation-code"
    ]

    data = {
        "userName": database_user.preferred_name,
        "confirmationCode": confirmation_code,
        "newPassword": new_password,
    }
    response = client.put("/forgot-password/confirm", json=data)

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response_create_token = client.post(
        "/mantik/tokens/create",
        json={"username": database_user.preferred_name, "password": new_password},
    )
    assert (
        response_create_token.status_code == status.HTTP_201_CREATED
    ), response_create_token.text


def test_forgot_password_confirm_put_fails(
    client: testclient.TestClient,
    boto3_cognito_idp_client,
    create_and_confirm_fake_cognito_user,
    moto_user_pool,
    database_user,
    add_example_user,
):
    # moto cannot mimic our expected behavior: moto fails to validate new passwords,
    # accepting passwords that are non-compliant with the defined password policy
    confirmation_code = "moto-confirmation-code:123invalid"
    new_password = "New-Test-Password-123"

    data = {
        "userName": database_user.preferred_name,
        "confirmationCode": confirmation_code,
        "newPassword": new_password,
    }

    response = client.put("/forgot-password/confirm", json=data)

    assert response.status_code == status.HTTP_424_FAILED_DEPENDENCY, response.text
    assert (
        "Unable to reset password" in response.text
    ), "Incorrect error message in response"
