import datetime

import fastapi.testclient as testclient
import pytest
import starlette.status as status

import mantik_api.testing as testing


@pytest.fixture
def sample_group_response(
    user_group_id_str: str,
    user_id_str: str,
    second_user_id_str: str,
    created_at,
) -> dict:
    return {
        "userGroupId": user_group_id_str,
        "name": "test-name",
        "admin": {
            "userId": user_id_str,
            "name": "test-name",
            "createdAt": created_at.isoformat(),
            "updatedAt": None,
        },
        "members": [
            {
                "userId": user_id_str,
                "name": "test-name",
                "createdAt": created_at.isoformat(),
                "updatedAt": None,
            },
            {
                "userId": second_user_id_str,
                "name": "test-name-2",
                "createdAt": created_at.isoformat(),
                "updatedAt": None,
            },
        ],
        "createdAt": created_at.isoformat(),
        "updatedAt": None,
    }


@pytest.mark.usefixtures("add_database_user_group")
def test_groups_get(client: testclient.TestClient, sample_group_response: dict):
    """Test case for groups_get

    Get all User group
    """
    params = [("startindex", 0), ("pagelength", 50)]
    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER
    response = client.get(
        "/groups",
        headers=headers,
        params=params,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    expected = {
        "totalRecords": 1,
        "pageRecords": 1,
        "userGroups": [sample_group_response],
    }

    assert result == expected


@pytest.mark.usefixtures("add_database_user_group")
def test_groups_group_id_get(
    client: testclient.TestClient, sample_group_response: dict
):
    """Test case for groups_group_id_get

    Get Group information
    """
    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER
    response = client.request(  # noqa
        "GET",
        "/groups/{groupId}".format(groupId=sample_group_response["userGroupId"]),
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json() == sample_group_response


@pytest.mark.usefixtures("add_database_user_group")
def test_groups_group_id_put(
    client: testclient.TestClient,
    user_group_id_str: str,
    second_user_id_str: str,
    second_api_user,
    mock_get_user_info_and_be_project_owner,
    created_at,
):
    # update name, change admin, and remove member)
    updated_json = {
        "name": "updated-name",
        "adminId": second_user_id_str,
        "memberIds": [second_user_id_str],
    }
    expected = {
        "admin": second_api_user.dict(),
        "members": [second_api_user.dict()],
        "name": "updated-name",
        "userGroupId": user_group_id_str,
        "createdAt": created_at.isoformat(),
    }

    response = client.put(
        f"/groups/{user_group_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=updated_json,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # then get the group to assert it was correctly updated
    response = client.get(
        f"/groups/{user_group_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()

    # assert user group has updatedAt date
    # Note: Cannot freeze time of updatedAt since this value
    # is set in the DB when updating the row
    assert result.pop("updatedAt") is not None
    assert result == expected


@pytest.mark.usefixtures("add_database_user_group")
def test_groups_group_id_put_when_not_admin(
    client: testclient.TestClient,
    user_group_id_str: str,
    second_user_id_str: str,
    second_api_user,
    mock_get_user_info_and_be_project_maintainer,
):
    # update name, change admin, and remove member)
    updated_json = {
        "name": "updated-name",
        "admin_id": second_user_id_str,
        "member_ids": [second_user_id_str],
    }

    response = client.put(
        f"/groups/{user_group_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=updated_json,
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    assert (
        response.json()["detail"] == "Only current Group's Admin "
        "can modify the Group information."
    )


@pytest.mark.usefixtures("add_database_users")
def test_groups_post(
    client: testclient.TestClient,
    sample_group_response: dict,
    api_user,
    mock_get_user_info_and_be_project_owner,
    created_at,
) -> None:
    response = client.post(
        "/groups",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json={"name": "new-group"},
    )
    assert response.status_code == status.HTTP_201_CREATED, response.text
    new_group_id = response.json()["groupId"]

    expected = {
        "userGroupId": new_group_id,
        "name": "new-group",
        "admin": api_user.dict(),
        "members": [api_user.dict()],
        "updatedAt": None,
    }

    # then get the group to assert it was correctly created
    response = client.get(
        f"/groups/{new_group_id}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()

    # assert user group has createdAt date
    # Note: Cannot freeze time of createdAt since this value
    # is set in the DB when updating the row
    assert result.pop("createdAt") is not None
    assert result == expected


@pytest.mark.usefixtures("clear_and_init_database")
@pytest.mark.usefixtures("add_database_user_group")
def test_groups_wrong_id_type_fails_with_422(client: testclient.TestClient) -> None:
    wrong_id_type = "1234"

    response = client.get(
        f"/groups/{wrong_id_type}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, response.text
    assert response.json() == {
        "detail": [
            {
                "loc": ["path", "groupId"],
                "msg": "value is not a valid uuid",
                "type": "type_error.uuid",
            }
        ]
    }

    response = client.put(
        f"/groups/{wrong_id_type}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json={},
    )
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, response.text
    assert {
        "loc": ["path", "groupId"],
        "msg": "value is not a valid uuid",
        "type": "type_error.uuid",
    } in response.json()["detail"]


@pytest.mark.usefixtures("add_all_example_invitations")
def test_groups_group_id_delete_and_related_invitations(
    client: testclient.TestClient,
    mock_token_user_id,
    user_group_id_str: str,
) -> None:
    invitations_regarding_this_group = [
        {
            "invitationId": "1989f1db-7489-4afc-93b6-5f369c204225",
            "invitedToType": "PROJECT",
            "invitedType": "GROUP",
            "invitedId": "7641ab6b-750c-4cb6-ac15-4699601300c1",
            "invitedToId": "e931c97e-917b-499c-b7b1-fcb52ae96f91",
            "role": "RESEARCHER",
            "createdAt": (
                testing.time.DEFAULT_CREATED_AT + datetime.timedelta(hours=1)
            ).isoformat(),
            "updatedAt": None,
            "invitedName": "test-name",
            "invitedToName": "test_project_name",
            "inviterName": "test-name-2",
        },
        {
            "invitationId": "1989f1db-7489-4afc-93b6-5f369c204229",
            "invitedToType": "GROUP",
            "invitedType": "USER",
            "invitedId": "6641ab6b-750c-4cb6-ac15-4699601300c1",
            "invitedToId": "7641ab6b-750c-4cb6-ac15-4699601300c1",
            "role": None,
            "createdAt": (
                testing.time.DEFAULT_CREATED_AT + datetime.timedelta(hours=5)
            ).isoformat(),
            "updatedAt": None,
            "invitedName": "test-name",
            "invitedToName": "test-name",
            "inviterName": "test-name-2",
        },
        {
            "invitationId": "1989f1db-7489-4afc-93b6-5f369c204228",
            "invitedToType": "ORGANIZATION",
            "invitedType": "GROUP",
            "invitedId": "7641ab6b-750c-4cb6-ac15-4699601300c1",
            "invitedToId": "1989f1db-7489-4afc-93c5-5f369c204224",
            "role": None,
            "createdAt": (
                testing.time.DEFAULT_CREATED_AT + datetime.timedelta(hours=4)
            ).isoformat(),
            "updatedAt": None,
            "invitedName": "test-name",
            "invitedToName": "foo",
            "inviterName": "test-name-2",
        },
    ]
    invitations = client.get(
        "/invitations",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params=[("startindex", 0), ("pagelength", 50)],
    ).json()["invitations"]
    assert all(invite in invitations for invite in invitations_regarding_this_group)

    # Delete the user group
    response = client.delete(
        f"/groups/{user_group_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Check that deletion succeeded
    response = client.get(
        f"/groups/{user_group_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND, response.text

    invitations = client.get(
        "/invitations",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params=[("startindex", 0), ("pagelength", 50)],
    ).json()["invitations"]
    assert all(invite not in invitations for invite in invitations_regarding_this_group)


@pytest.mark.usefixtures("add_database_user_group")
def test_groups_group_id_delete_when_not_admin(
    client: testclient.TestClient,
    mock_token_second_user_id,
    user_group_id_str: str,
) -> None:
    # Try to delete the user group as non-admin
    response = client.delete(
        f"/groups/{user_group_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED, response.text

    # Check that the user group still exists
    response = client.get(
        f"/groups/{user_group_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text


@pytest.mark.usefixtures("add_database_user_group")
def test_groups_group_member_id_delete_as_admin(
    client: testclient.TestClient,
    mock_token_user_id,
    user_group_id_str: str,
    second_user_id_str: str,
) -> None:
    # Check that there are two members
    _assert_group_has_two_members(client, user_group_id_str)

    # Delete one member from the user group
    response = client.delete(
        f"/groups/{user_group_id_str}/members/{second_user_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Check that deletion succeeded
    response = client.get(
        f"/groups/{user_group_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    result = response.json()["members"]

    assert len(result) == 1
    assert all(second_user_id_str != member["userId"] for member in result)


@pytest.mark.usefixtures("add_database_user_group")
def test_groups_group_member_id_delete_as_member(
    client: testclient.TestClient,
    mock_token_second_user_id,
    user_group_id_str: str,
    second_user_id_str: str,
) -> None:
    # Check that there are two members
    _assert_group_has_two_members(client, user_group_id_str)

    # Delete self from user group
    response = client.delete(
        f"/groups/{user_group_id_str}/members/{second_user_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Check that deletion succeeded
    response = client.get(
        f"/groups/{user_group_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    result = response.json()["members"]

    assert len(result) == 1
    assert all(second_user_id_str != member["userId"] for member in result)


@pytest.mark.usefixtures("add_database_user_group")
def test_groups_group_member_id_delete_when_not_admin(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_maintainer,
    user_group_id_str: str,
    user_id_str: str,
) -> None:
    # Check that there are two members
    _assert_group_has_two_members(client, user_group_id_str)

    # Try to delete a member from the user group as a non-admin
    response = client.delete(
        f"/groups/{user_group_id_str}/members/{user_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED, response.text

    # Check that the member still exists
    _assert_group_has_two_members(client, user_group_id_str)


@pytest.mark.usefixtures("add_database_user_group")
def test_groups_group_member_id_delete_when_not_member(
    client: testclient.TestClient,
    mock_token_third_user_id,
    user_group_id_str: str,
    second_user_id_str: str,
) -> None:
    _assert_group_has_two_members(client, user_group_id_str)

    # Try to delete a member from the user group as other member
    response = client.delete(
        f"/groups/{user_group_id_str}/members/{second_user_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED, response.text

    _assert_group_has_two_members(client, user_group_id_str)


@pytest.mark.usefixtures("add_database_user_group")
def test_groups_group_member_id_delete_self_when_admin(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    user_group_id_str: str,
    user_id_str: str,
) -> None:
    _assert_group_has_two_members(client, user_group_id_str)

    # Try to delete yourself from the user group as an admin
    response = client.delete(
        f"/groups/{user_group_id_str}/members/{user_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, response.text

    _assert_group_has_two_members(client, user_group_id_str)


def _assert_group_has_two_members(
    client: testclient.TestClient, user_group_id: str
) -> None:
    response = client.get(
        f"/groups/{user_group_id}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    result = response.json()["members"]

    assert len(result) == 2
