import fastapi.testclient as testclient
import pytest
import starlette.status as status

import mantik_api.database as database
import mantik_api.testing as testing


@pytest.fixture
def api_organizations(second_api_organization, project_id_str) -> dict:
    return {
        "organizations": [
            {
                "organization": second_api_organization.dict(),
                "projectId": project_id_str,
                "role": database.project.ProjectRole.REPORTER.value,
                "createdAt": testing.time.DEFAULT_CREATED_AT.isoformat(),
                "updatedAt": None,
            }
        ],
        "totalRecords": 1,
        "pageRecords": 1,
        "userRole": -1,
    }


@pytest.mark.parametrize(
    ("startindex", "length"),
    [
        (0, 1),
        (30, 0),
    ],
)
@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_second_organization_project_role_as_reporter")
def test_projects_project_id_organization_get_logged_in(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    startindex,
    length,
    organization_id_str,
    project_id_str,
):
    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER
    params = [("startindex", startindex), ("pagelength", 50)]

    response = client.get(
        f"/projects/{project_id_str}/organizations",
        headers=headers,
        params=params,
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()

    assert result["totalRecords"] == 1
    assert result["pageRecords"] == length
    assert result["userRole"] == database.project.ProjectRole.OWNER.value


@pytest.mark.parametrize(
    ("headers", "expected_status_code"),
    (
        ({}, status.HTTP_200_OK),
        ({"Authorization": ""}, status.HTTP_401_UNAUTHORIZED),
        (
            {"Authorization": "InvalidAuth test-invalid-token"},
            status.HTTP_401_UNAUTHORIZED,
        ),
        (
            testing.token_verifier.INVALID_AUTHORIZATION_HEADER,
            status.HTTP_401_UNAUTHORIZED,
        ),
    ),
)
@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_second_organization_project_role_as_reporter")
def test_projects_project_id_organization_get_logged_out(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    second_organization_id_str,
    project_id_str,
    headers,
    expected_status_code,
    api_organizations,
):
    response = client.get(
        f"/projects/{project_id_str}/organizations",
        headers=headers,
    )

    assert response.status_code == expected_status_code, response.text

    if expected_status_code == status.HTTP_200_OK:
        result = response.json()
        assert result == api_organizations


@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_second_organization_project_role_as_reporter")
def test_projects_project_id_organization_organization_id_get_logged_in(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    project_id_str,
    second_organization_id_str,
    second_api_organization,
):
    role = database.project.ProjectRole.REPORTER.value
    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER
    response = client.get(
        f"/projects/{project_id_str}/organizations/{second_organization_id_str}",
        headers=headers,
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["role"] == role
    assert result["organization"] == second_api_organization.dict()


@pytest.mark.parametrize(
    ("headers", "expected_status_code"),
    (
        ({}, status.HTTP_200_OK),
        ({"Authorization": ""}, status.HTTP_401_UNAUTHORIZED),
        (
            {"Authorization": "InvalidAuth test-invalid-token"},
            status.HTTP_401_UNAUTHORIZED,
        ),
        (
            testing.token_verifier.INVALID_AUTHORIZATION_HEADER,
            status.HTTP_401_UNAUTHORIZED,
        ),
    ),
)
@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_second_organization_project_role_as_reporter")
def test_projects_project_id_organization_organization_id_get_logged_out(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    project_id_str,
    second_organization_id_str,
    headers,
    expected_status_code,
):
    response = client.get(
        f"/projects/{project_id_str}/organizations/{second_organization_id_str}",
        headers=headers,
    )

    assert response.status_code == expected_status_code, response.text

    if expected_status_code == status.HTTP_200_OK:
        result = response.json()
        assert result["role"] == database.project.ProjectRole.REPORTER.value


@pytest.mark.parametrize(
    "new_role",
    [
        database.project.ProjectRole.OWNER,
        database.project.ProjectRole.GUEST,
        database.project.ProjectRole.MAINTAINER,
        database.project.ProjectRole.REPORTER,
    ],
)
@pytest.mark.usefixtures("add_second_organization_project_role_as_reporter")
def test_projects_project_id_organization_organization_id_put(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    second_organization_id_str,
    project_id_str,
    new_role: database.project.ProjectRole,
):
    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER
    organization_role = {
        "role": new_role.name,
        "organizationId": second_organization_id_str,
        "projectId": project_id_str,
    }
    response = client.put(
        f"/projects/{project_id_str}/organizations/{second_organization_id_str}",
        headers=headers,
        json=organization_role,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(
        f"/projects/{project_id_str}/organizations/{second_organization_id_str}",
        headers=headers,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["role"] == new_role.value


@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_second_organization_project_role_as_reporter")
def test_projects_project_id_organization_delete(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    second_organization_id_str,
    project_id_str,
):
    # Delete organization from project
    response = client.delete(
        f"/projects/{project_id_str}/organizations/{second_organization_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Check if deletion succeeded
    response = client.get(
        f"/projects/{project_id_str}/organizations/{second_organization_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND, response.text

    # Check organization still exists
    response = client.get(
        f"/organizations/{second_organization_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
