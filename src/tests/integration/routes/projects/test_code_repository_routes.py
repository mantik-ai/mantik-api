import uuid

import fastapi.testclient as testclient
import pytest
import responses
import starlette.status as status

import mantik_api.database as database
import mantik_api.testing as testing
import mantik_api.utils.git_utils as git_utils


@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_code_code_repository_id_get_logged_in(
    client: testclient.TestClient,
    code_repository_id_str: str,
    project_id_str: str,
    mock_token_user_id,
):
    response = client.get(
        f"/projects/{project_id_str}/code/{code_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["userRole"] == 5


@pytest.mark.parametrize(
    ("headers", "expected_status_code"),
    (
        ({}, status.HTTP_200_OK),
        ({"Authorization": ""}, status.HTTP_401_UNAUTHORIZED),
        (
            {"Authorization": "InvalidAuth test-invalid-token"},
            status.HTTP_401_UNAUTHORIZED,
        ),
        (
            testing.token_verifier.INVALID_AUTHORIZATION_HEADER,
            status.HTTP_401_UNAUTHORIZED,
        ),
    ),
)
@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_code_code_repository_id_get_logged_out(
    client: testclient.TestClient,
    code_repository_id_str: str,
    project_id_str: str,
    mock_token_user_id,
    headers,
    expected_status_code,
):
    response = client.get(
        f"/projects/{project_id_str}/code/{code_repository_id_str}",
        headers=headers,
    )

    assert response.status_code == expected_status_code, response.text

    if expected_status_code == status.HTTP_200_OK:
        result = response.json()
        assert result["codeRepositoryId"] == code_repository_id_str
        assert result["userRole"] == -1


@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_example_labels")
def test_projects_project_id_code_code_repository_id_put(
    client: testclient.TestClient,
    code_repository_id_str: str,
    project_id_str: str,
    mock_token_user_id,
    api_label_3,
):
    """Test case for projects_project_id_code_code_repository_id_put

    Updates code repository
    """

    code_repository_update = {
        "codeRepositoryName": "new_name",
        "uri": "new_uri",
        "description": "new description",
        "labels": [str(api_label_3.label_id)],
        "platform": "GitHub",
    }

    response = client.put(
        f"/projects/{project_id_str}/code/{code_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=code_repository_update,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(
        f"/projects/{project_id_str}/code/{code_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=code_repository_update,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    expected = result | code_repository_update | {"labels": [api_label_3.dict()]}

    assert result == expected


@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_code_code_repository_id_put_invalid_label_scope(
    client: testclient.TestClient,
    code_repository_id_str: str,
    project_id_str: str,
    mock_token_user_id,
    api_label_1,
    api_label_3,
):
    code_repository_update = {
        "codeRepositoryName": "new_name",
        "uri": "new_uri",
        "accessToken": "new_token",
        "description": "new description",
        "labels": [str(api_label_1.label_id)],
        "platform": "GitHub",
    }

    response = client.put(
        f"/projects/{project_id_str}/code/{code_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=code_repository_update,
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST, response.text
    assert "Unable to add label" in response.text, "Incorrect error message in response"
    not_updated_response = client.get(
        f"/projects/{project_id_str}/code/{code_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert (
        not_updated_response.status_code == status.HTTP_200_OK
    ), not_updated_response.text
    result = not_updated_response.json()
    assert result["labels"] == [api_label_3.dict()]


@pytest.mark.parametrize(("startindex", "length"), [(0, 3), (30, 0)])
@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_code_get_logged_in(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    startindex,
    length,
):
    params = [("startindex", startindex), ("pagelength", 50)]
    response = client.get(
        f"/projects/{project_id_str}/code",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params=params,
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["totalRecords"] == 3
    assert result["pageRecords"] == length
    assert result["userRole"] == 5

    for code_repository in result["codeRepositories"]:
        assert code_repository["userRole"] == 5


@pytest.mark.parametrize(
    ("headers", "expected_status_code"),
    (
        ({}, status.HTTP_200_OK),
        ({"Authorization": ""}, status.HTTP_401_UNAUTHORIZED),
        (
            {"Authorization": "InvalidAuth test-invalid-token"},
            status.HTTP_401_UNAUTHORIZED,
        ),
        (
            testing.token_verifier.INVALID_AUTHORIZATION_HEADER,
            status.HTTP_401_UNAUTHORIZED,
        ),
    ),
)
@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_code_get_logged_out(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    headers,
    expected_status_code,
):
    response = client.get(
        f"/projects/{project_id_str}/code",
        headers=headers,
    )
    assert response.status_code == expected_status_code, response.text

    if response.status_code == status.HTTP_200_OK:
        result = response.json()
        for code_repository in result["codeRepositories"]:
            assert code_repository["userRole"] == -1


@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_code_post(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    api_label_3,
):
    """Test case for projects_project_id_code_post

    Add code repository
    """
    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER
    new_code_repository = {
        "codeRepositoryName": "new_name",
        "uri": "new_uri",
        "description": "new description",
        "labels": [str(api_label_3.label_id)],
        "platform": "GitHub",
    }

    response = client.post(
        f"/projects/{project_id_str}/code",
        headers=headers,
        json=new_code_repository,
    )
    assert response.status_code == status.HTTP_201_CREATED, response.text
    code_repository_id = response.json()["codeRepositoryId"]

    response = client.get(
        f"/projects/{project_id_str}/code",
        headers=headers,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    assert len(response.json()["codeRepositories"]) == 4

    response = client.get(
        f"/projects/{project_id_str}/code/{code_repository_id}",
        headers=headers,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    expected = new_code_repository | {
        "codeRepositoryId": code_repository_id,
        "labels": [api_label_3.dict()],
        "userRole": 5,
        "updatedAt": None,
        "connectionId": None,
    }

    assert testing.time.has_created_at(result)
    assert result == expected


@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_code_post_invalid_label_scope(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    api_label_1,
):
    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER
    all_code_repositories_before = client.get(
        f"/projects/{project_id_str}/code",
        headers=headers,
    )
    new_code_repository = {
        "codeRepositoryName": "new_name",
        "uri": "new_uri",
        "accessToken": "new_token",
        "description": "new description",
        "labels": [str(api_label_1.label_id)],
        "platform": "GitHub",
    }

    response = client.post(
        f"/projects/{project_id_str}/code",
        headers=headers,
        json=new_code_repository,
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST, response.text
    assert "Unable to add label" in response.text, "Incorrect error message in response"

    all_code_repositories_after = client.get(
        f"/projects/{project_id_str}/code",
        headers=headers,
    )
    assert (
        all_code_repositories_before.status_code == status.HTTP_200_OK
    ), all_code_repositories_before.text
    assert (
        all_code_repositories_after.status_code == status.HTTP_200_OK
    ), all_code_repositories_after.text
    assert len(all_code_repositories_before.json()["codeRepositories"]) == len(
        all_code_repositories_after.json()["codeRepositories"]
    )


@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_code_code_repository_id_delete(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    code_repository_id_str,
    model_repository_id_str,
):
    # Delete Model Repository that uses the respective Code Repository
    response = client.delete(
        f"/projects/{project_id_str}/models/{model_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Delete the Code Repository
    response = client.delete(
        f"/projects/{project_id_str}/code/{code_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Check deletion succeeded
    response = client.get(
        f"/projects/{project_id_str}/code/{code_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND, response.text


@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_code_code_repository_id_delete_prevented(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    code_repository_id_str,
):
    response = client.delete(
        f"/projects/{project_id_str}/code/{code_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_409_CONFLICT, response.text

    result = response.json()["detail"]
    expected = "Resource still references Model Repository"
    assert result == expected

    # Check deletion not succeeded
    response = client.get(
        f"/projects/{project_id_str}/code/{code_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text


def test_code_repository_add_git_connection(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    project_id_str,
    code_repository_id_str,
    api_gitlab_connection_of_first_user,
    add_database_git_connection_of_first_user,
) -> None:
    """
    Given I have a private repository (code)
    When  I store it in Mantik
    Then  I can store an access token with it
    """
    response = client.get(
        f"/projects/{project_id_str}/code/{code_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    # Connection not assigned yet
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["connectionId"] is None

    # Assign a git connection to the repository
    response = client.post(
        f"/projects/{project_id_str}/code/{code_repository_id_str}/connection",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json={
            "connectionId": str(api_gitlab_connection_of_first_user.connection_id),
        },
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Connection should now be assigned
    response = client.get(
        f"/projects/{project_id_str}/code/{code_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["connectionId"] == str(
        api_gitlab_connection_of_first_user.connection_id
    )


@pytest.fixture
def connect_code_repo_with_git_connection(
    database_client,
    database_git_connection_of_first_user,
    database_code_repository,
    add_database_git_connection_of_first_user,
):
    with database_client.session():
        database_client.add(
            database.code_repository.CodeRepositoryAndConnectionAssociationTable(
                id=uuid.uuid4(),
                connection_id=database_git_connection_of_first_user.id,
                code_repository_id=database_code_repository.id,
            )
        )


def test_code_repository_git_connections_of_other_users_are_hidden(
    client: testclient.TestClient,
    add_example_project,
    mock_token_second_user_id,
    project_id_str,
    code_repository_id_str,
    api_gitlab_connection_of_first_user,
    connect_code_repo_with_git_connection,
) -> None:
    """
    Given I'm invited to someone else's project
    And   they have a private repository
    And   I have my own access token to the repository outside of the Mantik platform
    When  I want to associate my access token
    Then  I can do so using the mantik platform
    And   the other user has no access to my token
    """
    response = client.get(
        f"/projects/{project_id_str}/code/{code_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["connectionId"] is None


def test_code_repository_only_supports_adding_one_git_connection_per_user(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    project_id_str,
    code_repository_id_str,
    api_gitlab_connection_of_first_user,
    connect_code_repo_with_git_connection,
) -> None:
    """
    Given I have access to a code repository
    And   a git connection linked to it
    When  I try adding another git connection
    Then  I get an error
    """
    response = client.post(
        f"/projects/{project_id_str}/code/{code_repository_id_str}/connection",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json={
            "connectionId": str(api_gitlab_connection_of_first_user.connection_id),
        },
    )
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, response.text
    assert response.json() == {
        "detail": "Code repository already has a git connection."
    }


def test_code_repository_remove_git_connection(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    project_id_str,
    code_repository_id_str,
    api_gitlab_connection_of_first_user,
    connect_code_repo_with_git_connection,
) -> None:
    """
    Given I have access to a code repository
    And   I have git credentials linked to that repository
    When  I unlink the credentials
    Then  they are no longer linked to the repository
    """
    # check that git connection is linked
    response = client.get(
        f"/projects/{project_id_str}/code/{code_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["connectionId"] == str(
        api_gitlab_connection_of_first_user.connection_id
    )

    # unlink the connection
    response = client.delete(
        f"/projects/{project_id_str}/code/{code_repository_id_str}/connection"
        f"/{str(api_gitlab_connection_of_first_user.connection_id)}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # check that git connection is no longer linked
    response = client.get(
        f"/projects/{project_id_str}/code/{code_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["connectionId"] is None


def test_put_request_does_not_change_git_connection(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    project_id_str,
    code_repository_id_str,
    api_gitlab_connection_of_first_user,
    connect_code_repo_with_git_connection,
):
    """
    Given I have a code repository with a git connection
    When  I update the code repository
    Then  the git connection should not be changed
    """

    # check that git connection is linked
    response = client.get(
        f"/projects/{project_id_str}/code/{code_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["connectionId"] == str(
        api_gitlab_connection_of_first_user.connection_id
    )

    # update the code repository
    response = client.put(
        f"/projects/{project_id_str}/code/{code_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json={
            "uri": "privateinstance.gitlab.com",
            "platform": "gitlab",
            "connectionId": "12345",
        },
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # check that git connection is still linked
    response = client.get(
        f"/projects/{project_id_str}/code/{code_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["connectionId"] == str(
        api_gitlab_connection_of_first_user.connection_id
    )


def test_create_code_repository_with_pre_selected_git_connection(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    project_id_str,
    api_gitlab_connection_of_first_user,
    add_database_git_connection_of_first_user,
    api_label_3,
):
    """
    Given I have a git connection
    When  I create a code repository
    And   I attach that connection to it immediately
    Then  the repository is created
    And   the connection is linked to the repository

    """
    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER
    new_code_repository = {
        "codeRepositoryName": "new_name",
        "uri": "new_uri",
        "description": "new description",
        "labels": [str(api_label_3.label_id)],
        "platform": "GitLab",
        "connectionId": str(api_gitlab_connection_of_first_user.connection_id),
    }

    response = client.post(
        f"/projects/{project_id_str}/code",
        headers=headers,
        json=new_code_repository,
    )
    assert response.status_code == status.HTTP_201_CREATED, response.text
    code_repository_id = response.json()["codeRepositoryId"]

    response = client.get(
        f"/projects/{project_id_str}/code/{code_repository_id}",
        headers=headers,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["connectionId"] == str(
        api_gitlab_connection_of_first_user.connection_id
    )


@pytest.fixture()
def mock_git_request(api_code_repository, sample_gitlab_token):
    # mock request with correct headers
    responses.add(
        method="GET",
        url=git_utils.raw_gitlab_url(url=api_code_repository.uri),
        status=200,
        match=[
            responses.matchers.header_matcher(
                git_utils.git_authorization_header(
                    access_token=sample_gitlab_token,
                    platform=database.git_repository.Platform.GitLab,
                )
            )
        ],
    )

    # mock request without headers
    responses.add(
        method="GET",
        url=git_utils.raw_gitlab_url(url=api_code_repository.uri),
        status=404,
        match=[responses.matchers.header_matcher({})],
    )
    yield


@responses.activate
def test_code_repo_is_unlocked(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    project_id_str,
    code_repository_id_str,
    mock_vault_credential_sample_access_token,
    mock_git_request,
    connect_code_repo_with_git_connection,
):
    """
    Given I have a code repository
    And   a git connection linked to it
    And   that connection contains a valid access token
    When  I check the repository's unlocked status
    And   querying the git returns status code 200
    Then  I get that it is unlocked
    """

    response = client.get(
        f"/projects/{project_id_str}/code/is-unlocked",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params={"codeRepositoryIds": [code_repository_id_str]},
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json() == {code_repository_id_str: True}


@responses.activate
def test_code_repo_is_locked(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    project_id_str,
    code_repository_id_str,
    mock_git_request,
):
    """
    Given I have a code repository
    And   I do not have a valid access token
    And   querying the git repository returns status code 404
    When  I check the repository's unlocked status
    Then  I get that it is locked
    """

    response = client.get(
        f"/projects/{project_id_str}/code/is-unlocked",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params={"codeRepositoryIds": [code_repository_id_str]},
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json() == {code_repository_id_str: False}


def test_create_code_repository_with_pre_selected_not_git_connection(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    project_id_str,
    api_connection_dvc_of_first_user,
    add_database_connection_dvc_of_first_user,
    api_label_3,
):
    """
    Given I have a connection (not git)
    When  I create a code repository
    And   I attach that connection to it immediately
    Then  the repository is not created
    And   the connection is not linked to the repository
    And   a suitable error is returned

    """
    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER
    new_code_repository = {
        "codeRepositoryName": "new_name",
        "uri": "new_uri",
        "description": "new description",
        "labels": [str(api_label_3.label_id)],
        "platform": "GitHub",
        "connectionId": str(api_connection_dvc_of_first_user.connection_id),
    }

    response = client.post(
        f"/projects/{project_id_str}/code",
        headers=headers,
        json=new_code_repository,
    )
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, response.text
    assert (
        response.json()["detail"]
        == "Connection platform 'GitHub' and repository platform 'S3' do not match."
    )
