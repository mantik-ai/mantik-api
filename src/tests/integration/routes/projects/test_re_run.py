import unittest

import fastapi.testclient as testclient
import pytest
import starlette.status as status

import mantik_api.testing as testing

HEADERS = testing.token_verifier.VALID_AUTHORIZATION_HEADER


@pytest.mark.usefixtures("mock_tracking_server_url")
@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_example_run")
@pytest.mark.usefixtures("add_second_example_connection")
@pytest.mark.usefixtures("mock_compute_backend_api_integration_testing")
def test_projects_project_id_runs_run_id_reruns_post(
    client: testclient.TestClient,
    mock_token_user_id,
    second_connection_id_str,
    project_id_str,
    run_id_str,
    mock_hvac,
) -> None:
    json = {
        "connectionId": second_connection_id_str,
        "computeBudgetAccount": "new_account",
    }
    response = client.post(
        f"/projects/{project_id_str}/runs/{run_id_str}/reruns",
        headers=HEADERS,
        json=json,
    )
    assert response.status_code == status.HTTP_201_CREATED, response.text

    newly_created_run_id = response.json()["runId"]
    response = client.get(
        f"/projects/{project_id_str}/runs/{newly_created_run_id}",
        headers=HEADERS,
    )

    assert run_id_str != newly_created_run_id
    assert response.status_code == status.HTTP_200_OK, response.text
    result = response.json()

    assert result["connection"] is not None
    assert result["connection"]["connectionId"] == second_connection_id_str
    assert result["computeBudgetAccount"] == "new_account"
    assert result["status"] is not None


@pytest.mark.usefixtures("mock_tracking_server_url")
@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_example_run_without_data_repository")
@pytest.mark.usefixtures("add_second_example_connection")
@pytest.mark.usefixtures("mock_compute_backend_api_integration_testing")
def test_projects_project_id_runs_run_id_reruns_post_run_without_data_repository(
    client: testclient.TestClient,
    mock_token_user_id,
    second_connection_id_str,
    project_id_str,
    run_id_str,
    mock_hvac,
) -> None:
    json = {
        "connectionId": second_connection_id_str,
        "computeBudgetAccount": "new_account",
    }
    response = client.post(
        f"/projects/{project_id_str}/runs/{run_id_str}/reruns",
        headers=HEADERS,
        json=json,
    )
    assert response.status_code == status.HTTP_201_CREATED, response.text

    newly_created_run_id = response.json()["runId"]
    response = client.get(
        f"/projects/{project_id_str}/runs/{newly_created_run_id}",
        headers=HEADERS,
    )

    assert run_id_str != newly_created_run_id
    assert response.status_code == status.HTTP_200_OK, response.text
    result = response.json()

    assert result["connection"] is not None
    assert result["connection"]["connectionId"] == second_connection_id_str
    assert result["computeBudgetAccount"] == "new_account"
    assert result["status"] is not None


@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_example_run")
@pytest.mark.usefixtures("add_example_connection_second_user")
def test_projects_project_id_runs_run_id_reruns_post_user_not_connection_owner(
    client: testclient.TestClient,
    mock_token_user_id,
    connection_second_user_id_str,
    project_id_str,
    run_id_str,
    mock_hvac,
) -> None:
    json = {"connectionId": connection_second_user_id_str, "computeBudgetAccount": None}
    response = client.post(
        f"/projects/{project_id_str}/runs/{run_id_str}/reruns",
        headers=HEADERS,
        json=json,
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.usefixtures("mock_tracking_server_url")
@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_example_run")
@pytest.mark.usefixtures("mock_compute_backend_api_integration_testing")
def test_projects_project_id_runs_run_id_reruns_post_json_none(
    client: testclient.TestClient,
    mock_token_user_id,
    connection_id_str,
    compute_budget_account,
    project_id_str,
    run_id_str,
    mock_hvac,
) -> None:
    json = {
        "connectionId": None,
        "computeBudgetAccount": None,
    }
    response = client.post(
        f"/projects/{project_id_str}/runs/{run_id_str}/reruns",
        headers=HEADERS,
        json=json,
    )
    assert response.status_code == status.HTTP_201_CREATED, response.text

    newly_created_run_id = response.json()["runId"]
    response = client.get(
        f"/projects/{project_id_str}/runs/{newly_created_run_id}",
        headers=HEADERS,
    )

    assert run_id_str != newly_created_run_id
    assert response.status_code == status.HTTP_200_OK, response.text
    result = response.json()
    assert result["connection"] is not None
    assert result["connection"]["connectionId"] == connection_id_str
    assert result["computeBudgetAccount"] == compute_budget_account
    assert result["status"] is not None


@pytest.mark.parametrize(
    "run_setup_fixture,error_message",
    [
        (
            "add_database_local_run",
            "Triggering a re-run requires a connection: "
            "no connection given and the run to re-execute "
            "doesn't define a connection",
        ),
        (
            "add_database_run_without_compute_budget_account",
            "Submitting a run requires a connection and a compute budget account",
        ),
    ],
    ids=["No connection id", "No computing budget account"],
)
@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("mock_compute_backend_api_integration_testing")
def test_local_run_cannot_be_re_submitted(
    request,
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    run_id_str,
    run_setup_fixture: str,
    error_message: str,
    mock_tracking_server_url,
    mock_hvac,
) -> None:
    """
    Given a run was a local run
    And does not have a connection id or compute budget account
    When I try to re-trigger it through the api
    And do not specify properties needed to trigger it on a remote machine
    Then I get an error
    """

    with unittest.mock.patch("requests.post"):
        request.getfixturevalue(run_setup_fixture)
        response = client.post(
            f"/projects/{project_id_str}/runs/{run_id_str}/reruns",
            headers=HEADERS,
            json={},
        )

        assert (
            response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
        ), response.text
        result = response.json()["detail"]
        assert result == error_message
