import fastapi.testclient as testclient
import pytest
import starlette.status as status

import mantik_api.models as models
import mantik_api.testing as testing


@pytest.fixture
def expected_environment_response(
    api_environment: models.environment.Environment, api_data_repository
) -> dict:
    return {
        "environmentId": str(api_environment.environment_id),
        "name": api_environment.name,
        "type": api_environment.type,
        "variables": api_environment.variables,
        "dataRepository": api_data_repository.dict() | {"userRole": 5},
        "userRole": 5,
        "updatedAt": None,
    }


@pytest.mark.usefixtures("add_example_environment_to_project")
def test_get_environment(
    client: testclient.TestClient,
    environment_id_str: str,
    project_id_str: str,
    mock_token_user_id,
    api_environment: models.environment.Environment,
    expected_environment_response: dict,
):
    # Get the environment
    response = client.get(
        f"/projects/{project_id_str}/environments/{environment_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert testing.time.has_created_at(result)
    assert result == expected_environment_response


@pytest.mark.usefixtures("add_example_environment_to_project")
def test_get_all_project_environments(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str: str,
    expected_environment_response,
):
    response = client.get(
        f"/projects/{project_id_str}/environments",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["totalRecords"] == 1

    [environment] = result["environments"]
    assert testing.time.has_created_at(environment)
    assert environment == expected_environment_response


@pytest.mark.usefixtures("add_example_environment_to_project")
def test_delete_environment(
    client: testclient.TestClient,
    environment_id_str: str,
    project_id_str: str,
    mock_token_user_id,
):
    # Delete the environment
    response = client.delete(
        f"/projects/{project_id_str}/environments/{environment_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Check that deletion succeeded
    response = client.get(
        f"/projects/{project_id_str}/environments/{environment_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND, response.text

    # check project not accidentally deleted
    assert (
        client.get(
            f"/projects/{project_id_str}",
            headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        ).status_code
        == status.HTTP_200_OK
    )


@pytest.mark.usefixtures("add_example_environment_to_project")
def test_update_environment(
    client: testclient.TestClient,
    environment_id_str: str,
    project_id_str: str,
    second_api_data_repository,
    mock_token_user_id,
):
    updated_environment = models.environment.AddEnvironment(
        name="updated-name",
        type="updated-type",
        data_repository_id=second_api_data_repository.data_repository_id,
        variables={"updated_var": "updated_value"},
    )

    response = client.put(
        f"/projects/{project_id_str}/environments/{environment_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=updated_environment.dict(),
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Check that the environment has been updated
    response = client.get(
        f"/projects/{project_id_str}/environments/{environment_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    expected = {
        "environmentId": environment_id_str,
        "dataRepository": second_api_data_repository.dict() | {"userRole": 5},
        **updated_environment.dict(exclude={"data_repository_id"}),
        "userRole": 5,
    }

    assert testing.time.has_created_at(result)
    assert testing.time.has_updated_at(result)
    assert result == expected


@pytest.mark.usefixtures("add_example_environment_to_project")
def test_try_create_environment_with_data_repo_of_another_project(
    client: testclient.TestClient,
    mock_token_user_id,
    add_empty_project,
    api_add_environment,  # contains a data repo not part of current project
):
    response = client.post(
        f"/projects/{str(add_empty_project.id)}/environments",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=api_add_environment.dict(),
    )
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, response.text
    assert response.json()["detail"] == "IDs are inconsistent"


@pytest.mark.usefixtures("add_example_project")
def test_create_new_environment(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str: str,
    api_add_environment,
    expected_environment_response: dict,
):
    response = client.post(
        f"/projects/{project_id_str}/environments",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=api_add_environment.dict(),
    )
    assert response.status_code == status.HTTP_201_CREATED, response.text
    new_environment_id = response.json()["environmentId"]

    response = client.get(
        f"/projects/{project_id_str}/environments/{new_environment_id}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    expected = expected_environment_response | {"environmentId": new_environment_id}

    assert testing.time.has_created_at(result)
    assert result == expected
