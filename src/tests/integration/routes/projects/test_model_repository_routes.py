import fastapi.testclient as testclient
import pytest
import starlette.status as status

import mantik_api.testing as testing


HEADERS = testing.token_verifier.VALID_AUTHORIZATION_HEADER


@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_models_get_logged_in(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
):
    """Test case for projects_project_id_models_get

    Returns model entries for given project
    """
    response = client.get(
        f"/projects/{project_id_str}/models",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["totalRecords"] == 2
    assert result["pageRecords"] == 2
    assert result["userRole"] == 5

    for model_repository in result["modelRepositories"]:
        assert model_repository["userRole"] == 5


@pytest.mark.parametrize(
    ("headers", "expected_status_code"),
    (
        ({}, status.HTTP_200_OK),
        ({"Authorization": ""}, status.HTTP_401_UNAUTHORIZED),
        (
            {"Authorization": "InvalidAuth test-invalid-token"},
            status.HTTP_401_UNAUTHORIZED,
        ),
        (
            testing.token_verifier.INVALID_AUTHORIZATION_HEADER,
            status.HTTP_401_UNAUTHORIZED,
        ),
    ),
)
@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_models_get_logged_out(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    headers,
    expected_status_code,
):
    """Test case for projects_project_id_models_get

    Returns model entries for given project
    """
    response = client.get(
        f"/projects/{project_id_str}/models",
        headers=headers,
    )

    assert response.status_code == expected_status_code, response.text

    if expected_status_code == status.HTTP_200_OK:
        result = response.json()
        assert result["totalRecords"] == 2
        assert result["pageRecords"] == 2
        assert result["userRole"] == -1

        for model_repository in result["modelRepositories"]:
            assert model_repository["userRole"] == -1


@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_models_model_repository_id_get_logged_in(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    model_repository_id_str,
):
    """Test case for projects_project_id_models_model_repository_id_get

    Returns model entry for given ID
    """
    response = client.get(
        f"/projects/{project_id_str}/models/{model_repository_id_str}",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["userRole"] == 5


@pytest.mark.parametrize(
    ("headers", "expected_status_code"),
    (
        ({}, status.HTTP_200_OK),
        ({"Authorization": ""}, status.HTTP_401_UNAUTHORIZED),
        (
            {"Authorization": "InvalidAuth test-invalid-token"},
            status.HTTP_401_UNAUTHORIZED,
        ),
        (
            testing.token_verifier.INVALID_AUTHORIZATION_HEADER,
            status.HTTP_401_UNAUTHORIZED,
        ),
    ),
)
@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_models_model_repository_id_get_logged_out(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    model_repository_id_str,
    headers,
    expected_status_code,
):
    """Test case for projects_project_id_models_model_repository_id_get

    Returns model entry for given ID
    """

    response = client.get(
        f"/projects/{project_id_str}/models/{model_repository_id_str}",
        headers=headers,
    )

    assert response.status_code == expected_status_code, response.text

    if expected_status_code == status.HTTP_200_OK:
        result = response.json()
        assert result["modelRepositoryId"] == model_repository_id_str
        assert result["userRole"] == -1


@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_models_model_repository_id_put(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    model_repository_id_str,
):
    """Test case for projects_project_id_models_model_repository_id_put

    Update model repository
    """
    new_code_repository = {
        "description": "description",
        "uri": "uri",
        "codeRepositoryName": "new_code_repository",
        "platform": "GitHub",
    }
    response = client.post(
        f"/projects/{project_id_str}/code",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=new_code_repository,
    )

    assert response.status_code == status.HTTP_201_CREATED
    new_code_repository_id = response.json()["codeRepositoryId"]

    updated_model_repository = {
        "codeRepositoryId": new_code_repository_id,
        "branch": "main",
    }

    response = client.put(
        f"/projects/{project_id_str}/models/{model_repository_id_str}",
        headers=HEADERS,
        json=updated_model_repository,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(
        f"/projects/{project_id_str}/models/{model_repository_id_str}",
        headers=HEADERS,
    )
    assert response.json()["uri"] == "test-uri"
    assert (
        response.json()["codeRepository"]["codeRepositoryName"] == "new_code_repository"
    )
    assert response.status_code == status.HTTP_200_OK, response.text


@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_models_post(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    code_repository_id_str,
):
    """Test case for projects_project_id_models_post

    Add model repository
    """
    model_repository = {
        "commit": "test-post-commit",
        "description": "description",
        "codeRepositoryId": code_repository_id_str,
        "uri": "uri",
        "branch": "main",
    }

    response = client.post(
        f"/projects/{project_id_str}/models",
        headers=HEADERS,
        json=model_repository,
    )
    assert response.status_code == status.HTTP_201_CREATED, response.text
    model_repository_id = response.json()["modelRepositoryId"]

    response = client.get(
        f"/projects/{project_id_str}/models/{model_repository_id}",
        headers=HEADERS,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["commit"] == "test-post-commit"
    assert (
        response.json()["codeRepository"]["codeRepositoryId"] == code_repository_id_str
    )


@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_models_delete(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    code_repository_id_str,
):
    # Add new model repository
    model_repository = {
        "commit": "test-post-commit",
        "description": "description",
        "codeRepositoryId": code_repository_id_str,
        "uri": "uri",
        "branch": "main",
    }

    response = client.post(
        f"/projects/{project_id_str}/models",
        headers=HEADERS,
        json=model_repository,
    )
    assert response.status_code == status.HTTP_201_CREATED, response.text
    model_repository_id = response.json()["modelRepositoryId"]

    # Delete model repository
    response = client.delete(
        f"/projects/{project_id_str}/models/{model_repository_id}",
        headers=HEADERS,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Check deletion succeeded
    response = client.get(
        f"/projects/{project_id_str}/models/{model_repository_id}",
        headers=HEADERS,
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND, response.text
