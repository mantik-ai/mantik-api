import fastapi.testclient as testclient
import pytest
import starlette.status as status

import mantik_api.models as models
import mantik_api.testing as testing

HEADERS = testing.token_verifier.VALID_AUTHORIZATION_HEADER


@pytest.fixture(autouse=True)
def prepare_scheduler_tests(
    set_required_scheduler_env_vars, mock_boto3_scheduler_client
):
    pass


@pytest.fixture()
def add_example_run_schedule_on_aws(
    add_example_run_schedule, scheduler_client, run_schedule_properties
):
    scheduler_client.create(run_schedule_properties)


@pytest.mark.usefixtures("add_example_run_schedule")
def test_projects_project_id_runs_schedules_run_schedule_id_get_logged_in(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    project_id_str,
    run_schedule_id_str,
    mock_hvac,
):
    """Test case for projects_project_id_runs_schedules_run_schedule_id_get

    Returns model entry for given ID
    """
    response = client.get(
        f"/projects/{project_id_str}/run-schedules/{run_schedule_id_str}",
        headers=HEADERS,
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["userRole"] == 5


@pytest.mark.usefixtures("add_example_run_schedule")
def test_projects_project_id_runs_schedules_run_schedule_id_get_logged_out(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    project_id_str,
    run_schedule_id_str,
    mock_hvac,
):
    """Test case for projects_project_id_runs_schedules_run_schedule_id_get

    Returns model entry for given ID
    """
    response = client.get(
        f"/projects/{project_id_str}/run-schedules/{run_schedule_id_str}"
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["userRole"] == -1


@pytest.mark.parametrize(("startindex", "length"), [(0, 1), (30, 0)])
@pytest.mark.usefixtures("add_example_run_schedule")
def test_projects_project_id_runs_get_logged_in(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    project_id_str,
    startindex,
    length,
    mock_hvac,
):
    """Test case for projects_project_id_run_schedules_get

    Returns all runs for a given project
    """
    params = [("startindex", startindex), ("pagelength", 50)]
    response = client.get(
        f"/projects/{project_id_str}/run-schedules",
        headers=HEADERS,
        params=params,
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()

    assert result["totalRecords"] == 1
    assert result["pageRecords"] == length
    assert result["userRole"] == 5

    for run_schedule in result["runSchedules"]:
        assert run_schedule["userRole"] == 5


@pytest.mark.parametrize(
    ("headers", "expected_status_code"),
    (
        ({}, status.HTTP_200_OK),
        ({"Authorization": ""}, status.HTTP_401_UNAUTHORIZED),
        (
            {"Authorization": "InvalidAuth test-invalid-token"},
            status.HTTP_401_UNAUTHORIZED,
        ),
        (
            testing.token_verifier.INVALID_AUTHORIZATION_HEADER,
            status.HTTP_401_UNAUTHORIZED,
        ),
    ),
)
@pytest.mark.usefixtures("add_example_run_schedule")
def test_projects_project_id_runs_get_logged_out(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    project_id_str,
    headers,
    expected_status_code,
    mock_hvac,
):
    """Test case for projects_project_id_run_schedules_get

    Returns model entries for given project
    """
    response = client.get(
        f"/projects/{project_id_str}/run-schedules",
        headers=headers,
    )

    assert response.status_code == expected_status_code, response.text

    if expected_status_code == status.HTTP_200_OK:
        result = response.json()
        assert result["totalRecords"] == 1
        assert result["userRole"] == -1

        for run_schedule in result["runSchedules"]:
            assert run_schedule["userRole"] == -1


@pytest.mark.usefixtures("add_example_run")
def test_projects_project_id_run_schedules_post(
    monkeypatch,
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    project_id_str,
    api_add_run_schedule,
    mock_hvac,
) -> None:
    response = client.post(
        f"/projects/{project_id_str}/run-schedules",
        headers=HEADERS,
        json=api_add_run_schedule.dict(),
    )

    # Check for correct response
    assert response.status_code == status.HTTP_201_CREATED, response.text

    run_schedule_id = response.json()["runScheduleId"]

    response = client.get(
        f"/projects/{project_id_str}/run-schedules/{run_schedule_id}",
        headers=HEADERS,
    )

    # Check for correct response
    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()

    assert result["name"] == api_add_run_schedule.name
    assert result["owner"]["userId"] == str(api_add_run_schedule.owner_id)
    assert result["run"]["runId"] == str(api_add_run_schedule.run_id)
    assert result["connection"]["connectionId"] == str(
        api_add_run_schedule.connection_id
    )
    assert result["computeBudgetAccount"] == api_add_run_schedule.compute_budget_account
    assert result["cronExpression"] == api_add_run_schedule.cron_expression
    assert result["timeZone"] == api_add_run_schedule.time_zone
    assert result["endDate"] == api_add_run_schedule.end_date


@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_run_schedules_post_error_forwarded(
    monkeypatch,
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    project_id_str,
    api_add_run_schedule,
) -> None:
    # End date must be before start date, hence 1 as end date must fail
    api_add_run_schedule.end_date = 1

    response = client.post(
        f"/projects/{project_id_str}/run-schedules",
        headers=HEADERS,
        json=api_add_run_schedule.dict(),
    )

    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, response.text


@pytest.mark.usefixtures("add_example_run")
@pytest.mark.usefixtures("add_example_connection_second_user")
def test_projects_project_id_run_schedules_post_unowned_connection(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    project_id_str,
    api_add_run_schedule,
    api_connection_second_user,
) -> None:
    # Set to second API connection, which is owned by other user
    api_add_run_schedule.connection_id = api_connection_second_user.connection_id
    response = client.post(
        f"/projects/{project_id_str}/run-schedules",
        headers=HEADERS,
        json=api_add_run_schedule.dict(),
    )

    # Check for correct response
    assert response.status_code == status.HTTP_403_FORBIDDEN, response.text

    # Check no run schedules added
    response = client.get(
        f"/projects/{project_id_str}/run-schedules",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()["runSchedules"]
    assert len(result) == 0


@pytest.mark.usefixtures("add_example_run_schedule_on_aws")
@pytest.mark.usefixtures("add_second_example_run")
def test_projects_project_id_run_schedules_run_schedule_id_put(
    monkeypatch,
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    project_id_str,
    run_schedule_id_str,
    api_add_run_schedule,
    second_run_id_str,
    mock_hvac,
) -> None:
    new_data = models.run_schedule.AddRunSchedule(
        name="new-name",
        owner_id=api_add_run_schedule.owner_id,
        run_id=second_run_id_str,
        connection_id=api_add_run_schedule.connection_id,
        compute_budget_account="new-compute-budget-account",
        cron_expression="1 12 * * *",
        time_zone="UTC",
        end_date=1680013252,
    )

    response = client.put(
        f"/projects/{project_id_str}/run-schedules/{run_schedule_id_str}",
        headers=HEADERS,
        json=new_data.dict(),
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Check change successful
    response = client.get(
        f"/projects/{project_id_str}/run-schedules/{run_schedule_id_str}",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    result = response.json()

    assert result["name"] == new_data.name
    assert result["owner"]["userId"] == str(new_data.owner_id)
    assert result["run"]["runId"] == str(new_data.run_id)
    assert result["connection"]["connectionId"] == str(new_data.connection_id)
    assert result["computeBudgetAccount"] == new_data.compute_budget_account
    assert result["cronExpression"] == new_data.cron_expression
    assert result["timeZone"] == new_data.time_zone
    assert result["endDate"] == new_data.end_date


@pytest.mark.usefixtures("add_example_run_schedule_on_aws")
@pytest.mark.usefixtures("add_second_example_run")
@pytest.mark.usefixtures("add_second_example_user")
def test_projects_project_id_run_schedules_run_schedule_id_put_when_insufficient_role(
    client: testclient.TestClient,
    mock_get_second_user_info_and_be_project_researcher,
    project_id_str,
    run_schedule_id_str,
    api_add_run_schedule,
    api_run_schedule,
    second_run_id_str,
    second_user_id_str,
    mock_hvac,
) -> None:
    """Test changing of owner of a run schedule not possible for all
    roles < Maintainer.
    """

    new_data = models.run_schedule.AddRunSchedule(
        name="new-name",
        owner_id=second_user_id_str,
        run_id=second_run_id_str,
        connection_id=api_add_run_schedule.connection_id,
        compute_budget_account="new-compute-budget-account",
        cron_expression="1 12 * * ?",
        time_zone="Europe/Berlin",
        end_date=1680013252,
    )

    # Attempt to change owner while having insufficient role (Researcher)
    response = client.put(
        f"/projects/{project_id_str}/run-schedules/{run_schedule_id_str}",
        headers=HEADERS,
        json=new_data.dict(),
    )

    assert response.status_code == status.HTTP_403_FORBIDDEN, response.text

    # Check run schedule unchanged
    response = client.get(
        f"/projects/{project_id_str}/run-schedules/{run_schedule_id_str}",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["name"] == api_run_schedule.name
    assert result["owner"]["userId"] == str(api_run_schedule.owner.user_id)
    assert result["run"]["runId"] == str(api_run_schedule.run.run_id)
    assert result["connection"]["connectionId"] == str(
        api_run_schedule.connection.connection_id
    )
    assert result["computeBudgetAccount"] == api_run_schedule.compute_budget_account
    assert result["cronExpression"] == api_run_schedule.cron_expression
    assert result["timeZone"] == api_run_schedule.time_zone
    assert result["endDate"] == api_run_schedule.end_date


@pytest.mark.usefixtures("add_example_run_schedule_on_aws")
@pytest.mark.usefixtures("add_second_example_run")
@pytest.mark.usefixtures("add_example_connection_second_user")
def test_projects_project_id_run_schedules_run_schedule_id_put_unowned_connection(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    project_id_str,
    run_schedule_id_str,
    api_run_schedule,
    api_connection_second_user,
    mock_hvac,
) -> None:
    new_data = models.run_schedule.AddRunSchedule(
        name="new-name",
        owner_id=api_run_schedule.owner.user_id,
        run_id=api_run_schedule.run.run_id,
        connection_id=api_connection_second_user.connection_id,
        compute_budget_account="new-compute-budget-account",
        cron_expression="1 12 * * ?",
        time_zone="Europe/Berlin",
        end_date=1680013252,
    )

    # Attempt to change connection to connection owned by other user
    response = client.put(
        f"/projects/{project_id_str}/run-schedules/{run_schedule_id_str}",
        headers=HEADERS,
        json=new_data.dict(),
    )

    assert response.status_code == status.HTTP_403_FORBIDDEN, response.text

    # Check run schedule unchanged
    response = client.get(
        f"/projects/{project_id_str}/run-schedules/{run_schedule_id_str}",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["name"] == api_run_schedule.name
    assert result["owner"]["userId"] == str(api_run_schedule.owner.user_id)
    assert result["run"]["runId"] == str(api_run_schedule.run.run_id)
    assert result["connection"]["connectionId"] == str(
        api_run_schedule.connection.connection_id
    )
    assert result["computeBudgetAccount"] == api_run_schedule.compute_budget_account
    assert result["cronExpression"] == api_run_schedule.cron_expression
    assert result["timeZone"] == api_run_schedule.time_zone
    assert result["endDate"] == api_run_schedule.end_date


@pytest.mark.usefixtures("add_example_run_schedule_on_aws")
def test_projects_project_id_run_schedules_run_schedule_id_delete(
    monkeypatch,
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    project_id_str,
    run_schedule_id_str,
    mock_hvac,
) -> None:
    # Check run schedule exists
    response = client.get(
        f"/projects/{project_id_str}/run-schedules/{run_schedule_id_str}",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    # Delete run schedule
    response = client.delete(
        f"/projects/{project_id_str}/run-schedules/{run_schedule_id_str}",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Check run schedule deleted
    response = client.get(
        f"/projects/{project_id_str}/run-schedules/{run_schedule_id_str}",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND, response.text


@pytest.mark.usefixtures("add_example_run_schedule_on_aws")
def test_projects_project_id_run_schedules_run_schedule_id_delete_when_insufficient_role(  # noqa: E501
    client: testclient.TestClient,
    mock_get_second_user_info_and_be_project_researcher,
    project_id_str,
    run_schedule_id_str,
    mock_hvac,
) -> None:
    """Test delete of run schedule not possible for all roles < Maintainer."""
    # Check run schedule exists
    response = client.get(
        f"/projects/{project_id_str}/run-schedules/{run_schedule_id_str}",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    # Attempt delete of run schedule
    response = client.delete(
        f"/projects/{project_id_str}/run-schedules/{run_schedule_id_str}",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_403_FORBIDDEN, response.text

    # Check run schedule still exists
    response = client.get(
        f"/projects/{project_id_str}/run-schedules/{run_schedule_id_str}",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
