import fastapi.testclient as testclient
import pytest
import starlette.status as status

import mantik_api.database as database
import mantik_api.testing as testing


@pytest.fixture
def api_user_groups(second_api_user_group, project_id_str) -> dict:
    return {
        "groups": [
            {
                "projectId": project_id_str,
                "role": database.project.ProjectRole.REPORTER.value,
                "userGroup": second_api_user_group.dict(),
                "createdAt": testing.time.DEFAULT_CREATED_AT.isoformat(),
                "updatedAt": None,
            }
        ],
        "totalRecords": 1,
        "pageRecords": 1,
        "userRole": -1,
    }


@pytest.mark.parametrize(
    ("startindex", "length"),
    [
        (0, 1),
        (30, 0),
    ],
)
@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_second_user_group_project_role_as_reporter")
def test_projects_project_id_groups_get_logged_in(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    startindex,
    length,
    second_user_group_id_str,
    project_id_str,
):
    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER

    params = [("startindex", startindex), ("pagelength", 50)]
    response = client.get(
        f"/projects/{project_id_str}/groups",
        headers=headers,
        params=params,
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["totalRecords"] == 1
    assert result["pageRecords"] == length
    assert result["userRole"] == database.project.ProjectRole.OWNER.value


@pytest.mark.parametrize(
    ("headers", "expected_status_code"),
    (
        ({}, status.HTTP_200_OK),
        ({"Authorization": ""}, status.HTTP_401_UNAUTHORIZED),
        (
            {"Authorization": "InvalidAuth test-invalid-token"},
            status.HTTP_401_UNAUTHORIZED,
        ),
        (
            testing.token_verifier.INVALID_AUTHORIZATION_HEADER,
            status.HTTP_401_UNAUTHORIZED,
        ),
    ),
)
@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_example_user_group")
@pytest.mark.usefixtures("add_second_user_group_project_role_as_reporter")
def test_projects_project_id_groups_get_logged_out(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    user_group_id_str,
    project_id_str,
    headers,
    expected_status_code,
    api_user_groups,
):
    response = client.get(
        f"/projects/{project_id_str}/groups",
        headers=headers,
    )
    assert response.status_code == expected_status_code, response.text

    if expected_status_code == status.HTTP_200_OK:
        result = response.json()
        assert result == api_user_groups


@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_example_user_group")
@pytest.mark.usefixtures("add_second_user_group_project_role_as_reporter")
def test_projects_project_id_groups_group_id_get_logged_in(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    second_user_group_id_str,
    project_id_str,
    second_api_user_group,
):
    role = database.project.ProjectRole.REPORTER.value
    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER

    response = client.get(
        f"/projects/{project_id_str}/groups/{second_user_group_id_str}",
        headers=headers,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    result = response.json()
    assert result["role"] == role
    assert result["userGroup"] == second_api_user_group.dict()


@pytest.mark.parametrize(
    ("headers", "expected_status_code"),
    (
        ({}, status.HTTP_200_OK),
        ({"Authorization": ""}, status.HTTP_401_UNAUTHORIZED),
        (
            {"Authorization": "InvalidAuth test-invalid-token"},
            status.HTTP_401_UNAUTHORIZED,
        ),
        (
            testing.token_verifier.INVALID_AUTHORIZATION_HEADER,
            status.HTTP_401_UNAUTHORIZED,
        ),
    ),
)
@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_example_user_group")
@pytest.mark.usefixtures("add_second_user_group_project_role_as_reporter")
def test_projects_project_id_groups_group_id_get_logged_out(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    second_user_group_id_str,
    project_id_str,
    headers,
    expected_status_code,
):
    response = client.get(
        f"/projects/{project_id_str}/groups/{second_user_group_id_str}",
        headers=headers,
    )

    assert response.status_code == expected_status_code, response.text

    if response.status_code == status.HTTP_200_OK:
        result = response.json()
        assert result["role"] == database.project.ProjectRole.REPORTER.value


@pytest.mark.parametrize(
    "new_role",
    [
        database.project.ProjectRole.OWNER,
        database.project.ProjectRole.GUEST,
        database.project.ProjectRole.MAINTAINER,
        database.project.ProjectRole.REPORTER,
    ],
)
@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_second_user_group_project_role_as_reporter")
def test_projects_project_id_groups_group_id_put(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    second_user_group_id_str,
    project_id_str,
    new_role: database.project.ProjectRole,
):
    """
    Given a user group has a role in a project
    When the role is updated
    And its role is requested
    Then the retrieved role is the updated role
    """
    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER
    group_role = {
        "role": new_role.name,
    }
    response = client.put(
        f"/projects/{project_id_str}/groups/{second_user_group_id_str}",
        headers=headers,
        json=group_role,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(
        f"/projects/{project_id_str}/groups/{second_user_group_id_str}",
        headers=headers,
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["role"] == new_role.value


@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_second_user_group_project_role_as_reporter")
def test_projects_project_id_groups_group_id_delete(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    second_user_group_id_str,
    project_id_str,
):
    """
    Given a user group has a role in a project
    When the role is deleted
    And its role is requested
    Then it has no role in the project
    """
    # Delete group
    response = client.delete(
        f"/projects/{project_id_str}/groups/{second_user_group_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Check deletion succeeded
    response = client.get(
        f"/projects/{project_id_str}/groups/{second_user_group_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND, response.text

    # Check user group still exists
    response = client.get(
        f"/groups/{second_user_group_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
