import uuid

import fastapi.testclient as testclient
import pytest
import responses
import starlette.status as status

import mantik_api.database as database
import mantik_api.testing as testing
import mantik_api.utils.git_utils as git_utils


@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_data_data_repository_id_get_logged_in(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    data_repository_id_str,
):
    """Test case for projects_project_id_data_data_repository_id_get

    Returns data entry for given project
    """
    response = client.get(
        f"/projects/{project_id_str}/data/{data_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["dataRepositoryId"] == data_repository_id_str
    assert result["userRole"] == 5


@pytest.mark.parametrize(
    ("headers", "expected_status_code"),
    (
        ({}, status.HTTP_200_OK),
        ({"Authorization": ""}, status.HTTP_401_UNAUTHORIZED),
        (
            {"Authorization": "InvalidAuth test-invalid-token"},
            status.HTTP_401_UNAUTHORIZED,
        ),
        (
            testing.token_verifier.INVALID_AUTHORIZATION_HEADER,
            status.HTTP_401_UNAUTHORIZED,
        ),
    ),
)
@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_data_data_repository_id_get_logged_out(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    data_repository_id_str,
    headers,
    expected_status_code,
):
    """Test case for projects_project_id_data_data_repository_id_get

    Returns data entry for given project
    """
    response = client.get(
        f"/projects/{project_id_str}/data/{data_repository_id_str}",
        headers=headers,
    )

    assert response.status_code == expected_status_code, response.text

    if expected_status_code == status.HTTP_200_OK:
        result = response.json()
        assert result["dataRepositoryId"] == data_repository_id_str
        assert result["userRole"] == -1


@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_data_data_repository_id_put(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    data_repository_id_str,
    api_label_4,
):
    """Test case for projects_project_id_data_data_repository_id_put

    Update data repository
    """
    data_repository = {
        "description": "new_description",
        "dataRepositoryName": "dataRepositoryName",
        "platform": "GitLab",
        "uri": "uri",
        "labels": [str(api_label_4.label_id)],
    }

    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER
    response = client.put(
        f"/projects/{project_id_str}/data/{data_repository_id_str}",
        headers=headers,
        json=data_repository,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(f"/projects/{project_id_str}/data/{data_repository_id_str}")

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    expected = result | {
        "description": "new_description",
        "labels": [api_label_4.dict()],
        "userRole": -1,
    }

    assert result == expected


@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_data_data_repository_id_put_invalid_label_scope(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    data_repository_id_str,
    api_label_1,
    api_label_4,
    api_label_5,
):
    """Test case for projects_project_id_data_data_repository_id_put

    Update data repository
    """
    data_repository = {
        "description": "new_description",
        "dataRepositoryName": "dataRepositoryName",
        "platform": "GitLab",
        "uri": "uri",
        "labels": [str(api_label_1.label_id)],
    }

    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER
    response = client.put(
        f"/projects/{project_id_str}/data/{data_repository_id_str}",
        headers=headers,
        json=data_repository,
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST, response.text
    assert "Unable to add label" in response.text, "Incorrect error message in response"

    not_updated_response = client.get(
        f"/projects/{project_id_str}/data/{data_repository_id_str}",
        headers=headers,
    )

    assert (
        not_updated_response.status_code == status.HTTP_200_OK
    ), not_updated_response.text
    result = not_updated_response.json()
    assert result["labels"] == [api_label_4.dict(), api_label_5.dict()]


@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_data_get_logged_in(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
):
    """Test case for projects_project_id_data_get

    Returns data entries for given project
    """
    response = client.get(
        f"/projects/{project_id_str}/data",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["totalRecords"] == 2
    assert result["userRole"] == 5

    for data_repository in result["dataRepositories"]:
        assert data_repository["userRole"] == 5


@pytest.mark.parametrize(
    ("headers", "expected_status_code"),
    (
        ({}, status.HTTP_200_OK),
        ({"Authorization": ""}, status.HTTP_401_UNAUTHORIZED),
        (
            {"Authorization": "InvalidAuth test-invalid-token"},
            status.HTTP_401_UNAUTHORIZED,
        ),
        (
            testing.token_verifier.INVALID_AUTHORIZATION_HEADER,
            status.HTTP_401_UNAUTHORIZED,
        ),
    ),
)
@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_data_get_logged_out(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    headers,
    expected_status_code,
):
    """Test case for projects_project_id_data_get

    Returns data entries for given project
    """
    response = client.get(
        f"/projects/{project_id_str}/data",
        headers=headers,
    )

    assert response.status_code == expected_status_code, response.text

    if response.status_code == status.HTTP_200_OK:
        result = response.json()
        assert result["totalRecords"] == 2
        assert result["userRole"] == -1

        for data_repository in result["dataRepositories"]:
            assert data_repository["userRole"] == -1


@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_data_post(
    client: testclient.TestClient, mock_token_user_id, project_id_str, api_label_4
):
    """Test case for projects_project_id_data_post

    Add data repository
    """
    data_repository = {
        "description": "test_data_repository_post",
        "dataRepositoryName": "dataRepositoryName",
        "platform": "GitLab",
        "uri": "uri",
        "labels": [str(api_label_4.label_id)],
    }

    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER
    response = client.post(
        f"/projects/{project_id_str}/data",
        headers=headers,
        json=data_repository,
    )
    assert response.status_code == status.HTTP_201_CREATED, response.text
    data_repository_id = response.json()["dataRepositoryId"]

    response = client.get(
        f"/projects/{project_id_str}/data/{data_repository_id}",
        headers=headers,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["description"] == "test_data_repository_post"
    assert response.json()["dataRepositoryName"] == "dataRepositoryName"
    assert response.json()["platform"] == "GitLab"
    assert response.json()["uri"] == "uri"
    assert response.json()["isDvcEnabled"] is False
    assert response.json()["dvcConnectionId"] is None
    assert response.json()["versions"] == {}

    response = client.get(
        f"/projects/{project_id_str}/data",
        headers=headers,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    assert len(response.json()["dataRepositories"]) == 3

    response = client.get(
        f"/projects/{project_id_str}",
        headers=headers,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    assert len(response.json()["dataRepositories"]) == 3


@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_data_post_invalid_label_scope(
    client: testclient.TestClient, mock_token_user_id, project_id_str, api_label_1
):
    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER
    all_data_repositories_before = client.get(
        f"/projects/{project_id_str}/data",
        headers=headers,
    )

    data_repository = {
        "description": "test_data_repository_post",
        "dataRepositoryName": "dataRepositoryName",
        "platform": "GitLab",
        "uri": "uri",
        "labels": [str(api_label_1.label_id)],
    }
    response = client.post(
        f"/projects/{project_id_str}/data",
        headers=headers,
        json=data_repository,
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST, response.text
    assert "Unable to add label" in response.text, "Incorrect error message in response"

    all_data_repositories_after = client.get(
        f"/projects/{project_id_str}/data",
        headers=headers,
    )
    assert (
        all_data_repositories_before.status_code == status.HTTP_200_OK
    ), all_data_repositories_before.text
    assert (
        all_data_repositories_after.status_code == status.HTTP_200_OK
    ), all_data_repositories_after.text
    assert len(all_data_repositories_before.json()["dataRepositories"]) == len(
        all_data_repositories_after.json()["dataRepositories"]
    )


@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_data_data_repository_id_delete(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    data_repository_id_str,
):
    response = client.delete(
        f"/projects/{project_id_str}/data/{data_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Check deletion succeeded
    response = client.get(
        f"/projects/{project_id_str}/data/{data_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND, response.text


@pytest.mark.usefixtures("add_example_run")
def test_projects_project_id_data_data_repository_id_delete_prevented(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    data_repository_id_str,
    run_id_str,
):
    # Attempt to delete Data Repository
    response = client.delete(
        f"/projects/{project_id_str}/data/{data_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_409_CONFLICT, response.text

    result = response.json()["detail"]
    expected = "Resource still references Run"
    assert result == expected

    # Check deletion not succeeded
    response = client.get(
        f"/projects/{project_id_str}/data/{data_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text


def test_projects_project_id_data_data_repository_id_delete_manual_references(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    data_repository_id_str,
    run_id_str,
    mock_hvac,
    mock_tracking_server_url,
    add_example_run_with_mlflow_run,
):
    # Delete Run that uses the respective Data Repository
    response = client.delete(
        f"/projects/{project_id_str}/runs/{run_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Delete Data Repository
    response = client.delete(
        f"/projects/{project_id_str}/data/{data_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Check deletion succeeded
    response = client.get(
        f"/projects/{project_id_str}/data/{data_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND, response.text


def test_data_repository_dvc_add_new_version(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    project_id_str,
    data_repository_id_str,
) -> None:
    """
    Given I have access to a data repository
    When  I want to enable DVC and add a new version
    Then  I can do so
    And   the versions are persisted
    """

    data_repository = {
        "isDvcEnabled": True,
        "dataRepositoryName": "dataRepositoryName",
        "platform": "GitLab",
        "uri": "uri",
        "versions": {
            "v1": "some-git-commit-hash-123",
            "v2": "some-git-commit-hash-456",
        },
    }

    response = client.put(
        f"/projects/{project_id_str}/data/{data_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=data_repository,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(
        f"/projects/{project_id_str}/data/{data_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["isDvcEnabled"] is True
    assert response.json()["versions"] == {
        "v1": "some-git-commit-hash-123",
        "v2": "some-git-commit-hash-456",
    }


def test_data_repository_add_dvc_connection(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    project_id_str,
    data_repository_id_str,
    add_database_connection_dvc_of_first_user,
    api_connection_dvc_of_first_user,
) -> None:
    """
    Given I have access to a data repository
    When  I want to add DVC credentials to that repository
    Then  I can do so
    And   the credentials are persisted
    """
    response = client.get(
        f"/projects/{project_id_str}/data/{data_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    # Connection not assigned yet
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["dvcConnectionId"] is None

    # Assign a dvc connection to the repository
    response = client.post(
        f"/projects/{project_id_str}/data/{data_repository_id_str}/connection",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json={
            "connectionId": str(api_connection_dvc_of_first_user.connection_id),
        },
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Connection should now be assigned
    response = client.get(
        f"/projects/{project_id_str}/data/{data_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["dvcConnectionId"] == str(
        api_connection_dvc_of_first_user.connection_id
    )


@pytest.fixture
def connect_data_repo_with_dvc_connection(
    database_client,
    database_connection_dvc_of_first_user,
    database_data_repository,
    add_database_connection_dvc_of_first_user,
):
    with database_client.session():
        database_client.add(
            database.data_repository.DataRepositoryAndConnectionAssociationTable(
                id=uuid.uuid4(),
                connection_id=database_connection_dvc_of_first_user.id,
                data_repository_id=database_data_repository.id,
            )
        )


def test_data_repository_remove_dvc_connection(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    project_id_str,
    data_repository_id_str,
    api_connection_dvc_of_first_user,
    connect_data_repo_with_dvc_connection,
) -> None:
    """
    Given I have access to a data repository
    And   I have DVC credentials linked to that repository
    When  I unlink the credentials
    Then  they are no longer linked to the repository
    """
    # check that dvc connection is linked
    response = client.get(
        f"/projects/{project_id_str}/data/{data_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["dvcConnectionId"] == str(
        api_connection_dvc_of_first_user.connection_id
    )

    # unlink the connection
    response = client.delete(
        f"/projects/{project_id_str}/data/{data_repository_id_str}/connection"
        f"/{str(api_connection_dvc_of_first_user.connection_id)}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # check that dvc connection is no longer linked
    response = client.get(
        f"/projects/{project_id_str}/data/{data_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["dvcConnectionId"] is None


def test_data_repository_dvc_connections_of_other_users_are_hidden(
    client: testclient.TestClient,
    add_example_project,
    mock_token_second_user_id,
    project_id_str,
    data_repository_id_str,
    api_connection_dvc_of_first_user,
    connect_data_repo_with_dvc_connection,
) -> None:
    """
    Given I have access to a data repository
    And   another user has access to the repository
    And   that user has their dvc credentials to the repository linked
    When  I fetch the repository details
    Then  I do not see that user's dvc connection
    """
    response = client.get(
        f"/projects/{project_id_str}/data/{data_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["dvcConnectionId"] is None


def test_data_repository_only_supports_adding_one_dvc_connection_per_user(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    project_id_str,
    data_repository_id_str,
    api_connection_dvc_of_first_user,
    connect_data_repo_with_dvc_connection,
) -> None:
    """
    Given I have access to a data repository
    And   a dvc connection linked to it
    When  I try adding another dvc connection
    Then  I get an error
    """
    response = client.post(
        f"/projects/{project_id_str}/data/{data_repository_id_str}/connection",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json={
            "connectionId": str(api_connection_dvc_of_first_user.connection_id),
        },
    )
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, response.text
    assert response.json() == {
        "detail": "Data repository already has a DVC connection!"
    }


def test_data_repository_add_git_connection(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    project_id_str,
    data_repository_id_str,
    api_gitlab_connection_of_first_user,
    add_database_git_connection_of_first_user,
) -> None:
    """
    Given I have a private repository (data)
    When  I store it in Mantik
    Then  I can store an access token with it
    """
    response = client.get(
        f"/projects/{project_id_str}/data/{data_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    # Connection not assigned yet
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["connectionId"] is None

    # Assign a git connection to the repository
    response = client.post(
        f"/projects/{project_id_str}/data/{data_repository_id_str}/connection",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json={
            "connectionId": str(api_gitlab_connection_of_first_user.connection_id),
        },
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Connection should now be assigned
    response = client.get(
        f"/projects/{project_id_str}/data/{data_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["connectionId"] == str(
        api_gitlab_connection_of_first_user.connection_id
    )


def test_data_repository_add_new_data_repository_already_with_git_connection(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    project_id_str,
    api_gitlab_connection_of_first_user,
    add_database_git_connection_of_first_user,
) -> None:
    """
    Given I have a private repository (data)
    When  I store it in Mantik
    Then  I can store an access token with it directly while entering
    the details of the repo
    """
    new_data_repo_json = {
        "data_repository_name": "Repo Name",
        "uri": "github.com/fake",
        "platform": "GitLab",
        "connection_id": str(api_gitlab_connection_of_first_user.connection_id),
    }
    response = client.post(
        f"/projects/{project_id_str}/data",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=new_data_repo_json,
    )
    assert response.status_code == status.HTTP_201_CREATED, response.text
    new_data_repo_id = response.json()["dataRepositoryId"]

    # Connection should now be assigned
    response = client.get(
        f"/projects/{project_id_str}/data/{new_data_repo_id}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json() == response.json() | {
        "connectionId": str(api_gitlab_connection_of_first_user.connection_id),
        "dataRepositoryName": "Repo Name",
        "uri": "github.com/fake",
        "platform": "GitLab",
    }


@pytest.fixture
def connect_data_repo_with_git_connection(
    database_client,
    database_git_connection_of_first_user,
    database_data_repository,
    add_database_git_connection_of_first_user,
):
    with database_client.session():
        database_client.add(
            database.data_repository.DataRepositoryAndConnectionAssociationTable(
                id=uuid.uuid4(),
                connection_id=database_git_connection_of_first_user.id,
                data_repository_id=database_data_repository.id,
            )
        )


@pytest.mark.parametrize(
    "mock_token_fixture,connection_should_be_displayed",
    (["mock_token_user_id", True], ["mock_token_second_user_id", False]),
    ids=[
        "user 1 can see connection of user 1",
        "user 2 can not see connection of user 1",
    ],
)
def test_code_repository_git_connections_of_other_users_are_hidden(
    client: testclient.TestClient,
    add_example_project,
    project_id_str,
    data_repository_id_str,
    connect_data_repo_with_git_connection,
    mock_token_fixture: str,
    connection_should_be_displayed: bool,
    request,
) -> None:
    """
    Given I'm invited to someone else's project
    And   they have a private repository
    And   I have my own access token to the repository outside of the Mantik platform
    When  I want to associate my access token
    Then  I can do so using the mantik platform
    And   the other user has no access to my token
    """
    request.getfixturevalue(mock_token_fixture)

    response = client.get(
        f"/projects/{project_id_str}/data/{data_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    if connection_should_be_displayed:
        assert response.json()["connectionId"] is not None
    else:
        assert response.json()["connectionId"] is None


def test_data_repository_only_supports_adding_one_git_connection_per_user(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    project_id_str,
    data_repository_id_str,
    api_gitlab_connection_of_first_user,
    connect_data_repo_with_git_connection,
) -> None:
    """
    Given I have access to a data repository
    And   a git connection linked to it
    When  I try adding another git connection
    Then  I get an error
    """
    response = client.post(
        f"/projects/{project_id_str}/data/{data_repository_id_str}/connection",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json={
            "connectionId": str(api_gitlab_connection_of_first_user.connection_id),
        },
    )
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, response.text
    assert response.json() == {
        "detail": "Data repository already has a git connection."
    }


def test_data_repository_remove_git_connection(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    project_id_str,
    data_repository_id_str,
    api_gitlab_connection_of_first_user,
    connect_data_repo_with_git_connection,
) -> None:
    """
    Given I have access to a data repository
    And   I have git credentials linked to that repository
    When  I unlink the credentials
    Then  they are no longer linked to the repository
    """
    # check that git connection is linked
    response = client.get(
        f"/projects/{project_id_str}/data/{data_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["connectionId"] == str(
        api_gitlab_connection_of_first_user.connection_id
    )

    # unlink the connection
    response = client.delete(
        f"/projects/{project_id_str}/data/{data_repository_id_str}/connection"
        f"/{str(api_gitlab_connection_of_first_user.connection_id)}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # check that git connection is no longer linked
    response = client.get(
        f"/projects/{project_id_str}/data/{data_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["connectionId"] is None


def test_put_request_does_not_change_git_connection_for_data_repository(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    project_id_str,
    data_repository_id_str,
    api_gitlab_connection_of_first_user,
    connect_data_repo_with_git_connection,
):
    """
    Given I have a data repository with a git connection
    When  I update the data repository
    Then  the git connection should not be changed
    """

    # check that git connection is linked
    response = client.get(
        f"/projects/{project_id_str}/data/{data_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["connectionId"] == str(
        api_gitlab_connection_of_first_user.connection_id
    )

    # update the code repository
    response = client.put(
        f"/projects/{project_id_str}/data/{data_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json={
            "uri": "privateinstance.gitlab.com",
            "platform": "gitlab",
            "connectionId": "12345",
        },
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # check that git connection is still linked
    response = client.get(
        f"/projects/{project_id_str}/data/{data_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["connectionId"] == str(
        api_gitlab_connection_of_first_user.connection_id
    )


@pytest.fixture()
def mock_git_request(api_data_repository, sample_gitlab_token):
    # mock request with correct headers
    responses.add(
        method="GET",
        url=git_utils.raw_gitlab_url(url=api_data_repository.uri),
        status=200,
        match=[
            responses.matchers.header_matcher(
                git_utils.git_authorization_header(
                    access_token=sample_gitlab_token,
                    platform=database.git_repository.Platform.GitLab,
                )
            )
        ],
    )

    # mock request without headers
    responses.add(
        method="GET",
        url=git_utils.raw_gitlab_url(url=api_data_repository.uri),
        status=404,
        match=[responses.matchers.header_matcher({})],
    )
    yield


@responses.activate
def test_data_repo_is_unlocked(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    project_id_str,
    data_repository_id_str,
    mock_vault_credential_sample_access_token,
    mock_git_request,
    connect_data_repo_with_git_connection,
):
    """
    Given I have a data repository
    And   a git connection linked to it
    And   that connection contains a valid access token
    When  I check the repository's unlocked status
    And   querying the git returns status code 200
    Then  I get that it is unlocked
    """

    response = client.get(
        f"/projects/{project_id_str}/data/is-unlocked",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params={"dataRepositoryIds": [data_repository_id_str]},
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json() == {data_repository_id_str: True}


@responses.activate
def test_data_repo_is_locked(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    project_id_str,
    data_repository_id_str,
    mock_git_request,
):
    """
    Given I have a data repository
    And   I do not have a valid access token
    And   querying the git repository returns status code 404
    When  I check the repository's unlocked status
    Then  I get that it is locked
    """

    response = client.get(
        f"/projects/{project_id_str}/data/is-unlocked",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params={"dataRepositoryIds": [data_repository_id_str]},
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json() == {data_repository_id_str: False}


def test_create_data_repository_with_pre_selected_git_connection(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    project_id_str,
    api_gitlab_connection_of_first_user,
    add_database_git_connection_of_first_user,
    api_label_4,
):
    """
    Given I have a git connection
    When  I create a data repository
    And   I attach that connection to it immediately
    Then  the repository is created
    And   the connection is linked to the repository

    """
    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER
    new_data_repository = {
        "dataRepositoryName": "new_name",
        "uri": "new_uri",
        "description": "new description",
        "labels": [str(api_label_4.label_id)],
        "platform": "GitLab",
        "connectionId": str(api_gitlab_connection_of_first_user.connection_id),
    }

    response = client.post(
        f"/projects/{project_id_str}/data",
        headers=headers,
        json=new_data_repository,
    )
    assert response.status_code == status.HTTP_201_CREATED, response.text
    data_repository_id = response.json()["dataRepositoryId"]

    response = client.get(
        f"/projects/{project_id_str}/data/{data_repository_id}",
        headers=headers,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["connectionId"] == str(
        api_gitlab_connection_of_first_user.connection_id
    )


def test_create_data_repository_with_pre_selected_not_git_connection(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    project_id_str,
    api_connection_dvc_of_first_user,
    add_database_connection_dvc_of_first_user,
    api_label_4,
):
    """
    Given I have a connection ( not git one)
    When  I create a data repository
    And   I attach that connection to it immediately
    Then  the repository is not created
    And   the connection is not linked to the repository
    And   a suitable error is returned

    """
    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER
    new_data_repository = {
        "dataRepositoryName": "new_name",
        "uri": "new_uri",
        "description": "new description",
        "labels": [str(api_label_4.label_id)],
        "platform": "GitHub",
        "connectionId": str(api_connection_dvc_of_first_user.connection_id),
    }

    response = client.post(
        f"/projects/{project_id_str}/data",
        headers=headers,
        json=new_data_repository,
    )

    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, response.text
    assert (
        response.json()["detail"]
        == "Connection platform 'GitHub' and repository platform 'S3' do not match."
    )
