import unittest.mock
import uuid

import fastapi.testclient as testclient
import pytest
import starlette.status as status

import mantik_api.testing as testing


def test_projects_project_id_experiment_experiments_repository_id_get_logged_in(
    client: testclient.TestClient,
    add_example_project,
    experiment_repository_id_str: str,
    mock_token_user_id,
    project_id_str,
):
    """Test case for projects_project_id_experiments_experiment_repository_id_get

    Returns experiment entry for given project
    """
    response = client.get(
        f"/projects/{project_id_str}/experiments/{experiment_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    result = response.json()

    assert result["userRole"] == 5


@pytest.mark.parametrize(
    ("headers", "expected_status_code"),
    (
        ({}, status.HTTP_200_OK),
        ({"Authorization": ""}, status.HTTP_401_UNAUTHORIZED),
        (
            {"Authorization": "InvalidAuth test-invalid-token"},
            status.HTTP_401_UNAUTHORIZED,
        ),
        (
            testing.token_verifier.INVALID_AUTHORIZATION_HEADER,
            status.HTTP_401_UNAUTHORIZED,
        ),
    ),
)
def test_projects_project_id_experiment_experiments_repository_id_get_logged_out(
    client: testclient.TestClient,
    add_example_project,
    experiment_repository_id_str: str,
    mock_token_user_id,
    project_id_str,
    headers,
    expected_status_code,
):
    """Test case for projects_project_id_experiments_experiment_repository_id_get

    Returns experiment entry for given project
    """

    response = client.get(
        f"/projects/{project_id_str}/experiments/{experiment_repository_id_str}",
        headers=headers,
    )

    assert response.status_code == expected_status_code, response.text

    if expected_status_code == status.HTTP_200_OK:
        result = response.json()
        assert result["experimentRepositoryId"] == experiment_repository_id_str
        assert result["userRole"] == -1


def test_projects_project_id_experiment_experiment_repository_id_put(
    client: testclient.TestClient,
    mock_tracking_server_url,
    add_example_project,
    database_client,
    experiment_repository_id_str: str,
    mock_token_user_id,
    project_id_str,
    set_mlflow_tracking_url,
    api_label_4,
):
    """Test case for projects_project_id_experiments_experiment_repository_id_put

    Update experiment repository
    """
    new_name = f"new_name_put_test-{uuid.uuid4()}"
    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER
    experiment_repository = {"name": new_name, "labels": [str(api_label_4.label_id)]}

    response = client.put(
        f"/projects/{project_id_str}/experiments/{experiment_repository_id_str}",
        headers=headers,
        json=experiment_repository,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(
        f"/projects/{project_id_str}/experiments/{experiment_repository_id_str}",
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    expected = (
        result
        | experiment_repository
        | {"labels": [api_label_4.dict()], "userRole": -1}
    )

    assert result == expected


def test_projects_project_id_experiment_experiment_repository_id_put_duplicate_name(
    client: testclient.TestClient,
    mock_tracking_server_url,
    add_example_project,
    database_client,
    experiment_repository_id_str: str,
    mock_token_user_id,
    project_id_str,
    database_experiment_repository,
):
    """Test case for projects_project_id_experiments_experiment_repository_id_put

    Update experiment repository
    """
    new_name = database_experiment_repository.name
    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER
    experiment_repository = {
        "name": new_name,
    }
    expected = f"{new_name}-1"

    response = client.put(
        f"/projects/{project_id_str}/experiments/{experiment_repository_id_str}",
        headers=headers,
        json=experiment_repository,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(
        f"/projects/{project_id_str}/experiments/{experiment_repository_id_str}",
        headers=headers,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["name"] == expected


def test_projects_project_id_experiment_experiment_repository_id_put_invalid_label_scope(  # noqa E501
    client: testclient.TestClient,
    add_example_project,
    experiment_repository_id_str: str,
    mock_token_user_id,
    project_id_str,
    api_label_1,
    api_label_4,
    api_label_5,
):
    new_name = "new_name_put_test"
    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER
    experiment_repository = {"name": new_name, "labels": [str(api_label_1.label_id)]}
    response = client.put(
        f"/projects/{project_id_str}/experiments/{experiment_repository_id_str}",
        headers=headers,
        json=experiment_repository,
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST, response.text
    assert "Unable to add label" in response.text, "Incorrect error message in response"

    not_updated_response = client.get(
        f"/projects/{project_id_str}/experiments/{experiment_repository_id_str}",
        headers=headers,
        json=experiment_repository,
    )

    assert (
        not_updated_response.status_code == status.HTTP_200_OK
    ), not_updated_response.text
    result = not_updated_response.json()
    assert result["labels"] == [api_label_4.dict(), api_label_5.dict()]


@pytest.mark.parametrize(("startindex", "length"), [(0, 2), (30, 0)])
def test_projects_project_id_experiments_get_logged_in(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    project_id_str,
    startindex,
    length,
):
    """Test case for projects_project_id_experiments_get

    Returns experiment entries for given project
    """
    params = [("startindex", startindex), ("pagelength", 50)]
    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER
    response = client.get(
        f"/projects/{project_id_str}/experiments",
        headers=headers,
        params=params,
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["totalRecords"] == 2
    assert result["pageRecords"] == length
    assert result["userRole"] == 5

    for experiment_repository in result["experimentRepositories"]:
        assert experiment_repository["userRole"] == 5


@pytest.mark.parametrize(
    ("headers", "expected_status_code"),
    (
        ({}, status.HTTP_200_OK),
        ({"Authorization": ""}, status.HTTP_401_UNAUTHORIZED),
        (
            {"Authorization": "InvalidAuth test-invalid-token"},
            status.HTTP_401_UNAUTHORIZED,
        ),
        (
            testing.token_verifier.INVALID_AUTHORIZATION_HEADER,
            status.HTTP_401_UNAUTHORIZED,
        ),
    ),
)
def test_projects_project_id_experiments_get_logged_out(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    project_id_str,
    headers,
    expected_status_code,
):
    """Test case for projects_project_id_experiments_get

    Returns experiment entries for given project
    """
    response = client.get(
        f"/projects/{project_id_str}/experiments",
        headers=headers,
    )

    assert response.status_code == expected_status_code, response.text

    if response.status_code == status.HTTP_200_OK:
        result = response.json()
        assert result["totalRecords"] == 2
        assert result["pageRecords"] == 2
        assert result["userRole"] == -1

        for experiment_repository in result["experimentRepositories"]:
            assert experiment_repository["userRole"] == -1


def test_projects_project_id_experiments_post(
    client: testclient.TestClient,
    add_example_project,
    database_client,
    mock_token_user_id,
    project_id_str,
    set_mlflow_tracking_url,
    api_label_4,
):
    """Test case for projects_project_id_experiments_post

    Add experiment repository
    """
    name = str(uuid.uuid4())
    experiment_repository = {"name": name, "labels": [str(api_label_4.label_id)]}

    response = client.post(
        f"/projects/{project_id_str}/experiments",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=experiment_repository,
    )

    assert response.status_code == status.HTTP_201_CREATED, response.text
    experiment_repository_id = response.json()["experimentRepositoryId"]

    response = client.get(
        f"/projects/{project_id_str}/experiments/{experiment_repository_id}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    result = response.json()
    assert result["name"] == name


def test_projects_project_id_experiments_post_invalid_label_scope(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    project_id_str,
    api_label_1,
):
    name = "my_experiment_repo_test"
    all_experiment_repositories_before = client.get(
        f"/projects/{project_id_str}/experiments",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    experiment_repository = {"name": name, "labels": [str(api_label_1.label_id)]}
    response = client.post(
        f"/projects/{project_id_str}/experiments",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=experiment_repository,
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST, response.text
    assert "Unable to add label" in response.text, "Incorrect error message in response"

    all_experiment_repositories_after = client.get(
        f"/projects/{project_id_str}/experiments",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert (
        all_experiment_repositories_before.status_code == status.HTTP_200_OK
    ), all_experiment_repositories_before.text
    assert (
        all_experiment_repositories_after.status_code == status.HTTP_200_OK
    ), all_experiment_repositories_after.text
    assert len(
        all_experiment_repositories_before.json()["experimentRepositories"]
    ) == len(all_experiment_repositories_after.json()["experimentRepositories"])


def test_projects_project_id_experiments_post_duplicate_experiment_name(
    client: testclient.TestClient,
    mock_tracking_server_url,
    add_example_project,
    database_client,
    mock_token_user_id,
    project_id_str,
    database_experiment_repository,
):
    """Test case for projects_project_id_experiments_post

    Add experiment repository
    """
    name = database_experiment_repository.name
    experiment_repository = {"name": name}
    expected = f"{name}-1"

    response = client.post(
        f"/projects/{project_id_str}/experiments",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=experiment_repository,
    )

    assert response.status_code == status.HTTP_201_CREATED, response.text
    experiment_repository_id = response.json()["experimentRepositoryId"]

    response = client.get(
        f"/projects/{project_id_str}/experiments/{experiment_repository_id}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["name"] == expected


def test_projects_project_id_experiments_experiment_repository_id_delete(
    client: testclient.TestClient,
    add_example_project,
    database_client,
    mock_token_user_id,
    project_id_str,
    experiment_repository_id_str,
):
    response = client.delete(
        f"/projects/{project_id_str}/experiments/{experiment_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Check deletion succeeded
    response = client.get(
        f"/projects/{project_id_str}/experiments/{experiment_repository_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND, response.text


@pytest.fixture
def mock_create_unique_name():
    with unittest.mock.patch(
        "mantik_api.utils.mlflow.runs.create_unique_name",
        return_value="not unique name-1",
    ) as _patch:
        yield _patch


@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_experiments_experiment_repository_id_run_unique_name_get(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    set_mlflow_tracking_url,
    experiment_repository_id_str,
    mock_create_unique_name,
):
    name = "not unique name"
    response = client.get(
        f"/projects/{project_id_str}/experiments/"
        f"{experiment_repository_id_str}/unique-mlflow-run-name",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params={"runName": name},
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json() == "not unique name-1"

    mock_create_unique_name.assert_called()
