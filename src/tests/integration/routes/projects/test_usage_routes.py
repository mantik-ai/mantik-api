import fastapi.testclient as testclient
import pytest

import mantik_api.testing as testing


@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_usage_code_get(
    client: testclient.TestClient, project_id_str
):
    """Test case for projects_project_id_usage_code_get

    Returns all codeRepos for given project and runs that the codeRepos were used in
    """
    params = [("startindex", 56), ("pagelength", 50)]
    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER
    response = client.request(  # noqa
        "GET",
        f"/projects/{project_id_str}/usage/code",
        headers=headers,
        params=params,
    )

    # uncomment below to assert the status code of the HTTP response
    # assert response.status_code == status.HTTP_200_OK, response.text


@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_usage_data_get(
    client: testclient.TestClient, project_id_str
):
    """Test case for projects_project_id_usage_data_get

    Returns data entries for given project and runs grouped by the data
    """
    params = [("startindex", 56), ("pagelength", 50)]
    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER
    response = client.request(  # noqa
        "GET",
        f"/projects/{project_id_str}/usage/data",
        headers=headers,
        params=params,
    )

    # uncomment below to assert the status code of the HTTP response
    # assert response.status_code == status.HTTP_200_OK, response.text


@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_usage_experiments_get(
    client: testclient.TestClient, project_id_str
):
    """Test case for projects_project_id_usage_experiments_get

    Returns all experiments for given project and runs that the experiments were used in
    """
    params = [("startindex", 56), ("pagelength", 50)]
    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER
    response = client.request(  # noqa
        "GET",
        f"/projects/{project_id_str}/usage/experiments",
        headers=headers,
        params=params,
    )

    # uncomment below to assert the status code of the HTTP response
    # assert response.status_code == status.HTTP_200_OK, response.text


@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_usage_models_get(
    client: testclient.TestClient, project_id_str
):
    """Test case for projects_project_id_usage_models_get

    Returns all models for given project and runs that the models were created in
    """
    params = [("startindex", 56), ("pagelength", 50)]
    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER
    response = client.request(  # noqa
        "GET",
        f"/projects/{project_id_str}/usage/models",
        headers=headers,
        params=params,
    )

    # uncomment below to assert the status code of the HTTP response
    # assert response.status_code == status.HTTP_200_OK, response.text
