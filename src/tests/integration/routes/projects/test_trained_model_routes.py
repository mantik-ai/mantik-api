import os
import pathlib
import unittest.mock
import uuid

import boto3
import fastapi.testclient as testclient
import moto
import pytest
import responses
import starlette.status as status

import mantik_api.database as database
import mantik_api.models.trained_model as trained_models
import mantik_api.routes.mlflow.tracking_server_router as tracking_server_router
import mantik_api.routes.project_routes.model_repository as model_repository
import mantik_api.testing as testing
import mantik_api.testing.requests
import mantik_api.utils.s3 as s3

HEADERS = testing.token_verifier.VALID_AUTHORIZATION_HEADER
TEST_FILE_PATH = (
    pathlib.Path(__file__).parent.parent.parent.parent
    / "resources/files/hello-world.tar.gz"
)


@pytest.fixture
def mock_s3_and_add_sample_model_container(
    trained_model_id_str: str, project_id_str: str
):
    with moto.mock_s3():
        s3 = boto3.client("s3")
        s3.create_bucket(
            Bucket="TEST",
            CreateBucketConfiguration={"LocationConstraint": "eu-central-1"},
        )
        os.environ[model_repository.BUCKET_NAME_ENV_NAME] = "TEST"
        container_name = f"{project_id_str}/{trained_model_id_str}-docker.tar.gz"
        s3.upload_file(TEST_FILE_PATH.as_posix(), "TEST", container_name)

        yield

        os.unsetenv(model_repository.BUCKET_NAME_ENV_NAME)


@pytest.fixture
def api_add_trained_model(connection_id_str, created_at):
    return trained_models.AddTrainedModel(
        name="new-trained-model",
        uri="new-model-uri",
        connection_id=uuid.UUID(connection_id_str),
        location="new-model-location",
        mlflow_parameters={"1": "2"},
    )


@pytest.fixture
def api_add_trained_model_from_run(connection_id_str, run_id_str):
    return trained_models.AddTrainedModel(
        name="new-trained-model-from-run",
        run_id=uuid.UUID(run_id_str),
        connection_id=uuid.UUID(connection_id_str),
        mlflow_parameters={"1": "2"},
    )


@moto.mock_codebuild
@pytest.fixture(scope="function")
def mock_code_builder():
    os.environ[model_repository.CODE_BUILD_ENV_NAME] = "TEST"
    with unittest.mock.patch(
        "mantik_api.service.model_repository.containerize_trained_model"
    ) as patch:
        yield patch

    os.unsetenv(model_repository.CODE_BUILD_ENV_NAME)


@pytest.fixture
def add_trained_model_with_run_to_database(
    database_client,
    clear_and_init_database,
    database_saved_model_repository,
    add_example_run,
    run_id_str,
) -> None:
    database_saved_model_repository.run_id = run_id_str
    with database_client.session():
        database_client.add(database_saved_model_repository)


@pytest.mark.usefixtures("add_trained_model")
@pytest.mark.usefixtures("add_example_project")
def test_get_trained_model(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    trained_model_id_str,
    api_trained_model,
) -> None:
    """Given I have a trained model, I can fetch it"""
    response = client.get(
        f"/projects/{project_id_str}/models/trained/{trained_model_id_str}",
        headers={},
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json() == api_trained_model.dict()


@pytest.mark.usefixtures("add_example_project")
def test_get_all_trained_models_when_i_have_none(
    client: testclient.TestClient, mock_token_user_id, project_id_str, api_trained_model
) -> None:
    """
    Given I don't have trained models,
    Listing project's trained models returns nothing
    """

    response = client.get(
        f"/projects/{project_id_str}/models/trained",
        headers={},
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    expected = trained_models.GetTrainedModelsResponse(
        total_records=0,
        page_records=0,
        models=[],
        user_role=-1,
    ).dict()

    assert response.json() == expected


@pytest.mark.usefixtures("add_trained_model")
@pytest.mark.usefixtures("add_example_project")
def test_get_all_trained_models(
    client: testclient.TestClient, mock_token_user_id, project_id_str, api_trained_model
) -> None:
    """
    Given I have trained models,
    Then I can fetch all existing trained models
    """

    response = client.get(
        f"/projects/{project_id_str}/models/trained",
        headers={},
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    expected = trained_models.GetTrainedModelsResponse(
        total_records=1,
        page_records=1,
        models=[api_trained_model],
        user_role=-1,
    ).dict()
    assert response.json() == expected


@pytest.mark.usefixtures("add_example_project")
def test_add_trained_model_not_from_run(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    api_add_trained_model,
    created_at,
) -> None:
    """
    Given I want to save a trained model
    And  it is not produced by a run
    When I include location and uri in the request
    Then the model is saved
    And I can retrieve it
    """
    core_schema = api_add_trained_model

    response = client.post(
        f"/projects/{project_id_str}/models/trained",
        headers=HEADERS,
        json=api_add_trained_model.dict(),
    )

    assert response.status_code == status.HTTP_201_CREATED, response.text
    new_trained_model_id = response.json()["modelId"]

    response = client.get(
        f"/projects/{project_id_str}/models/trained/{new_trained_model_id}",
        headers={},
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    expected = trained_models.TrainedModel(
        name=core_schema.name,
        model_id=uuid.UUID(new_trained_model_id),
        model_repository=None,
        uri=core_schema.uri,
        connection_id=core_schema.connection_id,
        location=core_schema.location,
        mlflow_parameters=core_schema.mlflow_parameters,
        created_at=created_at,
        status=None,
    ).dict()
    expected.pop("createdAt")

    result = response.json()
    result.pop("createdAt")

    assert result == expected


@pytest.mark.usefixtures("add_trained_model")
@pytest.mark.usefixtures("add_example_project")
def test_add_trained_model_with_same_name_as_already_present(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    api_trained_model,
) -> None:
    updated_schema = api_trained_model
    updated_schema.name = "test-trained-model"

    response = client.post(
        f"/projects/{project_id_str}/models/trained",
        headers=HEADERS,
        json=updated_schema.dict(),
    )
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, response.text
    assert response.json()["detail"] == "Value already present"


@pytest.mark.usefixtures("add_trained_model")
@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("mock_s3_and_add_sample_model_container")
def test_delete_trained_model(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    trained_model_id_str,
) -> None:
    """
    Given I have a trained model,
    when I delete it,
    I can no longer fetch it"""

    response = client.get(
        f"/projects/{project_id_str}/models/trained/{trained_model_id_str}",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    assert s3.list_dir(
        bucket_name=os.environ[model_repository.BUCKET_NAME_ENV_NAME],
        prefix="",
    ) == [f"{project_id_str}/{trained_model_id_str}-docker.tar.gz"]

    response = client.delete(
        f"/projects/{project_id_str}/models/trained/{trained_model_id_str}",
        headers=HEADERS,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(
        f"/projects/{project_id_str}/models/trained/{trained_model_id_str}",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND, response.text
    assert (
        s3.list_dir(
            bucket_name=os.environ[model_repository.BUCKET_NAME_ENV_NAME],
            prefix="",
        )
        == []
    )


@pytest.mark.usefixtures("add_example_project")
def test_update_trained_model_name(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    trained_model_id_str,
    add_trained_model_with_run_to_database,
) -> None:
    """
    Given I have a trained model,
    When I update just its name,
    Then if I request it, it is updated
    And other attributes remain the same
    """
    response = client.get(
        f"/projects/{project_id_str}/models/trained/{trained_model_id_str}",
        headers={},
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    existing_model_json = response.json()

    updated_name = "UPDATED-NAME"
    response = client.put(
        f"/projects/{project_id_str}/models/trained/{trained_model_id_str}",
        headers=HEADERS,
        json={"name": updated_name},
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(
        f"/projects/{project_id_str}/models/trained/{trained_model_id_str}",
        headers={},
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    updated_model_json = response.json()

    updated_model_json.pop("updatedAt")
    existing_model_json.pop("updatedAt")

    assert updated_model_json == existing_model_json | {"name": updated_name}


@pytest.mark.usefixtures("add_trained_model")
@pytest.mark.usefixtures("add_example_project")
def test_update_trained_model(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    trained_model_id_str,
    api_add_trained_model,
    created_at,
) -> None:
    """
    Given I have a trained model,
    When I update it,
    Then if I request it, it is updated
    """
    updated_schema = api_add_trained_model
    updated_schema.name = "test-trained-model"
    response = client.put(
        f"/projects/{project_id_str}/models/trained/{trained_model_id_str}",
        headers=HEADERS,
        json=updated_schema.dict(),
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(
        f"/projects/{project_id_str}/models/trained/{trained_model_id_str}",
        headers={},
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    expected = trained_models.TrainedModel(
        name=updated_schema.name,
        model_id=uuid.UUID(trained_model_id_str),
        model_repository=None,  # model repo relationship removed
        uri=updated_schema.uri,
        connection_id=updated_schema.connection_id,
        location=updated_schema.location,
        mlflow_parameters=updated_schema.mlflow_parameters,
        created_at=created_at,
    ).dict()
    expected.pop("updatedAt")
    result = response.json()
    result.pop("updatedAt")
    assert result == expected


@pytest.mark.usefixtures("add_trained_model")
@pytest.mark.usefixtures("add_example_project")
def test_build_container_from_trained_model(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    trained_model_id_str,
    mock_code_builder,
    api_trained_model,
    user_id_str,
):
    """Given I have a trained model
    When I send it to be containerized
    Then I get a response that the sending succeded.
    And the build status is set to PENDING
    """

    response = client.post(
        f"/projects/{project_id_str}/models/trained/"
        f"{trained_model_id_str}/docker/build",
        headers=HEADERS,
    )
    assert response.status_code == status.HTTP_201_CREATED, response.text
    mock_code_builder.assert_called_once_with(
        code_build_project_name=os.environ[model_repository.CODE_BUILD_ENV_NAME],
        model_id=uuid.UUID(trained_model_id_str),
        project_id=uuid.UUID(project_id_str),
        token="test-valid-token",
        trained_model_uri=api_trained_model.uri,
    )

    response = client.get(
        f"/projects/{project_id_str}/models/trained/{trained_model_id_str}",
        headers=HEADERS,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert (
        response.json()["status"]
        == database.trained_model.ContainerBuildStatus.PENDING.value
    )


@pytest.mark.usefixtures("add_example_project")
def test_build_container_from_trained_model_fails(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    trained_model_id_str,
):
    """Given I don't have a trained model
    When I send it to be containerized
    Then I get a failure."""

    response = client.post(
        f"/projects/{project_id_str}/models/trained/"
        f"{trained_model_id_str}/docker/build",
        headers=HEADERS,
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND, response.text


@pytest.mark.usefixtures("add_trained_model")
@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("mock_s3_and_add_sample_model_container")
def test_create_and_return_model_container_presigned_url(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    trained_model_id_str,
):
    """Given I have a trained model
    When I request its container's download url
    Then I get a url back."""

    response = client.get(
        f"/projects/{project_id_str}/models/trained/{trained_model_id_str}/docker",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json().get("url") is not None


@pytest.mark.usefixtures("add_example_project")
def test_create_and_return_model_container_presigned_url_fails(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    trained_model_id_str,
):
    """Given I don't have access to particular model
    When I request its download url
    Then I get a failure"""

    response = client.get(
        f"/projects/{project_id_str}/models/trained/{trained_model_id_str}/docker",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND, response.text


@pytest.fixture
def mock_mlflow_get_run_artifact_path(run_id_str) -> str:
    os.environ[
        tracking_server_router.TRACKING_SERVER_URL_INTERNAL_ENV_VAR
    ] = mantik_api.testing.requests.BASE_MLFLOW_URL

    mlflow_artifacts_list_url = (
        mantik_api.testing.requests.BASE_MLFLOW_URL + "/api/2.0/mlflow/artifacts/list"
    )
    run_artifact_path = "some-uri"

    responses.add(
        responses.GET,
        url=mlflow_artifacts_list_url,
        json={
            "root_uri": run_artifact_path,
            "files": [
                {"path": "model/MLmodel", "is_dir": False},
            ],
        },
        match=[
            responses.matchers.json_params_matcher(
                params={"path": "model"}, strict_match=False
            )
        ],
    )

    responses.add(
        responses.GET,
        url=mlflow_artifacts_list_url,
        json={
            "root_uri": run_artifact_path,
            "files": [
                {"path": "model", "is_dir": True},
            ],
        },
        match=[responses.matchers.json_params_matcher({}, strict_match=False)],
    )

    yield run_artifact_path

    os.unsetenv(os.environ[tracking_server_router.TRACKING_SERVER_URL_INTERNAL_ENV_VAR])


@responses.activate
def test_register_trained_model_for_successful_run_succeeds(
    client: testclient.TestClient,
    add_example_project,
    api_add_trained_model_from_run,
    add_example_run,
    run_id_str,
    project_id_str,
    mock_token_user_id,
    mock_mlflow_get_run_artifact_path,
    api_run,
):
    """
    Given I have a successful run
    When I register it as a trained model

    Then I can fetch its details
    And the details contain a link to the run
    And the details contain the run name

    And when I fetch the run
    Then the response contains a reference to the trained model
    """

    response = client.post(
        f"/projects/{project_id_str}/models/trained",
        headers=HEADERS,
        json=api_add_trained_model_from_run.dict(),
    )

    assert response.status_code == status.HTTP_201_CREATED, response.text
    trained_model_id = response.json()["modelId"]

    response = client.get(
        f"/projects/{project_id_str}/models/trained/{trained_model_id}",
        headers=HEADERS,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["runId"] == run_id_str
    assert response.json()["uri"] == mock_mlflow_get_run_artifact_path + "/model"
    assert response.json()["runName"] == api_run.name

    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id_str}",
        headers=HEADERS,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json().get("savedModel") is not None
    assert response.json()["savedModel"]["name"] == api_add_trained_model_from_run.name


def test_register_trained_model_for_failed_run_fails(
    client: testclient.TestClient,
    add_example_project,
    api_add_trained_model_from_run,
    add_failed_run,
    run_id_str,
    project_id_str,
    mock_token_user_id,
):
    """
    Given I have a non-successful run
    When I register it as a trained model
    Then I receive an error
    """
    response = client.post(
        f"/projects/{project_id_str}/models/trained",
        headers=HEADERS,
        json=api_add_trained_model_from_run.dict(),
    )

    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, response.text
    assert response.json() == {
        "detail": "Run status needs to be FINISHED. Currently is FAILED"
    }


def test_trained_model_creation_fails_if_necessary_attributes_not_specified(
    client: testclient.TestClient,
    add_example_project,
    add_failed_run,
    run_id_str,
    project_id_str,
    mock_token_user_id,
):
    """
    Given I want to register a trained model
    When I submit neither the run_id nor model_uri + location
    Then I receive and error
    """

    response = client.post(
        f"/projects/{project_id_str}/models/trained",
        headers=HEADERS,
        json={"name": "i am broken"},
    )
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, response.text
    assert "Specify either runId or uri + location" in str(response.json())


@pytest.mark.usefixtures("add_trained_model")
@pytest.mark.usefixtures("add_example_project")
def test_update_trained_model_set_status_to_a_valid_status(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    trained_model_id_str,
    api_trained_model,
    created_at,
) -> None:
    """
    Given I have new trained model
    When  I make a request to update its containerization status
    When  I retrieve the trained model information
    Then  the response contains the status i set
    """
    updated_status = "SUCCESSFUL"

    response = client.put(
        f"/projects/{project_id_str}/models/trained/{trained_model_id_str}/status",
        headers=HEADERS,
        json=updated_status,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(
        f"/projects/{project_id_str}/models/trained/{trained_model_id_str}",
        headers={},
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    api_trained_model.status = updated_status

    expected = api_trained_model.dict()
    expected.pop("updatedAt")
    result = response.json()
    result.pop("updatedAt")

    assert result == expected


@pytest.mark.usefixtures("add_trained_model")
@pytest.mark.usefixtures("add_example_project")
def test_update_trained_model_set_status_to_an_invalid_status(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    trained_model_id_str,
    api_add_trained_model,
    created_at,
) -> None:
    """
    Given I have new trained model
    When  I make a request to update its containerization status
    And   the status is not `pending` | `building` | `successful` | `failed`
    Then  the update is rejected with an informative error message
    """
    updated_status = "non-existent-status"

    response = client.put(
        f"/projects/{project_id_str}/models/trained/{trained_model_id_str}/status",
        headers=HEADERS,
        json=updated_status,
    )

    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, response.text
    assert (
        "value is not a valid enumeration member; permitted: 'PENDING', "
        "'BUILDING', 'SUCCESSFUL', 'FAILED'" in str(response.json())
    )
