import fastapi.testclient as testclient
import pytest
import starlette.status as status

import mantik_api.testing as testing


@pytest.fixture(autouse=True, scope="session")
def skip_if_option_not_passed(skip_if_firecrest_option_not_passed):
    """Skip all tests in this file if the `--use-local-firecrest` option is
    not explicitly passed."""
    pass


def test_projects_project_id_runs_post(
    client: testclient.TestClient,
    add_example_project,
    mock_compute_backend_api_integration_testing_local_firecrest,
    mock_token_user_id,
    project_id_str,
    api_add_run_configuration_firecrest,
    mock_hvac,
) -> None:
    response = client.post(
        f"/projects/{project_id_str}/runs",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=api_add_run_configuration_firecrest.dict(),
    )

    # Check for correct response
    assert response.status_code == status.HTTP_201_CREATED, response.text

    result = response.json()
    run_id = result["runId"]

    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["status"] is not None
