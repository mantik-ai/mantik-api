import fastapi.testclient as testclient

import mantik_api.database.connection as _connection


def test_projects_project_id_deployments_deployment_id_delete(
    client: testclient.TestClient,
):
    """Test case for projects_project_id_deployments_deployment_id_delete

    Delete deployment
    """

    headers = {
        "Authorization": "Bearer special-key",
    }
    response = client.request(  # noqa
        "DELETE",
        "/projects/{projectId}/deployments/{deploymentId}".format(
            projectId="project_id_example", deploymentId="deployment_id_example"
        ),
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    # assert response.status_code == status.HTTP_200_OK, response.text


def test_projects_project_id_deployments_deployment_id_download_get(
    client: testclient.TestClient,
):
    """Test case for projects_project_id_deployments_deployment_id_download_get

    Download packaged deployment
    """

    headers = {
        "Authorization": "Bearer special-key",
    }
    response = client.request(  # noqa
        "GET",
        "/projects/{projectId}/deployments/{deploymentId}/download".format(
            projectId="project_id_example", deploymentId="deployment_id_example"
        ),
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    # assert response.status_code == status.HTTP_200_OK, response.text


def test_projects_project_id_deployments_deployment_id_get(
    client: testclient.TestClient,
):
    """Test case for projects_project_id_deployments_deployment_id_get

    Get deployment information
    """

    headers = {
        "Authorization": "Bearer special-key",
    }
    response = client.request(  # noqa
        "GET",
        "/projects/{projectId}/deployments/{deploymentId}".format(
            projectId="project_id_example", deploymentId="deployment_id_example"
        ),
        headers=headers,
    )

    # uncomment below to assert the status code of the HTTP response
    # assert response.status_code == status.HTTP_200_OK, response.text


def test_projects_project_id_deployments_deployment_id_put(
    client: testclient.TestClient,
):
    """Test case for projects_project_id_deployments_deployment_id_put

    Update deployment
    """
    deployment_information = {
        "cron_string": "cronString",
        "deployment_id": "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
        "resources": {"key": ""},
        "connection": {
            "connectionProvider": _connection.ConnectionProvider.HPC_JSC.value,
            "password": "password",
            "loginName": "loginName",
            "connection_id": "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
            "connectionName": "connectionName",
            "authMethod": "Token",
            "user_id": "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
            "token": "token",
        },
        "saved_model_repository_id": "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
    }

    headers = {
        "Authorization": "Bearer special-key",
    }
    response = client.request(  # noqa
        "PUT",
        "/projects/{projectId}/deployments/{deploymentId}".format(
            projectId="project_id_example", deploymentId="deployment_id_example"
        ),
        headers=headers,
        json=deployment_information,
    )

    # uncomment below to assert the status code of the HTTP response
    # assert response.status_code == status.HTTP_200_OK, response.text


def test_projects_project_id_deployments_deployment_id_predict_get(
    client: testclient.TestClient,
):
    """Test case for projects_project_id_deployments_deployment_id_predict_get

    Get deployment information
    """
    request_body = [{"x": {"0": 0, "1": 1, "2": 2}, "y": {"0": 3, "1": 4, "2": 5}}]

    headers = {
        "Authorization": "Bearer special-key",
    }
    response = client.request(  # noqa
        "GET",
        "/projects/{projectId}/deployments/{deploymentId}/predict".format(
            projectId="project_id_example", deploymentId="deployment_id_example"
        ),
        headers=headers,
        json=request_body,
    )

    # uncomment below to assert the status code of the HTTP response
    # assert response.status_code == status.HTTP_200_OK, response.text


def test_projects_project_id_deployments_get(client: testclient.TestClient):
    """Test case for projects_project_id_deployments_get

    List all deployments
    """
    params = [("startindex", 56), ("pagelength", 50)]
    headers = {
        "Authorization": "Bearer special-key",
    }
    response = client.request(  # noqa
        "GET",
        "/projects/{projectId}/deployments".format(projectId="project_id_example"),
        headers=headers,
        params=params,
    )

    # uncomment below to assert the status code of the HTTP response
    # assert response.status_code == status.HTTP_200_OK, response.text


def test_projects_project_id_deployments_post(client: testclient.TestClient):
    """Test case for projects_project_id_deployments_post

    Create deployment
    """
    deployment_information = {
        "cronString": "cronString",
        "deploymentId": "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
        "resources": {"key": ""},
        "connection": {
            "connectionProvider": _connection.ConnectionProvider.HPC_JSC.value,
            "password": "password",
            "loginName": "loginName",
            "connectionId": "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
            "connectionName": "connectionName",
            "authMethod": "Token",
            "userId": "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
            "token": "token",
        },
        "savedModelRepositoryId": "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
    }

    headers = {
        "Authorization": "Bearer special-key",
    }
    response = client.request(  # noqa
        "POST",
        "/projects/{projectId}/deployments".format(projectId="project_id_example"),
        headers=headers,
        json=deployment_information,
    )

    # uncomment below to assert the status code of the HTTP response
    # assert response.status_code == status.HTTP_200_OK, response.text
