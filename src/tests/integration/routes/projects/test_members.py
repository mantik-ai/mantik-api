import fastapi.testclient as testclient
import pytest
import starlette.status as status

import mantik_api.database as database
import mantik_api.testing as testing


@pytest.mark.parametrize(("startindex", "length"), [(0, 1), (30, 0)])
def test_projects_project_id_members_get_logged_in(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    startindex,
    length,
    user_id_str,
    project_id_str,
):
    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER

    params = [("startindex", startindex), ("pagelength", 50)]
    response = client.get(
        f"/projects/{project_id_str}/members",
        headers=headers,
        params=params,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    result = response.json()
    assert result["totalRecords"] == 1
    assert result["pageRecords"] == length
    assert result["userRole"] == database.project.ProjectRole.OWNER.value


@pytest.mark.parametrize(
    ("headers", "expected_status_code"),
    (
        ({}, status.HTTP_200_OK),
        ({"Authorization": ""}, status.HTTP_401_UNAUTHORIZED),
        (
            {"Authorization": "InvalidAuth test-invalid-token"},
            status.HTTP_401_UNAUTHORIZED,
        ),
        (
            testing.token_verifier.INVALID_AUTHORIZATION_HEADER,
            status.HTTP_401_UNAUTHORIZED,
        ),
    ),
)
@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_members_get_logged_out(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    user_id_str,
    project_id_str,
    headers,
    expected_status_code,
):
    response = client.get(
        f"/projects/{project_id_str}/members",
        headers=headers,
    )
    assert response.status_code == expected_status_code, response.text

    if expected_status_code == status.HTTP_200_OK:
        result = response.json()
        assert result["totalRecords"] == 1
        assert result["userRole"] == database.role_details.ProjectRole.NO_ROLE.value


@pytest.mark.usefixtures("add_example_project")
@pytest.mark.parametrize(
    "add_membership_fixtures",
    [
        ["add_second_project_member_as_reporter"],
        ["add_second_user_group_project_role_as_reporter"],
        ["add_second_organization_project_role_as_reporter"],
        [
            "add_second_project_member_as_reporter",
            "add_second_user_group_project_role_as_reporter",
            "add_second_organization_project_role_as_reporter",
        ],
    ],
    ids=[
        "Direct membership",
        "Membership through group",
        "Membership through organization",
        "Membership through all 3",
    ],
)
def test_get_project_members_returns_all_users_with_a_derived_role(
    request,
    client: testclient.TestClient,
    add_membership_fixtures: list[str],
    mock_token_user_id,
    user_id_str,
    project_id_str,
    second_api_user,
    api_user,
):
    """
    Given a user has a direct role in a project
    Or the user belongs to a group or an organisation
    And that group or organisation has a role in that project
    When I request all members of that project
    Then the user is returned too
    """
    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER

    for setup_fixture in add_membership_fixtures:
        request.getfixturevalue(setup_fixture)

    response = client.get(
        f"/projects/{project_id_str}/members",
        headers=headers,
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["totalRecords"] == 2  # one owner, plus second user

    owner_user_member_entry = {
        "projectId": project_id_str,
        "role": database.project.ProjectRole.OWNER.value,
        "user": api_user.dict(),
        "addedAt": testing.time.DEFAULT_CREATED_AT.isoformat(),
        "updatedAt": None,
    }

    second_user_member_entry = {
        "projectId": project_id_str,
        "role": database.project.ProjectRole.REPORTER.value,
        "user": second_api_user.dict(),
        "addedAt": testing.time.DEFAULT_CREATED_AT.isoformat(),
        "updatedAt": None,
    }

    assert owner_user_member_entry in result["members"]
    assert second_user_member_entry in result["members"]


@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_members_user_id_get_logged_in(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    user_id_str,
    api_user,
):
    role = database.project.ProjectRole.OWNER
    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER
    response = client.get(
        f"/projects/{project_id_str}/members/{user_id_str}",
        headers=headers,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    result = response.json()
    assert result["role"] == role.value
    assert result["user"] == api_user.dict()


@pytest.mark.parametrize(
    ("headers", "expected_status_code"),
    (
        ({}, status.HTTP_200_OK),
        ({"Authorization": ""}, status.HTTP_401_UNAUTHORIZED),
        (
            {"Authorization": "InvalidAuth test-invalid-token"},
            status.HTTP_401_UNAUTHORIZED,
        ),
        (
            testing.token_verifier.INVALID_AUTHORIZATION_HEADER,
            status.HTTP_401_UNAUTHORIZED,
        ),
    ),
)
@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_members_user_id_get_logged_out(
    client: testclient.TestClient,
    project_id_str,
    user_id_str,
    headers,
    expected_status_code,
):
    response = client.get(
        f"/projects/{project_id_str}/members/{user_id_str}",
        headers=headers,
    )
    assert response.status_code == expected_status_code, response.text

    if expected_status_code == status.HTTP_200_OK:
        result = response.json()
        assert result["role"] == database.project.ProjectRole.OWNER.value


@pytest.mark.parametrize(
    "new_role",
    [
        database.project.ProjectRole.OWNER,
        database.project.ProjectRole.MAINTAINER,
        database.project.ProjectRole.REPORTER,
    ],
)
@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_second_project_member_as_reporter")
def test_projects_project_id_members_user_id_put(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    second_user_id_str,
    new_role: database.project.ProjectRole,
):
    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER
    project_member = {
        "role": new_role.name,
    }
    response = client.put(
        f"/projects/{project_id_str}/members/{second_user_id_str}",
        headers=headers,
        json=project_member,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(
        f"/projects/{project_id_str}/members/{second_user_id_str}",
        headers=headers,
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["role"] == new_role.value


@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_second_project_member_as_reporter")
def test_projects_project_id_members_delete(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    second_user_id_str,
):
    """
    Given a user has a direct REPORTER role in a project
    When that role gets deleted
    Then the user's role no longer exists
    """
    response = client.delete(
        f"/projects/{project_id_str}/members/{second_user_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(
        f"/projects/{project_id_str}/members/{second_user_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND, response.text

    # Check user still exists
    response = client.get(
        f"/users/{second_user_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text


def test_projects_project_id_members_delete_owner(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    user_id_str,
    add_example_project,
):
    """
    Check that owner cannot be deleted
    """
    response = client.delete(
        f"/projects/{project_id_str}/members/{user_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, response.text
    result = response.json()["detail"]
    assert result == "User owner can not be deleted from project"


def test_projects_project_id_members_delete_user_from_organization(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    second_user_id_str,
    add_example_project,
    add_second_organization_project_role_as_reporter,
):
    """
    Given a user in an Organization that has a REPORTER role in a project
    When that user tries to get deleted
    Then the user is not deleted from the project.
    """
    response = client.delete(
        f"/projects/{project_id_str}/members/{second_user_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, response.text
    result = response.json()["detail"]
    assert result == (
        "Members is added implicitly as a group/organization member, "
        "either remove it from this group/organization, "
        "or remove the entire group/organization from the project"
    )


@pytest.mark.parametrize(
    "setup_fixtures,expected_role",
    [
        (
            [],
            "NOT-FOUND-ERROR",
        ),
        (
            ["add_second_project_member_as_reporter"],
            database.project.ProjectRole.REPORTER,
        ),
        (
            ["add_second_user_group_project_role_as_reporter"],
            database.project.ProjectRole.REPORTER,
        ),
        (
            ["add_second_organization_project_role_as_reporter"],
            database.project.ProjectRole.REPORTER,
        ),
        (
            [
                "add_second_project_member_as_maintainer",
                "add_second_user_group_project_role_as_reporter",
                "add_second_organization_project_role_as_reporter",
            ],
            database.project.ProjectRole.MAINTAINER,
        ),
        (
            [
                "add_second_project_member_as_reporter",
                "add_second_user_group_project_role_as_maintainer",
                "add_second_organization_project_role_as_reporter",
            ],
            database.project.ProjectRole.MAINTAINER,
        ),
        (
            [
                "add_second_project_member_as_reporter",
                "add_second_user_group_project_role_as_reporter",
                "add_second_organization_project_role_as_maintainer",
            ],
            database.project.ProjectRole.MAINTAINER,
        ),
    ],
)
@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_second_example_user")
def test_user_project_role_derives_from_group_and_organisation_relationships(
    request,
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    second_user_id_str,
    second_api_user,
    setup_fixtures: list[str],
    expected_role: database.project.ProjectRole,
):
    """
    Given user has mix of direct and indirect roles in a project
    through groups and organizations with roles in the project
    When the user's role in project is requested
    Then their highest role (derived from Group, and Organization Roles) is returned
    """
    for setup_fixture in setup_fixtures:
        request.getfixturevalue(setup_fixture)

    headers = testing.token_verifier.VALID_AUTHORIZATION_HEADER
    response = client.get(
        f"/projects/{project_id_str}/members/{second_user_id_str}",
        headers=headers,
    )
    if expected_role == "NOT-FOUND-ERROR":
        assert response.status_code == status.HTTP_404_NOT_FOUND, response.text
        return

    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["role"] == expected_role.value
