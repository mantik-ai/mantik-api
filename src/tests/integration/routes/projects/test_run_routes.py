import os
import uuid
import zipfile

import boto3
import fastapi.testclient as testclient
import firecrest as pyfirecrest
import moto
import pytest
import pyunicore.client
import requests_mock
import starlette.status as status

import mantik_api.compute_backend as compute_backend
import mantik_api.compute_backend.firecrest as firecrest
import mantik_api.compute_backend.unicore as unicore
import mantik_api.database as database
import mantik_api.models as models
import mantik_api.routes.mlflow.tracking_server_router as tracking_server_router
import mantik_api.testing as testing
import mantik_api.utils.mlflow.runs as _runs

HEADERS = testing.token_verifier.VALID_AUTHORIZATION_HEADER


@pytest.fixture()
def add_database_run_with_unicore_job_id_sleep_2(
    database_client,
    add_example_project,
    second_database_user,
    database_project,
    database_run_with_mlflow_run,
    create_unicore_job_sleep_2: unicore.job.Job,
) -> unicore.job.Job:
    database_run_with_mlflow_run.remote_system_job_id = create_unicore_job_sleep_2.id
    database_run_with_mlflow_run.status = database.run.RunStatus.SCHEDULED
    with database_client.session():
        # Add second database user to project that is not the run owner.
        database_client.add(second_database_user)

    with database_client.session():
        database_client.add(
            database.project.UserProjectAssociationTable(
                role=database.project.ProjectRole.MAINTAINER,
                user_id=second_database_user.id,
                project_id=database_project.id,
            )
        )
        database_client.add(database_run_with_mlflow_run)
    return create_unicore_job_sleep_2


@pytest.fixture()
def add_database_run_with_unicore_job_id_sleep_20(
    database_client,
    add_example_project,
    second_database_user,
    database_project,
    database_run_with_mlflow_run,
    create_unicore_job_sleep,
) -> unicore.job.Job:
    job = create_unicore_job_sleep(seconds=20)
    database_run_with_mlflow_run.remote_system_job_id = job.id
    database_run_with_mlflow_run.status = database.run.RunStatus.SCHEDULED
    with database_client.session():
        # Add second database user to project that is not the run owner.
        database_client.add(second_database_user)

    with database_client.session():
        database_client.add(
            database.project.UserProjectAssociationTable(
                role=database.project.ProjectRole.MAINTAINER,
                user_id=second_database_user.id,
                project_id=database_project.id,
            )
        )
        database_client.add(database_run_with_mlflow_run)
    return job


@pytest.fixture()
def add_database_run_with_unicore_job_id(
    database_client,
    add_example_project,
    second_database_user,
    database_project,
    database_run_with_mlflow_run,
    create_unicore_job_echo: unicore.job.Job,
) -> unicore.job.Job:
    database_run_with_mlflow_run.remote_system_job_id = create_unicore_job_echo.id
    database_run_with_mlflow_run.status = database.run.RunStatus.SCHEDULED
    with database_client.session():
        # Add second database user to project that is not the run owner.
        database_client.add(second_database_user)

    with database_client.session():
        database_client.add(
            database.project.UserProjectAssociationTable(
                role=database.project.ProjectRole.MAINTAINER,
                user_id=second_database_user.id,
                project_id=database_project.id,
            )
        )
        database_client.add(database_run_with_mlflow_run)
    # Wait until job is successful
    create_unicore_job_echo._job.poll(state=pyunicore.client.JobStatus.SUCCESSFUL)
    return create_unicore_job_echo


@pytest.fixture()
def add_database_run_with_firecrest_job_id(
    database_client,
    add_example_project,
    second_database_user,
    database_project,
    database_run_firecrest_with_mlflow_run,
    create_firecrest_job: firecrest.job.Job,
) -> firecrest.job.Job:
    database_run_firecrest_with_mlflow_run.remote_system_job_id = (
        create_firecrest_job.id
    )
    database_run_firecrest_with_mlflow_run.status = database.run.RunStatus.FINISHED
    with database_client.session():
        # Add second database user to project that is not the run owner.
        database_client.add(second_database_user)

    with database_client.session():
        database_client.add(
            database.project.UserProjectAssociationTable(
                role=database.project.ProjectRole.MAINTAINER,
                user_id=second_database_user.id,
                project_id=database_project.id,
            )
        )
        database_client.add(database_run_firecrest_with_mlflow_run)
    return create_firecrest_job


@pytest.fixture()
def add_database_run_with_deleted_unicore_job_id(
    database_client,
    add_example_project,
    database_run_with_mlflow_run,
) -> uuid.UUID:
    unicore_job_id = uuid.uuid4()
    database_run_with_mlflow_run.remote_system_job_id = unicore_job_id
    # Must set status to RUNNING to force interaction with UNICORE.
    database_run_with_mlflow_run.status = database.run.RunStatus.RUNNING
    with database_client.session():
        database_client.add(database_run_with_mlflow_run)
    return unicore_job_id


@pytest.mark.parametrize(("startindex", "length"), [(0, 1), (30, 0)])
@pytest.mark.usefixtures("add_example_run")
def test_projects_project_id_runs_get_logged_in(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    startindex,
    length,
    mock_hvac,
):
    """Test case for projects_project_id_runs_get

    Returns all runs for a given project
    """
    params = [("startindex", startindex), ("pagelength", 50)]
    response = client.get(
        f"/projects/{project_id_str}/runs",
        headers=HEADERS,
        params=params,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["totalRecords"] == 1
    assert result["pageRecords"] == length
    assert result["userRole"] == 5

    for run in result["runs"]:
        assert run["userRole"] == 5


@pytest.mark.parametrize(
    ("headers", "expected_status_code"),
    (
        ({}, status.HTTP_200_OK),
        ({"Authorization": ""}, status.HTTP_401_UNAUTHORIZED),
        (
            {"Authorization": "InvalidAuth test-invalid-token"},
            status.HTTP_401_UNAUTHORIZED,
        ),
        (
            testing.token_verifier.INVALID_AUTHORIZATION_HEADER,
            status.HTTP_401_UNAUTHORIZED,
        ),
    ),
)
@pytest.mark.usefixtures("add_example_run")
def test_projects_project_id_runs_get_logged_out(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    headers,
    expected_status_code,
    mock_hvac,
):
    """Test case for projects_project_id_runs_get

    Returns all runs for a given project
    """
    response = client.get(
        f"/projects/{project_id_str}/runs",
        headers=headers,
    )

    assert response.status_code == expected_status_code, response.text
    if expected_status_code == status.HTTP_200_OK:
        result = response.json()
        assert result["totalRecords"] == 1
        assert result["userRole"] == -1

        for run in result["runs"]:
            assert run["userRole"] == -1


@pytest.mark.usefixtures("mock_tracking_server_url")
@pytest.mark.usefixtures("add_example_run")
def test_runs_can_be_created_with_multiple_model_and_data_combinations(
    client: testclient.TestClient,
    mock_token_user_id,
    run_name,
    user_id_str,
    project_id_str,
    database_project,
    backend_config,
    entry_point,
    mlflow_parameters,
    experiment_repository_id_str,
    code_repository_id_str,
    connection_id_str,
    data_repository_id_str,
    second_code_repository_id_str,
    second_experiment_repository_id_str,
    second_data_repository_id_str,
    compute_budget_account,
    mock_compute_backend_api_integration_testing,
    mock_hvac,
):
    """Test case for projects_project_id_runs_post

    Creates a new run
    """

    add_run1 = {
        "name": run_name,
        "dataRepositoryId": data_repository_id_str,
        "experimentRepositoryId": experiment_repository_id_str,
        "connectionId": connection_id_str,
        "codeRepositoryId": code_repository_id_str,
        "branch": "main",
        "backendConfig": backend_config,
        "computeBudgetAccount": compute_budget_account,
        "mlflowMlprojectFilePath": "should-be-unzipped/MLproject",
        "entryPoint": entry_point,
        "mlflowParameters": mlflow_parameters,
    }
    add_run2 = {
        "name": run_name,
        "dataRepositoryId": second_data_repository_id_str,
        "experimentRepositoryId": second_experiment_repository_id_str,
        "connectionId": connection_id_str,
        "codeRepositoryId": second_code_repository_id_str,
        "branch": "main",
        "backendConfig": backend_config,
        "computeBudgetAccount": compute_budget_account,
        "mlflowMlprojectFilePath": "should-be-unzipped/MLproject",
        "entryPoint": entry_point,
        "mlflowParameters": mlflow_parameters,
    }
    add_run3 = {
        "name": run_name,
        "dataRepositoryId": second_data_repository_id_str,
        "experimentRepositoryId": experiment_repository_id_str,
        "connectionId": connection_id_str,
        "codeRepositoryId": second_code_repository_id_str,
        "branch": "main",
        "backendConfig": backend_config,
        "computeBudgetAccount": compute_budget_account,
        "mlflowMlprojectFilePath": "should-be-unzipped/MLproject",
        "entryPoint": entry_point,
        "mlflowParameters": mlflow_parameters,
    }

    response = client.post(
        f"/projects/{project_id_str}/runs",
        headers=HEADERS,
        json=add_run1,
    )
    assert response.status_code == status.HTTP_201_CREATED, response.text

    response = client.post(
        f"/projects/{project_id_str}/runs",
        headers=HEADERS,
        json=add_run2,
    )
    assert response.status_code == status.HTTP_201_CREATED, response.text

    response = client.post(
        f"/projects/{project_id_str}/runs",
        headers=HEADERS,
        json=add_run3,
    )
    assert response.status_code == status.HTTP_201_CREATED, response.text

    response = client.get(
        f"/projects/{project_id_str}/runs",
        headers=HEADERS,
    )

    # the first run is added in the add_example_run fixture
    assert response.json()["totalRecords"] == 4

    assert response.status_code == status.HTTP_200_OK, response.text


@pytest.mark.usefixtures("mock_tracking_server_url")
def test_projects_project_id_runs_post(
    client: testclient.TestClient,
    add_example_project,
    mock_compute_backend_api_integration_testing,
    mock_token_user_id,
    project_id_str,
    api_add_run_configuration,
    mock_hvac,
) -> None:
    response = client.post(
        f"/projects/{project_id_str}/runs",
        headers=HEADERS,
        json=api_add_run_configuration.dict(),
    )

    # Check for correct response
    assert response.status_code == status.HTTP_201_CREATED, response.text

    result = response.json()
    run_id = result["runId"]

    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id}",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["status"] is not None


@pytest.mark.usefixtures("mock_tracking_server_url")
def test_projects_project_id_runs_post_with_minimal_components(
    client: testclient.TestClient,
    add_example_project,
    mock_compute_backend_api_integration_testing,
    mock_token_user_id,
    project_id_str,
    api_add_minimal_run_configuration,
    mock_hvac,
) -> None:
    response = client.post(
        f"/projects/{project_id_str}/runs",
        headers=HEADERS,
        json=api_add_minimal_run_configuration.dict(),
        params={"submit": False},
    )

    # Check for correct response
    assert response.status_code == status.HTTP_201_CREATED, response.text

    result = response.json()
    run_id = result["runId"]

    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id}",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    assert response.json()["runId"] == run_id


@pytest.mark.usefixtures("mock_tracking_server_url")
def test_projects_project_id_runs_post_prefers_commit_hash(
    client: testclient.TestClient,
    add_example_project,
    mock_compute_backend_api_integration_testing,
    mock_hvac,
    mock_token_user_id,
    project_id_str,
    api_add_run_configuration,
) -> None:
    api_add_run_configuration.branch = "main"
    api_add_run_configuration.commit = "commit-hash"

    response = client.post(
        f"/projects/{project_id_str}/runs",
        headers=HEADERS,
        json=api_add_run_configuration.dict(),
    )

    # Check for correct response
    assert response.status_code == status.HTTP_201_CREATED, response.text

    run_id = response.json()["runId"]

    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id}",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()

    assert result["modelRepository"]["branch"] is None
    assert result["modelRepository"]["commit"] == api_add_run_configuration.commit


def test_projects_project_id_runs_post_bitbucket_repository_fails(
    client: testclient.TestClient,
    add_example_project,
    mock_compute_backend_api_integration_testing,
    mock_hvac,
    mock_token_user_id,
    project_id_str,
    api_add_run_configuration_bitbucket_repo,
) -> None:
    api_add_run_configuration_bitbucket_repo.branch = "main"
    api_add_run_configuration_bitbucket_repo.commit = None

    response = client.post(
        f"/projects/{project_id_str}/runs",
        headers=HEADERS,
        json=api_add_run_configuration_bitbucket_repo.dict(),
    )
    expected = "For Bitbucket Code Repositories, please provide a commit hash"

    # Check for correct response
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, response.text

    result = response.json()["detail"]

    assert result == expected


def test_projects_project_id_runs_post_fails(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    project_id_str,
    api_add_run_configuration,
    mock_hvac,
) -> None:
    api_add_run_configuration.branch = None
    expected = "No branch name or commit hash given"

    response = client.post(
        f"/projects/{project_id_str}/runs",
        headers=HEADERS,
        json=api_add_run_configuration.dict(convert_keys_to_camel_case=True),
    )

    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, response.text

    [result] = response.json()["detail"]

    assert result["msg"] == expected


@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_example_connection_second_user")
def test_projects_project_id_runs_post_when_connection_not_owned(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    api_add_run_configuration,
    api_connection_second_user,
) -> None:
    api_add_run_configuration.connection_id = api_connection_second_user.connection_id
    response = client.post(
        f"/projects/{project_id_str}/runs",
        headers=HEADERS,
        json=api_add_run_configuration.dict(),
    )

    # Check for correct response
    assert response.status_code == status.HTTP_403_FORBIDDEN, response.text


def test_create_and_get_new_run_configuration(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    project_id_str,
    api_add_run_configuration,
    api_run_configuration,
    api_connection,
    api_user,
) -> None:
    # create new run configuration
    response = client.post(
        f"/projects/{project_id_str}/run-configurations",
        headers=HEADERS,
        json=api_add_run_configuration.dict(convert_keys_to_camel_case=True),
    )

    # Check for correct response
    assert response.status_code == status.HTTP_201_CREATED, response.text
    new_run_configuration_id = response.json()["runConfigurationId"]

    # get all run configs and check that the new one is saved correctly

    response = client.get(
        f"/projects/{project_id_str}/run-configurations/{new_run_configuration_id}",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_200_OK

    result = response.json()
    expected = api_run_configuration.dict() | {"userRole": 5, "updatedAt": None}
    expected["runConfigurationId"] = new_run_configuration_id
    expected["codeRepository"]["userRole"] = 5
    expected["experimentRepository"]["userRole"] = 5
    expected["connection"] = api_connection.dict() | {
        "loginName": None,
        "password": None,
        "token": None,
    }

    assert testing.time.has_created_at(result)
    # Remove ``createdAt`` from ``expected`` since
    # ``testing.time.has_created_at`` also removed ``createdAt``
    # from ``result``
    expected.pop("createdAt")
    assert result == expected


def test_projects_project_id_run_configurations_post_fails(
    client: testclient.TestClient,
    add_example_project,
    mock_token_user_id,
    project_id_str,
    api_add_run_configuration,
    api_run_configuration,
    api_connection,
    api_user,
) -> None:
    api_add_run_configuration.branch = None
    expected = "No branch name or commit hash given"

    response = client.post(
        f"/projects/{project_id_str}/run-configurations",
        headers=HEADERS,
        json=api_add_run_configuration.dict(convert_keys_to_camel_case=True),
    )

    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, response.text

    [result] = response.json()["detail"]

    assert result["msg"] == expected


@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_run_configuration")
def test_get_run_configurations(
    client: testclient.TestClient,
    mock_token_user_id,
    run_configuration_id_str,
    project_id_str,
    api_run_configuration,
) -> None:
    response = client.get(
        f"/projects/{project_id_str}/run-configurations", headers=HEADERS
    )

    assert response.status_code == status.HTTP_200_OK

    result = response.json()

    assert result["totalRecords"] == 1
    assert result["pageRecords"] == 1
    assert result["userRole"] == 5

    for run_configuration in result["runConfigurations"]:
        assert run_configuration["userRole"] == 5


@pytest.mark.usefixtures("mock_tracking_server_url")
@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_run_configuration")
@pytest.mark.usefixtures("mock_compute_backend_api_integration_testing")
def test_trigger_run_from_run_configurations(
    client: testclient.TestClient,
    mock_token_user_id,
    run_configuration_id_str,
    project_id_str,
    mock_hvac,
) -> None:
    response = client.post(
        f"/projects/{project_id_str}/run-configurations/{run_configuration_id_str}/trigger",  # noqa: E501
        headers=HEADERS,
    )
    assert response.status_code == status.HTTP_201_CREATED, response.text

    result = response.json()
    run_id = result["runId"]

    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id}",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["status"] is not None


@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_run_configuration")
def test_delete_run_configuration(
    client: testclient.TestClient,
    mock_token_user_id,
    run_configuration_id_str,
    project_id_str,
    mock_hvac,
) -> None:
    response = client.delete(
        f"/projects/{project_id_str}/run-configurations/{run_configuration_id_str}",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(
        f"/projects/{project_id_str}/run-configurations", headers=HEADERS
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()

    assert result["totalRecords"] == 0
    assert result["pageRecords"] == 0


def test_projects_project_id_runs_delete(
    client: testclient.TestClient,
    add_example_run_with_mlflow_run,
    mock_token_user_id,
    project_id_str,
    run_id_str,
):
    response = client.delete(
        f"/projects/{project_id_str}/runs/{run_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Check if deletion succeeded
    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND, response.text


def test_projects_project_id_runs_delete_when_active(
    client: testclient.TestClient,
    mock_hvac,
    mock_token_user_id,
    unicore_client,
    project_id_str,
    run_id_str,
    add_database_run_with_unicore_job_id_sleep_20,
):
    response = client.delete(
        f"/projects/{project_id_str}/runs/{run_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Wait until job is cancelled
    add_database_run_with_unicore_job_id_sleep_20._job.poll(
        state=pyunicore.client.JobStatus.FAILED
    )

    assert (
        add_database_run_with_unicore_job_id_sleep_20.get_status()
        == models.run_info.UnicoreStatus.FAILED
    )

    # Check if deletion succeeded
    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND, response.text


@pytest.mark.usefixtures("mock_tracking_server_url")
@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_remote_environment_to_project")
@pytest.mark.usefixtures("mock_compute_backend_api_integration_testing")
def test_submit_run_with_environment(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    api_add_run_configuration,
    remote_environment_id_str: str,
    tmp_path,
    api_remote_environment,
    mock_hvac,
) -> None:
    # create run configuration (with env id)
    api_add_run_configuration.environment_id = uuid.UUID(
        remote_environment_id_str
    )  # re-use same config but differnet data repository

    run_configuration_response = client.post(
        f"/projects/{project_id_str}/run-configurations",
        headers=HEADERS,
        json=api_add_run_configuration.dict(convert_keys_to_camel_case=True),
    )

    assert (
        run_configuration_response.status_code == status.HTTP_201_CREATED
    ), run_configuration_response.text
    new_run_configuration_id = run_configuration_response.json()["runConfigurationId"]

    # trigger run from run config
    triggered_run_response = client.post(
        f"/projects/{project_id_str}/run-configurations/{new_run_configuration_id}/trigger",  # noqa: E501
        headers=HEADERS,
    )
    assert (
        triggered_run_response.status_code == status.HTTP_201_CREATED
    ), triggered_run_response.text
    triggered_run_id = triggered_run_response.json()["runId"]

    # fetch the run (assert env is correctly displayed)
    response = client.get(
        f"/projects/{project_id_str}/runs/{triggered_run_id}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["environmentId"] == remote_environment_id_str
    assert response.json()["backendConfig"]["Environment"] == {
        "Apptainer": {
            "Path": "hpc://much-power/very-fast/image.sif",
            "Type": "remote",
        },
        "Variables": {"ENV_VAR_1": "VALUE_1"},
    }


def test_projects_project_id_runs_run_id_cancel_put(
    client: testclient.TestClient,
    mock_hvac,
    mock_token_user_id,
    unicore_client,
    add_database_run_with_unicore_job_id_sleep_2,
    run_id_str,
    project_id_str,
):
    response = client.put(
        f"/projects/{project_id_str}/runs/{run_id_str}/cancel",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Wait until job is cancelled
    add_database_run_with_unicore_job_id_sleep_2._job.poll(
        state=pyunicore.client.JobStatus.FAILED
    )

    assert (
        add_database_run_with_unicore_job_id_sleep_2.get_status()
        == models.run_info.UnicoreStatus.FAILED
    )

    # Check that status was set to KILLED
    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["status"] == "KILLED"


def test_projects_project_id_runs_run_id_logs_get(
    client: testclient.TestClient,
    mock_hvac,
    mock_token_user_id,
    unicore_client,
    add_database_run_with_unicore_job_id,
    run_id_str,
    project_id_str,
):
    expected = ["UNICORE API LOGS", "APPLICATION LOGS"]

    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id_str}/logs",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert any(any(exp in line for exp in expected) for line in result)


def test_projects_project_id_runs_run_id_status_get(
    client: testclient.TestClient,
    mock_hvac,
    mock_token_user_id,
    unicore_client,
    add_database_run_with_unicore_job_id,
    run_id_str,
    project_id_str,
    caplog,
):
    expected = "FINISHED"

    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id_str}/status",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result == expected

    # Check if UNICORE API was called to update the run status
    unicore_called = "Run status mutable, fetching run status from remote system"
    assert testing.logs.message_in_caplog(unicore_called, caplog=caplog)

    # Call another time to check that status is not fetched from UNICORE again
    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id_str}/status",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result == expected

    unicore_not_called = "Run is not active, reading run status from database"
    assert testing.logs.message_in_caplog(unicore_not_called, caplog=caplog)


def test_projects_project_id_runs_run_id_status_and_logs_put_remote_run(
    client: testclient.TestClient,
    mock_hvac,
    mock_token_user_id,
    unicore_client,
    add_database_run_with_unicore_job_id,
    run_id_str,
    project_id_str,
):
    new_status = "FINISHED"
    new_logs = "New logs \n Test"

    response = client.put(
        f"/projects/{project_id_str}/runs/{run_id_str}/status",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=new_status,
    )

    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
    assert response.json()["detail"] == "Cannot manually modify status of remote job"

    response = client.put(
        f"/projects/{project_id_str}/runs/{run_id_str}/logs",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=new_logs,
    )

    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
    assert response.json()["detail"] == "Cannot manually modify logs of remote job"


def test_projects_project_id_runs_run_id_status_and_logs_put_second_user(
    client: testclient.TestClient,
    mock_hvac,
    mock_token_second_user_id,
    unicore_client,
    add_database_run_with_unicore_job_id,
    run_id_str,
    project_id_str,
):
    new_status = "FINISHED"
    new_logs = "New logs \n Test"

    response = client.put(
        f"/projects/{project_id_str}/runs/{run_id_str}/status",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=new_status,
    )

    assert response.status_code == status.HTTP_403_FORBIDDEN
    assert response.json()["detail"] == "Only run owner can change the run status"

    response = client.put(
        f"/projects/{project_id_str}/runs/{run_id_str}/logs",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=new_logs,
    )

    assert response.status_code == status.HTTP_403_FORBIDDEN
    assert response.json()["detail"] == "Only run owner can change the run logs"


def test_projects_project_id_runs_run_id_status_put_local_run(
    client: testclient.TestClient,
    mock_token_user_id,
    add_database_local_run,
    run_id_str,
    project_id_str,
):
    new_status = "FINISHED"

    response = client.put(
        f"/projects/{project_id_str}/runs/{run_id_str}/status",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=new_status,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id_str}/status",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    result = response.json()
    assert result == new_status

    new_status = "KILLED"

    response = client.put(
        f"/projects/{project_id_str}/runs/{run_id_str}/status",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=new_status,
    )

    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, response.text

    result = response.json()
    assert result["detail"] == (
        "A run status can only be modified if it the latest known status was "
        "SCHEDULED or RUNNING"
    )


def test_projects_project_id_runs_run_id_logs_put_and_get_local_run(
    client: testclient.TestClient,
    mock_token_user_id,
    add_database_local_run,
    run_id_str,
    project_id_str,
):
    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id_str}/logs",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    result = response.json()
    assert result == [
        "Run is still active, wait for the run to end to see the logs here"
    ]

    new_status = "KILLED"
    client.put(
        f"/projects/{project_id_str}/runs/{run_id_str}/status",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=new_status,
    )

    new_logs = "New logs \n Test"

    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id_str}/logs",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    result = response.json()
    assert result == ["Missing logs from local run, logs uploaded unsuccessfully"]

    response = client.put(
        f"/projects/{project_id_str}/runs/{run_id_str}/logs",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=new_logs,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id_str}/logs",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    result = response.json()
    assert result == new_logs.split("\n")

    response = client.put(
        f"/projects/{project_id_str}/runs/{run_id_str}/logs",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json="Should not update",
    )

    result = response.json()
    assert result["detail"] == "The run logs can only be modified for an active run"


def test_projects_project_id_runs_run_id_info_get_unicore(
    client: testclient.TestClient,
    mock_hvac,
    mock_token_user_id,
    unicore_client,
    add_database_run_with_unicore_job_id,
    run_id_str,
    project_id_str,
    caplog,
):
    expected = add_database_run_with_unicore_job_id.get_info().to_json()

    # Execute a first request that writes info to the database
    response1 = client.get(
        f"/projects/{project_id_str}/runs/{run_id_str}/info",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response1.status_code == status.HTTP_200_OK, response1.text

    result1 = response1.json()
    assert result1["queue"] == expected["queue"]
    assert result1["siteName"] == expected["siteName"]

    assert testing.logs.message_in_caplog(
        "Fetching run info from remote system", caplog
    ), "Info not fetched from connection"

    # Execute a second request that fetches info from the database
    response2 = client.get(
        f"/projects/{project_id_str}/runs/{run_id_str}/info",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response2.status_code == status.HTTP_200_OK, response1.text

    result2 = response2.json()
    assert result2["queue"] == expected["queue"]
    assert result2["siteName"] == expected["siteName"]

    assert testing.logs.message_in_caplog(
        "Fetching run info from database", caplog
    ), "Info not fetched from database"


def test_projects_project_id_runs_run_id_info_get_unicore_with_error(
    client: testclient.TestClient,
    add_example_run_wrong_backend_config,
    mock_hvac,
    mock_token_user_id,
    run_id_str,
    project_id_str,
    unicore_api_url,
):
    response1 = client.get(
        f"/projects/{project_id_str}/runs/{run_id_str}/info",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response1.status_code == status.HTTP_503_SERVICE_UNAVAILABLE, response1.text


def test_projects_project_id_runs_run_id_status_get_unicore_with_error(
    client: testclient.TestClient,
    add_example_run_wrong_backend_config,
    mock_hvac,
    mock_token_user_id,
    run_id_str,
    project_id_str,
    unicore_api_url,
):
    response1 = client.get(
        f"/projects/{project_id_str}/runs/{run_id_str}/status",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response1.status_code == status.HTTP_503_SERVICE_UNAVAILABLE, response1.text


def test_projects_project_id_runs_run_id_info_get_firecrest(
    client: testclient.TestClient,
    monkeypatch,
    mock_hvac,
    mock_token_user_id,
    add_database_run_with_firecrest_job_id,
    firecrest_run_id_str,
    project_id_str,
    caplog,
):
    expected = add_database_run_with_firecrest_job_id.get_info().to_json()
    monkeypatch.setattr(
        pyfirecrest,
        "Firecrest",
        testing.firecrest.FakeClient,
    )
    monkeypatch.setattr(
        pyfirecrest,
        "ClientCredentialsAuth",
        testing.firecrest.FakeClientCredentialsAuth,
    )
    # Execute a first request that writes info to the database
    response1 = client.get(
        f"/projects/{project_id_str}/runs/{firecrest_run_id_str}/info",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response1.status_code == status.HTTP_200_OK, response1.text

    result1 = response1.json()
    assert result1["partition"] == expected["partition"]
    assert result1["nodelist"] == expected["nodelist"]

    assert testing.logs.message_in_caplog(
        "Fetching run info from remote system", caplog
    ), "Info not fetched from connection"

    # Execute a second request that fetches info from the database
    response2 = client.get(
        f"/projects/{project_id_str}/runs/{firecrest_run_id_str}/info",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response2.status_code == status.HTTP_200_OK, response1.text

    result2 = response2.json()
    assert result2["partition"] == expected["partition"]
    assert result2["nodelist"] == expected["nodelist"]

    assert testing.logs.message_in_caplog(
        "Fetching run info from database", caplog
    ), "Info not fetched from database"


@pytest.mark.parametrize(
    ("params", "expected", "expected_headers"),
    [
        (
            {"path": "mantik.log"},
            "hello\n",
            {
                "content-disposition": "attachment;filename=mantik.log",
                "content-type": "application/octet-stream",
            },
        ),
    ],
)
def test_projects_project_id_runs_run_id_download_get_file(
    client: testclient.TestClient,
    mock_hvac,
    mock_token_user_id,
    unicore_client,
    add_database_run_with_unicore_job_id,
    run_id_str,
    project_id_str,
    params,
    expected,
    expected_headers,
):
    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id_str}/download",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params=params,
        stream=True,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.headers == expected_headers

    result = testing.stream.stream_response_chunks_to_string(response)
    assert result == expected


@pytest.mark.parametrize("params", [None, {"path": None}, {"path": "/"}])
def test_projects_project_id_runs_run_id_download_get_entire_run_folder(
    client: testclient.TestClient,
    mock_hvac,
    mock_token_user_id,
    unicore_client,
    add_database_run_with_unicore_job_id,
    run_id_str,
    project_id_str,
    params,
):
    expected_archive_name = add_database_run_with_unicore_job_id.id
    expected_logs = b"hello\n"
    expected_txt = b"test\n"
    expected_files = [
        f"{expected_archive_name}/UNICORE_SCRIPT_EXIT_CODE",
        f"{expected_archive_name}/sub/folder/test.txt",
        f"{expected_archive_name}/mantik.log",
    ]
    expected_headers = {
        "content-disposition": f"attachment;filename={expected_archive_name}.zip",
        "content-type": "application/x-zip-compressed",
    }

    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id_str}/download",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params=params,
        stream=True,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.headers == expected_headers

    result = testing.stream.stream_response_chunks_to_bytest_io(response)

    with zipfile.ZipFile(result) as zip_file:
        result_files = zip_file.namelist()
        with zip_file.open(
            f"{expected_archive_name}/{compute_backend.job.APPLICATION_LOGS_FILE}"
        ) as log_file:
            result_logs = log_file.read()
        with zip_file.open(f"{expected_archive_name}/sub/folder/test.txt") as txt_file:
            result_txt_file = txt_file.read()

    assert all(file in result_files for file in expected_files)
    assert result_logs == expected_logs
    assert result_txt_file == expected_txt


@pytest.mark.parametrize("path", ["sub/folder", "sub%2Ffolder"])
def test_projects_project_id_runs_run_id_download_get_sub_folder(
    client: testclient.TestClient,
    mock_hvac,
    mock_token_user_id,
    unicore_client,
    add_database_run_with_unicore_job_id,
    run_id_str,
    project_id_str,
    path,
):
    params = {"path": path}
    expected_archive_name = "folder"
    expected_txt = b"test\n"
    expected_files = [
        f"{expected_archive_name}/test.txt",
    ]
    expected_headers = {
        "content-disposition": f"attachment;filename={expected_archive_name}.zip",
        "content-type": "application/x-zip-compressed",
    }

    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id_str}/download",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params=params,
        stream=True,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.headers == expected_headers

    result = testing.stream.stream_response_chunks_to_bytest_io(response)

    with zipfile.ZipFile(result) as zip_file:
        result_files = zip_file.namelist()
        with zip_file.open(f"{expected_archive_name}/test.txt") as txt_file:
            result_txt_file = txt_file.read()

    assert all(file in result_files for file in expected_files)
    assert result_txt_file == expected_txt


@pytest.mark.parametrize(
    ("method", "endpoint", "expected"),
    [
        ("PUT", "cancel", "Only run owner can cancel the run"),
        ("GET", "logs", "Only run owner can fetch the run logs"),
        (
            "GET",
            "status",
            "Run still in a mutable state: only the run owner can fetch the run status",
        ),
        ("GET", "info", "Only run owner can fetch the run information"),
        ("GET", "download", "Only run owner can access the run's working directory"),
    ],
)
def test_projects_project_id_runs_run_id_request_fails_when_not_run_owner(
    client: testclient.TestClient,
    mock_hvac,
    mock_token_second_user_id,
    unicore_client,
    add_database_run_with_unicore_job_id,
    run_id_str,
    project_id_str,
    method,
    endpoint,
    expected,
):
    response = client.request(
        method,
        f"/projects/{project_id_str}/runs/{run_id_str}/{endpoint}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_403_FORBIDDEN, response.text

    result = response.json()
    assert result["detail"] == expected


@pytest.mark.parametrize(
    ("method", "endpoint"),
    [
        ("PUT", "cancel"),
        ("GET", "logs"),
        ("GET", "status"),
        ("GET", "info"),
        ("GET", "download"),
    ],
)
def test_projects_project_id_runs_run_id_request_fails_for_deleted_unicore_job(
    client: testclient.TestClient,
    mock_hvac,
    mock_token_user_id,
    unicore_client,
    add_database_run_with_deleted_unicore_job_id,
    run_id_str,
    project_id_str,
    method,
    endpoint,
):
    expected = (
        f"No job found on the remote system for run with ID {run_id_str}: "
        f"UNICORE Job with ID {add_database_run_with_deleted_unicore_job_id} "
        "not found. The job and it's working directory might have been "
        "deleted. Some systems frequently delete unused directories."
    )
    response = client.request(
        method,
        f"/projects/{project_id_str}/runs/{run_id_str}/{endpoint}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND, response.text

    result = response.json()
    assert result["detail"] == expected


@pytest.fixture()
def add_run_for_bookkeeping(
    experiment_repository_id_str,
    code_repository_id_str,
    trained_model_id_str,
):
    return models.run.AddRun(
        name="Local Run",
        experiment_repository_id=experiment_repository_id_str,
        code_repository_id=code_repository_id_str,
        branch="test-branch",
        mlflow_run_id=uuid.uuid4(),
        mlflow_mlproject_file_path="path/to/file",
        entry_point="something.py",
        backend_config={},
    )


@pytest.mark.usefixtures("add_example_project")
def test_add_run_without_submitting(
    client: testclient.TestClient,
    project_id_str: str,
    mock_token_user_id,
    add_run_for_bookkeeping: models.run.AddRun,
) -> None:
    """
    When I try store run details
    And I do not want to trigger a run
    Then they get stored
    And I can retrieve them
    """

    local_run_payload = add_run_for_bookkeeping.dict()
    response = client.post(
        f"/projects/{project_id_str}/runs",
        params={"submit": False},
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=local_run_payload,
    )
    assert response.status_code == status.HTTP_201_CREATED, response.text
    result = response.json()
    run_id = result["runId"]

    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    result = response.json()

    assert (
        result["experimentRepository"]["experimentRepositoryId"]
        == local_run_payload["experimentRepositoryId"]
    )
    assert result["name"] == local_run_payload["name"]
    assert (
        result["modelRepository"]["codeRepository"]["codeRepositoryId"]
        == local_run_payload["codeRepositoryId"]
    )
    assert result["mlflowRunId"] == local_run_payload["mlflowRunId"]
    assert (
        result["mlflowMlprojectFilePath"]
        == local_run_payload["mlflowMlprojectFilePath"]
    )
    assert result["entryPoint"] == local_run_payload["entryPoint"]
    assert result["backendConfig"] == local_run_payload["backendConfig"]


def test_change_run_name(
    client: testclient.TestClient,
    mock_token_user_id,
    add_example_project,
    mock_hvac,
    add_example_run_with_mlflow_run,
    run_id_str,
    project_id_str,
    fake_token,
) -> None:
    new_run_name = "Completely New Run"

    response = client.put(
        f"/projects/{project_id_str}/runs/{run_id_str}/name",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=new_run_name,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    mantik_response = client.get(
        f"/projects/{project_id_str}/runs/{run_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    result = mantik_response.json()
    assert result["name"] == new_run_name

    # Assert the change of the run name in mlflow too
    mlflow_run_id = result["mlflowRunId"]
    mlflow_response = _runs.get_run(mlflow_run_id, fake_token)

    assert mlflow_response["run"]["info"]["run_name"] == new_run_name


@pytest.fixture
def mock_s3_artifact_store(
    api_experiment_repository,
) -> str:
    bucket_name = "TEST"
    with moto.mock_s3():
        boto3.client("s3").create_bucket(
            Bucket=bucket_name,
            CreateBucketConfiguration={"LocationConstraint": "eu-central-1"},
        )
        os.environ[_runs.ARTIFACT_BUCKET_NAME_ENV_VAR] = bucket_name

        yield bucket_name

        os.unsetenv(_runs.ARTIFACT_BUCKET_NAME_ENV_VAR)


@pytest.fixture
def mock_s3_and_add_sample_run_artifacts(
    mock_s3_artifact_store: str,
    run_id_str: str,
    sample_text_file_path: str,
    api_experiment_repository,
    add_example_project,
    database_run_with_mlflow_run,
    add_example_run_with_mlflow_run,
) -> None:
    mlflow_experiment_id = api_experiment_repository.mlflow_experiment_id
    bucket_name = mock_s3_artifact_store
    mlflow_run_id = database_run_with_mlflow_run.mlflow_run_id

    boto3.client("s3").upload_file(
        sample_text_file_path,
        bucket_name,
        f"mlartifacts/{mlflow_experiment_id}/{mlflow_run_id.hex}/artifacts/model/empty-1.txt",  # noqa
    )

    yield


@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("mock_s3_and_add_sample_run_artifacts")
def test_fetch_run_artifacts_succeeds(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    database_run_with_mlflow_run,
):
    """
    Given I have a run artifacts in the artifact store
    When  I request its download url
    Then  I get a url back
    And   that url refers to a zipped folder
    """
    response = client.get(
        f"/projects/{project_id_str}/runs/{str(database_run_with_mlflow_run.id)}/artifacts",  # noqa
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json().get("url") is not None


@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("mock_s3_artifact_store")
def test_fetch_run_artifacts_fails_when_no_artifacts_exist(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    run_id_str,
    add_example_run_with_mlflow_run,
):
    """
    Given I don't have run artifacts in the artifact store
    When  I request its download url
    Then  I get an error
    """

    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id_str}/artifacts",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND, response.text
    assert response.json() == {"detail": "No artifacts exist for selected run."}


def test_get_runs_with_status_running_but_finished_in_mlflow_is_now_failed(
    client: testclient.TestClient,
    project_id_str,
    add_running_example_run,
    set_mlflow_tracking_url,
):
    with requests_mock.Mocker(real_http=True) as m:
        m.get(
            f"{os.environ[tracking_server_router.TRACKING_SERVER_URL_INTERNAL_ENV_VAR]}/api/2.0/mlflow/runs/get",  # noqa E501
            json={"run": {"info": {"status": "FINISHED"}}},
        )
        params = [("startindex", 0), ("pagelength", 50)]
        response = client.get(
            f"/projects/{project_id_str}/runs",
            headers=HEADERS,
            params=params,
        )

        assert response.status_code == status.HTTP_200_OK, response.text

        result = response.json()

        for run in result["runs"]:
            assert run["status"] == "FAILED"


def test_get_run_with_status_running_but_finished_in_mlflow_is_now_failed(
    client: testclient.TestClient,
    project_id_str,
    add_running_example_run,
    run_id_str,
    set_mlflow_tracking_url,
):
    with requests_mock.Mocker(real_http=True) as m:
        m.get(
            f"{os.environ[tracking_server_router.TRACKING_SERVER_URL_INTERNAL_ENV_VAR]}/api/2.0/mlflow/runs/get",  # noqa E501
            json={"run": {"info": {"status": "FINISHED"}}},
        )
        params = [("startindex", 0), ("pagelength", 50)]
        response = client.get(
            f"/projects/{project_id_str}/runs/{run_id_str}",
            headers=HEADERS,
            params=params,
        )
        assert response.status_code == status.HTTP_200_OK, response.text
        result = response.json()
        assert result["status"] == "FAILED"

    # Second time does not query Mlflow cause the value is updated in the database
    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id_str}",
        headers=HEADERS,
        params=params,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    result = response.json()
    assert result["status"] == "FAILED"


@pytest.mark.usefixtures("mock_tracking_server_url")
def test_projects_project_id_runs_post_with_data_repository_version(
    client: testclient.TestClient,
    add_example_project,
    mock_compute_backend_api_integration_testing,
    mock_token_user_id,
    project_id_str,
    api_add_run_configuration,
    mock_hvac,
) -> None:
    """
    Given I submit a run via a run configuration
    And it contains the branch and commit of a data repository
    When I fetch the run
    Then I get the information about the data repository branch and commit.
    """

    response = client.post(
        f"/projects/{project_id_str}/runs",
        headers=HEADERS,
        json=api_add_run_configuration.dict(),
    )

    # Check for correct response
    assert response.status_code == status.HTTP_201_CREATED, response.text

    result = response.json()
    run_id = result["runId"]

    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id}",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["dataBranch"] == api_add_run_configuration.data_branch
    assert result["dataCommit"] == api_add_run_configuration.data_commit
    assert result["dataTargetDir"] == api_add_run_configuration.data_target_dir


@pytest.fixture
def api_run_infrastructure():
    return models.run.RunInfrastructure(
        os="Ubuntu 20.04",
        cpu_cores=8,
        gpu_count=1,
        gpu_info=[
            models.run.GPUInfo(
                name="NVIDIA Tesla T4", id="0", driver="535.104.0", total_memory="15360"
            )
        ],
        hostname="example-host",
        memory_gb="32GB",
        platform="linux",
        processor="Intel(R) Xeon(R) CPU E5-2678 v3 @ 2.50GHz",
        python_version="3.8.10",
        python_executable="/usr/bin/python3",
    )


def test_add_run_infrastructure_to_existing_run(
    client: testclient.TestClient,
    mock_token_user_id,
    unicore_client,
    add_database_run_with_unicore_job_id,
    run_id_str,
    project_id_str,
    api_run_infrastructure,
):
    """
    Given I have run
    When I store the infrastructure
    Then
        - it appears in the run details
        − I can retrieve it through the run infrastructure endpoint
    """

    # prove that no initially there's no infrastructure info
    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == 200, response.text
    assert response.json()["infrastructure"] is None

    # Store infrastructure info
    response = client.put(
        f"/projects/{project_id_str}/runs/{run_id_str}/infrastructure",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=api_run_infrastructure.dict(),
    )
    assert response.status_code == 204, response.text

    # retrieve run details
    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == 200, response.text
    assert response.json()["infrastructure"] == api_run_infrastructure.dict()

    # retrieve infrastructure from dedicated endpoint
    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id_str}/infrastructure",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == 200, response.text
    assert response.json()["infrastructure"] == api_run_infrastructure.dict()


@pytest.fixture
def api_run_notebook_source():
    return models.run.NoteBookSource(
        location="/work/dev/project",
        version="0b21327f6e8cd7226e4c1b55e0610659146c3a32",
        provider=models.run.ProviderType.JUPYTER,
    )


def test_add_run_notebook_source_to_existing_run(
    client: testclient.TestClient,
    mock_token_user_id,
    unicore_client,
    add_database_run_with_unicore_job_id,
    run_id_str,
    project_id_str,
    api_run_notebook_source,
):
    """
    Given I have run
    When I store the notebook source information
    Then it appears in the run details
    And I can retrieve itb through an API endpoint
    """

    # prove that no initially there's no notebook source info
    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == 200, response.text
    assert response.json()["notebookSource"] is None

    # Store infrastructure info
    response = client.put(
        f"/projects/{project_id_str}/runs/{run_id_str}/notebook-source",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=api_run_notebook_source.dict(),
    )
    assert response.status_code == 204, response.text

    # retrieve run details
    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == 200, response.text
    assert (
        response.json()["notebookSource"] == api_run_notebook_source.dict()
    ), response.json()

    # retrieve infrastructure from dedicated endpoint
    response = client.get(
        f"/projects/{project_id_str}/runs/{run_id_str}/notebook-source",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == 200, response.text
    assert response.json()["notebookSource"] == api_run_notebook_source.dict()
