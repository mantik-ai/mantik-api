import fastapi.testclient as testclient
import pytest
import starlette.status as status

import mantik_api.database as database
import mantik_api.testing as testing
import mantik_api.tokens.jwt as jwt


@pytest.mark.usefixtures("add_example_user")
@pytest.mark.usefixtures("mock_token_user_id")
def test_users_user_id_email_put(
    client_with_moto_token_validation: testclient.TestClient,
    user_id_str,
    database_client,
    create_and_confirm_fake_cognito_user,
):
    _update_user_email_and_assert_unchanged(
        client_with_moto_token_validation=client_with_moto_token_validation,
        user_id_str=user_id_str,
        database_client=database_client,
        create_and_confirm_fake_cognito_user=create_and_confirm_fake_cognito_user,
        new_email="test-new-email",
    )


def _update_user_email_and_assert_unchanged(
    client_with_moto_token_validation: testclient.TestClient,
    user_id_str,
    database_client,
    create_and_confirm_fake_cognito_user,
    new_email: str,
) -> tuple[jwt.JWT, str]:
    """Update the user's email and assert it has _not_ been changed yet.

    moto cannot mimic our expected behavior:
    Cognito, as configured by us, keeps the original email until the new one
    has been verified. Hence, we _must_ keep the old one in the database here.

    """
    token, _ = testing.aws.cognito.get_access_token_and_credentials(
        username=create_and_confirm_fake_cognito_user["username"],
        password=create_and_confirm_fake_cognito_user["password"],
    )
    with database_client.session():
        email_before_update = database_client.get_one(
            orm_model_type=database.user.User,
            constraints={database.user.User.id: user_id_str},
        ).email

    response = client_with_moto_token_validation.put(
        f"/users/{user_id_str}/email",
        headers=testing.token_verifier.create_moto_jwt_authorization_header(token),
        json={"new_email": new_email},
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    with database_client.session():
        email_after_update = database_client.get_one(
            orm_model_type=database.user.User,
            constraints={database.user.User.id: user_id_str},
        ).email

    assert email_after_update != new_email and email_before_update == email_after_update

    return token, email_after_update


@pytest.mark.usefixtures("add_example_user")
@pytest.mark.usefixtures("mock_token_user_id")
def test_users_user_id_email_verify_update_post(
    client_with_moto_token_validation: testclient.TestClient,
    user_id_str,
    database_client,
    create_and_confirm_fake_cognito_user,
    cognito_patcher,
):
    expected_email = "new-test-email"
    confirmation_code = "123456"
    token, email_after_update = _update_user_email_and_assert_unchanged(
        client_with_moto_token_validation=client_with_moto_token_validation,
        user_id_str=user_id_str,
        database_client=database_client,
        create_and_confirm_fake_cognito_user=create_and_confirm_fake_cognito_user,
        new_email=expected_email,
    )

    cognito_patcher.patch_verify_user_attribute(
        attribute_name="email",
        confirmation_code=confirmation_code,
        token=token.to_string(),
    )

    response = client_with_moto_token_validation.post(
        f"/users/{user_id_str}/email/verify-update",
        headers=testing.token_verifier.create_moto_jwt_authorization_header(token),
        json={"confirmationCode": confirmation_code},
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Check whether email has now been updated
    with database_client.session():
        email_after_verification = database_client.get_one(
            orm_model_type=database.user.User,
            constraints={database.user.User.id: user_id_str},
        ).email
        assert (
            email_after_verification == expected_email
            and email_after_update != email_after_verification
        )


@pytest.mark.usefixtures("add_example_user")
@pytest.mark.usefixtures("mock_token_user_id")
def test_users_user_id_email_verify_update_post_fails(
    client_with_moto_token_validation: testclient.TestClient,
    user_id_str,
    database_client,
    create_and_confirm_fake_cognito_user,
    cognito_patcher,
):
    confirmation_code = "123456"

    token, email_after_update = _update_user_email_and_assert_unchanged(
        client_with_moto_token_validation=client_with_moto_token_validation,
        user_id_str=user_id_str,
        database_client=database_client,
        create_and_confirm_fake_cognito_user=create_and_confirm_fake_cognito_user,
        new_email="test-new-email",
    )

    expected = "Invalid confirmation code"

    cognito_patcher.patch_verify_user_attribute(
        attribute_name="email",
        confirmation_code=confirmation_code,
        token=token.to_string(),
        error_code="CodeMismatchException",
        error_message="Cognito error message",
    )

    response = client_with_moto_token_validation.post(
        f"/users/{user_id_str}/email/verify-update",
        headers=testing.token_verifier.create_moto_jwt_authorization_header(token),
        json={"confirmationCode": confirmation_code},
    )
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, response.text

    result = response.json()["detail"]
    assert result == expected

    # Check that email hasn't been updated
    with database_client.session():
        email_after_verification = database_client.get_one(
            orm_model_type=database.user.User,
            constraints={database.user.User.id: user_id_str},
        ).email
        assert email_after_verification == email_after_update
