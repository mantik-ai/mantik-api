import starlette.status as status

import mantik_api.testing as testing


def test_users_details_get(client, api_user, add_example_user, mock_token_user_id):
    expected = api_user.dict()

    response = client.get(
        "/user-details",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()

    assert result == expected


def test_users_details_get_logged_out(
    client, api_user, add_example_user, mock_token_user_id
):
    response = client.get(
        "/user-details",
    )

    assert response.status_code == status.HTTP_401_UNAUTHORIZED, response.text
