import datetime
import itertools
import uuid

import fastapi.testclient as testclient
import pytest
import starlette.status as status

import mantik_api.testing as testing


@pytest.mark.parametrize(
    ("startindex", "expected"),
    [
        (
            0,
            {
                "invitations": [
                    {
                        "invitationId": "1989f1db-7489-4afc-93b6-5f369c204229",
                        "invitedToType": "GROUP",
                        "invitedType": "USER",
                        "invitedId": "6641ab6b-750c-4cb6-ac15-4699601300c1",
                        "invitedToId": "7641ab6b-750c-4cb6-ac15-4699601300c1",
                        "role": None,
                        "createdAt": (
                            testing.time.DEFAULT_CREATED_AT
                            + datetime.timedelta(hours=5)
                        ).isoformat(),
                        "updatedAt": None,
                        "invitedName": "test-name",
                        "invitedToName": "test-name",
                        "inviterName": "test-name-2",
                    },
                    {
                        "invitationId": "1989f1db-7489-4afc-93b6-5f369c204228",
                        "invitedToType": "ORGANIZATION",
                        "invitedType": "GROUP",
                        "invitedId": "7641ab6b-750c-4cb6-ac15-4699601300c1",
                        "invitedToId": "1989f1db-7489-4afc-93c5-5f369c204224",
                        "role": None,
                        "createdAt": (
                            testing.time.DEFAULT_CREATED_AT
                            + datetime.timedelta(hours=4)
                        ).isoformat(),
                        "updatedAt": None,
                        "invitedName": "test-name",
                        "invitedToName": "foo",
                        "inviterName": "test-name-2",
                    },
                    {
                        "invitationId": "1989f1db-7489-4afc-93b6-5f369c204227",
                        "invitedToType": "ORGANIZATION",
                        "invitedType": "USER",
                        "invitedId": "6641ab6b-750c-4cb6-ac15-4699601300c1",
                        "invitedToId": "1989f1db-7489-4afc-93c5-5f369c204224",
                        "role": None,
                        "createdAt": (
                            testing.time.DEFAULT_CREATED_AT
                            + datetime.timedelta(hours=3)
                        ).isoformat(),
                        "updatedAt": None,
                        "invitedName": "test-name",
                        "invitedToName": "foo",
                        "inviterName": "test-name-2",
                    },
                    {
                        "invitationId": "1989f1db-7489-4afc-93b6-5f369c204226",
                        "invitedToType": "PROJECT",
                        "invitedType": "ORGANIZATION",
                        "invitedId": "1989f1db-7489-4afc-93c5-5f369c204224",
                        "invitedToId": "e931c97e-917b-499c-b7b1-fcb52ae96f91",
                        "role": "RESEARCHER",
                        "createdAt": (
                            testing.time.DEFAULT_CREATED_AT
                            + datetime.timedelta(hours=2)
                        ).isoformat(),
                        "updatedAt": None,
                        "invitedName": "foo",
                        "invitedToName": "test_project_name",
                        "inviterName": "test-name-2",
                    },
                    {
                        "invitationId": "1989f1db-7489-4afc-93b6-5f369c204225",
                        "invitedToType": "PROJECT",
                        "invitedType": "GROUP",
                        "invitedId": "7641ab6b-750c-4cb6-ac15-4699601300c1",
                        "invitedToId": "e931c97e-917b-499c-b7b1-fcb52ae96f91",
                        "role": "RESEARCHER",
                        "createdAt": (
                            testing.time.DEFAULT_CREATED_AT
                            + datetime.timedelta(hours=1)
                        ).isoformat(),
                        "updatedAt": None,
                        "invitedName": "test-name",
                        "invitedToName": "test_project_name",
                        "inviterName": "test-name-2",
                    },
                    {
                        "invitationId": "1989f1db-7489-4afc-93b6-5f369c204224",
                        "invitedToType": "PROJECT",
                        "invitedType": "USER",
                        "invitedId": "6641ab6b-750c-4cb6-ac15-4699601300c1",
                        "invitedToId": "e931c97e-917b-499c-b7b1-fcb52ae96f91",
                        "role": "RESEARCHER",
                        "createdAt": testing.time.DEFAULT_CREATED_AT.isoformat(),
                        "updatedAt": None,
                        "invitedName": "test-name",
                        "invitedToName": "test_project_name",
                        "inviterName": "test-name-2",
                    },
                ],
                "totalRecords": 6,
                "pageRecords": 6,
            },
        ),
        (30, {"invitations": [], "totalRecords": 6, "pageRecords": 0}),
    ],
)
@pytest.mark.usefixtures("add_all_example_invitations")
def test_invitations_get(
    client: testclient.TestClient,
    mock_token_user_id,
    startindex,
    expected,
):
    """Test case for invitations_get

    Get list of invitations
    """
    params = [("startindex", startindex), ("pagelength", 50)]
    response = client.get(
        "/invitations",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params=params,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()

    assert result["totalRecords"] == expected["totalRecords"]
    assert result["pageRecords"] == expected["pageRecords"]

    for res, exp in zip(result["invitations"], expected["invitations"], strict=True):
        assert res == exp


@pytest.mark.usefixtures("add_invitation_of_second_user_to_project")
def test_invitations_get_do_not_show_other_users_invitations(
    client: testclient.TestClient,
    mock_token_user_id,
):
    """
    Given I am the first user
    When I request invitations
    Then I can only see my invitations
    And not those of the second user
    """
    params = [("startindex", 0), ("pagelength", 50)]
    response = client.get(
        "/invitations",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params=params,
    )

    assert response.status_code == 200, response.text

    result = response.json()

    assert result == {"totalRecords": 0, "pageRecords": 0, "invitations": []}


@pytest.mark.usefixtures("add_example_invitation_user_to_project")
def test_invitations_invitation_id_delete(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_maintainer,
    invitation_user_to_project_id_str: str,
):
    """Test case for invitations_invitation_id_delete

    Delete invitation
    """

    response = client.delete(
        f"/invitations/{invitation_user_to_project_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(
        "/invitations",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params=[("startindex", 0), ("pagelength", 50)],
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    result = response.json()
    expected = {"invitations": [], "totalRecords": 0, "pageRecords": 0}
    assert result == expected


@pytest.mark.usefixtures("add_example_invitation_user_to_project")
def test_invitations_invitation_id_delete_as_random_user(
    client: testclient.TestClient,
    mock_impersonate_random_user,
    invitation_user_to_project_id_str: str,
):
    """Test case for invitations_invitation_id_delete

    Delete invitation
    """

    response = client.delete(
        f"/invitations/{invitation_user_to_project_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    response.status_code == status.HTTP_401_UNAUTHORIZED, response.text
    assert (
        response.json()["detail"] == "Only the inviting user can delete an invitation"
    )


@pytest.mark.parametrize("accept", [("true"), ("false")])
@pytest.mark.usefixtures("add_invitation_of_second_user_to_project")
def test_accept_decline_user_to_project_invitation(
    accept: str,
    client: testclient.TestClient,
    project_id_str,
    mock_token_second_user_id,
    second_user_id_str,
    invitation_second_user_to_project_id_str,
):
    """
    Given a user has invited me to their project
    And I accept/decline their invitation
    Then I do/do not become a project member
    And the invitation gets deleted
    """

    invitation_id = invitation_second_user_to_project_id_str
    update_invitation = {"accepted": accept}

    response = client.put(
        f"/invitations/{invitation_id}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=update_invitation,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(
        f"/projects/{project_id_str}/members/{second_user_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    if accept == "true":
        assert response.status_code == status.HTTP_200_OK, response.text
    else:
        assert response.status_code == status.HTTP_404_NOT_FOUND, response.text

    response = client.get(
        "/invitations",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["totalRecords"] == 0


@pytest.mark.parametrize("accept", [("true"), ("false")])
@pytest.mark.usefixtures("add_invitation_of_group_to_project")
def test_accept_decline_group_to_project_invitation(
    accept: str,
    client: testclient.TestClient,
    project_id_str,
    mock_token_user_id,
    user_group_id_str,
    invitation_group_to_project_id_str,
):
    """
    Given a group has been invited to a project
    And I as group owner accept/decline invitation
    Then the group gets a role in the project
    And the invitation gets deleted
    """

    invitation_id = invitation_group_to_project_id_str
    update_invitation = {"accepted": accept}

    response = client.put(
        f"/invitations/{invitation_id}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=update_invitation,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(
        f"/projects/{project_id_str}/groups/{user_group_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    if accept == "true":
        assert response.status_code == status.HTTP_200_OK, response.text
    else:
        assert response.status_code == status.HTTP_404_NOT_FOUND, response.text

    response = client.get(
        "/invitations",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["totalRecords"] == 0


@pytest.mark.parametrize("accept", [("true"), ("false")])
@pytest.mark.usefixtures("add_invitation_of_organization_to_project")
@pytest.mark.usefixtures("add_example_organization")
def test_accept_decline_organization_to_project_invitation(
    accept: str,
    client: testclient.TestClient,
    project_id_str,
    mock_token_user_id,
    user_group_id_str,
    invitation_organization_to_project_id_str,
    organization_id_str,
):
    """
    Given an organization has been invited to a project
    And I as organization owner accept/decline the invitation
    Then the organization gets a role in the project
    And the invitation gets deleted
    """

    invitation_id = invitation_organization_to_project_id_str
    update_invitation = {"accepted": accept}

    response = client.put(
        f"/invitations/{invitation_id}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=update_invitation,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(
        f"/projects/{project_id_str}/organizations/{organization_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    if accept == "true":
        assert response.status_code == status.HTTP_200_OK, response.text
    else:
        assert response.status_code == status.HTTP_404_NOT_FOUND, response.text

    response = client.get(
        "/invitations",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["totalRecords"] == 0


@pytest.mark.parametrize(
    ("accepted", "group_members"),
    [
        (
            "false",
            [],
        ),
        (
            "true",
            [
                {
                    "userId": "6641ab6b-750c-4cb6-ac15-4699601300c1",
                    "name": "test-name",
                    "createdAt": testing.time.DEFAULT_CREATED_AT.isoformat(),
                    "updatedAt": None,
                }
            ],
        ),
    ],
)
@pytest.mark.usefixtures("add_example_invitation_to_group")
def test_invitations_invitation_id_put_group(
    client: testclient.TestClient,
    user_group_id_str,
    accepted,
    group_members,
    mock_get_user_info_and_be_project_owner,
    invitation_user_to_group_id_str: str,
):
    """Test case for invitations_invitation_id_put

    Update invitation info
    """
    update_invitation = {"accepted": accepted}

    response = client.put(
        f"/invitations/{invitation_user_to_group_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=update_invitation,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(
        f"/groups/{user_group_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params=[("startindex", 0), ("pagelength", 50)],
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    result = response.json()["members"]
    assert result == group_members

    response = client.get(
        "/invitations",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params=[("startindex", 0), ("pagelength", 50)],
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    expected = {
        "invitations": [],
        "totalRecords": 0,
        "pageRecords": 0,
    }
    assert result == expected, "Still invitations in DB"


@pytest.mark.parametrize(
    ("accepted", "invitation_id", "organization_members"),
    [
        # Case Decline invitation
        (
            "false",
            "1989f1db-7489-4afc-93b6-5f369c204227",
            [
                {
                    "userId": "6641ab6b-750c-4cb6-ac15-4699601300c3",
                    "name": "test-name-3",
                    "createdAt": testing.time.DEFAULT_CREATED_AT.isoformat(),
                    "updatedAt": None,
                },
            ],
        ),
        # Case Accept invitation user to organization
        (
            "true",
            "1989f1db-7489-4afc-93b6-5f369c204227",
            [
                {
                    "userId": "6641ab6b-750c-4cb6-ac15-4699601300c1",
                    "name": "test-name",
                    "createdAt": testing.time.DEFAULT_CREATED_AT.isoformat(),
                    "updatedAt": None,
                },
                {
                    "userId": "6641ab6b-750c-4cb6-ac15-4699601300c3",
                    "name": "test-name-3",
                    "createdAt": testing.time.DEFAULT_CREATED_AT.isoformat(),
                    "updatedAt": None,
                },
            ],
        ),
        # Case Accept invitation group to organization
        (
            "true",
            "1989f1db-7489-4afc-93b6-5f369c204228",
            [
                {
                    "userId": "6641ab6b-750c-4cb6-ac15-4699601300c3",
                    "name": "test-name-3",
                    "createdAt": testing.time.DEFAULT_CREATED_AT.isoformat(),
                    "updatedAt": None,
                },
            ],
        ),
    ],
)
@pytest.mark.usefixtures("add_all_example_invitations")
def test_invitations_invitation_id_put_organization(
    client: testclient.TestClient,
    organization_id_str,
    accepted,
    invitation_id,
    organization_members,
    mock_get_user_info_and_be_project_owner,
):
    """Test case for invitations_invitation_id_put

    Update invitation info
    """
    update_invitation = {"accepted": accepted}

    response = client.put(
        f"/invitations/{invitation_id}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=update_invitation,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(
        f"/organizations/{organization_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params=[("startindex", 0), ("pagelength", 50)],
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    result = response.json()["members"]
    assert result == organization_members

    response = client.get(
        "/invitations",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params=[("startindex", 0), ("pagelength", 50)],
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["totalRecords"] == 5


@pytest.mark.usefixtures("add_example_users_project_group_organization")
def test_invitations_post_group(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    user_id_str,
    second_user_id_str,
    user_group_id_str,
):
    """Test case for invitations_post

    Add invitations
    """
    add_invitation = {
        "invitedToId": user_group_id_str,
        "invitedId": second_user_id_str,
        "invitedToType": "GROUP",
        "invitedType": "USER",
    }

    response = client.post(
        "/invitations",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=add_invitation,
    )

    assert response.status_code == status.HTTP_201_CREATED, response.text


@pytest.mark.usefixtures("add_example_users_project_group_organization")
def test_invitations_post_project(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    user_id_str,
    second_user_id_str,
    project_id_str,
):
    """Test case for invitations_post

    Add invitations
    """
    add_invitation = {
        "invitedToId": project_id_str,
        "invitedId": second_user_id_str,
        "invitedToType": "PROJECT",
        "invitedType": "USER",
    }

    response = client.post(
        "/invitations",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=add_invitation,
    )

    assert response.status_code == status.HTTP_201_CREATED


@pytest.mark.usefixtures("add_example_users_project_group_organization")
def test_invitations_post_organization(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    user_id_str,
    second_user_id_str,
    organization_id_str,
):
    """Test case for invitations_post

    Add invitations
    """
    add_invitation = {
        "invitedToId": organization_id_str,
        "invitedId": second_user_id_str,
        "invitedToType": "ORGANIZATION",
        "invitedType": "USER",
    }

    response = client.post(
        "/invitations",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=add_invitation,
    )

    assert response.status_code == status.HTTP_201_CREATED, response.text


@pytest.mark.usefixtures("add_example_users_project_group_organization")
def test_post_same_invitation_twice(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    user_id_str,
    second_user_id_str,
    organization_id_str,
):
    """Test case for invitations_post

    Add invitations
    """
    add_invitation = {
        "invitedToId": organization_id_str,
        "invitedId": second_user_id_str,
        "invitedToType": "ORGANIZATION",
        "invitedType": "USER",
    }

    response = client.post(
        "/invitations",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=add_invitation,
    )

    assert response.status_code == status.HTTP_201_CREATED, response.text

    response = client.post(
        "/invitations",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=add_invitation,
    )

    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
    assert response.json()["detail"] == "Value already present"


@pytest.mark.usefixtures("add_example_users_project_group_organization")
def test_invitations_post_and_get(
    client: testclient.TestClient,
    organization_id_str,
    second_user_id_str,
    user_id_str,
    mocker,
):
    mocker.patch(
        "mantik_api.tokens.jwt.JWT.user_id",
        return_value=uuid.UUID(user_id_str),
        new_callable=mocker.PropertyMock,
    )
    add_invitation = {
        "invitedToId": organization_id_str,
        "invitedId": second_user_id_str,
        "invitedToType": "ORGANIZATION",
        "invitedType": "USER",
    }

    response = client.post(
        "/invitations",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=add_invitation,
    )
    assert response.status_code == status.HTTP_201_CREATED, response.text

    mocker.patch(
        "mantik_api.tokens.jwt.JWT.user_id",
        return_value=uuid.UUID(second_user_id_str),
        new_callable=mocker.PropertyMock,
    )
    params = [("startindex", 0), ("pagelength", 50)]
    response = client.get(
        "/invitations",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params=params,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    assert (
        response.json()["invitations"][0] | add_invitation
        == response.json()["invitations"][0]
    )


@pytest.mark.parametrize(
    ("invited_to_type", "message"),
    [
        (
            "ORGANIZATION",
            "Entity already part of the organization",
        ),
        (
            "GROUP",
            "Entity already part of the group",
        ),
        (
            "PROJECT",
            "Entity already part of the project",
        ),
    ],
)
@pytest.mark.usefixtures("add_example_users_project_group_organization")
def test_invitations_post_already_present_user(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    user_id_str,
    organization_id_str,
    project_id_str,
    user_group_id_str,
    message,
    invited_to_type,
):
    invited_to_id = {
        "ORGANIZATION": organization_id_str,
        "GROUP": user_group_id_str,
        "PROJECT": project_id_str,
    }
    add_invitation = {
        "invitedToId": invited_to_id[invited_to_type],
        "invitedId": user_id_str,
        "invitedToType": invited_to_type,
        "invitedType": "USER",
    }
    response = client.post(
        "/invitations",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=add_invitation,
    )
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, response.text
    assert response.json()["detail"] == message


@pytest.mark.parametrize(
    ("invited_to_type", "invited_type", "message", "error"),
    [
        (invited_to_type, invited_type, message, error)
        for (invited_to_type, invited_type), (message, error) in list(
            zip(
                itertools.product(
                    ["GROUP", "ORGANIZATION", "PROJECT"],
                    ["USER", "GROUP", "ORGANIZATION"],
                ),
                (
                    ("Resource not found", status.HTTP_404_NOT_FOUND),
                    (
                        [
                            {
                                "loc": ["body", "invitedType"],
                                "msg": "Invited type is not valid for invited to type",
                                "type": "value_error",
                            }
                        ],
                        status.HTTP_422_UNPROCESSABLE_ENTITY,
                    ),
                    (
                        [
                            {
                                "loc": ["body", "invitedType"],
                                "msg": "Invited type is not valid for invited to type",
                                "type": "value_error",
                            }
                        ],
                        status.HTTP_422_UNPROCESSABLE_ENTITY,
                    ),
                    ("Resource not found", status.HTTP_404_NOT_FOUND),
                    ("Resource not found", status.HTTP_404_NOT_FOUND),
                    (
                        [
                            {
                                "loc": ["body", "invitedType"],
                                "msg": "Invited type is not valid for invited to type",
                                "type": "value_error",
                            }
                        ],
                        status.HTTP_422_UNPROCESSABLE_ENTITY,
                    ),
                    ("Resource not found", status.HTTP_404_NOT_FOUND),
                    ("Resource not found", status.HTTP_404_NOT_FOUND),
                    ("Resource not found", status.HTTP_404_NOT_FOUND),
                ),
            )
        )
    ],
)
@pytest.mark.usefixtures("add_example_users_project_group_organization")
def test_invitations_post_non_existing_resources(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    user_id_str,
    organization_id_str,
    project_id_str,
    user_group_id_str,
    invited_to_type,
    invited_type,
    error,
    message,
):
    invited_to_id = {
        "USER": user_id_str,
        "ORGANIZATION": organization_id_str,
        "GROUP": user_group_id_str,
        "PROJECT": project_id_str,
    }
    add_invitation = {
        "invitedToId": invited_to_id[invited_to_type],
        # Here we pass a random id to catch errors for non-existing resources
        "invitedId": "2d2ef169-2920-4f23-b9f5-cefa3ea5d55a",
        "invitedToType": invited_to_type,
        "invitedType": invited_type,
    }
    response = client.post(
        "/invitations",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=add_invitation,
    )
    assert response.status_code == error, response.text
    assert response.json()["detail"] == message
