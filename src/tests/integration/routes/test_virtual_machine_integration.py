import boto3
import fastapi.testclient as testclient
import moto
import pytest
import requests
import starlette.status as status

import mantik_api.service as service
import mantik_api.testing as testing

TEST_FILENAME = "vm_image_test_file.txt"
TEST_FILE_PATH = f"src/tests/integration/routes/{TEST_FILENAME}"
TEST_BUCKET_NAME = "test-bucket"
TEST_FILE_CONTENT = "hello world\n"


@pytest.fixture
def test_file_key_s3(user_id_str: str) -> str:
    return f"{user_id_str}/{TEST_FILENAME}"


@pytest.fixture
def test_file_url_s3(test_file_key_s3: str) -> str:
    return f"s3://{TEST_BUCKET_NAME}/{test_file_key_s3}"


def _add_vm_url(client: testclient.TestClient, url: str):
    response = client.post(
        "/virtual-machine/urls",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=url,
    )
    assert response.status_code == status.HTTP_201_CREATED, response.text
    return response


@pytest.mark.usefixtures("add_database_users")
def test_get_urls_when_empty(client: testclient.TestClient, mock_token_user_id) -> None:
    response = client.get(
        "/virtual-machine/urls",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json() == {"totalRecords": 0, "pageRecords": 0, "urls": []}


@pytest.mark.usefixtures("add_database_users")
def test_add_new_url(client: testclient.TestClient, mock_token_user_id) -> None:
    new_url = "www.new-url.com/image.vmdk"
    response = _add_vm_url(client, new_url)

    assert response.json() == {"url": new_url}

    response = client.get(
        "/virtual-machine/urls",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    expected = {"totalRecords": 1, "pageRecords": 1, "urls": [new_url]}

    assert result == expected


@pytest.fixture
def mock_s3():
    with moto.mock_s3():
        s3 = boto3.client("s3")
        s3.create_bucket(
            Bucket=TEST_BUCKET_NAME,
            CreateBucketConfiguration={"LocationConstraint": "eu-central-1"},
        )

        yield


@pytest.fixture
def mock_s3_and_add_sample_file(mock_s3, test_file_key_s3: str):
    s3 = boto3.client("s3")
    s3.upload_file(TEST_FILE_PATH, TEST_BUCKET_NAME, test_file_key_s3)
    yield


@pytest.mark.usefixtures("add_database_users")
def test_delete_url(
    client: testclient.TestClient, mock_token_user_id, user_id_str
) -> None:
    _add_vm_url(client, "www.new-url.com/image.vmdk")

    response = client.delete(
        "/virtual-machine/urls",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params={"url": "www.new-url.com/image.vmdk"},
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(
        "/virtual-machine/urls",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json() == {
        "totalRecords": 0,
        "pageRecords": 0,
        "urls": [],
    }


@pytest.mark.usefixtures("add_database_users")
def test_delete_non_existent_url_fails(
    client: testclient.TestClient, mock_token_user_id
) -> None:
    response = client.delete(
        "/virtual-machine/urls",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params={"url": "http://non-existent-url.com/something.vmdk"},
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND, response.text
    assert response.json() == {"detail": "Resource not found"}


@pytest.mark.usefixtures("add_database_users")
def test_add_vm_image_succeeds(
    client: testclient.TestClient,
    mock_token_user_id,
    user_id_str: str,
    mock_s3,
    test_file_url_s3: str,
    test_file_key_s3: str,
) -> None:
    """Add VM image through endpoint, image ends up in correct bucket with correct name"""  # noqa: E501
    # upload image
    response = client.post(
        "/virtual-machine/images",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params={"baseUrl": f"s3://{TEST_BUCKET_NAME}"},
        files={"file": open(TEST_FILE_PATH, "rb")},
    )
    assert response.status_code == status.HTTP_201_CREATED, response.text
    assert response.json() == {"url": test_file_url_s3}

    # Assert that file ended up in bucket
    s3 = boto3.client("s3")
    assert service.virtual_machine.file_exists_in_s3(
        file_key=test_file_key_s3, bucket_name=TEST_BUCKET_NAME, s3_client=s3
    ), "File not is s3"

    # Check whether url has been added to mantik db
    response = client.get(
        "/virtual-machine/urls",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    expected = {"totalRecords": 1, "pageRecords": 1, "urls": [test_file_url_s3]}

    assert result == expected


@pytest.mark.usefixtures("add_database_users")
def test_add_vm_image_to_non_existing_s3_bucket_fails(
    client: testclient.TestClient, mock_token_user_id, user_id_str: str, mock_s3
) -> None:
    response = client.post(
        "/virtual-machine/images",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params={"baseUrl": "s3://i-dont-exist"},
        files={"file": open(TEST_FILE_PATH, "rb")},
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.usefixtures("add_database_users")
def test_add_vm_image_to_unauthorized_url_fails(
    client: testclient.TestClient, mock_token_user_id
) -> None:
    """Add VM image with wrong URL fails with 403."""
    response = client.post(
        "/virtual-machine/images",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params={"baseUrl": "http://www.example.com/something"},
        files={"file": open(TEST_FILE_PATH, "rb")},
    )

    assert response.status_code == status.HTTP_403_FORBIDDEN, response.text
    assert (
        response.json()["detail"]
        == "Not authorized to upload to http://www.example.com/something"
    )


@pytest.mark.usefixtures("add_database_users")
def test_add_vm_image_without_valid_token_fails(
    client: testclient.TestClient, mock_token_user_id
) -> None:
    """Add VM image without correct token fails with 403."""
    response = client.post(
        "/virtual-machine/images",
        headers=testing.token_verifier.INVALID_AUTHORIZATION_HEADER,
        params={"baseUrl": "http://www.example.com/something"},
        files={"file": open(TEST_FILE_PATH, "rb")},
    )
    response.status_code == status.HTTP_401_UNAUTHORIZED
    assert response.json() == {"detail": "Invalid token"}


@pytest.mark.usefixtures("add_database_users")
def test_add_vm_image_file_already_exists_warning(
    client: testclient.TestClient, mock_token_user_id, user_id_str: str, mock_s3
) -> None:
    # upload image
    response = client.post(
        "/virtual-machine/images",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params={"baseUrl": f"s3://{TEST_BUCKET_NAME}"},
        files={"file": open(TEST_FILE_PATH, "rb")},
    )
    assert response.status_code == status.HTTP_201_CREATED

    # try upload again
    response = client.post(
        "/virtual-machine/images",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params={"baseUrl": f"s3://{TEST_BUCKET_NAME}"},
        files={"file": open(TEST_FILE_PATH, "rb")},
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json()["detail"] == "Filename already exists. Delete it first."


@pytest.mark.parametrize(
    "url",
    [
        f"s3://{TEST_BUCKET_NAME}/123/{TEST_FILENAME}",
        f"http://www.example.com/{TEST_FILENAME}",
    ],
    ids=["Another user's bucket", "Non s3 url"],
)
@pytest.mark.usefixtures("add_database_users")
def test_delete_virtual_machine_from_url_fails_for_unauthorized_url(
    url: str, client: testclient.TestClient, mock_token_user_id, user_id_str: str
):
    response = client.delete(
        "/virtual-machine/images",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params={"url": url},
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN, response.text
    assert response.json()["detail"] == f"Not authorized to delete {url}"


@pytest.mark.usefixtures("add_database_users")
def test_delete_virtual_machine_image(
    client: testclient.TestClient,
    mock_token_user_id,
    user_id_str: str,
    mock_s3_and_add_sample_file,
    test_file_key_s3: str,
    test_file_url_s3: str,
):
    _add_vm_url(client, test_file_url_s3)

    response = client.delete(
        "/virtual-machine/images",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params={"url": test_file_url_s3},
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    assert not service.virtual_machine.file_exists_in_s3(
        s3_client=boto3.client("s3"),
        bucket_name=TEST_BUCKET_NAME,
        file_key=test_file_key_s3,
    ), "File still exists in S3"

    # assert it's removed from url handling api
    response = client.get(
        "/virtual-machine/urls",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK
    assert test_file_url_s3 not in response.json()["urls"]


@pytest.mark.usefixtures("add_database_users")
def test_delete_non_existent_virtual_machine_image_fails_with_404(
    client: testclient.TestClient, mock_token_user_id, user_id_str: str, mock_s3
):
    NON_EXISTENT_FILE = (f"s3://{TEST_BUCKET_NAME}/{user_id_str}/i-dont-exist.txt",)
    response = client.delete(
        "/virtual-machine/images",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params={"url": NON_EXISTENT_FILE},
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert response.json()["detail"] == "Image not found therefore not deleted"


def test_delete_image_without_entry_in_url_api_should_still_succeed(
    client: testclient.TestClient,
    mock_token_user_id,
    user_id_str: str,
    mock_s3_and_add_sample_file,
    test_file_url_s3: str,
) -> None:
    response = client.delete(
        "/virtual-machine/images",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params={"url": test_file_url_s3},
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text


@pytest.mark.usefixtures("add_database_users")
def test_get_virtual_machine_image_of_another_user_fails(
    client: testclient.TestClient, mock_token_user_id, user_id_str: str
):
    OTHER_USER_FILE_URL = f"s3://{TEST_BUCKET_NAME}/123/{TEST_FILENAME}"

    response = client.get(
        "/virtual-machine/images",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params={"url": OTHER_USER_FILE_URL},
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN, response.text
    assert response.json()["detail"] == "Not authorized"


@pytest.mark.usefixtures("add_database_users")
def test_get_virtual_machine_image_of_non_s3_url_return_unauthorized_on_error(
    client: testclient.TestClient, mock_token_user_id, user_id_str: str, mocker
):
    mocker.patch("requests.get", side_effect=requests.exceptions.ConnectionError())

    response = client.get(
        "/virtual-machine/images",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params={"url": "http://www.something.com/fake.txt"},
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN, response.text
    assert response.json()["detail"] == "Not authorized"


@pytest.mark.usefixtures("add_database_users")
def test_get_virtual_machine_image_succeeds(
    client: testclient.TestClient,
    mock_token_user_id,
    user_id_str: str,
    mock_s3_and_add_sample_file,
    test_file_url_s3: str,
):
    response = client.get(
        "/virtual-machine/images",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params={"url": test_file_url_s3},
    )
    assert response.status_code == status.HTTP_200_OK
    assert response.text == TEST_FILE_CONTENT


@pytest.mark.usefixtures("add_database_users")
def test_get_missing_virtual_machine_image_fails_with_404(
    client: testclient.TestClient, mock_token_user_id, user_id_str: str, mock_s3
):
    MISSING_TEST_FILE_URL = f"s3://{TEST_BUCKET_NAME}/{user_id_str}/i-dont-exist.txt"

    response = client.get(
        "/virtual-machine/images",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params={"url": MISSING_TEST_FILE_URL},
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert response.json()["detail"] == "Resource not found"
