import datetime
import uuid

import fastapi.routing
import fastapi.testclient as testclient
import pytest
import starlette.status as status

import mantik_api.testing as testing


@pytest.mark.parametrize(
    "headers",
    (
        {},
        {"Authorization": ""},
        {"Authorization": "InvalidAuth test-invalid-token"},
        testing.token_verifier.INVALID_AUTHORIZATION_HEADER,
    ),
)
@pytest.mark.usefixtures("add_example_project")
def test_all_post_put_delete_routes_logged_out(
    client: testclient.TestClient, project_id_str: str, headers
):
    """Assure that all PUT, POST, DELETE endpoints reject logged-out users."""
    for route in client.app.routes:
        # Skip non project routes
        if not _is_projects_endpoint(route):
            continue

        # Only test for PUT, POST, DELETE routes
        for method in ["PUT", "POST", "DELETE"]:
            if not _endpoint_allows_method(method, route=route):
                continue

            response = client.request(
                method,
                _create_route_path(route, project_id=project_id_str),
                headers=headers,
            )

            assert response.status_code == status.HTTP_401_UNAUTHORIZED, response.text


def _is_projects_endpoint(route: fastapi.routing.APIRoute) -> bool:
    return "/projects/{projectId}" in route.path


def _endpoint_allows_method(method: str, route: fastapi.routing.APIRoute) -> bool:
    return method in route.methods


def _create_route_path(route: fastapi.routing.APIRoute, project_id: str) -> str:
    params = {
        "projectId": project_id,
    } | {
        param: uuid.uuid4() for param in route.param_convertors if param != "projectId"
    }
    return route.path.format(**params)


@pytest.mark.parametrize(
    ("headers", "expected_status_code"),
    (
        ({}, status.HTTP_200_OK),
        ({"Authorization": ""}, status.HTTP_401_UNAUTHORIZED),
        (
            {"Authorization": "InvalidAuth test-invalid-token"},
            status.HTTP_401_UNAUTHORIZED,
        ),
        (
            testing.token_verifier.INVALID_AUTHORIZATION_HEADER,
            status.HTTP_401_UNAUTHORIZED,
        ),
    ),
)
@pytest.mark.usefixtures("add_2_example_projects")
def test_projects_get_logged_out(
    client: testclient.TestClient, headers, expected_status_code
):
    """Test case for projects_get

    Returns only public projects
    """
    response = client.get(
        "/projects",
        headers=headers,
    )
    assert response.status_code == expected_status_code, response.text

    if expected_status_code == status.HTTP_200_OK:
        json_response = response.json()
        assert json_response["totalRecords"] == 1

        [project] = json_response["projects"]
        assert project["public"], "Project should be public"
        assert project["userRole"] == -1, "User role should be NO_ROLE"


@pytest.mark.usefixtures("add_2_example_projects")
@pytest.mark.usefixtures("add_example_user")
@pytest.mark.usefixtures("mock_token_user_id")
def test_projects_get_logged_in(client: testclient.TestClient):
    """Test case for projects_get

    Returns all private projects
    """
    response = client.get(
        "/projects",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["totalRecords"] == 2

    for project in result["projects"]:
        assert project["userRole"] == 5, "User role should be OWNER"


@pytest.mark.parametrize(
    ("order_by", "ascending", "expected"),
    [
        # Test case: order by created, private project is created last in the setup
        ("created", "false", ["project_private_id_str", "project_id_str"]),
        ("created", "true", ["project_id_str", "project_private_id_str"]),
        # Test case: order by updated, both projects haven't been updated
        ("updated", "false", ["project_id_str", "project_private_id_str"]),
        ("updated", "true", ["project_id_str", "project_private_id_str"]),
        # Test case: order by name, private project comes first alphabetically
        ("name", "true", ["project_private_id_str", "project_id_str"]),
        ("name", "false", ["project_id_str", "project_private_id_str"]),
    ],
)
@pytest.mark.usefixtures("add_2_example_projects")
@pytest.mark.usefixtures("add_example_user")
@pytest.mark.usefixtures("mock_token_user_id")
def test_projects_get_ordered(
    client: testclient.TestClient, request, order_by, ascending, expected
):
    expected = [request.getfixturevalue(project) for project in expected]

    response = client.get(
        "/projects",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params={
            "order_by": order_by,
            "ascending": ascending,
        },
    )
    assert response.status_code == 200, response.text

    result = [project["projectId"] for project in response.json()["projects"]]
    assert result == expected


@pytest.mark.parametrize("startindex", [56, 130])
@pytest.mark.usefixtures("add_100_example_projects")
@pytest.mark.usefixtures("add_example_user")
@pytest.mark.usefixtures("mock_token_user_id")
def test_projects_get_pagination(client: testclient.TestClient, startindex):
    """Test case for projects_get

    Returns all projects
    """
    params = [("startindex", startindex), ("pagelength", 50)]
    response = client.get(
        "/projects",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params=params,
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()

    assert result["totalRecords"] == 100
    assert result["pageRecords"] == max(100 - startindex, 0)

    for project in result["projects"]:
        assert project["userRole"] == -1, "User role should be NO_ROLE"


@pytest.mark.parametrize(
    ("headers", "labels", "expected", "expected_project_ids"),
    [
        # Test case: logged in, label 3 matches all projects
        (
            testing.token_verifier.VALID_AUTHORIZATION_HEADER,
            ["database_label_3"],
            3,
            [
                "project_with_labels_3_to_5_id_str",
                "project_with_labels_1_to_5_id_str",
                "project_with_labels_1_to_5_on_project_id_str",
            ],
        ),
        # Test case: logged in, label 3 matches all projects, but one is private
        (
            None,
            ["database_label_3"],
            2,
            [
                "project_with_labels_1_to_5_id_str",
                "project_with_labels_1_to_5_on_project_id_str",
            ],
        ),
    ],
)
@pytest.mark.usefixtures("add_example_project_with_labels_3_to_5")
@pytest.mark.usefixtures("add_example_project_with_labels_1_to_5")
@pytest.mark.usefixtures("add_example_project_with_labels_1_to_5_on_project")
@pytest.mark.usefixtures("mock_token_user_id")
def test_projects_get_filter_by_labels(
    request,
    client: testclient.TestClient,
    headers,
    labels,
    expected,
    expected_project_ids,
):
    """Test case for projects_get with labels filter.

    Notes
    -----
    - project_private
      - is private
      - contains labels 1 and 2
    - project_with_labels_1_to_5
      - is private
      - contains database_label 1 to 4

    """
    params = (
        {"labels": [request.getfixturevalue(label).id for label in labels]}
        if labels
        else None
    )
    expected_project_ids = (
        [request.getfixturevalue(project_id) for project_id in expected_project_ids]
        if expected_project_ids
        else None
    )

    response = client.get("/projects", headers=headers, params=params)
    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["totalRecords"] == expected

    result_project_ids = [project["projectId"] for project in result["projects"]]

    if expected_project_ids is not None:
        assert all(
            project_id in result_project_ids for project_id in expected_project_ids
        ), (
            f"Not all project IDs {expected_project_ids} in search query "
            f"result {result_project_ids}"
        )
    else:
        assert not result_project_ids


@pytest.mark.parametrize(
    ("headers", "project_id", "expected_status_code"),
    (
        # Test case: no header, public project
        ({}, "project_id_str", status.HTTP_200_OK),
        # Test case: invalid header, public project
        ({"Authorization": ""}, "project_id_str", status.HTTP_401_UNAUTHORIZED),
        # Test case: invalid header, public project
        (
            {"Authorization": "InvalidAuth test-invalid-token"},
            "project_id_str",
            status.HTTP_401_UNAUTHORIZED,
        ),
        # Test case: invalid header, public project
        (
            testing.token_verifier.INVALID_AUTHORIZATION_HEADER,
            "project_id_str",
            status.HTTP_401_UNAUTHORIZED,
        ),
        # Test case: no header, private project
        ({}, "project_private_id_str", status.HTTP_403_FORBIDDEN),
        # Test case: invalid header, private project
        ({"Authorization": ""}, "project_private_id_str", status.HTTP_401_UNAUTHORIZED),
        # Test case: invalid header, private project
        (
            {"Authorization": "InvalidAuth test-invalid-token"},
            "project_private_id_str",
            status.HTTP_401_UNAUTHORIZED,
        ),
        # Test case: invalid header, private project
        (
            testing.token_verifier.INVALID_AUTHORIZATION_HEADER,
            "project_private_id_str",
            status.HTTP_401_UNAUTHORIZED,
        ),
    ),
)
@pytest.mark.usefixtures("add_2_example_projects")
def test_projects_project_id_get_logged_out(
    request,
    client: testclient.TestClient,
    mock_token_user_id,
    headers,
    project_id,
    expected_status_code,
):
    """Test case for projects_project_id_get

    Returns the information on a specific project
    """
    project_id = request.getfixturevalue(project_id)

    response = client.get(
        f"/projects/{project_id}",
        headers=headers,
    )
    assert response.status_code == expected_status_code, response.text

    if expected_status_code == status.HTTP_200_OK:
        result = response.json()
        assert result["projectId"] == project_id
        assert result["userRole"] == -1


@pytest.mark.usefixtures("add_2_example_projects")
def test_projects_project_id_get_logged_in(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
):
    """Test case for projects_project_id_get

    Returns the information on a specific project
    """

    response = client.get(
        f"/projects/{project_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    result = response.json()
    assert result["projectId"] == project_id_str
    assert result["userRole"] == 5


def test_projects_project_id_get_private_project(
    client: testclient.TestClient,
    add_example_project_private,
    mock_token_second_user_id,
    project_private_id_str,
):
    response = client.get(
        f"/projects/{project_private_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_403_FORBIDDEN, response.text


@pytest.mark.usefixtures("add_example_project")
def test_projects_project_id_put(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    api_project,
    api_label_1,
    api_label_2,
    created_at,
):
    # Example project has 2 labels, so delete 1 label while updating
    updated_project_json = {
        "detailedDescription": "updated=description",
        "name": "updated_name",
        "labels": [str(api_label_2.label_id)],
    }

    expected = (
        api_project.dict()
        | updated_project_json
        | {"labels": [api_label_2.dict()], "userRole": -1}
    )

    response = client.put(
        f"/projects/{project_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=updated_project_json,
    )

    updated_project_response = client.get(f"/projects/{project_id_str}")

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    result = updated_project_response.json()
    assert testing.time.has_updated_at(result)
    # Remove ``updatedAt`` field from expected since it is also removed
    # from ``result`` by ``testing.time.has_updated_at``.
    expected.pop("updatedAt")
    assert result == expected


@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_example_labels")
def test_projects_project_id_put_invalid_label_scope(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    api_project,
    api_label_1,
    api_label_2,
    api_label_3,
    created_at,
):
    updated_project_json = {
        "detailedDescription": "updated=description",
        "name": "updated_name",
        "labels": [str(api_label_3.label_id)],
    }

    response = client.put(
        f"/projects/{project_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=updated_project_json,
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST, response.text
    assert "Unable to add label" in response.text, "Incorrect error message in response"

    not_updated_project_response = client.get(f"/projects/{project_id_str}")
    result = not_updated_project_response.json()
    assert result["labels"] == [api_label_1.dict(), api_label_2.dict()]


@pytest.mark.usefixtures("add_example_labels")
@pytest.mark.usefixtures("add_example_user")
def test_projects_post(
    client: testclient.TestClient, mock_token_user_id, api_user, api_label_1, created_at
):
    new_project_body = {
        "detailedDescription": "new-description",
        "public": True,
        "name": "test-post-project",
        "executiveSummary": "new-summary",
        "labels": [str(api_label_1.label_id)],
    }

    response = client.post(
        "/projects",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=new_project_body,
    )
    assert response.status_code == status.HTTP_201_CREATED, response.text
    new_project_id = response.json()["projectId"]

    expected = {
        "name": "test-post-project",
        "owner": api_user.dict(),
        "projectId": new_project_id,
        "public": True,
        "codeRepositories": [],
        "dataRepositories": [],
        "detailedDescription": "new-description",
        "executiveSummary": "new-summary",
        "experimentRepositories": [],
        "labels": [api_label_1.dict()],
        "modelRepositories": [],
        "userRole": -1,
        "updatedAt": None,
    }

    new_project_response = client.get(f"/projects/{new_project_id}")
    assert (
        new_project_response.status_code == status.HTTP_200_OK
    ), new_project_response.text
    result = new_project_response.json()
    assert testing.time.has_created_at(result)
    assert result == expected


def test_projects_post_invalid_label_scope(
    client: testclient.TestClient,
    add_example_labels,
    add_example_user,
    mock_token_user_id,
    api_user,
    api_label_3,
):
    all_projects_response_before = client.get(
        "/projects",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    new_project_body = {
        "detailedDescription": "new-description",
        "public": True,
        "name": "test-post-project",
        "executiveSummary": "new-summary",
        "labels": [str(api_label_3.label_id)],
    }

    response = client.post(
        "/projects",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=new_project_body,
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST, response.text
    assert "Unable to add label" in response.text, "Incorrect error message in response"

    all_projects_response_after = client.get(
        "/projects",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert len(all_projects_response_before.json()["projects"]) == len(
        all_projects_response_after.json()["projects"]
    )


def test_projects_project_id_delete_and_related_invitations(
    client: testclient.TestClient,
    mock_token_user_id,
    project_id_str,
    mock_tracking_server_url,
    add_all_example_invitations,
):
    invitations_regarding_this_project = [
        {
            "invitationId": "1989f1db-7489-4afc-93b6-5f369c204226",
            "invitedToType": "PROJECT",
            "invitedType": "ORGANIZATION",
            "invitedId": "1989f1db-7489-4afc-93c5-5f369c204224",
            "invitedToId": "e931c97e-917b-499c-b7b1-fcb52ae96f91",
            "role": "RESEARCHER",
            "createdAt": (
                testing.time.DEFAULT_CREATED_AT + datetime.timedelta(hours=2)
            ).isoformat(),
            "updatedAt": None,
            "invitedName": "foo",
            "invitedToName": "test_project_name",
            "inviterName": "test-name-2",
        },
        {
            "invitationId": "1989f1db-7489-4afc-93b6-5f369c204225",
            "invitedToType": "PROJECT",
            "invitedType": "GROUP",
            "invitedId": "7641ab6b-750c-4cb6-ac15-4699601300c1",
            "invitedToId": "e931c97e-917b-499c-b7b1-fcb52ae96f91",
            "role": "RESEARCHER",
            "createdAt": (
                testing.time.DEFAULT_CREATED_AT + datetime.timedelta(hours=1)
            ).isoformat(),
            "updatedAt": None,
            "invitedName": "test-name",
            "invitedToName": "test_project_name",
            "inviterName": "test-name-2",
        },
        {
            "invitationId": "1989f1db-7489-4afc-93b6-5f369c204224",
            "invitedToType": "PROJECT",
            "invitedType": "USER",
            "invitedId": "6641ab6b-750c-4cb6-ac15-4699601300c1",
            "invitedToId": "e931c97e-917b-499c-b7b1-fcb52ae96f91",
            "role": "RESEARCHER",
            "createdAt": testing.time.DEFAULT_CREATED_AT.isoformat(),
            "updatedAt": None,
            "invitedName": "test-name",
            "invitedToName": "test_project_name",
            "inviterName": "test-name-2",
        },
    ]
    invitations = client.get(
        "/invitations",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params=[("startindex", 0), ("pagelength", 50)],
    ).json()["invitations"]
    assert all(invite in invitations for invite in invitations_regarding_this_project)

    response = client.delete(
        f"/projects/{project_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(
        f"/projects/{project_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND, response.text

    invitations = client.get(
        "/invitations",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params=[("startindex", 0), ("pagelength", 50)],
    ).json()["invitations"]
    assert all(
        invite not in invitations for invite in invitations_regarding_this_project
    )


def test_projects_project_id_delete_when_insufficient_role(
    client: testclient.TestClient,
    add_example_project,
    mock_get_second_user_info_and_be_project_researcher,
    project_id_str,
):
    # Note: This test case is considered a failure if it raises
    # `NameError: Environment variable 'MLFLOW_TRACKING_URI_INTERNAL' not set`
    # since the route should return 403 before trying to read this env var.
    response = client.delete(
        f"/projects/{project_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_403_FORBIDDEN, response.text

    # Check project still exists
    response = client.get(
        f"/projects/{project_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text


@pytest.mark.parametrize(("startindex", "length"), [(0, 1), (30, 0)])
@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("mock_token_user_id")
def test_projects_user_user_id_get_pagination(
    client: testclient.TestClient, user_id_str, startindex, length
):
    """Test case for projects_user_user_id_get

    Returns all projects for user with userId
    """
    params = [("startindex", startindex), ("pagelength", 50)]
    response = client.get(
        f"/projects/user/{user_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params=params,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()

    assert result["totalRecords"] == 1
    assert result["pageRecords"] == length

    for project in result["projects"]:
        assert project["userRole"] == 5, "User role should be OWNER"


@pytest.mark.parametrize(
    ("header", "expected_status_code"),
    (
        ({}, status.HTTP_200_OK),
        ({"Authorization": ""}, status.HTTP_401_UNAUTHORIZED),
        (
            {"Authorization": "InvalidAuth test-invalid-token"},
            status.HTTP_401_UNAUTHORIZED,
        ),
        (
            testing.token_verifier.INVALID_AUTHORIZATION_HEADER,
            status.HTTP_401_UNAUTHORIZED,
        ),
    ),
)
@pytest.mark.usefixtures("add_2_example_projects")
def test_projects_user_user_id_get_logged_out(
    client: testclient.TestClient, user_id_str, header, expected_status_code
):
    """Test case for projects_user_user_id_get

    Returns all public projects for user with userId if request from
    unauthenticated user (i.e. user that is not logged in).
    """
    response = client.get(
        f"/projects/user/{user_id_str}",
        headers=header,
    )

    assert response.status_code == expected_status_code, response.text

    if response == status.HTTP_200_OK:
        result = response.json()
        assert result["totalRecords"] == 1

        for project in result["projects"]:
            assert project["userRole"] == -1, "User role should be NO ROLE"


@pytest.mark.usefixtures("add_2_example_projects")
def test_projects_user_user_id_get_logged_in(
    client: testclient.TestClient, mock_token_user_id, user_id_str
):
    """Test case for projects_user_user_id_get

    Returns all projects for user with userId if request is by that user.
    """
    response = client.get(
        f"/projects/user/{user_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()

    assert result["totalRecords"] == 2

    for project in result["projects"]:
        assert project["userRole"] == 5, "User role should be OWNER"


@pytest.mark.parametrize(
    ("order_by", "ascending", "expected"),
    [
        # Test case: order by created, private project is created last in the setup
        ("created", "false", ["project_private_id_str", "project_id_str"]),
    ],
)
@pytest.mark.usefixtures("add_2_example_projects")
@pytest.mark.usefixtures("add_example_user")
@pytest.mark.usefixtures("mock_token_user_id")
def test_projects_user_user_id_get_ordered(
    client: testclient.TestClient, request, user_id_str, order_by, ascending, expected
):
    expected = [request.getfixturevalue(project) for project in expected]

    response = client.get(
        f"/projects/user/{user_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params={
            "order_by": order_by,
            "ascending": ascending,
        },
    )
    assert response.status_code == 200, response.text

    result = [project["projectId"] for project in response.json()["projects"]]
    assert result == expected


@pytest.mark.parametrize(
    ("headers", "labels", "expected", "expected_project_ids"),
    [
        # Test case: logged in, label 3 matches both projects
        (
            testing.token_verifier.VALID_AUTHORIZATION_HEADER,
            ["database_label_3"],
            2,
            ["project_with_labels_3_to_5_id_str", "project_with_labels_1_to_5_id_str"],
        ),
        # Test case: logged in, label 3 matches both projects, but one is private
        (
            None,
            ["database_label_3"],
            1,
            ["project_with_labels_1_to_5_id_str"],
        ),
    ],
)
@pytest.mark.usefixtures("add_example_project_with_labels_3_to_5")
@pytest.mark.usefixtures("add_example_project_with_labels_1_to_5")
@pytest.mark.usefixtures("mock_token_user_id")
def test_projects_user_user_id_get_filter_by_labels(
    request,
    client: testclient.TestClient,
    headers,
    labels,
    expected,
    expected_project_ids,
    user_id_str,
):
    """Test case for projects_get with labels filter.

    Notes
    -----
    - project_private
      - is private
      - contains labels 1 and 2
    - project_with_labels_1_to_5
      - is private
      - contains database_label 1 to 4

    """
    params = (
        {"labels": [request.getfixturevalue(label).id for label in labels]}
        if labels
        else None
    )
    expected_project_ids = (
        [request.getfixturevalue(project_id) for project_id in expected_project_ids]
        if expected_project_ids
        else None
    )

    response = client.get(
        f"/projects/user/{user_id_str}", headers=headers, params=params
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["totalRecords"] == expected

    result_project_ids = [project["projectId"] for project in result["projects"]]

    if expected_project_ids is not None:
        assert all(
            project_id in result_project_ids for project_id in expected_project_ids
        )
    else:
        assert not result_project_ids


@pytest.mark.parametrize(
    "headers", (testing.token_verifier.VALID_AUTHORIZATION_HEADER, {})
)
@pytest.mark.usefixtures("add_example_user")
@pytest.mark.usefixtures("mock_token_user_id")
def test_projects_get_without_any_projects(client: testclient.TestClient, headers):
    # bug report https://gitlab.com/mantik-ai/mantik-api/-/issues/72
    # test needs to be executed in isolation to see failure

    response = client.get(
        "/projects",
        headers=headers,
    )
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json() == {"totalRecords": 0, "pageRecords": 0, "projects": []}


def test_post_without_proper_references(
    client: testclient.TestClient, mock_token_user_id, api_user
):
    new_project_body = {
        "detailedDescription": "new-description",
        "public": True,
        "name": "test-post-project",
        "executiveSummary": "new-summary",
    }

    response = client.post(
        "/projects",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=new_project_body,
    )
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, response.text
    assert response.json()["detail"] == "Linked resource not found"


@pytest.mark.usefixtures("add_example_user")
def test_private_project_post_and_and_add_extra_element(
    client: testclient.TestClient, mock_token_user_id, api_user
):
    new_project_body = {
        "detailedDescription": "new-description",
        "public": False,
        "name": "test-private-project",
        "executiveSummary": "new-summary",
    }

    response = client.post(
        "/projects",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=new_project_body,
    )
    assert response.status_code == status.HTTP_201_CREATED, response.text
    new_project_id = response.json()["projectId"]

    code_repository_update = {
        "codeRepositoryName": "new_name",
        "uri": "new_uri",
        "accessToken": "new_token",
        "description": "new description",
        "platform": "GitHub",
    }

    response = client.post(
        f"/projects/{new_project_id}/code",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=code_repository_update,
    )
    assert response.status_code == status.HTTP_201_CREATED


@pytest.fixture
def setup_sample_projects(client: testclient.TestClient):
    def add_new_project(test_client: testclient.TestClient, new_project_body: dict):
        response = test_client.post(
            "/projects",
            headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
            json=new_project_body,
        )
        assert response.status_code == status.HTTP_201_CREATED, response.text

    add_new_project(
        client,
        {
            "detailedDescription": "new ML topic",
            "public": False,
            "name": "project1",
            "executiveSummary": "thisisalongstring",
        },
    )
    add_new_project(
        client,
        {
            "detailedDescription": "1234",
            "public": False,
            "name": "project2",
            "executiveSummary": "new topic ML",
        },
    )
    add_new_project(
        client,
        {
            "detailedDescription": "none",
            "public": False,
            "name": "project3 (ML new topic)",
            "executiveSummary": "thisisalsoaalongstring",
        },
    )
    add_new_project(
        client,
        {
            "detailedDescription": "none",
            "public": False,
            "name": "project4 (Only ML)",
            "executiveSummary": "none",
        },
    )


@pytest.mark.parametrize(
    "search_query_params,expected_projects",
    [
        (
            [("words", ["ML"])],
            [
                "project4 (Only ML)",
                "project3 (ML new topic)",
                "project2",
                "project1",
            ],
        ),
        (
            [("words", ["new", "ML", "topic"])],
            [
                "project3 (ML new topic)",
                "project2",
                "project1",
            ],
        ),
        (
            [("words", ["3"])],
            [
                "project3 (ML new topic)",
                "project2",
            ],
        ),
        ([("words", ["ML", "long", "also"])], []),
    ],
    ids=[
        "Search for projects that contain [ML] (all projects contain it)",
        "Search for projects that contain [new, ML, topic] "
        "(project 1, 2 and 3 contain all of these words)",
        "Search for projects that contain [3] "
        "(project3 contains 3 in the name and project2 in the description)",
        "Search for projects that contain [ML, long, also] "
        "(no project contains all of these words in one field)",
    ],
)
@pytest.mark.usefixtures("add_example_user")
def test_projects_get_with_words_parameters(
    client: testclient.TestClient,
    mock_token_user_id,
    setup_sample_projects,
    search_query_params,
    expected_projects,
):
    response = client.get(
        "/projects",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params=search_query_params,
    )

    assert response.status_code == status.HTTP_200_OK
    assert [
        project["name"] for project in response.json()["projects"]
    ] == expected_projects


@pytest.mark.parametrize(
    "search_query_params,expected_projects",
    [
        (
            [("words", ["ML"])],
            [
                "project4 (Only ML)",
                "project3 (ML new topic)",
                "project2",
                "project1",
            ],
        ),
        (
            [("words", ["new", "ML", "topic"])],
            [
                "project3 (ML new topic)",
                "project2",
                "project1",
            ],
        ),
        (
            [("words", ["3"])],
            [
                "project3 (ML new topic)",
                "project2",
            ],
        ),
        ([("words", ["ML", "long", "also"])], []),
    ],
    ids=[
        "Search for projects that contain [ML] (all projects contain it)",
        "Search for projects that contain [new, ML, topic] "
        "(project 1, 2 and 3 contain all of these words)",
        "Search for projects that contain [3] "
        "(project3 contains 3 in the name and project2 in the description)",
        "Search for projects that contain [ML, long, also] "
        "(no project contains all of these words in one field)",
    ],
)
@pytest.mark.usefixtures("add_example_user")
def test_projects_user_user_id_get_with_words_parameters(
    client: testclient.TestClient,
    mock_token_user_id,
    setup_sample_projects,
    search_query_params,
    expected_projects,
    user_id_str,
):
    response = client.get(
        f"/projects/user/{user_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params=search_query_params,
    )

    assert response.status_code == status.HTTP_200_OK

    result = [project["name"] for project in response.json()["projects"]]
    assert result == expected_projects
