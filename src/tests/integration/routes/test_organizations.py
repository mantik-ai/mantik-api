import datetime

import fastapi.testclient as testclient
import pytest
import starlette.status as status

import mantik_api.models as models
import mantik_api.testing as testing

HEADERS = testing.token_verifier.VALID_AUTHORIZATION_HEADER


@pytest.mark.usefixtures("add_example_organization")
def test_organizations_get(
    client: testclient.TestClient,
    api_organization: models.organization.Organization,
) -> None:
    params = [("startindex", 0), ("pagelength", 50)]
    response = client.get("/organizations", headers=HEADERS, params=params)
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json() == {
        "totalRecords": 1,
        "pageRecords": 1,
        "organizations": [api_organization.dict()],
    }


@pytest.mark.usefixtures("add_example_organization")
def test_organizations_organization_id_get(
    client: testclient.TestClient,
    organization_id_str: str,
    api_organization: models.organization.Organization,
) -> None:
    expected = api_organization.dict()
    response = client.get(f"/organizations/{organization_id_str}", headers=HEADERS)

    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json() == expected


@pytest.mark.usefixtures("add_example_organization")
def test_organizations_organization_id_put(
    client: testclient.TestClient,
    organization_id_str: str,
    second_user_id_str: str,
    mock_get_user_info_and_be_project_owner,
    user_group_id_str,
    user_id_str,
    second_api_user,
    api_user_group,
    api_user,
) -> None:
    # Rename organization and add second user
    new_name = "test_organization_renamed"
    updated_organisation_json = {
        "name": new_name,
        "contact_id": user_id_str,
        "group_ids": [user_group_id_str],
        "member_ids": [second_user_id_str, user_id_str],
    }

    response = client.put(
        f"/organizations/{organization_id_str}",
        headers=HEADERS,
        json=updated_organisation_json,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    _assert_update_successful(
        client=client,
        organization_id=organization_id_str,
        expected={
            "organizationId": organization_id_str,
            "name": new_name,
            "contact": api_user.dict(),
            "groups": [api_user_group.dict()],
            "members": [
                api_user.dict(),
                second_api_user.dict(),
            ],
            "createdAt": testing.time.DEFAULT_CREATED_AT.isoformat(),
        },
    )

    # Change contact, remove user group and second user
    updated_organisation_json = {
        "name": new_name,
        "contact_id": second_user_id_str,
        "group_ids": [],
        "member_ids": [second_user_id_str],
    }

    response = client.put(
        f"/organizations/{organization_id_str}",
        headers=HEADERS,
        json=updated_organisation_json,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    _assert_update_successful(
        client=client,
        organization_id=organization_id_str,
        expected={
            "organizationId": organization_id_str,
            "name": new_name,
            "contact": second_api_user.dict(),
            "groups": [],
            "members": [
                second_api_user.dict(),
            ],
            "createdAt": testing.time.DEFAULT_CREATED_AT.isoformat(),
        },
    )


def _assert_update_successful(client, organization_id: str, expected: dict):
    response = client.get(f"/organizations/{organization_id}", headers=HEADERS)

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert testing.time.has_updated_at(result)
    assert result == expected


@pytest.mark.usefixtures("add_example_organization")
def test_organizations_organization_id_put_when_not_contact(
    client: testclient.TestClient,
    organization_id_str: str,
    second_user_id_str: str,
    user_group_id_str,
    user_id_str,
    mock_get_user_info_and_be_project_maintainer,
) -> None:
    updated_organisation_json = {
        "name": "1234",
        "contact_id": second_user_id_str,
        "group_ids": [user_group_id_str],
        "member_ids": [second_user_id_str, user_id_str],
    }

    response = client.put(
        f"/organizations/{organization_id_str}",
        headers=HEADERS,
        json=updated_organisation_json,
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED, response.text
    assert (
        response.json()["detail"] == "Only current Organization's Contact "
        "can modify the Organization information"
    )


@pytest.mark.usefixtures("add_example_user_group")
def test_organizations_post(
    client: testclient.TestClient,
    api_add_organization: models.organization.AddOrganization,
    mock_get_user_info_and_be_project_owner,
    api_user,
    api_organization,
) -> None:
    # create org
    response = client.post(
        "/organizations",
        headers=HEADERS,
        json=api_add_organization.dict(),
    )

    assert response.status_code == status.HTTP_201_CREATED, response.text
    new_organization_id = response.json()["organizationId"]

    # fetch org - should exist now
    response = client.get(f"/organizations/{new_organization_id}", headers=HEADERS)
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["contact"] == api_user.dict()
    assert response.json()["name"] == api_add_organization.name


@pytest.mark.usefixtures("add_all_example_invitations")
def test_organizations_organization_id_delete_and_related_invitations(
    client: testclient.TestClient,
    mock_token_user_id,
    organization_id_str: str,
) -> None:
    invitations_regarding_this_organization = [
        {
            "invitationId": "1989f1db-7489-4afc-93b6-5f369c204228",
            "invitedToType": "ORGANIZATION",
            "invitedType": "GROUP",
            "invitedId": "7641ab6b-750c-4cb6-ac15-4699601300c1",
            "invitedToId": "1989f1db-7489-4afc-93c5-5f369c204224",
            "role": None,
            "createdAt": (
                testing.time.DEFAULT_CREATED_AT + datetime.timedelta(hours=4)
            ).isoformat(),
            "updatedAt": None,
            "invitedName": "test-name",
            "invitedToName": "foo",
            "inviterName": "test-name-2",
        },
        {
            "invitationId": "1989f1db-7489-4afc-93b6-5f369c204227",
            "invitedToType": "ORGANIZATION",
            "invitedType": "USER",
            "invitedId": "6641ab6b-750c-4cb6-ac15-4699601300c1",
            "invitedToId": "1989f1db-7489-4afc-93c5-5f369c204224",
            "role": None,
            "createdAt": (
                testing.time.DEFAULT_CREATED_AT + datetime.timedelta(hours=3)
            ).isoformat(),
            "updatedAt": None,
            "invitedName": "test-name",
            "invitedToName": "foo",
            "inviterName": "test-name-2",
        },
        {
            "invitationId": "1989f1db-7489-4afc-93b6-5f369c204226",
            "invitedToType": "PROJECT",
            "invitedType": "ORGANIZATION",
            "invitedId": "1989f1db-7489-4afc-93c5-5f369c204224",
            "invitedToId": "e931c97e-917b-499c-b7b1-fcb52ae96f91",
            "role": "RESEARCHER",
            "createdAt": (
                testing.time.DEFAULT_CREATED_AT + datetime.timedelta(hours=2)
            ).isoformat(),
            "updatedAt": None,
            "invitedName": "foo",
            "invitedToName": "test_project_name",
            "inviterName": "test-name-2",
        },
    ]
    invitations = client.get(
        "/invitations",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params=[("startindex", 0), ("pagelength", 50)],
    ).json()["invitations"]
    assert all(
        invite in invitations for invite in invitations_regarding_this_organization
    )

    # Delete the organization
    response = client.delete(
        f"/organizations/{organization_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Check that deletion succeeded
    response = client.delete(
        f"/organizations/{organization_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND, response.text

    invitations = client.get(
        "/invitations",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        params=[("startindex", 0), ("pagelength", 50)],
    ).json()["invitations"]
    assert all(
        invite not in invitations for invite in invitations_regarding_this_organization
    )


@pytest.mark.usefixtures("add_example_organization")
def test_organizations_organization_id_delete_when_not_contact(
    client: testclient.TestClient,
    mock_token_second_user_id,
    organization_id_str: str,
) -> None:
    # Try to delete the organization as non-contact person
    response = client.delete(
        f"/organizations/{organization_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED, response.text

    # Check that the organization is still there
    response = client.get(
        f"/organizations/{organization_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text


@pytest.mark.usefixtures("add_example_organization")
def test_organizations_organization_member_id_delete_when_contact(
    client: testclient.TestClient,
    mock_token_user_id,
    organization_id_str: str,
    third_user_id_str: str,
) -> None:
    _assert_organization_has_one_member(client, organization_id_str)

    # Delete one member from the organization
    response = client.delete(
        f"/organizations/{organization_id_str}/members/{third_user_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Check that deletion succeeded
    response = client.get(
        f"/organizations/{organization_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    result = response.json()["members"]
    assert len(result) == 0


@pytest.mark.usefixtures("add_example_organization")
def test_organizations_organization_member_id_delete_when_member(
    client: testclient.TestClient,
    mock_token_third_user_id,
    organization_id_str: str,
    third_user_id_str: str,
) -> None:
    _assert_organization_has_one_member(client, organization_id_str)

    # Delete one member from the organization
    response = client.delete(
        f"/organizations/{organization_id_str}/members/{third_user_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Check that deletion succeeded
    response = client.get(
        f"/organizations/{organization_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    result = response.json()["members"]
    assert len(result) == 0


@pytest.mark.usefixtures("add_example_organization")
def test_organizations_organization_member_id_delete_when_not_user_or_contact(
    client: testclient.TestClient,
    mock_token_second_user_id,
    organization_id_str: str,
    third_user_id_str: str,
) -> None:
    _assert_organization_has_one_member(client, organization_id_str)

    # Delete one member from the organization
    response = client.delete(
        f"/organizations/{organization_id_str}/members/{third_user_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED, response.text

    _assert_organization_has_one_member(client, organization_id_str)


def _assert_organization_has_one_member(
    client: testclient.TestClient, organization_id: str
) -> None:
    response = client.get(
        f"/organizations/{organization_id}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    result = response.json()["members"]
    assert len(result) == 1


@pytest.mark.usefixtures("add_example_organization")
def test_organizations_organization_group_id_delete_when_contact(
    client: testclient.TestClient,
    mock_token_user_id,
    organization_id_str: str,
    user_group_id_str: str,
) -> None:
    _assert_organization_has_one_user_group(client, organization_id_str)

    # Delete one user group from the organization
    response = client.delete(
        f"/organizations/{organization_id_str}/groups/{user_group_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Check that deletion succeeded
    response = client.get(
        f"/organizations/{organization_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    result = response.json()["groups"]
    assert len(result) == 0


@pytest.mark.usefixtures("add_second_example_organization")
def test_organizations_organization_group_id_delete_when_group_admin(
    client: testclient.TestClient,
    mock_token_second_user_id,
    second_organization_id_str: str,
    second_user_group_id_str: str,
) -> None:
    _assert_organization_has_one_user_group(client, second_organization_id_str)

    # Delete one member from the organization
    response = client.delete(
        f"/organizations/{second_organization_id_str}/groups/{second_user_group_id_str}",  # noqa: E501
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # Check that deletion succeeded
    response = client.get(
        f"/organizations/{second_organization_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    result = response.json()["groups"]
    assert len(result) == 0


@pytest.mark.usefixtures("add_example_organization")
def test_organizations_organization_group_id_delete_when_not_group_admin_or_contact(
    client: testclient.TestClient,
    mock_token_third_user_id,
    organization_id_str: str,
    user_group_id_str: str,
    third_user_id_str: str,
) -> None:
    _assert_organization_has_one_user_group(client, organization_id_str)

    # Delete one member from the organization
    response = client.delete(
        f"/organizations/{organization_id_str}/groups/{user_group_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED, response.text

    _assert_organization_has_one_user_group(client, organization_id_str)


def _assert_organization_has_one_user_group(
    client: testclient.TestClient, organization_id: str
) -> None:
    response = client.get(
        f"/organizations/{organization_id}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    result = response.json()["groups"]
    assert len(result) == 1
