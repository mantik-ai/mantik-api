import json
import uuid

import fastapi.testclient as testclient
import pytest
import starlette.status as status

import mantik_api.database as database
import mantik_api.models as models
import mantik_api.testing as testing

HEADERS = testing.token_verifier.VALID_AUTHORIZATION_HEADER
UNAUTHORIZED_RESPONSE_MESSAGE = "Token issued for unauthorized user"


@pytest.mark.usefixtures("add_example_users")
def test_users_get(client: testclient.TestClient):
    """Test case for users_get

    List of all users
    """
    page_length = 50
    params = [("startindex", 56), ("pagelength", page_length)]
    response = client.get(
        "/users",
        params=params,
    )

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["totalRecords"] == 200
    assert result["pageRecords"] == page_length


@pytest.mark.usefixtures("add_example_user")
def test_users_get_with_search_string(client: testclient.TestClient, api_user):
    """Test case for users_get with a search string

    Return a curation of users by searchString
    """
    params = {"search_string": "name"}

    response = client.get("/users", params=params)

    assert response.status_code == status.HTTP_200_OK, response.text
    assert (
        models.user.UsersGet200Response(
            **dict(json.loads(response.content.decode("utf-8")))
        ).users[0]
        == api_user
    )


def test_users_post(
    client_with_token_verifier: testclient.TestClient,
    database_client,
    moto_user_pool,
):
    """Test case for users_post

    Creates a new user

    Note: Checks that no token is needed to sign up a new user.
    """

    name = "name"
    email = "email"
    user = {
        "name": name,
        "password": "Password123!",
        "email": email,
    }

    response = client_with_token_verifier.post(
        "/users",
        headers=HEADERS,
        json=user,
    )

    assert response.status_code == status.HTTP_201_CREATED, response.text

    generated_id = uuid.UUID(response.json()["userId"])

    with database_client.session():
        result = models.user.User.from_database_model(
            database_client.get_one(
                orm_model_type=database.user.User,
                constraints={database.user.User.id: generated_id},
            )
        )
    expected = models.user.User(
        user_id=generated_id,
        name=name,
        email=email,
        created_at=result.created_at,
        updated_at=result.updated_at,
    )

    assert result == expected


def test_add_user_with_same_email_twice(
    client_with_token_verifier: testclient.TestClient,
    moto_user_pool,
    add_example_user,
):
    response = client_with_token_verifier.post(
        "/users",
        headers=HEADERS,
        json={
            "name": "second",
            "email": testing.aws.cognito.EMAIL,
            "password": "Foobar123.",
        },
    )

    assert response.status_code == status.HTTP_409_CONFLICT
    assert response.json()["detail"] == "This email is already in use."


def test_add_user_with_same_username(
    client_with_token_verifier: testclient.TestClient,
    moto_user_pool,
):
    client_with_token_verifier.post(
        "/users",
        headers=HEADERS,
        json={
            "name": testing.aws.cognito.COGNITO_USERNAME,
            "email": "first@example.com",
            "password": "Foobar123.",
        },
    )

    response = client_with_token_verifier.post(
        "/users",
        headers=HEADERS,
        json={
            "name": testing.aws.cognito.COGNITO_USERNAME,
            "email": "second@example.com",
            "password": "Foobar123.",
        },
    )

    assert response.status_code == status.HTTP_409_CONFLICT
    assert response.json()["detail"] == "Username is already taken."


@pytest.mark.usefixtures("add_example_user")
def test_users_user_id_get(client: testclient.TestClient, user_id_str, api_user):
    """Test case for users_user_id_get

    Returns the information on a specific user
    """

    response = client.get(
        f"/users/{user_id_str}",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    assert (
        models.user.User(**dict(json.loads(response.content.decode("utf-8"))))
        == api_user
    )


@pytest.mark.usefixtures("add_example_user")
@pytest.mark.usefixtures("mock_get_user_info_and_be_project_owner")
def test_users_user_id_put(
    client: testclient.TestClient, database_client, user_id_str, database_user
):
    """Test case for users_user_id_put

    Update user info
    """
    name = database_user.preferred_name
    email = database_user.email
    new_full_name = "new_full_name"
    new_info = "new_info"
    new_company = "new_company"
    new_job_title = "new_job_title"
    new_website_url = "new_website_url"

    user1 = {
        "name": name,
        "userId": user_id_str,
        "email": email,
        "fullName": new_full_name,
        "info": new_info,
        "company": new_company,
        "jobTitle": new_job_title,
        "websiteUrl": new_website_url,
    }

    response1 = client.put(
        f"/users/{user_id_str}",
        headers=HEADERS,
        json=user1,
    )
    assert response1.status_code == status.HTTP_204_NO_CONTENT, response1.text
    response_get1 = client.get(f"/users/{user_id_str}", headers=HEADERS)
    assert response_get1.status_code == status.HTTP_200_OK, response1.text
    result1 = response_get1.json()
    expected1 = user1
    assert testing.time.has_created_at(result1)
    assert testing.time.has_updated_at(result1)
    assert result1 == expected1

    # Make sure empty fields in the request are overwritten in the database
    user2 = {**user1, "websiteUrl": None}
    response2 = client.put(
        f"/users/{user_id_str}",
        headers=HEADERS,
        json=user2,
    )
    assert response2.status_code == status.HTTP_204_NO_CONTENT, response2.text
    response_get2 = client.get(f"/users/{user_id_str}", headers=HEADERS)
    assert response_get2.status_code == status.HTTP_200_OK, response2.text
    result2 = response_get2.json()
    expected2 = {**expected1, "websiteUrl": None}
    assert testing.time.has_created_at(result2)
    assert testing.time.has_updated_at(result2)
    assert result2 == expected2


@pytest.mark.usefixtures("add_example_user")
@pytest.mark.usefixtures("mock_impersonate_random_user")
def test_users_user_id_put_while_other_user(
    client: testclient.TestClient, database_client, user_id_str
):
    """Test case for users_user_id_put

    Update user info
    """
    new_name = "new_name"
    email = "test-email"
    new_full_name = "new_full_name"
    new_info = "new_info"
    new_company = "new_company"
    new_job_title = "new_job_title"
    new_website_url = "new_website_url"

    user = {
        "name": new_name,
        "userId": user_id_str,
        "email": email,
        "fullName": new_full_name,
        "info": new_info,
        "company": new_company,
        "jobTitle": new_job_title,
        "websiteUrl": new_website_url,
    }

    response = client.put(
        f"/users/{user_id_str}",
        headers=HEADERS,
        json=user,
    )

    assert response.status_code == status.HTTP_401_UNAUTHORIZED, response.text
    assert response.json()["detail"] == UNAUTHORIZED_RESPONSE_MESSAGE


@pytest.mark.usefixtures("add_example_user")
@pytest.mark.usefixtures("mock_token_user_id")
def test_users_user_id_delete(
    monkeypatch,
    client: testclient.TestClient,
    user_id_str,
    database_client,
    cognito_patcher,
):
    """Test case for users_user_id_delete

    Delete user
    """
    cognito_patcher.patch_delete_user(token=testing.token_verifier.VALID_JWT)

    response = client.delete(
        f"/users/{user_id_str}",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    with database_client.session():
        with pytest.raises(database.exceptions.NotFoundError):
            database_client.get_one(
                orm_model_type=database.user.User,
                constraints={database.user.User.id: user_id_str},
            )

        with pytest.raises(database.exceptions.NotFoundError):
            database_client.get_one(
                orm_model_type=database.connection.Connection,
                constraints={database.connection.Connection.user_id: user_id_str},
            )


@pytest.mark.usefixtures("add_example_user")
@pytest.mark.usefixtures("mock_token_user_id")
def test_users_user_id_delete_cognito_request_fails(
    monkeypatch,
    client: testclient.TestClient,
    user_id_str,
    database_client,
    cognito_patcher,
):
    # Will raise the given error with a message
    # Error must correspond to any exception given in
    # https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/cognito-idp/client/delete_user.html  # noqa: E501
    message = "test-message"
    expected_message = (
        "Unable to delete user: An error occurred "
        "(PasswordResetRequiredException) when calling the DeleteUser "
        f"operation: {message}"
    )
    cognito_patcher.patch_delete_user(
        token=testing.token_verifier.VALID_JWT,
        error_code="PasswordResetRequiredException",
        error_message=message,
    )

    response = client.delete(
        f"/users/{user_id_str}",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_409_CONFLICT, response.text

    result = response.json()["detail"]

    assert result == expected_message

    with database_client.session():
        user_retained = database_client.get_one(
            orm_model_type=database.user.User,
            constraints={database.user.User.id: user_id_str},
        )

        expected = uuid.UUID(user_id_str)
        assert user_retained.id == expected

        connections_retained = database_client.get_many(
            orm_model_type=database.connection.Connection,
            constraints={database.connection.Connection.user_id: user_id_str},
            order_by=database.connection.OrderBy.created,
            ascending=False,
        )

        assert all(
            connection.user_id == expected
            for connection in connections_retained.entities
        )


@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("mock_get_user_info_and_be_project_owner")
def test_users_user_id_delete_rejected_with_owned_project_group_organization(
    monkeypatch, client: testclient.TestClient, user_id_str, database_client
):
    """Test case for users_user_id_delete

    Delete user
    """
    expected = (
        "Owned projects, groups and organizations must be deleted "
        "or transferred to another user: Resource still references Project"
    )
    response = client.delete(
        f"/users/{user_id_str}",
        headers=HEADERS,
    )
    assert response.status_code == status.HTTP_409_CONFLICT, response.text

    result = response.json()["detail"]
    assert result == expected


@pytest.mark.usefixtures("add_example_user")
@pytest.mark.usefixtures("mock_get_user_info_and_be_project_owner")
def test_users_user_id_settings_get(
    client: testclient.TestClient,
    user_id_str: str,
    api_user_settings: models.user_settings.UserSettings,
    mock_hvac,
) -> None:
    """Test case for users_user_id_settings_get
    Returns the settings for a specific user
    """

    response = client.get(f"/users/{user_id_str}/settings", headers=HEADERS)
    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json() == api_user_settings.dict()


@pytest.mark.usefixtures("mock_impersonate_random_user")
def test_users_user_id_settings_get_while_other_user(
    client: testclient.TestClient,
    user_id_str: str,
    api_user_settings: models.user_settings.UserSettings,
) -> None:
    """Test case for users_user_id_settings_get
    Returns the settings for a specific user
    """

    response = client.get(f"/users/{user_id_str}/settings", headers=HEADERS)

    assert response.status_code == status.HTTP_401_UNAUTHORIZED, response.text
    assert response.json()["detail"] == UNAUTHORIZED_RESPONSE_MESSAGE


@pytest.mark.usefixtures("add_user_without_payment_info")
def test_create_user_and_get_settings(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    user_id_str,
    created_at,
):
    response = client.get(f"/users/{user_id_str}/settings", headers=HEADERS)
    assert response.status_code == status.HTTP_200_OK, response.text
    result = response.json()
    expected = {
        "paymentInfo": {"address": None, "paymentDetails": None, "paymentMethod": None},
        "user": {
            "name": "user_with_no_payment_info",
            "updatedAt": None,
            "userId": user_id_str,
        },
    }

    assert testing.time.has_created_at(result["user"])
    assert result == expected


@pytest.mark.usefixtures("add_example_user_group")
@pytest.mark.usefixtures("mock_get_user_info_and_be_project_owner")
def test_users_user_id_groups_get(
    client: testclient.TestClient,
    user_id_str,
    user_group_id_str,
):
    """Test case for users_user_id_groups_get

    Get all User groups of user
    """
    params = [("startindex", 0), ("pagelength", 50)]
    response = client.get(
        f"/users/{user_id_str}/groups",
        headers=HEADERS,
        params=params,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["totalRecords"] == 1
    assert response.json()["userGroups"][0]["userGroupId"] == user_group_id_str


@pytest.mark.usefixtures("mock_impersonate_random_user")
def test_users_user_id_groups_get_while_other_user(
    client: testclient.TestClient, user_id_str
):
    """Test case for users_user_id_groups_get

    Get all User groups of user
    """
    params = [("startindex", 0), ("pagelength", 50)]
    response = client.get(
        f"/users/{user_id_str}/groups",
        headers=HEADERS,
        params=params,
    )

    assert response.status_code == status.HTTP_401_UNAUTHORIZED, response.text
    assert response.json()["detail"] == UNAUTHORIZED_RESPONSE_MESSAGE


@pytest.mark.usefixtures("add_example_organization")
@pytest.mark.usefixtures("mock_get_user_info_and_be_project_owner")
def test_users_user_id_organizations_get(
    client: testclient.TestClient,
    user_id_str,
    organization_id_str,
):
    """Test case for users_user_id_organizations_get

    Get all user organizations of user
    """
    params = [("startindex", 0), ("pagelength", 50)]
    response = client.get(
        f"/users/{user_id_str}/organizations",
        headers=HEADERS,
        params=params,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["totalRecords"] == 1
    assert response.json()["organizations"][0]["organizationId"] == organization_id_str


@pytest.mark.usefixtures("mock_impersonate_random_user")
def test_users_user_id_organizations_get_while_other_user(
    client: testclient.TestClient,
    user_id_str,
    organization_id_str,
):
    """Test case for users_user_id_organizations_get

    Get all user organizations of user
    """
    params = [("startindex", 0), ("pagelength", 50)]
    response = client.get(
        f"/users/{user_id_str}/organizations",
        headers=HEADERS,
        params=params,
    )

    assert response.status_code == status.HTTP_401_UNAUTHORIZED, response.text
    assert response.json()["detail"] == UNAUTHORIZED_RESPONSE_MESSAGE


@pytest.mark.usefixtures("add_example_connection")
@pytest.mark.usefixtures("mock_token_user_id")
def test_users_user_id_settings_connections_get(
    client: testclient.TestClient,
    user_id_str,
    api_connection,
    second_api_connection,
    api_connection_firecrest,
    api_connection_slurm_private_key,
    mock_hvac,
):
    """Test case for users_user_id_settings_connections_get

    Get all user connections
    """
    expected = {
        "totalRecords": 4,
        "pageRecords": 4,
        "connections": [
            api_connection.dict(),
            second_api_connection.dict(),
            api_connection_firecrest.dict(),
            api_connection_slurm_private_key.dict(),
        ],
    }
    response = client.get(
        f"/users/{user_id_str}/settings/connections",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    result = response.json()

    assert response.status_code == status.HTTP_200_OK, response.text
    assert result == expected


@pytest.mark.usefixtures("add_example_user")
@pytest.mark.usefixtures("mock_token_user_id")
def test_users_user_id_password_post(
    client_with_moto_token_validation: testclient.TestClient,
    user_id_str,
    database_user,
    boto3_cognito_idp_client,
    create_and_confirm_fake_cognito_user,
):
    """Test case for users_user_id_password_post

    Change user password
    """
    password = create_and_confirm_fake_cognito_user["password"]
    token, _ = testing.aws.cognito.get_access_token_and_credentials(
        username=create_and_confirm_fake_cognito_user["username"], password=password
    )

    new_password = "New-test-password!2"

    response = client_with_moto_token_validation.post(
        f"/users/{user_id_str}/password",
        headers=testing.token_verifier.create_moto_jwt_authorization_header(token),
        json={"old_password": password, "new_password": new_password},
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response_create_token = client_with_moto_token_validation.post(
        "/mantik/tokens/create",
        json={"username": database_user.preferred_name, "password": new_password},
    )
    assert (
        response_create_token.status_code == status.HTTP_201_CREATED
    ), response_create_token.text


@pytest.mark.usefixtures("add_example_user")
@pytest.mark.usefixtures("mock_token_user_id")
@pytest.mark.parametrize(
    ("old_password", "new_password", "expected_http_error"),
    [
        (
            testing.aws.cognito.PASSWORD,
            testing.aws.cognito.PASSWORD,
            status.HTTP_422_UNPROCESSABLE_ENTITY,
        ),
        ("Incorrect-Password.1234", "New-Password.1234", status.HTTP_400_BAD_REQUEST),
        (testing.aws.cognito.PASSWORD, "1", status.HTTP_400_BAD_REQUEST),
    ],
)
def test_users_user_id_password_post_cognito_request_fails(
    client_with_moto_token_validation: testclient.TestClient,
    old_password,
    new_password,
    expected_http_error,
    user_id_str,
    database_client,
    database_user,
    moto_user_pool,
    boto3_cognito_idp_client,
    create_and_confirm_fake_cognito_user,
):
    token, _ = testing.aws.cognito.get_access_token_and_credentials(
        username=create_and_confirm_fake_cognito_user["username"],
        password=testing.aws.cognito.PASSWORD,
    )

    response = client_with_moto_token_validation.post(
        f"/users/{user_id_str}/password",
        headers=testing.token_verifier.create_moto_jwt_authorization_header(token),
        json={"old_password": old_password, "new_password": new_password},
    )
    assert response.status_code == expected_http_error, response.text


@pytest.mark.usefixtures("mock_impersonate_random_user")
def test_users_user_id_settings_connections_get_while_other_user(
    client: testclient.TestClient, user_id_str, api_connection
):
    """Test case for users_user_id_settings_connections_get

    Get all user connections
    """
    response = client.get(
        f"/users/{user_id_str}/settings/connections",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_401_UNAUTHORIZED, response.text


@pytest.mark.usefixtures("add_example_connection")
def test_users_user_id_settings_connections_get_does_not_return_others_connections(
    client: testclient.TestClient,
    user_id_str,
    api_connection,
    mock_impersonate_random_user,
):
    """Test case for users_user_id_settings_connections_get

    Get all user connections
    """
    random_user_id = mock_impersonate_random_user
    response = client.get(
        f"/users/{str(random_user_id)}/settings/connections",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json() == {"connections": [], "totalRecords": 0, "pageRecords": 0}


@pytest.mark.usefixtures("add_example_connection")
@pytest.mark.usefixtures("mock_token_user_id")
def test_users_user_id_settings_connections_connection_id_get(
    client: testclient.TestClient,
    user_id_str,
    api_connection,
    connection_id_str,
    mock_hvac,
):
    expected = api_connection.dict()

    response = client.get(
        f"/users/{user_id_str}/settings/connections/{connection_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    result = response.json()

    assert response.status_code == status.HTTP_200_OK, response.text
    assert result == expected


@pytest.mark.usefixtures("mock_impersonate_random_user")
def test_users_user_id_settings_connections_connection_id_get_while_other_user(
    client: testclient.TestClient, user_id_str, connection_id_str
):
    response = client.get(
        f"/users/{user_id_str}/settings/connections/{connection_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )

    assert response.status_code == status.HTTP_401_UNAUTHORIZED, response.text


@pytest.mark.usefixtures("add_example_user")
@pytest.mark.usefixtures("mock_get_user_info_and_be_project_owner")
def test_users_user_id_settings_connections_post(
    client: testclient.TestClient, user_id_str, mock_hvac
):
    """Test case for users_user_id_settings_connections_post

    Adds user connection
    """
    add_connection = {
        "connectionProvider": database.connection.ConnectionProvider.HPC_JSC,
        "password": "password",
        "loginName": "loginName",
        "connectionName": "connectionName",
        "authMethod": "Token",
        "token": "token",
        "privateKey": "privateKey",
    }

    response = client.post(
        f"/users/{user_id_str}/settings/connections",
        headers=HEADERS,
        json=add_connection,
    )

    assert response.status_code == status.HTTP_201_CREATED

    response = client.get(f"/users/{user_id_str}/settings", headers=HEADERS)
    assert response.status_code == status.HTTP_200_OK, response.text


@pytest.mark.usefixtures("add_example_user")
@pytest.mark.usefixtures("mock_get_user_info_and_be_project_owner")
def test_users_user_id_settings_ssh_connections_post_and_get(
    client: testclient.TestClient, user_id_str, mock_hvac, second_api_user
):
    """Test case for users_user_id_settings_connections_post

    Given I want to store SSH credentials
    When  I create a new connections object via mantik API
    Then  I can specify the type of connection as Remote Computing System
    And   I can store my private SSH key
    And   the new connection gets stored to my connections
    And   I can see the connection in my connections
    And   I only I can access this connection via mantik API

    """
    add_connection = {
        "connectionProvider": database.connection.ConnectionProvider.REMOTE_COMPUTE_SYSTEM,  # noqa E501
        "connectionName": "connectionName",
        "authMethod": database.connection.AuthMethod.SSH_KEY,
        "privateKey": "privateKey",
    }
    # create the specified connection
    response = client.post(
        f"/users/{user_id_str}/settings/connections",
        headers=HEADERS,
        json=add_connection,
    )
    assert response.status_code == status.HTTP_201_CREATED

    # Get the specified connection for first user
    connection_id = response.json()["connectionId"]
    response = client.get(
        f"/users/{user_id_str}/settings/connections/{connection_id}", headers=HEADERS
    )

    result = response.json()

    assert response.status_code == status.HTTP_200_OK, response.text
    assert result["connectionName"] == "connectionName", (
        result["privateKey"] == "privateKey"
    )

    # test that another user does not have access to the same connection
    response = client.get(
        f"/users/{second_api_user.user_id}/settings/connections/{connection_id}",
        headers=HEADERS,
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED, response.text
    assert response.json()["detail"] == UNAUTHORIZED_RESPONSE_MESSAGE


@pytest.mark.usefixtures("mock_impersonate_random_user")
def test_users_user_id_settings_connections_post_while_other_user(
    client: testclient.TestClient,
    user_id_str,
):
    """Test case for users_user_id_settings_connections_post

    Adds user connection
    """
    add_connection = {
        "connectionProvider": database.connection.ConnectionProvider.HPC_E4.value,
        "password": "password",
        "loginName": "loginName",
        "connectionName": "connectionName",
        "authMethod": "Token",
        "token": "token",
        "privateKey": "privateKey",
    }

    response = client.post(
        f"/users/{user_id_str}/settings/connections",
        headers=HEADERS,
        json=add_connection,
    )

    assert response.status_code == status.HTTP_401_UNAUTHORIZED, response.text
    assert response.json()["detail"] == UNAUTHORIZED_RESPONSE_MESSAGE


@pytest.mark.usefixtures("add_example_connection")
@pytest.mark.usefixtures("mock_get_user_info_and_be_project_owner")
def test_users_user_id_settings_connections_connection_id_put(
    client: testclient.TestClient, user_id_str, connection_id_str, mock_hvac
):
    """Test case for users_user_id_settings_connections_put

    Updated user connection
    """
    add_connection = {
        "connectionProvider": database.connection.ConnectionProvider.HPC_E4.value,
        "password": "New password",
        "loginName": "New loginName",
        "connectionName": "New connectionName",
        "authMethod": "Username-Password",
        "privateKey": "privateKey",
    }

    response = client.put(
        f"/users/{user_id_str}/settings/connections/{connection_id_str}",
        headers=HEADERS,
        json=add_connection,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(
        f"/users/{user_id_str}/settings/connections/{connection_id_str}",
        headers=HEADERS,
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()
    assert result["connectionId"] == connection_id_str
    assert result["privateKey"] == add_connection["privateKey"]
    assert (
        result["connectionProvider"]
        == database.connection.ConnectionProvider.HPC_E4.value
    )


@pytest.mark.usefixtures("add_example_connection")
@pytest.mark.usefixtures("mock_get_user_info_and_be_project_owner")
def test_users_user_id_settings_connections_connection_id_delete(
    client: testclient.TestClient, user_id_str, connection_id_str, mock_hvac
):
    response = client.delete(
        f"/users/{user_id_str}/settings/connections/{connection_id_str}",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(
        f"/users/{user_id_str}/settings/connections/{connection_id_str}",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND, response.text


@pytest.mark.usefixtures("mock_impersonate_random_user")
def test_users_user_id_settings_connections_connection_id_put_while_other_user(
    client: testclient.TestClient,
    user_id_str,
    connection_id_str,
):
    """Test case for users_user_id_settings_connections_put

    Updated user connection
    """
    add_connection = {
        "connectionProvider": database.connection.ConnectionProvider.HPC_E4.value,
        "password": "password",
        "loginName": "loginName",
        "connectionName": "connectionName",
        "authMethod": "Token",
        "token": "token",
        "privateKey": "privateKey",
    }

    response = client.put(
        f"/users/{user_id_str}/settings/connections/{connection_id_str}",
        headers=HEADERS,
        json=add_connection,
    )

    assert response.status_code == status.HTTP_401_UNAUTHORIZED, response.text
    assert response.json()["detail"] == UNAUTHORIZED_RESPONSE_MESSAGE


@pytest.mark.usefixtures("add_example_user")
@pytest.mark.usefixtures("mock_token_user_id")
def test_users_user_id_email_put(
    client_with_moto_token_validation: testclient.TestClient,
    user_id_str,
    database_client,
    database_user,
    boto3_cognito_idp_client,
    create_and_confirm_fake_cognito_user,
):
    token, _ = testing.aws.cognito.get_access_token_and_credentials(
        username=create_and_confirm_fake_cognito_user["username"],
        password=create_and_confirm_fake_cognito_user["password"],
    )
    with database_client.session():
        user_before_email_update = database_client.get_one(
            orm_model_type=database.user.User,
            constraints={database.user.User.id: user_id_str},
        )

    new_email = "new-test-email"
    response = client_with_moto_token_validation.put(
        f"/users/{user_id_str}/email",
        headers=testing.token_verifier.create_moto_jwt_authorization_header(token),
        json={"new_email": new_email},
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    # moto cannot mimic our expected behavior:
    # Cognito, as configured by us, keeps the original email until the new one
    # has been verified. Hence, we _must_ keep the old one in the database here.
    with database_client.session():
        user_after_email_update = database_client.get_one(
            orm_model_type=database.user.User,
            constraints={database.user.User.id: user_id_str},
        )
        assert (
            user_after_email_update.email != new_email
            and user_before_email_update.email == user_after_email_update.email
        )


@pytest.mark.usefixtures("add_example_user")
@pytest.mark.usefixtures("mock_get_user_info_and_be_project_owner")
def test_users_user_id_settings_payment_infos_put(
    client: testclient.TestClient, user_id_str, mock_hvac
):
    """Test case for users_user_id_settings_payment_infos_put

    Updates payment info
    """
    add_payment_info = {
        "address": "test address",
        "paymentMethod": "test method",
        "paymentDetails": "test details",
    }

    response = client.put(
        f"/users/{user_id_str}/settings/paymentInfos",
        headers=HEADERS,
        json=add_payment_info,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(f"/users/{user_id_str}/settings", headers=HEADERS)

    assert response.status_code == status.HTTP_200_OK, response.text
    assert response.json()["paymentInfo"] == add_payment_info


@pytest.mark.usefixtures("add_example_user")
@pytest.mark.usefixtures("mock_get_user_info_and_be_project_owner")
def test_users_user_id_settings_payment_infos_delete(
    client: testclient.TestClient, user_id_str, mock_hvac
):
    response = client.delete(
        f"/users/{user_id_str}/settings/paymentInfos",
        headers=HEADERS,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text

    response = client.get(f"/users/{user_id_str}/settings", headers=HEADERS)

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()["paymentInfo"]
    expected = {"address": None, "paymentDetails": None, "paymentMethod": None}

    assert result == expected


@pytest.mark.usefixtures("mock_impersonate_random_user")
def test_users_user_id_settings_payment_infos_put_while_other_user(
    client: testclient.TestClient,
    user_id_str,
):
    """Test case for users_user_id_settings_payment_infos_put

    Updates payment info
    """
    add_payment_info = {
        "address": "test address",
        "paymentMethod": "test method",
        "paymentDetails": "test details",
    }

    response = client.put(
        f"/users/{user_id_str}/settings/paymentInfos",
        headers=HEADERS,
        json=add_payment_info,
    )

    assert response.status_code == status.HTTP_401_UNAUTHORIZED, response.text
    assert response.json()["detail"] == UNAUTHORIZED_RESPONSE_MESSAGE
