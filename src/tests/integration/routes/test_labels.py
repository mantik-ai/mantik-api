import uuid

import fastapi.testclient as testclient
import pytest
import starlette.status as status

import mantik_api.database as database
import mantik_api.models as models


@pytest.fixture
def api_label_3(created_at) -> models.label.Label:
    return models.label.Label(
        label_id=str(uuid.uuid4()),
        scopes=["data"],
        category="Domain",
        name="test-name-2",
        created_at=created_at,
    )


@pytest.fixture(autouse=True)
def add_three_example_labels(
    clear_and_init_database,
    database_client,
    api_label_3,
    database_label_1,
    database_label_2,
) -> None:
    database_label_3 = database.label.Label(
        id=api_label_3.label_id,
        identifier="0002",
        scopes=api_label_3.scopes,
        category=api_label_3.category,
        name=api_label_3.name,
        created_at=api_label_3.created_at,
    )
    with database_client.session():
        database_client.add(database_label_1)
        database_client.add(database_label_2)
        database_client.add(database_label_3)


@pytest.mark.parametrize(
    ("scope", "expected_status_code", "expected"),
    [
        (
            None,
            200,
            {
                "totalRecords": 3,
                "pageRecords": 3,
                "labels": ["api_label_1", "api_label_2", "api_label_3"],
            },
        ),
        (
            "invalid",
            422,
            {
                "detail": [
                    {
                        "ctx": {
                            "enum_values": ["project", "code", "data", "experiment"]
                        },
                        "loc": ["query", "scope"],
                        "msg": "value is not a valid enumeration member; permitted: 'project', 'code', 'data', 'experiment'",  # noqa: E501
                        "type": "type_error.enum",
                    }
                ]
            },
        ),
        (
            "project",
            200,
            {
                "totalRecords": 2,
                "pageRecords": 2,
                "labels": ["api_label_1", "api_label_2"],
            },
        ),
        (
            "code",
            200,
            {
                "totalRecords": 0,
                "pageRecords": 0,
                "labels": [],
            },
        ),
    ],
)
def test_labels_get(
    request, client: testclient.TestClient, scope, expected_status_code, expected
):
    """Test case for labels_get

    Get all labels
    """
    if "labels" in expected:
        expected["labels"] = [
            request.getfixturevalue(exp).dict() for exp in expected["labels"]
        ]

    params = {"scope": scope} if scope is not None else None

    response = client.get("/labels", params=params)

    assert response.status_code == expected_status_code, response.text

    result = response.json()
    assert result == expected


def test_labels_label_id_get(client: testclient.TestClient, api_label_3):
    """Test case for labels_label_id_get

    Get Label information
    """
    expected = api_label_3.dict()

    response = client.get(f"/labels/{api_label_3.label_id}")

    assert response.status_code == status.HTTP_200_OK, response.text

    result = response.json()

    assert result == expected
