import uuid

import fastapi.testclient as testclient
import pytest
import starlette.status as status

import mantik_api.database as database
import mantik_api.testing as testing


@pytest.fixture()
def add_private_project(
    clear_and_init_database,
    add_database_users,
    database_client,
    second_database_user,
    user_group_id_str,
) -> uuid.UUID:
    # JWT sent with request contains user ID of first database user, hence
    # we make second database user the owner here.
    id_ = uuid.uuid4()
    project = database.project.Project(
        id=id_,
        name="test_project_name",
        executive_summary="test_executive_summary",
        detailed_description="test_detailed_description",
        owner=second_database_user,
        public=False,
    )
    with database_client.session():
        database_client.add(project)
    return id_


@pytest.mark.usefixtures("add_private_project")
@pytest.mark.usefixtures("mock_get_user_info_and_be_project_owner")
def test_projects_get_no_projects(client: testclient.TestClient):
    """Gets no projects since all private and not owned by user."""
    response = client.get(
        "/projects",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    json_response = response.json()
    assert json_response["totalRecords"] == 0


ROLES = [
    database.project.ProjectRole.GUEST,
    database.project.ProjectRole.REPORTER,
    database.project.ProjectRole.RESEARCHER,
    database.project.ProjectRole.MAINTAINER,
    database.project.ProjectRole.OWNER,
]


@pytest.fixture()
def add_unassociated_project(
    database_client,
    second_database_user,
) -> None:
    """Add a project not associated with the user.

    It should never appear in the response.

    """
    project = database.project.Project(
        id=uuid.uuid4(),
        name="test_project_name",
        executive_summary="test_executive_summary",
        detailed_description="test_detailed_description",
        owner=second_database_user,
        public=False,
    )

    with database_client.session():
        database_client.add(project)


@pytest.fixture(params=ROLES)
def add_private_project_with_associated_user_group_owned(
    request,
    add_database_users,
    add_database_user_group,
    add_private_project,
    add_unassociated_project,
    database_client,
    database_user_group,
    database_user,
    second_database_user,
    user_group_id_str,
) -> None:
    association = database.project.UserGroupProjectAssociationTable(
        role=request.param,
        project_id=add_private_project,
        user_group_id=user_group_id_str,
    )

    with database_client.session():
        database_client.add(association)


@pytest.mark.usefixtures("add_private_project_with_associated_user_group_owned")
@pytest.mark.usefixtures("mock_get_user_info_and_be_project_owner")
def test_projects_get_all_private_projects_with_user_groups_owned(
    client: testclient.TestClient,
):
    """Get project since user is in a project that was shared with their group."""
    response = client.get(
        "/projects",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    json_response = response.json()
    assert json_response["totalRecords"] == 1


@pytest.fixture(params=ROLES)
def add_private_project_with_associated_user_group_not_owned(
    request,
    add_database_users,
    add_database_user_group,
    add_private_project,
    add_unassociated_project,
    database_client,
    database_user_group,
    database_user,
    second_database_user,
    user_group_id_str,
) -> None:
    group = database.user_group.UserGroup(
        id=uuid.uuid4(),
        name="test-name",
        admin=second_database_user,
        members=[second_database_user],
    )
    association = database.project.UserGroupProjectAssociationTable(
        role=request.param,
        project_id=add_private_project,
        user_group_id=user_group_id_str,
    )

    with database_client.session():
        database_client.add(group)
        database_client.commit()
        database_client.add(association)


@pytest.mark.usefixtures("add_private_project_with_associated_user_group_not_owned")
@pytest.mark.usefixtures("mock_get_user_info_and_be_project_owner")
def test_projects_get_all_private_projects_with_user_groups_not_owned(
    client: testclient.TestClient,
):
    """Get project since user is in a project that was shared with their group."""
    response = client.get(
        "/projects",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    json_response = response.json()
    assert json_response["totalRecords"] == 1


@pytest.fixture(params=ROLES)
def add_private_project_with_associated_organization_owned(
    request,
    add_database_users,
    add_example_organization,
    add_private_project,
    add_unassociated_project,
    database_client,
    second_database_user,
    organization_id_str,
) -> None:
    association = database.project.OrganizationProjectAssociationTable(
        role=request.param,
        project_id=add_private_project,
        organization_id=organization_id_str,
    )

    with database_client.session():
        database_client.add(association)


@pytest.mark.usefixtures("add_private_project_with_associated_organization_owned")
@pytest.mark.usefixtures("mock_get_user_info_and_be_project_owner")
def test_projects_get_all_private_projects_with_user_organization_owned(
    client: testclient.TestClient,
):
    """Get project since user is in a project that was shared with their group."""
    response = client.get(
        "/projects",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    json_response = response.json()
    assert json_response["totalRecords"] == 1


@pytest.fixture(params=ROLES)
def add_private_project_with_associated_organization_not_owned_via_group(
    request,
    add_database_users,
    add_private_project,
    add_unassociated_project,
    database_client,
    database_user,
    second_database_user,
) -> None:
    group = database.user_group.UserGroup(
        id=uuid.uuid4(),
        name="test-name",
        admin=second_database_user,
        members=[database_user, second_database_user],
    )
    organization_id = uuid.uuid4()
    organization = database.organization.Organization(
        id=organization_id,
        name="foo",
        contact=second_database_user,
        groups=[group],
    )
    association = database.project.OrganizationProjectAssociationTable(
        role=request.param,
        project_id=add_private_project,
        organization_id=organization_id,
    )

    with database_client.session():
        database_client.add(group)
        database_client.add(organization)
        database_client.commit()
        database_client.add(association)


@pytest.mark.usefixtures(
    "add_private_project_with_associated_organization_not_owned_via_group"
)
@pytest.mark.usefixtures("mock_get_user_info_and_be_project_owner")
def test_projects_get_all_private_projects_with_user_organization_not_owned_via_group(
    client: testclient.TestClient,
):
    """Get project since user is in a project that was shared with an organization
    that their user group belongs to.

    """
    response = client.get(
        "/projects",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    json_response = response.json()
    assert json_response["totalRecords"] == 1


@pytest.fixture(params=ROLES)
def add_private_project_with_associated_organization_not_owned_via_member(
    request,
    add_database_users,
    add_private_project,
    add_unassociated_project,
    database_client,
    database_user,
    second_database_user,
) -> None:
    group = database.user_group.UserGroup(
        id=uuid.uuid4(),
        name="test-name",
        admin=second_database_user,
        members=[second_database_user],
    )
    organization_id = uuid.uuid4()
    organization = database.organization.Organization(
        id=organization_id,
        name="foo",
        contact=second_database_user,
        groups=[group],
        members=[database_user],
    )
    association = database.project.OrganizationProjectAssociationTable(
        role=request.param,
        project_id=add_private_project,
        organization_id=organization_id,
    )

    with database_client.session():
        database_client.add(group)
        database_client.add(organization)
        database_client.commit()
        database_client.add(association)


@pytest.mark.usefixtures(
    "add_private_project_with_associated_organization_not_owned_via_member"
)
@pytest.mark.usefixtures("mock_get_user_info_and_be_project_owner")
def test_projects_get_all_private_projects_with_user_organization_not_owned_via_member(
    client: testclient.TestClient,
):
    """Get project since user is in a project that was shared with an organization
    they are a member of.

    """
    response = client.get(
        "/projects",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
    )
    assert response.status_code == status.HTTP_200_OK, response.text

    json_response = response.json()
    assert json_response["totalRecords"] == 1
