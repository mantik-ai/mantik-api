import fastapi.testclient as testclient
import pytest
import starlette.status as status

import mantik_api.aws.cognito as cognito
import mantik_api.testing as testing


@pytest.mark.parametrize(
    ("data", "expected_status_code"),
    [
        (
            {
                "userName": "preferred_name",
            },
            status.HTTP_200_OK,
        ),
        (
            {
                "email": "email",
            },
            status.HTTP_200_OK,
        ),
    ],
    ids=[
        "resend confirmation code using user name",
        "resend confirmation code using email",
    ],
)
def test_signup_resend_confirmation_code_post(
    client: testclient.TestClient,
    add_example_user,
    database_user,
    api_user,
    data,
    expected_status_code,
    cognito_patcher,
    moto_user_pool,
):
    # For the requests that are expected to succeed,
    # get the username or email from the fixture.
    data = {key: getattr(database_user, value) for key, value in data.items()}

    cognito_patcher.patch_resend_confirmation_code(
        username=testing.aws.cognito.COGNITO_USERNAME
    )

    response = client.post(
        "/signup/resend-confirmation-code",
        json=data,
    )

    assert response.status_code == expected_status_code, response.text

    assert response.json()["userName"] == api_user.name, "Incorrect user name returned"


@pytest.mark.parametrize(
    ("data", "expected_status_code", "expected_message"),
    [
        (
            {
                "userName": "doesnt-exist",
            },
            status.HTTP_404_NOT_FOUND,
            """User with name doesnt-exist not found""",
        ),
        (
            {
                "email": "doesnt-exist",
            },
            status.HTTP_404_NOT_FOUND,
            """User with email doesnt-exist not found""",
        ),
        (
            {
                "userName": "cannot-provide-both",
                "email": "cannot-provide-both",
            },
            status.HTTP_422_UNPROCESSABLE_ENTITY,
            "Either user name or email must be provided, not both",
        ),
        (
            {},
            status.HTTP_422_UNPROCESSABLE_ENTITY,
            "Either user name or email must be provided",
        ),
    ],
    ids=[
        "user doesn't exist",
        "user with email doesn't exist",
        "user name and email cannot be both provided",
        "either user name or email have to be provided",
    ],
)
def test_signup_resend_confirmation_code_post_fails(
    client: testclient.TestClient,
    add_example_user,
    api_user,
    data,
    expected_status_code,
    expected_message,
    cognito_patcher,
    moto_user_pool,
):
    response = client.post(
        "/signup/resend-confirmation-code",
        json=data,
    )

    assert response.status_code == expected_status_code, response.text
    assert expected_message in response.text, "Incorrect error message in response"


def test_signup_verify_email_post(
    client: testclient.TestClient,
    moto_user_pool,
    add_example_user,
    create_fake_cognito_user,
    api_user,
):
    data = {
        "confirmationCode": "123456",
        "userName": api_user.name,
    }

    response = client.post(
        "/signup/verify-email",
        json=data,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT, response.text


@pytest.mark.parametrize(
    ("username", "confirmation_code", "expected_status", "expected_message"),
    [
        (
            "doesnt-exist",
            "123456",
            status.HTTP_404_NOT_FOUND,
            "User 'doesnt-exist' not found",
        ),
        # The given code here doesn't affect the patched behavior of Cognito.
        # The behavior is defined in the test below by calling
        # `cognito_patcher.patch_confirm_sign_up_raises_error()`.
        (
            "exists",
            "incorrect-code",
            status.HTTP_422_UNPROCESSABLE_ENTITY,
            "Incorrect confirmation code",
        ),
        (
            "exists",
            "incorrect-code",
            status.HTTP_424_FAILED_DEPENDENCY,
            "Cognito error message",
        ),
    ],
    ids=[
        "given username doesn't exist",
        "incorrect confirmation code",
        "cognito raises any other error message",
    ],
)
def test_signup_verify_email_post_fails(
    client: testclient.TestClient,
    add_example_user,
    database_user,
    api_user,
    cognito_patcher,
    username,
    confirmation_code,
    expected_status,
    expected_message,
):
    data = {
        "confirmationCode": confirmation_code,
        "userName": api_user.name if username == "exists" else username,
    }

    secret_hash = cognito.client.create_secret_hash(username=database_user.cognito_name)

    if expected_status != status.HTTP_404_NOT_FOUND:
        if expected_status == status.HTTP_422_UNPROCESSABLE_ENTITY:
            # Define error code for incorrect confirmation code
            error_code = "CodeMismatchException"
            error_message = "Incorrect confirmation code"
        elif expected_status == status.HTTP_424_FAILED_DEPENDENCY:
            # Define error code of some other exception that
            # is caught in the general except block of
            # `mantik_api.aws.cognito.signup.verify_email()`
            error_code = "ResourceNotFoundException"
            error_message = "Cognito error message"
        else:
            raise RuntimeError(
                f"Given response status {expected_status} doesn't have a "
                f"patched Cognito error response"
            )

        cognito_patcher.patch_confirm_sign_up_raises_error(
            username=database_user.cognito_name,
            confirmation_code=confirmation_code,
            error_code=error_code,
            error_message=error_message,
            secret_hash=secret_hash,
        )

    response = client.post(
        "/signup/verify-email",
        json=data,
    )

    assert response.status_code == expected_status, response.text
    result = response.json()["detail"]
    assert expected_message in result, "Incorrect error response message"
