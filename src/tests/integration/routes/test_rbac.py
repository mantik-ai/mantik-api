import uuid

import pytest

import mantik_api.database as database
import mantik_api.routes.rbac._project as _project


@pytest.fixture
def user_rbac_id() -> uuid.UUID:
    return uuid.UUID("a154170b-43b4-458e-9b24-0d1679b1c68a")


@pytest.fixture
def user_rbac(user_rbac_id):
    return database.user.User(
        id=user_rbac_id,
        cognito_name="cognito_rbac_user",
        preferred_name="rbac_user",
        email="email_rbac_user",
    )


@pytest.fixture
def add_user_rbac(database_client, clear_and_init_database, user_rbac) -> None:
    with database_client.session():
        database_client.add(user_rbac)


@pytest.fixture
def add_user_role_to_project(
    database_client,
    clear_and_init_database,
    add_example_project,
    add_user_rbac,
    user_rbac_id,
    project_id_str,
) -> None:
    with database_client.session():
        database_client.add(
            database.project.UserProjectAssociationTable(
                role=database.project.ProjectRole.MAINTAINER,
                user_id=user_rbac_id,
                project_id=uuid.UUID(project_id_str),
            )
        )


@pytest.fixture
def add_group_role_to_project_with_user_as_admin(
    database_client,
    clear_and_init_database,
    add_example_project,
    add_user_rbac,
    user_rbac_id,
    user_rbac,
    project_id_str,
    database_user_group,
    user_group_id_str,
) -> None:
    database_user_group.admin = user_rbac
    with database_client.session():
        database_client.add(database_user_group)
        database_client.commit()
        database_client.add(
            database.project.UserGroupProjectAssociationTable(
                role=database.project.ProjectRole.REPORTER,
                user_group_id=uuid.UUID(user_group_id_str),
                project_id=uuid.UUID(project_id_str),
            )
        )


@pytest.fixture
def add_group_role_to_project_with_user_as_member(
    database_client,
    clear_and_init_database,
    add_example_project,
    add_user_rbac,
    user_rbac_id,
    user_rbac,
    project_id_str,
    database_user_group,
    user_group_id_str,
) -> None:
    database_user_group.members = [user_rbac]
    with database_client.session():
        database_client.add(database_user_group)
        database_client.commit()
        database_client.add(
            database.project.UserGroupProjectAssociationTable(
                role=database.project.ProjectRole.REPORTER,
                user_group_id=uuid.UUID(user_group_id_str),
                project_id=uuid.UUID(project_id_str),
            )
        )


@pytest.fixture
def add_organization_role_to_project_as_contact(
    database_client,
    clear_and_init_database,
    add_example_project,
    add_user_rbac,
    user_rbac,
    database_user_group,
    database_organization,
    project_id_str,
    organization_id_str,
) -> None:
    database_organization.contact = user_rbac
    with database_client.session():
        database_client.add(database_user_group)
        database_client.add(database_organization)
        database_client.commit()
        database_client.add(
            database.project.OrganizationProjectAssociationTable(
                role=database.project.ProjectRole.RESEARCHER,
                organization_id=uuid.UUID(organization_id_str),
                project_id=uuid.UUID(project_id_str),
            )
        )


@pytest.fixture
def add_organization_role_to_project_as_member(
    database_client,
    clear_and_init_database,
    add_example_project,
    add_user_rbac,
    user_rbac,
    database_user_group,
    database_organization,
    project_id_str,
    organization_id_str,
) -> None:
    database_organization.members = [user_rbac]
    with database_client.session():
        database_client.add(database_user_group)
        database_client.add(database_organization)
        database_client.commit()
        database_client.add(
            database.project.OrganizationProjectAssociationTable(
                role=database.project.ProjectRole.RESEARCHER,
                organization_id=uuid.UUID(organization_id_str),
                project_id=uuid.UUID(project_id_str),
            )
        )


@pytest.fixture
def add_organization_role_to_project_as_group_member(
    database_client,
    clear_and_init_database,
    add_example_project,
    add_user_rbac,
    user_rbac,
    database_user_group,
    database_organization,
    project_id_str,
    organization_id_str,
) -> None:
    database_user_group.members = [user_rbac]
    database_organization.groups = [database_user_group]

    with database_client.session():
        database_client.add(database_user_group)
        database_client.add(database_organization)
        database_client.commit()
        database_client.add(
            database.project.OrganizationProjectAssociationTable(
                role=database.project.ProjectRole.RESEARCHER,
                organization_id=uuid.UUID(organization_id_str),
                project_id=uuid.UUID(project_id_str),
            )
        )


@pytest.fixture
def add_extra_organization_and_group(
    database_client,
    clear_and_init_database,
    add_example_project,
    project_id_str,
    database_user,
) -> None:
    group_id = uuid.uuid4()
    organization_id = uuid.uuid4()
    with database_client.session():
        database_client.add(
            database.user_group.UserGroup(
                id=group_id, name="member_less_group", admin=database_user
            )
        )
        database_client.add(
            database.organization.Organization(
                id=organization_id,
                name="member_less_organization",
                contact=database_user,
            )
        )
        database_client.commit()
        database_client.add(
            database.project.UserGroupProjectAssociationTable(
                role=database.project.ProjectRole.REPORTER,
                user_group_id=group_id,
                project_id=uuid.UUID(project_id_str),
            )
        )
        database_client.add(
            database.project.OrganizationProjectAssociationTable(
                role=database.project.ProjectRole.RESEARCHER,
                organization_id=organization_id,
                project_id=uuid.UUID(project_id_str),
            )
        )


@pytest.mark.usefixtures("add_example_project")
def test_query_project_role_when_user_is_project_owner(
    database_client, user_id_str, project_id_str
):
    with database_client.session():
        project = _project.get_project_with_roles(
            project_id_str,
            client=database_client,
        )
        result = _project.get_user_role_in_project(
            project=project,
            user_id=uuid.UUID(user_id_str),
            client=database_client,
        ).role.name
    assert result == "OWNER"


@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_user_rbac")
@pytest.mark.usefixtures("add_user_role_to_project")
def test_query_project_role_when_user_has_a_maintainer_role(
    database_client, user_rbac_id, project_id_str
):
    with database_client.session():
        project = _project.get_project_with_roles(
            project_id_str,
            client=database_client,
        )
        result = _project.get_user_role_in_project(
            project=project, user_id=user_rbac_id, client=database_client
        ).role.name
    assert result == "MAINTAINER"


@pytest.mark.usefixtures("add_user_rbac")
@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_extra_organization_and_group")
def test_query_project_role_when_user_has_no_role(
    database_client, user_rbac_id, project_id_str
):
    with database_client.session():
        project = _project.get_project_with_roles(
            project_id_str,
            client=database_client,
        )
        result = _project.get_user_role_in_project(
            project=project, user_id=user_rbac_id, client=database_client
        ).role.name
    assert result == "NO_ROLE"


@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_user_rbac")
@pytest.mark.usefixtures("add_group_role_to_project_with_user_as_admin")
def test_query_project_role_when_user_has_group_reporter_role_as_admin(
    database_client, user_rbac_id, project_id_str
):
    with database_client.session():
        project = _project.get_project_with_roles(
            project_id_str,
            client=database_client,
        )
        result = _project.get_user_role_in_project(
            project=project, user_id=user_rbac_id, client=database_client
        ).role.name
    assert result == "REPORTER"


@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_user_rbac")
@pytest.mark.usefixtures("add_group_role_to_project_with_user_as_member")
def test_query_project_role_when_user_has_group_reporter_role_as_member(
    database_client, user_rbac_id, project_id_str
):
    with database_client.session():
        project = _project.get_project_with_roles(
            project_id_str,
            client=database_client,
        )
        result = _project.get_user_role_in_project(
            project=project, user_id=user_rbac_id, client=database_client
        ).role.name
    assert result == "REPORTER"


@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_user_rbac")
@pytest.mark.usefixtures("add_organization_role_to_project_as_contact")
def test_query_project_role_when_user_has_organization_role_as_contact(
    database_client, user_rbac_id, project_id_str
):
    with database_client.session():
        project = _project.get_project_with_roles(
            project_id_str,
            client=database_client,
        )
        result = _project.get_user_role_in_project(
            project=project, user_id=user_rbac_id, client=database_client
        ).role.name
    assert result == "RESEARCHER"


@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_user_rbac")
@pytest.mark.usefixtures("add_organization_role_to_project_as_member")
def test_query_project_role_when_user_has_organization_role_as_member(
    database_client, user_rbac_id, project_id_str
):
    with database_client.session():
        project = _project.get_project_with_roles(
            project_id_str,
            client=database_client,
        )
        result = _project.get_user_role_in_project(
            project=project, user_id=user_rbac_id, client=database_client
        ).role.name
    assert result == "RESEARCHER"


@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_user_rbac")
@pytest.mark.usefixtures("add_organization_role_to_project_as_group_member")
def test_query_project_role_when_user_has_organization_role_as_group_member(
    database_client, user_rbac_id, project_id_str
):
    with database_client.session():
        project = _project.get_project_with_roles(
            project_id_str,
            client=database_client,
        )
        result = _project.get_user_role_in_project(
            project=project, user_id=user_rbac_id, client=database_client
        ).role.name
    assert result == "RESEARCHER"


@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_user_rbac")
@pytest.mark.usefixtures("add_group_role_to_project_with_user_as_member")
@pytest.mark.usefixtures("add_organization_role_to_project_as_member")
def test_query_project_role_when_user_is_added_twice(
    database_client, user_rbac_id, project_id_str, organization_id_str
):
    """
    Given that a user was added to a project as a part of a group
    and added to the same project as part of n organization
    and the roles are different for both
    then the user role in the project is the higher role of the two
    """
    with database_client.session():
        project = _project.get_project_with_roles(
            project_id_str,
            client=database_client,
        )
        result = _project.get_user_role_in_project(
            project=project, user_id=user_rbac_id, client=database_client
        ).role.name
    assert result == "RESEARCHER"


@pytest.mark.usefixtures("add_example_project")
@pytest.mark.usefixtures("add_user_rbac")
@pytest.mark.usefixtures("add_group_role_to_project_with_user_as_member")
@pytest.mark.usefixtures("add_organization_role_to_project_as_member")
def test_query_project_role_when_user_is_added_twice_and_removed_once(
    database_client, user_rbac_id, project_id_str, organization_id_str
):
    """
    Given that a user was added to a project as a part of a group
    and added to the same project as part of an organization
    and the roles are different for both
    when the organization is removed from project
    then the user role in the project is the one from the group
    """

    # Remove organization from project
    with database_client.session():
        organization_id = uuid.UUID(organization_id_str)
        project_id = uuid.UUID(project_id_str)

        database_client.delete(
            database.project.OrganizationProjectAssociationTable,
            constraints={
                database.project.OrganizationProjectAssociationTable.organization_id: organization_id,  # noqa: E501
                database.project.OrganizationProjectAssociationTable.project_id: project_id,  # noqa: E501
            },
        )

    with database_client.session():
        project = _project.get_project_with_roles(
            project_id_str,
            client=database_client,
        )
        result = _project.get_user_role_in_project(
            project=project, user_id=user_rbac_id, client=database_client
        ).role.name

    assert result == "REPORTER"
