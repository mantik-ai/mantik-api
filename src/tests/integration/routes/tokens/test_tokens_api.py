import datetime

import pytest
import requests
import starlette.status as status
from fastapi import testclient

import mantik_api.testing as testing


HEADERS = testing.token_verifier.VALID_AUTHORIZATION_HEADER


@pytest.mark.parametrize(
    (
        "data",
        "cognito_response",
        "expected_status_code",
        "expected_payload",
        "expected_message",
    ),
    [
        (
            {
                "username": testing.aws.cognito.USERNAME,
                "password": "test-password",
            },
            "cognito_auth_response",
            status.HTTP_201_CREATED,
            {
                "AccessToken": "test-access-token",
                "RefreshToken": "test-refresh-token",
                "ExpiresAt": datetime.datetime(
                    2022, 5, 24, 9, 40, 19, tzinfo=datetime.UTC
                ).isoformat(),
            },
            None,
        ),
        (
            {
                "username": testing.aws.cognito.EMAIL,
                "password": "test-password",
            },
            "cognito_auth_response",
            status.HTTP_201_CREATED,
            {
                "AccessToken": "test-access-token",
                "RefreshToken": "test-refresh-token",
                "ExpiresAt": datetime.datetime(
                    2022, 5, 24, 9, 40, 19, tzinfo=datetime.UTC
                ).isoformat(),
            },
            None,
        ),
        (
            {
                "username": testing.aws.cognito.USERNAME,
                "password": "test-password",
            },
            "cognito_user_not_found_response",
            status.HTTP_401_UNAUTHORIZED,
            None,
            "user does not exist",
        ),
        (
            {
                "username": testing.aws.cognito.USERNAME,
                "password": "test-password",
            },
            "cognito_incorrect_login_credentials_response",
            status.HTTP_401_UNAUTHORIZED,
            None,
            "incorrect username or password",
        ),
        (
            {
                "username": testing.aws.cognito.USERNAME,
                "password": "test-password",
            },
            "cognito_user_not_confirmed_response",
            status.HTTP_403_FORBIDDEN,
            None,
            "user is not confirmed",
        ),
    ],
    ids=[
        "login with username",
        "login with email",
        "user not found by Cognito",
        "Incorrect password",
        "User not confirmed",
    ],
)
def test_create_token(
    monkeypatch,
    client: testclient.TestClient,
    request,
    add_example_user,
    cognito_patcher,
    data,
    cognito_response,
    expected_status_code,
    expected_payload,
    expected_message,
):
    fake_response = request.getfixturevalue(cognito_response)
    (
        error_code,
        error_message,
    ) = testing.aws.cognito.get_error_code_and_message_from_json(fake_response)

    cognito_patcher.patch_initiate_auth_get_token(
        password=data["password"],
        response=fake_response,
        error_code=error_code,
        error_message=error_message,
    )

    response = client.post("/mantik/tokens/create", json=data)

    _expect_correct_response(
        response=response,
        expected_status_code=expected_status_code,
        expected_payload=expected_payload,
        expected_message=expected_message,
    )


@pytest.mark.parametrize(
    (
        "cognito_response",
        "expected_status_code",
        "expected_payload",
        "expected_message",
    ),
    [
        (
            "cognito_refresh_response",
            status.HTTP_201_CREATED,
            {
                "AccessToken": "test-refreshed-access-token",
                "ExpiresAt": datetime.datetime(
                    2022, 6, 7, 14, 21, 6, tzinfo=datetime.UTC
                ).isoformat(),
            },
            None,
        ),
        (
            "cognito_refresh_token_expired_response",
            status.HTTP_401_UNAUTHORIZED,
            None,
            "refresh token has expired",
        ),
        (
            "cognito_refresh_token_invalid_response",
            status.HTTP_401_UNAUTHORIZED,
            None,
            "refresh token is invalid",
        ),
        (
            "cognito_different_client_response",
            status.HTTP_401_UNAUTHORIZED,
            None,
            "refresh token is invalid",
        ),
    ],
)
def test_refresh_token(
    monkeypatch,
    client,
    request,
    add_example_user,
    cognito_patcher,
    cognito_response,
    expected_status_code,
    expected_payload,
    expected_message,
):
    fake_response = request.getfixturevalue(cognito_response)
    (
        error_code,
        error_message,
    ) = testing.aws.cognito.get_error_code_and_message_from_json(fake_response)

    data = {
        "refresh_token": "test-refresh-token",
        "username": "test-name",
    }

    cognito_patcher.patch_initiate_auth_refresh_token(
        refresh_token=data["refresh_token"],
        response=fake_response,
        error_code=error_code,
        error_message=error_message,
    )

    response = client.post("/mantik/tokens/refresh", json=data)

    _expect_correct_response(
        response=response,
        expected_status_code=expected_status_code,
        expected_payload=expected_payload,
        expected_message=expected_message,
    )


def _expect_correct_response(
    response: requests.Response,
    expected_status_code: int,
    expected_payload: dict | None,
    expected_message: str | None,
) -> None:
    assert response.status_code == expected_status_code, response.text
    if expected_payload is not None:
        assert response.json() == expected_payload
    if expected_message is not None:
        assert expected_message.lower() in response.text.lower()
