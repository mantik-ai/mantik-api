import uuid

import fastapi
import fastapi.testclient as testclient
import pytest
import starlette.status as status

import mantik_api.testing as testing


ROUTES = [
    "/project/{projectId}",
    "/users/{userId}",
    "/groups/{groupId}",
    "/organizations/{organizationId}",
    "/projects/{projectId}/code/{codeRepositoryId}",
    "/projects/{projectId}/data/{dataRepositoryId}",
    "/projects/{projectId}/deployments/{deploymentId}",
    "/projects/{projectId}/experiments/{experimentRepositoryId}",
    "/projects/{projectId}/groups/{groupId}",
    "/projects/{projectId}/members/{userId}",
    "/projects/{projectId}/models/{modelRepositoryId}",
    "/projects/{projectId}/organizations/{organizationId}",
    "/projects/{projectId}/runs/{runId}",
    "/projects/{projectId}/run-schedules/{runScheduleId}",
]


@pytest.mark.usefixtures("clear_and_init_database")
def test_get_routes_return_404(client: testclient.TestClient, second_user_id_str):
    """All GET routes should return 404 if a resource doesn't exist in the database."""

    for route in client.app.routes:
        if not _endpoint_allows_get(route) or route.path not in ROUTES:
            continue

        response = client.get(
            _create_route_path_with_random_uuids(route),
            headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        )

        assert response.status_code == status.HTTP_404_NOT_FOUND, response.text


def _endpoint_allows_get(route: fastapi.routing.APIRoute) -> bool:
    """Return whether the route returns a collective get.

    Collective get here means e.g. all users, which could be an emtpy 200 response.
    Opposite would be to try get an individual resource, e.g. a single user.

    """
    return "GET" in route.methods


def _create_route_path_with_random_uuids(route: fastapi.routing.APIRoute) -> str:
    params = {param: uuid.uuid4() for param in route.param_convertors}
    return route.path.format(**params)
