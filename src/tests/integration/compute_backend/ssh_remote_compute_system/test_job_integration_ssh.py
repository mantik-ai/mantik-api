import pathlib
import unittest.mock

import pytest

import mantik_api.compute_backend.ssh_remote_compute_system.job as job


@pytest.fixture(scope="function")
def fake_ssh_client():
    return unittest.mock.Mock()


@pytest.fixture
def fake_job(fake_ssh_client):
    return job.Job(job_id="123", ssh_client=fake_ssh_client)


def test_get_status(fake_ssh_client, fake_job):
    """
    When I use the `get_status` function of an ssh Job
    Then the attached ssh client's `get_job_status` function gets called
    And  the correct parameters are passed
    """
    fake_job.get_status()
    fake_ssh_client.get_job_status.assert_called_with(job_id=fake_job.id)


def test_cancel(fake_ssh_client, fake_job):
    """
    When I use the `cancel` function of an ssh Job
    Then the attached ssh client's `cancel_job` function gets called
    And  the correct parameters are passed
    """
    fake_job.cancel()
    fake_ssh_client.cancel_job.assert_called_with(job_id=fake_job.id)


def test_job_dir(fake_ssh_client, fake_job):
    """
    When I fetch the job's job directory
    Then I receive the path of the job
    """
    job_dir = fake_job.job_dir
    assert job_dir == pathlib.Path(f"mantik-runs/{fake_job.id}")


def test_get_logs(fake_ssh_client, fake_job):
    """
    When I use the `get_logs` function of an ssh Job
    Then the attached ssh client's `get_job_logs` function gets called
    And  the correct parameters are passed
    """

    fake_job.get_logs()
    fake_ssh_client.get_job_logs.assert_called_with(
        log_file_path=fake_job.log_file_path
    )


def test_download(fake_ssh_client, fake_job):
    fake_job.download(path=pathlib.Path("sample.file"))
    fake_ssh_client.download_from_path.assert_called_with(
        path=fake_job.job_dir / pathlib.Path("sample.file")
    )


def test_get_info(fake_ssh_client, fake_job):
    """
    When I use the `get_info` function of an ssh Job
    Then the attached ssh client's `get_job_info` function gets called
    And  the correct parameters are passed
    """

    fake_job.get_info()
    fake_ssh_client.get_job_info.assert_called_with(job_id=fake_job.id)
