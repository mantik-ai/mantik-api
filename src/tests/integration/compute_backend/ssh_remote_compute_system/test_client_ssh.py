import io
import pathlib
import unittest.mock
import zipfile

import paramiko
import pytest

import mantik_api.compute_backend.ssh_remote_compute_system.client as client
import mantik_api.compute_backend.ssh_remote_compute_system.exceptions as exceptions
import mantik_api.models


@pytest.fixture(scope="function")
def fake_ssh_client():
    return unittest.mock.Mock()


def _byte_stream(value: str) -> io.BytesIO:
    return io.BytesIO(value.encode("utf-8"))


@pytest.fixture
def fake_job_id():
    return "123"


def test_get_job_status_from_squeue(fake_job_id, fake_ssh_client) -> None:
    """
    Given there is a status for a job
    When  I request it using squeue
    Then  I get the status
    """

    fake_stdout = _byte_stream("RUNNING")
    fake_stderr = _byte_stream("")

    fake_ssh_client.exec_command.return_value = (
        unittest.mock.Mock(),
        fake_stdout,
        fake_stderr,
    )

    status = client.get_job_status_from_squeue(
        job_id=fake_job_id, connected_ssh_client=fake_ssh_client
    )
    assert status == "RUNNING"


def test_get_job_status_from_squeue_fails(fake_job_id, fake_ssh_client) -> None:
    """
    Given there is an error when retrieving status using squeue
    When  I request it using squeue
    Then  I get an error
    """

    with pytest.raises(
        exceptions.JobInformationException,
        match=f"Error executing `squeue -j {fake_job_id} --format=%T -h`. "
        f"output: ``. error: `some error`",
    ):
        fake_stdout = _byte_stream("")
        fake_stderr = _byte_stream("some error")

        fake_ssh_client.exec_command.return_value = (
            unittest.mock.Mock(),
            fake_stdout,
            fake_stderr,
        )

        client.get_job_status_from_squeue(
            job_id=fake_job_id, connected_ssh_client=fake_ssh_client
        )


def test_get_job_status_from_sacct(fake_job_id, fake_ssh_client) -> None:
    """
    Given there is a status for a job
    When  I request it using sacct
    Then  I get the status
    """

    fake_stdout = _byte_stream("COMPLETED\nCOMPLETED")
    fake_stderr = _byte_stream("")

    fake_ssh_client.exec_command.return_value = (
        unittest.mock.Mock(),
        fake_stdout,
        fake_stderr,
    )

    status = client.get_job_status_from_sacct(
        job_id=fake_job_id, connected_ssh_client=fake_ssh_client
    )
    assert status == "COMPLETED"


def test_get_job_status_from_sacct_fails(fake_job_id, fake_ssh_client) -> None:
    """
    Given there is an error when retrieving status
    When  I request it using sacct
    Then  I get an error
    """

    with pytest.raises(
        exceptions.JobInformationException,
        match=f"Error executing `sacct -j {fake_job_id} --format=State -P -n`. "
        f"output: ``. error: `some error`",
    ):
        fake_stdout = _byte_stream("")
        fake_stderr = _byte_stream("some error")

        fake_ssh_client.exec_command.return_value = (
            unittest.mock.Mock(),
            fake_stdout,
            fake_stderr,
        )

        client.get_job_status_from_sacct(
            job_id=fake_job_id, connected_ssh_client=fake_ssh_client
        )


@pytest.mark.parametrize(
    "job_output,expected_state",
    (
        ["COMPLETED\nFAILED", "COMPLETED"],
        ["COMPLETED", "COMPLETED"],
    ),
    ids=["two statues, return 1st", "one status"],
)
def test_parse_job_state_from_sacct(job_output, expected_state):
    assert client.parse_job_state_from_sacct(output=job_output) == expected_state


def test_get_job_status(fake_job_id, fake_ssh_client) -> None:
    """
    Given a job is not in the SQUEUE
    When  I request its status
    Then  it is retrieved from SLURM accounting (sacct)
    """

    with unittest.mock.patch(
        "mantik_api.compute_backend.ssh_remote_compute_system"
        ".client.get_job_status_from_squeue",
        side_effect=exceptions.JobInformationException,
    ):
        with unittest.mock.patch(
            "mantik_api.compute_backend.ssh_remote_compute_system"
            ".client.get_job_status_from_sacct",
            return_value="COMPLETED",
        ) as mock_sacct:
            status = client.get_job_status(
                job_id=fake_job_id, connected_ssh_client=fake_ssh_client
            )
            assert status == mantik_api.models.run_info.SSHSlurmJobStatus.COMPLETED
            mock_sacct.assert_called()


def test_cancel_job(fake_job_id, fake_ssh_client) -> None:
    client.cancel_job(job_id=fake_job_id, connected_ssh_client=fake_ssh_client)
    fake_ssh_client.exec_command.assert_called_with(f"scancel {fake_job_id}")


def test_get_job_logs(fake_job_id, fake_ssh_client) -> None:
    """
    Given a job has resulted in saved logs
    When  I request the logs
    Then  I receive them
    """
    fake_sftp_file = unittest.mock.MagicMock()
    fake_sftp_file.__enter__.return_value = (  # mock the `with ... as ...` function
        fake_sftp_file
    )
    fake_sftp_file.readlines.return_value = ["hello\n", "world"]

    fake_sftp = unittest.mock.Mock()
    fake_sftp.open.return_value = fake_sftp_file

    fake_ssh_client.open_sftp.return_value = fake_sftp

    fake_logs_file_path = pathlib.Path("home/user123/mantik.log")
    logs = client.get_job_logs(
        log_file_path=fake_logs_file_path, connected_ssh_client=fake_ssh_client
    )
    assert logs == ["hello\n", "world"]


def test_get_job_info(fake_job_id, fake_ssh_client) -> None:
    """
    Given a job has resulted in saved info
    When  I request the info
    Then  I receive it
    """
    fake_stdout = _byte_stream(
        f" {fake_job_id} | COMPLETED | <user> | <job-name> | "
        f"<node-list> | <n-nodes> | <partition> | <cpu-time> | "
        f"<elapsed> | <start> | <end> | <exit-code> | <work-dir> |"
        f"<consumed-energy> | <submit>"
    )
    fake_stderr = _byte_stream("")

    fake_ssh_client.exec_command.return_value = (
        unittest.mock.Mock(),
        fake_stdout,
        fake_stderr,
    )

    info = client.get_job_info(job_id=fake_job_id, connected_ssh_client=fake_ssh_client)
    assert info == mantik_api.models.run_info.SSHSlurm(
        id=fake_job_id,
        state=mantik_api.models.run_info.SSHSlurmJobStatus.COMPLETED,
        user="<user>",
        job_name="<job-name>",
        node_list="<node-list>",
        nodes="<n-nodes>",
        partition="<partition>",
        cpu_time="<cpu-time>",
        elapsed_time="<elapsed>",
        start_time="<start>",
        end_time="<end>",
        exit_code="<exit-code>",
        work_dir="<work-dir>",
        consumed_energy="<consumed-energy>",
        submit="<submit>",
    )


sample_rsa_key_string = """\
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAniaaz+wiZ2wBMUA/HThQVLQ9k7HS97JNXgNhlxMjtTb7Zmnu
jZGOGi2cOdjQJz91L09DRRPcGm8zlzu96ZNeqZgS7cmW2pKb5wIeKQgFATsXOw0R
hOd3wt65yUW7S1PzhhAFH4CqdtYjwzJyiPUmzgiXTYHiunrTSHoQ/DmuZanqVrXA
WmA7y5RqfRF5n17mlDgQPlow2KFbB2WWmkrO50OaHRdexbAYisvlsGIfhcVJynzF
BGxvP5XacQBgldca53SSHd+jJetdu+Ix9ugkaf8kOSYi9lujVEB/dlR/LEuY6TwW
m0Amf7PJhGgc9Iix2J29CMp1IyFVfgPNAh/GDwIDAQABAoIBAFw+JhPaJeLusu9Y
CrfvTaNqEXRgxq0keU25aSVly3D55b5BURuPZzPLoJB47kfGEoKKl7DluX5Nl+jA
tq9S/maqxXeeGffYhUhDCvZhsBGctpsBr1M2crrdj71eh7SS/boXA/Xw5Pw1QFys
wvMxEfYgOqfBd03PMAbY86k5t4ehnVxOgkEJ8B6bqgbKpaarrHjace98ZUUJ5mEh
tZz8Xz4J16QL2FEsYkddnqJM5r7u/sdU0GPVtA5aT246wkIPOcqAIvUHpuTKiOvC
IYJn+Pg+qcMGP9xZZIcUhjbTVwYsbB84fu1tkJ95qkq6ulJD3YqHeZsoAhZ6vBZH
cjUuDlkCgYEA1e7yG5wZy/C46kljAA9sEPbSZHx1R6Sabnh6vVo10vlcJ628ept3
pdP7xlFrmwIDmYWY1FG3tlve26hi9LppKqAwoa6YNbpN9A3pZh4hhKq3Ces7dAlF
9ZMsr7PkWuWvb7aNK0F4Oo2jI8ZacziU02uBbYQrHEH1K4nIjsIX0h0CgYEAvT+n
2LCO2kGUkQ4/F0kfpquz8T91ITxWTZLSPPk2YaZwFc6SiSgenTB5ipLIPxe9SKJ3
Wpiqqe14XVZu7RnG6x3e3LeQexmJuZoS/LghbAX6yJ7wcZNryQkO9EHBLAcY+yus
g5kvxkznXF8/cxeFomqxWwopBr30BH8e7k7sgRsCgYEApna2VvuBKyqViGAwM5TM
fuq/zUb2rxeKvxjqULqIFTDJH2rVtQWR9SvcxnUGaOgJSwUkZVlsvO4BnCQLU+hU
+sEI9lX3xB7Cl3vXuAkMBcIciRBMA79Pe4XYiKNOtdfxSdjfQeBAoDcj0St/qBZH
37bQUBo+vU8paYZd0499n5UCgYAx99TBihyt1BL+GdzesRgCUeO5FyA+HkhLQzDv
mH2bWu7NUzWtsUIkDuCIjikBP6tiukL5UMX/CAx32JKBWAUFn2VwsaccWanbr6rD
v3pTo2CMCCtEUcBr3FBufc4baeRWrTlnpdLPcQ7FfQCrytImCDW76/rZJN6BMW9h
TMV1cQKBgGplT2d5KjBxSBAY3iXumTsXntQDsENVXwff78AThX7rbKrCpbh+MT0m
BZQmrrjhfEsMM/3YyZZUMboID0LVBNroaB0DuwX3XXYNw1Ym09tcflCgOSB5a05s
dnZ30Gx+9oEx1KGWneYETXDUPxqgMa3kl956JrgWxy2+UyKo8mQi
-----END RSA PRIVATE KEY-----"""

sample_dsa_key_string = """\
-----BEGIN DSA PRIVATE KEY-----
MIIDTQIBAAKCAQEAj3k12bmq6b+r7Yh6z0lRtvMuxZ47rzcY6OrElh8+/TYG50NR
qcQYMzm4CefCrhxTm6dHW4XQEa24tHmHdUmEaVysDo8UszYIKKIv+icRCj1iqZNF
NAmg/mlsRlj4S90ggZw3CaAQV7GVrc0AIz26VIS2KR+dZI74g0SGd5ec7AS0NKas
LnXpmF3iPbApL8ERjJ/6nYGB5zONt5K3MNe540lZL2gJmHIVORXqPWuLRlPGM0WP
gDsypMLg8nKQJW5OP4o7CDihxFDk4YwaKaN9316hQ95LZv8EkD7VzxYj4VjUh8YI
6X8hHNgdyiPLbjgHZfgi40K+SEwFdjk5YBzWZwIdALr2lqaFePff3uf6Z8l3x4Xv
MrIzuuWAwLzVaV0CggEAFqZcWCBIUHBOdQKjl1cEDTTaOjR4wVTU5KXALSQu4E+W
5h5L0JBKvayPN+6x4J8xgtI8kEPLZC+IAEFg7fnKCbMgdqecMqYn8kc+kYebosTn
RL0ggVRMtVuALDaNH6g+1InpTg+gaI4yQopceMR4xo0FJ7ccmjq7CwvhLERoljnn
08502xAaZaorh/ZMaCbbPscvS1WZg0u07bAvfJDppJbTpV1TW+v8RdT2GfY/Pe27
hzklwvIk4HcxKW2oh+weR0j4fvtf3rdUhDFrIjLe5VPdrwIRKw0fAtowlzIk/ieu
2oudSyki2bqL457Z4QOmPFKBC8aIt+LtQxbh7xfb3gKCAQBP0RrX8gWXyHeWQg++
b67Zs3dNpdMn9wQo7Kwvf2H4mNtiC3NFdDpH7k5nM1x56vT6WSMBHvQYK1lESGeN
fSxnCThHo8iIkmb4sgemlE1euJ1DcTKhe6TclHcCrXBNj2OmuY2Oi8Nzj6G0g2HQ
4reuV8DFpxP0p11bO8fEk7mL2DMsGIwSXCHkPJxUxyM016oAHLapSr/tXpmKP1kQ
51cykgKAhBa/XiLhg/7JirvRRkeQf6uRfohg80mDBabaOb4wZvGF7em1uTycL4OW
VMYAqdbSHRbm8p3pG/JaqcNkhmGDMgPBcS+QdxADGhE5BZJuDwHU1WremvFX3TPn
xfWdAhw+k76LbnfdGfIUqucJBIbh9YnpjzhWZ8A9hL+J
-----END DSA PRIVATE KEY-----"""

sample_ecdsa_key_string = """\
-----BEGIN EC PRIVATE KEY-----
MIHcAgEBBEIBc+IJfN989qG3ygTtXCpkNhBvahHNFeRO/vVkuz8NhcdDPahUL7Z3
lHDhINccqPlCjlWAK9WemtsGAEu4ma2OEYCgBwYFK4EEACOhgYkDgYYABAEpR8BY
3J+9uu9bUyFnV96Y0CsEBF3bKBpz7xlhEjLUZ6MwGNAHBrzqUc5arCD6S5UGzJhz
XBnRT2Ir7mfNOsfb5wFe0+ugcElsYfvTOqgar02GdEnqq6QvZP4TM5rwYx0KNq7u
Ud4OVQaCZwpWRnN9tpXSFeFXHmc2PHqHzCpAnncrNg==
-----END EC PRIVATE KEY-----"""

sample_ed25519_key_string = """\
-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAMwAAAAtzc2gtZW
QyNTUxOQAAACBWBVCBpi6gVwsFvIIGpAVt3BzfROEeHcYBN3RiF4VLdgAAAJAoA9cWKAPX
FgAAAAtzc2gtZWQyNTUxOQAAACBWBVCBpi6gVwsFvIIGpAVt3BzfROEeHcYBN3RiF4VLdg
AAAEBFus8OzEI0F9Ik8zwwLBEcZJ2QFGRSNRyAkU71wTkFslYFUIGmLqBXCwW8ggakBW3c
HN9E4R4dxgE3dGIXhUt2AAAABm5vbmFtZQECAwQFBgc=
-----END OPENSSH PRIVATE KEY-----"""


sample_broken_key_string = """\
1234-i'll-be-broken-forever-more"""


@pytest.mark.parametrize(
    "private_key_string,expected_key_type",
    (
        [sample_rsa_key_string, paramiko.RSAKey],
        [sample_dsa_key_string, paramiko.DSSKey],
        [sample_ecdsa_key_string, paramiko.ECDSAKey],
        [sample_ed25519_key_string, paramiko.Ed25519Key],
    ),
)
def test_get_private_key_from_key_string(private_key_string: str, expected_key_type):
    parsed_key = client.get_private_key_from_key_string(private_key=private_key_string)
    assert isinstance(parsed_key, expected_key_type)


def test_get_private_key_from_key_string_invalid_key_raises_error():
    with pytest.raises(exceptions.SSHError, match="Private key is invalid."):
        client.get_private_key_from_key_string(private_key=sample_broken_key_string)


@pytest.fixture
def fake_sftp_client():
    return unittest.mock.Mock()


def test_download_file_in_memory(fake_sftp_client):
    """
    Given there's file content available from an SFTP server
    When  I retrieve it
    Then  it gets stored in memory to a byte stream
    """

    remote_path = pathlib.Path("/remote/path/to/file.txt")
    sample_file_data = b"This is test file content."

    fake_sftp_client.getfo = lambda remote_path, file_stream: file_stream.write(
        sample_file_data
    )

    file_stream = client.download_file_in_memory(fake_sftp_client, remote_path)
    assert isinstance(file_stream, io.BytesIO)
    assert file_stream.getvalue() == sample_file_data


def test_download_file_or_folder_not_found(fake_sftp_client, fake_ssh_client):
    """
    Given there's no file from an SFTP server
    When  I retrieve it
    Then  I get a `FileNotFound` error
    """
    remote_path = pathlib.Path("/remote/path/to/nonexistent.txt")

    fake_sftp_client.lstat.side_effect = FileNotFoundError
    fake_ssh_client.open_sftp.return_value = fake_sftp_client

    with pytest.raises(
        exceptions.SSHError,
        match=f"Filepath: `{str(remote_path)}` not found on the remote system!",
    ):
        client.download_from_remote_path(remote_path, fake_ssh_client)


ST_MODE_FOR_FILE = 0o100644
ST_MODE_FOR_DIRECTORY = 0o040755


@pytest.fixture
def fake_sftp_client_with_fake_files():
    """Fixture to create a mock SFTP client with a directory structure."""

    mock_client = unittest.mock.Mock()
    mock_client.listdir_attr.side_effect = [
        # First call returns two files and a subdirectory
        [
            unittest.mock.Mock(filename="file1.txt", st_mode=ST_MODE_FOR_FILE),
            unittest.mock.Mock(filename="file2.txt", st_mode=ST_MODE_FOR_FILE),
            unittest.mock.Mock(filename="subdir", st_mode=ST_MODE_FOR_DIRECTORY),
        ],
        # Second call returns a single file within the subdirectory
        [
            unittest.mock.Mock(
                filename="file3.txt", st_mode=ST_MODE_FOR_FILE
            )  # File inside subdir
        ],
    ]
    return mock_client


@unittest.mock.patch(
    "mantik_api.compute_backend.ssh_remote_compute_system."
    "client.download_file_in_memory"
)
def test_download_directory_as_zip_to_memory(
    mock_download_file, fake_sftp_client_with_fake_files
):
    """
    (also indirectly tests the internal `_add_directory_to_zip` helper function.)

    Given there exists a remote directory
    And   it contains files and a sub-directory
    When  I request to download it as a zip file
    Then  I receive a zip-file bytestream
    And   it contains the files
    """
    # Mock file content for downloaded files
    mock_download_file.return_value = io.BytesIO(b"Mock file contents")

    # Call the function under test
    remote_dir_path = pathlib.Path("/remote/path/to/directory")
    zip_stream = mantik_api.compute_backend.ssh_remote_compute_system.client.download_directory_as_zip_to_memory(  # noqa E501
        fake_sftp_client_with_fake_files, remote_dir_path
    )

    # Verify the contents of the zip archive
    with zipfile.ZipFile(zip_stream, "r") as zip_file:
        assert "directory/file1.txt" in zip_file.namelist()
        assert "directory/file2.txt" in zip_file.namelist()
        assert "directory/subdir/file3.txt" in zip_file.namelist()

    # Ensure the mock was called correctly
    fake_sftp_client_with_fake_files.listdir_attr.assert_any_call(
        "/remote/path/to/directory"
    )
    fake_sftp_client_with_fake_files.listdir_attr.assert_any_call(
        "/remote/path/to/directory/subdir"
    )
    mock_download_file.assert_any_call(
        fake_sftp_client_with_fake_files,
        pathlib.Path("/remote/path/to/directory/file1.txt"),
    )
    mock_download_file.assert_any_call(
        fake_sftp_client_with_fake_files,
        pathlib.Path("/remote/path/to/directory/file2.txt"),
    )
    mock_download_file.assert_any_call(
        fake_sftp_client_with_fake_files,
        pathlib.Path("/remote/path/to/directory/subdir/file3.txt"),
    )


@unittest.mock.patch(
    "mantik_api.compute_backend.ssh_remote_compute_system"
    ".client.download_file_in_memory"
)
def test_download_file_from_remote_path(
    mock_download_file, fake_sftp_client, fake_ssh_client
):
    """
    When  I request to download from a path
    And   the path refers to a file
    Then  the file gets downloaded
    """
    # setup
    mock_file_attr = unittest.mock.Mock(st_mode=ST_MODE_FOR_FILE)
    fake_sftp_client.lstat.return_value = mock_file_attr

    fake_ssh_client.open_sftp.return_value = fake_sftp_client

    # test function
    download_response = mantik_api.compute_backend.ssh_remote_compute_system.client.download_from_remote_path(  # noqa E501
        path=pathlib.Path("/remote/file.txt"), connected_ssh_client=fake_ssh_client
    )
    list(download_response.get_content())

    # assert
    mock_download_file.assert_called_once_with(
        sftp_client=fake_sftp_client,
        remote_file_path=pathlib.Path("/remote/file.txt"),
    )


@unittest.mock.patch(
    "mantik_api.compute_backend.ssh_remote_compute_system"
    ".client.download_directory_as_zip_to_memory"
)
def test_download_directory_from_remote_path(
    mock_download_dir,
    fake_sftp_client,
    fake_ssh_client,
):
    """
    When  I request to download from a path
    And   the path refers to a directory
    Then  the directory gets downloaded
    """
    # setup
    mock_dir_attr = unittest.mock.Mock(st_mode=ST_MODE_FOR_DIRECTORY)
    fake_sftp_client.lstat.return_value = mock_dir_attr

    fake_ssh_client.open_sftp.return_value = fake_sftp_client

    # test function
    download_response = mantik_api.compute_backend.ssh_remote_compute_system.client.download_from_remote_path(  # noqa E501
        path=pathlib.Path("/remote/directory"), connected_ssh_client=fake_ssh_client
    )
    list(download_response.get_content())
    # assert
    mock_download_dir.assert_called_once_with(
        sftp_client=fake_sftp_client,
        remote_dir_path=pathlib.Path("/remote/directory"),
    )
