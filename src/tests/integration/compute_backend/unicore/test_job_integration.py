import pathlib
import uuid
import zipfile

import pytest
import pyunicore.client

import mantik_api.compute_backend as compute_backend
import mantik_api.compute_backend.unicore.job as _job
import mantik_api.database as database
import mantik_api.testing as testing


class TestJob:
    def test_id(self, create_unicore_job_echo):
        assert uuid.UUID(create_unicore_job_echo.id)

    def test_get_status(self, create_unicore_job_echo):
        assert create_unicore_job_echo.get_status()

    def test_get_properties(self, create_unicore_job_echo):
        result = create_unicore_job_echo.get_info()

        assert result.id == create_unicore_job_echo.id
        assert result.site_name == "DEMO-SITE"
        assert result.bss_details

    def test_get_logs(self, create_unicore_job_echo):
        _wait_until_job_successful(create_unicore_job_echo)

        result = create_unicore_job_echo.get_logs()

        assert any("UNICORE API LOGS" in line for line in result)
        assert any("APPLICATION LOGS" in line for line in result)

    @pytest.mark.slow
    def test_cancel(self, create_unicore_job_sleep_2):
        """Test cancellation of a job.

        Cancelling a job takes a couple of seconds, hence ``job.poll()`` is
        called to await its cancellation (failure).

        """
        create_unicore_job_sleep_2._job.poll(state=pyunicore.client.JobStatus.QUEUED)

        create_unicore_job_sleep_2.cancel()

        create_unicore_job_sleep_2._job.poll(state=pyunicore.client.JobStatus.FAILED)

        assert create_unicore_job_sleep_2.get_status() == database.run.RunStatus.FAILED

    @pytest.mark.parametrize(
        ("path", "expected_content", "expected_file_name"),
        [
            (
                pathlib.Path(compute_backend.job.APPLICATION_LOGS_FILE),
                "hello\n",
                compute_backend.job.APPLICATION_LOGS_FILE,
            ),
            (pathlib.Path("sub/folder/test.txt"), "test\n", "test.txt"),
        ],
    )
    @pytest.mark.slow
    def test_download_file(
        self, create_unicore_job_echo, path, expected_content, expected_file_name
    ):
        _wait_until_job_successful(create_unicore_job_echo)

        download = create_unicore_job_echo.download(path)
        result = testing.stream.byte_chunks_to_string(download.get_content())

        assert result == expected_content
        assert download.media_type == "application/octet-stream"
        assert (
            download.headers["Content-Disposition"]
            == f"attachment;filename={expected_file_name}"
        )

    @pytest.mark.parametrize("path", ["sub/folder", "/sub/folder"])
    @pytest.mark.slow
    def test_download_sub_folder(self, create_unicore_job_echo, path):
        expected_archive_name = "folder"
        _wait_until_job_successful(create_unicore_job_echo)

        expected_txt = b"test\n"
        expected_files = [
            f"{expected_archive_name}/test.txt",
        ]

        download = create_unicore_job_echo.download(path=pathlib.Path(path))

        result = testing.stream.chunks_to_bytes(download.get_content())

        with zipfile.ZipFile(result) as zip_file:
            result_files = zip_file.namelist()
            with zip_file.open(f"{expected_archive_name}/test.txt") as txt_file:
                result_txt_file = txt_file.read()

        assert all(file in result_files for file in expected_files)
        assert result_txt_file == expected_txt
        assert download.media_type == "application/x-zip-compressed"
        assert (
            download.headers["Content-Disposition"]
            == f"attachment;filename={expected_archive_name}.zip"
        )

    @pytest.mark.parametrize("path", [None, pathlib.Path("/")])
    @pytest.mark.slow
    def test_download_entire_job_folder(self, create_unicore_job_echo, path):
        expected_archive_name = create_unicore_job_echo.id
        _wait_until_job_successful(create_unicore_job_echo)

        expected_logs = b"hello\n"
        expected_txt = b"test\n"
        expected_files = [
            f"{expected_archive_name}/UNICORE_SCRIPT_EXIT_CODE",
            f"{expected_archive_name}/sub/folder/test.txt",
            f"{expected_archive_name}/mantik.log",
        ]

        download = create_unicore_job_echo.download(path)

        result = testing.stream.chunks_to_bytes(download.get_content())

        with zipfile.ZipFile(result) as zip_file:
            result_files = zip_file.namelist()
            with zip_file.open(
                f"{expected_archive_name}/{compute_backend.job.APPLICATION_LOGS_FILE}"
            ) as log_file:
                result_logs = log_file.read()
            with zip_file.open(
                f"{expected_archive_name}/sub/folder/test.txt"
            ) as txt_file:
                result_txt_file = txt_file.read()

        assert all(file in result_files for file in expected_files)
        assert result_logs == expected_logs
        assert result_txt_file == expected_txt
        assert download.media_type == "application/x-zip-compressed"
        assert (
            download.headers["Content-Disposition"]
            == f"attachment;filename={expected_archive_name}.zip"
        )


def _wait_until_job_successful(job: _job.Job) -> None:
    # Wait until job successful to access application logs
    job._job.poll(state=pyunicore.client.JobStatus.SUCCESSFUL)
