import pytest

import mantik_api.compute_backend.unicore.connect as connect
import mantik_api.compute_backend.unicore.exceptions as exceptions
import mantik_api.models as models


def test_connect_to_unicore_api(unicore_api_url, api_connection):
    result = connect.connect_to_unicore_api(
        url=unicore_api_url, credentials=api_connection.to_unicore_credentials()
    )
    assert result.properties["client"]["xlogin"]["UID"] == "demouser"


@pytest.mark.parametrize("attribute", ["login_name", "password"])
def test_connect_to_unicore_api_fails(
    unicore_api_url, api_connection, expect_raise_if_exception, attribute
):
    api_connection = models.connection.Connection(
        connection_id=api_connection.connection_id,
        user=api_connection.user,
        connection_name=api_connection.connection_name,
        connection_provider=api_connection.connection_provider,
        auth_method=api_connection.auth_method,
        login_name=api_connection.login_name,
        password=api_connection.password,
        created_at=api_connection.created_at,
    )
    setattr(api_connection, attribute, "incorrect")
    with expect_raise_if_exception(exceptions.AuthenticationFailedException()):
        connect.connect_to_unicore_api(
            url=unicore_api_url, credentials=api_connection.to_unicore_credentials()
        )
