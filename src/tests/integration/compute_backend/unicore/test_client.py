import mantik_api.compute_backend.unicore as unicore


class TestClient:
    def test_get_job(self, create_unicore_job_echo, unicore_client):
        job = unicore_client.get_job(create_unicore_job_echo.id)

        assert job.id == create_unicore_job_echo.id

    def test_get_job_fails(self, expect_raise_if_exception, unicore_client):
        with expect_raise_if_exception(unicore.exceptions.JobNotFoundException()):
            unicore_client.get_job("invalid-job-id")
