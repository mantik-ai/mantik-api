import pathlib
import zipfile

import pytest

import mantik_api.database as database
import mantik_api.models as models
import mantik_api.testing as testing


@pytest.fixture(autouse=True, scope="session")
def skip_if_option_not_passed(skip_if_firecrest_option_not_passed):
    """Skip all tests in this file if the `--use-local-firecrest` option is
    not explicitly passed."""
    pass


class TestJob:
    def test_get_status(self, create_firecrest_job_integration):
        _, _, job = create_firecrest_job_integration

        expected = database.run.RunStatus.FINISHED

        result = job.get_status()

        assert result == expected, "Incorrect job status"

    def test_get_info(self, create_firecrest_job_integration):
        _, _, job = create_firecrest_job_integration

        result = job.get_info()

        assert result.state == models.run_info.FirecrestStatus.COMPLETED
        assert result.name == "script.batch"
        assert result.nodes == 1
        assert result.nodelist == "localhost"
        assert result.partition == testing.firecrest.LOCAL_PARTITION

    def test_get_logs(self, create_firecrest_job_integration):
        _, _, job = create_firecrest_job_integration

        # The output has one newline, which results in one empty string
        # when splitting by '\n' character.
        expected = testing.firecrest.TEST_LOGS_CONTENT.decode("utf-8").split("\n")

        result = job.get_logs()

        assert result == expected, "Incorrect job logs"

    def test_download_file(self, create_firecrest_job_integration):
        run_id, run_dir, job = create_firecrest_job_integration

        for path, expected_file_name, expected_content in [
            (
                testing.firecrest.TEST_LOGS_FILE_NAME,
                testing.firecrest.TEST_LOGS_FILE_NAME,
                "hello\n",
            ),
            (
                f"sub/folder/{testing.firecrest.TEST_FILE_NAME}",
                testing.firecrest.TEST_FILE_NAME,
                "test\n",
            ),
        ]:
            download = job.download(pathlib.Path(path))

            result = testing.stream.byte_chunks_to_string(download.get_content())

            assert result == expected_content, "Incorrect file content"
            assert (
                download.media_type == "application/octet-stream"
            ), "Incorrect media type"
            assert (
                download.headers["Content-Disposition"]
                == f"attachment;filename={expected_file_name}"
            ), "Incorrect filename in attachment header"

    @pytest.mark.parametrize("path", ["sub/folder", "/sub/folder"])
    def test_download_sub_folder(self, create_firecrest_job_integration, path):
        run_id, run_dir, job = create_firecrest_job_integration

        expected_archive_name = "folder"
        expected_txt = testing.firecrest.TEST_FILE_CONTENT
        expected_files = [
            f"{expected_archive_name}/{testing.firecrest.TEST_FILE_NAME}",
        ]

        download = job.download(path=pathlib.Path(path))

        result = testing.stream.chunks_to_bytes(download.get_content())

        with zipfile.ZipFile(result) as zip_file:
            result_files = zip_file.namelist()
            with zip_file.open(expected_files[0]) as txt_file:
                result_txt_file = txt_file.read()

        assert all(
            file in result_files for file in expected_files
        ), "Not all files in ZIP"
        assert result_txt_file == expected_txt, "Incorrect file content"
        assert (
            download.media_type == "application/x-zip-compressed"
        ), "Incorrect media type"
        assert (
            download.headers["Content-Disposition"]
            == f"attachment;filename={expected_archive_name}.zip"
        ), "Incorrect filename in attachment header"

    @pytest.mark.parametrize("path", [None, pathlib.Path("/")])
    def test_download_entire_job_folder(self, create_firecrest_job_integration, path):
        run_id, run_dir, job = create_firecrest_job_integration

        expected_archive_name = str(run_id)
        expected_files = {
            f"{expected_archive_name}/{testing.firecrest.TEST_LOGS_FILE_NAME}": testing.firecrest.TEST_LOGS_CONTENT,  # noqa: E501
            f"{expected_archive_name}/sub/folder/{testing.firecrest.TEST_FILE_NAME}": testing.firecrest.TEST_FILE_CONTENT,  # noqa: E501
        }

        download = job.download(path)

        result = testing.stream.chunks_to_bytes(download.get_content())

        with zipfile.ZipFile(result) as zip_file:
            result_files = zip_file.namelist()

            assert all(
                file in result_files for file in expected_files.keys()
            ), "Not all expected files found in ZIP"

            for expected_name, expected_content in expected_files.items():
                with zip_file.open(expected_name) as file:
                    result_content = file.read()
                    assert result_content == expected_content, "Incorrect file content"

        assert (
            download.media_type == "application/x-zip-compressed"
        ), "Incorrect media type"
        assert (
            download.headers["Content-Disposition"]
            == f"attachment;filename={expected_archive_name}.zip"
        ), "Incorrect filename in attachment header"
