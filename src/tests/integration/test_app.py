import fastapi

import mantik_api


def test_create_app():
    app = mantik_api.app.create_app()

    assert isinstance(app, fastapi.FastAPI)
    route_paths = [route.path for route in app.routes]
    assert "/docs" in route_paths
