import os
import pathlib
import typing as t
import unittest
import uuid

import fastapi
import fastapi.testclient as testclient
import pytest
import pyunicore.helpers.jobs
import sqlalchemy.exc
import sqlalchemy_utils
import starlette.status as status

import mantik_api.app as _app
import mantik_api.compute_backend as compute_backend
import mantik_api.compute_backend.firecrest as firecrest
import mantik_api.compute_backend.unicore as unicore
import mantik_api.database as database
import mantik_api.database.base as base
import mantik_api.routes.dependencies as dependencies
import mantik_api.routes.mlflow.tracking_server_router as tracking_server_router
import mantik_api.testing as testing
import mantik_api.tokens as tokens
import mantik_api.tokens.verifier as _verifier
import mantik_api.utils as utils

_DEFAULT_URI = "postgresql+psycopg2://postgres:postgres@localhost:5432/{db_name}"
_TEST_POSTGRES_URI = os.environ.get("TEST_POSTGRES_URI", _DEFAULT_URI)

# Changing port to 5101 locally  to not clash with firecREST microservices.
_DEFAULT_TRACKING_SERVER_URI = "http://localhost:5101"
_TEST_TRACKING_SERVER_URI = os.getenv(
    "TRACKING_SERVER_URI_INTERNAL", _DEFAULT_TRACKING_SERVER_URI
)


@pytest.fixture
def sample_text_file_path():
    return (
        pathlib.Path(__file__).parent.parent / "resources/files/empty.txt"
    ).as_posix()


@pytest.fixture()
def set_mlflow_tracking_url(fake_token) -> str:
    env_vars = {
        tracking_server_router.TRACKING_SERVER_URL_INTERNAL_ENV_VAR: _TEST_TRACKING_SERVER_URI,  # noqa E501
    }
    with utils.env.env_vars_set(env_vars):
        yield _TEST_TRACKING_SERVER_URI


@pytest.fixture(scope="session")
def unicore_client(unicore_api_url, api_connection) -> unicore.client.Client:
    return unicore.connect.connect_to_unicore_api(
        unicore_api_url, api_connection.to_unicore_credentials()
    )


@pytest.fixture(scope="session")
def create_unicore_job_echo(unicore_client) -> unicore.job.Job:
    job_description = pyunicore.helpers.jobs.Description(
        executable="echo",
        # Echo 'hello' to stdout and create a file in `sub/folder/test.txt`
        # and write 'test' to that file.
        arguments=[
            "hello",
            "&&",
            "mkdir",
            "-p",
            "sub/folder",
            "&&",
            "touch",
            "sub/folder/test.txt",
            "&&",
            "echo",
            "test",
            ">>",
            "sub/folder/test.txt",
        ],
        # Compute Backend sets this file to write stdout and stderr
        stdout=compute_backend.job.APPLICATION_LOGS_FILE,
        stderr=compute_backend.job.APPLICATION_LOGS_FILE,
    )
    return testing.unicore.submit_job(
        client=unicore_client, job_description=job_description
    )


@pytest.fixture()
def create_unicore_job_sleep(unicore_client) -> t.Callable[[int], unicore.job.Job]:
    def create_job(seconds: int = 1):
        job_description = pyunicore.helpers.jobs.Description(
            executable="sleep",
            arguments=[str(seconds), "&&", "echo", "hello"],
            # Compute Backend sets this file to write stdout and stderr
            stdout=compute_backend.job.APPLICATION_LOGS_FILE,
            stderr=compute_backend.job.APPLICATION_LOGS_FILE,
        )
        return testing.unicore.submit_job(
            client=unicore_client, job_description=job_description
        )

    return create_job


@pytest.fixture()
def create_unicore_job_sleep_2(create_unicore_job_sleep) -> unicore.job.Job:
    return create_unicore_job_sleep(seconds=2)


@pytest.fixture(scope="session")
def skip_if_firecrest_option_not_passed(request):
    """The `--use-local-firecrest` option allows to run integration tests
    with local firecREST microservices.

    Those tests will be skipped per default (e.g. for the CI), but can
    be run locally by passing the option when running pytest
    (see CONTRIBUTING.md).

    """
    option = request.config.getoption("--use-local-firecrest")
    if not option:
        pytest.skip("Skipping firecREST integration tests")


@pytest.fixture(scope="session")
def create_firecrest_job() -> firecrest.job.Job:
    return firecrest.job.Job(
        client=testing.firecrest.FakeClient(),
        machine=testing.firecrest.TEST_MACHINE,
        job_id="test-job-id",
        mlflow_run_id=testing.firecrest.TEST_RUN_ID,
    )


@pytest.fixture(scope="session")
def create_firecrest_job_integration() -> (
    tuple[uuid.UUID, pathlib.Path, firecrest.job.Job]
):
    run_id, run_dir, job = testing.firecrest.submit_job()
    testing.firecrest.wait_until_job_is_finished(job)
    return run_id, run_dir, job


@pytest.fixture(scope="session")
def worker_suffix(request) -> str | None:
    return getattr(request.config, "workerinput", {}).get("workerid")


@pytest.fixture(scope="session")
def postgres_uri() -> str:
    if worker_suffix is not None:
        return _TEST_POSTGRES_URI.format(db_name=f"test_db_{worker_suffix}")
    return _TEST_POSTGRES_URI.format(db_name="test_db")


@pytest.fixture(scope="session")
def engine(request, postgres_uri):
    engine = sqlalchemy.create_engine(postgres_uri)
    if not sqlalchemy_utils.database_exists(engine.url):
        sqlalchemy_utils.create_database(engine.url)
    return engine


@pytest.fixture()
def session(engine):
    try:
        base.Base.metadata.drop_all(bind=engine, checkfirst=False)
    except sqlalchemy.exc.ProgrammingError:
        # Table not found; DB has not been initialized
        pass
    base.Base.metadata.create_all(bind=engine)
    session_handler = database.client.sessions.SessionHandler(engine=engine)
    return session_handler


@pytest.fixture()
def database_client(engine, session) -> database.client.main.Client:
    return database.client.main.Client(engine=engine, session_handler=session)


@pytest.fixture
def app(database_client) -> fastapi.FastAPI:
    def override_get_db():
        try:
            yield database_client
        finally:
            pass

    application = _app.create_app()
    application.dependency_overrides = {dependencies.get_db_client: override_get_db}

    return application


@pytest.fixture
def client(monkeypatch, app) -> testclient.TestClient:
    monkeypatch.setattr(
        _verifier,
        "TokenVerifier",
        testing.token_verifier.FakeTokenVerifier,
    )
    return testclient.TestClient(app)


@pytest.fixture
def client_with_moto_token_validation(monkeypatch, app) -> testclient.TestClient:
    monkeypatch.setattr(
        tokens.jwks.JWKS,
        "from_cognito",
        testing.token_verifier.MotoJWKS.create,
    )
    return testclient.TestClient(app)


@pytest.fixture
def client_with_token_verifier(app) -> testclient.TestClient:
    return testclient.TestClient(app)


@pytest.fixture
def clear_and_init_database(database_client) -> None:
    base.Base.metadata.drop_all(bind=database_client._session_handler._engine)
    base.Base.metadata.create_all(bind=database_client._session_handler._engine)


@pytest.fixture
def add_example_labels(
    database_client,
    clear_and_init_database,
    database_label_1,
    database_label_2,
    database_label_3,
    database_label_4,
    database_label_5,
) -> None:
    with database_client.session():
        database_client.add(database_label_1)
        database_client.add(database_label_2)
        database_client.add(database_label_3)
        database_client.add(database_label_4)
        database_client.add(database_label_5)


@pytest.fixture
def add_example_user_roles(
    database_client,
    clear_and_init_database,
    database_user_role,
) -> None:
    with database_client.session():
        database_client.add(database_user_role)


@pytest.fixture
def add_example_user(
    database_client,
    clear_and_init_database,
    database_user,
    database_connection,
    second_database_connection,
    database_connection_firecrest,
    database_connection_slurm_private_key,
) -> None:
    with database_client.session():
        database_client.add(database_user)
        database_client.add(database_connection)
        database_client.add(second_database_connection)
        database_client.add(database_connection_firecrest)
        database_client.add(database_connection_slurm_private_key)


@pytest.fixture
def add_second_example_user(
    database_client,
    clear_and_init_database,
    second_database_user,
    database_connection_second_user,
) -> None:
    with database_client.session():
        database_client.add(second_database_user)
        database_client.add(database_connection_second_user)


@pytest.fixture
def add_third_example_user(
    database_client,
    clear_and_init_database,
    third_database_user,
) -> None:
    with database_client.session():
        database_client.add(third_database_user)


@pytest.fixture
def add_example_users(
    database_client, clear_and_init_database, connection_id_str
) -> None:
    with database_client.session():
        for i in range(200):
            database_client.add(
                model=database.user.User(
                    id=uuid.uuid4(),
                    cognito_name=f"example-cognito-name-{i}",
                    preferred_name=f"example-name-{i}",
                    email=f"example-email-{i}",
                )
            )


@pytest.fixture
def add_user_without_payment_info(
    database_client, clear_and_init_database, user_id_str
) -> None:
    with database_client.session():
        database_client.add(
            database.user.User(
                id=uuid.UUID(user_id_str),
                cognito_name="cognito_user_with_no_payment_info",
                preferred_name="user_with_no_payment_info",
                email="test_email",
            )
        )


@pytest.fixture
def add_database_user_group(
    database_client,
    clear_and_init_database,
    database_user_group,
) -> None:
    with database_client.session():
        database_client.add(database_user_group)


@pytest.fixture
def add_database_users(
    database_client, clear_and_init_database, database_user, second_database_user
) -> None:
    with database_client.session():
        database_client.add(database_user)
        database_client.add(second_database_user)


@pytest.fixture
def add_database_experiment_repository(
    database_client, clear_and_init_database, database_experiment_repository
) -> None:
    with database_client.session():
        database_client.add(database_experiment_repository)


@pytest.fixture
def add_trained_model(
    database_client, clear_and_init_database, database_saved_model_repository
) -> None:
    with database_client.session():
        database_client.add(database_saved_model_repository)


@pytest.fixture
def entry_point() -> str:
    return "some-path/file.exe"


@pytest.fixture
def mlflow_parameters() -> dict:
    return {"learning_rate": 0.5}


@pytest.fixture
def add_example_project(
    database_client,
    clear_and_init_database,
    add_example_user,
    database_code_repository,
    database_experiment_repository,
    database_model_repository,
    database_data_repository,
    second_database_code_repository,
    second_database_data_repository,
    second_database_experiment_repository,
    second_database_model_repository,
    third_database_code_repository,
    database_project,
) -> database.project.Project:
    with database_client.session():
        database_client.add(database_project)
        database_client.add(database_code_repository)
        database_client.add(second_database_code_repository)
        database_client.add(database_experiment_repository)
        database_client.add(second_database_experiment_repository)
        database_client.add(database_model_repository)
        database_client.add(second_database_model_repository)
        database_client.add(database_data_repository)
        database_client.add(second_database_model_repository)
        database_client.add(third_database_code_repository)
    return database_project


@pytest.fixture
def add_run_configuration(
    database_client,
    clear_and_init_database,
    add_example_user,
    add_example_connection,
    database_code_repository,
    database_experiment_repository,
    database_project,
    database_run_configuration,
) -> None:
    with database_client.session():
        database_client.add(database_project)
        database_client.add(database_code_repository)
        database_client.add(database_experiment_repository)
        database_client.add(database_run_configuration)


@pytest.fixture
def add_example_project_private(
    database_client, clear_and_init_database, database_project_private, add_example_user
) -> None:
    with database_client.session():
        database_client.add(database_project_private)


@pytest.fixture()
def add_example_project_with_labels_3_to_5(
    database_client,
    clear_and_init_database,
    database_project_labels_3_to_5,
    add_example_user,
) -> None:
    with database_client.session():
        database_client.add(database_project_labels_3_to_5)


@pytest.fixture()
def add_example_project_with_labels_1_to_5(
    database_client,
    clear_and_init_database,
    database_project_labels_1_to_5,
    add_example_user,
) -> None:
    with database_client.session():
        database_client.add(database_project_labels_1_to_5)


@pytest.fixture()
def add_example_project_with_labels_1_to_5_on_project(
    database_client,
    clear_and_init_database,
    database_project_labels_1_to_5_on_project,
    add_example_user,
) -> None:
    with database_client.session():
        database_client.add(database_project_labels_1_to_5_on_project)


@pytest.fixture
def add_example_run(
    database_client,
    clear_and_init_database,
    add_example_user,
    add_example_connection,
    add_example_project,
    database_run,
) -> None:
    with database_client.session():
        database_client.add(database_run)


@pytest.fixture
def add_running_example_run(
    database_client,
    clear_and_init_database,
    add_example_user,
    add_example_connection,
    add_example_project,
    database_run,
) -> None:
    with database_client.session():
        database_run.status = database.run.RunStatus.RUNNING
        database_client.add(database_run)


@pytest.fixture
def add_example_run_wrong_backend_config(
    database_client,
    clear_and_init_database,
    add_example_user,
    add_example_connection,
    add_example_project,
    database_run_wrong_backend_config,
) -> None:
    with database_client.session():
        database_client.add(database_run_wrong_backend_config)


@pytest.fixture
def add_example_run_private(
    database_client,
    clear_and_init_database,
    add_example_user,
    add_example_connection,
    add_example_project_private,
    database_run,
) -> None:
    with database_client.session():
        database_client.add(database_run)


@pytest.fixture
def add_example_experiment(
    database_client,
    clear_and_init_database,
    add_example_user,
    add_example_connection,
    add_example_project,
    database_experiment_repository,
) -> None:
    with database_client.session():
        database_client.add(database_experiment_repository)


@pytest.fixture
def add_example_experiment_private(
    database_client,
    clear_and_init_database,
    add_example_user,
    add_example_connection,
    add_example_project_private,
    database_experiment_repository,
) -> None:
    with database_client.session():
        database_client.add(database_experiment_repository)


@pytest.fixture
def add_failed_run(
    database_client,
    clear_and_init_database,
    add_example_user,
    add_example_connection,
    add_example_project,
    database_run,
):
    with database_client.session():
        database_run.status = database.run.RunStatus.FAILED
        database_client.add(database_run)


@pytest.fixture()
def add_example_run_with_mlflow_run(
    database_client,
    add_example_project,
    second_database_user,
    database_project,
    database_run_with_mlflow_run,
) -> None:
    with database_client.session():
        database_client.add(database_run_with_mlflow_run)


@pytest.fixture
def add_second_example_run(
    database_client,
    clear_and_init_database,
    add_example_user,
    add_example_connection,
    add_example_project,
    second_database_run,
) -> None:
    with database_client.session():
        database_client.add(second_database_run)


@pytest.fixture
def add_example_run_without_data_repository(
    database_client,
    clear_and_init_database,
    add_example_user,
    add_example_connection,
    add_example_project,
    database_run_without_data_repository,
) -> None:
    with database_client.session():
        database_client.add(database_run_without_data_repository)


@pytest.fixture
def add_example_run_schedule(
    database_client,
    clear_and_init_database,
    add_example_user,
    add_example_connection,
    add_example_project,
    add_example_run,
    database_run_schedule,
) -> None:
    with database_client.session():
        database_client.add(database_run_schedule)


@pytest.fixture
def add_2_example_projects(add_example_project, add_example_project_private) -> None:
    pass


@pytest.fixture
def add_example_user_group(
    database_client, clear_and_init_database, database_user_group
) -> None:
    with database_client.session():
        database_client.add(database_user_group)


@pytest.fixture
def add_second_example_user_group(
    database_client, clear_and_init_database, second_database_user_group
) -> None:
    with database_client.session():
        database_client.add(second_database_user_group)


@pytest.fixture
def add_example_organization(
    database_client,
    clear_and_init_database,
    add_example_user,
    add_third_example_user,
    add_example_user_group,
    database_organization,
) -> None:
    with database_client.session():
        database_client.add(database_organization)


@pytest.fixture
def add_second_example_organization(
    database_client,
    clear_and_init_database,
    add_second_example_user_group,
    second_database_organization,
) -> None:
    with database_client.session():
        database_client.add(second_database_organization)


@pytest.fixture
def add_example_connection(
    database_client, clear_and_init_database, add_example_user, database_connection
) -> None:
    with database_client.session():
        database_client.add(database_connection)


@pytest.fixture
def add_second_example_connection(
    database_client,
    clear_and_init_database,
    add_example_user,
    second_database_connection,
) -> None:
    with database_client.session():
        database_client.add(second_database_connection)


@pytest.fixture
def add_example_connection_second_user(
    database_client,
    clear_and_init_database,
    add_second_example_user,
    database_connection_second_user,
) -> None:
    with database_client.session():
        database_client.add(database_connection_second_user)


@pytest.fixture
def add_example_invitation_user_to_project(
    database_client,
    clear_and_init_database,
    database_project,
    database_invitation_user_to_project,
    add_database_users,
) -> None:
    with database_client.session():
        database_client.add(database_project)
        database_client.add(database_invitation_user_to_project)


@pytest.fixture
def add_invitation_of_second_user_to_project(
    database_client,
    clear_and_init_database,
    database_invitation_second_user_to_project,
    add_database_users,
    add_example_project,
):
    with database_client.session():
        database_client.add(database_invitation_second_user_to_project)


@pytest.fixture
def add_invitation_of_group_to_project(
    database_client,
    clear_and_init_database,
    add_database_users,
    add_example_project,
    add_database_user_group,
    database_invitation_group_to_project,
):
    with database_client.session():
        database_client.add(database_invitation_group_to_project)


@pytest.fixture
def add_invitation_of_organization_to_project(
    database_client,
    clear_and_init_database,
    add_database_users,
    add_example_project,
    database_invitation_organization_to_project,
):
    with database_client.session():
        database_client.add(database_invitation_organization_to_project)


@pytest.fixture
def add_all_example_invitations(
    database_client,
    clear_and_init_database,
    database_project,
    database_organization_without_group,
    database_invitation_user_to_project,
    database_invitation_organization_to_project,
    database_invitation_user_to_organization,
    database_invitation_group_to_organization,
    database_invitation_user_to_group,
    add_database_users,
    add_invitation_of_group_to_project,
    add_invitation_of_organization_to_project,
    add_invitation_of_second_user_to_project,
) -> None:
    with database_client.session():
        database_client.add(database_project)
        database_client.add(database_organization_without_group)
        database_client.add(database_invitation_user_to_project)
        database_client.add(database_invitation_user_to_organization)
        database_client.add(database_invitation_group_to_organization)
        database_client.add(database_invitation_user_to_group)


@pytest.fixture
def add_example_invitation_to_group(
    database_client,
    clear_and_init_database,
    database_user,
    user_group_id_str,
    database_invitation_user_to_group,
    add_database_users,
) -> None:
    with database_client.session():
        database_client.add(
            database.user_group.UserGroup(
                id=uuid.UUID(user_group_id_str),
                name="test-name",
                admin=database_user,
                members=[],
            )
        )
        database_client.add(database_invitation_user_to_group)


@pytest.fixture
def add_example_users_project_group_organization(
    database_client,
    clear_and_init_database,
    organization_id_str,
    database_project,
    add_database_users,
    database_user,
    user_group_id_str,
) -> None:
    with database_client.session():
        database_client.add(
            database.organization.Organization(
                id=uuid.UUID(organization_id_str),
                name="foo",
                contact=database_user,
                groups=[
                    database.user_group.UserGroup(
                        id=uuid.UUID(user_group_id_str),
                        name="test-name",
                        admin=database_user,
                        members=[database_user],
                    )
                ],
            )
        )
        database_client.add(database_project)


@pytest.fixture
def add_example_invitation_user_to_organization(
    database_client,
    clear_and_init_database,
    database_user_group,
    database_organization,
    database_invitation_user_to_organization,
    add_database_users,
) -> None:
    with database_client.session():
        database_client.add(database_user_group)
        database_client.add(database_organization)
        database_client.add(database_invitation_user_to_organization)


@pytest.fixture
def add_example_environment_to_project(
    database_client,
    clear_and_init_database,
    add_database_users,
    add_example_project,
    database_environment,
) -> None:
    with database_client.session():
        database_client.add(database_environment)


@pytest.fixture
def add_100_example_projects(database_client, clear_and_init_database) -> None:
    with database_client.session():
        for i in range(100):
            owner = database.user.User(
                id=uuid.uuid4(),
                cognito_name=f"example-cognito-name-{i}",
                preferred_name=f"example-name-{i}",
                email=f"example-email-{i}",
            )
            database_client.add(owner)
            database_client.add(
                model=database.project.Project(
                    id=uuid.uuid4(),
                    name=f"example-project_{i}",
                    owner=owner,
                    public=True,
                )
            )


@pytest.fixture
def mock_token_user_id(mocker, user_id_str):
    mocker.patch(
        "mantik_api.tokens.jwt.JWT.user_id",
        return_value=uuid.UUID(user_id_str),
        new_callable=mocker.PropertyMock,
    )


@pytest.fixture
def mock_token_second_user_id(mocker, second_user_id_str):
    mocker.patch(
        "mantik_api.tokens.jwt.JWT.user_id",
        return_value=uuid.UUID(second_user_id_str),
        new_callable=mocker.PropertyMock,
    )


@pytest.fixture
def mock_token_third_user_id(mocker, third_user_id_str):
    mocker.patch(
        "mantik_api.tokens.jwt.JWT.user_id",
        return_value=uuid.UUID(third_user_id_str),
        new_callable=mocker.PropertyMock,
    )


@pytest.fixture
def mock_get_user_info_and_be_project_owner(mocker, mock_token_user_id):
    mocker.patch(
        "mantik_api.routes.rbac._project.get_user_role_in_project",
        return_value=database.role_details.RoleDetails(
            role=database.role_details.ProjectRole.OWNER
        ),
    )


@pytest.fixture
def mock_get_second_user_info_and_be_project_researcher(
    mocker, mock_token_second_user_id
):
    """Make second user project owner."""
    mocker.patch(
        "mantik_api.routes.rbac._project.get_user_role_in_project",
        return_value=database.role_details.RoleDetails(
            role=database.role_details.ProjectRole.RESEARCHER
        ),
    )


@pytest.fixture
def mock_get_user_info_and_be_project_maintainer(mocker, mock_token_second_user_id):
    mocker.patch(
        "mantik_api.routes.rbac._project.get_user_role_in_project",
        return_value=database.role_details.RoleDetails(
            role=database.role_details.ProjectRole.MAINTAINER
        ),
    )


@pytest.fixture
def mock_impersonate_random_user(mocker, database_client):
    random_id = uuid.uuid4()
    with database_client.session():
        database_client.add(
            model=database.user.User(
                id=random_id,
                cognito_name="example-cognito-name",
                preferred_name="example-name",
                email="example-email",
            )
        )
    mocker.patch(
        "mantik_api.tokens.jwt.JWT.user_id",
        return_value=random_id,
        new_callable=mocker.PropertyMock,
    )
    return random_id


@pytest.fixture
def create_mlflow_runs(create_mlflow_run) -> t.Iterator[uuid.UUID]:
    """Creates 3 runs for first experiment."""
    return iter(create_mlflow_run() for _ in range(3))


@pytest.fixture
def mock_compute_backend_api_integration_testing(
    mock_gitlab_zip_download,
    mock_compute_backend_api_env_vars,
    fake_compute_backend_url,
    fake_token,
    api_experiment_repository,
    second_api_experiment_repository,
    create_mlflow_runs,
    create_unicore_job_sleep,
):
    # Use function to lazy-create UNICORE job when compute backend API
    # is invoked.
    def create_compute_backend_response(*args, **kwargs) -> dict:
        job = create_unicore_job_sleep(seconds=0)
        return {
            "experiment_id": second_api_experiment_repository.mlflow_experiment_id,
            "run_id": str(next(create_mlflow_runs)),
            "unicore_job_id": str(job.id),
        }

    return _mock_gitlab_zip_download(
        fake_compute_backend_url=fake_compute_backend_url,
        mock_gitlab_zip_download_fixture=mock_gitlab_zip_download,
        compute_backend_response_factory=create_compute_backend_response,
        api_experiment_repository=api_experiment_repository,
        second_api_experiment_repository=second_api_experiment_repository,
    )


@pytest.fixture
def mock_compute_backend_api_integration_testing_local_firecrest(
    mock_gitlab_zip_download,
    mock_compute_backend_api_env_vars,
    fake_compute_backend_url,
    fake_token,
    api_experiment_repository,
    second_api_experiment_repository,
    create_mlflow_runs,
):
    # Use function to lazy-create firecREST job when compute backend API
    # is invoked.
    def create_compute_backend_response(*args, **kwargs) -> dict:
        run_id = next(create_mlflow_runs)
        _, _, job = testing.firecrest.submit_job(run_id)
        testing.firecrest.wait_until_job_is_finished(job)
        return {
            "experiment_id": second_api_experiment_repository.mlflow_experiment_id,
            "run_id": str(run_id),
            "unicore_job_id": str(job.id),
        }

    return _mock_gitlab_zip_download(
        fake_compute_backend_url=fake_compute_backend_url,
        mock_gitlab_zip_download_fixture=mock_gitlab_zip_download,
        compute_backend_response_factory=create_compute_backend_response,
        api_experiment_repository=api_experiment_repository,
        second_api_experiment_repository=second_api_experiment_repository,
    )


def _mock_gitlab_zip_download(
    fake_compute_backend_url: str,
    mock_gitlab_zip_download_fixture,
    compute_backend_response_factory: t.Callable[[...], dict],
    api_experiment_repository: database.experiment_repository.ExperimentRepository,  # noqa: E501
    second_api_experiment_repository: database.experiment_repository.ExperimentRepository,  # noqa: E501
):
    mock_gitlab_zip_download_fixture.post(
        (
            f"{fake_compute_backend_url}/submit/"
            f"{api_experiment_repository.mlflow_experiment_id}"
        ),
        json=compute_backend_response_factory,
        status_code=status.HTTP_201_CREATED,
    )
    mock_gitlab_zip_download_fixture.post(
        (
            f"{fake_compute_backend_url}/submit/"
            f"{second_api_experiment_repository.mlflow_experiment_id}"
        ),
        json=compute_backend_response_factory,
        status_code=status.HTTP_201_CREATED,
    )
    return mock_gitlab_zip_download_fixture


@pytest.fixture
def add_labels_for_curation_tests(clear_and_init_database, database_client):
    with database_client.session():
        for label in testing.labels.DATABASE_LABELS_BEFORE_CRUD:
            database_client.add(label)


@pytest.fixture
def add_second_project_member_as_reporter(
    add_example_project,
    database_client,
    second_user_id_str,
    project_id_str,
    add_second_example_user,
    created_at,
):
    guest = database.project.UserProjectAssociationTable(
        role=database.project.ProjectRole.REPORTER,
        user_id=uuid.UUID(second_user_id_str),
        project_id=uuid.UUID(project_id_str),
        created_at=created_at,
    )
    with database_client.session():
        database_client.add(guest)


@pytest.fixture
def add_second_project_member_as_maintainer(
    add_example_project,
    database_client,
    second_user_id_str,
    project_id_str,
    add_second_example_user,
    created_at,
):
    guest = database.project.UserProjectAssociationTable(
        role=database.project.ProjectRole.MAINTAINER,
        user_id=uuid.UUID(second_user_id_str),
        project_id=uuid.UUID(project_id_str),
        created_at=created_at,
    )
    with database_client.session():
        database_client.add(guest)


@pytest.fixture
def add_second_user_group_project_role_as_reporter(
    add_second_example_user_group,
    add_example_project,
    add_second_example_user,
    database_client,
    second_user_group_id_str,
    project_id_str,
    created_at,
):
    guest = database.project.UserGroupProjectAssociationTable(
        role=database.project.ProjectRole.REPORTER,
        user_group_id=uuid.UUID(second_user_group_id_str),
        project_id=uuid.UUID(project_id_str),
        created_at=created_at,
    )
    with database_client.session():
        database_client.add(guest)


@pytest.fixture
def add_second_user_group_project_role_as_maintainer(
    add_second_example_user_group,
    add_example_project,
    add_second_example_user,
    database_client,
    second_user_group_id_str,
    project_id_str,
    created_at,
):
    guest = database.project.UserGroupProjectAssociationTable(
        role=database.project.ProjectRole.MAINTAINER,
        user_group_id=uuid.UUID(second_user_group_id_str),
        project_id=uuid.UUID(project_id_str),
        created_at=created_at,
    )
    with database_client.session():
        database_client.add(guest)


@pytest.fixture
def add_second_organization_project_role_as_reporter(
    add_second_example_organization,
    add_second_example_user,
    add_example_project,
    database_client,
    second_organization_id_str,
    project_id_str,
    created_at,
):
    guest = database.project.OrganizationProjectAssociationTable(
        role=database.project.ProjectRole.REPORTER,
        organization_id=uuid.UUID(second_organization_id_str),
        project_id=uuid.UUID(project_id_str),
        created_at=created_at,
    )
    with database_client.session():
        database_client.add(guest)


@pytest.fixture
def add_second_organization_project_role_as_maintainer(
    add_second_example_organization,
    add_second_example_user,
    add_example_project,
    database_client,
    second_organization_id_str,
    project_id_str,
    created_at,
):
    guest = database.project.OrganizationProjectAssociationTable(
        role=database.project.ProjectRole.MAINTAINER,
        organization_id=uuid.UUID(second_organization_id_str),
        project_id=uuid.UUID(project_id_str),
        created_at=created_at,
    )
    with database_client.session():
        database_client.add(guest)


@pytest.fixture()
def add_database_local_run(
    database_client,
    add_example_project,
    database_run,
) -> None:
    with database_client.session():
        database_run.status = database.run.RunStatus.RUNNING
        database_run.compute_budget_account = None
        database_run.connection_id = None
        database_run.connection = None
        database_client.add(database_run)


@pytest.fixture()
def add_database_run_without_compute_budget_account(
    database_client,
    add_example_project,
    database_run,
) -> None:
    with database_client.session():
        database_run.compute_budget_account = None
        database_client.add(database_run)


@pytest.fixture
def mock_tracking_server_url():
    os.environ[tracking_server_router.TRACKING_SERVER_URL_INTERNAL_ENV_VAR] = os.getenv(
        "TRACKING_SERVER_URI_INTERNAL", _DEFAULT_TRACKING_SERVER_URI
    )
    os.environ[tracking_server_router.TRACKING_SERVER_URL_PUBLIC_ENV_VAR] = os.getenv(
        "TRACKING_SERVER_URI_INTERNAL", _DEFAULT_TRACKING_SERVER_URI
    )
    yield
    os.unsetenv(tracking_server_router.TRACKING_SERVER_URL_INTERNAL_ENV_VAR)
    os.unsetenv(tracking_server_router.TRACKING_SERVER_URL_PUBLIC_ENV_VAR)


@pytest.fixture
def add_database_connection_dvc_of_first_user(
    database_client,
    clear_and_init_database,
    database_connection_dvc_of_first_user,
) -> None:
    with database_client.session():
        database_client.add(database_connection_dvc_of_first_user)


@pytest.fixture
def add_database_git_connection_of_first_user(
    database_client,
    clear_and_init_database,
    database_git_connection_of_first_user,
) -> None:
    with database_client.session():
        database_client.add(database_git_connection_of_first_user)


@pytest.fixture()
def mock_vault_credential_sample_access_token(sample_gitlab_token):
    with unittest.mock.patch(
        "mantik_api.utils.vault.get_credential",
        return_value=utils.vault.Credentials(token=sample_gitlab_token),
    ):
        yield
