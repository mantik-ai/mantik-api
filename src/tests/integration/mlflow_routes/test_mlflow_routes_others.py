import unittest.mock

import pytest

import mantik_api.testing as testing

HEADERS = testing.token_verifier.VALID_AUTHORIZATION_HEADER


@pytest.mark.parametrize(
    "endpoint",
    [
        "/tracking/health",
        "/tracking/version",
    ],
)
@pytest.mark.asyncio
async def test_health_version(
    endpoint: str,
    request,
    client,
    mock_tracking_server_url,
) -> None:
    # Arrange
    expected_status_code = 200

    # Act
    with unittest.mock.patch(
        "requests.request",
        return_value=unittest.mock.Mock(text="hello world", status_code=200),
    ):
        response = client.get(
            endpoint,
            headers=HEADERS,
        )

        # Assert
        assert response.status_code == expected_status_code, response.text


@pytest.mark.asyncio
async def test_proxy_gateway(
    client,
) -> None:
    """Test empty, wellformed response on call to gateway proxies"""
    # Arrange
    endpoint = "/tracking/ajax-api/2.0/mlflow/gateway-proxy"
    expected = {"endpoints": []}

    # Act
    response = client.get(endpoint, headers=HEADERS)

    # Assert
    assert response.status_code == 200
    assert response.json() == expected


@pytest.mark.parametrize(
    "endpoint",
    [
        "/tracking/api/2.0/mlflow/artifacts/list",
        "/tracking/ajax-api/2.0/mlflow/artifacts/list",
    ],
)
@pytest.mark.parametrize(
    "fixtures,expected_status_code",
    [
        (["add_example_run", "mock_token_user_id"], 200),
        (["add_example_run_private", "mock_token_user_id"], 200),
        (["add_example_run_private", "mock_token_second_user_id"], 403),
        (["mock_token_user_id"], 404),
    ],
)
@pytest.mark.asyncio
async def test_artifacts_get_endpoints_with_mlflow_run_id(
    endpoint: str,
    request,
    fixtures: list,
    expected_status_code,
    client,
    add_example_user,
    mock_tracking_server_url,
    api_run,
    api_experiment_repository,
) -> None:
    for fixture in fixtures:
        request.getfixturevalue(fixture)
    with unittest.mock.patch(
        "requests.request",
        return_value=unittest.mock.Mock(text="hello world", status_code=200),
    ):
        response = client.get(
            endpoint,
            params={
                "run_id": api_run.mlflow_run_id.hex,
            },
            headers=HEADERS,
        )
        assert response.status_code == expected_status_code, response.text


@pytest.mark.parametrize(
    "endpoint",
    [
        "/tracking/api/2.0/mlflow-artifacts/artifacts",
        "/tracking/ajax-api/2.0/mlflow-artifacts/artifacts",
    ],
)
@pytest.mark.parametrize(
    "fixtures,expected_status_code",
    [
        (["add_example_run", "mock_token_user_id"], 200),
        (["add_example_run_private", "mock_token_user_id"], 200),
        (["add_example_run_private", "mock_token_second_user_id"], 403),
        (["mock_token_user_id"], 404),
    ],
)
@pytest.mark.asyncio
async def test_get_mlflow_artifacts_direct_endpoints_with_mlflow_run_id(
    endpoint: str,
    request,
    fixtures: list,
    expected_status_code,
    client,
    add_example_user,
    mock_tracking_server_url,
    api_run,
    api_experiment_repository,
) -> None:
    for fixture in fixtures:
        request.getfixturevalue(fixture)
    with unittest.mock.patch(
        "requests.request",
        return_value=unittest.mock.Mock(text="hello world", status_code=200),
    ):
        response = client.get(
            endpoint,
            headers=HEADERS,
            params={
                "path": f"{api_experiment_repository.mlflow_experiment_id}"
                "/run_id/path/to/artifact"
            },
        )
        assert response.status_code == expected_status_code, response.text


@pytest.mark.parametrize("method", ["GET", "PUT", "POST", "DELETE"])
@pytest.mark.parametrize(
    "fixtures,expected_status_code",
    [
        (["add_example_experiment", "mock_token_user_id"], 200),
        (["add_example_experiment_private", "mock_token_user_id"], 200),
        (["add_example_experiment_private", "mock_token_second_user_id"], 403),
        (["mock_token_user_id"], 404),
    ],
)
@pytest.mark.asyncio
async def test_put_artifacts_with_path_rbac_restful(
    request,
    client,
    add_example_user,
    mock_tracking_server_url,
    api_run,
    api_experiment_repository,
    fixtures,
    expected_status_code,
    method,
) -> None:
    for fixture in fixtures:
        request.getfixturevalue(fixture)
    with unittest.mock.patch(
        "requests.request",
        return_value=unittest.mock.Mock(text="hello world", status_code=200),
    ):
        # Arrange
        mlflow_experiment_id = api_experiment_repository.mlflow_experiment_id
        endpoint = "/tracking/api/2.0/mlflow-artifacts/artifacts/"
        endpoint += f"{mlflow_experiment_id}/12312bc/path/to/dir"

        # Act
        response = client.request(
            method=method,
            url=endpoint,
            headers=HEADERS,
            data={},
        )

        # Assert
        assert response.status_code == expected_status_code


@pytest.mark.parametrize("method", ["GET", "PUT", "POST", "DELETE"])
@pytest.mark.parametrize(
    "fixtures,expected_status_code",
    [
        (["add_example_experiment", "mock_token_user_id"], 200),
        (["add_example_experiment_private", "mock_token_user_id"], 200),
        (["add_example_experiment_private", "mock_token_second_user_id"], 403),
        (["mock_token_user_id"], 404),
    ],
)
@pytest.mark.asyncio
async def test_artifacts_with_path_rbac_ajax(
    request,
    client,
    add_example_user,
    mock_tracking_server_url,
    api_run,
    api_experiment_repository,
    fixtures,
    expected_status_code,
    method,
) -> None:
    for fixture in fixtures:
        request.getfixturevalue(fixture)
    with unittest.mock.patch(
        "requests.request",
        return_value=unittest.mock.Mock(text="hello world", status_code=200),
    ):
        # Arrange
        mlflow_experiment_id = api_experiment_repository.mlflow_experiment_id
        endpoint = "/tracking/ajax-api/2.0/mlflow-artifacts/artifacts/"
        endpoint += f"{mlflow_experiment_id}/12312bc/path/to/dir"

        # Act
        response = client.request(
            method=method,
            url=endpoint,
            headers=HEADERS,
            data={},
        )

        # Assert
        assert response.status_code == expected_status_code


@pytest.mark.parametrize(
    "endpoint",
    [
        "/tracking/api/2.0/mlflow/metrics/get-history",
        "/tracking/ajax-api/2.0/mlflow/metrics/get-history",
        "/tracking/ajax-api/2.0/mlflow/metrics/get-history-bulk",
        "/tracking/ajax-api/2.0/mlflow/metrics/get-history-bulk-interval",
    ],
)
@pytest.mark.parametrize(
    "fixtures,expected_status_code",
    [
        (["add_example_run", "mock_token_user_id"], 200),
        (["add_example_run_private", "mock_token_user_id"], 200),
        (["add_example_run_private", "mock_token_second_user_id"], 403),
        (["mock_token_user_id"], 404),
    ],
)
@pytest.mark.asyncio
async def test_metrics_get_endpoints_with_mlflow_run_id(
    endpoint: str,
    request,
    fixtures: list,
    expected_status_code,
    client,
    add_example_user,
    mock_tracking_server_url,
    api_run,
    api_experiment_repository,
) -> None:
    for fixture in fixtures:
        request.getfixturevalue(fixture)
    with unittest.mock.patch(
        "requests.request",
        return_value=unittest.mock.Mock(text="hello world", status_code=200),
    ):
        response = client.get(
            endpoint,
            params={
                "run_id": api_run.mlflow_run_id.hex,
            },
            headers=HEADERS,
        )
        assert response.status_code == expected_status_code, response.text


@pytest.mark.parametrize(
    "fixtures,expected_status_code",
    [
        (["add_example_run", "mock_token_user_id"], 200),
        (["add_example_run_private", "mock_token_user_id"], 200),
        (["add_example_run_private", "mock_token_second_user_id"], 403),
        (["mock_token_user_id"], 404),
    ],
)
@pytest.mark.asyncio
async def test_get_artifact_endpoint_with_mlflow_run_id(
    request,
    fixtures: list,
    expected_status_code,
    client,
    add_example_user,
    mock_tracking_server_url,
    api_run,
    api_experiment_repository,
) -> None:
    for fixture in fixtures:
        request.getfixturevalue(fixture)
    with unittest.mock.patch(
        "requests.request",
        return_value=unittest.mock.Mock(content=b"hello world", status_code=200),
    ):
        endpoint = "/tracking/get-artifact"
        response = client.get(
            endpoint,
            params={
                "run_id": api_run.mlflow_run_id.hex,
            },
            headers=HEADERS,
        )
        assert response.status_code == expected_status_code, response.text
