import fastapi.testclient as testclient
import pytest
import starlette.status as status

import mantik_api.routes.mlflow as mlflow_routes
import mantik_api.testing as testing


@pytest.mark.parametrize(
    ["request_data", "expected"],
    [
        (
            {"endpoint": f"{mlflow_routes.experiments.EXPERIMENTS_PATH}create"},
            {
                "allowed": False,
                "message": "Experiments can only be created on the Mantik platform.",
                "experimentIds": None,
            },
        ),
        (
            {
                "endpoint": f"{mlflow_routes.experiments.EXPERIMENTS_PATH}search",
            },
            {
                "allowed": True,
                "message": "Allowed experiments are passed "
                "in the experimentIds field.",
                "experimentIds": [
                    "second_database_experiment_repository",
                    "database_experiment_repository",
                ],
            },
        ),
        (
            {
                "endpoint": f"{mlflow_routes.experiments.EXPERIMENTS_PATH}get",
                "experimentId": "database_experiment_repository",
            },
            {
                "allowed": True,
                "message": (
                    "User is allowed to see the " "experiment with ID {experiment_id}."
                ),
                "experimentIds": None,
            },
        ),
        (
            {
                "endpoint": f"{mlflow_routes.experiments.EXPERIMENTS_PATH}get-by-name",
                "experimentName": "database_experiment_repository",
            },
            {
                "allowed": True,
                "message": "User is allowed to see "
                "the experiment with name {experiment_name}.",
                "experimentIds": None,
            },
        ),
        (
            {
                "endpoint": f"{mlflow_routes.experiments.EXPERIMENTS_PATH}delete",
                "experimentId": "database_experiment_repository",
            },
            {
                "allowed": False,
                "message": "Experiments can only be deleted on the Mantik platform.",
                "experimentIds": None,
            },
        ),
        (
            {
                "endpoint": f"{mlflow_routes.experiments.EXPERIMENTS_PATH}restore",
                "experimentId": "database_experiment_repository",
            },
            {
                "allowed": False,
                "message": "Restoring an experiment is not yet possible in Mantik.",
                "experimentIds": None,
            },
        ),
        (
            {
                "endpoint": f"{mlflow_routes.experiments.EXPERIMENTS_PATH}update",
                "experimentId": "database_experiment_repository",
            },
            {
                "allowed": False,
                "message": "Experiments can only be updated on the Mantik platform.",
                "experimentIds": None,
            },
        ),
        (
            {
                "endpoint": f"{mlflow_routes.experiments.EXPERIMENTS_PATH}"
                "set-experiment-tag",
                "experimentId": "database_experiment_repository",
            },
            {
                "allowed": False,
                "message": "MLflow tags are not supported, use Mantik labels instead.",
                "experimentIds": None,
            },
        ),
        (
            {
                "endpoint": f"{mlflow_routes.experiments.EXPERIMENTS_PATH}new-endpoint",
                "experimentId": "database_experiment_repository",
            },
            {
                "allowed": False,
                "message": "Not yet supported by Mantik.",
                "experimentIds": None,
            },
        ),
    ],
)
@pytest.mark.usefixtures("add_2_example_projects")
@pytest.mark.usefixtures("add_example_user")
@pytest.mark.usefixtures("mock_token_user_id")
def test_mlflow_permissions_experiments_post(
    client: testclient.TestClient, request, request_data, expected
):
    request_data, expected = _put_experiment_id_or_name(
        request, request_data=request_data, expected=expected
    )

    response = client.request(
        "POST",
        "/mlflow/permissions/experiments",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=request_data,
    )

    assert response.status_code == status.HTTP_200_OK
    assert response.json() == expected


@pytest.mark.parametrize(
    ["request_data", "expected"],
    [
        (
            {
                "endpoint": f"{mlflow_routes.experiments.EXPERIMENTS_PATH}search",
            },
            {
                "allowed": True,
                "message": "Allowed experiments are passed "
                "in the experimentIds field.",
                "experimentIds": [],
            },
        ),
        (
            {
                "endpoint": f"{mlflow_routes.experiments.EXPERIMENTS_PATH}get",
                "experimentId": "database_experiment_repository",
            },
            {
                "allowed": False,
                "message": (
                    "User is not allowed to see the "
                    "experiment with ID {experiment_id}."
                ),
                "experimentIds": None,
            },
        ),
        (
            {
                "endpoint": f"{mlflow_routes.experiments.EXPERIMENTS_PATH}get-by-name",
                "experimentName": "database_experiment_repository",
            },
            {
                "allowed": False,
                "message": "User is not allowed to see "
                "the experiment with name {experiment_name}.",
                "experimentIds": None,
            },
        ),
    ],
)
@pytest.mark.usefixtures("add_example_project_private")
@pytest.mark.usefixtures("add_second_example_user")
@pytest.mark.usefixtures("mock_token_second_user_id")
def test_mlflow_permissions_experiments_post_private_project(
    client: testclient.TestClient, request, request_data, expected
):
    request_data, expected = _put_experiment_id_or_name(
        request, request_data=request_data, expected=expected
    )
    response = client.request(
        "POST",
        "/mlflow/permissions/experiments",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json=request_data,
    )

    assert response.status_code == status.HTTP_200_OK
    result = response.json()
    assert result == expected


def _put_experiment_id_or_name(
    request, request_data: dict, expected: dict
) -> tuple[dict, dict]:
    if "experimentId" in request_data:
        request_data["experimentId"] = request.getfixturevalue(
            request_data["experimentId"]
        ).mlflow_experiment_id
        expected["message"] = expected["message"].format(
            experiment_id=request_data["experimentId"]
        )
    elif "experimentName" in request_data:
        request_data["experimentName"] = request.getfixturevalue(
            request_data["experimentName"]
        ).name
        expected["message"] = expected["message"].format(
            experiment_name=request_data["experimentName"]
        )

    if expected["experimentIds"] is not None:
        expected["experimentIds"] = [
            request.getfixturevalue(name).mlflow_experiment_id
            for name in expected["experimentIds"]
        ]
    return request_data, expected
