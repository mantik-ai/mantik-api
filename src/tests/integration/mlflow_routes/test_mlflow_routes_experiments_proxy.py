import json
import unittest.mock

import pytest

import mantik_api.testing as testing

HEADERS = testing.token_verifier.VALID_AUTHORIZATION_HEADER


@pytest.mark.asyncio
async def test_experiments_create_endpoint(
    client,
    mock_tracking_server_url,
    mock_token_user_id,
    add_example_user,
    api_experiment_repository,
):
    endpoint = "/tracking/api/2.0/mlflow/experiments/create"
    json_payload = {
        "experiment_id": api_experiment_repository.mlflow_experiment_id,
    }

    with unittest.mock.patch(
        "requests.request",
        return_value=unittest.mock.Mock(text="hello world", status_code=200),
    ) as mocked_request:
        response = client.post(
            endpoint,
            json=json_payload,
            headers=HEADERS,
        )
        assert response.status_code == 200, response.text

        mocked_request.assert_called_once()
        _, kwargs = mocked_request.call_args
        assert kwargs["json"] == json_payload


@pytest.mark.parametrize(
    "endpoint",
    [
        "/tracking/api/2.0/mlflow/experiments/delete",
        "/tracking/api/2.0/mlflow/experiments/restore",
        "/tracking/api/2.0/mlflow/experiments/update",
        "/tracking/api/2.0/mlflow/experiments/set-experiment-tag",
    ],
)
@pytest.mark.parametrize(
    "fixtures,expected_status_code",
    [
        (["add_example_experiment", "mock_token_user_id"], 200),
        (["add_example_experiment", "mock_token_second_user_id"], 403),
        (["mock_token_user_id"], 404),
    ],
)
@pytest.mark.asyncio
async def test_experiments_get_endpoints(
    endpoint: str,
    request,
    fixtures: list,
    expected_status_code,
    client,
    add_example_user,
    mock_tracking_server_url,
    api_experiment_repository,
) -> None:
    for fixture in fixtures:
        request.getfixturevalue(fixture)
    with unittest.mock.patch(
        "requests.request",
        return_value=unittest.mock.Mock(text="hello world", status_code=200),
    ):
        response = client.post(
            endpoint,
            json={
                "experiment_id": api_experiment_repository.mlflow_experiment_id,
            },
            headers=HEADERS,
        )
        assert response.status_code == expected_status_code, response.text


@pytest.mark.parametrize(
    "endpoint",
    [
        "/tracking/api/2.0/mlflow/experiments/get",
        "/tracking/api/2.0/mlflow/experiments/get-by-name",
        "/tracking/ajax-api/2.0/mlflow/experiments/get",
        "/tracking/ajax-api/2.0/mlflow/experiments/get-by-name",
    ],
)
@pytest.mark.parametrize(
    "fixtures,expected_status_code",
    [
        (["add_example_experiment", "mock_token_user_id"], 200),
        (["add_example_experiment", "mock_token_second_user_id"], 200),
        (["mock_token_user_id"], 404),
    ],
)
@pytest.mark.asyncio
async def test_experiment_get_endpoints(
    endpoint: str,
    request,
    fixtures: list,
    expected_status_code,
    client,
    add_example_user,
    mock_tracking_server_url,
    api_experiment_repository,
):
    for fixture in fixtures:
        request.getfixturevalue(fixture)
    with unittest.mock.patch(
        "requests.request",
        return_value=unittest.mock.Mock(text="hello world", status_code=200),
    ):
        response = client.get(
            endpoint,
            params={
                "experiment_id": api_experiment_repository.mlflow_experiment_id,
                "experiment_name": api_experiment_repository.name,
            },
            headers=HEADERS,
        )
        assert response.status_code == expected_status_code, response.text


@pytest.mark.parametrize(
    "method",
    [
        "POST",
        "GET",
    ],
)
@pytest.mark.parametrize(
    "endpoint",
    [
        "/tracking/api/2.0/mlflow/experiments/search",
        "/tracking/ajax-api/2.0/mlflow/experiments/search",
    ],
)
@pytest.mark.asyncio
async def test_experiments_search(
    client,
    method: str,
    endpoint: str,
    add_example_user,
    add_example_project,
    api_experiment_repository,
    mock_tracking_server_url,
    mock_token_user_id,
) -> None:
    sample_mlflow_experiment_response = {
        "experiments": [
            {"experiment_id": str(api_experiment_repository.mlflow_experiment_id)},
            {"experiment_id": "8"},
        ]
    }

    with unittest.mock.patch(
        "requests.request",
        return_value=unittest.mock.Mock(
            text=json.dumps(sample_mlflow_experiment_response),
            status_code=200,
        ),
    ):
        response = client.request(
            method=method,
            url=endpoint,
            json={
                "experiment_ids": [str(api_experiment_repository.mlflow_experiment_id)]
            },
            headers=HEADERS,
        )
        assert response.status_code == 200, response.text
        assert response.json() == {
            "experiments": [
                {
                    "experiment_id": str(
                        api_experiment_repository.mlflow_experiment_id
                    )  # noqa E231
                }
            ]
        }


@pytest.mark.parametrize(
    "method",
    [
        "POST",
        "GET",
    ],
)
@pytest.mark.parametrize(
    "endpoint",
    [
        "/tracking/api/2.0/mlflow/experiments/search",
        "/tracking/ajax-api/2.0/mlflow/experiments/search",
    ],
)
@pytest.mark.asyncio
async def test_experiments_search_no_experiments_found(
    client,
    method: str,
    endpoint: str,
    add_example_user,
    add_example_project,
    api_experiment_repository,
    mock_tracking_server_url,
    mock_token_user_id,
) -> None:
    sample_mlflow_experiment_response = {}

    with unittest.mock.patch(
        "requests.request",
        return_value=unittest.mock.Mock(
            text=json.dumps(sample_mlflow_experiment_response),
            status_code=200,
        ),
    ):
        response = client.request(
            method=method,
            url=endpoint,
            json={
                "experiment_ids": [str(api_experiment_repository.mlflow_experiment_id)]
            },
            headers=HEADERS,
        )
        assert response.status_code == 200, response.text
        assert response.json() == {}


@pytest.mark.asyncio
async def test_search_datasets(
    client,
) -> None:
    """Test empty, wellformed response on call to search-dataset"""
    # Arrange
    endpoint = "/tracking/ajax-api/2.0/mlflow/experiments/search-datasets"
    expected = {"data_summaries": []}

    # Act
    response = client.post(endpoint, headers=HEADERS)

    # Assert
    assert response.status_code == 200
    assert response.json() == expected
