import mantik_api.routes.mlflow.proxied_mlflow_endpoints as proxied_mlflow_endpoints
import mantik_api.routes.mlflow.tracking_server_router as tracking_server_router

router_prefix = tracking_server_router.MLFLOW_ROUTER_PREFIX


def test_all_mlflow_endpoints_to_proxy_are_defined(app) -> None:
    # collect all endpoints of mantik api, in format {"/endpoint/path": {"GET", "POST"}}
    app_routes_with_methods = {}
    for route in app.routes:
        app_routes_with_methods[route.path] = (
            app_routes_with_methods.get(route.path, set()) | route.methods
        )

    # for each endpoint expected to be proxied, assert that it exists on the api
    for endpoint, method in proxied_mlflow_endpoints.endpoints_to_proxy:
        endpoint_with_prefix = router_prefix + endpoint
        methods_defined_for_route = app_routes_with_methods.get(
            endpoint_with_prefix, {}
        )

        assert (
            method in methods_defined_for_route
        ), f"{method} {endpoint_with_prefix} is not defined on Mantik API"
