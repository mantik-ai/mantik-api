import json
import unittest.mock

import pytest

import mantik_api.testing as testing

HEADERS = testing.token_verifier.VALID_AUTHORIZATION_HEADER


@pytest.mark.parametrize(
    "endpoint",
    [
        "/tracking/api/2.0/mlflow/runs/delete",
        "/tracking/api/2.0/mlflow/runs/restore",
        "/tracking/api/2.0/mlflow/runs/log-metric",
        "/tracking/api/2.0/mlflow/runs/log-batch",
        "/tracking/api/2.0/mlflow/runs/log-model",
        "/tracking/api/2.0/mlflow/runs/log-inputs",
        "/tracking/api/2.0/mlflow/runs/set-tag",
        "/tracking/api/2.0/mlflow/runs/delete-tag",
        "/tracking/api/2.0/mlflow/runs/log-parameter",
        "/tracking/api/2.0/mlflow/runs/update",
        "/tracking/api/2.0/mlflow/runs/create",
    ],
)
@pytest.mark.parametrize(
    "fixtures,expected_status_code",
    [
        (["add_example_run", "mock_token_user_id"], 200),
        (["add_example_run", "mock_token_second_user_id"], 403),
        (["add_example_run_private", "mock_token_user_id"], 200),
        (["add_example_run_private", "mock_token_second_user_id"], 403),
        (["mock_token_user_id"], 404),
    ],
)
@pytest.mark.asyncio
async def test_runs_post_endpoints_with_mlflow_run_id(
    endpoint: str,
    request,
    fixtures: list,
    expected_status_code,
    client,
    add_example_user,
    mock_tracking_server_url,
    api_run,
    api_experiment_repository,
) -> None:
    for fixture in fixtures:
        request.getfixturevalue(fixture)
    with unittest.mock.patch(
        "requests.request",
        return_value=unittest.mock.Mock(text="hello world", status_code=200),
    ):
        response = client.post(
            endpoint,
            json={
                "run_id": api_run.mlflow_run_id.hex,
                "experiment_id": api_experiment_repository.mlflow_experiment_id,
            },
            headers=HEADERS,
        )
        assert response.status_code == expected_status_code, response.text


@pytest.mark.parametrize(
    "endpoint",
    [
        "/tracking/api/2.0/mlflow/runs/get",
        "/tracking/ajax-api/2.0/mlflow/runs/get",
    ],
)
@pytest.mark.parametrize(
    "fixtures,expected_status_code",
    [
        (["add_example_run", "mock_token_user_id"], 200),
        (["add_example_run", "mock_token_second_user_id"], 200),
        (["add_example_run_private", "mock_token_user_id"], 200),
        (["add_example_run_private", "mock_token_second_user_id"], 403),
        (["mock_token_user_id"], 404),
    ],
)
@pytest.mark.asyncio
async def test_runs_get_endpoints(
    endpoint: str,
    request,
    fixtures: list,
    expected_status_code,
    client,
    add_example_user,
    mock_tracking_server_url,
    api_run,
) -> None:
    for fixture in fixtures:
        request.getfixturevalue(fixture)
    with unittest.mock.patch(
        "requests.request",
        return_value=unittest.mock.Mock(text="hello world", status_code=200),
    ):
        response = client.get(
            endpoint,
            params={"run_id": api_run.mlflow_run_id.hex},
            headers=HEADERS,
        )
        assert response.status_code == expected_status_code, response.text


@pytest.mark.parametrize(
    "endpoint",
    [
        "/tracking/api/2.0/mlflow/runs/search",
        "/tracking/ajax-api/2.0/mlflow/runs/search",
    ],
)
@pytest.mark.asyncio
async def test_runs_search(
    client,
    endpoint: str,
    add_example_user,
    add_example_run,
    add_example_project,
    api_experiment_repository,
    mock_tracking_server_url,
    mock_token_user_id,
    api_run,
) -> None:
    sample_mlflow_runs_response = {
        "runs": [
            {"info": {"run_id": api_run.mlflow_run_id.hex}},
            {"info": {"run_id": "some-other-run-id"}},
        ]
    }

    with unittest.mock.patch(
        "requests.request",
        return_value=unittest.mock.Mock(
            text=json.dumps(sample_mlflow_runs_response),
            status_code=200,
        ),
    ):
        response = client.post(
            endpoint,
            json={"experiment_ids": [api_experiment_repository.mlflow_experiment_id]},
            headers=HEADERS,
        )
        assert response.status_code == 200, response.text
        assert response.json() == {
            "runs": [{"info": {"run_id": api_run.mlflow_run_id.hex}}]
        }


@pytest.mark.parametrize(
    "endpoint",
    [
        "/tracking/api/2.0/mlflow/runs/search",
        "/tracking/ajax-api/2.0/mlflow/runs/search",
    ],
)
@pytest.mark.asyncio
async def test_runs_search_no_runs_found(
    client,
    endpoint: str,
    add_example_user,
    add_example_run,
    add_example_project,
    api_experiment_repository,
    mock_tracking_server_url,
    mock_token_user_id,
    api_run,
) -> None:
    sample_mlflow_runs_response = {}

    with unittest.mock.patch(
        "requests.request",
        return_value=unittest.mock.Mock(
            text=json.dumps(sample_mlflow_runs_response),
            status_code=200,
        ),
    ):
        response = client.post(
            endpoint,
            json={"experiment_ids": [api_experiment_repository.mlflow_experiment_id]},
            headers=HEADERS,
        )
        assert response.status_code == 200, response.text
        assert response.json() == {}
