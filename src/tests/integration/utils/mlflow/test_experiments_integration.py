import uuid

import pytest

import mantik_api.utils.mlflow as mlflow


@pytest.mark.parametrize(
    ("exists", "new", "expected"),
    [
        (["test-name"], "test-name", "test-name-1"),
        (["test-name", "test-name-1"], "test-name-1", "test-name-2"),
        (["test-name-1"], "test-name-1", "test-name"),
    ],
)
def test_create_unique_name(fake_token, exists, new, expected, set_mlflow_tracking_url):
    # Need to prepend UUID to each name because of experiments
    # not being deleted by MLflow (will lead to duplicate names when
    # re-running the tests).
    prefix = uuid.uuid4()
    expected = f"{prefix}-{expected}"

    for name in exists:
        mlflow.experiments.create(experiment_name=f"{prefix}-{name}", token=fake_token)

    result = mlflow.experiments.create_unique_name(
        name=f"{prefix}-{new}", token=fake_token
    )

    assert result == expected


def test_create_with_duplicate_name(fake_token, set_mlflow_tracking_url):
    name = "Default"
    unique_name = mlflow.experiments.create_unique_name(name=name, token=fake_token)

    experiment_id = mlflow.experiments.create(
        experiment_name=unique_name, token=fake_token
    )

    mlflow.experiments.delete(experiment_id=experiment_id, token=fake_token)


def test_create_duplicate_name_fails(
    expect_raise_if_exception, fake_token, set_mlflow_tracking_url
):
    name = str(uuid.uuid4())
    expected = f"Experiment '{name}' already exists"

    experiment_id = mlflow.experiments.create(experiment_name=name, token=fake_token)

    with expect_raise_if_exception(mlflow.exceptions.DuplicateResourceException()) as e:
        mlflow.experiments.create(experiment_name=name, token=fake_token)

    mlflow.experiments.delete(experiment_id=experiment_id, token=fake_token)

    result = str(e.value)
    assert result == expected


def test_update(create_mlflow_experiment, fake_token, set_mlflow_tracking_url):
    name = f"test-name-updated-{uuid.uuid4()}"
    mlflow.experiments.update(
        experiment_id=create_mlflow_experiment(), experiment_name=name, token=fake_token
    )

    [result] = mlflow.experiments.search(
        filter_by=f"name LIKE '{name}'", token=fake_token
    )

    assert result.name == name


def test_delete(fake_token, set_mlflow_tracking_url):
    name = str(uuid.uuid4())
    experiment_id = mlflow.experiments.create(experiment_name=name, token=fake_token)

    mlflow.experiments.delete(experiment_id=experiment_id, token=fake_token)
