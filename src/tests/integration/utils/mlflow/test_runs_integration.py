import uuid

import pytest

import mantik_api.testing as testing
import mantik_api.utils.mlflow as mlflow


@pytest.mark.parametrize(
    ("exists", "new", "expected"),
    [
        (["test-name"], "test-name", "test-name-1"),
        (["test-name", "test-name-1"], "test-name-1", "test-name-2"),
        (["test-name-1"], "test-name-1", "test-name"),
    ],
)
def test_create_unique_name(
    create_mlflow_experiment,
    mock_tracking_server_url,
    fake_token,
    exists,
    new,
    expected,
):
    experiment_id = create_mlflow_experiment()

    for name in exists:
        testing.mlflow.create_run(
            experiment_id=experiment_id, name=name, token=fake_token
        )

    result = mlflow.runs.create_unique_name(
        experiment_id=experiment_id, name=new, token=fake_token
    )

    assert result == expected


def test_delete(create_mlflow_experiment, mock_tracking_server_url, fake_token):
    run_id = testing.mlflow.create_run(
        experiment_id=create_mlflow_experiment(), name="test-run", token=fake_token
    )

    mlflow.runs.delete(run_id, token=fake_token)


def test_get_model_uri(fake_token, run_id_str):
    expected_model_uri = "mlflow-artifacts:/something/model"

    def _list_artifact_directory(run_id: uuid.UUID, token, path: str | None = None):
        """Mocks the artifacts/list behaviour of MLflow"""

        if path == "model":
            return {"files": [{"path": "model/MLmodel", "is_dir": False}]}

        if path == "another_model":
            return {"files": [{"path": "another_model/MLmodel", "is_dir": False}]}

        return {
            "root_uri": "mlflow-artifacts:/something",
            "files": [
                {"path": "model", "is_dir": True},
                {"path": "another_model", "is_dir": True},
            ],
        }

    actual = mlflow.runs.get_model_uri(
        mlflow_run_id=uuid.UUID(run_id_str),
        token=fake_token,
        list_artifact_directory=_list_artifact_directory,
    )

    assert actual == expected_model_uri
