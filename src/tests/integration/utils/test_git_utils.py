import pytest

import mantik_api.database.git_repository
import mantik_api.utils.git_utils as git_utils

SAMPLE_TOKEN = "1234"
GITLAB_URL_SAMPLE = "https://gitlab.com/project/mantik-tutorials"
SELF_HOSTED_GITLAB_URL_SAMPLE = "https://gl.self-hosted.de/project/mantik-tutorials"
GITHUB_URL_SAMPLE = "https://github.com/FakeUser/mantik-tutorials"


@pytest.mark.parametrize(
    ("git_url", "filepath", "ref", "expected"),
    (
        (
            GITLAB_URL_SAMPLE,
            "minimal-mantik-project/mlproject/MLproject",
            "main",
            "https://gitlab.com/api/v4/projects/project%2Fmantik-tutorials/repository/files/minimal-mantik-project%2Fmlproject%2FMLproject/raw?ref=main",  # noqa E501
        ),
        (
            SELF_HOSTED_GITLAB_URL_SAMPLE,
            "minimal-mantik-project/mlproject/MLproject",
            "main",
            "https://gl.self-hosted.de/api/v4/projects/project%2Fmantik-tutorials/repository/files/minimal-mantik-project%2Fmlproject%2FMLproject/raw?ref=main",  # noqa E501
        ),
        (
            "https://gitlab.com/project",
            "path/MLproject",
            "commit-hash",
            "https://gitlab.com/api/v4/projects/project/repository/files/path%2FMLproject/raw?ref=commit-hash",  # noqa E501
        ),
    ),
    ids=["public gitlab", "self-hosted gitlab", "different ref and filepath"],
)
def test_raw_file_gitlab_url(
    git_url: str, filepath: str, ref: str, expected: str
) -> None:
    assert (
        git_utils.raw_file_gitlab_url(url=git_url, filepath=filepath, ref=ref)
        == expected
    )


@pytest.mark.parametrize(
    ("git_url", "expected"),
    (
        (
            GITLAB_URL_SAMPLE,
            "https://gitlab.com/api/v4/projects/project%2Fmantik-tutorials",
        ),
        (
            SELF_HOSTED_GITLAB_URL_SAMPLE,
            "https://gl.self-hosted.de/api/v4/projects/project%2Fmantik-tutorials",
        ),
    ),
    ids=["public gitlab", "self-hosted gitlab"],
)
def test_raw_gitlab_url(git_url: str, expected: str) -> None:
    assert git_utils.raw_gitlab_url(url=git_url) == expected


def test_raw_github_url() -> None:
    expected = "https://api.github.com/repos/FakeUser/mantik-tutorials"
    assert git_utils.raw_github_url(url=GITHUB_URL_SAMPLE) == expected


def test_raw_bitbucket_url() -> None:
    bitbucket_url_sample = (
        "https://bitbucket.org/sonarsource/sample-maven-project/src/master/src/"
    )
    expected = "https://api.bitbucket.org/2.0/repositories/sonarsource/sample-maven-project/src/master/src/"  # noqa E50§
    assert git_utils.raw_bitbucket_url(url=bitbucket_url_sample) == expected


def test_raw_file_github_url() -> None:
    repo = "FakeUser/mantik-tutorials"
    filepath = "minimal-mantik-project/mlproject/MLproject"
    expected = "https://raw.githubusercontent.com/FakeUser/mantik-tutorials/main/minimal-mantik-project/mlproject/MLproject"  # noqa E501
    assert (
        git_utils.raw_file_github_url(repo=repo, filepath=filepath, ref="main")
        == expected
    )


@pytest.mark.parametrize(
    ("repo", "filepath", "ref", "domain", "expected"),
    (
        (
            "mantik-tutorials",
            "minimal-mantik-project/mlproject/MLproject",
            "main",
            "bitbucket.org",
            "mantik-tutorials/raw/main/minimal-mantik-project/mlproject/MLproject",
        ),
        (
            "mantik-tutorials",
            "minimal-mantik-project/mlproject/MLproject",
            "main",
            "bb.self-hosted",
            "mantik-tutorials/browse/minimal-mantik-project/mlproject/MLproject?at=main",  # noqa E501
        ),
    ),
    ids=["public bitbucket", "self-hosted bitbucket"],
)
def test_raw_file_bitbucket_url(
    repo: str, filepath: str, ref: str, domain: str, expected: str
) -> None:
    assert (
        git_utils.raw_file_bitbucket_url(
            repo=repo, filepath=filepath, ref=ref, domain=domain
        )
        == expected
    )


@pytest.mark.parametrize(
    "platform,expected_header",
    (
        [
            mantik_api.database.git_repository.Platform.GitLab,
            {"PRIVATE-TOKEN": SAMPLE_TOKEN},
        ],
        [
            mantik_api.database.git_repository.Platform.GitHub,
            {"Authorization": f"token {SAMPLE_TOKEN}"},
        ],
    ),
)
def test_git_authorization_header(
    platform: mantik_api.database.git_repository.Platform, expected_header: dict
) -> None:
    assert (
        git_utils.git_authorization_header(access_token=SAMPLE_TOKEN, platform=platform)
        == expected_header
    )
