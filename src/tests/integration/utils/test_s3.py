import os
import tempfile
import zipfile

import boto3
import moto
import pytest

import mantik_api.utils.s3 as utils_s3


@pytest.fixture
def sample_bucket_name():
    return "TEST"


@pytest.fixture
def sample_key_1() -> str:
    return "something/prefix-1/empty-1.txt"


@pytest.fixture
def sample_key_2() -> str:
    return "something/prefix-1/model/empty-2.txt"


@pytest.fixture
def sample_key_3() -> str:
    return "something/prefix-2/empty-3.txt"


@pytest.fixture
def mock_s3_bucket_sample_run_artifacts(
    sample_bucket_name, sample_text_file_path, sample_key_1, sample_key_2, sample_key_3
) -> None:
    with moto.mock_s3():
        s3 = boto3.client("s3")
        s3.create_bucket(
            Bucket=sample_bucket_name,
            CreateBucketConfiguration={"LocationConstraint": "eu-central-1"},
        )

        # Upload sample file multiple times under different s3 keys
        for sample_key in [sample_key_1, sample_key_2, sample_key_3]:
            s3.upload_file(sample_text_file_path, sample_bucket_name, sample_key)

        yield


def test_list_dir(
    mock_s3_bucket_sample_run_artifacts,
    sample_bucket_name,
    sample_key_1,
    sample_key_2,
    sample_key_3,
) -> None:
    # without prefix
    all_keys_in_bucket = utils_s3.list_dir(bucket_name=sample_bucket_name, prefix="")
    assert sorted(all_keys_in_bucket) == sorted(
        [
            sample_key_1,
            sample_key_2,
            sample_key_3,
        ]
    )

    # with prefix
    keys_in_bucket_with_prefix = utils_s3.list_dir(
        bucket_name=sample_bucket_name, prefix="something/prefix-1"
    )
    assert sorted(keys_in_bucket_with_prefix) == sorted([sample_key_1, sample_key_2])


def test_key_exists(
    mock_s3_bucket_sample_run_artifacts,
    sample_bucket_name,
    sample_key_1,
) -> None:
    assert utils_s3.key_exists(sample_bucket_name, sample_key_1)
    assert not utils_s3.key_exists(sample_bucket_name, "non-existent-key")


def test_zip_s3_directory(
    mock_s3_bucket_sample_run_artifacts,
    sample_bucket_name,
) -> None:
    zipped_folder_key = "something/zipped.zip"

    # trigger zipping of s3 directory
    utils_s3.zip_s3_directory(
        sample_bucket_name, prefix="something/prefix-1", target_key=zipped_folder_key
    )

    # check that a new zipped object exists in s3
    assert utils_s3.key_exists(sample_bucket_name, zipped_folder_key)

    # download zip file from s3, and test structure
    with tempfile.TemporaryDirectory() as tmp_dir:
        local_zipped_folder_path = os.path.join(tmp_dir, "zipped.zip")
        extract_to_directory = os.path.join(tmp_dir, "unzipped")

        with open(local_zipped_folder_path, "wb") as f:
            boto3.client("s3").download_fileobj(
                sample_bucket_name, zipped_folder_key, f
            )

        with zipfile.ZipFile(local_zipped_folder_path, "r") as zip_ref:
            zip_ref.extractall(extract_to_directory)

        # expected structure is:
        # tmp_dir/unzipped
        #    |
        #    +-- empty-1.txt
        #    |
        #    +-- model
        #    |  |
        #    |  \-- empty-2.txt
        #    |

        assert sorted(os.listdir(tmp_dir)) == sorted(["unzipped", "zipped.zip"])
        assert sorted(os.listdir(extract_to_directory)) == sorted(
            ["empty-1.txt", "model"]
        )
        assert os.listdir(os.path.join(extract_to_directory, "model")) == [
            "empty-2.txt"
        ]


def test_delete_key(
    mock_s3_bucket_sample_run_artifacts,
    sample_bucket_name,
    sample_key_1,
) -> None:
    all_keys = utils_s3.list_dir(bucket_name=sample_bucket_name, prefix="")
    deleted_key = sample_key_1
    utils_s3.delete_key(bucket_name=sample_bucket_name, key=sample_key_1)
    remaining_keys = utils_s3.list_dir(bucket_name=sample_bucket_name, prefix="")
    assert sorted(all_keys) == sorted(remaining_keys + [deleted_key])


def test_delete_non_existent_key(
    mock_s3_bucket_sample_run_artifacts,
    sample_bucket_name,
) -> None:
    all_keys = utils_s3.list_dir(bucket_name=sample_bucket_name, prefix="")
    deleted_key = "random_key"
    utils_s3.delete_key(bucket_name=sample_bucket_name, key=deleted_key)
    remaining_keys = utils_s3.list_dir(bucket_name=sample_bucket_name, prefix="")
    assert all_keys == remaining_keys
