import uuid

import pytest

import mantik_api.database as database


@pytest.fixture()
def database_label_6(api_label_5) -> database.label.Label:
    return database.label.Label(
        id=uuid.uuid4(),
        identifier="0014",
        scopes=api_label_5.scopes,
        category=api_label_5.category,
        sub_category=api_label_5.sub_category,
        name="test-label-6",
    )


@pytest.fixture(autouse=True)
def add_database_label_6(database_client, database_label_6) -> None:
    with database_client.session():
        database_client.add(database_label_6)


class TestProject:
    @pytest.mark.parametrize(
        ("labels", "expected"),
        [
            # Test case: None given
            (
                None,
                [
                    "database_project_labels_3_to_5",
                    "database_project_labels_1_to_5",
                ],
            ),
            # Test case: No labels given
            (
                [],
                [
                    "database_project_labels_3_to_5",
                    "database_project_labels_1_to_5",
                ],
            ),
            # Test case: match all labels of a project
            (
                [
                    "database_label_1",
                    "database_label_2",
                    "database_label_3",
                    "database_label_4",
                    "database_label_5",
                ],
                [
                    "database_project_labels_1_to_5",
                ],
            ),
            # Test case: match only one label of a project
            (
                [
                    "database_label_2",
                ],
                [
                    "database_project_labels_1_to_5",
                ],
            ),
            # Test case: match one label assigned to two projects
            (
                [
                    "database_label_3",
                ],
                [
                    "database_project_labels_3_to_5",
                    "database_project_labels_1_to_5",
                ],
            ),
            # Test case: match one label assigned to two projects
            (
                [
                    "database_label_3",
                ],
                [
                    "database_project_labels_3_to_5",
                    "database_project_labels_1_to_5",
                ],
            ),
        ],
    )
    @pytest.mark.usefixtures("add_example_project_with_labels_1_to_5")
    @pytest.mark.usefixtures("add_example_project_with_labels_3_to_5")
    def test_matches_labels(
        self,
        request,
        database_client,
        database_user,
        labels,
        expected,
    ):
        with database_client.session():
            labels = (
                [request.getfixturevalue(label).id for label in labels]
                if labels is not None
                else None
            )
            expected = [request.getfixturevalue(project).id for project in expected]

            constraint = database.project.Project.matches_labels(labels)

            if labels is None:
                assert constraint
            else:
                result: database.client.main.GetAllResponse = database_client.get_all(
                    database.project.Project,
                    constraints=constraint,
                )
                result_project_ids = [res.id for res in result.entities]

                assert result_project_ids == expected
