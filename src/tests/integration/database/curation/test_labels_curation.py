import os

import pytest

import mantik_api.database as database
import mantik_api.database.curation.labels as _labels
import mantik_api.testing as testing
import mantik_api.utils.env as env


@pytest.mark.usefixtures("add_labels_for_curation_tests")
def test_crud_labels(monkeypatch, database_client, labels_yaml_test_path):
    monkeypatch.setattr(
        database.client.main.Client,
        "from_env",
        lambda: database_client,
    )

    with env.env_vars_set(
        {_labels.LABELS_YAML_PATH_ENV_VAR: str(labels_yaml_test_path)}
    ):
        _labels.crud_labels()

    testing.labels.assert_labels_crud_correct(database_client)


@pytest.mark.usefixtures("add_labels_for_curation_tests")
def test_crud_labels_env_var_unset(caplog):
    expected = (
        f"Skipping curation of labels since {_labels.LABELS_YAML_PATH_ENV_VAR!r} "
        "env var unset"
    )
    os.environ.pop(_labels.LABELS_YAML_PATH_ENV_VAR, None)

    _labels.crud_labels()

    assert expected in caplog.text


@pytest.mark.usefixtures("add_labels_for_curation_tests")
def test_crud_labels_labels_yaml_not_found(caplog):
    path = "/no/labels.yaml"
    expected = f"No labels YAML found at given path {path}"

    with env.env_vars_set({_labels.LABELS_YAML_PATH_ENV_VAR: path}):
        _labels.crud_labels()

    assert expected in caplog.text


@pytest.mark.usefixtures("add_labels_for_curation_tests")
def test_crud_labels_from_yaml(database_client, labels_yaml_test_path):
    _labels._crud_labels_from_yaml(path=labels_yaml_test_path, client=database_client)

    testing.labels.assert_labels_crud_correct(database_client)


@pytest.mark.usefixtures("add_example_project")
def test_delete_labels_from_database_when_referenced(database_client, database_labels):
    """Test that labels can be deleted even if referenced."""
    to_delete = {label.identifier for label in database_labels}
    with database_client.session():
        _labels._delete_labels_from_database(to_delete, client=database_client)
