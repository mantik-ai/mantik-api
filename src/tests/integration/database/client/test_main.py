import pytest
import sqlalchemy.sql


@pytest.fixture
def another_engine(postgres_uri) -> sqlalchemy.engine.Engine:
    return sqlalchemy.create_engine(postgres_uri)


class TestClient:
    def test_session_exit_closes_connection(self, another_engine, database_client):
        connections_before = _get_number_of_connections(another_engine)

        with database_client.session():
            pass

        connections_after = _get_number_of_connections(another_engine)

        assert (
            connections_before > connections_after
        ), "Connection not closed after closing session"


def _get_number_of_connections(engine: sqlalchemy.engine.Engine) -> int:
    with engine.connect() as conn:
        connections = conn.execute(
            sqlalchemy.sql.text(
                # Get number of connections
                "SELECT count(*) FROM pg_stat_activity;"
            )
        ).scalar_one()
        return connections
