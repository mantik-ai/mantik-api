import uuid

import fastapi.testclient as testclient
import pytest
import starlette.status as status

import mantik_api.database.connection as _connection
import mantik_api.testing as testing

HEADERS = testing.token_verifier.VALID_AUTHORIZATION_HEADER


@pytest.mark.usefixtures("clear_and_init_database")
def test_add_connection_with_reference_to_not_existing_user(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    user_id_str,
    mock_hvac,
):
    add_connection = {
        "connectionProvider": _connection.ConnectionProvider.HPC_JSC.value,
        "password": "password",
        "loginName": "loginName",
        "connectionName": "connectionName",
        "authMethod": "Token",
        "token": "token",
    }

    response = client.post(
        f"/users/{user_id_str}/settings/connections",
        headers=HEADERS,
        json=add_connection,
    )

    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, response.text
    assert response.json()["detail"] == "Linked resource not found"


@pytest.mark.usefixtures("add_example_user_group")
def test_update_user_group_with_reference_to_non_existing_user(
    client: testclient.TestClient,
    mock_get_user_info_and_be_project_owner,
    user_id_str,
    user_group_id_str,
):
    random_id = str(uuid.uuid4())
    response = client.put(
        f"/groups/{user_group_id_str}",
        headers=testing.token_verifier.VALID_AUTHORIZATION_HEADER,
        json={"name": "new-group", "adminId": random_id, "memberIds": []},
    )

    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, response.text
    assert (
        response.json()["detail"]
        == f"Unable to change admin: user with ID {random_id} not found"
    )
