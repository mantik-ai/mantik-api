import pytest

import mantik_api.database as database


class TestUser:
    @pytest.mark.usefixtures("add_example_user")
    def test_get(self, database_client, user_id_str, database_user):
        with database_client.session():
            user = database_client.get_one(
                orm_model_type=database.user.User,
                constraints={database.user.User.id: user_id_str},
            )

            assert str(user.id) == user_id_str
