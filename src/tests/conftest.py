"""Contains reusable fixtures.

The model UUIDs have the following convention:

"<first>641ab6b-750c-4cb6-ac15-4699601300c<second>"

where `<first>` and `<second>` are integer numbers.

- `<first>` enumerates the database models (e.g. 1 is for labels, 2 for code
  repositories etc.)
- `<second>` enumerates each instance of the
  respective model (e.g. 1 is the first label, 2 the second, and so forth).


"""
import contextlib
import datetime
import json
import os
import pathlib
import re
import typing as t
import uuid

import boto3
import hvac
import moto
import pytest
import requests_mock

import mantik_api.aws.cognito as cognito
import mantik_api.aws.scheduler as scheduler
import mantik_api.database as database
import mantik_api.invoke_compute_backend.submit_run as submit_run
import mantik_api.models as models
import mantik_api.routes.mlflow.tracking_server_router as tracking_server_router
import mantik_api.testing as testing
import mantik_api.tokens.jwt as jwt
import mantik_api.utils as utils
import mantik_api.utils.git_utils as git_utils
from mantik_api.models.run_info import ConsumedTime

_FILE_PATH = pathlib.Path(__file__).parent

UNICORE_DEFAULT_USERNAME = "demouser"
UNICORE_DEFAULT_PASSWORD = "test123"
SSH_PRIVATE_KEY_EXAMPLE = "b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAA"

RESOURCES_LABELS_DIR = pathlib.Path(__file__).parent / "resources/labels"
LABELS_DIR = pathlib.Path(__file__).parent / "../../labels"
TEST_LABELS_YAML = RESOURCES_LABELS_DIR / "labels.yaml"
LABELS_YAML = LABELS_DIR / "labels.yaml"

# Changing UNICORE port to 8888 to not clash with firecREST microservices.
_DEFAULT_UNICORE_API_URL = "https://localhost:8888/DEMO-SITE/rest/core"
_TEST_UNICORE_API_URL = os.getenv("UNICORE_API_URL", _DEFAULT_UNICORE_API_URL)


def pytest_addoption(parser):
    parser.addoption(
        "--use-local-firecrest",
        action="store_true",
        default=False,
        help=(
            "Run the tests that require local firecREST microservices for "
            "testing (see CONTRIBUTING.md)."
        ),
    )


@pytest.fixture(scope="session")
def created_at() -> datetime.datetime:
    return testing.time.DEFAULT_CREATED_AT


@pytest.fixture(scope="session")
def unicore_api_url() -> str:
    return _TEST_UNICORE_API_URL


@pytest.fixture()
def labels_yaml_test_path() -> pathlib.Path:
    return TEST_LABELS_YAML


@pytest.fixture()
def labels_yaml_real_path() -> pathlib.Path:
    return LABELS_YAML


@pytest.fixture
def expect_raise_if_exception() -> (
    t.Callable[[t.Any], contextlib.AbstractContextManager]
):
    def expect_can_be_error(
        expected: t.Any,
    ) -> contextlib.AbstractContextManager:
        return (
            pytest.raises(type(expected))
            if isinstance(expected, Exception)
            else contextlib.nullcontext()
        )

    return expect_can_be_error


@pytest.fixture
def fixture_str_or_value(request) -> t.Callable[[t.Any], t.Any]:
    def fixture_or_value(value: t.Any) -> t.Any:
        try:
            return request.getfixturevalue(value)
        except pytest.FixtureLookupError:
            return value

    return fixture_or_value


@pytest.fixture()
def create_mlflow_experiment(fake_token) -> t.Callable[[str], int]:
    """Create experiment with random UUID as name to allow parallel tests."""

    def create(name: str | None = None, default_id: int | None = 0) -> int:
        if _tracking_server_available():
            name = name or str(uuid.uuid4())
            experiment_id = utils.mlflow.experiments.create(
                experiment_name=name, token=fake_token
            )
            return experiment_id

        return default_id

    return create


@pytest.fixture()
def create_mlflow_run(
    create_mlflow_experiment, fake_token
) -> t.Callable[[str], uuid.UUID]:
    """Create experiment with random UUID as name to allow parallel tests."""

    def create(name: str | None = None, experiment_id: int | None = None) -> uuid.UUID:
        if _tracking_server_available():
            experiment_id = experiment_id or create_mlflow_experiment()
            return testing.mlflow.create_run(
                experiment_id=experiment_id,
                name=name or f"run-{uuid.uuid4()}",
                token=fake_token,
            )
        else:
            return uuid.uuid4()

    return create


def _tracking_server_available() -> bool:
    # Assume that port 5001 is given for MLflow Tracking URI
    # when testing.
    return (
        tracking_server_router.TRACKING_SERVER_URL_INTERNAL_ENV_VAR in os.environ
        and (
            ":5101"
            in os.environ[tracking_server_router.TRACKING_SERVER_URL_INTERNAL_ENV_VAR]
            or ":5001"
            in os.environ[tracking_server_router.TRACKING_SERVER_URL_INTERNAL_ENV_VAR]
        )
    )


@pytest.fixture
def label_id_1_str() -> str:
    return "1641ab6b-750c-4cb6-ac15-4699601300c1"


@pytest.fixture
def api_label_1(label_id_1_str, created_at) -> models.label.Label:
    return models.label.Label(
        label_id=label_id_1_str,
        scopes=["project"],
        category="Tasks",
        sub_category="Tabular",
        name="test-label-1",
        created_at=created_at,
    )


@pytest.fixture
def label_id_2_str() -> str:
    return "1641ab6b-750c-4cb6-ac15-4699601300c2"


@pytest.fixture
def api_label_2(label_id_2_str, created_at) -> models.label.Label:
    return models.label.Label(
        label_id=label_id_2_str,
        scopes=["project"],
        category="Tasks",
        sub_category="Tabular",
        name="test-label-2",
        created_at=created_at,
    )


@pytest.fixture
def label_id_3_str() -> str:
    return "1641ab6b-750c-4cb6-ac15-4699601300c3"


@pytest.fixture
def api_label_3(label_id_3_str, created_at) -> models.label.Label:
    return models.label.Label(
        label_id=label_id_3_str,
        scopes=["code"],
        category="Tasks",
        sub_category="Tabular",
        name="test-label-3",
        created_at=created_at,
    )


@pytest.fixture
def label_id_4_str() -> str:
    return "1641ab6b-750c-4cb6-ac15-4699601300c4"


@pytest.fixture
def api_label_4(label_id_4_str, created_at) -> models.label.Label:
    return models.label.Label(
        label_id=label_id_4_str,
        scopes=["data", "experiment"],
        category="Tasks",
        sub_category="Tabular",
        name="test-label-4",
        created_at=created_at,
    )


@pytest.fixture
def label_id_5_str() -> str:
    return "1641ab6b-750c-4cb6-ac15-4699601300c5"


@pytest.fixture
def api_label_5(label_id_5_str, created_at) -> models.label.Label:
    return models.label.Label(
        label_id=label_id_5_str,
        scopes=["data", "experiment"],
        category="Tasks",
        sub_category="Tabular",
        name="test-label-5",
        created_at=created_at,
    )


@pytest.fixture
def api_labels(api_label_1, api_label_2) -> list[models.label.Label]:
    return [api_label_1, api_label_2]


@pytest.fixture
def api_labels_code_scope(api_label_3) -> list[models.label.Label]:
    return [api_label_3]


@pytest.fixture
def api_labels_data_experiment_scope(
    api_label_4, api_label_5
) -> list[models.label.Label]:
    return [api_label_4, api_label_5]


@pytest.fixture
def database_label_1(api_label_1) -> database.label.Label:
    return database.label.Label(
        id=api_label_1.label_id,
        identifier="0000",
        scopes=api_label_1.scopes,
        category=api_label_1.category,
        sub_category=api_label_1.sub_category,
        name=api_label_1.name,
        created_at=api_label_1.created_at,
    )


@pytest.fixture
def database_label_2(api_label_2) -> database.label.Label:
    return database.label.Label(
        id=api_label_2.label_id,
        identifier="0001",
        scopes=api_label_2.scopes,
        category=api_label_2.category,
        sub_category=api_label_2.sub_category,
        name=api_label_2.name,
        created_at=api_label_2.created_at,
    )


@pytest.fixture
def database_label_3(api_label_3) -> database.label.Label:
    return database.label.Label(
        id=api_label_3.label_id,
        identifier="0002",
        scopes=api_label_3.scopes,
        category=api_label_3.category,
        sub_category=api_label_3.sub_category,
        name=api_label_3.name,
        created_at=api_label_3.created_at,
    )


@pytest.fixture
def database_label_4(api_label_4) -> database.label.Label:
    return database.label.Label(
        id=api_label_4.label_id,
        identifier="0003",
        scopes=api_label_4.scopes,
        category=api_label_4.category,
        sub_category=api_label_4.sub_category,
        name=api_label_4.name,
        created_at=api_label_4.created_at,
    )


@pytest.fixture
def database_label_5(api_label_5) -> database.label.Label:
    return database.label.Label(
        id=api_label_5.label_id,
        identifier="0004",
        scopes=api_label_5.scopes,
        category=api_label_5.category,
        sub_category=api_label_5.sub_category,
        name=api_label_5.name,
        created_at=api_label_5.created_at,
    )


@pytest.fixture
def database_labels(database_label_1, database_label_2) -> list[database.label.Label]:
    return [database_label_1, database_label_2]


@pytest.fixture
def database_labels_code_scope(database_label_3) -> list[database.label.Label]:
    return [database_label_3]


@pytest.fixture
def database_labels_data_experiment_scope(
    database_label_4, database_label_5
) -> list[database.label.Label]:
    return [database_label_4, database_label_5]


@pytest.fixture
def code_repository_id_str() -> str:
    return "2641ab6b-750c-4cb6-ac15-4699601300c1"


@pytest.fixture
def second_code_repository_id_str() -> str:
    return "2641ab6b-750c-4cb6-ac15-4699601300c2"


@pytest.fixture
def third_code_repository_id_str() -> str:
    return "2641ab6b-750c-4cb6-ac15-4699601300c3"


@pytest.fixture
def code_repository_label_3_id_str() -> str:
    return "100c401e-9999-48ca-ad30-436030d527b0"


@pytest.fixture()
def fake_gitlab_repo_url() -> str:
    return "https://gitlab.com/test-account/test-repo"


@pytest.fixture()
def fake_github_repo_url() -> str:
    return "https://github.com/test-owner/test-repo"


@pytest.fixture()
def fake_bitbucket_org_repo_url() -> str:
    return "https://bitbucket.org/test-owner/test-repo"


@pytest.fixture()
def fake_self_hosted_repo_url() -> str:
    return "https://test-host/test-owner/test-repo"
    # return "https://git.ecmwf.int/projects/MLFET/repos/maelstrom-radiation"


@pytest.fixture()
def fake_branch() -> str:
    return "test-branch"


@pytest.fixture()
def fake_token() -> jwt.JWT:
    return testing.token_verifier.create_token(testing.token_verifier.VALID_JWT)


@pytest.fixture
def api_code_repository(
    api_labels_code_scope, code_repository_id_str, fake_gitlab_repo_url, created_at
):
    return models.code_repository.CodeRepository(
        code_repository_id=code_repository_id_str,
        uri=fake_gitlab_repo_url,
        labels=api_labels_code_scope,
        created_at=created_at,
        platform=database.git_repository.Platform.GitLab,
    )


@pytest.fixture
def second_api_code_repository(
    api_labels_code_scope,
    second_code_repository_id_str,
    fake_gitlab_repo_url,
    created_at,
):
    return models.code_repository.CodeRepository(
        code_repository_id=second_code_repository_id_str,
        uri=fake_gitlab_repo_url,
        labels=api_labels_code_scope,
        created_at=created_at,
        platform=database.git_repository.Platform.GitLab,
    )


@pytest.fixture
def third_api_code_repository(
    api_labels_code_scope,
    third_code_repository_id_str,
    fake_bitbucket_org_repo_url,
    created_at,
):
    return models.code_repository.CodeRepository(
        code_repository_id=third_code_repository_id_str,
        uri=fake_bitbucket_org_repo_url,
        labels=api_labels_code_scope,
        created_at=created_at,
        platform=database.git_repository.Platform.Bitbucket,
    )


@pytest.fixture
def database_code_repository(
    database_labels_code_scope, api_code_repository, project_id_str
) -> database.code_repository.CodeRepository:
    return database.code_repository.CodeRepository(
        id=api_code_repository.code_repository_id,
        uri=api_code_repository.uri,
        project_id=uuid.UUID(project_id_str),
        labels=database_labels_code_scope,
        created_at=api_code_repository.created_at,
        platform=database.git_repository.Platform.GitLab,
    )


@pytest.fixture
def second_database_code_repository(
    database_labels_code_scope, second_api_code_repository, project_id_str
) -> database.code_repository.CodeRepository:
    return database.code_repository.CodeRepository(
        id=second_api_code_repository.code_repository_id,
        uri=second_api_code_repository.uri,
        project_id=uuid.UUID(project_id_str),
        labels=database_labels_code_scope,
        created_at=second_api_code_repository.created_at,
        platform=database.git_repository.Platform.GitLab,
    )


@pytest.fixture
def third_database_code_repository(
    database_labels_code_scope, third_api_code_repository, project_id_str
) -> database.code_repository.CodeRepository:
    return database.code_repository.CodeRepository(
        id=third_api_code_repository.code_repository_id,
        uri=third_api_code_repository.uri,
        project_id=uuid.UUID(project_id_str),
        labels=database_labels_code_scope,
        created_at=third_api_code_repository.created_at,
        platform=database.git_repository.Platform.Bitbucket,
    )


@pytest.fixture
def data_repository_id_str() -> str:
    return "3641ab6b-750c-4cb6-ac15-4699601300c1"


@pytest.fixture
def second_data_repository_id_str() -> str:
    return "3641ab6b-750c-4cb6-ac15-4699601300c2"


@pytest.fixture
def data_repository_id_label_4_str() -> str:
    return "3641ab6b-750c-4cb6-ac15-4699601300c3"


@pytest.fixture
def api_data_repository(
    api_labels_data_experiment_scope, data_repository_id_str, created_at
) -> models.data_repository.DataRepository:
    return models.data_repository.DataRepository(
        data_repository_id=data_repository_id_str,
        uri="https://gitlab.com/test-uri",
        platform=database.git_repository.Platform.GitLab,
        labels=api_labels_data_experiment_scope,
        created_at=created_at,
    )


@pytest.fixture
def second_api_data_repository(
    api_labels_data_experiment_scope, second_data_repository_id_str, created_at
) -> models.data_repository.DataRepository:
    return models.data_repository.DataRepository(
        data_repository_id=second_data_repository_id_str,
        uri="test-uri",
        labels=api_labels_data_experiment_scope,
        platform=database.git_repository.Platform.GitLab,
        created_at=created_at,
    )


@pytest.fixture
def database_data_repository(
    database_labels_data_experiment_scope, api_data_repository
) -> database.data_repository.DataRepository:
    return database.data_repository.DataRepository(
        id=api_data_repository.data_repository_id,
        uri=api_data_repository.uri,
        is_dvc_enabled=False,
        platform=database.git_repository.Platform.GitLab.value,
        labels=database_labels_data_experiment_scope,
        created_at=api_data_repository.created_at,
    )


@pytest.fixture
def second_database_data_repository(
    database_labels_data_experiment_scope, second_api_data_repository
) -> database.data_repository.DataRepository:
    return database.data_repository.DataRepository(
        id=second_api_data_repository.data_repository_id,
        uri=second_api_data_repository.uri,
        is_dvc_enabled=False,
        platform=database.git_repository.Platform.GitLab,
        labels=database_labels_data_experiment_scope,
        created_at=second_api_data_repository.created_at,
    )


@pytest.fixture
def experiment_repository_id_str() -> str:
    return "4641ab6b-750c-4cb6-ac15-4699601300c1"


@pytest.fixture
def second_experiment_repository_id_str() -> str:
    return "4641ab6b-750c-4cb6-ac15-4699601300c2"


@pytest.fixture
def experiment_repository_label_4_id_str() -> str:
    return "4641ab6b-750c-4cb6-ac15-4699601300c3"


@pytest.fixture
def api_experiment_repository(
    create_mlflow_experiment,
    api_labels_data_experiment_scope,
    experiment_repository_id_str,
    created_at,
):
    # Name must be unique to allow integration testing with MLflow:
    # It only soft-deletes experiments by marking them as deleted.
    name = f"test-name-{uuid.uuid4()}"
    mlflow_experiment_id = create_mlflow_experiment(name=name)
    return models.experiment_repository.ExperimentRepository(
        experiment_repository_id=experiment_repository_id_str,
        mlflow_experiment_id=mlflow_experiment_id,
        name=name,
        labels=api_labels_data_experiment_scope,
        created_at=created_at,
    )


@pytest.fixture
def second_api_experiment_repository(
    create_mlflow_experiment,
    api_labels_data_experiment_scope,
    second_experiment_repository_id_str,
    created_at,
):
    # Name must be unique to allow integration testing with MLflow:
    # It only soft-deletes experiments by marking them as deleted.
    name = f"test-name-2-{uuid.uuid4()}"
    mlflow_experiment_id = create_mlflow_experiment(name=name, default_id=2)
    return models.experiment_repository.ExperimentRepository(
        experiment_repository_id=second_experiment_repository_id_str,
        mlflow_experiment_id=mlflow_experiment_id,
        name=name,
        labels=api_labels_data_experiment_scope,
        created_at=created_at,
    )


@pytest.fixture
def database_experiment_repository(
    database_labels_data_experiment_scope,
    api_experiment_repository,
    project_id_str,
) -> database.experiment_repository.ExperimentRepository:
    return database.experiment_repository.ExperimentRepository(
        id=api_experiment_repository.experiment_repository_id,
        mlflow_experiment_id=api_experiment_repository.mlflow_experiment_id,
        name=api_experiment_repository.name,
        labels=database_labels_data_experiment_scope,
        project_id=uuid.UUID(project_id_str),
        created_at=api_experiment_repository.created_at,
    )


@pytest.fixture
def second_database_experiment_repository(
    database_labels_data_experiment_scope,
    second_api_experiment_repository,
    project_id_str,
) -> database.experiment_repository.ExperimentRepository:
    return database.experiment_repository.ExperimentRepository(
        id=second_api_experiment_repository.experiment_repository_id,
        mlflow_experiment_id=second_api_experiment_repository.mlflow_experiment_id,
        name=second_api_experiment_repository.name,
        labels=database_labels_data_experiment_scope,
        project_id=uuid.UUID(project_id_str),
        created_at=second_api_experiment_repository.created_at,
    )


@pytest.fixture
def model_repository_id_str() -> str:
    return "5641ab6b-750c-4cb6-ac15-4699601300c1"


@pytest.fixture
def second_model_repository_id_str() -> str:
    return "5641ab6b-750c-4cb6-ac15-4699601300c2"


@pytest.fixture
def model_repository_label_4_id_str() -> str:
    return "5641ab6b-750c-4cb6-ac15-4699601300c3"


@pytest.fixture
def api_model_repository(
    api_labels, model_repository_id_str, api_code_repository, created_at
) -> models.model_repository.ModelRepository:
    return models.model_repository.ModelRepository(
        model_repository_id=model_repository_id_str,
        uri="test-uri",
        labels=api_labels,
        code_repository=api_code_repository,
        branch="main",
        created_at=created_at,
    )


@pytest.fixture
def second_api_model_repository(
    api_labels, second_model_repository_id_str, second_api_code_repository, created_at
) -> models.model_repository.ModelRepository:
    return models.model_repository.ModelRepository(
        model_repository_id=second_model_repository_id_str,
        uri="test-uri",
        labels=api_labels,
        code_repository=second_api_code_repository,
        branch="main",
        created_at=created_at,
        platform="GitHub",
    )


@pytest.fixture
def database_model_repository(
    database_labels, api_model_repository, database_code_repository
) -> database.model_repository.ModelRepository:
    return database.model_repository.ModelRepository(
        id=api_model_repository.model_repository_id,
        uri=api_model_repository.uri,
        labels=database_labels,
        code_repository_id=database_code_repository.id,
        code_repository=database_code_repository,
        branch=api_model_repository.branch,
        created_at=api_model_repository.created_at,
    )


@pytest.fixture
def second_database_model_repository(
    database_labels, second_api_model_repository, second_database_code_repository
) -> database.model_repository.ModelRepository:
    return database.model_repository.ModelRepository(
        id=second_api_model_repository.model_repository_id,
        uri=second_api_model_repository.uri,
        labels=database_labels,
        code_repository_id=second_database_code_repository.id,
        code_repository=second_database_code_repository,
        branch=second_api_model_repository.branch,
        created_at=second_api_model_repository.created_at,
    )


@pytest.fixture(scope="session")
def user_id_str() -> str:
    return "6641ab6b-750c-4cb6-ac15-4699601300c1"


@pytest.fixture
def second_user_id_str() -> str:
    return "6641ab6b-750c-4cb6-ac15-4699601300c2"


@pytest.fixture
def third_user_id_str() -> str:
    return "6641ab6b-750c-4cb6-ac15-4699601300c3"


@pytest.fixture(scope="session")
def api_user(user_id_str, created_at):
    return models.user.User(
        user_id=user_id_str,
        name=testing.aws.cognito.USERNAME,
        created_at=created_at,
    )


@pytest.fixture
def second_api_user(second_user_id_str: str, created_at):
    return models.user.User(
        user_id=second_user_id_str,
        name="test-name-2",
        created_at=created_at,
    )


@pytest.fixture
def third_api_user(third_user_id_str: str, created_at):
    return models.user.User(
        user_id=third_user_id_str,
        name="test-name-3",
        created_at=created_at,
    )


@pytest.fixture
def database_user(api_user) -> database.user.User:
    return database.user.User(
        id=api_user.user_id,
        cognito_name=testing.aws.cognito.COGNITO_USERNAME,
        preferred_name=api_user.name,
        email=testing.aws.cognito.EMAIL,
        address="test-address",
        payment_method="test-payment_method",
        payment_details="test-payment_details",
        created_at=api_user.created_at,
        full_name="test-full-name",
        info="test-info",
        company="test-company",
        job_title="test-job-title",
        website_url="test-website-url",
    )


@pytest.fixture
def second_database_user(second_api_user) -> database.user.User:
    return database.user.User(
        id=second_api_user.user_id,
        cognito_name="test-cognito-name-2",
        preferred_name=second_api_user.name,
        email="test-email-2",
        address="test-address",
        payment_method="test-payment_method",
        payment_details="test-payment_details",
        created_at=second_api_user.created_at,
        full_name="test-full-name",
        info="test-info",
        company="test-company",
        job_title="test-job-title",
        website_url="test-website-url",
    )


@pytest.fixture
def third_database_user(third_api_user) -> database.user.User:
    return database.user.User(
        id=third_api_user.user_id,
        cognito_name="test-cognito-name-3",
        preferred_name=third_api_user.name,
        address="test-address",
        payment_method="test-payment_method",
        payment_details="test-payment_details",
        created_at=third_api_user.created_at,
        email="test-email-3",
        full_name="test-full-name",
        info="test-info",
        company="test-company",
        job_title="test-job-title",
        website_url="test-website-url",
    )


@pytest.fixture
def user_group_id_str() -> str:
    return "7641ab6b-750c-4cb6-ac15-4699601300c1"


@pytest.fixture
def second_user_group_id_str() -> str:
    return "7641ab6b-750c-4cb6-ac15-4699601300c2"


@pytest.fixture
def api_user_group(user_group_id_str, api_user, second_api_user, created_at):
    return models.user_group.UserGroup(
        user_group_id=user_group_id_str,
        name="test-name",
        admin=api_user,
        members=[api_user, second_api_user],
        created_at=created_at,
    )


@pytest.fixture
def second_api_user_group(second_user_group_id_str, second_api_user, created_at):
    return models.user_group.UserGroup(
        user_group_id=second_user_group_id_str,
        name="test-name",
        admin=second_api_user,
        members=[second_api_user],
        created_at=created_at,
    )


@pytest.fixture
def database_user_group(
    api_user_group,
    database_user: database.user.User,
    second_database_user: database.user.User,
) -> database.user_group.UserGroup:
    return database.user_group.UserGroup(
        id=api_user_group.user_group_id,
        name=api_user_group.name,
        admin=database_user,
        members=[database_user, second_database_user],
        created_at=api_user_group.created_at,
    )


@pytest.fixture
def second_database_user_group(
    second_api_user_group,
    second_database_user: database.user.User,
) -> database.user_group.UserGroup:
    return database.user_group.UserGroup(
        id=second_api_user_group.user_group_id,
        name=second_api_user_group.name,
        admin=second_database_user,
        members=[second_database_user],
        created_at=second_api_user_group.created_at,
    )


@pytest.fixture
def payment_info_id_str(user_id_str) -> str:
    return user_id_str


@pytest.fixture
def api_payment_info(payment_info_id_str):
    return models.payment_info.PaymentInfo(
        payment_info_id=payment_info_id_str,
        address="test-address",
        payment_method="test-payment_method",
        payment_details="test-payment_details",
    )


@pytest.fixture
def api_user_settings(api_user, api_payment_info, api_connection):
    return models.user_settings.UserSettings(
        user=api_user,
        payment_info=api_payment_info,
    )


@pytest.fixture(scope="session")
def connection_id_str() -> str:
    return "9641ab6b-750c-4cb6-ac15-4699601300c1"


@pytest.fixture(scope="session")
def second_connection_id_str() -> str:
    return "9641ab6b-750c-4cb6-ac15-4699601300c2"


@pytest.fixture(scope="session")
def connection_id_firecrest_str() -> str:
    return "9641ab6b-750c-4cb6-ac15-4699601300c5"


@pytest.fixture(scope="session")
def connection_id_slurm_private_key_str() -> str:
    return "9641ab6b-750c-4cb6-ac15-4699601300c6"


@pytest.fixture(scope="session")
def connection_second_user_id_str() -> str:
    return "9641ab6b-750c-4cb6-ac15-4699601300c3"


@pytest.fixture(scope="session")
def api_connection(connection_id_str, api_user, created_at):
    return models.connection.Connection(
        connection_id=uuid.UUID(connection_id_str),
        user=api_user,
        connection_name="test-connection-name",
        connection_provider=database.connection.ConnectionProvider.HPC_JSC.value,
        auth_method=database.connection.AuthMethod.USERNAME_PASSWORD,
        login_name=UNICORE_DEFAULT_USERNAME,
        password=UNICORE_DEFAULT_PASSWORD,
        created_at=created_at,
    )


@pytest.fixture
def second_api_connection(second_connection_id_str, api_user, created_at):
    return models.connection.Connection(
        connection_id=uuid.UUID(second_connection_id_str),
        user=api_user,
        connection_name="test-connection-name",
        connection_provider=database.connection.ConnectionProvider.HPC_JSC.value,
        auth_method=database.connection.AuthMethod.USERNAME_PASSWORD,
        login_name=UNICORE_DEFAULT_USERNAME,
        password=UNICORE_DEFAULT_PASSWORD,
        created_at=created_at,
    )


@pytest.fixture
def api_connection_firecrest(connection_id_firecrest_str, api_user, created_at):
    return models.connection.Connection(
        connection_id=uuid.UUID(connection_id_firecrest_str),
        user=api_user,
        connection_name="test-connection-name",
        connection_provider=database.connection.ConnectionProvider.HPC_JSC.value,
        auth_method=database.connection.AuthMethod.USERNAME_PASSWORD,
        login_name=testing.firecrest.LOCAL_CLIENT_ID,
        password=testing.firecrest.LOCAL_CLIENT_SECRET,
        created_at=created_at,
    )


@pytest.fixture
def api_connection_slurm_private_key(
    connection_id_slurm_private_key_str, api_user, created_at
):
    return models.connection.Connection(
        connection_id=uuid.UUID(connection_id_slurm_private_key_str),
        user=api_user,
        connection_name="test-connection-name",
        connection_provider=database.connection.ConnectionProvider.REMOTE_COMPUTE_SYSTEM.value,  # noqa E501
        auth_method=database.connection.AuthMethod.SSH_KEY,
        private_key=SSH_PRIVATE_KEY_EXAMPLE,
        created_at=created_at,
    )


@pytest.fixture
def api_connection_second_user(
    connection_second_user_id_str, second_api_user, created_at
):
    return models.connection.Connection(
        connection_id=uuid.UUID(connection_second_user_id_str),
        user=second_api_user,
        connection_name="test-connection-name",
        connection_provider=database.connection.ConnectionProvider.HPC_JSC.value,
        auth_method=database.connection.AuthMethod.USERNAME_PASSWORD,
        login_name=UNICORE_DEFAULT_USERNAME,
        password=UNICORE_DEFAULT_PASSWORD,
        created_at=created_at,
    )


@pytest.fixture
def database_connection(
    api_connection, database_user
) -> database.connection.Connection:
    return database.connection.Connection(
        id=api_connection.connection_id,
        user_id=database_user.id,
        user=database_user,
        connection_name=api_connection.connection_name,
        connection_provider=api_connection.connection_provider,
        auth_method=api_connection.auth_method,
        created_at=api_connection.created_at,
    )


@pytest.fixture
def second_database_connection(
    second_api_connection, database_user
) -> database.connection.Connection:
    return database.connection.Connection(
        id=second_api_connection.connection_id,
        user_id=database_user.id,
        user=database_user,
        connection_name=second_api_connection.connection_name,
        connection_provider=second_api_connection.connection_provider,
        auth_method=second_api_connection.auth_method,
        created_at=second_api_connection.created_at,
    )


@pytest.fixture
def database_connection_firecrest(
    api_connection_firecrest, database_user
) -> database.connection.Connection:
    return database.connection.Connection(
        id=api_connection_firecrest.connection_id,
        user_id=database_user.id,
        user=database_user,
        connection_name=api_connection_firecrest.connection_name,
        connection_provider=api_connection_firecrest.connection_provider,
        auth_method=api_connection_firecrest.auth_method,
        created_at=api_connection_firecrest.created_at,
    )


@pytest.fixture
def database_connection_slurm_private_key(
    api_connection_slurm_private_key, database_user
) -> database.connection.Connection:
    return database.connection.Connection(
        id=api_connection_slurm_private_key.connection_id,
        user_id=database_user.id,
        user=database_user,
        connection_name=api_connection_slurm_private_key.connection_name,
        connection_provider=api_connection_slurm_private_key.connection_provider,
        auth_method=api_connection_slurm_private_key.auth_method,
        created_at=api_connection_slurm_private_key.created_at,
    )


@pytest.fixture
def database_connection_second_user(
    api_connection_second_user, second_database_user
) -> database.connection.Connection:
    return database.connection.Connection(
        id=api_connection_second_user.connection_id,
        user_id=second_database_user.id,
        user=second_database_user,
        connection_name=api_connection_second_user.connection_name,
        connection_provider=api_connection_second_user.connection_provider,
        auth_method=api_connection_second_user.auth_method,
        created_at=api_connection_second_user.created_at,
    )


@pytest.fixture(scope="session")
def api_connection_dvc_of_first_user(api_user, created_at):
    return models.connection.Connection(
        connection_id=uuid.uuid4(),
        user=api_user,
        connection_name="Some s3 connection",
        connection_provider=database.connection.ConnectionProvider.S3.value,
        auth_method=database.connection.AuthMethod.USERNAME_PASSWORD,
        login_name=UNICORE_DEFAULT_USERNAME,
        password=UNICORE_DEFAULT_PASSWORD,
        created_at=created_at,
    )


@pytest.fixture
def database_connection_dvc_of_first_user(
    api_connection_dvc_of_first_user, database_user
) -> database.connection.Connection:
    return database.connection.Connection(
        id=api_connection_dvc_of_first_user.connection_id,
        user_id=database_user.id,
        user=database_user,
        connection_name=api_connection_dvc_of_first_user.connection_name,
        connection_provider=api_connection_dvc_of_first_user.connection_provider,
        auth_method=api_connection_dvc_of_first_user.auth_method,
        created_at=api_connection_dvc_of_first_user.created_at,
    )


@pytest.fixture(scope="session")
def sample_github_token() -> str:
    return "ghp_1234567890"


@pytest.fixture(scope="session")
def sample_gitlab_token() -> str:
    return "glpat_1234567890"


@pytest.fixture(scope="session")
def api_gitlab_connection_of_first_user(api_user, created_at, sample_github_token):
    return models.connection.Connection(
        connection_id=uuid.uuid4(),
        user=api_user,
        connection_name="Some private git repo name",
        connection_provider=database.connection.ConnectionProvider.GITLAB.value,
        auth_method=database.connection.AuthMethod.TOKEN,
        token=sample_github_token,
        created_at=created_at,
    )


@pytest.fixture
def database_git_connection_of_first_user(
    api_gitlab_connection_of_first_user, database_user
) -> database.connection.Connection:
    return database.connection.Connection(
        id=api_gitlab_connection_of_first_user.connection_id,
        user_id=database_user.id,
        user=database_user,
        connection_name=api_gitlab_connection_of_first_user.connection_name,
        connection_provider=api_gitlab_connection_of_first_user.connection_provider,
        auth_method=api_gitlab_connection_of_first_user.auth_method,
        created_at=api_gitlab_connection_of_first_user.created_at,
    )


@pytest.fixture
def organization_id_str() -> str:
    return "1989f1db-7489-4afc-93c5-5f369c204224"


@pytest.fixture
def second_organization_id_str() -> str:
    return "1989f1db-7489-4afc-93c5-5f369c204225"


@pytest.fixture
def api_organization(
    organization_id_str,
    api_user,
    api_user_group,
    second_api_user_group,
    third_api_user,
    created_at,
):
    return models.organization.Organization(
        organization_id=organization_id_str,
        name="foo",
        contact=api_user,
        groups=[api_user_group],
        members=[third_api_user.dict()],
        created_at=created_at,
    )


@pytest.fixture
def second_api_organization(
    second_organization_id_str, api_user, second_api_user_group, created_at
):
    return models.organization.Organization(
        organization_id=second_organization_id_str,
        name="foo",
        contact=api_user,
        groups=[second_api_user_group],
        members=[],
        created_at=created_at,
    )


@pytest.fixture
def api_add_organization():
    return models.organization.AddOrganization(
        name="foo",
    )


@pytest.fixture
def api_update_organization(user_id_str):
    return models.organization.UpdateOrganization(
        name="foo", contact_id=user_id_str, member_ids=[], group_ids=[]
    )


@pytest.fixture
def database_organization(
    api_organization, database_user, database_user_group, third_database_user
):
    return database.organization.Organization(
        id=api_organization.organization_id,
        name=api_organization.name,
        contact=database_user,
        groups=[database_user_group],
        members=[third_database_user],
        created_at=api_organization.created_at,
    )


@pytest.fixture
def database_organization_without_group(
    organization_id_str, database_user, third_database_user
):
    return database.organization.Organization(
        id=uuid.UUID(organization_id_str),
        name="foo",
        contact=database_user,
        groups=[],
        members=[third_database_user],
    )


@pytest.fixture
def second_database_organization(
    second_api_organization,
    database_user,
    database_user_group,
    second_database_user_group,
):
    return database.organization.Organization(
        id=second_api_organization.organization_id,
        name=second_api_organization.name,
        contact=database_user,
        groups=[second_database_user_group],
        created_at=second_api_organization.created_at,
    )


@pytest.fixture
def project_id_str() -> str:
    return "e931c97e-917b-499c-b7b1-fcb52ae96f91"


@pytest.fixture
def project_private_id_str() -> str:
    return "e931c97e-917b-499c-b7b1-fcb52ae96f92"


@pytest.fixture
def project_with_labels_3_to_5_id_str() -> str:
    return "e931c97e-917b-499c-b7b1-fcb52ae96f93"


@pytest.fixture
def project_with_labels_1_to_5_id_str() -> str:
    return "e931c97e-917b-499c-b7b1-fcb52ae96f94"


@pytest.fixture
def project_with_labels_1_to_5_on_project_id_str() -> str:
    return "e931c97e-917b-499c-b7b1-fcb52ae96f95"


@pytest.fixture
def api_project(
    project_id_str,
    api_user,
    api_code_repository,
    api_experiment_repository,
    api_model_repository,
    api_data_repository,
    api_labels,
    second_api_model_repository,
    second_api_data_repository,
    second_api_code_repository,
    second_api_experiment_repository,
    third_api_code_repository,
    created_at,
):
    return models.project.Project(
        project_id=project_id_str,
        name="test_project_name",
        executive_summary="test_executive_summary",
        detailed_description="test_detailed_description",
        owner=api_user,
        code_repositories=[
            api_code_repository,
            second_api_code_repository,
            third_api_code_repository,
        ],
        experiment_repositories=[
            api_experiment_repository,
            second_api_experiment_repository,
        ],
        model_repositories=[api_model_repository, second_api_model_repository],
        data_repositories=[api_data_repository, second_api_data_repository],
        labels=api_labels,
        public=True,
        created_at=created_at,
    )


@pytest.fixture
def environment_id_str() -> str:
    return "5a7ab596-6da6-47b3-96bf-eaa7a5ef45c3"


@pytest.fixture
def second_environment_id_str() -> str:
    return "651e1ff2-726c-4c7b-9e43-765d8da0379d"


@pytest.fixture
def api_add_environment(database_data_repository):
    return models.environment.AddEnvironment(
        name="sample-environment-1",
        type="Apptainer",
        variables={"ENV_VAR_1": "VALUE_1"},
        data_repository_id=database_data_repository.id,
    )


@pytest.fixture
def database_environment(
    environment_id_str: str,
    api_add_environment: models.environment.AddEnvironment,
    database_data_repository,
    created_at,
):
    return database.environment.Environment(
        id=uuid.UUID(environment_id_str),
        name=api_add_environment.name,
        type=api_add_environment.type,
        variables=api_add_environment.variables,
        data_repository_id=api_add_environment.data_repository_id,
        data_repository=database_data_repository,
        created_at=created_at,
    )


@pytest.fixture
def api_environment(
    api_add_environment: models.environment.AddEnvironment,
    api_data_repository: models.data_repository.DataRepository,
    environment_id_str: str,
    created_at,
):
    return models.environment.Environment(
        environment_id=uuid.UUID(environment_id_str),
        name=api_add_environment.name,
        type=api_add_environment.type,
        variables=api_add_environment.variables,
        data_repository=api_data_repository,
        created_at=created_at,
    )


@pytest.fixture
def api_add_project():
    return models.project.AddProject(
        name="test_project_name",
        executive_summary="test_executive_summary",
        detailed_description="test_detailed_description",
        public=True,
    )


@pytest.fixture
def database_project(
    api_project,
    database_user,
    database_code_repository,
    database_experiment_repository,
    database_model_repository,
    database_data_repository,
    second_database_data_repository,
    second_database_model_repository,
    second_database_code_repository,
    second_database_experiment_repository,
    third_database_code_repository,
    database_labels,
):
    return database.project.Project(
        id=api_project.project_id,
        name=api_project.name,
        executive_summary=api_project.executive_summary,
        detailed_description=api_project.detailed_description,
        owner=database_user,
        code_repositories=[
            database_code_repository,
            second_database_code_repository,
            third_database_code_repository,
        ],
        experiment_repositories=[
            database_experiment_repository,
            second_database_experiment_repository,
        ],
        model_repositories=[
            database_model_repository,
            second_database_model_repository,
        ],
        data_repositories=[database_data_repository, second_database_data_repository],
        labels=database_labels,
        public=api_project.public,
        created_at=api_project.created_at,
    )


@pytest.fixture
def database_project_private(
    project_private_id_str,
    database_user,
    database_code_repository,
    database_experiment_repository,
    database_model_repository,
    database_data_repository,
    database_labels,
    created_at,
):
    return database.project.Project(
        id=uuid.UUID(project_private_id_str),
        name="test_private_project_name",
        executive_summary="test_private_executive_summary",
        detailed_description="test_private_detailed_description",
        owner=database_user,
        code_repositories=[database_code_repository],
        experiment_repositories=[database_experiment_repository],
        model_repositories=[database_model_repository],
        data_repositories=[database_data_repository],
        labels=database_labels,
        public=False,
        # Let private project be created one day later to test ordering
        # by created_at
        created_at=created_at + datetime.timedelta(days=1),
    )


@pytest.fixture
def database_project_labels_3_to_5(
    project_with_labels_3_to_5_id_str,
    database_user,
    database_label_3,
    database_label_4,
    database_label_5,
    fake_gitlab_repo_url,
    created_at,
) -> database.project.Project:
    project_id = uuid.UUID(project_with_labels_3_to_5_id_str)
    code_repositories = [
        database.code_repository.CodeRepository(
            id=uuid.uuid4(),
            uri=fake_gitlab_repo_url,
            project_id=project_id,
            labels=[database_label_3],
            platform="GitLab",
        )
    ]
    experiment_repositories = [
        database.experiment_repository.ExperimentRepository(
            id=uuid.uuid4(),
            mlflow_experiment_id=3,
            name="test-name",
            labels=[database_label_4],
            project_id=project_id,
        )
    ]

    data_repositories = [
        database.data_repository.DataRepository(
            id=uuid.uuid4(),
            uri="test-uri",
            labels=[database_label_5],
            project_id=project_id,
        )
    ]
    return database.project.Project(
        id=project_id,
        name="test_project_name",
        executive_summary="test_executive_summary",
        detailed_description="test_detailed_description",
        owner=database_user,
        code_repositories=code_repositories,
        experiment_repositories=experiment_repositories,
        data_repositories=data_repositories,
        public=False,
        created_at=created_at,
    )


@pytest.fixture
def database_project_labels_1_to_5_on_project(
    database_user,
    project_with_labels_1_to_5_on_project_id_str,
    database_label_1,
    database_label_2,
    database_label_3,
    database_label_4,
    database_label_5,
    created_at,
) -> database.project.Project:
    return database.project.Project(
        id=uuid.UUID(project_with_labels_1_to_5_on_project_id_str),
        name="test_project_name",
        executive_summary="test_executive_summary",
        detailed_description="test_detailed_description",
        owner=database_user,
        labels=[
            database_label_1,
            database_label_2,
            database_label_3,
            database_label_4,
            database_label_5,
        ],
        public=True,
        created_at=created_at,
    )


@pytest.fixture
def database_project_labels_1_to_5(
    project_with_labels_1_to_5_id_str,
    database_user,
    database_label_1,
    database_label_2,
    database_label_3,
    database_label_4,
    database_label_5,
    fake_gitlab_repo_url,
    created_at,
) -> database.project.Project:
    project_id = uuid.UUID(project_with_labels_1_to_5_id_str)
    code_repositories = [
        database.code_repository.CodeRepository(
            id=uuid.uuid4(),
            uri=fake_gitlab_repo_url,
            project_id=project_id,
            labels=[database_label_3],
            platform="GitLab",
        )
    ]
    experiment_repositories = [
        database.experiment_repository.ExperimentRepository(
            id=uuid.uuid4(),
            mlflow_experiment_id=3,
            name="test-name",
            labels=[database_label_4],
            project_id=project_id,
        )
    ]

    data_repositories = [
        database.data_repository.DataRepository(
            id=uuid.uuid4(),
            uri="test-uri",
            labels=[database_label_5],
            project_id=project_id,
        )
    ]
    return database.project.Project(
        id=project_id,
        name="test_project_name",
        executive_summary="test_executive_summary",
        detailed_description="test_detailed_description",
        owner=database_user,
        labels=[database_label_1, database_label_2],
        code_repositories=code_repositories,
        experiment_repositories=experiment_repositories,
        data_repositories=data_repositories,
        public=True,
        created_at=created_at,
    )


@pytest.fixture
def saved_model_repository_id_str() -> str:
    return "108dea76-3c50-4cd8-85c4-4f098271b8fa"


@pytest.fixture
def trained_model_id_str(saved_model_repository_id_str) -> str:
    return saved_model_repository_id_str


@pytest.fixture
def api_saved_model_repository(
    saved_model_repository_id_str, api_model_repository, created_at
):
    return models.trained_model.TrainedModel(
        name="test-trained-model",
        model_id=saved_model_repository_id_str,
        model_repository=api_model_repository,
        location="JUWELS",
        uri="test-uri",
        created_at=created_at,
    )


@pytest.fixture
def api_trained_model(api_saved_model_repository):
    return api_saved_model_repository


@pytest.fixture
def database_saved_model_repository(
    api_saved_model_repository,
    database_model_repository,
):
    return database.trained_model.TrainedModel(
        name=api_saved_model_repository.name,
        id=api_saved_model_repository.model_id,
        location=api_saved_model_repository.location,
        project_id=database_model_repository.project_id,
        uri=api_saved_model_repository.uri,
        created_at=api_saved_model_repository.created_at,
    )


@pytest.fixture
def run_id_str() -> str:
    return "6b01d96f-d9a4-4357-aace-ecebcf08eadc"


@pytest.fixture
def run_name() -> str:
    return f"test-run-{uuid.uuid4()}"


@pytest.fixture
def compute_budget_account() -> str:
    return "test-compute-budget-account"


@pytest.fixture
def second_run_id_str() -> str:
    return "6b01d96f-d9a4-4357-aace-ecebcf08ead1"


@pytest.fixture
def firecrest_run_id_str() -> str:
    return "6b01d96f-d9a4-4357-aace-ecebcf08ead2"


@pytest.fixture
def api_run(
    run_id_str,
    run_name,
    api_user,
    api_model_repository,
    api_experiment_repository,
    api_data_repository,
    user_id_str,
    api_connection,
    api_saved_model_repository,
    compute_budget_account,
    mlflow_mlproject_file_path,
    backend_config,
    created_at,
):
    return models.run.Run(
        run_id=run_id_str,
        name=run_name,
        experiment_repository=api_experiment_repository,
        saved_model=api_saved_model_repository,
        model_repository=api_model_repository,
        data_repository=api_data_repository,
        user=api_user,
        connection=api_connection,
        status=database.run.RunStatus.FINISHED,
        start_time=1680013202,
        end_time=1680013251,
        artifact_uri="test-artifact-uri",
        lifecycle_stage="test-lifecycle-stage",
        mlflow_run_id=uuid.uuid4(),
        compute_budget_account=compute_budget_account,
        mlflow_mlproject_file_path=mlflow_mlproject_file_path,
        entry_point="main",
        backend_config=backend_config,
        created_at=created_at,
    )


@pytest.fixture
def api_run_firecrest(
    firecrest_run_id_str,
    run_name,
    api_user,
    api_model_repository,
    api_experiment_repository,
    api_data_repository,
    user_id_str,
    api_connection_firecrest,
    api_saved_model_repository,
    compute_budget_account,
    mlflow_mlproject_file_path,
    backend_config_firecrest,
    created_at,
):
    return models.run.Run(
        run_id=firecrest_run_id_str,
        name=run_name,
        experiment_repository=api_experiment_repository,
        saved_model=api_saved_model_repository,
        model_repository=api_model_repository,
        data_repository=api_data_repository,
        user=api_user,
        connection=api_connection_firecrest,
        status=database.run.RunStatus.FINISHED,
        start_time=1680013202,
        end_time=1680013251,
        artifact_uri="test-artifact-uri",
        lifecycle_stage="test-lifecycle-stage",
        mlflow_run_id=uuid.uuid4(),
        compute_budget_account=compute_budget_account,
        mlflow_mlproject_file_path=mlflow_mlproject_file_path,
        entry_point="main",
        backend_config=backend_config_firecrest,
        created_at=created_at,
    )


@pytest.fixture
def database_run(
    database_model_repository,
    database_experiment_repository,
    database_data_repository,
    database_user,
    database_connection,
    database_saved_model_repository,
    api_run,
):
    return database.run.Run(
        id=api_run.run_id,
        name=api_run.name,
        mlflow_run_id=api_run.mlflow_run_id,
        status=api_run.status,
        artifact_uri=api_run.artifact_uri,
        lifecycle_stage=api_run.lifecycle_stage,
        experiment_repository_id=database_experiment_repository.id,
        experiment_repository=database_experiment_repository,
        model_repository_id=database_model_repository.id,
        model_repository=database_model_repository,
        data_repository_id=database_data_repository.id,
        data_repository=database_data_repository,
        user_id=database_user.id,
        user=database_user,
        connection_id=database_connection.id,
        connection=database_connection,
        project_id=database_experiment_repository.project_id,
        start_time=api_run.start_time,
        end_time=api_run.end_time,
        mlflow_mlproject_file_path=api_run.mlflow_mlproject_file_path.as_posix(),
        entry_point=api_run.entry_point,
        mlflow_parameters=api_run.mlflow_parameters,
        backend_config=api_run.backend_config,
        compute_budget_account=api_run.compute_budget_account,
        environment_id=api_run.environment_id,
        created_at=api_run.created_at,
    )


@pytest.fixture
def database_run_wrong_backend_config(database_run):
    database_run.status = database.run.RunStatus.RUNNING
    database_run.backend_config = {
        "UnicoreApiUrl": "https://wrong-tracking-server",
        "Environment": {
            "Singularity": {"Path": "some/image/image1.sif", "Type": "remote"}
        },
        "Resources": {"Queue": "devel", "Nodes": 1},
    }
    return database_run


@pytest.fixture
def database_run_firecrest(
    database_model_repository,
    database_experiment_repository,
    database_data_repository,
    database_user,
    database_connection_firecrest,
    database_saved_model_repository,
    api_run_firecrest,
):
    return database.run.Run(
        id=api_run_firecrest.run_id,
        name=api_run_firecrest.name,
        mlflow_run_id=api_run_firecrest.mlflow_run_id,
        status=api_run_firecrest.status,
        artifact_uri=api_run_firecrest.artifact_uri,
        lifecycle_stage=api_run_firecrest.lifecycle_stage,
        experiment_repository_id=database_experiment_repository.id,
        experiment_repository=database_experiment_repository,
        model_repository_id=database_model_repository.id,
        model_repository=database_model_repository,
        data_repository_id=database_data_repository.id,
        data_repository=database_data_repository,
        user_id=database_user.id,
        user=database_user,
        connection_id=database_connection_firecrest.id,
        connection=database_connection_firecrest,
        project_id=database_experiment_repository.project_id,
        start_time=api_run_firecrest.start_time,
        end_time=api_run_firecrest.end_time,
        mlflow_mlproject_file_path=api_run_firecrest.mlflow_mlproject_file_path.as_posix(),  # noqa E501
        entry_point=api_run_firecrest.entry_point,
        mlflow_parameters=api_run_firecrest.mlflow_parameters,
        backend_config=api_run_firecrest.backend_config,
        compute_budget_account=api_run_firecrest.compute_budget_account,
        environment_id=api_run_firecrest.environment_id,
        created_at=api_run_firecrest.created_at,
    )


@pytest.fixture()
def database_run_with_mlflow_run(
    create_mlflow_run, mock_tracking_server_url, database_run
) -> database.run.Run:
    database_run.mlflow_run_id = create_mlflow_run(name=database_run.name)
    return database_run


@pytest.fixture()
def database_run_firecrest_with_mlflow_run(
    create_mlflow_run, database_run_firecrest
) -> database.run.Run:
    database_run_firecrest.mlflow_run_id = create_mlflow_run(
        name=database_run_firecrest.name
    )
    return database_run_firecrest


@pytest.fixture
def second_database_run(
    second_run_id_str,
    database_model_repository,
    database_experiment_repository,
    database_data_repository,
    database_user,
    database_connection,
    database_saved_model_repository,
    api_run,
):
    return database.run.Run(
        id=uuid.UUID(second_run_id_str),
        name=api_run.name,
        mlflow_run_id=api_run.mlflow_run_id,
        model_repository=database_model_repository,
        model_repository_id=database_model_repository.id,
        saved_model_repository=database_saved_model_repository,
        connection_id=database_connection.id,
        connection=database_connection,
        experiment_repository=database_experiment_repository,
        data_repository=database_data_repository,
        data_repository_id=database_data_repository.id,
        experiment_repository_id=database_experiment_repository.id,
        user_id=database_user.id,
        user=database_user,
        compute_budget_account=api_run.compute_budget_account,
        status=api_run.status,
        start_time=api_run.start_time,
        end_time=api_run.end_time,
        artifact_uri=api_run.artifact_uri,
        lifecycle_stage=api_run.lifecycle_stage,
        project_id=database_experiment_repository.project_id,
        mlflow_mlproject_file_path=api_run.mlflow_mlproject_file_path.as_posix(),
        entry_point=api_run.entry_point,
        mlflow_parameters=api_run.mlflow_parameters,
        backend_config=api_run.backend_config,
        created_at=api_run.created_at,
    )


@pytest.fixture()
def database_run_without_data_repository(
    run_id_str,
    database_model_repository,
    database_experiment_repository,
    database_user,
    database_connection,
    database_saved_model_repository,
    api_run,
):
    return database.run.Run(
        id=uuid.UUID(run_id_str),
        name=api_run.name,
        mlflow_run_id=api_run.mlflow_run_id,
        status=api_run.status,
        artifact_uri="test-artifact-uri",
        lifecycle_stage="test-lifecycle-stage",
        experiment_repository_id=database_experiment_repository.id,
        experiment_repository=database_experiment_repository,
        model_repository_id=database_model_repository.id,
        model_repository=database_model_repository,
        user_id=database_user.id,
        user=database_user,
        saved_model_repository=database_saved_model_repository,
        connection_id=database_connection.id,
        connection=database_connection,
        project_id=database_experiment_repository.project_id,
        start_time=api_run.start_time,
        end_time=api_run.end_time,
        mlflow_mlproject_file_path=api_run.mlflow_mlproject_file_path.as_posix(),
        entry_point=api_run.entry_point,
        mlflow_parameters=api_run.mlflow_parameters,
        backend_config=api_run.backend_config,
        compute_budget_account=api_run.compute_budget_account,
        created_at=api_run.created_at,
    )


@pytest.fixture
def run_schedule_id_str() -> str:
    return "edffb9dd-6dd1-4788-842a-ce514affddcf"


@pytest.fixture
def run_schedule_name_str() -> str:
    return "run schedule test"


@pytest.fixture
def run_schedule_aws_arn() -> str:
    return "arn:aws:lambda:us-east-2:123456789012:function:my-function:1"


@pytest.fixture
def api_add_run_schedule(
    run_schedule_name_str,
    user_id_str,
    run_id_str,
    connection_id_str,
    compute_budget_account,
):
    return models.run_schedule.AddRunSchedule(
        name=run_schedule_name_str,
        owner_id=user_id_str,
        run_id=run_id_str,
        connection_id=connection_id_str,
        compute_budget_account=compute_budget_account,
        cron_expression="0 12 * * ?",
        time_zone="Europe/Amsterdam",
        end_date=9230671843,
    )


@pytest.fixture
def api_run_schedule(
    run_schedule_id_str,
    run_schedule_name_str,
    user_id_str,
    api_user,
    compute_budget_account,
    api_run,
    api_connection,
    api_add_run_schedule,
    created_at,
):
    return models.run_schedule.RunSchedule(
        run_schedule_id=run_schedule_id_str,
        name=run_schedule_name_str,
        owner=api_user,
        run=api_run,
        connection=api_connection,
        compute_budget_account=api_add_run_schedule.compute_budget_account,
        cron_expression=api_add_run_schedule.cron_expression,
        time_zone=api_add_run_schedule.time_zone,
        end_date=api_add_run_schedule.end_date,
        created_at=created_at,
    )


@pytest.fixture
def api_run_info_unicore():
    return models.run_info.Unicore(
        id="ab768a82-a47d-4f3d-8478-9cda58372249",
        status=models.run_info.UnicoreStatus.SUCCESSFUL,
        type="a-job-type",
        logs=["test-log-1", "test-log-2"],
        owner="test-user",
        site_name="test-site-name",
        consumed_time=ConsumedTime(
            total="9",
            queued="N/A",
            stage_in="0",
            pre_command="N/A",
            main="N/A",
            post_command="N/A",
            stage_out="0",
        ),
        current_time="2023-01-01T00:00:00+0000",
        submission_time="2023-01-01T00:00:00+0000",
        termination_time="2023-01-01T00:00:00+0000",
        status_message="test-status-message",
        tags=["test-tag-1", "test-tag-2"],
        resource_status="test-resource-status",
        name="test-name",
        queue="test-queue",
        submission_preferences={},
        resource_status_message="N/A",
        acl=[],
        batch_system_id="52",
        exit_code="0",
        bss_details=None,
    )


@pytest.fixture
def api_run_info_firecrest():
    return models.run_info.Firecrest(
        id="test-job-id",
        user="test-user",
        state=models.run_info.FirecrestStatus.PENDING,
        name="test-name",
        nodelist="None assigned",
        nodes="1",
        partition="test-partition",
        cpu_time="0",
        elapsed_time="0",
        start_time="2023-01-01T00:00:00+0000",
        time="Unknown",
        time_left="Unknown",
        termination_time="Unknown",
        exit_code="0:0",
        job_data_err="",
        job_data_out="",
        job_file="/scratch/path/to/script.bath",
        job_file_err="/scratch/path/to/mantik.log",
        job_file_out="/scratch/path/to/mantik.log",
        job_info_extra="Job info returned successfully",
    )


@pytest.fixture
def api_run_info_as_json_unicore():
    return {
        "status": models.run_info.UnicoreStatus.SUCCESSFUL,
        "jobType": "a-job-type",
        "log": ["test-log-1", "test-log-2"],
        "owner": "test-user",
        "siteName": "test-site-name",
        "consumedTime": {
            "total": "9",
            "queued": "N/A",
            "stage-in": "0",
            "preCommand": "N/A",
            "main": "N/A",
            "postCommand": "N/A",
            "stage-out": "0",
        },
        "currentTime": "2023-01-01T00:00:00+0000",
        "submissionTime": "2023-01-01T00:00:00+0000",
        "terminationTime": "2023-01-01T00:00:00+0000",
        "statusMessage": "test-status-message",
        "tags": ["test-tag-1", "test-tag-2"],
        "resourceStatus": "test-resource-status",
        "name": "test-name",
        "queue": "test-queue",
        "submissionPreferences": {},
        "resourceStatusMessage": "N/A",
        "acl": [],
        "batchSystemID": "52",
        "exitCode": "0",
        "bssDetails": None,
    }


@pytest.fixture
def api_run_info_as_json_firecrest():
    return {
        "user": "test-user",
        "state": models.run_info.FirecrestStatus.PENDING,
        "name": "test-name",
        "nodelist": "None assigned",
        "nodes": 1,
        "partition": "test-partition",
        "cpu_time": "0",
        "elapsed_time": "0",
        "start_time": "2023-01-01T00:00:00+0000",
        "time": "Unknown",
        "time_left": "Unknown",
        "termination_time": "Unknown",
        "exit_code": "0:0",
        "job_data_err": "",
        "job_data_out": "",
        "job_file": "/scratch/path/to/script.bath",
        "job_file_err": "/scratch/path/to/mantik.log",
        "job_file_out": "/scratch/path/to/mantik.log",
        "job_info_extra": "Job info returned successfully",
    }


@pytest.fixture
def database_run_schedule(
    run_schedule_aws_arn,
    database_user,
    database_run,
    database_connection,
    api_run_schedule,
):
    return database.run_schedule.RunSchedule(
        id=api_run_schedule.run_schedule_id,
        aws_schedule_arn=run_schedule_aws_arn,
        name=api_run_schedule.name,
        owner_id=database_user.id,
        owner=database_user,
        run_id=database_run.id,
        run=database_run,
        connection_id=database_connection.id,
        connection=database_connection,
        compute_budget_account=api_run_schedule.compute_budget_account,
        cron_expression=api_run_schedule.cron_expression,
        time_zone=api_run_schedule.time_zone,
        end_date=api_run_schedule.end_date,
        project_id=database_run.project_id,
        created_at=api_run_schedule.created_at,
    )


@pytest.fixture
def run_configuration_id_str() -> str:
    return "8152ae0d-8a10-4eb0-9105-ee7bc266d975"


@pytest.fixture
def mlflow_mlproject_file_path() -> pathlib.Path:
    return pathlib.Path("should-be-unzipped/MLproject")


@pytest.fixture
def api_add_run_configuration(
    run_name,
    data_repository_id_str,
    experiment_repository_id_str,
    connection_id_str,
    code_repository_id_str,
    backend_config,
    mlflow_mlproject_file_path,
    entry_point,
    mlflow_parameters,
    compute_budget_account,
    user_id_str,
) -> models.run.AddRun:
    return models.run.AddRun(
        name=run_name,
        experiment_repository_id=uuid.UUID(experiment_repository_id_str),
        code_repository_id=uuid.UUID(code_repository_id_str),
        branch="main",
        commit=None,
        data_commit="123",
        data_branch="main",
        data_target_dir="data",
        connection_id=uuid.UUID(connection_id_str),
        compute_budget_account=compute_budget_account,
        mlflow_mlproject_file_path=mlflow_mlproject_file_path,
        entry_point=entry_point,
        mlflow_parameters=mlflow_parameters,
        backend_config=backend_config,
    )


@pytest.fixture
def api_add_minimal_run_configuration(
    run_name, experiment_repository_id_str, user_id_str, backend_config
) -> models.run.AddRun:
    return models.run.AddRun(
        name=run_name,
        experiment_repository_id=uuid.UUID(experiment_repository_id_str),
        backend_config=backend_config,
        mlflow_run_id=uuid.UUID("5707c97a-1e3f-409e-9527-be5ce7255723"),
    )


@pytest.fixture
def backend_config_firecrest_integration() -> dict:
    return {
        "Firecrest": {
            "ApiUrl": testing.firecrest.LOCAL_API_URL,
            "TokenUrl": testing.firecrest.LOCAL_TOKEN_URI,
            "Machine": testing.firecrest.LOCAL_MACHINE,
        },
        "Resources": {
            "Queue": testing.firecrest.LOCAL_PARTITION,
            "Nodes": 1,
        },
    }


@pytest.fixture
def api_add_run_configuration_firecrest(
    run_name,
    data_repository_id_str,
    experiment_repository_id_str,
    connection_id_firecrest_str,
    code_repository_id_str,
    backend_config_firecrest_integration,
    mlflow_mlproject_file_path,
    entry_point,
    mlflow_parameters,
    compute_budget_account,
    user_id_str,
) -> models.run.AddRun:
    return models.run.AddRun(
        name=run_name,
        experiment_repository_id=uuid.UUID(experiment_repository_id_str),
        code_repository_id=uuid.UUID(code_repository_id_str),
        branch="main",
        commit=None,
        connection_id=uuid.UUID(connection_id_firecrest_str),
        compute_budget_account=compute_budget_account,
        mlflow_mlproject_file_path=mlflow_mlproject_file_path,
        entry_point=entry_point,
        mlflow_parameters=mlflow_parameters,
        backend_config=backend_config_firecrest_integration,
    )


@pytest.fixture
def api_add_run_configuration_bitbucket_repo(
    run_name,
    data_repository_id_str,
    experiment_repository_id_str,
    connection_id_str,
    third_code_repository_id_str,
    backend_config,
    mlflow_mlproject_file_path,
    entry_point,
    mlflow_parameters,
    compute_budget_account,
    user_id_str,
) -> models.run.AddRun:
    return models.run.AddRun(
        name=run_name,
        experiment_repository_id=uuid.UUID(experiment_repository_id_str),
        code_repository_id=uuid.UUID(third_code_repository_id_str),
        branch=None,
        commit="test-commit",
        connection_id=uuid.UUID(connection_id_str),
        compute_budget_account=compute_budget_account,
        mlflow_mlproject_file_path=mlflow_mlproject_file_path,
        entry_point=entry_point,
        mlflow_parameters=mlflow_parameters,
        backend_config=backend_config,
    )


def exclude(d: dict, keys: list[str]) -> dict:
    return {key: value for key, value in d.items() if key not in keys}


@pytest.fixture
def api_run_configuration(
    run_configuration_id_str,
    api_user,
    project_id_str,
    api_add_run_configuration,
    api_code_repository,
    api_experiment_repository,
    api_connection,
    created_at,
):
    return models.run.RunConfiguration(
        run_configuration_id=uuid.UUID(run_configuration_id_str),
        name=api_add_run_configuration.name,
        user=api_user,
        experiment_repository=api_experiment_repository,
        code_repository=api_code_repository,
        branch=api_add_run_configuration.branch,
        commit=api_add_run_configuration.commit,
        data_branch=api_add_run_configuration.data_branch,
        data_commit=api_add_run_configuration.data_commit,
        data_target_dir=api_add_run_configuration.data_target_dir,
        connection=api_connection,
        compute_budget_account=api_add_run_configuration.compute_budget_account,
        mlflow_mlproject_file_path=api_add_run_configuration.mlflow_mlproject_file_path,
        entry_point=api_add_run_configuration.entry_point,
        mlflow_parameters=api_add_run_configuration.mlflow_parameters,
        backend_config=api_add_run_configuration.backend_config,
        user_role=database.role_details.ProjectRole.OWNER,
        created_at=created_at,
    )


@pytest.fixture
def database_run_configuration(
    api_run_configuration,
    database_user,
    database_experiment_repository,
    database_code_repository,
    database_connection,
):
    return database.run.RunConfiguration(
        id=api_run_configuration.run_configuration_id,
        name=api_run_configuration.name,
        project_id=database_code_repository.project_id,
        user=database_user,
        experiment_repository=database_experiment_repository,
        code_repository=database_code_repository,
        branch=api_run_configuration.branch,
        commit=api_run_configuration.commit,
        compute_budget_account=api_run_configuration.compute_budget_account,
        connection=database_connection,
        mlflow_mlproject_file_path=api_run_configuration.mlflow_mlproject_file_path.as_posix(),  # noqa: E501
        entry_point=api_run_configuration.entry_point,
        mlflow_parameters=api_run_configuration.mlflow_parameters,
        backend_config=api_run_configuration.backend_config,
        created_at=api_run_configuration.created_at,
    )


@pytest.fixture
def backend_config(unicore_api_url) -> dict:
    return {
        "UnicoreApiUrl": unicore_api_url,
        "Environment": {
            "Singularity": {"Path": "some/image/image1.sif", "Type": "remote"}
        },
        "Resources": {"Queue": "devel", "Nodes": 1},
    }


@pytest.fixture
def backend_config_firecrest() -> dict:
    return {
        "Firecrest": {
            "ApiUrl": testing.firecrest.TEST_API_URL,
            "TokenUrl": testing.firecrest.TEST_TOKEN_URL,
            "Machine": testing.firecrest.TEST_MACHINE,
        },
        "Environment": {
            "Singularity": {"Path": "some/image/image1.sif", "Type": "remote"}
        },
        "Resources": {"Queue": "normal", "Nodes": "1"},
    }


@pytest.fixture
def entry_point() -> str:
    return "some-path/file.exe"


@pytest.fixture
def mlflow_parameters() -> dict:
    return {"learning_rate": 0.5}


@pytest.fixture
def run_database_model(
    run_name: str,
    mlflow_mlproject_file_path: pathlib.Path,
    entry_point: str,
    mlflow_parameters: dict,
    backend_config: dict,
    created_at,
):
    return database.run.Run(
        id=uuid.UUID("4507c97a-1e3f-409e-9527-be5ce7255723"),
        name=run_name,
        mlflow_run_id=uuid.UUID("5707c97a-1e3f-409e-9527-be5ce7255723"),
        model_repository=database.model_repository.ModelRepository(
            id=uuid.UUID("bc777a95-d555-4463-bd29-db1f18840fcf"),
            uri="model_uri",
            code_repository=database.code_repository.CodeRepository(
                id=uuid.UUID("008cbbd0-0e66-4ee4-8019-ff2df19adae3"),
                uri="https://gitlab.com/mantik-ai/tutorials/"
                "-/tree/main/wine-quality-estimator/mlproject",
            ),
        ),
        experiment_repository=database.experiment_repository.ExperimentRepository(
            id=uuid.UUID("f3f60564-eeed-4e54-949a-3409c0329626"),
            name="test_experiment_name",
            mlflow_experiment_id=0,
        ),
        data_repository=database.data_repository.DataRepository(
            id=uuid.UUID("7c49a26f-91a5-44ff-97d1-51418055c4cd"),
            uri="data-uri",
        ),
        mlflow_mlproject_file_path=mlflow_mlproject_file_path,
        entry_point=entry_point,
        mlflow_parameters=mlflow_parameters,
        backend_config=backend_config,
        created_at=created_at,
    )


# Fixtures required for testing the invocation of the Compute Backend


@pytest.fixture()
def fake_zip_file_path_main() -> pathlib.Path:
    return pathlib.Path(__file__).parent / "resources/zip/test-repo-main.zip"


@pytest.fixture()
def fake_zip_file_pattern_github() -> re.Pattern:
    zip_name_pattern = re.compile(r"resources/zip/test-repo-.*?\.zip")
    return re.compile(f"{pathlib.Path(__file__).parent} / {zip_name_pattern}")


@pytest.fixture()
def fake_zip_file_path_commit_hash() -> pathlib.Path:
    return pathlib.Path(__file__).parent / "resources/zip/test-repo-commit-hash.zip"


@pytest.fixture()
def fake_zip_file_path_commit_hash_long() -> pathlib.Path:
    return (
        pathlib.Path(__file__).parent / "resources/zip/test-repo-commit-hash-long.zip"
    )


@pytest.fixture()
def fake_zip_file_path_self_hosted() -> pathlib.Path:
    return (
        pathlib.Path(__file__).parent / "resources/zip/test-repo-main@commit-hash.zip"
    )


@pytest.fixture()
def fake_compute_backend_url() -> str:
    return "https://test-compute-backend-url.com"


@pytest.fixture()
def fake_compute_backend_env_vars(fake_compute_backend_url) -> dict[str, str]:
    return {
        submit_run._COMPUTE_BACKEND_SERVER_URL_ENV_VAR: fake_compute_backend_url,
    }


@pytest.fixture
def mock_compute_backend_api_env_vars(fake_compute_backend_env_vars):
    with utils.env.env_vars_set(fake_compute_backend_env_vars):
        yield


# TODO: This is an unused fixture, we can clean it up
@pytest.fixture(autouse=True)
def mock_mlproject_file_path_validation(
    mock_mlflow_mlproject_file_path_validation,
    fake_gitlab_repo_url,
    mlflow_mlproject_file_path,
):
    with mock_mlflow_mlproject_file_path_validation(
        git_url=fake_gitlab_repo_url,
        platform=database.git_repository.Platform.GitLab,
        # The branch names of the repo ZIPs in `src/tests/resources/zip`.
        branch="main",
        commit="commit-hash",
        path=mlflow_mlproject_file_path,
    ):
        yield


@pytest.fixture
def mock_gitlab_zip_download(
    fake_zip_file_path_main,
    fake_zip_file_path_commit_hash,
    fake_gitlab_repo_url,
):
    with requests_mock.Mocker(real_http=True) as m:
        # Mock ZIP for main branch
        m.get(
            f"{fake_gitlab_repo_url}/-/archive/main/test-repo-main.zip",
            body=lambda *args, **kwargs: open(fake_zip_file_path_main, "rb"),
        )

        # Mock ZIP for commit hash
        m.get(
            f"{fake_gitlab_repo_url}/-/archive/commit-hash/test-repo-commit-hash.zip",
            body=lambda *args, **kwargs: open(fake_zip_file_path_commit_hash, "rb"),
        )

        yield m


@pytest.fixture
def invitation_user_to_project_id_str() -> str:
    return "1989f1db-7489-4afc-93b6-5f369c204224"


@pytest.fixture
def invitation_group_to_project_id_str() -> str:
    return "1989f1db-7489-4afc-93b6-5f369c204225"


@pytest.fixture
def invitation_organization_to_project_id_str() -> str:
    return "1989f1db-7489-4afc-93b6-5f369c204226"


@pytest.fixture
def invitation_user_to_organization_id_str() -> str:
    return "1989f1db-7489-4afc-93b6-5f369c204227"


@pytest.fixture
def invitation_group_to_organization_id_str() -> str:
    return "1989f1db-7489-4afc-93b6-5f369c204228"


@pytest.fixture
def invitation_user_to_group_id_str() -> str:
    return "1989f1db-7489-4afc-93b6-5f369c204229"


@pytest.fixture
def database_invitation_user_to_project(
    invitation_user_to_project_id_str,
    user_id_str,
    second_user_id_str,
    project_id_str,
    created_at,
):
    return database.invitation.Invitation(
        id=uuid.UUID(invitation_user_to_project_id_str),
        inviter_id=uuid.UUID(second_user_id_str),
        invited_id=uuid.UUID(user_id_str),
        invited_type=database.invitation.InvitedType.USER,
        invited_to_type=database.invitation.InvitedToType.PROJECT,
        role=database.project.ProjectRole.RESEARCHER,
        invited_to_id=uuid.UUID(project_id_str),
        created_at=created_at,
    )


@pytest.fixture
def invitation_second_user_to_project_id_str() -> str:
    return "1989f1db-7489-4afc-93b6-5f369c204230"


@pytest.fixture
def database_invitation_second_user_to_project(
    invitation_second_user_to_project_id_str,
    user_id_str,
    second_user_id_str,
    project_id_str,
    created_at,
):
    return database.invitation.Invitation(
        id=uuid.UUID(invitation_second_user_to_project_id_str),
        inviter_id=uuid.UUID(user_id_str),
        invited_id=uuid.UUID(second_user_id_str),
        invited_type=database.invitation.InvitedType.USER,
        invited_to_type=database.invitation.InvitedToType.PROJECT,
        role=database.project.ProjectRole.REPORTER,
        invited_to_id=uuid.UUID(project_id_str),
    )


@pytest.fixture
def database_invitation_group_to_project(
    invitation_group_to_project_id_str,
    user_group_id_str,
    second_user_id_str,
    project_id_str,
    created_at,
):
    return database.invitation.Invitation(
        id=uuid.UUID(invitation_group_to_project_id_str),
        inviter_id=uuid.UUID(second_user_id_str),
        invited_id=uuid.UUID(user_group_id_str),
        invited_type=database.invitation.InvitedType.GROUP,
        invited_to_type=database.invitation.InvitedToType.PROJECT,
        role=database.project.ProjectRole.RESEARCHER,
        invited_to_id=uuid.UUID(project_id_str),
        # Add 1 hour to test sorting
        created_at=created_at + datetime.timedelta(hours=1),
    )


@pytest.fixture
def database_invitation_organization_to_project(
    invitation_organization_to_project_id_str,
    organization_id_str,
    second_user_id_str,
    project_id_str,
    created_at,
):
    return database.invitation.Invitation(
        id=uuid.UUID(invitation_organization_to_project_id_str),
        inviter_id=uuid.UUID(second_user_id_str),
        invited_id=uuid.UUID(organization_id_str),
        invited_type=database.invitation.InvitedType.ORGANIZATION,
        invited_to_type=database.invitation.InvitedToType.PROJECT,
        role=database.project.ProjectRole.RESEARCHER,
        invited_to_id=uuid.UUID(project_id_str),
        # Add 2 hours to test sorting
        created_at=created_at + datetime.timedelta(hours=2),
    )


@pytest.fixture
def database_invitation_user_to_organization(
    invitation_user_to_organization_id_str,
    user_id_str,
    second_user_id_str,
    organization_id_str,
    created_at,
):
    return database.invitation.Invitation(
        id=uuid.UUID(invitation_user_to_organization_id_str),
        inviter_id=uuid.UUID(second_user_id_str),
        invited_id=uuid.UUID(user_id_str),
        invited_type=database.invitation.InvitedType.USER,
        invited_to_type=database.invitation.InvitedToType.ORGANIZATION,
        invited_to_id=uuid.UUID(organization_id_str),
        # Add 3 hours later to test sorting
        created_at=created_at + datetime.timedelta(hours=3),
    )


@pytest.fixture
def database_invitation_group_to_organization(
    invitation_group_to_organization_id_str,
    user_group_id_str,
    second_user_id_str,
    organization_id_str,
    created_at,
):
    return database.invitation.Invitation(
        id=uuid.UUID(invitation_group_to_organization_id_str),
        inviter_id=uuid.UUID(second_user_id_str),
        invited_id=uuid.UUID(user_group_id_str),
        invited_type=database.invitation.InvitedType.GROUP,
        invited_to_type=database.invitation.InvitedToType.ORGANIZATION,
        invited_to_id=uuid.UUID(organization_id_str),
        # Add 4 hours later to test sorting
        created_at=created_at + datetime.timedelta(hours=4),
    )


@pytest.fixture
def database_invitation_user_to_group(
    invitation_user_to_group_id_str,
    user_id_str,
    second_user_id_str,
    user_group_id_str,
    created_at,
):
    return database.invitation.Invitation(
        id=uuid.UUID(invitation_user_to_group_id_str),
        inviter_id=uuid.UUID(second_user_id_str),
        invited_id=uuid.UUID(user_id_str),
        invited_type=database.invitation.InvitedType.USER,
        invited_to_type=database.invitation.InvitedToType.GROUP,
        invited_to_id=uuid.UUID(user_group_id_str),
        # Add 5 hours later to test sorting
        created_at=created_at + datetime.timedelta(hours=5),
    )


@pytest.fixture
def add_empty_project(
    database_client,
    clear_and_init_database,
    add_database_users,
    add_example_project,
    database_environment,
    database_user,
):
    with database_client.session():
        project_id = uuid.uuid4()
        project = database.project.Project(
            id=project_id,
            name="empty-project",
            owner=database_user,
            code_repositories=[],
            experiment_repositories=[],
            model_repositories=[],
            data_repositories=[],
            public=True,
        )
        database_client.add(project)
        return project


@pytest.fixture()
def set_required_aws_env_vars():
    os.environ["AWS_ACCESS_KEY_ID"] = "test-access-key-id"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "test-secret-access-key"
    os.environ["AWS_SECURITY_TOKEN"] = "test-security-token"
    os.environ["AWS_SESSION_TOKEN"] = "test-session-token"
    os.environ["AWS_DEFAULT_REGION"] = "us-east-1"
    yield
    os.environ.pop("AWS_ACCESS_KEY_ID", None)
    os.environ.pop("AWS_SECRET_ACCESS_KEY", None)
    os.environ.pop("AWS_SECURITY_TOKEN", None)
    os.environ.pop("AWS_SESSION_TOKEN", None)
    os.environ.pop("AWS_DEFAULT_REGION", None)


@pytest.fixture()
def set_required_scheduler_env_vars(set_required_aws_env_vars) -> None:
    os.environ["RUN_SCHEDULER_DEAD_LETTER_QUEUE_ARN"] = "test-dead-letter-queue-arn"
    os.environ["RUN_SCHEDULER_LAMBDA_ARN"] = "test-lambda-arn"
    os.environ["RUN_SCHEDULER_ACCESS_KEY_ID"] = "test-access-key-id"
    os.environ["RUN_SCHEDULER_SECRET_ACCESS_KEY"] = "test-secret-access-key"
    os.environ["RUN_SCHEDULER_REGION"] = "us-east-1"
    os.environ["RUN_SCHEDULER_ROLE_ARN"] = "test-role-arn"
    yield
    os.environ.pop("RUN_SCHEDULER_DEAD_LETTER_QUEUE_ARN", None)
    os.environ.pop("RUN_SCHEDULER_LAMBDA_ARN", None)
    os.environ.pop("RUN_SCHEDULER_ACCESS_KEY_ID", None)
    os.environ.pop("RUN_SCHEDULER_SECRET_ACCESS_KEY", None)
    os.environ.pop("RUN_SCHEDULER_REGION", None)
    os.environ.pop("RUN_SCHEDULER_ROLE_ARN", None)


@pytest.fixture()
def run_schedule_properties(
    api_run_schedule, compute_budget_account
) -> scheduler.schedule.Properties:
    return scheduler.schedule.Properties(
        id=api_run_schedule.run_schedule_id,
        name=api_run_schedule.name,
        run_id=uuid.uuid4(),
        project_id=uuid.uuid4(),
        connection_id=uuid.uuid4(),
        compute_budget_account=compute_budget_account,
        token=testing.token_verifier.create_token("test-token"),
        cron_expression=api_run_schedule.cron_expression,
        time_zone=api_run_schedule.time_zone,
        end_date=datetime.datetime.fromtimestamp(api_run_schedule.end_date),
    )


@pytest.fixture()
def mock_boto3_scheduler_client(
    set_required_scheduler_env_vars,
) -> scheduler.client.Client:
    with moto.mock_scheduler():
        yield


@pytest.fixture()
def scheduler_client(
    set_required_scheduler_env_vars, mock_boto3_scheduler_client
) -> scheduler.client.Client:
    with scheduler.client.Client() as client:
        yield client


@pytest.fixture
def remote_data_repository_id_str() -> str:
    return "8aaf61c4-1ad4-45af-bf94-50da4f5b29db"


@pytest.fixture
def remote_environment_id_str() -> str:
    return "ec894a74-0ce1-4604-9060-ca4915deb0c3"


@pytest.fixture
def api_remote_data_repository(
    api_labels, remote_data_repository_id_str, api_data_repository, created_at
) -> models.data_repository.DataRepository:
    return models.data_repository.DataRepository(
        data_repository_id=remote_data_repository_id_str,
        uri="hpc://much-power/very-fast/image.sif",
        platform=database.git_repository.Platform.GitLab,
        labels=api_labels,
        created_at=created_at,
    )


@pytest.fixture
def database_remote_data_repository(
    api_remote_data_repository,
    database_labels,
    project_id_str: str,
):
    return database.data_repository.DataRepository(
        id=api_remote_data_repository.data_repository_id,
        uri=api_remote_data_repository.uri,
        labels=database_labels,
        project_id=uuid.UUID(project_id_str),
        created_at=api_remote_data_repository.created_at,
    )


@pytest.fixture
def api_add_remote_environment(
    database_remote_data_repository, api_add_environment, remote_data_repository_id_str
):
    return models.environment.AddEnvironment(
        name=api_add_environment.name,
        type=api_add_environment.type,
        variables=api_add_environment.variables,
        data_repository_id=uuid.UUID(remote_data_repository_id_str),
    )


@pytest.fixture
def api_remote_environment(
    database_remote_data_repository,
    api_add_environment,
    remote_data_repository_id_str,
    api_remote_data_repository,
    remote_environment_id_str,
    created_at,
):
    return models.environment.Environment(
        environment_id=uuid.UUID(remote_environment_id_str),
        name=api_add_environment.name,
        type=api_add_environment.type,
        variables=api_add_environment.variables,
        data_repository=api_remote_data_repository,
        created_at=created_at,
    )


@pytest.fixture
def remote_database_environment(
    api_remote_environment,
):
    return database.environment.Environment(
        id=api_remote_environment.environment_id,
        name=api_remote_environment.name,
        type=api_remote_environment.type,
        variables=api_remote_environment.variables,
        data_repository_id=api_remote_environment.data_repository.data_repository_id,
        created_at=api_remote_environment.created_at,
    )


@pytest.fixture
def add_remote_environment_to_project(
    database_client,
    clear_and_init_database,
    add_database_users,
    add_example_project,
    remote_database_environment,
    database_remote_data_repository,
) -> None:
    with database_client.session():
        database_client.add(database_remote_data_repository)
        database_client.add(remote_database_environment)


@pytest.fixture()
def mock_mlflow_mlproject_file_path_validation(
    fake_gitlab_repo_url,
    fake_github_repo_url,
    fake_bitbucket_org_repo_url,
    fake_self_hosted_repo_url,
    fake_branch,
    mlflow_mlproject_file_path,
) -> t.Callable:
    def create_url(
        git_url: str,
        platform: database.git_repository.Platform,
        branch: str | None,
        commit: str | None,
        path: pathlib.Path,
    ):
        match platform:
            case database.git_repository.Platform.GitLab:
                url = git_utils.raw_file_gitlab_url(
                    url=git_url, filepath=str(path), ref=commit or branch
                )
            case database.git_repository.Platform.GitHub:
                url = git_utils.raw_file_github_url(
                    repo="test-owner/test-repo",
                    ref=commit or branch,
                    filepath=str(path),
                )
            case database.git_repository.Platform.Bitbucket:
                url = git_utils.raw_file_bitbucket_url(
                    repo=fake_bitbucket_org_repo_url
                    if "bitbucket.org" in git_url
                    else fake_self_hosted_repo_url,
                    ref=commit or branch,
                    filepath=str(path),
                    domain="bitbucket.org"
                    if "bitbucket.org" in git_url
                    else "self-hosted",
                )
            case _:
                raise ValueError(f"{platform} not available for testing")
        return url

    @contextlib.contextmanager
    def mock(
        git_url: str,
        platform: database.git_repository.Platform = database.git_repository.Platform.GitLab,  # noqa E501
        branch: str = fake_branch,
        commit: str | None = None,
        status_code: int = 200,
        path: pathlib.Path = mlflow_mlproject_file_path,
    ):
        with requests_mock.Mocker(real_http=True) as m:
            m.get(
                create_url(
                    git_url=git_url,
                    branch=branch,
                    commit=None,
                    path=path,
                    platform=platform,
                ),
                status_code=status_code,
            )

            if commit is not None:
                m.get(
                    create_url(
                        git_url=git_url,
                        branch=None,
                        commit=commit,
                        path=path,
                        platform=platform,
                    ),
                    status_code=status_code,
                )

            yield m

    return mock


@pytest.fixture
def mock_hvac(
    monkeypatch,
    connection_id_str,
    connection_id_firecrest_str,
    connection_second_user_id_str,
    connection_id_slurm_private_key_str,
    second_connection_id_str,
):
    class FakeHVACClient:
        token = None
        url = None

        def __init__(self, url):
            self.url = url

        class Auth:
            class Token:
                def lookup_self(self):
                    return {"data": {"entity_id": "test_entity_id"}}

            token = Token()

            class JWT:
                role = None
                jwt = None

                def jwt_login(self, role, jwt):
                    self.role = role
                    self.jwt = jwt
                    return {"auth": {"client_token": "test_client_token"}}

            jwt = JWT()

        auth = Auth()

        class Secrets:
            class KV:
                class V2:
                    secrets_data = {
                        f"test_entity_id/{connection_id_str}": {
                            "login_name": UNICORE_DEFAULT_USERNAME,
                            "password": UNICORE_DEFAULT_PASSWORD,
                            "token": None,
                            "private_key": None,
                        },
                        f"test_entity_id/{second_connection_id_str}": {
                            "login_name": UNICORE_DEFAULT_USERNAME,
                            "password": UNICORE_DEFAULT_PASSWORD,
                            "token": None,
                            "private_key": None,
                        },
                        f"test_entity_id/{connection_second_user_id_str}": {
                            "login_name": UNICORE_DEFAULT_USERNAME,
                            "password": UNICORE_DEFAULT_PASSWORD,
                            "token": None,
                            "private_key": None,
                        },
                        f"test_entity_id/{connection_id_firecrest_str}": {
                            "login_name": testing.firecrest.LOCAL_CLIENT_ID,
                            "password": testing.firecrest.LOCAL_CLIENT_SECRET,
                            "token": None,
                            "private_key": None,
                        },
                        f"test_entity_id/{connection_id_slurm_private_key_str}": {
                            "login_name": None,
                            "password": None,
                            "token": None,
                            "private_key": SSH_PRIVATE_KEY_EXAMPLE,
                        },
                    }

                    def delete_latest_version_of_secret(self, path):
                        self.secrets_data.pop(path)

                    def create_or_update_secret(self, path, secret):
                        self.secrets_data[path] = secret

                    def read_secret(self, path):
                        return {"data": {"data": self.secrets_data[path]}}

                v2 = V2()

            kv = KV()

        secrets = Secrets()

    monkeypatch.setattr(
        hvac,
        "Client",
        FakeHVACClient,
    )
    with utils.env.env_vars_set({"VAULT_URL": "fake_vault_url"}):
        yield


@pytest.fixture()
def cognito_auth_response() -> dict:
    path = _FILE_PATH / "resources/cognito/auth-response.json"
    return _open_json_file(path)


@pytest.fixture()
def cognito_refresh_response() -> dict:
    path = _FILE_PATH / "resources/cognito/refresh-response.json"
    return _open_json_file(path)


@pytest.fixture()
def cognito_incorrect_login_credentials_response() -> dict:
    path = _FILE_PATH / "resources/cognito/incorrect-login-credentials-response.json"
    return _open_json_file(path)


@pytest.fixture()
def cognito_user_not_confirmed_response() -> dict:
    path = _FILE_PATH / "resources/cognito/user-not-confirmed-response.json"
    return _open_json_file(path)


@pytest.fixture()
def cognito_user_not_found_response() -> dict:
    path = _FILE_PATH / "resources/cognito/user-not-found-response.json"
    return _open_json_file(path)


@pytest.fixture()
def cognito_refresh_token_expired_response() -> dict:
    path = _FILE_PATH / "resources/cognito/refresh-token-expired-response.json"
    return _open_json_file(path)


@pytest.fixture()
def cognito_refresh_token_invalid_response() -> dict:
    path = _FILE_PATH / "resources/cognito/refresh-token-invalid-response.json"
    return _open_json_file(path)


@pytest.fixture()
def cognito_different_client_response() -> dict:
    path = _FILE_PATH / "resources/cognito/different-client.json"
    return _open_json_file(path)


def _open_json_file(path: pathlib.Path) -> dict:
    with open(path) as json_response:
        return json.load(json_response)


@pytest.fixture
def moto_user_pool_env():
    def moto_user_pool_env_wrapped():
        user_pool_config = {
            "PoolName": "MantikUserPool",
            "AutoVerifiedAttributes": ["email"],
            "MfaConfiguration": "OFF",
            "Policies": {
                "PasswordPolicy": {
                    "MinimumLength": 8,
                    "RequireLowercase": True,
                    "RequireNumbers": True,
                    "RequireSymbols": True,
                    "RequireUppercase": True,
                    "TemporaryPasswordValidityDays": 7,
                }
            },
            "Schema": [{"Mutable": True, "Name": "email", "Required": True}],
            "UserAttributeUpdateSettings": {
                "AttributesRequireVerificationBeforeUpdate": ["email"]
            },
            "AccountRecoverySetting": {
                "RecoveryMechanisms": [
                    {
                        "Priority": 1,
                        "Name": "verified_email",
                    },
                ]
            },
        }
        cognito_region = "eu-central-1"
        with utils.env.env_vars_set(
            {cognito.client.COGNITO_REGION_ENV_VAR: cognito_region}
        ):
            client = boto3.client("cognito-idp", region_name=cognito_region)
            user_pool_id = client.create_user_pool(**user_pool_config)["UserPool"]["Id"]
            user_pool_client_id = client.create_user_pool_client(
                UserPoolId=user_pool_id, ClientName="MantikCognitoClient"
            )["UserPoolClient"]["ClientId"]
            return user_pool_id, user_pool_client_id

    return moto_user_pool_env_wrapped


@pytest.fixture
def moto_user_pool(moto_user_pool_env) -> tuple[str, str]:
    """Use moto to mock Cognito methods.

    Creates a User Pool with client.

    Returns
    -------
    user_pool_id : str
    user_pool_client_id : str

    """
    with moto.mock_cognitoidp():
        user_pool_id, user_pool_client_id = moto_user_pool_env()
        with testing.aws.cognito.cognito_env_vars_set(
            user_pool_id=user_pool_id, user_pool_client_id=user_pool_client_id
        ):
            yield user_pool_id, user_pool_client_id


@pytest.fixture()
def cognito_patcher(
    moto_user_pool,
) -> testing.aws.cognito.MotoCognitoPatcherForUnimplemtedApis:
    """Allows to patch any unimplemented moto methods."""
    with testing.aws.cognito.MotoCognitoPatcherForUnimplemtedApis().enable() as patcher:
        yield patcher


@pytest.fixture()
def boto3_cognito_idp_client(moto_user_pool):
    return boto3.client("cognito-idp", region_name="eu-central-1")


@pytest.fixture()
def create_fake_cognito_user(boto3_cognito_idp_client) -> dict[str, str]:
    data = {
        # Internally, we treat users with the preferred_name,
        # but Cognito requires the cognito_name.
        "username": testing.aws.cognito.COGNITO_USERNAME,
        "password": testing.aws.cognito.PASSWORD,
        "email": testing.aws.cognito.EMAIL,
    }
    cognito.users.create_cognito_user(**data)
    return data


@pytest.fixture()
def create_and_confirm_fake_cognito_user(
    boto3_cognito_idp_client, create_fake_cognito_user
) -> dict[str, str]:
    boto3_cognito_idp_client.admin_confirm_sign_up(
        UserPoolId=os.getenv(cognito.client.COGNITO_USER_POOL_ID_ENV_VAR),
        Username=testing.aws.cognito.COGNITO_USERNAME,
    )
    return create_fake_cognito_user
