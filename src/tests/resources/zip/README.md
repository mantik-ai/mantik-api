# Test ZIP file

`test-repo-main.zip`, `test-repo-commit-hash`, `test-repo-commit-hash-long` and `test-repo-main@commit-hash` are ZIP files for testing of unzipping git repos,
which is required for submitting runs from API to Compute Backend.

They represent the `main` branch and `commit-hash` or `commit-hash-long` commit hash, respectively, of a repo named `test-repo`.

We only want to unzip the MLflow project directory (here `should-be-unzipped`),
since this is the only folder from a git repo that needs to be submitted to the Compute Backend.

The ZIP has the following folder structure:
```
| test-zip-file
|-- should-be-unzipped
|---- should-be-unzipped.txt
|---- MLproject
|-- should-not-be-unzipped.txt
```