import os

import pytest
import sqlalchemy.exc

import mantik_api.database as database
import mantik_api.database.base as base
import mantik_api.database.curation._parse as _parse
import mantik_api.database.curation.labels as _labels
import mantik_api.utils.env as env

_DEFAULT_URI = "postgresql+psycopg2://postgres:postgres@localhost:5432/{db_name}"
_TEST_POSTGRES_URI = os.environ.get("TEST_POSTGRES_URI", _DEFAULT_URI)


@pytest.fixture(scope="module")
def engine():
    engine = sqlalchemy.create_engine(_TEST_POSTGRES_URI.format(db_name="test_db"))
    return engine


@pytest.fixture
def session(engine):
    try:
        base.Base.metadata.drop_all(bind=engine, checkfirst=False)
    except sqlalchemy.exc.ProgrammingError:
        # Table not found; DB has not been initialized
        pass
    base.Base.metadata.create_all(bind=engine)
    session_handler = database.client.sessions.SessionHandler(engine=engine)
    return session_handler


@pytest.fixture()
def database_client(engine, session) -> database.client.main.Client:
    return database.client.main.Client(engine=engine, session_handler=session)


def test_parse_yaml_to_database_models_real_yaml(labels_yaml_real_path):
    _parse.parse_yaml_to_labels(labels_yaml_real_path)


def test_crud_labels_real(monkeypatch, database_client, labels_yaml_real_path):
    monkeypatch.setattr(
        database.client.main.Client,
        "from_env",
        lambda: database_client,
    )

    with env.env_vars_set(
        {_labels.LABELS_YAML_PATH_ENV_VAR: str(labels_yaml_real_path)}
    ):
        _labels.crud_labels()
