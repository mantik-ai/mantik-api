"""
It is tested that
the Mantik API database is properly initialized (tables are present),
connected to the API and that the API works as expected.

The test adds a test user to the database and tries to fetch it again.
The user is not added if at least one user is present in the database,
since we suppose that this test user would be already present in that case.

Since we are using two different databases for users, the Mantik API database and
a AWS Cognito UserPool.
In this case this user is only added in the Mantik API database,
which is not responsible for user log-in,
so that the test credentials are not valid.
"""
import os

import requests
import starlette.status as status

if __name__ == "__main__":
    base_url = os.getenv("MANTIK_BASE_URL")
    create_request = {
        "username": os.getenv("MANTIK_USERNAME"),
        "password": os.getenv("MANTIK_PASSWORD"),
    }
    tokens = requests.post(f"{base_url}mantik/tokens/create", json=create_request)

    assert tokens.status_code == status.HTTP_201_CREATED

    test_user_id = "492fd69f-aa81-4ab4-a3b9-3dcebe8c9635"

    test_user = {"user_id": test_user_id, "name": "test_name", "email": "test_email"}

    users_request = {"startindex": 0, "pagelength": 20}

    response = requests.get(
        f"{base_url}/users",
        json=users_request,
        headers={"Authorization": f"Bearer {tokens.json()['AccessToken']}"},
    )

    print(response.json())

    assert response.status_code == status.HTTP_200_OK

    if response.json()["total_records"] == 0:
        print("Adding new user...")
        insert = requests.put(
            f"{base_url}/users",
            json=test_user,
            headers={"Authorization": f"Bearer {tokens.json()['AccessToken']}"},
        )
        print("...successfully")

    response = requests.get(
        f"{base_url}/users/{test_user_id}",
        json=users_request,
        headers={"Authorization": f"Bearer {tokens.json()['AccessToken']}"},
    )

    print(response.json())

    assert response.status_code == status.HTTP_200_OK
