import sqlalchemy

import mantik_api.database.base as orm_base
import mantik_api.database.client.main as client

if __name__ == "__main__":
    uri = client._get_uri_from_env()
    engine = sqlalchemy.create_engine(uri)
    orm_base.Base.metadata.create_all(bind=engine)
