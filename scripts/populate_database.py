"""
A first draft of how data migrations could look like,
It is used to test whether the migration
2024-02-02T10-04_2efbf4239b40_268_model_naming_implemented_and_.py
was successful in setting the uuid as the name
for trained model already present in the database,
since name is a new but also a required field.

NOTE: this code could be improved and more generalized,
it is just a first draft, feel free to extend it.
"""
import os
import uuid

import sqlalchemy

IDS = {
    "user1": uuid.UUID("e7bc8999-f384-4dfe-a030-22fa77f1f961"),
    "user2": uuid.UUID("ef3a8384-1ff2-48d6-a6b4-2fbe3d4da982"),
    "user_group": uuid.UUID("25251b04-138d-4527-bb63-a064f85c5746"),
    "project": uuid.UUID("79610cb9-e36b-4bfb-96cc-1a5b71217264"),
    "code_repository": uuid.UUID("2bb7edde-51f1-416e-8b36-44331124ce59"),
    "experiment_repository": uuid.UUID("9d1a687a-7173-4bfe-8542-4304c4158d54"),
    "model_repository": uuid.UUID("81d9d501-481a-4087-9a46-89ffabdc4baf"),
    "data_repository": uuid.UUID("a4610fb0-8260-4ce5-9087-f761e54df941"),
    "trained_model": uuid.UUID("05f7b1e3-3b3d-482d-843e-19ed72357483"),
}

MODEL_ORDER = [
    "user",
    "user_group",
    "user_group_user_association_table",
    "project",
    "code_repository",
    "experiment_repository",
    "model_repository",
    "data_repository",
    "trained_model",
]

SQL_MODELS = {
    "user": """INSERT INTO mantik_user (updated_at,
    user_id, cognito_name, preferred_name, email, address,
    payment_method, payment_details, full_name,
    info, company, job_title, website_url, vm_urls) VALUES (
    :updated_at, :user_id, :cognito_name,
    :preferred_name, :email, :address, :payment_method,
    :payment_details, :full_name, :info,
    :company, :job_title, :website_url, :vm_urls)""",
    "user_group": """INSERT INTO user_group
    (updated_at, user_group_id, name, user_id) VALUES
    (:updated_at, :user_group_id, :name, :user_id)""",
    "user_group_user_association_table": """INSERT INTO
    user_group_user_association_table (updated_at, user_group_id,
    user_id) VALUES (:updated_at, :user_group_id, :user_id)""",
    "project": """INSERT INTO project (updated_at,
    project_id, name, owner_id, executive_summary, detailed_description,
    public) VALUES (:updated_at, :project_id, :name,
    :owner_id, :executive_summary, :detailed_description, :public)""",
    "code_repository": """INSERT INTO code_repository
    (updated_at, code_repository_id, uri, code_repository_name,
    access_token, description, project_id, platform)
    VALUES (:updated_at, :code_repository_id, :uri,
    :code_repository_name, :access_token, :description, :project_id, :platform)""",
    "experiment_repository": """INSERT INTO experiment_repository
    (updated_at, experiment_repository_id,
     mlflow_experiment_id, name, artifact_location,
    lifecycle_stage, last_update_time, creation_time, project_id) VALUES
    (:updated_at, :experiment_repository_id, :mlflow_experiment_id, :name,
    :artifact_location, :lifecycle_stage,
    :last_update_time, :creation_time, :project_id)""",
    "model_repository": """INSERT INTO model_repository
    (updated_at, model_repository_id, uri, description,
    code_repository_id, branch, commit, project_id) VALUES
    (:updated_at, :model_repository_id, :uri, :description,
    :code_repository_id, :branch, :commit, :project_id)""",
    "data_repository": """INSERT INTO data_repository
    (updated_at, data_repository_id, uri, data_repository_name,
    access_token, description, project_id) VALUES
    (:updated_at, :data_repository_id, :uri,
    :data_repository_name, :access_token, :description, :project_id)""",
    "trained_model": """INSERT INTO trained_model
    (updated_at, model_id, model_repository_id,
    uri, location, project_id, connection_id) VALUES
    (:updated_at, :model_id, :model_repository_id,
    :uri, :location, :project_id, :connection_id)""",
}

SQL_DATA = {
    "user": [
        {
            "updated_at": None,
            "user_id": IDS["user1"],
            "cognito_name": "test-user-1-cognito",
            "preferred_name": "test-user-1-preferred",
            "email": "test-email-1",
            "address": None,
            "payment_method": None,
            "payment_details": None,
            "full_name": None,
            "info": None,
            "company": None,
            "job_title": None,
            "website_url": None,
            "vm_urls": None,
        },
        {
            "updated_at": None,
            "user_id": IDS["user2"],
            "cognito_name": "test-user-2-cognito",
            "preferred_name": "test-user-2-preferred",
            "email": "test-email-2",
            "address": None,
            "payment_method": None,
            "payment_details": None,
            "full_name": None,
            "info": None,
            "company": None,
            "job_title": None,
            "website_url": None,
            "vm_urls": None,
        },
    ],
    "user_group": [
        {
            "updated_at": None,
            "user_group_id": IDS["user_group"],
            "name": "test-user-group",
            "user_id": IDS["user1"],
        }
    ],
    "user_group_user_association_table": [
        {
            "updated_at": None,
            "user_group_id": IDS["user_group"],
            "user_id": IDS["user2"],
        }
    ],
    "project": [
        {
            "updated_at": None,
            "project_id": IDS["project"],
            "name": "test-project",
            "owner_id": IDS["user1"],
            "executive_summary": None,
            "detailed_description": None,
            "public": True,
        }
    ],
    "code_repository": [
        {
            "updated_at": None,
            "code_repository_id": IDS["code_repository"],
            "uri": "https://test-uri.org",
            "code_repository_name": None,
            "access_token": None,
            "description": None,
            "project_id": IDS["project"],
            "platform": "GitLab",
        }
    ],
    "experiment_repository": [
        {
            "updated_at": None,
            "experiment_repository_id": IDS["experiment_repository"],
            "mlflow_experiment_id": 0,
            "name": "test-experiment-repository",
            "artifact_location": None,
            "lifecycle_stage": None,
            "last_update_time": None,
            "creation_time": None,
            "project_id": IDS["project"],
        }
    ],
    "model_repository": [
        {
            "updated_at": None,
            "model_repository_id": IDS["model_repository"],
            "uri": None,
            "description": None,
            "code_repository_id": IDS["code_repository"],
            "branch": None,
            "commit": None,
            "project_id": IDS["project"],
        }
    ],
    "data_repository": [
        {
            "updated_at": None,
            "data_repository_id": IDS["data_repository"],
            "uri": "test-data-repository",
            "data_repository_name": None,
            "access_token": None,
            "description": None,
            "project_id": IDS["project"],
        }
    ],
    "trained_model": [
        {
            "updated_at": None,
            "model_id": IDS["trained_model"],
            "model_repository_id": IDS["model_repository"],
            "uri": None,
            "location": "my-test-location",
            "project_id": IDS["project"],
            "connection_id": None,
        }
    ],
}

LOCAL_POSTGRES_URI = "postgresql+psycopg2://postgres:postgres@localhost:5432/{db_name}"

if __name__ == "__main__":
    engine = sqlalchemy.create_engine(
        os.environ.get("TEST_POSTGRES_URI", LOCAL_POSTGRES_URI).format(
            db_name="test_db"
        )
    )
    with engine.connect() as con:
        for model_name in MODEL_ORDER:
            statement = sqlalchemy.text(SQL_MODELS[model_name])
            data = SQL_DATA[model_name]
            for data_point in data:
                con.execute(statement, **data_point)
