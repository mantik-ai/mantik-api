build:
	docker compose -f docker/docker-compose.yml build --no-cache

up: stop build
	docker compose -f docker/docker-compose.yml up -d --remove-orphans

stop:
	docker compose -f docker/docker-compose.yml stop
	docker compose -f docker/docker-compose.yml rm -f

up-db: stop-db
	docker compose -f docker/docker-compose.yml up -d --remove-orphans db

stop-db:
	docker compose -f docker/docker-compose.yml stop db
	docker compose -f docker/docker-compose.yml rm -f db

up-integration: up-db
	docker compose -f docker/docker-compose.yml up -d --remove-orphans tracking-server tracking-server-db unicore

stop-integration: stop-db
	docker compose -f docker/docker-compose.yml stop tracking-server tracking-server-db unicore
	docker compose -f docker/docker-compose.yml rm -f tracking-server tracking-server-db unicore

re-build-run-db: stop-db build up-db

create-migrations: stop-db up-db
	# bring it up to date
	poetry run alembic upgrade head

	# generate migration based on current SQLAlchemy models
	poetry run alembic revision --autogenerate -m "$(shell git branch --show-current)"

	# apply newest migration to local database (if desired)
	poetry run alembic upgrade head

	$(MAKE) stop-db

run-migrations: stop-db up-db
	poetry run alembic upgrade head

test-unit:
	poetry run pytest src/tests/unit $(args)

test-integration: up-integration
	poetry run pytest -n auto src/tests/integration $(args)

test: up-integration
	poetry run pytest -n auto src/tests/integration src/tests/unit $(args)

test-data-migrations: stop-db up-db
	echo "Migrations will start from b78c1e1e8d3a, if you want to insert to a table that was not present before this version it will fail"
	poetry run alembic upgrade b78c1e1e8d3a
	poetry run python3 scripts/populate_database.py
	poetry run alembic upgrade head
	$(MAKE) stop-db