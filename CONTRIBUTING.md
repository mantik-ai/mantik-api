# Contributing to mantik API

<!-- TOC -->
- [Governance](#governance)
- [How to Contribute](#how-to-contribute)
  - [Reporting bugs](#reporting-bugs)
  - [Suggesting improvements](#suggesting-improvements)
- [Code contributions](#code-contributions)
  - [SQLAlchemy conventions](#sqlalchemy-conventions)
  - [Development and testing](#development-and-testing)
    - [Setup and unit tests](#setup-and-unit-tests)
    - [Integration tests](#integration-tests)
      - [Requirements](#requirements)
      - [Run tests in docker](#run-tests-in-docker)
      - [Run tests locally](#run-tests-locally)
    - [Test CI locally](#test-ci-locally)
    - [Database Migrations](#database-migrations)
  - [API Versioning](#api-versioning)
    - [Introducing API-breaking Changes or New APIs](#introducing-api-breaking-changes-or-new-apis)
    - [Upgrade Mantik API Version](#upgrade-mantik-api-version)
  - [Merge requests](#merge-requests)
  - [Labels](#labels)
    - [Adding a new label](#adding-a-new-label)
    - [Label Identifier Conventions](#label-identifier-conventions)
    - [Adding a new scope](#adding-a-new-scope)
  - [Known Issues](#known-issues)
    - [A string literal cannot contain NUL (0x00) characters](#a-string-literal-cannot-contain-nul-0x00-characters)
- [Contributors](#contributors)
<!-- /TOC -->

## Governance

Governance of mantik is conducted by [ambrosys GmbH](https://ambrosys.de)
represented by

* [Fabian Emmerich](https://gitlab.com/fabian.emmerich) [(fabian.emmerich@4-cast.de)](mailto:fabian.emmerich@4-cast.de)
* [Thomas Seidler](https://gitlab.com/thomas_ambrosys) [(thomas.seidler@ambrosys.de)](mailto:thomas.seidler@ambrosys.de)

## How to Contribute

### Reporting bugs

This section guides you through submitting a bug report for the mantik API.
Following these guidelines helps understand your report, reproduce the behavior, and find related reports.

**Notes:**

* Before submitting a bug report, check that your **issue does not already exist** in
  the [issue tracker](https://gitlab.com/mantik-ai/mantik-api/issues).
* Make sure your issue **is really a bug**, and is not a support request.
* If you find a **closed** issue that seems like it is the same thing that you're experiencing, open a new issue and
  include a link to the original issue in the body of your new one.

Submit your bugs in the [issue tracker](https://gitlab.com/mantik-ai/mantik-api/issues):

1. Use the bug issue template. This can be selected from the dropdown menu in the `Description` field.
2. Completely fill out the template.
3. Save the issue.

### Suggesting improvements

You may submit enhancement suggestion such as new features, improvements of existing functionality, or performance by
submitting a ticket.
Before submitting a new suggestion, check that your **issue does not already exist** in
the [issue tracker](https://gitlab.com/mantik-ai/mantik-api/issues).

## Code contributions

### SQLAlchemy conventions

In order to have a cohesive SQLAlchemy codebase,
we want to have some guidelines/conventions regarding SQLAlchemy related code in this repo.

As a general rule look at what is already there before implementing new ORMs.

Some more specific guidelines:

- A new ORM class extends an abstract class defined in `mantik_api/database/base.py`
- Table names are singular (e.g. `user`)
- Any PK Id column name is `id`.
- Any FK is foreign table name + `_id` (e.g. `user_id`).
- Follow the namings for association tables
    - Class name: `FirstORMSecondORMAssociationTable` (e.g. `CodeRepositoryLabelAssociationTable`)
    - Table name: `first_orm_second_orm_association_table` (e.g. `code_repository_label_association_table`)
    - Id names are orm name + `_id` (e.g. `user_id`), like FK.

### Development and testing

We use [poetry](https://python-poetry.org/docs/) (`poetry>=1.2.0`) for dependency management and packaging.
Thus an installation of poetry is required.
A guide on how to install poetry can be found [here](https://python-poetry.org/docs/#installation).

You might further need to install `gcc` and `python3.11-dev`.

```bash
apt-get install gcc
apt-get install python3.11-dev
```


#### Integration tests

##### Requirements

- Docker ([engine](https://docs.docker.com/engine/install/ubuntu/))
- docker-compose ([standalone](https://docs.docker.com/compose/install/other/) install recommended)

This assumes that docker can be run as a non-root
user ([see instructions here](https://docs.docker.com/engine/install/linux-postinstall/))


##### Running the tests

To start up all the docker containers related to the integration tests:
```bash
make up-integration
```

Run the integration tests

```bash
poetry run pytest src/tests/integration
```

*Note*, correct initialization of database may require killing, removing and restarting database before tests work.

To kill running database run completely, which will reset all data contained

```bash
make stop-db
```

Optionally, you can get access to the locally running database via

```bash
docker exec -ti mantik-api-test-db /bin/bash
```

See [this post](https://earthly.dev/blog/postgres-docker/) for a similar example with additional useful commands (
running with docker not docker-compose though).


##### Testing firecREST

There are some integration tests for firecREST that can only be executed locally
with a running firecREST instance (i.e. all required microservices).
These tests can _only_ be run using the `--use-local-firecrest` CLI option:


1. Start the local firecREST microservices:
    1. Clone the [firecREST GitHub repo](https://github.com/eth-cscs/firecrest)
       into a separate folder

       ```bash
       git clone https://github.com/eth-cscs/firecrest.git
       ```

       **Note:** Cloning into a subfolder of this repo caused technical issues.

    2. Move to `demo` directory

       ```shell
       cd deploy/demo
       ```

    3. Change the permissions of the keys used by the certificator

       ```bash
       chmod 400 ../test-build/environment/keys/ca-key  ../test-build/environment/keys/user-key
       ```

    4. Build and run containers

       ```bash
       docker compose build
       docker compose up -d
       ```

       **Note:** If `docker compose build` fails because the base image `f7t-base`
       could not be found, try to first build it explicitly

       ```bash
       docker-compose build f7t-base
       ```

       or by passing the entire build order

       ```bash
       docker-compose build f7t-base certificator client compute reservations status storage tasks utilities
       ```

2. Run the tests
   ```bash
   poetry run pytest src/tests/integration --use-local-firecrest
   ```

#### Test CI locally

Install gitlab-runner ([see this example](https://www.lullabot.com/articles/debugging-jobs-gitlab-ci)) on your machine.
Usually, you would need to register your gitlab-ci tokens, which is however not required as we want to run
everything locally.

Therefore, just executing the integration test in the ci can be done with a single command

```bash
gitlab-runner exec docker "<job name>"
```

#### Database Migrations

To create a new database migration:

```bash
make create-migrations
```

> _Note:_ Sometimes the last command fails, because the migrations have to be manually adjusted.

Commit the autogenerated file with the project. On fastapi startup, the migrations will be applied automatically.

### API Versioning

The API defines one stable version given in the `pyproject.toml` `tool.poetry.version` section.
Stable API endpoints are served under the default path without a version prefix as well as a path with that version prefix.
For example, if the current stable version is `0.2.2`, the stable APIs are served at `/` and `/v0.2`.

New or updated APIs, which underwent breaking changes, are served under the next minor version,
e.g. `/v0.3` given the above example.
These APIs may still undergo breaking changes until the stable API version is updated.

#### Introducing API-breaking Changes or New APIs

The route versions are managed in `src/mantik_api/routes/versions.py`.

- For API-breaking changes, the previously stable API should be kept and a new version introduced
  and served under the upcoming minor version route.
- New API endpoints should go to the upcoming minor version route.
- If an API undergoes changes that are not visible to the client, e.g. bug fixes or optional query or body
  parameters, it can still be served under the stable release route.

#### Upgrade Mantik API Version

To update the stable Mantik API version:

1. Bump the version with the `make` command

   ```bash
   make change-stable-api-version version=<new-version - e.g. 0.2.0>
   ```

2. In `src/mantik_api/app.py`, change the prefix of the router with the previous version
   (`V_<previous stable minor version>_ROUTERS`, e.g. `V_0_2_ROUTERS`)
   manually to that version.
3. All unchanged routes that don't have an updated version move to the router of the new stable version
   (`V_<new stable minor version>_ROUTERS`, e.g. `V_0_3_ROUTERS` by copy-pasting.
   This is essential to also be able to still serve them under the old version path.

   The new stable routes are then also used for the default path without version prefix.
4. A new (`V_<next minor version>_ROUTERS`, e.g. `V_0_4_ROUTERS`) could get added,
   but will be empty until any API receives a breaking change or new APIs are introduced.

### Merge requests

* Describe your changes as accurately as possible.
  The merge request body should be kept up to date as it will usually form the base for the changelog entry.
* Be sure that your merge request contains tests that cover the changed or added code.
  Tests are generally required for code be to be considered mergable, and code without passing tests will not be merged.
  Ideally, the test coverage should not decrease.
* Ensure your merge request passes the pre-commit checks.
  Remember that you can run these tools locally.
* If your changes warrant a documentation change, the merge request must also update the documentation.

**Note:**
Make sure your branch is [rebased](https://docs.github.com/en/get-started/using-git/about-git-rebase) against the latest
main branch.


### Labels

The `labels/labels.yaml` file contains a record of all existing labels. These labels are grouped into
categories, and each category has one or more scopes. Some categories might also be further divided into
subcategories. Every label is assigned both an identifier, used for internal bookkeeping, and a name, which is visible to
users. It is important to note that the identifier in the `labels/labels.yaml` file is _not_ the label ID (UUID)
assigned by the database. Changing a label's identifier will result in the deletion of the old label and the creation of a new label with the same name
and identifier but a new UUID. This action will remove the label from all entities where it was previously applied.

#### Adding a new label

A new label can be added at any time by extending one of the existing categories/subcategories or adding a new
categories/subcategories.

**Notes:**

- Make sure that the label identifier is unique.
- The identifier should conform to the conventions (see below).

#### Label Identifier Conventions

The identifier conventions are as follows:

Each identifier has 4 digits, where

- the first digit is the number of the category
- the second digit is the number of the sub-category
- the third and fourth digit is the number of the label within that (sub-)category

**Examples:**

- The `Task` category is category `0`, and `Tabular` sub-category `0` within `Tasks`.
  Hence, the first label, named `Tabular Classification` is label `0` and has identifier `0000`.
- The `Software` category is the 2nd category (category `1`) and has the sub-category `Programming Languages` (sub-category `0`) and `Frameworks` (sub-category `1`).
  The PyTorch framework is the 12th label (label `11`) in the `Software > Frameworks` sub-category.
  It thus has the identifier `1111`.

And so forth.

#### Adding a new scope

Scopes are specified in the `Scope` enumeration, which is part of the database schema for `Label` defined in the
file `src/mantik_api/database/label.py`. To add a new scope, simply extend the `Scope` enumeration.


### Known Issues

##### A string literal cannot contain NUL (0x00) characters

Sometimes a test randomly starts to fail because of the error `ValueError: A string literal cannot contain NUL (0x00) characters.`

<details><summary>Full Error Traceback</summary>

```shell
ERROR:mantik_api.database.client.main:Database transaction failed, rolling back transaction
Traceback (most recent call last):
  File "/builds/mantik-ai/mantik-api/src/mantik_api/database/client/main.py", line 87, in session
    yield self
  File "/builds/mantik-ai/mantik-api/src/mantik_api/routes/tokens/tokens_v_0_3.py", line 97, in refresh_token
    user = db.get_one(
           ^^^^^^^^^^^
  File "/builds/mantik-ai/mantik-api/src/mantik_api/database/client/main.py", line 52, in wrapper
    return func(*args, **kwargs)
           ^^^^^^^^^^^^^^^^^^^^^
  File "/builds/mantik-ai/mantik-api/src/mantik_api/database/client/main.py", line 201, in get_one
    return self._session_handler.get_one_or_none(select_one.get_query())
           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  File "/builds/mantik-ai/mantik-api/src/mantik_api/database/client/sessions.py", line 142, in get_one_or_none
    result = self._session.execute(query)
             ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  File "/usr/local/lib/python3.11/site-packages/sqlalchemy/orm/session.py", line 1717, in execute
    result = conn._execute_20(statement, params or {}, execution_options)
             ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  File "/usr/local/lib/python3.11/site-packages/sqlalchemy/engine/base.py", line 1710, in _execute_20
    return meth(self, args_10style, kwargs_10style, execution_options)
           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  File "/usr/local/lib/python3.11/site-packages/sqlalchemy/sql/elements.py", line 334, in _execute_on_connection
    return connection._execute_clauseelement(
           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  File "/usr/local/lib/python3.11/site-packages/sqlalchemy/engine/base.py", line 1577, in _execute_clauseelement
    ret = self._execute_context(
          ^^^^^^^^^^^^^^^^^^^^^^
  File "/usr/local/lib/python3.11/site-packages/sqlalchemy/engine/base.py", line 1948, in _execute_context
    self._handle_dbapi_exception(
  File "/usr/local/lib/python3.11/site-packages/sqlalchemy/engine/base.py", line 2133, in _handle_dbapi_exception
    util.raise_(exc_info[1], with_traceback=exc_info[2])
  File "/usr/local/lib/python3.11/site-packages/sqlalchemy/util/compat.py", line 211, in raise_
    raise exception
  File "/usr/local/lib/python3.11/site-packages/sqlalchemy/engine/base.py", line 1905, in _execute_context
    self.dialect.do_execute(
  File "/usr/local/lib/python3.11/site-packages/sqlalchemy/engine/default.py", line 736, in do_execute
    cursor.execute(statement, parameters)
ValueError: A string literal cannot contain NUL (0x00) characters.
```

</details>

## Contributors

Omar Ahmed, Sara Ansari, Elia Boscaini, Fabian Emmerich, Kristian Ehlert, Hagen-Henrik Kowalski, Thomas Seidler, Jakub
Jagielski
