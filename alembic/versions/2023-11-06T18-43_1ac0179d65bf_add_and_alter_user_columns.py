"""Add and alter User columns

Revision ID: 1ac0179d65bf
Revises: 361cf802d148
Create Date: 2023-11-06 18:43:54.549173

"""
import sqlalchemy as sa
from alembic import op


# revision identifiers, used by Alembic.
revision = "1ac0179d65bf"
down_revision = "361cf802d148"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column(
        "mantik_user", sa.Column("full_name", sa.String(length=101), nullable=True)
    )
    op.add_column("mantik_user", sa.Column("info", sa.String(), nullable=True))
    op.add_column(
        "mantik_user", sa.Column("company", sa.String(length=50), nullable=True)
    )
    op.add_column(
        "mantik_user", sa.Column("job_title", sa.String(length=50), nullable=True)
    )
    op.add_column(
        "mantik_user", sa.Column("website_url", sa.String(length=200), nullable=True)
    )
    op.alter_column(
        table_name="mantik_user",
        column_name="name",
        new_column_name="cognito_name",
        nullable=False,
        comment="Reflects the name of the Congito user and is _not_ mutable.\n\n"
        "It's only used for internal interaction with Cognito.\n"
        "The API should _always_ just return the `name`.\n",
    )
    op.add_column(
        "mantik_user",
        sa.Column(
            "preferred_name",
            sa.String(),
            comment="Cognito does _not_ allow to change the initial username.\n",
        ),
    )
    op.execute("UPDATE mantik_user SET preferred_name=cognito_name")
    op.alter_column("mantik_user", "preferred_name", nullable=False)


def downgrade() -> None:
    op.drop_column("mantik_user", "website_url")
    op.drop_column("mantik_user", "job_title")
    op.drop_column("mantik_user", "company")
    op.drop_column("mantik_user", "info")
    op.drop_column("mantik_user", "full_name")
    op.alter_column(
        table_name="mantik_user",
        column_name="cognito_name",
        new_column_name="name",
        nullable=False,
    )

    op.drop_column("mantik_user", "preferred_name")
