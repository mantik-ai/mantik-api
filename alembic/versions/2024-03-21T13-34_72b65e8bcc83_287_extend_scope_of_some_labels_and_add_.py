"""287-extend-scope-of-some-labels-and-add-new-labels

Revision ID: 72b65e8bcc83
Revises: 0509710b1944
Create Date: 2024-03-21 13:34:29.997470

"""
import sqlalchemy as sa
from alembic import op


# revision identifiers, used by Alembic.
revision = "72b65e8bcc83"
down_revision = "0509710b1944"
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column(
        table_name="label",
        column_name="category",
        type_=sa.Enum(
            "Tasks",
            "Software",
            "Datasets",
            "Domain",
            "Licenses",
            "Models",
            name="category",
            native_enum=False,
        ),
        nullable=True,
    )
    op.alter_column(
        table_name="label",
        column_name="sub_category",
        type_=sa.Enum(
            "Multimodal",
            "Tabular",
            "NaturalLanguageProcessing",
            "ComputerVision",
            "Audio",
            "ReinforcementLearning",
            "ProgrammingLanguage",
            "Library",
            "Size",
            "Source",
            "Databases",
            "Software",
            "Data",
            "NeuralNetworks",
            "Clustering",
            "Classification",
            "Linear",
            "Ensemble",
            "DimensionalityReduction",
            "Manifold",
            "NLP",
            "ImageProcessing",
            name="subcategory",
            native_enum=False,
        ),
        nullable=True,
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column(
        table_name="label",
        column_name="category",
        type_=sa.Enum(
            "Tasks",
            "Software",
            "Datasets",
            "Domain",
            "Licenses",
            name="category",
            native_enum=False,
        ),
        nullable=True,
    )
    op.alter_column(
        table_name="label",
        column_name="sub_category",
        type_=sa.Enum(
            "Multimodal",
            "Tabular",
            "NaturalLanguageProcessing",
            "ComputerVision",
            "Audio",
            "ReinforcementLearning",
            "ProgrammingLanguage",
            "Library",
            "Size",
            "Software",
            "Data",
            name="subcategory",
            native_enum=False,
        ),
        nullable=True,
    )
    # ### end Alembic commands ###
