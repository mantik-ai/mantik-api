"""45-ids-handling-in-api

Revision ID: f10320002af0
Revises: 6b7ea6f15448
Create Date: 2023-05-10 14:55:26.205773

"""
import sqlalchemy as sa
import sqlalchemy_utils
from alembic import op


# revision identifiers, used by Alembic.
revision = "f10320002af0"
down_revision = "6b7ea6f15448"
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column("project", "public", existing_type=sa.BOOLEAN(), nullable=False)
    op.add_column("run", sa.Column("name", sa.String(), nullable=True))
    op.add_column(
        "run",
        sa.Column(
            "mlflow_run_id",
            sqlalchemy_utils.types.uuid.UUIDType(binary=False),
            nullable=True,
        ),
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("run", "mlflow_run_id")
    op.drop_column("run", "name")
    op.alter_column("project", "public", existing_type=sa.BOOLEAN(), nullable=True)
    # ### end Alembic commands ###
