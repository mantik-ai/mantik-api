"""Alter Table CodeRepository with new attribute platform

Revision ID: b78c1e1e8d3a
Revises: b407c8b58b30
Create Date: 2023-11-30 16:12:38.523395

"""
import sqlalchemy as sa
from alembic import op


# revision identifiers, used by Alembic.
revision = "b78c1e1e8d3a"
down_revision = "41192b2c37ed"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column(
        "code_repository",
        sa.Column(
            "platform",
            sa.Enum(
                "GitHub", "GitLab", "Bitbucket", name="platform", native_enum=False
            ),
        ),
    )
    op.execute("UPDATE code_repository SET platform='GitHub'")
    op.alter_column("code_repository", "platform", nullable=False)


def downgrade() -> None:
    op.drop_column("code_repository", "platform")
