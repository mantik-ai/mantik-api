"""empty message

Revision ID: 26391b15a7dd
Revises: 0e00653fd264, 2de643562c59
Create Date: 2025-03-04 14:02:36.493703

"""
# revision identifiers, used by Alembic.
revision = "26391b15a7dd"
down_revision = ("0e00653fd264", "2de643562c59")
branch_labels = None
depends_on = None


def upgrade() -> None:
    pass


def downgrade() -> None:
    pass
