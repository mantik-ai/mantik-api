"""322-Bookkeep-mantik-runs-2

Revision ID: 0e00653fd264
Revises: c41958fbd916
Create Date: 2025-02-27 14:04:14.322387

"""
from alembic import op
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "0e00653fd264"
down_revision = "c41958fbd916"
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column(
        "model_repository",
        "code_repository_id",
        existing_type=postgresql.UUID(),
        nullable=True,
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column(
        "model_repository",
        "code_repository_id",
        existing_type=postgresql.UUID(),
        nullable=False,
    )
    # ### end Alembic commands ###
