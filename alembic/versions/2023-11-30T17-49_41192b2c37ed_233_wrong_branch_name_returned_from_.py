"""233-wrong-branch-name-returned-from-backend-in-rerun-form-if-run-was-submitted-with-commit-hash

Revision ID: 41192b2c37ed
Revises: b407c8b58b30
Create Date: 2023-11-30 17:49:10.645137

"""
import sqlalchemy.sql
from alembic import op


# revision identifiers, used by Alembic.
revision = "41192b2c37ed"
down_revision = "b407c8b58b30"
branch_labels = None
depends_on = None


def upgrade() -> None:
    conn = op.get_bind()
    # Frontend has been sending empty commit thus far, hence set to NULL where empty
    # string is given.
    conn.execute(
        sqlalchemy.sql.text(
            "UPDATE model_repository SET commit = NULL WHERE commit = '';"
        )
    )
    # Set branch to NULL for all model repositories where a commit hash was given.
    conn.execute(
        sqlalchemy.sql.text(
            "UPDATE model_repository SET branch = NULL WHERE commit IS NOT NULL;"
        )
    )


def downgrade() -> None:
    pass
