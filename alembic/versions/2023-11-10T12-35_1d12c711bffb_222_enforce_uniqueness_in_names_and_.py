"""222-enforce-uniqueness-in-names-and-emails

Revision ID: 1d12c711bffb
Revises: 1ac0179d65bf
Create Date: 2023-11-10 12:35:15.739922

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = "1d12c711bffb"
down_revision = "1ac0179d65bf"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_unique_constraint(
        "user_cognito_name_unique", "mantik_user", ["cognito_name"]
    )
    op.create_unique_constraint("user_email_unique", "mantik_user", ["email"])
    op.create_unique_constraint(
        "user_preferred_name_unique", "mantik_user", ["preferred_name"]
    )


def downgrade() -> None:
    op.drop_constraint("user_cognito_name_unique", "mantik_user")
    op.drop_constraint("user_email_unique", "mantik_user")
    op.drop_constraint("user_preferred_name_unique", "mantik_user")
