# mantik_api

This repo contains the implementation of the mantik API backend.

We use SQLAlchemy for database management and fastAPI for API implementation.
